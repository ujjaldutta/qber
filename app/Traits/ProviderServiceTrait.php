<?php
/****************************************** For Provider  App, Registration, ServiceController and Home Controller ***********************************/

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Hash;
use Response;
use Session;
use App\Models\Provider;
use App\Models\Providerprofile;
use App\Models\Customer;
use App\Models\Servicetype;
use App\Models\Service;
use App\Models\Providerworktime;
use App\Models\Servicestatus;
use App\Models\Providerpreferencelocation;
use App\Models\Providertimehistory;
use App\Models\Servicesettings;
use App\Models\Providerfreetime;
use App\Models\Providerbookedtime;
use App\Models\CustomerRatings;
use App\Models\Providernotification;
use App\Models\Customernotification;
use App\Models\ProviderCancelHistory;
use App\Models\CompanyProvider;
use App\Models\ProviderCompanyAccount;
use App\Models\Providerbalancehistory;
use App\Models\Servicecardetails;
use App\Models\Appointments;
use App\Models\Cancelreason;
use App\Models\Serviceoptiondetails;
use App\Models\ProviderMessage;
use App\Models\ProviderRatings;
use App\Models\Providerschedule;
use App\Models\Smslog;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;



trait ProviderServiceTrait {

	/***********************  update provider details*******************************/

	protected function setProviderProfileTrait($request) {

		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		
		
		$profile=Providerprofile::where('provider_id',$provider->id)->first();
		if(!$profile){
			
			$profile=new Providerprofile();
			$profile->provider_id=$provider->id;
			$profile->save();
			$profile=Providerprofile::where('provider_id',$provider->id)->first();
		}

		
		

        $banner_img1 = "";
        if ($request->hasFile('banner_img1')) {
            $file = $request->file('banner_img1');
            $filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
            $extension = $file->getClientOriginalExtension();
            $banner_img1 = $filename ."_". time() . '.' . $extension;
            $profile->banner_img1 = "media/provider/banners/".$banner_img1;
        }

        $banner_img2 = "";
        if ($request->hasFile('banner_img2')) {
            $file = $request->file('banner_img2');
            $filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
            $extension = $file->getClientOriginalExtension();
            $banner_img2 = $filename ."_". time() . '.' . $extension;
            $profile->banner_img2 = "media/provider/banners/".$banner_img2;
        }

        $banner_img3 = "";
        if ($request->hasFile('banner_img3')) {
            $file = $request->file('banner_img3');
            $filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
            $extension = $file->getClientOriginalExtension();
            $banner_img3 = $filename ."_". time() . '.' . $extension;
            $profile->banner_img3 = "media/provider/banners/".$banner_img3;
        }


			$profile->date_of_birth=$request->input('date_of_birth');
			$profile->gender=$request->input('gender');
			$profile->nationality=$request->input('nationality');
			$profile->internet_access=$request->input('internet_access');
			$profile->transport=$request->input('transport');

			
			$profile->your_offer=$request->input('your_offer');
			$profile->year_of_experience=$request->input('year_of_experience');
			$profile->live_in_qatar=$request->input('live_in_qatar');
			$profile->work_preference=$request->input('work_preference');
			$profile->language_preference=$request->input('language_preference');
			$profile->work_in_company=$request->input('work_in_company');
			$profile->shop_name=$request->input('shop_name');


			/*************************************Profile Picture********************************************/
			$image = "";
			if ($request->hasFile('profile_image')) {
				$file = $request->file('profile_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$image = $filename ."_". time() . '.' . $extension;
				$profile->image = "media/provider/".$image;
			}

			
			if ($request->hasFile('profile_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('profile_image')->move($destinationPath, $image);
			}


			/*************************************Admin Profile Picture********************************************/
			$admin_profile_image = "";
			if ($request->hasFile('admin_profile_image')) {
				$file = $request->file('admin_profile_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$admin_profile_image = $filename ."_". time() . '.' . $extension;
				$profile->admin_profile_image = "media/provider/".$admin_profile_image;
			}

			
			if ($request->hasFile('admin_profile_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('admin_profile_image')->move($destinationPath, $admin_profile_image);
			}


			
			/*************************************Tools Picture ********************************************/
			$tools_use_image = "";
			if ($request->hasFile('tools_use_image')) {
				$file = $request->file('tools_use_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$tools_use_image = $filename ."_". time() . '.' . $extension;
				$profile->tools_use_image = "media/provider/".$tools_use_image;
			}

			
			if ($request->hasFile('tools_use_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('tools_use_image')->move($destinationPath, $tools_use_image);
			}


			/*************************************Shop Picture ********************************************/	
			
			$shop_image = "";
			if ($request->hasFile('shop_image')) {
				$file = $request->file('shop_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$shop_image = $filename ."_". time() . '.' . $extension;
				$profile->shop_image = "media/provider/".$shop_image;
			}

			
			if ($request->hasFile('shop_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('shop_image')->move($destinationPath, $shop_image);
			}

			/*************************************CV File ********************************************/	
			
			$cv_file = "";
			if ($request->hasFile('cv_file')) {
				$file = $request->file('cv_file');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$cv_file = $filename ."_". time() . '.' . $extension;
				$profile->cv_file = "media/provider/".$cv_file;
			}

			
			if ($request->hasFile('cv_file')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('cv_file')->move($destinationPath, $cv_file);
			}


			/*************************************Car Registration Image ********************************************/	
			
			$car_registration_image = "";
			if ($request->hasFile('car_registration_image')) {
				$file = $request->file('car_registration_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$car_registration_image = $filename ."_". time() . '.' . $extension;
				$profile->car_registration_image = "media/provider/".$car_registration_image;
			}

			
			if ($request->hasFile('car_registration_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('car_registration_image')->move($destinationPath, $car_registration_image);
			}


			/*************************************Driving License Image ********************************************/	
			
			$driving_license_image = "";
			if ($request->hasFile('driving_license_image')) {
				$file = $request->file('driving_license_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$driving_license_image = $filename ."_". time() . '.' . $extension;
				$profile->driving_license_image = "media/provider/".$driving_license_image;
			}

			
			if ($request->hasFile('driving_license_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('driving_license_image')->move($destinationPath, $driving_license_image);
			}


			/*************************************Quatar ID Image ********************************************/	
			
			$identity_image = "";
			if ($request->hasFile('identity_image')) {
				$file = $request->file('identity_image');
				$filename = preg_replace("/[^A-Za-z0-9]/", "", $file->getClientOriginalName());
				$extension = $file->getClientOriginalExtension();
				$identity_image = $filename ."_". time() . '.' . $extension;
				$profile->identity_image = "media/provider/".$identity_image;
			}

			
			if ($request->hasFile('identity_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('identity_image')->move($destinationPath, $identity_image);
			}

		//---------------------delete image part------------------------

		if($request->input('profile_image_delete')=='1')
		{
			$oldimage=$profile->image;
			@unlink($oldimage);
			$profile->image='';

		}
		if($request->input('banner_img1_delete')=='1')
		{
			$oldimage=$profile->banner_img1;
			@unlink($oldimage);
			$profile->banner_img1='';

		}
		if($request->input('banner_img2_delete')=='1')
		{
			$oldimage=$profile->banner_img2;
			@unlink($oldimage);
			$profile->banner_img2='';

		}
		if($request->input('banner_img3_delete')=='1')
		{
			$oldimage=$profile->banner_img3;
			@unlink($oldimage);
			$profile->banner_img3='';

		}
		if($request->input('email')!='')
		{
			$email=$provider->email;
			if($email!=$request->input('email')){
				$provider->email=$request->input('email');
				$provider->email_verified=0;
				
			}
		}
		
		$provider->name=$request->input('name');;
		
		$profile->service_details= $request->input('service_details');

		$provider->save();
		$profile->save();
		













		if ($request->hasFile('image')) {
            
            $destinationPath = public_path() . '/media/provider/' ;
            $request->file('image')->move($destinationPath, $image);
		}

		if ($request->hasFile('banner_img1')) {
            
            $destinationPath = public_path() . '/media/provider/banners/' ;
            $request->file('banner_img1')->move($destinationPath, $banner_img1);

        }
        if ($request->hasFile('banner_img2')) {
            
            $destinationPath = public_path() . '/media/provider/banners/' ;
            $request->file('banner_img2')->move($destinationPath, $banner_img2);

        }
        if ($request->hasFile('banner_img3')) {
            
            $destinationPath = public_path() . '/media/provider/banners/' ;
            $request->file('banner_img3')->move($destinationPath, $banner_img3);

        }

        return true;
	}
	

	/***********************  this function search providers which is called in Service Controller , called by customer *******************************/
   
    protected function getProviders($service) {

		

		$time_start = microtime(true); 
		$provider_limit=Servicesettings::select('code','value')->where('code','provider_display_limit')->where('type_id',$service['type_id'])->first();
	
		$providers=Provider::with(['Profile' => function ($query) use ($service) {
			
		}])
		

		->leftJoin('provider_rating','providers.id','=','provider_rating.provider_id')
		->selectRaw("providers.*,AVG(rate_value) as ratings_average,count(rate_value) as ratings_count")
		//whereIn('id', function($query){

		//})
		//->select('*')
		//->selectRaw("providers.*,AVG(rate_value) as ratings_average,count(rate_value) as ratings_count")
		//->leftJoin('provider_rating','providers.id','=','provider_rating.provider_id')
		
		->where('is_online','1')
		->where('sms_verified','1')
		//->where('providers.id','=','28')
		->where('approve_status','1')
		->where('type_id',$service['type_id'])
		->where('is_active','1')
		->where('ready_to_serve','1')
		//->where('provider_status','free')
		->groupBy('providers.id')
		->orderBy('priority', 'desc')->orderBy('ratings_count', 'desc')
		->skip(0)->take($provider_limit->value+50);


		if($service['type_id']!=1){
		$providers=$providers->whereHas('Profile', function ($query)  use ($service){
				$query->where('nationality',$service['nationality']);
			});
		}
		
		$providers=$providers->get();
		$preferedProviders=array();
		$invalidProviders=array();
		//print_r($providers->toArray());exit;
		
		//check provider location preference within area
		foreach($providers as $provider){
			
				/*if($provider->location_preference=='fixed')
				{
					$status=$this->validateProviderPreference($service,$provider);
					
					if($status)
					$preferedProviders[]=$provider->toArray();
					
				}else{
					
					$preferedProviders[]=$provider->toArray();
					

				}*/
			//logic change according to client requirement
				$status=false;
			
				$status=$this->validateProviderPreference($service,$provider);
					
					if($status)
					$preferedProviders[]=$provider->toArray();
					else
					$invalidProviders[]=array("id"=>$provider->id,"error"=>"Invalid preference or service location outside service area");

		}
		
//print_r($preferedProviders);exit;

//filterout who has no car more than 1 km of service
		$providerDistanceValidated=array();
		$i=0;
		foreach($preferedProviders as $provider){
				if($provider['profile']['transport']=='walk'){
					
					if($this->distanceBetweenServiceProvider($service,$provider)){
						$providerDistanceValidated[]=$provider;
					}else{
						$invalidProviders[]=array("id"=>$provider['id'],"error"=>"Outside 1 km for walk");
					}
				}else{
					$providerDistanceValidated[]=$provider;
				}

				
			
		}

		

//print_r($providerDistanceValidated);exit;
$range=Servicesettings::select('code','value')->where('code','radius_range')->where('type_id',$service['type_id'])->first();
//filterout who has with car more than 30 km of service
		$providerDistanceRadiousValidated=array();
		$i=0;
		foreach($providerDistanceValidated as $provider){
				if($provider['profile']['transport']!='walk' && $range->value >0){
					
					if($this->distanceBetweenServiceProvider($service,$provider,$range->value)){
						$providerDistanceRadiousValidated[]=$provider;
					}else{
						$invalidProviders[]=array("id"=>$provider['id'],"error"=>"Outside radius range");
					}
				}else{

					$providerDistanceRadiousValidated[]=$provider;
				}

				
			
		}

		
//print_r($providerDistanceRadiousValidated);exit;		

//filterout who is already requested and not cancelled or confirmed
	$providerAlreadyBooked=array();
	
	foreach($providerDistanceRadiousValidated as $provider){

			if($this->CheckAlreadyRequested($provider)){
				$providerAlreadyBooked[]=$provider;
			}else{
				$invalidProviders[]=array("id"=>$provider['id'],"error"=>"Already Requested");
			}

	}

	
//print_r($providerAlreadyBooked);exit;

	$validPaidProviders=array();
foreach($providerAlreadyBooked as $provider){

			if($this->CheckIfPaid($provider)){
				$validPaidProviders[]=$provider;
			}else{
				$invalidProviders[]=array("id"=>$provider['id'],"error"=>"Provider not paid");
			}

	}

	
	
//print_r($validPaidProviders);exit;
	
	
	$providerFreeSlot=array();
	
	foreach($validPaidProviders as $provider){
			if($this->validatedFreeSlot($service,$provider)){
				$providerFreeSlot[]=$provider;

			}else{
				$invalidProviders[]=array("id"=>$provider['id'],"error"=>"Free slot not found or map api limit exceed");
			}

	}
//print_r($providerFreeSlot);exit;
	$time_end = microtime(true);
		$execution_time = ($time_end - $time_start);
		//echo 'Total Execution Time:'.$execution_time.' Seconds'. PHP_EOL;
		
	$pid=array();
	foreach($providerFreeSlot as $provider){
		$pid[]=$provider['id'];

	}

	
//print_r($pid);exit;		
	

	$providers=Provider::with('Profile')
	->leftJoin('provider_rating','providers.id','=','provider_rating.provider_id')
		->selectRaw("providers.*,AVG(rate_value) as ratings_average,count(rate_value) as ratings_count")
		 //->select(array('providers.*', DB::raw('AVG(rate_value) as ratings_average')  ))
		->groupBy('providers.id')
		->where('providers.current_latitude','<>','')
		->where('providers.current_longitude','<>','')
		 ->whereIn('providers.id', $pid)
		->orderBy('ratings_count', 'desc')->orderBy('priority', 'desc')->skip(0)->take($provider_limit->value)->get();
		
	foreach($providers as $provider){

				$mode=($provider->profile->transport=='walk')?'walking':'driving';
				
			
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='destinations='.$service['latitude'].','.$service['longitude'];
				$destination='&origins='.$provider->current_latitude.','.$provider->current_longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
				$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);

				$provider->distance=$distance;
				$provider->duration=$duration;
				
				
				$from_time=date("Y-m-d H:i:s", strtotime('-60 minutes', strtotime($service['from_time'])));
				$to_time=date("Y-m-d H:i:s", strtotime('+60 minutes', strtotime($service['to_time'])));

				/*$bslot=Providerbookedtime::with('Service')
					->selectRaw("from_time,to_time")
					->where('provider_id',$provider->id)
					->whereRaw("to_time between  '".date("Y-m-d H:i:s",strtotime($from_time))."' and '".date("Y-m-d H:i:s",strtotime($to_time))."' and from_time between  '".date("Y-m-d H:i:s",strtotime($from_time))."' and '".date("Y-m-d H:i:s",strtotime($to_time))."' ")
					->whereDate('book_date',date("Y-m-d",strtotime($to_time)))
					->whereHas('Service', function ($query){
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
					})

					->orWhere('provider_id',$provider->id)
					->whereDate('book_date',date("Y-m-d",strtotime($to_time)))
					->whereRaw("'".date("Y-m-d H:i:s",strtotime($from_time))."' between from_time and to_time or '".date("Y-m-d H:i:s",strtotime($to_time))."'
					between from_time and to_time")
					->whereHas('Service', function ($query){
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
					})

					->orWhere('provider_id',$provider->id)
					->whereDate('book_date',date("Y-m-d",strtotime($to_time)))
					->whereRaw("from_time<'".date("Y-m-d H:i:s",strtotime($from_time))."' and to_time>'".date("Y-m-d H:i:s",strtotime($to_time))."'")
					->whereHas('Service', function ($query){
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
					})
					->orderby('from_time','asc')->get();

					
					$free=array();
					
					if($bslot ){
						$free=Providerfreetime::where('provider_id',$provider->id)
							->where('time_type','unavailable')
							->whereDate('date',date("Y-m-d",strtotime($from_time)))
							->get();

					}

					$provider->booked_timeslot=array("booked"=>$bslot,"slots"=>$free,"from_time"=>$from_time,"to_time"=>$to_time);*/
					//$provider->booked_timeslot=$this->getCurrentSchedule($provider,$from_time);
					$bslot=Providerschedule::
					where("work_start_time",'>=',$from_time)->where("work_end_time",'<=',$to_time)->where("provider_id",$provider->id)->whereDate('date',date("Y-m-d",strtotime($to_time)))
					->orWhere("provider_id",$provider->id)->whereRaw(" (('".$from_time."' between from_time and to_time and time_type<>'booked' and date='".date("Y-m-d",strtotime($to_time))."') or ('".$to_time."' between from_time and to_time and time_type<>'booked' and date='".date("Y-m-d",strtotime($to_time))."'))  ")
					->orWhere("provider_id",$provider->id)->whereRaw(" from_time > '".$from_time."' and time_type<>'booked' ")->whereDate('date',date("Y-m-d",strtotime($to_time)))
					->get();

					$provider->booked_timeslot=array("slots"=>$bslot,"from_time"=>$from_time,"to_time"=>$to_time);
					
				

			

	}	

	//$selectedProviders=$providerDistanceValidated;

		return array("providers"=>$providers,"invalidproviders"=>$invalidProviders);
	}

	//check if provider is already requested for service and not accepted or rejected when searching a provider
	private function CheckAlreadyRequested($provider){
		$service=Service::where('provider_id',$provider['id'])->where('service_status_id',1)->count();
		
		if($service>0)
		return false;

		//also check if there is any activity in past
		//$worktime=Providerworktime::where('provider_id',$provider['id'])->count();
		//if($worktime<=0)
		//return false;


		
		return true;

	}

	//validate free slot of provider when searching using service controller
	private function validatedFreeSlot($service,$provider){

		//first check if there is any booking on that day
		$booked=Providerbookedtime::with("Service")->where('provider_id',$provider['id'])
		->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
		->whereDate('from_time',date("Y-m-d",strtotime($service['to_time'])))->get();
		
		$mode=($provider['profile']['transport']=='walk')?'walking':'driving';

		$arrival_duration=Servicesettings::select('code','value')->where('code','arrival_duration')
								->where('type_id',$service['type_id'])->first();
		$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service['type_id'])->first();
		$buffer_time=(int)$buffer_time->value;
		
		if($booked->count()<=0){
			
			
			//get duration between provider and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				if($provider['base_latitude'] && $provider['base_longitude'] && $provider['location_preference']=='fixed')
				$origin='origins='.$provider['base_latitude'].','.$provider['base_longitude'];
				else
				$origin='origins='.$provider['current_latitude'].','.$provider['current_longitude'];
				
				$destination='&destinations='.$service['latitude'].','.$service['longitude'];
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);

				//echo $origin;
				//echo $destination;
				
				
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				$current_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
				
				$head_start=date("H:i:s",strtotime("- ".$current_torequest_duration." Minutes",strtotime($service['from_time'])));
				
				if($head_start<date("H:i:s"))
				$espected_request_start_time=date("H:i:s",strtotime("+ ".$current_torequest_duration." Minutes",time()));
				else
				$espected_request_start_time=date("H:i:s",strtotime($service['from_time']));

				
				$espected_request_end_time=date("H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($espected_request_start_time)));
				
				
				//if cant start in arrival time
				 $espected_current_work_start_time=date("Y-m-d H:i:s",strtotime("+ ".$current_torequest_duration." Minutes",time()));
				
				if($espected_current_work_start_time>date("Y-m-d H:i:s",strtotime($service['to_time'])))
				return false;
				
				//validate if duration is within max travel time
				if($current_torequest_duration>$arrival_duration->value)
					return false;

				
				$freeslot=Providerfreetime::where('provider_id',$provider['id'])
						->whereRaw(" `provider_id` = ".$provider['id']." and '".$espected_request_start_time."' between from_time and to_time and time_type='free' and date= '".date("Y-m-d",strtotime($service['from_time']))."'
						
						 AND '".$espected_request_end_time."' between from_time and to_time and time_type='free' and `provider_id` = ".$provider['id']." and date= '".date("Y-m-d",strtotime($service['from_time']))."'
						")->count();
				
				if($freeslot<=0){
					
					$from_time=date("H:i:s",strtotime($service['from_time']));
					$to_time=date("H:i:s",strtotime($service['to_time']));
					
					 $freeslot=Providerfreetime::where('provider_id',$provider['id'])
						->whereRaw(" `provider_id` = ".$provider['id']." and from_time>='".$from_time."' and to_time<='".$to_time."' and time_type='free' and date= '".date("Y-m-d",strtotime($service['from_time']))."'
						
						
						")->count();
					if($freeslot>0) return true;
						
					$head_start_time=date("H:i:s",strtotime("-".$current_torequest_duration." Minutes",strtotime($service['to_time'])));
					$work_start_time=date("H:i:s",strtotime($service['to_time']));
					$work_end_time=date("H:i:s",strtotime('+'.$service['job_duration_set_by_provider'].' minutes',strtotime($work_start_time)));
					
					
					$fcount=Providerfreetime::where('provider_id',$provider['id'])
						->whereRaw(" `provider_id` = ".$provider['id']." and '".$head_start_time."' between from_time and to_time and time_type='free' and date= '".date("Y-m-d",strtotime($service['from_time']))."'
						
						 AND '".$work_start_time."' between from_time and to_time and time_type='free' and `provider_id` = ".$provider['id']." and date= '".date("Y-m-d",strtotime($service['from_time']))."'
						 AND '".$work_end_time."' between from_time and to_time and time_type='free' and `provider_id` = ".$provider['id']." and date= '".date("Y-m-d",strtotime($service['from_time']))."'
						")->count();

					
				
					if($fcount<=0)
					return false;
				 }
				
				

			//if no booking find the freeslot on requested date and time
			
			$freeslot=Providerfreetime::where('provider_id',$provider['id'])
					->where('from_time','<=',date("H:i:s",strtotime($service['from_time'])))
					->where('to_time','>=',date("H:i:s",strtotime($service['from_time'])))
					->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))
					->where('time_type','free')
					->orWhere('from_time','<=',date("H:i:s",strtotime($service['to_time'])))
					->where('to_time','>=',date("H:i:s",strtotime($service['to_time'])))
					->where('provider_id',$provider['id'])
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))
					->orWhere('provider_id',$provider['id'])->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))
					->whereRaw(" from_time <='".date("H:i:s",strtotime($service['from_time'])).
					"' and to_time >= '".date("H:i:s",strtotime($service['to_time']))."'")
					->orwhereRaw("'".$espected_request_start_time."' between from_time and to_time and '".$espected_request_end_time."' between from_time and to_time")
					->where('provider_id',$provider['id'])
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))

					->get();

				
				
			
			if($freeslot->count()>0) return true;

			
		}elseif($booked->count()>0){
			
			//if any booking found then verify if its eligible by using drive time, current working location,

			//1. find if there is any booking after current request
				

			$booked=Providerbookedtime::with('Service')->where('provider_id',$provider['id'])
			->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))
			->where('from_time','<=', $service['to_time'])
			->where('from_time','>=', $service['from_time'])
			->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
			->orWhere('to_time','<=', $service['to_time'])
			->where('provider_id',$provider['id'])
			->where('to_time','>=', $service['from_time'])
			->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))
			->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
			->orWhere('provider_id',$provider['id'])
			->whereRaw("to_time between  '".date("Y-m-d H:i:s",strtotime($service['from_time']))."' and '".date("Y-m-d H:i:s",strtotime($service['to_time']))."' and from_time between  '".date("Y-m-d H:i:s",strtotime($service['from_time']))."' and '".date("Y-m-d H:i:s",strtotime($service['to_time']))."' ")
			->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
			->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))
			

			->orderby('from_time','asc');

			
			
			//echo $booked->count();exit;
			if($booked && $booked->count()>1){
							
				//if there is booking after current request calculate duration and distance
				
				$booked=$booked->first();
				if(!$booked->service->id)
				return false;
				$working_service=Service::with("Booked")->where('provider_id',$provider['id'])->where('id',$booked->service->id)->first();

				//$working_service_totime=date("Y-m-d H:i:s", strtotime('+'.$working_service->job_duration_set_by_provider.' minutes', strtotime($working_service->from_time)));
				$working_service_totime=$working_service->booked->to_time;
				$travel_start_time=$working_service_totime;

				

				$confirmed=Providerbookedtime::with('Service')
				->where('provider_id',$provider['id'])
				->where('from_time','<=', $service['from_time'])
				->where('to_time','>=', $service['from_time'])
				->where(DB::raw("DATE(book_date)"),"=",date("Y-m-d",strtotime($service['to_time'])))
				->whereHas('Service', function ($query){
					$query->where("service_status_id",4);
					$query->orwhere("service_status_id",5);
					$query->orwhere("service_status_id",9);
					 
				})
				->orWhere('from_time','<=', $service['to_time'])
				->where('to_time','>=', $service['to_time'])
				//->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))
				->where(DB::raw("DATE(book_date)"),"=",date("Y-m-d",strtotime($service['to_time'])))
				->where('provider_id',$provider['id'])
				->whereHas('Service', function ($query){
						$query->where("service_status_id",4);
						$query->orwhere("service_status_id",5);
						$query->orwhere("service_status_id",9);
						 
					})
				->orWhere('provider_id',$provider['id'])
			->whereRaw("to_time between  '".date("Y-m-d H:i:s",strtotime($service['from_time']))."' and '".date("Y-m-d H:i:s",strtotime($service['to_time']))."' and from_time between  '".date("Y-m-d H:i:s",strtotime($service['from_time']))."' and '".date("Y-m-d H:i:s",strtotime($service['to_time']))."' ")
			->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))
			->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
			
				->orderby('from_time','desc');

				
				if($confirmed)
				{
					$confirmed=$confirmed->first();
					

				}				
				else
				return false;

				if(!$confirmed)
				return false;
				$confirmed_end=$confirmed->to_time;
				
				$duration_reach_at_confirmed_place=(strtotime($confirmed_end)-strtotime($travel_start_time))/60; //65

				

				//get duration between working (first booking) and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$working_service->latitude.','.$working_service->longitude;
				$destination='&destinations='.$service['latitude'].','.$service['longitude'];
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				$working_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10

			
				//validate if duration is within max travel time
				if($working_torequest_duration>$arrival_duration->value)
					return false;
				
				
				//get duration between request and booked confirmed location
				$origin='destinations='.$service['latitude'].','.$service['longitude'];
				$destination='&origins='.$confirmed->service->latitude.','.$confirmed->service->longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				$requestto_booked_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60); //5



				

				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($travel_start_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($espected_request_start_time)));
				$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;
			

				
				$total_expected_duration=$buffer_time+(int)$working_torequest_duration+(int)$requestto_booked_duration+(int)$request_service_duration;


				
				



				if($total_expected_duration<$duration_reach_at_confirmed_place){
					
					//provider can accept because of enough time and now find if provider got free slot

					//echo $espected_request_start_time;
					//echo $espected_request_end_time;
					//exit;
					

					$freeslot=Providerfreetime::where('provider_id',$provider['id'])
					//->where('from_time','<=',date("H:i:s",strtotime($service['from_time'])))
					//->where('to_time','>=',date("H:i:s",strtotime($service['to_time'])))
					->where('from_time','<=',date("H:i:s",strtotime($travel_start_time)))
					->where('to_time','>=',date("H:i:s",strtotime($espected_request_end_time)))
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($espected_request_end_time)))
					->get();
					

					if(date("H:i:s",strtotime($espected_request_start_time))>date("H:i:s",strtotime($service['to_time'])))
					return false;


					$book=Providerbookedtime::with("Service")->where('provider_id',$provider['id'])
					->whereRaw(" ( '".$travel_start_time."' between heading_start_time and to_time  or  '".$espected_request_start_time."' between heading_start_time and to_time or  '".$espected_request_end_time."' between heading_start_time and to_time)")
					->whereHas('Service', function ($query){
						$query->where("service_status_id",4);
						$query->orwhere("service_status_id",5);
						$query->orwhere("service_status_id",9);
						 
					})
					->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))->count();

					if($book>0) return false;
					

					if($freeslot->count()>0) return true;

					
				}else{
				//try other method
				//A>C>B
				
				
				$travel_start_time=$confirmed->to_time;
				

				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$requestto_booked_duration.' minutes', strtotime($travel_start_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($espected_request_start_time)));

				$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;

				$working_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($espected_request_end_time)));

				//if provider goes before first service 

							if($working_start_time<$working_service->to_time){
								
								$freeslot=Providerfreetime::where('provider_id',$provider['id'])
								//->where('from_time','<=',date("H:i:s",strtotime($service['from_time'])))
								//->where('to_time','>=',date("H:i:s",strtotime($service['to_time'])))
								->where('from_time','<=',date("H:i:s",strtotime($travel_start_time)))
								->where('to_time','>=',date("H:i:s",strtotime($espected_request_end_time)))
								->where('time_type','free')
								->whereDate('date',date("Y-m-d",strtotime($espected_request_end_time)))->get();


								
								
								if(date("H:i:s",strtotime($espected_request_start_time))>date("H:i:s",strtotime($service['to_time'])))
								return false;
								

								if($freeslot->count()>0) return true;
							}

							elseif($espected_request_start_time<$service['to_time'])
								{

									


									$book=Providerbookedtime::with("Service")->where('provider_id',$provider['id'])
									->whereRaw(" ( '".$travel_start_time."' between heading_start_time and to_time  or  '".$espected_request_start_time."' between heading_start_time and to_time or  '".$espected_request_end_time."' between heading_start_time and to_time)")
									->whereHas('Service', function ($query){
										$query->where("service_status_id",4);
										$query->orwhere("service_status_id",5);
										$query->orwhere("service_status_id",9);
										 
									})
									->whereDate('book_date',date("Y-m-d",strtotime($service['to_time'])))->first();

									
									if(count($book)>0) {
										$travel_start_time=$book->to_time;

										$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$requestto_booked_duration.' minutes', strtotime($travel_start_time)));
										$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($espected_request_start_time)));



										}

									//echo $travel_start_time;
									///echo $espected_request_start_time;
									//echo $espected_request_end_time;
									//exit;

									$freeslot=Providerfreetime::where('provider_id',$provider['id'])
									
									->where('from_time','<=',date("H:i:s",strtotime($travel_start_time)))
									->where('to_time','>=',date("H:i:s",strtotime($espected_request_end_time)))
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($espected_request_end_time)))->get();
									

									if($freeslot->count()>0) return true;
								
								
								


								}
				
				


				}
				
				

				
				return false;

			}
			elseif($booked && $booked->count()==1){
							
					$booked=$booked->first();
					//echo $booked->service->id;
					$confirmed=Service::with("Booked")->where('provider_id',$provider['id'])->where('id',$booked->service->id)->first();
					
					$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
					$origin='origins='.$confirmed->latitude.','.$confirmed->longitude;
					$destination='&destinations='.$service['latitude'].','.$service['longitude'];
					
					$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
					$data=json_decode(($data),true);
					if(!isset($data['rows'][0]['elements'][0]['duration']))
					return false;

					$confirmed_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10
					//echo $confirmed_torequest_duration."--".$arrival_duration->value;exit;
					//validate if duration is within max travel time
					if($confirmed_torequest_duration>$arrival_duration->value)
						return false;

					 $new_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed->booked->to_time)));
					
					//if current requrest can go before
					if($service['to_time'] <= $confirmed->booked->from_time){
						
						//get duration between provider and request location
						$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
						if($provider['base_latitude'] && $provider['base_longitude'] && $provider['location_preference']=='fixed')
						$origin='origins='.$provider['base_latitude'].','.$provider['base_longitude'];
						else
						$origin='origins='.$provider['current_latitude'].','.$provider['current_longitude'];
						
						$destination='&destinations='.$service['latitude'].','.$service['longitude'];
						
						$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
						$data=json_decode(($data),true);
						if(!isset($data['rows'][0]['elements'][0]['duration']))
						return false;
						$current_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10
					
						$new_start_time=date("Y-m-d H:i:s", strtotime('-'.($current_torequest_duration+$buffer_time).' minutes', strtotime($service['from_time'])));
						$new_end_time=date("Y-m-d H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($new_start_time)));


						$bookcount=Providerbookedtime::whereRaw("'".$new_start_time."' between from_time and to_time and provider_id='".$provider['id']."'
						OR '".$new_end_time."' between from_time and to_time and provider_id='".$provider['id']."'

						")->count();
						
						if($bookcount>0) return false;

						
						$expected_confirmed_start_time =date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($new_end_time)));

						//try A>B>C
						if($expected_confirmed_start_time<=$confirmed->booked->from_time)
							{
								
								$freeslot=Providerfreetime::where('provider_id',$provider['id'])
						
								->where('from_time','<=',date("H:i:s",strtotime($new_start_time)))
								->where('to_time','>=',date("H:i:s",strtotime($new_end_time)))
								->where('time_type','free')
								->whereDate('date',date("Y-m-d",strtotime($new_end_time)))->get();
									
								if($new_start_time>$service['to_time'])
								return false;


								
								
								if($freeslot->count()>0) return true;
								

							}
							
					
								
					}elseif($service['from_time'] >= $confirmed->booked->to_time || $new_start_time<$service['to_time']){
					//try A>C>B
					//echo $service['from_time']."-".$confirmed->booked->to_time;exit;
					
									
						$head_start=$confirmed->booked->to_time;
						$new_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed->booked->to_time)));

						$new_end_time=date("Y-m-d H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($new_start_time)));

						
						
						if($new_start_time < $service['to_time'])
						{
							
								
								$freeslot=Providerfreetime::where('provider_id',$provider['id'])
						
								->whereRaw("'".date("H:i:s", strtotime($new_start_time))."' between from_time and to_time and '".date("H:i:s", strtotime($new_end_time))."' between from_time and to_time ")
								
								->where('time_type','free')
								->whereDate('date',date("Y-m-d",strtotime($new_end_time)))
								
								->orwhereRaw("'".date("H:i:s", strtotime($new_end_time))."' between from_time and to_time

								and '".date("H:i:s", strtotime($head_start))."' between from_time and to_time and '".date("H:i:s", strtotime($new_start_time))."' between from_time and to_time

								")
								->where('provider_id',$provider['id'])
								->where('time_type','free')
								->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))
								->get();
								
								
								if(date("H:i:s", strtotime($new_start_time))>date("H:i:s",strtotime($service['to_time'])))
								{
									
									return false;

								}

								
							
								

								if($freeslot->count()>0)
								{ return true;
								 }else
								 {
									$unavailableslot=Providerfreetime::where('provider_id',$provider['id'])
									->whereRaw(" ( ('".date("H:i:s", strtotime($new_end_time))."' between from_time and to_time)

									or ('".date("H:i:s", strtotime($head_start))."' between from_time and to_time) or ('".date("H:i:s", strtotime($new_start_time))."' between from_time and to_time ))
									and time_type<>'free' and date='".date("Y-m-d",strtotime($service['to_time']))."'
									")->first();
									$head_start=date("Y-m-d H:i:s", strtotime('+1 minutes', strtotime($unavailableslot->date." ".$unavailableslot->to_time)));
									$new_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($head_start)));
									$new_end_time=date("Y-m-d H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($new_start_time)));



									$freeslot=Providerfreetime::where('provider_id',$provider['id'])
									->whereRaw(" ('".date("H:i:s",strtotime($head_start))."' between from_time and to_time) and
									('".date("H:i:s",strtotime($new_start_time))."' between from_time and to_time)
									and ('".date("H:i:s",strtotime($new_end_time))."' between from_time and to_time)
									")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($new_start_time)))
									->count();
									$book=Providerbookedtime::whereRaw(" ('".($new_start_time)."' between from_time and to_time or  '".($new_end_time)."' between from_time and to_time) ")
									->where('provider_id',$provider['id'])
									->count();

									if($book<=0 && $freeslot>0){

										return true;

									}


									

								 }

						}



					}
					

					return false;
				}
			else{
		
				//if no booking after current request then check freeslot
				


				//get duration between provider and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				if($provider['base_latitude'] && $provider['base_longitude'] && $provider['location_preference']=='fixed')
				$origin='origins='.$provider['base_latitude'].','.$provider['base_longitude'];
				else
				$origin='origins='.$provider['current_latitude'].','.$provider['current_longitude'];
				
				$destination='&destinations='.$service['latitude'].','.$service['longitude'];
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration'])) return false;
				$current_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;  //10
						
				$espected_current_work_start_time=date("Y-m-d H:i:s",strtotime("+ ".$current_torequest_duration." Minutes",time()));
				
				if($espected_current_work_start_time>date("Y-m-d H:i:s",strtotime($service['to_time'])))
				return false;
				
					//validate if duration is within max travel time
					if($current_torequest_duration>$arrival_duration->value)
						return false;

				$head_start=date("H:i:s",strtotime("- ".$current_torequest_duration." Minutes",strtotime($service['from_time'])));
				
				if($head_start<date("H:i:s"))
				{
					$espected_request_start_time=date("H:i:s",strtotime("+ ".$current_torequest_duration." Minutes",time()));
					$head_start=date("H:i:s");
				}
				else
				$espected_request_start_time=date("H:i:s",strtotime($service['from_time']));

				
								
				$espected_request_end_time=date("H:i:s", strtotime('+'.$service['job_duration_set_by_provider'].' minutes', strtotime($espected_request_start_time)));
				
				
				
				
				/*$freeslot=Providerfreetime::where('provider_id',$provider['id'])
				->where('from_time','<=',date("H:i:s",strtotime($service['from_time'])))
				->where('to_time','>=',date("H:i:s",strtotime($service['to_time'])))->where('time_type','free')
				->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))
				->orwhereRaw("'".$espected_request_start_time."' between from_time and to_time and '".$espected_request_end_time."' between from_time and to_time")
					->where('provider_id',$provider['id'])
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))

				->orwhereRaw("'".$espected_request_end_time."' between from_time and to_time and '".$head_start."' between from_time and to_time and '".$espected_request_start_time."' between from_time and to_time")
					->where('provider_id',$provider['id'])
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))

				->get();*/

				//echo $head_start;
				//echo $espected_request_start_time;
				//echo $espected_request_end_time;
				$freeslot=Providerfreetime::where('provider_id',$provider['id'])->whereDate('date',date("Y-m-d",strtotime($service['to_time'])))
				->where('time_type','free')->whereRaw(" (('".$head_start."' between from_time and to_time and '".$espected_request_start_time."' between from_time and to_time and '".$espected_request_end_time."' between from_time and to_time)
					)")->get();
				

				//echo $freeslot->count();exit;
								
				
				if($freeslot->count()>0) return true;



			}

		}

			
		return false;

	}

	//check if provider not exceed max_due_amount
	private function CheckIfPaid($provider){

		$limit=$provider['profile']['max_due_amount'];
		
		$count=CompanyProvider::where("provider_id",$provider['id'])->count();
		if($count>0){
			$account=ProviderCompanyAccount::where("provider_id",$provider['id'])->first();
			if(!$account)
				return false;
				
			$account->current_credit_balance;
			if($account->current_credit_balance>=$limit)
				return false;
			
		}else{

			$balance=$provider['profile']['current_credit_balance'];
			if($balance>=$limit)
			return false;
			
		}

		return true;

	}


	
//get distance between provider current location and service
	private function distanceBetweenServiceProvider($service,$provider,$range=1){
			$lat1=$provider_lat=$provider['current_latitude'];
			
			$lon1=$provider_lng=$provider['current_longitude'];
			
			$lat2=$service_lat=$service['latitude'];
			
			$lon2=$service_lng=$service['longitude'];
			
			$unit="K";
			
			if($lat1=='' || $lon1=='' || $lat2=='' || $lon2==''){

			return false;
			}

			  $theta = $lon1 - $lon2;
			  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			  $dist = acos($dist);
			  $dist = rad2deg($dist);
			  $miles = $dist * 60 * 1.1515;
			  $unit = strtoupper($unit);
			
			
			
			  if ($unit == "K") {
				  $distance= ($miles * 1.609344);
			  } else if ($unit == "N") {
				  $distance= ($miles * 0.8684);
			  } else {
				  $distance= $miles;
			  }
			 

			  if($distance<=$range){
					return true;
				}else{
					return false;
				}


	}




	//this function will search those providers to set their prefered location
	private function validateProviderPreference($service,$provider){
		
		$polygon=Providerpreferencelocation::with('Polygon')->where('provider_id',$provider->id)->get();
		


		$servicepoint=array("latitude"=>$service['latitude'],"longitude"=>$service['longitude']);

		foreach($polygon as $points){
		$coordinates=$this->pointInPolygon($servicepoint,$points->polygon);
		
			if($coordinates)
				return true;
			
			
		}
		return false;
	
	}


    //find if service is within prefered area in polygon
	private function pointInPolygon($point, $polygon, $pointOnVertex = true) {
		
        $this->pointOnVertex = $pointOnVertex;
 
        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array(); 
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex); 
        }
 
        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return true;
        }
 
        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);
 
        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1]; 
            $vertex2 = $vertices[$i];
            
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) {
				// Check if point is on an horizontal polygon boundary
                return true;
            }
            
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y'])
            { 

                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 

                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return true;
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        // If the number of edges we passed through is odd, then it's in the polygon. 
        if ($intersections % 2 != 0) {
            return true;
        } else {
            return false;
        }
    }
 
	private  function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
 
    }
	
	private function pointStringToCoordinates($pointString) 
    {
            //$coordinates = explode(",", $pointString);
            return array("x" => trim($pointString['latitude']), "y" => trim($pointString['longitude']));
    }

	//validate provider login
    protected function ProviderTraitLogin($request)
	{
			$phone = $request->input('phone');
		$password = $request->input('password');


		$user_arr = DB::table('providers')->where('phone', '=', $phone)->first();
//print_r($user_arr);exit;
		if($user_arr){
				$user_arr = (array)$user_arr;
				if(Hash::check($password, $user_arr['password']))
					{
						
						if($user_arr['is_active'] == 1 && $user_arr['sms_verified'] == 1 && $user_arr['approve_status'] == 1 && $user_arr['banned_duration'] <=0) 
						{
							
							$new_user_arr['data']['phone'] = $user_arr['phone'];
							$new_user_arr['data']['status'] = $this->authenticate($phone,$password);
						
							
							############# create token -start ##############
							
							$new_token = $this->create_token_provider($request,$user_arr['id']); 
							$new_user_arr['data']['token'] = $new_token;
							$new_user_arr['success'] = 0;



							$provider=Provider::with("Profile")->where('id',$user_arr['id'])->first();
							$new_user_arr['update_settings'] = (string)$provider->profile->update_settings;
							$free=Providerfreetime::where('provider_id',$provider->id)->count();
							$new_user_arr['schedule_count'] =(string) $free;

							$company=CompanyProvider::where("provider_id",$provider->id)->count();
							if($company>0){
								$account=ProviderCompanyAccount::where("provider_id",$provider->id)->first();
								$totalamount=$account->current_credit_balance;
							}else{
							
								$totalamount=$provider->profile->current_credit_balance;
					

							}
							//Send notification to provider if balance to sattle more than 80%
							$totalamountpercent=($totalamount/$provider->profile->max_due_amount)*100;
							$new_user_arr['current_amount_percent'] =(string) $totalamountpercent;
							
							if($totalamountpercent>=100){
								$msg="You have reached 100% of your balance, please pay now to avoid account deactivation";
								if($provider->device_type=='ios' && $provider->device_id!=''){
									
										$arr = array('type'=>'balancelimit100',"provider_name"=>$provider->name);
										$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
									
									}elseif($provider->device_id!='' ){

										$arr = array('type'=>'balancelimit100',"provider_name"=>$provider->name);
										$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);
									}
							}
							elseif($totalamountpercent>=80){
								$msg="You have reached 80% of your balance, please pay now to avoid account deactivation";
								if($provider->device_type=='ios' && $provider->device_id!=''){
									
										$arr = array('type'=>'balancelimit80',"provider_name"=>$provider->name);
										$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
									
									}elseif($provider->device_id!='' ){

										$arr = array('type'=>'balancelimit80',"provider_name"=>$provider->name);
										$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);
									}
							}

		
							$new_user_arr['msg'] = 'Logged in successfully.';
							############# create token -end ###############
							
						}
						elseif($user_arr['banned_duration'] >0){
								$new_user_arr['success'] = 2;
								$new_user_arr['msg'] = 'Your account is banned temporarily.';

						}
						elseif($user_arr['is_active'] == 1 && $user_arr['sms_verified'] != 1){
								$new_user_arr['success'] = 2;
								$new_user_arr['msg'] = 'Your account sms not verified.';
						}
						elseif($user_arr['is_active'] != 1 && $user_arr['sms_verified'] != 1){
								$new_user_arr['success'] = 2;
								$new_user_arr['msg'] = 'Your account not verified.';
						}
						elseif($user_arr['is_active'] == 1 && $user_arr['sms_verified'] == 1 && $user_arr['approve_status'] != 1 ){
								$new_user_arr['success'] = 3;
								$new_user_arr['msg'] = 'Your account is not approved.';
						}
						else
						{
							$new_user_arr['success'] = 1;
							$new_user_arr['msg'] = 'Your account is inactive.';
						}
					}
					else
					{
						$new_user_arr['success'] = 1;
						$new_user_arr['msg'] = 'Your password is wrong.';
					}

				

			}else
			{
				$new_user_arr['success'] = 1;
				$new_user_arr['msg'] = 'Your phone number or password is wrong.';
			}

			if($new_user_arr['success']==2){


						$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

						$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

						$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
						$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$request->input('phone'))->where('send_date','>=',$future_time)->count();

						$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
						$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$request->input('phone'))->where('send_date','>=',$future_time)->count();

						$new_user_arr["per_min_sms"]=$per_min_sms;
						$new_user_arr["per_min_sms_count"]=$one_min_log;
						$new_user_arr["per_hour_sms"]=$per_hour_sms;
						$new_user_arr["per_hour_sms_count"]=$per_hour_log;





					}
					
			return $new_user_arr;
			die;

	}

//get distance and duration from any latitude and longitude
	private function httpGet($url)
	{
		$ch = curl_init();  
		//echo $url=urlencode($url);
		curl_setopt($ch,CURLOPT_URL,($url));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	//  curl_setopt($ch,CURLOPT_HEADER, false); 
	 
		$output=curl_exec($ch);
		$info = curl_getinfo($ch);
		
	 
		curl_close($ch);
		return $output;
	}
 
//send rating to customer after completion of job
	protected function createRatingtoCustomer($request)
	{
			if($request->input('service_id')=='' || $request->input('phone')=='') return false;
			$phone = $request->input('phone');
			$provider=Provider::where('phone',$phone)->first();
			$service=Service::where("id",$request->input('service_id'))->first();
			$ratecount=CustomerRatings::where('provider_id',$provider->id)->where('service_id',$request->input('service_id'))->count();
			if($ratecount<=0 && $service->service_status_id=='6')
			{
				$rating=new CustomerRatings();
				$rating->customer_id=$service->customer_id;
				$rating->provider_id=$provider->id;
				$rating->service_id=$request->input('service_id');
				$rating->rate_value=$request->input('rating');
				//$rating->professional=$request->input('professional');
				//$rating->friendly=$request->input('friendly');
				//$rating->communication=$request->input('communication');
				 
				$rating->comment=$request->input('comment');
				$rating->date=date('Y-m-d H:i:s');
				$rating->save();
				return $rating;
			}else{
				return false;
			}
			
			
			
			

	}

	protected function getRatingtoCustomer($request)
	{
		if($request->input('service_id')=='' || $request->input('phone')=='') return false;
			$phone = $request->input('phone');
			$provider=Provider::where('phone',$phone)->first();
			$service=Service::where("id",$request->input('service_id'))->first();
			$ratecount=CustomerRatings::where('provider_id',$provider->id)->where('service_id',$request->input('service_id'))->count();
			if($ratecount>0 )
			{
				$rating=CustomerRatings::where('provider_id',$provider->id)->where('service_id',$request->input('service_id'))->first();
				
				return $rating;
			}else{
				return false;
			}
			
			
			
			

	}

	

//validate and accept customer appointment notification sent to provider so slot is booked here
	protected function acceptBooking($request){

		if($request->input('service_id')=='' || $request->input('phone')=='') return false;
		
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		if($request->input('service_id')=='') return false;
		$service=Service::where("id",$request->input('service_id'))->where('service_status_id','=',1)->first();
        
		if($service){

			
			//validate booking slot and update service
			$status=$this->bookProviderTable($service,$request);
              //print_r($status);exit;

				if($status){

					$pnotice=new Providernotification();
					$pnotice->provider_id=$provider->id;
					$pnotice->service_id=$service->id;
					$pnotice->date=date('Y-m-d H:i:s');
					$pnotice->details='Job Accepted';
					$pnotice->notification_latitude=$provider->current_latitude;
					$pnotice->notification_longitude=$provider->current_longitude;
					$pnotice->invite_status='accepted';
					$pnotice->service_notice_status='confirmed';
					$pnotice->price=$service->expected_cost;
					$pnotice->save();

					
					
					$pnotice=new Customernotification();
					$pnotice->customer_id=$service->customer_id;
					$pnotice->service_id=$service->id;
					$pnotice->date=date('Y-m-d H:i:s');
					$pnotice->details='Job Accepted';
					$pnotice->notification_latitude=$provider->current_latitude;
					$pnotice->notification_longitude=$provider->current_longitude;
					
					$pnotice->service_notice_status='confirmed';
					$pnotice->price=$service->expected_cost;
					$pnotice->save();


					$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);

					$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
					if($customer->device_type=='ios' && $customer->device_id!=''){
		
					$arr = array('type'=>'jobaccepted','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
					$status= $this->sendCustomerAPNS($provider->name. " has accepted your request, you will be notified when he starts heading to you.",$arr,$customer->device_id);
					
					}elseif($customer->device_id!='' ){

					$arr = array('type'=>'jobaccepted','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
					$status= $this->sendCustomerFCM($provider->name. " has accepted your request, you will be notified when he starts heading to you.",$arr,$customer->device_id);
		
					}

				return true;
				}else{
					return false;
				}

			}
	return false;
		
	}

	//validate if provider has enough free slot for accepting an appointment
	protected function bookProviderTable($service,$request)
	{
		$phone = $request->input('phone');
		
		$provider=Provider::with("Profile")->where('phone',$phone)->first();
		$mode=($provider->profile->transport=='walk')?'walking':'driving';
		
		if($provider->id!=$service->provider_id)
		return false;
		
		$freeslot=Providerfreetime::where('provider_id',$provider->id)
					->where('from_time','<=',date("H:i:s",strtotime($service->from_time)))
					->where('to_time','>=',date("H:i:s",strtotime($service->to_time)))
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					
					->orWhere('from_time','<=',date("H:i:s",strtotime($service->to_time)))
					->where('to_time','>=',date("H:i:s",strtotime($service->to_time)))
					->where('provider_id',$provider->id)
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))


					->orWhere('from_time','>=',date("H:i:s",strtotime($service->from_time)))
					->where('to_time','<=',date("H:i:s",strtotime($service->to_time)))
					->where('provider_id',$provider->id)
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))

					->orWhere('provider_id',$provider->id)
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->whereRaw("'".date("H:i:s",strtotime($service->from_time))."' between from_time and to_time")
					->first();
       
			
		if($freeslot)
		{
			
		
			/*$booked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
			->where('from_time','<=', $service->from_time)
			->where('to_time','>=', $service->from_time)
			->orWhere('from_time','<=', $service->to_time)
			->where('to_time','>=', $service->to_time)
			//->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
			->where(DB::raw("DATE(book_date)"),"=",date("Y-m-d",strtotime($service->to_time)))
			->orderby('from_time','asc')->first();*/

			$booked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
			->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
			->where('from_time','<=', $service->to_time)
			->where('from_time','>=', $service->from_time)
			->whereHas('Service', function ($query) {
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
						
					})
			->orWhere('to_time','<=', $service->to_time)
			->where('provider_id',$provider->id)
			->where('to_time','>=', $service->from_time)
			->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
			->whereHas('Service', function ($query) {
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
						
					})
			->orderby('from_time','asc');
			

			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();

			$buffer_time=(int)$buffer_time->value;

				//get duration between current and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				if($provider->base_latitude && $provider->base_longitude && $provider->location_preference=='fixed')
				$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
				else
				$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
				$destination='&destinations='.$service->latitude.','.$service->longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				//exit;
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				
				$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;  
				$estimated_traveltime_heading_current_location=$estimated_traveltime_heading;



			//set booking order
			$order=1;
			$canbebooked=false;
			if($booked && $booked->count()>1)
			{
				
				
				
				$booked=$booked->first();
				
				//if there is booking after current request calculate duration and distance
				
								
				$working_service=Service::with("Booked")->where('provider_id',$provider->id)->where('id',$booked->service->id)->first();

				//$working_service_totime=date("Y-m-d H:i:s", strtotime('+'.$working_service->job_duration_set_by_provider.' minutes', strtotime($working_service->from_time)));
				$working_service_totime=$working_service->booked->to_time;
				$travel_start_time=$working_service_totime;

				$confirmed=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
				->where('from_time','<=', $service->from_time)
				->where('to_time','>=', $service->from_time)
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
				->whereHas('Service', function ($query) {
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
						
					})
				->orWhere('from_time','<=', $service->to_time)
				->where('to_time','>=', $service->to_time)
				->where('provider_id',$provider->id)
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
				->whereHas('Service', function ($query) {
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
						
					})
				->orWhere('provider_id',$provider->id)
				->whereRaw(" ( from_time >='".$service->from_time."'  and to_time <= '".$service->to_time."')")
				->whereHas('Service', function ($query) {
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
						
					})
				->orderby('from_time','desc')->first();

				
				
				if(!$confirmed) return false;
				
				$confirmed_end=$confirmed->to_time;
				
				$duration_reach_at_confirmed_place=(strtotime($confirmed_end)-strtotime($travel_start_time))/60; //65

				

				//get duration between working and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$working_service->latitude.','.$working_service->longitude;
				$destination='&destinations='.$service->latitude.','.$service->longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;

				$working_torequest_duration=$buffer_time+floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10
				
				
				//get duration between request and booked confirmed location
				$origin='origins='.$service->latitude.','.$service->longitude;
				$destination='&destinations='.$confirmed->service->latitude.','.$confirmed->service->longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				$requestto_booked_duration=$buffer_time+floor($data['rows'][0]['elements'][0]['duration']['value']/60); //5



				

				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($travel_start_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
				$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;
			

				$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();

				$buffer_time=(int)$buffer_time->value;
				//$working_torequest_duration.'=';
				//$requestto_booked_duration.'=';
				//$request_service_duration.'=';

				$total_expected_duration=(int)$working_torequest_duration+(int)$requestto_booked_duration+(int)$request_service_duration;



				if($total_expected_duration<=$duration_reach_at_confirmed_place)
				{
					

					//book with ascending order
					if($booked->sort_order>0 && $confirmed->sort_order>0 && $booked->sort_order>$confirmed->sort_order){
						$gap=$booked->sort_order-$confirmed->sort_order;
						$booked->sort_order=$booked->sort_order-$gap;
						$confirmed->sort_order=$confirmed->sort_order+$gap;
					}
					elseif($booked->sort_order>0 && $confirmed->sort_order>0 && $confirmed->sort_order>$booked->sort_order){
						
						$booked->sort_order=$booked->sort_order;
						$confirmed->sort_order=$confirmed->sort_order;
					}
					else{
					$booked->sort_order='1';
					
					$confirmed->sort_order='3';
					}
					$canbebooked=true;

					$estimated_traveltime_heading=(int)$working_torequest_duration;

					//not required because first booking end time is travel start time
					//$travel_start_time=date("Y-m-d H:i:s", strtotime($confirmed->to_time));
					

					$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($travel_start_time)));
					$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));

					$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;
					
					

					
				}else{
				//try other method
				//A>C>B
				
					
					//$travel_start_time=date("Y-m-d H:i:s", strtotime('+'.$confirmed->service->job_duration_set_by_provider.' minutes', strtotime($confirmed->from_time)));
					$travel_start_time=date("Y-m-d H:i:s", strtotime($confirmed->to_time));
					

					$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$requestto_booked_duration.' minutes', strtotime($travel_start_time)));
					$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
					
					$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;

					$working_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($espected_request_end_time)));
					
					if($working_start_time<=$working_service->to_time)
					{
						
						//book with descending order
						
						
						if($booked->sort_order>0 && $confirmed->sort_order>0 && $booked->sort_order>$confirmed->sort_order){
								$booked->sort_order=$booked->sort_order;
								$confirmed->sort_order=$confirmed->sort_order;
							}
							elseif($booked->sort_order>0 && $confirmed->sort_order>0 && $confirmed->sort_order>$booked->sort_order){
								
								$gap=$confirmed->sort_order-$booked->sort_order;
								$booked->sort_order=$booked->sort_order+$gap;
								$confirmed->sort_order=$confirmed->sort_order-$gap;
							}
							else{
							$booked->sort_order='3';
							
							$confirmed->sort_order='1';
							}
					
						$canbebooked=true;

						$estimated_traveltime_heading=(int)$requestto_booked_duration;
				
					}elseif($espected_request_start_time<=$service->to_time){

						$booked->sort_order='1';
							
						$confirmed->sort_order='3';

						$canbebooked=true;

						$estimated_traveltime_heading=(int)$requestto_booked_duration;

					}
				
				}
			
				if($booked->to_time>$working_service->booked->to_time)
				{
				$maxtime=$booked->to_time;
				}else{
					$maxtime=$working_service->booked->to_time;
				}
			}
			elseif($booked && $booked->count()==1){
			
					
				
					
					$booked=$booked->first();
					
					$confirmed_service=Service::with("Booked")->where('provider_id',$provider->id)->where('id',$booked->service->id)->first();

					$confirmed=Providerbookedtime::where("id",$confirmed_service->booking_slot_id)->first();
					
					//get duration between working and request location
					$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
					$origin='origins='.$confirmed_service->latitude.','.$confirmed_service->longitude;
					$destination='&destinations='.$service->latitude.','.$service->longitude;
					
					$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
					$data=json_decode(($data),true);
					if(!isset($data['rows'][0]['elements'][0]['duration']))
					return false;

					$confirmed_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10
					$new_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed_service->booked->to_time)));

					//if provider goes at request service first
					if($service->to_time <= $confirmed_service->booked->from_time){
						
						//get duration between provider and request location
						$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
						if($provider->base_latitude && $provider->base_longitude && $provider->location_preference=='fixed')
						$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
						else
						$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
						$destination='&destinations='.$service->latitude.','.$service->longitude;
						
						$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
						$data=json_decode(($data),true);
						if(!isset($data['rows'][0]['elements'][0]['duration']))
						return false;

						$current_torequest_duration=$buffer_time+floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10

						//heading will start before arrival time
						

					
						$espected_request_head_start_time=date("Y-m-d H:i:s", strtotime('-'.($current_torequest_duration+$buffer_time).' minutes', strtotime($service->from_time)));


						$freeslot=Providerfreetime::where('provider_id',$provider->id)
						->whereRaw(" (( '".date("H:i:s",strtotime($espected_request_head_start_time))."' between from_time and to_time ) or ('".date("H:i:s", strtotime($service->from_time))."' between from_time and to_time ))")
						->where('time_type','free')
						->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
						->first();
						
						$prebook=Providerbookedtime::with("Service")->where('provider_id',$provider->id)
						->whereRaw("'".$espected_request_head_start_time."' between from_time and to_time")
						->whereHas('Service', function ($query){
							$query->where("service_status_id",4);
							$query->orwhere("service_status_id",5);
							$query->orwhere("service_status_id",9);
							 
						})
						->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
						->count();
								
								if(count($freeslot)<=0 || $prebook>0)
								{
									$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.($current_torequest_duration+$buffer_time).' minutes', strtotime($service->from_time)));
			
								}else{

									if($freeslot->from_time>=$espected_request_head_start_time)
									$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$current_torequest_duration+$buffer_time.' minutes', strtotime(date("Y-m-d",strtotime($service->to_time))." ".$freeslot->from_time)));
									else
									$espected_request_start_time=$service->from_time;
								
									

								}
						
						$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));


						$expected_confirmed_start_time =date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($espected_request_end_time)));

						

						//try A>B>C
						if($expected_confirmed_start_time<=$confirmed_service->to_time)
							{
								//$booked->sort_order='3';

								
								$estimated_traveltime_heading=$current_torequest_duration;
						
								$confirmed->sort_order='2';
								//$booked->save();
								$canbebooked=true;
								$order=1;
								

							}
						
							



								
					}
					//if provider goes at request service later
					elseif($service->from_time >= $confirmed_service->booked->to_time || $new_start_time<=$service['to_time']){
					//try A>C>B
					
						$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed_service->booked->to_time)));
						$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));

						
						
						if($espected_request_start_time <= $service->to_time)
							{
								//$booked->sort_order='1';
								$estimated_traveltime_heading=$confirmed_torequest_duration+$buffer_time;
							
								$confirmed->sort_order='2';
								//$booked->save();
								$canbebooked=true;
								$order=1;
							}
					




					}
					elseif($service->to_time >= $confirmed_service->booked->from_time){

						$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
						if($provider->base_latitude && $provider->base_longitude && $provider->location_preference=='fixed')
						$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
						else
						$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
						$destination='&destinations='.$service->latitude.','.$service->longitude;
						
						$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
						$data=json_decode(($data),true);
						if(!isset($data['rows'][0]['elements'][0]['duration']))
						return false;

						$current_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10

						//heading will start before arrival time
						

						$espected_request_end_time=date("Y-m-d H:i:s", strtotime('-'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed_service->booked->from_time)));

						$espected_request_start_time=date("Y-m-d H:i:s", strtotime('-'.($service->job_duration_set_by_provider).' minutes', strtotime($espected_request_end_time)));
						
						$espected_request_head_start_time=date("Y-m-d H:i:s", strtotime('-'.($current_torequest_duration+$buffer_time).' minutes', strtotime($espected_request_start_time)));

						


						$freeslot=Providerfreetime::where('provider_id',$provider->id)
						->whereRaw(" '".date("H:i:s",strtotime($espected_request_head_start_time))."' between from_time and to_time ")
						->where('time_type','free')
						->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
						->count();
						
						$prebook=Providerbookedtime::with("Service")->where('provider_id',$provider->id)
						->whereRaw("'".$espected_request_head_start_time."' between from_time and to_time")
						->whereHas('Service', function ($query){
							$query->where("service_status_id",4);
							$query->orwhere("service_status_id",5);
							$query->orwhere("service_status_id",9);
							 
						})
						->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
						->count();


						if($espected_request_end_time <= $confirmed_service->booked->from_time && $freeslot>0 && $prebook<=0)
							{
								
								$estimated_traveltime_heading=$current_torequest_duration+$buffer_time;
								$confirmed->sort_order='2';
								
								$canbebooked=true;
								$order=1;
							}
						

					}
					else{
						
					 $espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed_service->booked->to_time)));
					  $espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));



					}

					
			
					if($booked->to_time>=$espected_request_end_time)
						{
						$maxtime=$booked->to_time;
						}else{
							$maxtime=$espected_request_end_time;
						}
						
				


			}
			else{
				
				
				$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
					->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
					->where('to_time','<=', $service->from_time)
					->whereHas('Service', function ($query) {
						$query->where('service_status_id',4);
						$query->orWhere('service_status_id',5);
						$query->orWhere('service_status_id',9);
						
					})
					->orderby('from_time','desc')->first();

					
			
			//if no booking then estimate job start time and end time
					$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
					if($provider->base_latitude && $provider->base_longitude )
					$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
					else
					$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
					$destination='&destinations='.$service->latitude.','.$service->longitude;
					
					$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
					//exit;
					$data=json_decode(($data),true);
					if(!isset($data['rows'][0]['elements'][0]['duration']))
					return false;
					
					
					$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
					//$estimated_traveltime_heading_current_location=$estimated_traveltime_heading;  
					$heading_start=date("H:i:s", strtotime('-'.$estimated_traveltime_heading.' minutes', strtotime($service->from_time)));
					$work_start=date("H:i:s", strtotime($service->from_time));
					$work_end=date("H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($work_start)));;

					$freeslot=Providerfreetime::where('provider_id',$provider->id)
					->whereRaw(" (('".$heading_start."' between from_time and to_time and '".$work_start."' between from_time and to_time and '".$work_end."' between from_time and to_time)
					)")
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->first();
					
					//echo count($freeslot);exit;
					
					//check if heading can start before arrival
					if(count($freeslot)>0)
					{
						if($freeslot->from_time>$heading_start)
						{

							$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime(date("Y-m-d",strtotime($service->to_time))." ".$freeslot->from_time)));
							

						}
						
						elseif($prevbooked && $prevbooked->to_time<$service->from_time ){

							$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($prevbooked->to_time)));

							if($espected_request_start_time<=$service->from_time)
							{
							
								$espected_request_start_time=$service->from_time;
							}
							
						}
						else
						{
							
							$espected_request_start_time=$service->from_time;
							
						}
					}else
					{
						$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($service->from_time)));
						
					}

					
					
					$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
					$canbebooked=true;
				


			}
			
			
			//check if end time is not in free time
			$free=Providerfreetime::where('provider_id',$provider->id)
					->whereRaw(" (('".Date("H:i:s", strtotime($espected_request_end_time))."' between from_time and to_time and '".Date("H:i:s", strtotime($espected_request_start_time))."' between from_time and to_time ) )")
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->count();
			
			if($free<=0)
			{
				
				$free=Providerfreetime::where('provider_id',$provider->id)
					->whereRaw("from_time > '".Date("H:i:s", strtotime($espected_request_start_time))."'")
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))->first();
					
				$heading_start=$free->from_time;
				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($free->date." ".$free->from_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));


				$free=Providerfreetime::where('provider_id',$provider->id)
					->whereRaw(" ('".Date("H:i:s", strtotime($espected_request_end_time))."' between from_time and to_time and '".Date("H:i:s", strtotime($espected_request_start_time))."' between from_time and to_time and '".Date("H:i:s", strtotime($heading_start))."' between from_time and to_time )")
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->count();
				if($free>0)
				{
				$canbebooked=true;
				
				}
				else
				{
				$canbebooked=false;
				
				}
				 

			 }
			//if provider cannot start within arrival range
			if($espected_request_start_time>$service->to_time)	$canbebooked=false;
			
			

			//echo $estimated_traveltime_heading."=";
			//echo $estimated_traveltime_heading_current_location."=";
			//echo $espected_request_start_time."=";
			//echo $espected_request_end_time."=";
			//exit;
			$book=new Providerbookedtime;
			$book->book_date=$espected_request_start_time;
			$book->provider_id=$provider->id;
			$book->booked_customer_id=$service->customer_id;
			$book->heading_start_time=date("Y-m-d H:i:s", strtotime('-'.$estimated_traveltime_heading.' minutes', strtotime($espected_request_start_time)));
			$book->from_time=$espected_request_start_time;
			
			$book->to_time=$espected_request_end_time;
			$book->total_duration=round(abs(strtotime($espected_request_end_time) - strtotime($espected_request_start_time)) / 60,2);
			
			//$book->booking_latitude=$service->latitude;
			//$book->booking_longitude=$service->longitude;
			$book->distance_duration=0;
			
			
			if($booked->count()>0 && $canbebooked && $booked->id!=$confirmed->id)
			{
				
				if($booked->sort_order>0 && $confirmed->sort_order>0 && $booked->sort_order>$confirmed->sort_order){
					
						$gap=$booked->sort_order-$confirmed->sort_order;
						$order=$booked->sort_order-$gap;
						$booked->sort_order=$booked->sort_order-1;
						$confirmed->sort_order=$confirmed->sort_order+1;
						$maxorder=$confirmed->sort_order+1;

						
					}
					elseif($booked->sort_order>0 && $confirmed->sort_order>0 && $confirmed->sort_order>$booked->sort_order){
					
						$gap=$confirmed->sort_order-$booked->sort_order;
						$order=$confirmed->sort_order-$gap;
						$confirmed->sort_order=$confirmed->sort_order+1;
						$booked->sort_order=$booked->sort_order-1;
						$maxorder=$booked->sort_order+1;
					}
					else{
					$booked->sort_order='3';
					$order=2;
					$confirmed->sort_order='1';
					$maxorder=4;
					}
				
				
				 $book->sort_order=$order;

				$futurebook=Providerbookedtime::where('provider_id',$provider->id)
				
				->where('to_time','>', $maxtime)
				//->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
				->where(DB::raw("DATE(book_date)"),"=",date("Y-m-d",strtotime($service->to_time)))
				->orderby('from_time','asc')->get();
					$i=$maxorder;
					foreach($futurebook as $fbook)
					{
						$updatefbook=Providerbookedtime::where('id',$fbook->id)->first();
						$updatefbook->sort_order=$i;
						$updatefbook->save();
					$i++;
					}
					
				$book->save();
				$booked->save();
				$confirmed->save();

				
				}
			elseif($canbebooked){
				$book->sort_order=$order;
				$book->save();
				if(isset($confirmed))
				$confirmed->save();
			}
			
			

			
			if($book->id)
			{

				

		
				$service->booking_slot_id=$book->id;
				$service->service_status_id=4;
				$service->provider_id=$provider->id;
				$service->estimated_traveltime_heading=$estimated_traveltime_heading;
				$service->estimated_traveltime_currentlocation=$estimated_traveltime_heading_current_location;
				
				
				$service->save();


				
				$appointment=new Appointments;
				$appointment->service_id=$service->id;
				$appointment->provider_id=$provider->id;
				$appointment->customer_id=$service->customer_id;
				$appointment->booking_slot_id=$service->booking_slot_id;
				$appointment->date=$book->book_date;
				$appointment->service_start_time=$book->from_time;
				$appointment->service_end_time=$book->to_time;
				$appointment->service_status_id=4;
				$appointment->save();
				
				return true;
			}else{
				
				return false;
			}
			
		}else{
				
				return false;
			}
		
		
	}

	
	//provider can reject booking within time and later rejection is chargable by admin
	protected function rejectBooking($request){

			$phone = $request->input('phone');
			if($request->input('reason_id')=='' || $request->input('service_id')=='') return false;
			$provider=Provider::where('phone',$phone)->first();
			$service=Service::where("id",$request->input('service_id'))->where("provider_id",$provider->id)->where('service_status_id','<>',2)->where('service_status_id','<>',6)->first();
			if($service){

				$confirmednotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','confirmed')->count();
				
				$headnotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','heading')->count();
				
				$startednotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','jobstarted')->count();

				
				$booking_slot_id=$service->booking_slot_id;
				if($confirmednotice>0){
					//cancelled after confirming
				$service->service_status_id=8;
				}
				elseif($headnotice>0){
					//cancelled after heading
				$service->service_status_id=10;
				}
				elseif($startednotice>0){
					//cancelled after job start
				$service->service_status_id=11;
				}
				else{
					//cancelled
				$service->service_status_id=2;
				}
				$service->booking_slot_id=null;
				$service->save();

				if($booking_slot_id){
					//reschedule
					$this->TimeupdateRescheduleProvider($booking_slot_id,$provider,$service,true);
					
				
					Providerbookedtime::where('id',$booking_slot_id)->delete();
				}
				Appointments::where('service_id',$service->id)->delete();

				$history=new ProviderCancelHistory();
				$history->provider_id=$provider->id;
				$history->service_id=$service->id;
				$history->date=date('Y-m-d H:i:s');
				$history->reason_id=$request->input('reason_id');
				if($request->input('comment'))
				$history->comment=$request->input('comment');

				

				$nocancel=Servicesettings::select('code','value')->where('code','provider_no_penalty_duration')
				->where('type_id',$service->type_id)->first();
				$date = date("Y-m-d H:i:s");
				$time = strtotime($date);
				$time = $time - ($nocancel->value * 60);
				$pasttime = date("Y-m-d H:i:s", $time);
				$duration=(time()-strtotime($service->request_date))/60;

				$maxcancel=Servicesettings::select('code','value')->where('code','provider_max_cancel_unit')
							->where('type_id',$service->type_id)->first();

							
				$pastcancelcount=ProviderCancelHistory::where('provider_id',$provider->id)->where('date','<=',$date)->where('date','>=',$pasttime)->count();
				$penalty_amount=0;
				
				if($nocancel->value<$duration){
				
							
							

							$moderate=Servicesettings::select('code','value')->where('code','provider_moderate_penalty_duration')
							->where('type_id',$service->type_id)->first();

							$higher=Servicesettings::select('code','value')->where('code','provider_higher_penalty_duration')
							->where('type_id',$service->type_id)->first();

							


								if($duration<$moderate->value){
									
										$moderateprice=Servicesettings::select('code','value')->where('code','provider_moderate_penalty_amount')
										->where('type_id',$service->type_id)->first();

										$penalty_amount=$moderateprice->value;

										}
									elseif($duration>=$higher->value){
											$higherprice=Servicesettings::select('code','value')->where('code','provider_higher_penalty_amount')
										->where('type_id',$service->type_id)->first();

										$penalty_amount=$higherprice->value;
										}


						
						

				}

				$history->penalty_amount=$penalty_amount;
				
				$history->save();
				$this->updateProviderCredit($provider,$penalty_amount);
				
					$pnotice=new Providernotification();
					$pnotice->provider_id=$provider->id;
					$pnotice->service_id=$service->id;
					$pnotice->date=date('Y-m-d H:i:s');
					$pnotice->details='Job Rejected';
					$pnotice->notification_latitude=$provider->current_latitude;
					$pnotice->notification_longitude=$provider->current_longitude;
					$pnotice->invite_status='cancelled';
					$pnotice->service_notice_status='cancelled';
					$pnotice->price=$service->expected_cost;
					$pnotice->save();

					
					
			
				
					$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();

					if($service->service_status_id==2)
					$msg="The provider rejected your request, sorry for this!";
					else
					$msg=$provider->name." had to cancel the job, sorry for this!";

					if($customer->device_type=='ios' && $customer->device_id!=''){
					
					$type=($request->input('type')=='cancelled')?'cancelled':'rejected';
					$arr = array('type'=>$type,'id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
					$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
				
					}elseif($customer->device_id!='' ){
						$type=($request->input('type')=='cancelled')?'cancelled':'rejected';
						$arr = array('type'=>$type,'id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
						$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

					}

				
					
					return true;
			}
			
		return false;
	}

/*
	protected function rescheduleProvider($booking_slot_id,$provider,$service)
	{

			//reschedule
					//reschedule
					$booktime=Providerbookedtime::where("id",$booking_slot_id)->first();
					
					$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
					$buffer_time=(int)$buffer_time->value;
					$mode=($provider->profile->transport=='walk')?'walking':'driving';


						
					$all_next_booking=Providerbookedtime::with('Service')
					->whereHas('Service', function ($query) use ($booktime){
						$query->where('service_status_id',4);
						$query->where('booking_slot_id','>',0);
						$query->where('to_time','>',$booktime->to_time);
						
					})
					->orderby("from_time","asc")->get();
				
					//print_r($all_next_booking->toArray());
					
					if($all_next_booking){


						foreach($all_next_booking as $nbooking)
						{
							$from_time=$nbooking->from_time;
							$to_time=$nbooking->to_time;

							$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
							->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
							->where('to_time','<', $from_time)
							->where('id','<>', $booking_slot_id)
							->whereHas('Service', function ($query) use ($service){
									$query->where('service_status_id',4);
									$query->where('booking_slot_id','>',0);
									//$query->where('id','>',$service->id);
									
								})
							->orderby('from_time','desc')->first();
							//print_r($prevbooked->toArray());exit;
						
							//if no booking then estimate job start time and end time
									$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
									if($provider->base_latitude && $provider->base_longitude && $provider->location_preference=='fixed')
									$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
									elseif($prevbooked)
									$origin='origins='.$prevbooked->service->latitude.','.$prevbooked->service->longitude;
									else
									$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
									
									$destination='&destinations='.$nbooking->service->latitude.','.$nbooking->service->longitude;
									
									$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
								
									$data=json_decode(($data),true);
								
									
									if(!isset($data['rows'][0]['elements'][0]['duration'])) continue;
									$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;

									
							if($prevbooked)
								{
									
									$heading_start=date("H:i:s", strtotime($prevbooked->to_time));
								}
							else
								$heading_start=date("H:i:s", strtotime('-'.$estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));

							$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" (('".$heading_start."' between from_time and to_time)  )")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($nbooking->service->to_time)))
									->first();
						
									if(count($freeslot)>0)
									{
										if($freeslot->from_time>$heading_start)
										{
												
												$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime(date("Y-m-d",strtotime($nbooking->service->to_time))." ".$freeslot->from_time)));

												

										}

										elseif($prevbooked  )
										{
											
											$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($prevbooked->to_time)));
											
											//if($espected_request_start_time<=$nbooking->service->from_time)
											//$espected_request_start_time=$nbooking->service->from_time;

											
										}
										
										else
										{
										
										$espected_request_start_time=$nbooking->service->from_time;

										
										}
									}else
									{
										
										  $espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));
										 
									}
									
									
									$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
									
								

									if(strtotime($espected_request_start_time)>=strtotime($nbooking->service->from_time)  && $espected_request_end_time<=$nbooking->service->to_time)
									{
									$booking=Providerbookedtime::where("id",$nbooking->service->booking_slot_id)->first();
									//echo "id".$nbooking->service->booking_slot_id;
									$booking->from_time=$espected_request_start_time;
									
									$booking->to_time=$espected_request_end_time;
									$booking->save();
									}elseif($espected_request_start_time<$nbooking->service->from_time){
										//echo "id".$nbooking->service->booking_slot_id;
										//echo "ff";
										//if provider reach at place before arrival that means
										//more time within schedule gap so adjustment not required for next bookings
										break;
									}





									

						}		


					}
					//echo "test";
					//exit;

	}

*/




	//reschedule provider
	protected function TimeupdateRescheduleProvider($booking_slot_id,$provider,$service,$cancel=false)
	{

			//reschedule
					//reschedule
					$booktime=Providerbookedtime::where("id",$booking_slot_id)->first();
					
					$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
					$buffer_time=(int)$buffer_time->value;
					$mode=($provider->profile->transport=='walk')?'walking':'driving';


						
					$all_next_booking=Providerbookedtime::with('Service')
					->whereHas('Service', function ($query) use ($booktime,$service){
						$query->where('service_status_id',4);
						$query->where('id','<>',$service->id);
						$query->where('booking_slot_id','>',0);
						
						
					})
					->where('to_time','>',$booktime->to_time)
					->whereDate('to_time',date("Y-m-d",strtotime($booktime->to_time)))
					->where('provider_id',$provider->id)
					->orderby("from_time","asc")->get();
					//print_r($all_next_booking->toArray());
					//exit;
					$changebooking=array();
					
					if($all_next_booking){

						$canchange=false;						
						$i=0;
						foreach($all_next_booking as $nbooking)
						{
							$from_time=$nbooking->from_time;
							$to_time=$nbooking->to_time;

							if($cancel==true){

										$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
									->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
									->where('id','<=', $nbooking->id)
									
									->whereHas('Service', function ($query) use ($service,$nbooking){
											
											$query->where('booking_slot_id','>',0);
											$query->where('booking_slot_id','<>',$nbooking->id);
											//$query->where('id','>',$service->id);
											
										})
									->orderby('id','desc')->first();

							}else{
								$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
								->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
								->where('id','<=', $nbooking->id)
								
								->whereHas('Service', function ($query) use ($service,$nbooking){
										$query->whereRaw('(service_status_id=4 or service_status_id=5 or service_status_id=9)');
										$query->where('booking_slot_id','>',0);
										$query->where('booking_slot_id','<>',$nbooking->id);
										//$query->where('id','>',$service->id);
										
									})
								->orderby('id','desc')->first();
						}

						//print_r($prevbooked);exit;

						
						

									
							if($prevbooked)
								{
									if($i==0){


										if($cancel==true){

												$booktime=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
												->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
												->where('to_time','<=', $booktime->from_time)
												
												->whereHas('Service', function ($query) use ($service,$nbooking){
														$query->whereRaw('(service_status_id=4 or service_status_id=5 or service_status_id=9)');
														$query->where('booking_slot_id','>',0);
														$query->where('booking_slot_id','<>',$nbooking->id);
														
														
													})
												->orderby('id','desc')->first();

														if(!$booktime){

															$booktime=Providerbookedtime::where("id",$booking_slot_id)->first();
															$head_start_time=date("Y-m-d H:i:s", strtotime('-'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));
															
															$freeslot=Providerfreetime::where('provider_id',$provider->id)
															->whereRaw(" ('".date("H:s:i",strtotime($head_start_time))."' between from_time and to_time
															OR '".date("H:s:i",strtotime($nbooking->service->from_time))."' between from_time and to_time )
															")
														
															->whereDate('date',date("Y-m-d",strtotime($booktime->from_time)))
															->orderby("from_time","desc")->first();
															

															if($freeslot)
															{
																if($freeslot->time_type=='free')
																$booktime->to_time=$head_start_time;
																else
																{
																 $booktime->to_time=$head_start_time=$freeslot->date." ".$freeslot->to_time;
																
																}
															

															

															
															}

														}

												

												}
												
											$heading_start=date("H:i:s", strtotime($booktime->to_time));
											
											}else{
											$heading_start=date("H:i:s", strtotime($changebooking[$i-1]['to_time']));
											}
									
								}
							else
							$heading_start=date("H:i:s", strtotime('-'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));


							

							$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" (('".$heading_start."' between from_time and to_time)  )")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($nbooking->service->to_time)))
									->first();
						
									if(count($freeslot)>0)
									{
										if($freeslot->from_time>$heading_start)
										{
												
												$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime(date("Y-m-d",strtotime($nbooking->service->to_time))." ".$freeslot->from_time)));
											
												

										}

										elseif($prevbooked  )
										{
											if($i==0){

												
											
												
											$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($booktime->to_time)));
											
											}else{
											$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($changebooking[$i-1]['to_time'])));
											
											}
											//echo "previd".$prevbooked->service->id;
											//echo "prevtime".$prevbooked->to_time;
											//echo '----';
											//if($espected_request_start_time<=$nbooking->service->from_time)
											//$espected_request_start_time=$nbooking->service->from_time;
											
											
										}
										
										else
										{
										
										$espected_request_start_time=$nbooking->service->from_time;

										
										}
									}else
									{
										
										  $espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));
										 
									}
									
									
									$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
									//echo "id".$nbooking->service->id;
									//echo $espected_request_start_time;
									//echo $espected_request_end_time;
									//echo $nbooking->service->from_time;
									//echo $nbooking->service->to_time;
									//echo "---------------------------<br />";

									//$booking=Providerbookedtime::where("id",$nbooking->service->booking_slot_id)->first();

									$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" (('".date("H:i:s",strtotime($espected_request_start_time))."' between from_time and to_time and '".date("H:i:s",strtotime($espected_request_end_time))."' between from_time and to_time)  )")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($espected_request_start_time)))
									->count();

									//echo $heading_start;
									//echo $espected_request_start_time;
									//echo $nbooking->service->from_time;
									//echo $nbooking->service->to_time;
									//echo $espected_request_start_time;
									//echo $espected_request_end_time;
									//echo $freeslot;
									//echo "-----------------<br />";
									//exit;
											
									if($espected_request_start_time>=$nbooking->service->from_time && $espected_request_start_time<=$nbooking->service->to_time && $freeslot>0)
									{
									
									//echo "id".$nbooking->service->id;
									//$booking->from_time=$espected_request_start_time;
									
									//$booking->to_time=$espected_request_end_time;
									
									$changebooking[]=array("id"=>$nbooking->id,"from_time"=>$espected_request_start_time,"to_time"=>$espected_request_end_time);
									//$booking->save();
									$canchange=true;
									}elseif($espected_request_start_time<$nbooking->service->from_time){
										//echo "id".$nbooking->service->booking_slot_id;
										//echo "ff";
										//if provider reach at place before arrival that means
										//more time within schedule gap so adjustment not required for next bookings
										$canchange=false;
										break;
									}else{
										$canchange=false;
										break;
									}





							$i++;		

						}		


					}

					
					
					if($canchange==true)
					{
						//echo "ddd";
						//print_r($changebooking);
						//exit;
						foreach($changebooking as $booking){
							$book=Providerbookedtime::with('Service')->where("id",$booking['id'])->first();
							$book->heading_start_time=date("Y-m-d H:i:s", strtotime('-'.$book->service->estimated_traveltime_heading.' minutes', strtotime($booking['from_time'])));
							$book->from_time=$booking['from_time'];
							$book->to_time=$booking['to_time'];
							$book->save();

						

						}
					}elseif(count($all_next_booking)<=0){
						
						$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" ('".date("H:i:s",strtotime($booktime->to_time))."' between from_time and to_time) ")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($booktime->to_time)))
									->first();

									
						/*$book=Providerbookedtime::whereRaw(" ('".($booktime->to_time)."' between from_time and to_time and id <>'".$booktime->id."' )
						OR ( to_time > '".($booktime->to_time)."'  and id <>'".$booktime->id."' )

						")
						->whereDate('to_time',date("Y-m-d",strtotime($booktime->to_time)))
						->where('provider_id',$provider->id)

						
						->count();*/
						//echo "bcount".$book;
					//exit;
						if(count($freeslot)>0) return true;


					}

					

					return $canchange;
					//echo "test";
					//exit;

	}

	//get current sattle balance of a provider
	protected function getProviderBalance($request)
	{

		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		$count=CompanyProvider::where('provider_id',$provider->id)->count();

		$balance=array();
		
		$profile=Providerprofile::select('max_due_amount','current_credit_balance')->where('provider_id',$provider->id)->first();
		$balance['max_due_amount']=$profile->max_due_amount;		
		$balance['current_credit_balance']=(float)$profile->current_credit_balance;

		//print_r($profile->toArray());exit;

		$account=ProviderCompanyAccount::where('provider_id',$provider->id)
		->whereDate('arrival_time','>=',date("Y-m-d",time()))
		->where('sattled','no')->orderby('id','desc')->first();
		

		if($account){
			$balance['can_cancel']=1;
			//company provider
				if($count>0){
					
					$balance['account_no']=$provider->account_no;
					$balance['pay_type']=$account->pay_type;
					$balance['arrival_time']=$account->arrival_time;
					$balance['address']=$account->address;
					$balance['departure_time']=$account->departure_time;
					$balance['agent_arrival_latitude']=$account->agent_arrival_latitude;
					$balance['agent_arrival_longitude']=$account->agent_arrival_longitude;
					$balance['current_credit_balance']=$account->current_credit_balance;
					if($balance['current_credit_balance']>0){
					$balance['sattle_required']=1;
					
					}else{
					$balance['sattle_required']=0;
					
					}
					

				}else{
				//normal provider

					
					
					$balance['account_no']=$provider->account_no;
					$balance['pay_type']=$account->pay_type;
					$balance['arrival_time']=$account->arrival_time;
					$balance['address']=$account->address;
					$balance['departure_time']=$account->departure_time;
					$balance['agent_arrival_latitude']=$account->agent_arrival_latitude;
					$balance['agent_arrival_longitude']=$account->agent_arrival_longitude;
					$balance['current_credit_balance']=$profile->current_credit_balance;
					if($balance['current_credit_balance']>0){
					$balance['sattle_required']=1;
					
					
					}else{
					$balance['sattle_required']=0;
					
					}
					
					

				}
				
		}else{
		$balance['can_cancel']=0;
		}

		return $balance;;
	}


	//cancel the sattle request
	protected function cancelSattle($request){

		if($request->input('phone')=='')
		return false;
		
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();

		$sattle=ProviderCompanyAccount::where('provider_id',$provider->id)
		//->whereDate('arrival_time','>=',date("Y-m-d",time()))
		//->where('sattled','no')->orderby('id','desc')
		->first();

		if($sattle){

			$sattle->sattled='cancel';
			$sattle->arrival_time='';
			$sattle->departure_time='';
			$sattle->pay_type='';
			$sattle->agent_arrival_latitude='';
			$sattle->agent_arrival_longitude='';
			$sattle->save();
			return true;
		}

		return false;

		
	}

	//request qber to get due credit balance from provider
	protected function requestBalanceSattle($request){

		if($request->input('arrival_time')=='' || $request->input('departure_time')==''
		|| $request->input('agent_arrival_latitude')=='' || $request->input('agent_arrival_longitude')=='')
		return false;
		
		
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		$count=CompanyProvider::where('provider_id',$provider->id)->count();

		


		$sattled=ProviderCompanyAccount::where('provider_id',$provider->id)
		->whereDate('arrival_time','>=',date("Y-m-d",time()))
		->where('sattled','no')->orderby('id','desc')->first();

		if($sattled) return false;
		

		$account=ProviderCompanyAccount::where('provider_id',$provider->id)->first();

		//if no account then create the account
		if(!$account){
			
			$pcompany=new ProviderCompanyAccount;
			$pcompany->provider_id=$provider->id;
			//$pcompany->account_no=strtoupper(uniqid());
			//provider is under company
			if($count>0)
			{
				$balance=Providerbalancehistory::where('provider_id',$provider->id)->sum('commission_amount');
				$pcompany->current_credit_balance=$balance;
			}
		}
		else{
			$pcompany=ProviderCompanyAccount::where('provider_id',$provider->id)->orderby('id','desc')->first();
			
			
		}

		//check if provider has balance to sattle
		if($count>0)
		{
			$credit=$pcompany->current_credit_balance;

		}else{
			$profile=Providerprofile::where("provider_id",$provider->id)->first();
			$credit=$profile->current_credit_balance;
		}
		if($credit<=0) return "nocredit";
			
			
			$pcompany->pay_type='agent';
			$pcompany->sattled='no';
			$pcompany->arrival_time=$request->input('arrival_time');
			$pcompany->departure_time=$request->input('departure_time');
			$pcompany->agent_arrival_latitude=$request->input('agent_arrival_latitude');
			$pcompany->agent_arrival_longitude=$request->input('agent_arrival_longitude');
			$pcompany->address=$request->input('address'); 	 
			$pcompany->save();

			
				
			$balance=$this->getProviderBalance($request);


			return array("sattle"=>1,"data"=>$balance);


	}

	//provider can wait a request to avoid autocancel
	protected function setWaitDuration($request){

		if($request->input('phone')=='' || $request->input('service_id')=='')
		return false;
		
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		$service=Service::where('id',$request->input('service_id'))->where('provider_id',$provider->id)->first();

		if($service){
		$service->provider_waitto_cancel=1;
		$service->provider_waitto_cancel_time=date("Y-m-d H:i:s");
		$service->save();

		$min_duration=Servicesettings::select('code','value')->where('code','provider_wait_duration')->where('type_id',$service->type_id)->first();

		$cancel_duration=Servicesettings::select('code','value')
		->where('code','provider_wait_autocancel_duration')->where('type_id',$service->type_id)->first();


		

		$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
		
		$msg=$provider->name." is now reviewing your request, he/she has 3 minutes to respond.";
		if($customer->device_type=='ios' && $customer->device_id!='')
		{
			
			$arr = array('type'=>'jobwait','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,
			"default_cancel_duration"=>$min_duration,"auto_cancel_duration"=>$cancel_duration->value);
			$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
	
		}
		elseif($customer->device_id!='' )
		{
			$arr = array('type'=>'jobwait','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,
		"default_cancel_duration"=>$min_duration,"auto_cancel_duration"=>$cancel_duration->value
			);
			$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

		}
				

		return true;
		}

		return false;


		
	}

	//automatically cancel the service request which is not accepted by provider within time
	protected function autoCancel($request){

		$provider=Provider::where('phone',$request->input('phone'))->first();
		$service=Service::where('id',$request->input('service_id'))->where('provider_id',$provider->id)->first();

		$min_duration=Servicesettings::select('code','value')->where('code','provider_wait_duration')->where('type_id',$service->type_id)->first();

		$cancel_duration=Servicesettings::select('code','value')
		->where('code','provider_wait_autocancel_duration')->where('type_id',$service->type_id)->first();
		
		if($service->provider_waitto_cancel=='1'){
			$duration=$min_duration->value+$cancel_duration->value;
			$futuretime=date("Y-m-d H:i:s",strtotime($service->request_date) . ' +'.$duration." minutes");
			if($futuretime>date("Y-m-d H:i:s")){
				$service->service_status_id=12;
				$service->save();

				
				$provider->is_online=0;
				$provider->ready_to_serve=0;
				//$provider->remember_token='';
				//$provider->api_token='';
				$provider->save();

				$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
				
				$msg=$provider->name." did not reply, please select another provider";
				if($customer->device_type=='ios' && $customer->device_id!=''){
		
				$arr = array('type'=>'ignored','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
			
				}elseif($customer->device_id!='' ){
					$arr = array('type'=>'ignored','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
					$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

				}
				
			}

		}
		else
		{

			$duration=$min_duration->value;
			$futuretime=date("Y-m-d H:i:s",strtotime(' +'.$duration." seconds",strtotime($service->request_date)));
			if($futuretime>date("Y-m-d H:i:s")){
				$service->service_status_id=12;
				$service->save();

				$provider->is_online=0;
				$provider->ready_to_serve=0;
				//$provider->remember_token='';
				//$provider->api_token='';
				$provider->save();

				$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
				

				if($customer->device_type=='ios' && $customer->device_id!=''){
		
				$arr = array('type'=>'ignored','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerAPNS("Appoinment Rejected!",$arr,$customer->device_id);
			
				}elseif($customer->device_id!='' ){
					$arr = array('type'=>'ignored','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
					$status= $this->sendCustomerFCM("Appoinment Rejected!",$arr,$customer->device_id);

				}
			
			}

		}

		return true;
		
		
	}

	//provider can update price of appointment at different state 
	protected function updateProviderPrice($request){

		if($request->input('price')=='' || $request->input('phone')=='' || $request->input('service_id')=='')
		return false;

		$provider=Provider::where('phone',$request->input('phone'))->first();
		$service=Service::where('id',$request->input('service_id'))->where('provider_id',$provider->id)->first();

		if($request->input('price')>0 && $service){
			$service->expected_cost=$request->input('price');
			$service->save();

			$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
			
			$msg=$provider->name." has edited job details, tap here to see the new job";
			if($customer->device_type=='ios' && $customer->device_id!=''){
		
				$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
			
			}elseif($customer->device_id!='' ){
				$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

			}
		

		return true;

		}

		return false;

	}

	//provider can update the job duration at different state
	protected function updateDuration($request){

		if($request->input('duration')=='' || $request->input('phone')=='' || $request->input('service_id')=='')
		return false;

		$provider=Provider::where('phone',$request->input('phone'))->first();
		$service=Service::where('id',$request->input('service_id'))->where('provider_id',$provider->id)->first();
		if(!$service) return false;
		$min_duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$service->type_id)->first();
		

			$changed=false;

			
			$booked=Providerbookedtime::where("id",$service->booking_slot_id)->first();
			if(!$booked) return false;
			
			$nextbooked=Providerbookedtime::with("Service")->whereDate("book_date",date("Y-m-d",strtotime($booked->from_time)))
			->where("provider_id",$provider->id)
			->where("from_time",'>',date("Y-m-d H:i:s",strtotime($booked->from_time)))->orderby("from_time","asc")
			->first();

			$new_to_time=date("Y-m-d H:i:s", strtotime('+'.$request->input('duration').' minutes',strtotime($booked->from_time)));
			$new_from_time=date("Y-m-d H:i:s", strtotime('-'.$request->input('duration').' minutes',strtotime($new_to_time)));
			
			if($nextbooked)
			{
				$nextbooked_heading_start=date("Y-m-d H:i:s", strtotime('-'.$nextbooked->service->estimated_traveltime_heading.' minutes',strtotime($nextbooked->from_time)));
				
				$nextbooked->from_time=date("Y-m-d H:i:s", strtotime('+'.$nextbooked->service->estimated_traveltime_heading.' minutes',strtotime($new_to_time)));
				
			
				if($nextbooked->from_time>$nextbooked->service->to_time)
				return false;
			}
		
			
			
			
			if(date("Y-m-d", strtotime($new_to_time))!=date("Y-m-d", strtotime($booked->from_time)))
			return false;
			//if not in free slot then disallow
			$new_time=date("H:i:s", strtotime('+'.$request->input('duration').' minutes',strtotime($booked->from_time)));
			$freeslot=Providerfreetime::where('provider_id',$provider->id)
					
					->whereDate('date',date("Y-m-d",strtotime($new_time)))
					->whereRaw("'".$new_time. "' between from_time and to_time and time_type='free'")
					->count();
			
		
			if($freeslot<=0)
			return false;
			
			//echo $nextbooked_heading_start;
			//echo $nextbooked->to_time;
			//echo $new_to_time;
			//echo "test1".$nextbooked->service->id;exit;
			if(!$nextbooked
			//&& $request->input('duration')>=$min_duration->value
			&& $service){
					$booked->to_time=$new_to_time;
					$booked->save();
					$service->job_duration_set_by_provider=$request->input('duration');
					$service->save();

					$changed=true;
				
				}
				elseif($nextbooked
				//&& $nextbooked_heading_start>=$new_to_time
				
				){

					//$new_from_time=date("Y-m-d H:i:s", strtotime('-'.$request->input('duration').' minutes',strtotime($new_to_time)));
					
					$old_to_time=$booked->to_time;
					$old_from_time=$booked->from_time;
					$booked->to_time=$new_to_time;
					$booked->from_time=$new_from_time;
					$booked->save();
					$old_duration=$service->job_duration_set_by_provider;
					$service->job_duration_set_by_provider=$request->input('duration');
					$service->save();
					
					//$nextbooked->from_time=date("Y-m-d H:i:s", strtotime('+'.$nextbooked->service->estimated_traveltime_heading.' minutes',strtotime($new_to_time)));
					//$nextbooked->to_time=date("Y-m-d H:i:s", strtotime('+'.$nextbooked->service->job_duration_set_by_provider.' minutes',strtotime($nextbooked->from_time)));
					//$nextbooked->save();
					$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);

					if($status==false){
				
					$booked->to_time=$old_to_time;
					$booked->from_time=$old_from_time;
					$booked->save();
					$changed=false;
					$service->job_duration_set_by_provider=$old_duration;
					$service->save();
					}else{
						
						$changed=true;
						}
				}
				
				//if duration not enough to cover next booking
				elseif($nextbooked && $nextbooked->to_time<$new_to_time){

					
					//echo "test";exit;
					return false;
				}
			
		
		if($changed){
			
		
			

			$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
			$msg=$provider->name." has edited job details, tap here to see the new job";

			if($customer->device_type=='ios' && $customer->device_id!=''){
		
				$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
			
			}elseif($customer->device_id!='' ){
				$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

			}
			

		return true;

		}

		return false;

	}

	// provider can update service end time
	protected function updateEndtime($request){

		if($request->input('to_time')=='' || $request->input('phone')=='' || $request->input('service_id')=='')
		return false;
		
		$provider=Provider::where('phone',$request->input('phone'))->first();
		$service=Service::where('id',$request->input('service_id'))->where('provider_id',$provider->id)->first();
		$min_duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$service->type_id)->first();

		if(!$service->booking_slot_id)
		return false;
		
		$booking=Providerbookedtime::where('id',$service->booking_slot_id)->first();
		$time=strtotime(date("Y-m-d",strtotime($booking->from_time))." ".$request->input('to_time'));
		$currentEndtime=date("Y-m-d H:i:s",$time);

		if(date("Y-m-d", strtotime($currentEndtime))!=date("Y-m-d", strtotime($booking->from_time)))
			return false;

		if($currentEndtime>$service->to_time)
			return false;
			
		$freeslot=Providerfreetime::where('provider_id',$provider->id)
					
					->whereDate('date',date("Y-m-d",strtotime($currentEndtime)))
					->whereRaw("'".date("H:i:s",strtotime($currentEndtime)). "' between from_time and to_time and time_type='free'")
					->count();
		
			if($freeslot<=0)
			return false;
		
		$expected=date("Y-m-d H:i:s",strtotime($booking->from_time. ' +'.$service->job_duration_set_by_provider." minutes") );



		$nextbooked=Providerbookedtime::with("Service")->whereDate("book_date",date("Y-m-d",strtotime($booking->from_time)))
			->where("provider_id",$provider->id)
			->where("from_time",'>',date("Y-m-d H:i:s",strtotime($booking->to_time)))->orderby("from_time","asc")
			->first();
		if($nextbooked)
		{
			$nextbooked_heading_start=date("Y-m-d H:i:s", strtotime('-'.$nextbooked->service->estimated_traveltime_heading.' minutes',strtotime($nextbooked->from_time)));

			$nextbooked->from_time=date("Y-m-d H:i:s", strtotime('+'.$nextbooked->service->estimated_traveltime_heading.' minutes',strtotime($new_to_time)));
				if($nextbooked->from_time>$nextbooked->service->to_time)
				return false;

		}

		
		
		if(!$nextbooked && $currentEndtime>=$expected && $service){
			
			$booking->to_time=$currentEndtime;
			$booking->save();
			$service->job_duration_set_by_provider=round(abs(strtotime($booking->to_time) - strtotime($booking->from_time)) / 60,2);
			$service->save();

			$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
			
			$msg=$provider->name." has edited job details, tap here to see the new job";

			if($customer->device_type=='ios' && $customer->device_id!=''){
		
			$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
			$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
			
			}elseif($customer->device_id!='' ){
				$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

			}

			//$this->rescheduleProvider($service->booking_slot_id,$provider,$service);
			$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);

			return true;

		} 
		elseif($nextbooked && $nextbooked_heading_start >= $currentEndtime && $service &&  $currentEndtime>=$expected){

			/*
			$booking->to_time=$currentEndtime;
			$booking->save();
			$service->job_duration_set_by_provider=round(abs(strtotime($booking->to_time) - strtotime($booking->from_time)) / 60,2);
			$service->save();
			*/


					$old_to_time=$booking->to_time;
					$booking->to_time=$currentEndtime;
					$booking->save();
					$old_duration=$service->job_duration_set_by_provider;
					$service->job_duration_set_by_provider=round(abs(strtotime($booking->to_time) - strtotime($booking->from_time)) / 60,2);
					$service->save();
					
					//$nextbooked->from_time=date("Y-m-d H:i:s", strtotime('+'.$nextbooked->service->estimated_traveltime_heading.' minutes',strtotime($new_to_time)));
					//$nextbooked->to_time=date("Y-m-d H:i:s", strtotime('+'.$nextbooked->service->job_duration_set_by_provider.' minutes',strtotime($nextbooked->from_time)));
					//$nextbooked->save();
					$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);

					if($status==false){
					$booking->to_time=$old_to_time;
					$booking->save();
					$changed=false;
					$service->job_duration_set_by_provider=$old_duration;
					$service->save();
					}else{

						$changed=true;
						}

						
					
			if($changed==true){
					$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
					
					$msg=$provider->name." has edited job details, tap here to see the new job";

					if($customer->device_type=='ios' && $customer->device_id!=''){
				
					$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
					$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
					
					}elseif($customer->device_id!='' ){
						$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
						$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

					}
					return true;
			}
			


		}

		return false;

	}

	//provider can update car details if required which change the price of service
	protected function updateCarDetails($request){

		if($request->input('car_type')=='' || $request->input('phone')=='' || $request->input('service_id')=='')
		return false;
		$provider=Provider::where('phone',$request->input('phone'))->first();
		
		$service=Service::where('id',$request->input('service_id'))->where('provider_id',$provider->id)->first();
		$duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$service->type_id)->first();
		
		$cartype=json_decode($request->input('car_type'),true);

		$jduration=0;

		foreach($cartype as $details){
			
				
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){

							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							
							$jduration=$jduration+$options['duration'];
							
						}

				
					}
			

			}

			$old_duration=$service->job_duration_set_by_provider;
			
			if($jduration < $duration->value)	
			$service->job_duration_set_by_provider=$duration->value;
			else
			$service->job_duration_set_by_provider=$jduration;


			


			$booked=Providerbookedtime::where("id",$service->booking_slot_id)->first();
			$nextbooked=Providerbookedtime::whereDate("book_date",date("Y-m-d",strtotime($booked->from_time)))
			->where("from_time",'>',date("Y-m-d H:i:s",strtotime($booked->to_time)))->orderby("from_time","asc")
			->first();

			$old_to_time=$booked->to_time;

			
			$new_to_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($booked->from_time)));
			if(!$nextbooked){
				$booked->to_time=$new_to_time;
				$booked->save();
				$service->save();
				
				}
				elseif($nextbooked && $nextbooked->from_time>$new_to_time){
				$booked->to_time=$new_to_time;
				$booked->save();
				$service->save();
				//Servicecardetails::where('service_id',$request->input('service_id'))->delete();
				}
				//if duration not enough to cover next booking
				elseif($nextbooked && $nextbooked->from_time<$new_to_time){
					
					return false;
				}
				
		//reschedule
			$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);

			if($status==false)
			{

				$booked->to_time=$old_to_time;
				$booked->save();

				$service->job_duration_set_by_provider=$old_duration;
				$service->save();
				return false;

			}

		Servicecardetails::where('service_id',$request->input('service_id'))->delete();
			
		foreach($cartype as $details){
			
				$stype=1;
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){

							$car=new Servicecardetails();
							$car->service_id=$service->id;
							$car->number_of_cars=1;
							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							$car->carwash_type=$options['serviceoption']['name'].$stype;
							$car->option_details_id=$details[$i]['option_details'][$x];
							
							$car->save();
							$jduration=$jduration+$options['duration'];
							
						}

					$stype++;
					}
			

			}

			

		$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
		$msg=$provider->name." has edited job details, tap here to see the new job";

		if($customer->device_type=='ios' && $customer->device_id!=''){
		
			$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
			$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
			
		}elseif($customer->device_id!='' ){
			$arr = array('type'=>'jobupdate','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
			$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

		}

		return true;
		
	}

	//list all past and current booking done by provider

	protected function listBooking($request){
		$datewiseService=array();
		$provider=Provider::where('phone',$request->input('phone'))->first();
		$service=Service::selectRaw("*,DATE_FORMAT(request_date, '%Y-%m-%d') as bookdate")->with('Booked')->where('provider_id',$provider->id)
					;

		if($request->input('type')=='current'){
			$service=$service->whereDate('to_time',">=",date("Y-m-d"))->whereRaw(' ( service_status_id=1 OR service_status_id=4 OR service_status_id=5  OR service_status_id=9)');
			$datewiseService=Service::selectRaw("DATE_FORMAT(request_date, '%Y-%m-%d') as date")->where('provider_id',$provider->id)->whereRaw(' ( service_status_id=1 OR service_status_id=4 OR service_status_id=5 OR service_status_id=9)')->whereDate('to_time',">=",date("Y-m-d"))->groupBy("date")->orderBy('to_time', 'desc')->get()->toArray();
          
		}elseif($request->input('type')=='past'){
			$service=$service->whereDate('to_time',"<=",date("Y-m-d"))->whereRaw(' ( service_status_id=2 OR service_status_id=3 OR service_status_id=6 OR service_status_id=7 OR service_status_id=8 OR service_status_id=10 OR service_status_id=11 OR service_status_id=12)');

			$datewiseService=Service::selectRaw("DATE_FORMAT(request_date, '%Y-%m-%d') as date")->where('provider_id',$provider->id)->whereRaw(' ( service_status_id=2 OR service_status_id=3 OR service_status_id=6 OR service_status_id=7 OR service_status_id=8 OR service_status_id=10 OR service_status_id=11 OR service_status_id=12)')->whereDate('to_time',"<=",date("Y-m-d"))->orderBy('to_time', 'desc')->groupBy("date")->get()->toArray();
			
			//print_r($datewiseService);exit;

		}
		$service=$service->orderBy('to_time', 'desc')->get();
		
		if($service)
		{
			$service=$service->toArray();

			
			
			for($i=0;$i<count($service);$i++){

				$status=Servicestatus::where('id',$service[$i]['service_status_id'])->first();
				$service[$i]['job_status']=$status->name;
				
				if($status->id==6){
				
					$notice=Providernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","jobcompleted")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;
				}elseif($status->id==2 || $status->id==8 || $status->id==10 || $status->id==11){
					$notice=Providernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","cancelled")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;

				}

				$service[$i]['rating']=CustomerRatings::where("customer_id",$service[$i]['customer_id'])->avg('rate_value');
				$service[$i]['current_rate']=CustomerRatings::where("customer_id",$service[$i]['customer_id'])
				->where("service_id",$service[$i]['id'])->first();

				$service[$i]['already_rated']=CustomerRatings::where("customer_id",$service[$i]['customer_id'])->where("service_id",$service[$i]['id'])->count();
				
				$service[$i]['customer']=Customer::leftJoin('customer_profile','customers.id','=','customer_profile.customer_id')->selectRaw('name,image,phone')->where("customers.id",$service[$i]['customer_id'])->first();

			

				$mapurl='https://maps.googleapis.com/maps/api/staticmap?center='.$service[$i]['latitude'].",".$service[$i]['longitude'].'&zoom=11&size=600x300&
				maptype=roadmap&markers=icon:'.url("img/map_ico.png")."|".$service[$i]['latitude'].",".$service[$i]['longitude'].'&key='.env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k");

				$service[$i]['map_url']=$mapurl;

				

				for($x=0;$x<count($datewiseService);$x++){
					if($datewiseService[$x]['date']==$service[$i]['bookdate']){
						$datewiseService[$x]['service'][]=$service[$i];
					}

				}

			}

		
			//print_r($datewiseService);exit;
		return $datewiseService;
		}
		return false;
	}

	//get single appointment details 
	protected function getBooking($request){
		$provider=Provider::with("Profile")->where('phone',$request->input('phone'))->first();
		$service=Service::with('Cardetails')->where('service.provider_id',$provider->id)
		->leftJoin('customer_rating','service.customer_id','=','customer_rating.customer_id')
		->selectRaw("service.*,AVG(rate_value) as rating,count(rate_value) as ratecount")
		->where('service.id',$request->input('service_id'))->first();
		//print_r($service->toArray());exit;

		$mode=($provider->profile->transport=='walk')?'walking':'driving';
		
		$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
		//print_r($customer->toArray());exit;
		if($customer->profile->name)
		$service['customer_name']=$customer->profile->name;
		$service['customer_phone']=$customer->phone;


		$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
		$origin='destinations='.$service->latitude.','.$service->longitude;
		$destination='&origins='.$provider->current_latitude.','.$provider->current_longitude;
		
			
		$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
		$data=json_decode(($data),true);
		if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;

		
		$service['distance']=($data['rows'][0]['elements'][0]['distance']['value']/1000);

		
       /* 
        ///-----Bhim Kumar---///
        $customerrating=CustomerRatings::with('Rating')->where('customer_id',$service->customer_id)->get();
        
        if(!empty($customerrating))
        {
        	$cusallratingval ="";
	        foreach ($customerrating->toArray() as $cussinglerating) {
	        	   $cusallratingval += $cussinglerating['rate_value'];
	            }
	        $service['rating'] = $cusallratingval/count($customerrating->toArray());
	        $service['ratecount'] = count($customerrating->toArray());
        }
       
        ///-----Bhim Kumar---///

	*/

		$confirmednotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','confirmed')->first();
				
		$headnotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','heading')->first();
				
		$startednotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','jobstarted')->first();
				
		$completednotice=Providernotification::where('service_id',$request->input('service_id'))
				->where('provider_id',$provider->id)->where('service_notice_status','jobcompleted')->first();
				
		if($confirmednotice)		
		$service['confirmed_at']=$confirmednotice->date;
		if($headnotice)
		$service['heading_at']=$headnotice->date;
		if($startednotice)
		$service['started_at']=$startednotice->date;
		if($completednotice)
		$service['completed_at']=$completednotice->date;

		$service=$service->toArray();

		//print_r($service['cardetails']);exit;
		$caroptions=array();
		foreach($service['cardetails'] as $carserv)
		{
			if($carserv['price']>0)
			{
			$carserv['option_details'][0]['cost']=$carserv['price'];
			}
					$caroptions[$carserv['carwash_type']][]=$carserv['option_details'];
		
			
		}
		$service['cardetails']=$caroptions;
		
		
		return $service;
	}

	//called when provider started travelling to attend an appointment

	protected function startHeading($request){

		if($request->input('service_id')=='' || $request->input('latitude')=='' || $request->input('longitude')==''){
		return array("status"=>false,"reason"=>"Invalid Fields");
		}
		
		$provider=Provider::with("Profile")->where('phone',$request->input('phone'))->first();
		$service=Service::where('provider_id',$provider->id)->where('id',$request->input('service_id'))->first();
		//print_r($service->toArray());exit;
		if(!$service)
		return array("status"=>false,"reason"=>"Not a valid service");
		if($service->service_status_id!=4) return array("status"=>false,"reason"=>"Invalid Service Status");
		
		$lJob=Service::where('provider_id',$provider->id)->where('service_status_id',9)->first();
		if($lJob) return array("status"=>false,"reason"=>"Please complete your previous service","service_id"=>$lJob->id,"price"=>$lJob->expected_cost);

		$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
		$buffer_time=(int)$buffer_time->value;

			
		$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
		$notice=Providernotification::where("service_notice_status","heading")->where("service_id",$service->id)->count();
		$mode=($provider->profile->transport=='walk')?'walking':'driving';

		$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
		$origin='destinations='.$service->latitude.','.$service->longitude;
		$destination='&origins='.$request->input('latitude').','.$request->input('longitude');
		
		$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
		$data=json_decode(($data),true);
		if(!isset($data['rows'][0]['elements'][0]['duration']))  return array("status"=>false,"reason"=>"Map api expired or invalid location");
		$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
		$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);

		$tottime=(int)$duration+(int)$buffer_time;
		
		$fromtime=date("Y-m-d H:i:s", strtotime('+'.($tottime).' minutes', time()));

		
		
		//cant start job before arrival time
		if($fromtime<$service->from_time)
		return array("status"=>false,"reason"=>"Can't start job before arrival time");

		
		if($notice<=0){

					$mode=($provider->profile->transport=='walk')?'walking':'driving';
					
					$service->service_status_id=9;
					



					
					$notice=new Providernotification();
					$notice->provider_id=$provider->id;
					$notice->service_id=$service->id;
					$notice->date=date("Y-m-d H:i:s");
					$notice->details="Provider Started Heading.";
					$notice->notification_latitude=$request->input('latitude');
					$notice->notification_longitude=$request->input('longitude');


					
					
					$notice->distance_from_customer=$distance;
					$notice->duration_from_customer=$duration;
					$notice->service_notice_status='heading';
					if($request->input('price'))
					$notice->price=$request->input('price');
					$notice->save();

					$timehistory=Providertimehistory::where('provider_id',$provider->id)->where('service_id',$service->id)->first();
					if(!$timehistory){

						$timehistory=new Providertimehistory();
						$timehistory->provider_id=$service->provider_id;
						$timehistory->service_id=$service->id;
						$timehistory->save();

					}

							$worktime=Providerworktime::where('provider_id',$provider->id)->whereDate('date',date("Y-m-d"))
							->whereRaw("from_time=to_time")
							->where("type","online")->orderby("from_time","desc")->first();
							if($worktime){
								$worktime->to_time=date("Y-m-d H:i:s");
								$worktime->save();
								}

							$futuretime=date("Y-m-d H:i:s",strtotime("+".$duration." minutes", time()) );
							$worktime=new Providerworktime;
							$worktime->provider_id=$provider->id;
							$worktime->provider_timehistory_id=$timehistory->id;
							$worktime->from_time=date("Y-m-d H:i:s");
							$worktime->to_time=$futuretime;
							$worktime->service_id=$service->id;
							$worktime->date=date("Y-m-d H:i:s");
							$worktime->type='heading';
							$worktime->save();
						

					$service->estimated_traveltime_currentlocation=$duration;
					$service->total_traveltime=$duration;
					$service->save();


					$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();
					
					$booked->from_time=$fromtime;
					
					$booked->to_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($booked->from_time)));
					$booked->save();

					$provider->provider_status='heading';
					$provider->save();


					//reschedule
					$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);

			
					$this->updateProviderService($provider->id);
					

					if($customer->device_type=='ios' && $customer->device_id!=''){
					
						$arr = array('type'=>'heading','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
						$status= $this->sendCustomerAPNS($provider->name." is on the way, you can live track his location by tapping here!",$arr,$customer->device_id);
						
					}elseif($customer->device_id!='' ){
						$arr = array('type'=>'heading','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
						$status= $this->sendCustomerFCM($provider->name." is on the way, you can live track his location by tapping here!",$arr,$customer->device_id);
					}







					$type=Servicetype::where("id",$service->type_id)->first();
			
					$rating=ProviderRatings::selectRaw('AVG(rate_value) as rating, count(*) as ratecount')->where('provider_id',$provider->id)->get();
					$arr = array('type'=>'livetrack',"id"=>$service->id,"provider_name"=>$provider->name,"latitude"=>$request->input('latitude'),"longitude"=>$request->input('longitude'),"customer_name"=>$customer->profile->name,"transport"=>$provider->profile->transport,"duration"=>$duration,"provider_image"=>$provider->profile->image,"provider_rating"=>$rating,"service_type"=>$type->name);
					

					if($customer->device_type=='ios' && $customer->device_id!=''){
				
					
					$status= $this->sendCustomerAPNS("Live Track",$arr,$customer->device_id);
					
					}elseif($customer->device_id!='' ){

					$status= $this->sendCustomerFCM("Live Track",$arr,$customer->device_id);

					}



			

					return array("status"=>true,"reason"=>"Heading Started");;
		}else{

		return array("status"=>false,"reason"=>"Heading already started");
		}

	}

	//called when provider started working
	protected function startWorking($request){

		if($request->input('service_id')=='' || $request->input('latitude')=='' || $request->input('longitude')==''){
		return array("status"=>false,"reason"=>"Invalid Fields");
		}

		$provider=Provider::with("Profile")->where('phone',$request->input('phone'))->first();
		$service=Service::where('provider_id',$provider->id)->where('id',$request->input('service_id'))->first();
		if(!$service)
		return array("status"=>false,"reason"=>"Not a valid service");
		if($service->service_status_id!=9) return array("status"=>false,"reason"=>"Invalid Service Status");

		$count=Service::where('provider_id',$provider->id)->where('service_status_id',5)->count();
		if($count>0) return array("status"=>false,"reason"=>"Please complete your previous service");

		$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();

		//can't start before arrival time
		if($service->from_time>date("Y-m-d H:i:s")) return array("status"=>false,"reason"=>"Can't start before arrival time");
		//can't start after arrival range
		if($service->to_time<date("Y-m-d H:i:s")) return array("status"=>false,"reason"=>"Can't start after arrival range");

		
		
		
		$appointment=Appointments::where('provider_id',$provider->id)->where('service_id',$request->input('service_id'))->first();
		$customer=Customer::with("Profile")->where('id',$service->customer_id)->first();
		$notice=Providernotification::where("service_notice_status","jobstarted")->where("service_id",$service->id)->count();
		$mode=($provider->profile->transport=='walk')?'walking':'driving';

		
			if($notice<=0){
			$notice=new Providernotification();
			$notice->provider_id=$provider->id;
			$notice->service_id=$service->id;
			$notice->date=date("Y-m-d H:i:s");
			$notice->details="Provider Started Job.";
			$notice->notification_latitude=$request->input('latitude');
			$notice->notification_longitude=$request->input('longitude');


			$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
			$origin='destinations='.$service->latitude.','.$service->longitude;
			$destination='&origins='.$request->input('latitude').','.$request->input('longitude');
			
			$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
			$data=json_decode(($data),true);
			if(!isset($data['rows'][0]['elements'][0]['duration']))
			return false;
			
			
			$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
			$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);
			
			$notice->distance_from_customer=$distance;
			$notice->duration_from_customer=$duration;
			$notice->service_notice_status='jobstarted';
			$notice->price=$request->input('price');
			$notice->save();

			$traveltime=0;
			
			$startnotice=Providernotification::where("service_notice_status","heading")->where("service_id",$service->id)->first();
			if($startnotice){
			$traveltime=(time()-strtotime($startnotice->date))/60;
			$service->service_status_id=5;
			$service->total_traveltime=$traveltime;
			$service->save();

			}
			
			

			$timehistory=Providertimehistory::where("provider_id",$provider->id)->where("service_id",$service->id)->first();
			if($timehistory){
				$timehistory->drive_time=$traveltime;
				$timehistory->save();
				}else{
					$timehistory=new Providertimehistory();
					$timehistory->provider_id=$service->provider_id;
					$timehistory->service_id=$service->id;
					$timehistory->drive_time=$traveltime;
					$timehistory->save();

				}
				
			
				if($appointment){
					$appointment->service_status_id=5;
					$appointment->service_start_time=date("Y-m-d H:i:s");
					$appointment->save();
				}


			$timehistory=Providertimehistory::where('provider_id',$provider->id)->where('service_id',$service->id)->first();
			if($timehistory){

					//end duration of heading
					$worktime=Providerworktime::where('provider_id',$provider->id)->where('service_id',$service->id)->where("type","heading")->first();
					$worktime->to_time=date("Y-m-d H:i:s");
					$worktime->save();

					//working duration of service

					$worktime=new Providerworktime;
					$worktime->provider_id=$provider->id;
					$worktime->provider_timehistory_id=$timehistory->id;
					$worktime->from_time=date("Y-m-d H:i:s");
					$worktime->to_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($worktime->from_time)));
					$worktime->service_id=$service->id;
					$worktime->date=date("Y-m-d H:i:s");
					$worktime->type='working';
					$worktime->save();
					
				}

			$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();
			
			$booked->from_time=date("Y-m-d H:i:s");
			$booked->to_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($booked->from_time)));
			$booked->save();

			$provider->provider_status='working';
			$provider->save();


			//reschedule
			$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);
			$this->updateProviderService($provider->id);
			
			
			$msg=$provider->name." has Started the Job, expected finish time is ".$booked->to_time; 
			if($customer->device_type=='ios' && $customer->device_id!=''){
				
				$arr = array('type'=>'working','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
				$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
				
			}elseif($customer->device_id!='' ){

			$arr = array('type'=>'working','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
			$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);
			}

			return array("status"=>true,"reason"=>"You have started working");
		
		}
		else
		{
		return array("status"=>false,"reason"=>"You have already started working");
		
		}

		


	}

	//called when provider complated job by the app
	protected function completedWorking($request){

		if($request->input('service_id')=='' || $request->input('latitude')=='' || $request->input('longitude')=='' || $request->input('price')==''){
		return array("status"=>false,"reason"=>"Invalid Fields");
		}

		$provider=Provider::with("Profile")->where('phone',$request->input('phone'))->first();
		$service=Service::where('provider_id',$provider->id)->where('id',$request->input('service_id'))->first();
		if(!$service)
		return array("status"=>false,"reason"=>"Not a valid service");
		
		if($service->service_status_id!=5) return array("status"=>false,"reason"=>"Invalid Service Status");

		$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();

		//cannot complete service before arrival range
		if($booked->from_time>date("Y-m-d H:i:s")) return array("status"=>false,"reason"=>"Can't complete before arrival time");


		
		$appointment=Appointments::where('provider_id',$provider->id)->where('service_id',$request->input('service_id'))->first();
		$customer=Customer::with("Profile")->where('id',$service->customer_id)->first();
		$notice=Providernotification::where("service_notice_status","jobcompleted")->where("service_id",$service->id)->count();

		$mode=($provider->profile->transport=='walk')?'walking':'driving';
		
		if($notice<=0){

		//send notification to provider
		$notice=new Providernotification();
		$notice->provider_id=$provider->id;
		$notice->service_id=$service->id;
		$notice->date=date("Y-m-d H:i:s");
		$notice->details="Provider Completed Job.";
		$notice->notification_latitude=$request->input('latitude');
		$notice->notification_longitude=$request->input('longitude');


		$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
		$origin='destinations='.$service->latitude.','.$service->longitude;
		$destination='&origins='.$request->input('latitude').','.$request->input('longitude');
			
		$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
		$data=json_decode(($data),true);
		if(!isset($data['rows'][0]['elements'][0]['duration']))
		return false;

		$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
		$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);
		
		$notice->distance_from_customer=$distance;
		$notice->duration_from_customer=$duration;
		$notice->service_notice_status='jobcompleted';
		$notice->price=$request->input('price');
		$notice->save();


		$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();
		$booked->to_time=date("Y-m-d H:i:s");
		$booked->save();




		//send notification to customer
		$notice=new Customernotification();
		$notice->customer_id=$service->customer_id;
		$notice->service_id=$service->id;
		$notice->date=date("Y-m-d H:i:s");
		$notice->details="Provider Completed Job.";
		$notice->notification_latitude=$request->input('latitude');
		$notice->notification_longitude=$request->input('longitude');


		$notice->distance_from_provider=$distance;
		$notice->duration_from_provider=$duration;
		$notice->service_notice_status='jobcompleted';
		$notice->price=$request->input('price');
		$notice->save();


		
		$service->service_status_id=6;
		$service->save();
		
			if($appointment){

			$commission=Servicesettings::select('code','value')->where('code','payment_commission_percent')->where('type_id',$service->type_id)->first();
				
				$appointment->service_status_id=6;
				$appointment->service_end_time=date("Y-m-d H:i:s");
				$appointment->paid_amount=$request->input('price');
				$price=($request->input('price')*$commission->value)/100;
				$appointment->provider_amount=$request->input('price')-$price;


				$pastdate=date('Y-m-d H:i:s', strtotime('-500 hours', time()));
	 			
				$worktime=DB::select('select sum(work_time) as work_time from provider_time_log  left join appointments as app on(provider_time_log.service_id=app.service_id and provider_time_log.provider_id=app.provider_id)
				where provider_time_log.provider_id="'.$provider->id.'" and provider_time_log.date >="'.$pastdate.'" and app.service_status_id=6 order by provider_time_log.id desc');
				//print_r($worktime);
				if($worktime)
				$appointment->work_hours_oncomplete=$worktime[0]->work_time/60;
				$appointment->save();

				
				$totalamount=$this->updateProviderCredit($provider,$price);

				$balance=new Providerbalancehistory();
				$balance->provider_id=$provider->id;
				$balance->service_id=$service->id;
				$balance->duration=$duration;
				$balance->receive_amount=$request->input('price');
				$balance->commission_amount=$price;
				$balance->date=date("Y-m-d H:i:s");
				$balance->save();





				$timehistory=Providertimehistory::where('provider_id',$provider->id)->where('service_id',$service->id)->first();
				if($timehistory){

						//end duration of working
						$worktime=Providerworktime::where('provider_id',$provider->id)->where('service_id',$service->id)->where("type","working")->first();
						if($worktime){
						$worktime->to_time=date("Y-m-d H:i:s");
						$worktime->save();
						}

						
						
					}

			//$this->rescheduleProvider($service->booking_slot_id,$provider,$service);
			//reschedule
					$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);
					$this->updateProviderService($provider->id);
			
			}

		$msg=$provider->name." has completed the Job, he is requesting ".$request->input('price')." QAR";
		
		if($customer->device_type=='ios' && $customer->device_id!=''){
		
			$arr = array('type'=>'jobcompleted','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"provider_image"=>$provider->profile->image);
			$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
			
			}elseif($customer->device_id!='' ){

				$arr = array('type'=>'jobcompleted','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"provider_image"=>$provider->profile->image);
				$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);
			}
		
		$provider->preferred_location=$service->preference_job_area;
		$provider->provider_status='free';
		$provider->save();


		//Send notification to provider if balance to sattle more than 80%
		$totalamountpercent=($totalamount/$provider->profile->max_due_amount)*100;
		if($totalamountpercent>=100){
			$msg="You have reached 100% of your balance, please pay now to avoid account deactivation";
			if($provider->device_type=='ios' && $provider->device_id!=''){
				
					$arr = array('type'=>'balancelimit100',"provider_name"=>$provider->name);
					$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
				
				}elseif($provider->device_id!='' ){

					$arr = array('type'=>'balancelimit100',"provider_name"=>$provider->name);
					$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);
				}
		}
		elseif($totalamountpercent>=80){
			$msg="You have reached 80% of your balance, please pay now to avoid account deactivation";
			if($provider->device_type=='ios' && $provider->device_id!=''){
				
					$arr = array('type'=>'balancelimit80',"provider_name"=>$provider->name);
					$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
				
				}elseif($provider->device_id!='' ){

					$arr = array('type'=>'balancelimit80',"provider_name"=>$provider->name);
					$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);
				}
		}

		





		
		return array("status"=>true,"reason"=>"You have completed appointment");
		}else{

		return array("status"=>false,"reason"=>"You have already completed appointment");
		}

		


	}



	private function updateProviderCredit($provider,$price){
		if(!$provider && !$price)
			return false;

			$totalamount=0;

			$company=CompanyProvider::where("provider_id",$provider->id)->count();
				if($company>0){
					$account=ProviderCompanyAccount::where("provider_id",$provider->id)->first();
					$account->current_credit_balance=$account->current_credit_balance+$price;
					$account->sattled='no';
					$account->save();

					$totalamount=$account->current_credit_balance;
					
				}else{
					$profile=Providerprofile::where("provider_id",$provider->id)->first();
					$profile->current_credit_balance=$profile->current_credit_balance+$price;
					$profile->save();

					$totalamount=$profile->current_credit_balance;

					$account=ProviderCompanyAccount::where("provider_id",$provider->id)->first();
					if($account){
						$account->sattled='no';
						$account->save();
					}else{

						$pcompany=new ProviderCompanyAccount;
						$pcompany->provider_id=$provider->id;
						$pcompany->sattled='no';
						$pcompany->save();

					}



					
				}

				return $totalamount;

		
	}

	protected function getProviderCredit($provider){
		if(!$provider)
		return false;
		$balance=0;

			$company=CompanyProvider::where("provider_id",$provider->id)->count();
				if($company>0){
					$account=ProviderCompanyAccount::where("provider_id",$provider->id)->first();
					$balance=$account->current_credit_balance;
					
				}else{
					$profile=Providerprofile::where("provider_id",$provider->id)->first();
					$balance=$profile->current_credit_balance;
					
				}

		return $balance;

		
	}

	

	//set online status of provider if he is able to receive the job
	protected function ReadyforJob($request){

		if($request->input('phone')=='' || $request->input('latitude')=='' || $request->input('longitude')=='' || $request->input('status')==''){
		return false;
		}
		
		$phone = $request->input('phone');
		$status = $request->input('status');
		$latitude = $request->input('latitude');
		$longitude = $request->input('longitude');
		
		$provider=Provider::where('phone',$phone)->first();
		$provider->ready_to_serve=$status;
		$provider->is_online=$status;
		$provider->current_latitude=$latitude;
		$provider->current_longitude=$longitude;
		$provider->save();

		$timehistory=Providertimehistory::where("provider_id",$provider->id)->whereDate("date",date("Y-m-d"))->first();
		if(!$timehistory){
			$timehistory=new Providertimehistory();
			$timehistory->provider_id=$provider->id;
			$timehistory->save();
		}


		if($timehistory && $status=='1'){

						//default received worktime
						
							$worktime=new Providerworktime;
							$worktime->provider_id=$provider->id;
							$worktime->provider_timehistory_id=$timehistory->id;
							$worktime->from_time=date("Y-m-d H:i:s");
							$worktime->to_time=date("Y-m-d H:i:s");
							//$worktime->service_id=$service->id;
							$worktime->date=date("Y-m-d H:i:s");
							$worktime->type='online';
							$worktime->save();
					}
		if($timehistory && $status=='0'){
			
			$worktime=Providerworktime::where('provider_id',$provider->id)
			->whereDate('date',date("Y-m-d"))->where("type","online")
			->whereRaw("from_time=to_time")
			->orderby("from_time","desc")->first();
							if($worktime){
								$worktime->to_time=date("Y-m-d H:i:s");
								$worktime->save();
								}
		}
	

	}

	//create dispute report to a customer
	protected function DisputeCustomer($request){
			$service=Service::where("id",$request->input('service_id'))->first();

			$count=Dispute::where('service_id',$service->id)->where('dispute_to','customer')->count();
			if($count<=0 && $provider->id==$service->provider_id)
			{
			$phone = $request->input('phone');
			$provider=Provider::where('phone',$phone)->first();
			$customer=Customer::with("Profile")->where('phone',$service->customer_id)->first();
			$dispute=new Dispute;
			$dispute->service_id=$request->input('service_id');
			$dispute->customer_id=$service->customer_id;
			$dispute->provider_id=$provider->id;
			$dispute->date=date("Y-m-d H:i:s");
			$dispute->details=$request->input('details');
			$dispute->status=0;
			$dispute->dispute_to='customer';
			$dispute->save();

			if($customer->device_id!=''){
					$arr = array('type'=>'dispute','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
					$status= $this->sendProviderFCM("Job is in dispute!",$arr,$customer->device_id);

				}
				
			}
			else
			{
				return false;
			}
		
		return true;

		}

	//update location for live track
	protected function updateLocation($request){
		if( $request->input('latitude')=='' || $request->input('longitude')=='' || $request->input('phone')==''){
		return false;
		}
		
		$phone = $request->input('phone');
		$latitude = $request->input('latitude');
		$longitude = $request->input('longitude');
		
		
		
		$provider=Provider::with("Profile")->where('phone',$phone)->first();
		$provider->current_latitude=$latitude;
		$provider->current_longitude=$longitude;
		$provider->save();
		Log::info('Pid :'.$provider->id." Lat:".$latitude." Lon: ".$longitude);
		
		$service=Service::whereRaw("(service_status_id=5 or service_status_id=9) and provider_id=".$provider->id)->orderby("id","desc")->first();

		
		
		
		if($provider && $service) 
		{
			

			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
			
			$buffer_time=(int)$buffer_time->value;

			$customer=Customer::with("Profile")->where("id",$service->customer_id)->first();

			//duration between service and provider...provider name and image with rating
			/*$mode=($provider->profile->transport=='walk')?'walking':'driving';
			$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
			$origin='origins='.$latitude.','.$longitude;
			$destination='&destinations='.$service->latitude.','.$service->longitude;
			
			
			$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
			$data=json_decode(($data),true);
			if(!isset($data['rows'][0]['elements'][0]['duration'])) return false;

			$duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60); */
			
			$type=Servicetype::where("id",$service->type_id)->first();
			
			$rating=ProviderRatings::selectRaw('AVG(rate_value) as rating, count(*) as ratecount')->where('provider_id',$provider->id)->get();
			$arr = array('type'=>'livetrack',"id"=>$service->id,"provider_name"=>$provider->name,"latitude"=>$latitude,"longitude"=>$longitude,
			"customer_name"=>$customer->profile->name,"transport"=>$provider->profile->transport,
			"provider_image"=>$provider->profile->image,"provider_rating"=>$rating,"service_type"=>$type->name);
			

			if($customer->device_type=='ios' && $customer->device_id!=''){
		
			
			$status= $this->sendCustomerAPNS("Live Track",$arr,$customer->device_id);
			
			}elseif($customer->device_id!='' ){

			$status= $this->sendCustomerFCM("Live Track",$arr,$customer->device_id);

			}
			
			
		
			return $arr;
		}
		
		
		return false;
	}

	
	

	//get different notification sent to provider from customer
	protected function providerNotification($request){
		$phone = $request->input('phone');
		$type = $request->input('invite_type');
		$status = $request->input('status');
		$date = $request->input('date');
		$company = $request->input('company');
		$service_id = $request->input('service_id');
		
		$provider=Provider::where('phone',$phone)->first();
		$notice=Providernotification::where('provider_id',$provider->id)->orderBy('id', 'desc')->skip(0)->take(1);

		if($notice)
		{
			if($type!='')
			$notice=$notice->where("invite_status",$type);

			if($status!='')
			$notice=$notice->where("service_notice_status",$status);

			if($date!='')
			$notice=$notice->whereDate("date",$date);

			if($company!='')
			$notice=$notice->where("company_id",$company);

			if($service_id!='')
			$notice=$notice->where("service_id",$service_id);
			
			$notice=$notice->get();
			return $notice;
		}
		return false;
		

	}

	//using this function provider join a company
	protected function providerCompanyAccept($request){

		if($request->input('company_id')=='' || $request->input('phone')==''){
		return false;
		}
		
		$phone = $request->input('phone');
		$company = $request->input('company_id');
		
		$provider=Provider::where('phone',$phone)->first();
		$tie=CompanyProvider::where("provider_id",$provider->id)->count();
		
		if($provider && ($tie<=0))
		{
			$cpro=new CompanyProvider;
			$cpro->provider_id=$provider->id;
			$cpro->company_id=$company;
			$cpro->tied_on=date("Y-m-d H:i:s");
			$cpro->save();
			return true;
		}
		
		
		return false;
	}

	//send notification to customer
	private	function sendCustomerFCM($title,$body,$id) {
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
				'to' => $id,
				/*'notification' => array (
						"body" => ($body),
						"title" => $title,
						"icon" => "myicon"
				),*/
				'data'=>$body
		);

		

		$fields = json_encode ( $fields );
		$headers = array (
				'Authorization: key=' . env('FCM_KEY',"AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg"),
				'Content-Type: application/json'
		);
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		return $result;
		}


	private function sendCustomerAPNS($title,$body,$id){

		// Put your device token here (without spaces):
		$deviceToken = $id;
		// Put your private key's passphrase here:
		$passphrase = env('APNS_KEY',"xxxxxxx");
		// Put your alert message here:
		$message = $body;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck_file.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'body' => $message,
				'action-loc-key' => $title,
			),
			'badge' => 2,
			'sound' => 'oven.caf',
			);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		if (!$result)
			return false;
		else
			return $result;
		// Close the connection to the server
		fclose($fp);

	}

	//get the cancel reason dropdown
	protected function CancelReason(){

			$reason=Cancelreason::where("is_active",1)->get();
            
			return $reason;
	}

	//update device id for provider to send notification
	protected function providerDeviceUpdate($request){
			$phone = $request->input('phone');
			
			$provider=Provider::where('phone',$phone)->where('sms_verified',1)->where('is_active',1)->first();

			if($provider){

				$provider->device_id=$request->input('device_id');
				$provider->save();
				return true;
			}

			return false;

		}

	protected function sendProviderMessage(Request $request){

		if($request->input('message_type')=='' || $request->input('email')=='' || $request->input('message')=='' || $request->input('phone')==''){
		return false;
		}

		$provider=Provider::where('phone',$request->phone)->first();
        if($provider)
        {
		$message=new ProviderMessage();
		$message->provider_id=$provider->id;
		$message->subject=$request->input('message_type');
		$message->message_type=$request->input('message_type');
		$message->email=$request->input('email');
		$message->message=$request->input('message');
		$message->save();

		return true;

		}
		else
		return false;

	}

	protected function providerBaselocationUpdate($request){
		if( $request->input('latitude')=='' || $request->input('longitude')=='' || $request->input('phone')==''){
		return false;
		}
		
		$phone = $request->input('phone');
		$latitude = $request->input('latitude');
		$longitude = $request->input('longitude');
		$provider=Provider::with("Profile")->where('phone',$phone)->first();
		if($provider) 
		{
			$provider->base_latitude=$latitude;
			$provider->base_longitude=$longitude;
			$provider->save();

		
			return true;
		}
		
		
		return false;
	}

	


	 private function getLatlngaddress($lat,$lng)
	  {
		 $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		 $json = @file_get_contents($url);
		 $result=json_decode(($json),true);
		  
		 $status = $result['status'];
		
		 if($status=="OK")
		 {
			 $result=$result['results'];

			 $location = array();

				  foreach ($result[0]['address_components'] as $component) {

					switch ($component['types']) {
					  case in_array('street_number', $component['types']):
						$location['street_number'] = $component['long_name'];
						break;
					  case in_array('route', $component['types']):
						$location['street'] = $component['long_name'];
						break;
					  case in_array('sublocality', $component['types']):
						$location['sublocality'] = $component['long_name'];
						break;
					  case in_array('locality', $component['types']):
						$location['locality'] = $component['long_name'];
						break;
					  case in_array('administrative_area_level_2', $component['types']):
						$location['admin_2'] = $component['long_name'];
						break;
					  case in_array('administrative_area_level_1', $component['types']):
						$location['admin_1'] = $component['long_name'];
						break;
					  case in_array('postal_code', $component['types']):
						$location['postal_code'] = $component['long_name'];
						break;
					  case in_array('country', $component['types']):
						$location['country'] = $component['short_name'];
						break;
					}

				  }
			
			$location['formatted_address']=$result[0]['formatted_address'];
			 //['results']['0']['address_components']['5']['long_name'];
		   //return $data->results[0]->formatted_address;
		   return $location;
		 }
		 else
		 {
		   return false;
		 }
	  }





	  private function getCurrentSchedule($provider,$currentdate=false){


		$datetime = ($currentdate)?date("Y-m-d H:i:s",strtotime($currentdate)):date('Y-m-d H:i:s');
		$date = ($currentdate)?date("Y-m-d",strtotime($currentdate)):date('Y-m-d');
		$currenttime = ($currentdate)?date("H:i:s",strtotime($currentdate)):date('H:i:s');
		

		$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
		->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})

		->where(DB::raw("DATE(book_date)"),"=",$date)->where('to_time',">=",$datetime)->orderby('from_time','asc')->get()->toArray();


		
//print_r($bookedcalendar);exit;
		

		$freetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",$date)
		->where('to_time',">=",$currenttime)
		
		->orderby('from_time','asc')->first();

		$schedule=array();

		if(count($bookedcalendar)>0){

			//find all free time before booking
			if($bookedcalendar[0]['from_time']>$datetime){

				

					$freetime=Providerfreetime::
					
					whereRaw(" `provider_id` = ".$provider->id." and DATE(date) = '".$date."' and to_time between '".$currenttime."' and
					'".date("H:i:s",strtotime($bookedcalendar[0]['from_time']))."' or ( `provider_id` = ".$provider->id." and DATE(date) = '".$date."'
					and '".date("H:i:s",strtotime($bookedcalendar[0]['from_time']))."' between from_time and to_time )")
					
					->orderby('from_time','asc')->get();
					

					$loop=0;
					foreach($freetime as $time){

									$from_time=($loop==0 && $currenttime>$time->from_time)?$datetime:$time->date." ".$time->from_time;

									$to_time=$time->date." ".$time->to_time;
									if($to_time>$bookedcalendar[0]['from_time'])
									$to_time=$bookedcalendar[0]['from_time'];

									$duration=round(abs(strtotime($to_time) - strtotime($from_time)) / 60,2);



									if($duration>0)
									$schedule[]['schedule']=array("from_time"=>$from_time,"to_time"=>$to_time,"time_type"=>$time->time_type,"duration"=>$duration,"free_id"=>$time->id);
									$loop++;

								}

			
			
			}



			
			//find booking after first freetime

			/*************************************************Bookings***********************************************************************/
			$i=0;
		foreach($bookedcalendar as $booked){

			
			$buffer_time=0;
			if(isset($booked['service']['type_id']))
			{
			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$booked['service']['type_id'])->first();
			
			$buffer_time=(int)$buffer_time->value;
			}



			// if its first schedule
			if( $i==0){
			
				if(($booked['service']['estimated_traveltime_heading']=='' || $booked['service']['estimated_traveltime_heading']=='0')){
				//get duration between current and first appointment
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
				$destination='&destinations='.$booked['service']['latitude'].','.$booked['service']['longitude'];
				
				$data=$this->httpGetGoogle($url.$origin.$destination."&mode=driving&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
				}else{

				$estimated_traveltime_heading=$booked['service']['estimated_traveltime_heading'];
				}

				$travel_start_time=$booked['service']['from_time'];
			
			//fetch heading time
			}elseif($booked['service']['estimated_traveltime_heading']!='' &&  $booked['service']['estimated_traveltime_heading']!='0'){

				$estimated_traveltime_heading=$booked['service']['estimated_traveltime_heading'];



			//update heading time
			}elseif(($booked['service']['estimated_traveltime_heading']=='' || $booked['service']['estimated_traveltime_heading']=='0')

			&& $i>0 && isset($booked['service']['type_id'])){

				$source_latitude=$bookedcalendar[$i-1]['service']['latitude'];
				$source_longitude=$bookedcalendar[$i-1]['service']['longitude'];

				$destination_latitude=$booked['service']['latitude'];
				$destination_longitude=$booked['service']['longitude'];


				//get duration between current and first appointment
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$source_latitude.','.$source_longitude;
				$destination='&destinations='.$destination_latitude.','.$destination_longitude;
				
				$data=$this->httpGetGoogle($url.$origin.$destination."&mode=driving&key=". env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;

				$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
				
				
			}
			if($i>0 && isset($booked['service']['type_id'])){
				$prevschedule=end($schedule);
				//if no workend time means lastone is free slot
				$travel_start_time=isset($prevschedule['schedule']['work_end_time'])?$prevschedule['schedule']['work_end_time']:$prevschedule['schedule']['to_time'];
			}

			
			if(isset($booked['service']['type_id'])){
			$service=Service::where('id',$booked['service']['id'])->first();
			if($service->estimated_traveltime_heading=='' || $service->estimated_traveltime_heading=='0')
				{
				$service->estimated_traveltime_heading=$estimated_traveltime_heading;
				$service->save();
				}
			}

			$work_duration=$booked['service']['job_duration_set_by_provider'];
			
			//$to_time=$booked['service']['to_time'];
			//$from_time=$booked['service']['from_time'];
			

			/*
			 * This calculation already made at accpet of booking so not required
			 $from_time=date("Y-m-d H:i:s", strtotime('+'.($buffer_time+$estimated_traveltime_heading).' minutes', strtotime($from_time)));
			$to_time=date("Y-m-d H:i:s", strtotime('+'.($work_duration).' minutes', strtotime($from_time)));
			*/
			$from_time=$booked['from_time'];
			$to_time=$booked['to_time'];




/*************************************************Freeslot between ***********************************************************************/
		

			//find free slots between
			$lastloop=count($schedule)-1;
			if(isset($schedule[$lastloop]['schedule']['work_end_time'])){
				
				$free_from_time=$schedule[$lastloop]['schedule']['work_end_time'];
				$free_to_time=$from_time;
				
				$duration=round(abs(strtotime($free_to_time) - strtotime($free_from_time)) / 60,2);

				if($free_from_time<$free_to_time){
				$betweenfreetime=Providerfreetime::whereRaw("('".date("H:i:s",strtotime($free_from_time)) ."' between from_time and to_time
					and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d",strtotime($booked['to_time']))."')
					OR  ('".date("H:i:s",strtotime($free_to_time))."' between from_time and to_time and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d",strtotime($booked['to_time']))."')
					OR( from_time >='".date("H:i:s",strtotime($free_from_time))."' and to_time<='".date("H:i:s",strtotime($free_to_time))."' and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d",strtotime($booked['to_time']))."')

					")->orderby('from_time','asc')->get()->toArray();
	
				if(count($betweenfreetime)==1 && $duration>0){
					$timetype=$betweenfreetime[0]['time_type'];
					$schedule[]['schedule']=array("from_time"=>$free_from_time,"to_time"=>$free_to_time,"time_type"=>$timetype,
					"duration"=>$duration,"free_id"=>$betweenfreetime[0]['id']);

				}
				elseif(count($betweenfreetime)>1){

					for($tloop=0;$tloop<count($betweenfreetime);$tloop++){
						
						$frto_time=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['to_time'];
						$timetype=$betweenfreetime[$tloop]['time_type'];
						
						if($tloop>0){
							$frfrom_time=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['from_time'];

						}
						if($tloop==count($betweenfreetime)-1){

							
							$frto_time=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['to_time'];

							
						}
						if($tloop==0){
							$frfrom_time=$free_from_time;
						}

						$duration=round(abs(strtotime($frto_time) - strtotime($frfrom_time)) / 60,2);

						$free_to_time2=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['to_time'];
							
							//check if last loop and already have booking then dont show this slot
							if($free_to_time2>$booked['service']['to_time'])
							continue;

						if($duration>0)
						$schedule[]['schedule']=array("from_time"=>$frfrom_time,"to_time"=>$frto_time,"time_type"=>$timetype,
						"duration"=>$duration,"free_id"=>$betweenfreetime[$tloop]['id']);
					}

				}

				}
			}
			/*************************************************Freeslot between End***********************************************************************/
			if($booked['service']['service_status_id']==5){
			$estimated_traveltime_heading=$booked['service']['total_traveltime'];
			}


				$schedule[]['schedule']=array("from_time"=>$booked['service']['from_time'],"to_time"=>$booked['service']['to_time'],"work_start_time"=>$from_time,"work_end_time"=>$to_time,"service_status_id"=>$booked['service']['service_status_id'],
			"work_duration"=>$work_duration,"heading_time"=>$estimated_traveltime_heading,'time_type'=>'booked',"service_id"=>$booked['service']['id'],"job_id"=>$booked['service']['job_id'],"shorttime"=>"false","can_attend"=>(($booked['service']['short_duration'])=='1')?'false':'true');
			
		$i++;

			
		
		}
//exit;	
		/*************************************************Bookings End***********************************************************************/




			//detect short time and mark
			for($i=0;$i<count($schedule);$i++){

				$previous_schedule=array();

				for($j=$i-1;$j>=0;$j--){
						if(isset($schedule[$j]['schedule']['from_time']) && isset($schedule[$j]['schedule']['service_id'])){
							$previous_schedule=$schedule[$j];
							break;

						}

				}
				
				
				if(isset($schedule[$i]['schedule']['work_end_time'])
				//&& isset($schedule[$i-1]['schedule']['from_time'])
				//&& isset($schedule[$i-1]['schedule']['service_id'])
				&& count($previous_schedule)>0
				&& isset($schedule[$i]['schedule']['service_id']))
				{
					
					//if previous actual_to_time > current start time (red symbol)
					//and current end time > next heading time
				
					
					if(isset($previous_schedule['schedule']['work_end_time'])){

						$actual_next_heading_time=date("Y-m-d H:i:s", strtotime('-'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($schedule[$i]['schedule']["work_start_time"])));
						//$nextschedule=$schedule[$i+1];
						
						//$next_heading_time=$nextschedule['schedule']['from_time'];
						
						
					}
					//echo $previous_schedule['schedule']['work_end_time'];
					//echo $actual_next_heading_time;
					//echo "-------------<br />";
					
					//if current schedule work start time > arrival end time then schedule should not be booked.
					
					if(isset($previous_schedule['schedule']['work_end_time']) &&
					$previous_schedule['schedule']['work_end_time']>=$actual_next_heading_time
						//&& $schedule[$i]['schedule']['work_end_time'] > $next_heading_time
					){
						//print_r($previous_schedule);
						
						$schedule[$i]['schedule']["shorttime"]="true";

						if(isset($previous_schedule['schedule']['current_work_end_time']))
						$schedule[$i]['schedule']["current_work_start_time"]=date("Y-m-d H:i:s", strtotime('+'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($previous_schedule['schedule']['current_work_end_time'])));
						else
						$schedule[$i]['schedule']["current_work_start_time"]=date("Y-m-d H:i:s", strtotime('+'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($previous_schedule['schedule']['work_end_time'])));

						$schedule[$i]['schedule']["current_work_end_time"]=date("Y-m-d H:i:s", strtotime('+'.$work_duration.' minutes', strtotime($schedule[$i]['schedule']["current_work_start_time"])));
					}


				}
				//check schedule before free slot
				elseif(isset($schedule[$i]['schedule']['work_end_time'])
				&& isset($schedule[$i-2]['schedule']['from_time'])
				&& isset($schedule[$i-2]['schedule']['service_id'])
				&& isset($schedule[$i]['schedule']['service_id']))
				{
					

					if(isset($schedule[$i-2]['schedule']['work_end_time'])){
						
						$actual_next_heading_time=date("Y-m-d H:i:s", strtotime('-'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($schedule[$i]['schedule']["work_start_time"])));;
						
						
						
					}

					//if current schedule work start time > arrival end time then schedule should not be booked.
					
					if(isset($schedule[$i-2]['schedule']['work_end_time']) 
					&& $schedule[$i-2]['schedule']['work_end_time']>=$actual_next_heading_time
						
					){
						
						$schedule[$i]['schedule']["shorttime"]="true";
						$schedule[$i]['schedule']["current_work_start_time"]=date("Y-m-d H:i:s", strtotime('+'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($schedule[$i-2]['schedule']['work_end_time'])));

						$schedule[$i]['schedule']["current_work_end_time"]=date("Y-m-d H:i:s", strtotime('+'.$work_duration.' minutes', strtotime($schedule[$i]['schedule']["current_work_start_time"])));
					}

				}
				

			}


//print_r($schedule);
//exit;
/*************************************************************************************************************************************/



			

							$lastschedule=end($schedule);
							//get all next free or unavailable slots
						$nextfreetime=Providerfreetime::
						whereRaw(" ( '".date("H:i:s",strtotime($lastschedule['schedule']['work_end_time']))."' between from_time and to_time  and
						provider_id='".$provider->id."' and ".DB::raw("DATE(date)")." = '".date("Y-m-d",strtotime($lastschedule['schedule']['to_time']))."')
						OR (to_time>= '".date("H:i:s",strtotime($lastschedule['schedule']['work_end_time']))."' and
						provider_id='".$provider->id."' and ".DB::raw("DATE(date)")." = '".date("Y-m-d",strtotime($lastschedule['schedule']['to_time']))."')	
								
								")
								
								
								->orderby('from_time','asc')->get();
								$loop=0;
								foreach($nextfreetime as $time){
									$toworktime=($lastschedule['schedule']['work_end_time']);
									
									if(isset($lastschedule['schedule']['current_work_end_time'])){
										$toworktime=$lastschedule['schedule']['current_work_end_time'];
										}

									$fromfreetime=$time->date." ".$time->from_time;

									
									if($loop==0 && $toworktime>$fromfreetime)
									$fromfreetime=$toworktime;
									$schedule[]['schedule']=array("from_time"=>$fromfreetime,"to_time"=>$time->date." ".$time->to_time,"time_type"=>$time->time_type,"duration"=>$time->total_duration,"free_id"=>$time->id);
									$loop++;
								}


			




			

		}elseif($freetime){

		
			///this condition falls on no-booking
			
			if($datetime>$freetime->date." ".$freetime->from_time)
			$from_time = $datetime;
			else
			$from_time = $freetime->date." ".$freetime->from_time;

			$to_time = ($freetime->date." ".$freetime->to_time);


			$duration=round(abs(strtotime($to_time) - strtotime($from_time)) / 60,2);
			
			$schedule[]['schedule']=array("from_time"=>$from_time,"to_time"=>$to_time,"duration"=>$duration,"time_type"=>$freetime->time_type);

						
								$nextfreetime=Providerfreetime::where('from_time','>=', $to_time)
								->where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",date("Y-m-d",strtotime($to_time)))
								->orderby('from_time','asc')->get();

								foreach($nextfreetime as $time){
									$schedule[]['schedule']=array("from_time"=>$time->date." ".$time->from_time,"to_time"=>$time->date." ".$time->to_time,"time_type"=>$time->time_type,"duration"=>$time->total_duration,"free_id"=>$time->id);

								}

								



		}


		return $schedule;
	  }


	//this function will count worktime of provider and deduct overlapped worktime is using in current trait
  protected function getDuration($id){
		$pastdate=date('Y-m-d H:i:s', strtotime('-500 hours', time()));
		$data=Providerworktime::where("provider_id",$id)
				
					->select("id","from_time","to_time")->whereRaw(" from_time is not null and to_time is not null and date >='".$pastdate."'")->orderby('from_time','asc')
					->get()->unique('from_time','to_time');
		$duration=0;
		if(count($data)>0){
						
					$i=0;
					$data=$data->toArray();

					
					$i=0;
					$totcount=count($data);
					foreach($data as $slot){
						
							$to_time=$slot['to_time'];
							$from_time=$slot['from_time'];

						
						for($j=$i+1;$j<($totcount);$j++){
							
							
								if( isset($data[$j]) && $from_time<=$data[$j]['from_time']  && $to_time>= $data[$j]['to_time'] )
								{
									
									unset($data[$j]);
									

								}
								

						}
						
						$i++;
					}

					
					$data=array_values($data);
				


					$i=0;
					foreach($data as $slot){
						if(isset($data[$i+1]) && $data[$i+1]['from_time']<=$slot['to_time'] && $data[$i+1]['to_time']<=$slot['to_time'])
							{
								
								unset($data[$i+1]);
								//break;
							}
						
							$i++;
					}
					$data=array_values($data);
					//print_r($data);
					
					
					$i=0;
					foreach($data as $slot){
							
							if(isset($data[$i+1]) && $slot['to_time'] >= $data[$i+1]['from_time'] && $data[$i+1]['to_time']>$slot['to_time'])
							{
								$data[$i+1]['from_time']=$slot['to_time'];
								
							}
						
					$i++;
					}
				//print_r($data);
					$duration=0;

					foreach($data as $slot){
						

						$duration +=strtotime($slot['to_time'])-strtotime($slot['from_time']);
					}
					$duration=(round(abs($duration ) / 60,2));

		}

		return $duration;

  }


  //this function will count worktime of provider and deduct overlapped worktime is using in -- cron job
  protected function getworkDuration($id){
		$pastdate=date('Y-m-d H:i:s', strtotime('-500 hours', time()));
		$data=Providerworktime::where("provider_id",$id)
				
					->select("id","from_time","to_time")->whereRaw(" from_time is not null and to_time is not null and date >='".$pastdate."'")->orderby('from_time','asc')
					->get()->unique('from_time','to_time');
		$duration=0;
		if(count($data)>0){
						
					$i=0;
					$data=$data->toArray();

					
					$i=0;
					$totcount=count($data);
					foreach($data as $slot){
						
							$to_time=$slot['to_time'];
							$from_time=$slot['from_time'];

						
						for($j=$i+1;$j<($totcount);$j++){
							
							
								if( isset($data[$j]) && $from_time<=$data[$j]['from_time']  && $to_time>= $data[$j]['to_time'] )
								{
									
									unset($data[$j]);
									

								}
								

						}
						
						$i++;
					}

					
					$data=array_values($data);
				


					$i=0;
					foreach($data as $slot){
						if(isset($data[$i+1]) && $data[$i+1]['from_time']<=$slot['to_time'] && $data[$i+1]['to_time']<=$slot['to_time'])
							{
								
								unset($data[$i+1]);
								//break;
							}
						
							$i++;
					}
					$data=array_values($data);
					//print_r($data);
					
					
					$i=0;
					foreach($data as $slot){
							
							if(isset($data[$i+1]) && $slot['to_time'] >= $data[$i+1]['from_time'] && $data[$i+1]['to_time']>$slot['to_time'])
							{
								$data[$i+1]['from_time']=$slot['to_time'];
								
							}
						
					$i++;
					}
				//print_r($data);
					$duration=0;

					foreach($data as $slot){
						

						$duration +=strtotime($slot['to_time'])-strtotime($slot['from_time']);
					}
					$duration=(round(abs($duration ) / 3600,2));

		}

		return $duration;

  }  



 protected function updateProviderService($proid){

			
		$pid=0;
		$count=0;
		//update heading if provider "must head now" or passed heading time
        $services=Service::whereRaw(' ( service_status_id=4 OR service_status_id=5 OR service_status_id=9)')
		->whereDate("from_time",date("Y-m-d"))
		->where("provider_id",$proid)
        ->orderby("from_time","asc")->get();
		
		foreach($services as $service ){
		
		$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
		$buffer_time=(int)$buffer_time->value;
		$provider=Provider::with("Profile")->where('id',$service->provider_id)->first();
		$booked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();



		

		/***************************************************************************************************************************************/

		
		
		if($service->service_status_id==4 && $provider->is_online=='1')
		{
		if($pid!=$provider->id)
		$count=0;
			
					$booked=Providerbookedtime::with("Service")->where('provider_id',$provider->id)
					//->where('from_time','<',date("Y-m-d H:s:i"))
					->where('id',$service->booking_slot_id)->first();


					
				if($booked)
				{	
							if(time()>strtotime($booked->to_time))
							{
								$free=Providerfreetime::whereRaw("('".date("H:i:s") ."' between from_time and to_time
									and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d")."')	")->first();
								if($free && $free->time_type=='free')
								$provider->provider_status='free';
								
								if($free && $free->time_type=='unavailable')
								$provider->provider_status='unavailable';

								$provider->save();
							}





							$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)
							->where('service_notice_status','arrivaldeadline')->count();

							$alert_time=strtotime('- '.(15+$booked->service->estimated_traveltime_heading).' minutes', strtotime($booked->service->to_time));

				
									//send notification to reach the job to provider before 15 mins
									if($notice<=0 && $alert_time<time()){


										$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
										
										$msg="Possible Delay Is Expected for Appointment ".$service->job_id.", please review your schedule and make sure to arrive there before ".$booked->from_time;
										$time=strtotime('- '.($booked->service->estimated_traveltime_heading).' minutes', strtotime($booked->service->to_time));
										
											if($provider->device_type=='ios' && $provider->device_id!=''){
										
												$arr = array('type'=>'arrivaldeadline','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"time"=>$time);
												$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
											
												}
												elseif($provider->device_id!='' )
												{
													$arr = array('type'=>'arrivaldeadline','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"time"=>$time);
													$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);

												}


												
												$pnotice=new Providernotification();
												$pnotice->provider_id=$provider->id;
												$pnotice->service_id=$service->id;
												$pnotice->date=date('Y-m-d H:i:s');
												$pnotice->details="Possible Delay Is Expected for Appointment ".$service->job_id.", please review your schedule and make sure to arrive there before ".$service->to_time;
												$pnotice->notification_latitude=$provider->current_latitude;
												$pnotice->notification_longitude=$provider->current_longitude;
												
												$pnotice->service_notice_status='arrivaldeadline';
												$pnotice->price=$service->expected_cost;
												$pnotice->save();
												

									}







									
							$minutes=(int)$service->estimated_traveltime_heading;
							$head_time=strtotime('-'.$minutes.' minutes', strtotime($booked->from_time));
							
							
							
							if( $head_time<time() ){

								
								
							

								$mode=($provider->profile->transport=='walk')?'walking':'driving';

								$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
								$origin='destinations='.$service->latitude.','.$service->longitude;
								$destination='&origins='.$provider->current_latitude.','.$provider->current_longitude;
								
								$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
								$data=json_decode(($data),true);
								if(!isset($data['rows'][0]['elements'][0]['duration'])) continue;
								
								$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
								$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);
								
								$duration=(int)$duration+(int)$buffer_time;
								 $work_start_time=date("Y-m-d H:i:s", strtotime('+'.($duration).' minutes', time()));
								 $work_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($work_start_time)));
								//$headtime=date("Y-m-d H:i:s", strtotime('-'.$service->estimated_traveltime_heading.' minutes',strtotime($booked->from_time)));
								
								 $freeslot=Providerfreetime::where('provider_id',$provider->id)
									->where('from_time','<=',date("H:i:s",strtotime($work_start_time)))
									->where('to_time','>=',date("H:i:s",strtotime($work_end_time)))
									->where('date','=',date("Y-m-d",strtotime($work_end_time)))
									->where('time_type','free')->count()
										
									;

								//print_r( $freeslot);exit;
								//echo $work_start_time ;
								//echo $booked->service->to_time;
								//exit;
								
								if($work_start_time>=$booked->service->to_time && $work_start_time>$booked->service->from_time){
									$singleservice=Service::where("id",$service->id)->first();
									$singleservice->short_duration=1;
									$singleservice->save();

								}
									
								//change booking slot time if free slot	
								if($freeslot>0 && $work_start_time<$booked->service->to_time && $work_start_time>$booked->service->from_time && $count==0){

									//echo date("H:i:s",$head_time);
									//echo date("H:i:s",time());
									//echo $booked->from_time;
									//echo $service->id;
									//echo '-------------<br />';
							

									$pid=$provider->id;
									
									$old_head_start_time=$booked->heading_start_time;
									$booked->heading_start_time=date("Y-m-d H:i:s", strtotime('-'.$duration.' minutes', strtotime($work_start_time)));
									
									$old_to_time=$booked->to_time;
									$old_from_time=$booked->from_time;
									
									$booked->from_time=$work_start_time;
									$booked->to_time=$work_end_time;
									$booked->save();


									//$singleservice=Service::where("id",$service->id)->first();
									//$old_estimated_traveltime_heading=$singleservice->estimated_traveltime_heading;
									//$singleservice->estimated_traveltime_heading=$duration;
									//$singleservice->save();
									
									//if next all service can reschedule then update current service else revert to previous time
									$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);
									if($status==false){
									//	echo "test";exit;
									$booked->heading_start_time=$old_head_start_time;
									$booked->from_time=$old_from_time;
									$booked->to_time=$old_to_time;
									$booked->save();
									
									//$singleservice->estimated_traveltime_heading=$old_estimated_traveltime_heading;
									//$singleservice->save();

									}

									$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
									->where(DB::raw("DATE(book_date)"),"=",date("Y-m-d",strtotime($booked->to_time)))
									->where('from_time',">",$booked->from_time)->orderby('from_time','asc')->first();
									
									$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)->where('service_notice_status','must head now')->count();

				
									//send notification to start heading the job to provider
									if($notice<=0 ){


										$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
										$msg="It’s Time to Head to ".$customer->profile->name." for Appointment ".$service->job_id."! ";
										
											if($provider->device_type=='ios' && $provider->device_id!=''){
										
												$arr = array('type'=>'mustheadnow','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"address"=>$service->area_name);
												$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
											
												}
												elseif($provider->device_id!='' )
												{
													$arr = array('type'=>'mustheadnow','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"address"=>$service->area_name);
													$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);

												}


												
												$pnotice=new Providernotification();
												$pnotice->provider_id=$provider->id;
												$pnotice->service_id=$service->id;
												$pnotice->date=date('Y-m-d H:i:s');
												$pnotice->details='Start heading soon';
												$pnotice->notification_latitude=$provider->current_latitude;
												$pnotice->notification_longitude=$provider->current_longitude;
												
												$pnotice->service_notice_status='must head now';
												$pnotice->price=$service->expected_cost;
												$pnotice->save();
												

									}


								$count++;				
								}
								
								
							}
				}
			}
			elseif($service->service_status_id==5 || $service->service_status_id==9)
			{

				
							if($booked && date("H:i")>date("H:i",strtotime($booked->to_time)) ){
								$booked->to_time=date("Y-m-d H:i:s");
								$booked->total_duration=round((strtotime($booked->to_time)-strtotime($booked->from_time))/60);
								$booked->save();
								$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);
							}

					

					///if time already passed and status is still on heading or working then set the provider status
					if($booked && time()>strtotime($booked->to_time))
					{
						$free=Providerfreetime::whereRaw("('".date("H:i:s") ."' between from_time and to_time
							and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d")."')	")->first();
						if($free && $free->time_type=='free')
						$provider->provider_status='free';
						
						if($free && $free->time_type=='unavailable')
						$provider->provider_status='unavailable';

						
					}
					elseif($service->service_status_id==5)
						{

							$provider->provider_status='working';
							$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();

							$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)->where('service_notice_status','endsoon')->count();

				
							//send notification to end the job to provider
							if($booked && date("H:i")>=date("H:i",strtotime('-5 minutes',strtotime($booked->to_time))) && $notice<=0 ){

								$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();

								

				
								if($provider->device_type=='ios' && $provider->device_id!=''){
							
									$arr = array('type'=>'endsoon','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
									$status= $this->sendCustomerAPNS("Appoinment will end soon!",$arr,$provider->device_id);
								
									}elseif($provider->device_id!='' ){
										$arr = array('type'=>'endsoon','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
										$status= $this->sendCustomerFCM("Appoinment will end soon!",$arr,$provider->device_id);

									}


									$pnotice=new Providernotification();
									$pnotice->provider_id=$provider->id;
									$pnotice->service_id=$service->id;
									$pnotice->date=date('Y-m-d H:i:s');
									$pnotice->details='Job will end soon';
									$pnotice->notification_latitude=$provider->current_latitude;
									$pnotice->notification_longitude=$provider->current_longitude;
									
									$pnotice->service_notice_status='endsoon';
									$pnotice->price=$service->expected_cost;
									$pnotice->save();
						
								
							}

							
						}
						elseif($service->service_status_id==9)
						{

							$provider->provider_status='heading';
						}


					$provider->save();

			


		}

		
		}





 }


		
 }
