<?php
/****************************************** For Customer  App, Registration, ServiceController and Home Controller ***********************************/

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Response;
use Session;
use Hash;

use App\Models\Customer;
use App\Models\Customerprofile;
use App\Models\CustomerCancelHistory;
use App\Models\Provider;
use App\Models\CompanyProvider;
use App\Models\ProviderCompanyAccount;
use App\Models\Servicetype;
use App\Models\Service;
use App\Models\Providerworktime;
use App\Models\Servicestatus;
use App\Models\Servicesettings;
use App\Models\Providerbookedtime;
use App\Models\Providerfreetime;
use App\Models\ProviderRatings;
use App\Models\Dispute;
use App\Models\Customernotification;
use App\Models\Providernotification;
use App\Models\Providertimehistory;
use App\Models\Providerprofile;
use App\Models\Servicecardetails;
use App\Models\Serviceoptiondetails;
use App\Models\CustomerMessage;
use App\Models\Smslog;
use App\Models\Providerpreferencelocation;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

trait CustomerServiceTrait {


	
	/***********************  update customer profile after signup *******************************/
	
	protected function updateCustomerProfile($request)
	{

		$phone = $request->input('phone');
		$customer=Customer::with('Profile')->where('phone',$phone)->first();
		$new_phone = $request->input('new_phone');
		$newcustcount=Customer::where('phone',$new_phone)->count();
		
		if(!empty($new_phone) && $newcustcount<=0)
		{
				$customer->phone=$new_phone;
				$customer->sms_verified=0;
		}
		elseif(!empty($new_phone) && $newcustcount>0)
		{
			return false;
		}
		
		$customer->email=$request->input("email");
		
		$customer->save();


		
		$profile=Customerprofile::where('customer_id',$customer->id)->first();
		$profile->name=$request->input("name");
		
		$profile->age=$request->input("birth_year");

		 $picture = $profile->image;
        if ($request->hasFile('profile_Image')) {
            $file = $request->file('profile_Image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            $profile->image = "media/customer/".$picture;
        }
        
		
		if ($request->hasFile('profile_Image')) {
            
            $destinationPath = public_path() . '/media/customer/' ;
            $request->file('profile_Image')->move($destinationPath, $picture);

        }
		//$profile->annual_income=$request->annual_income;
		$profile->save();

		if(!empty($new_phone)){

			


			return array("phone"=>1,"success"=>0);
		}
		else
		{
			return array("phone"=>0,"success"=>0);
		}

	}

	//store address from latitude and longitude in database to display in app for service
	 private function getaddress($lat,$lng)
	  {
		 $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		 $json = @file_get_contents($url);
		 $result=json_decode(($json),true);
		  
		 $status = $result['status'];
		
		 if($status=="OK")
		 {
			 $result=$result['results'];

			 $location = array();

				  foreach ($result[0]['address_components'] as $component)
				  {

					switch ($component['types']) {
					  case in_array('street_number', $component['types']):
						$location['street_number'] = $component['long_name'];
						break;
					  case in_array('route', $component['types']):
						$location['street'] = $component['long_name'];
						break;
					  case in_array('sublocality', $component['types']):
						$location['sublocality'] = $component['long_name'];
						break;
					  case in_array('locality', $component['types']):
						$location['locality'] = $component['long_name'];
						break;
					  case in_array('administrative_area_level_2', $component['types']):
						$location['admin_2'] = $component['long_name'];
						break;
					  case in_array('administrative_area_level_1', $component['types']):
						$location['admin_1'] = $component['long_name'];
						break;
					  case in_array('postal_code', $component['types']):
						$location['postal_code'] = $component['long_name'];
						break;
					  case in_array('country', $component['types']):
						$location['country'] = $component['short_name'];
						break;
					}

				  }
			
			$location['formatted_address']=$result[0]['formatted_address'];
			 //['results']['0']['address_components']['5']['long_name'];
		   //return $data->results[0]->formatted_address;
		   return $location;
		 }
		 else
		 {
		   return false;
		 }
	  }

	//When searching provider if exceed max_due_amount will not display in provider search result
    private function CheckIfPaidcust($provider)
    {
		if(!$provider) return false;
		$provider=$provider->toArray();
		$limit=$provider['profile']['max_due_amount'];
		
		$count=CompanyProvider::where("provider_id",$provider['id'])->count();
		if($count>0)
		{
			$account=ProviderCompanyAccount::where("provider_id",$provider['id'])->first();
			$account->current_credit_balance;
			if($account->current_credit_balance>=$limit)
				return false;
			
		}else{

			$balance=$provider['profile']['current_credit_balance'];
			if($balance>=$limit)
			return false;
			
		}

		return true;

	}

	
	/***********************  create appointment or service when send notify request to provider *******************************/
	
    protected function SaveServiceRequest($request)
    {
		$customer=Customer::with("Profile")->where('phone',$request->input('phone'))->first();
		$duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$request->input('type_id'))->first();
		$mincredit=Servicesettings::select('code','value')->where('code','provider_max_due_amount')->where('type_id',$request->input('type_id'))->first();
		$provider=Provider::with('Profile')->where('id',$request->input('provider_id'))->first();

		$default_cancel_duration=Servicesettings::select('code','value')->where('code','provider_wait_duration')->where('type_id',$request->input('type_id'))->first();
		
		$auto_cancel_duration=Servicesettings::select('code','value')->where('code','provider_wait_autocancel_duration')->where('type_id',$request->input('type_id'))->first();

		$cartype=json_decode($request->input('car_type'), true);

		//cant book provider if due is more than limit
		//$providerprofile=Providerprofile::where("provider_id",$provider->id)->first();
		
		if(!$this->CheckIfPaidcust($provider))
		
			return false;
		

		
		
		$inputs=$request->all();

		$service=new Service();
		$service->type_id=$request->input('type_id');
		
		$service->customer_id=$customer->id;
		$service->job_description=$request->input('job_description');
		$service->request_date=date("Y-m-d H:i:s");
		
		$service->longitude=$request->input('longitude');
		$service->latitude=$request->input('latitude');
		//for pickup and delivery
		if($service->type_id==3){
			$service->pickup_latitude=$request->input('pickup_latitude');
			$service->pickup_longitude=$request->input('pickup_longitude');

		}
		
		$address=$this->getaddress($request->input('latitude'),$request->input('longitude'));
		$countrycode='QA';
		if($address)
		{
			
			$service->address=$address['formatted_address'];
			
			$countrycode=$address['country'];
				
		}
		$service->nationality=$countrycode;
		$service->expected_cost=$request->input('expected_cost');


		$jduration=0;
			foreach($cartype as $details){
			
				$stype=1;
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++)
						{

							
							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							$jduration=$jduration+$options['duration'];
							
							
						}

					$stype++;
					}
			

			}
			

		if($jduration < $duration->value)	
		$service->job_duration_set_by_provider=$duration->value;
		else
		$service->job_duration_set_by_provider=$jduration;

		/*if(!$request->input('job_duration_set_by_provider') || $request->input('job_duration_set_by_provider')<$duration->value)
		$service->job_duration_set_by_provider=$duration->value;
		else
		$service->job_duration_set_by_provider=$request->input('job_duration_set_by_provider');*/

		$service->from_time=$request->input('from_time');
		$service->to_time=$request->input('to_time');
		//$service->arrival_witin_time=$request->input('arrival_witin_time');
		$service->job_id = strtoupper($this->generate_uuid());
		
		$service->service_status_id=3;
		//$status=$service->save();

		
		//booking part will go here after service creation
		$service=$this->bookProvider($service,$provider);

		if($service){



			$polygon=Providerpreferencelocation::with('Polygon','Preferencehead')->get();
				$preference_job_area=0;
				$area_name='';
				if($polygon){
					//print_r($polygon->toArray());exit;
					$servicepoint=array("latitude"=>$service->latitude,"longitude"=>$service->longitude);
					$preference_job_area=0;
					foreach($polygon as $points){
					$coordinates=$this->FindpointInPolygon($servicepoint,$points->polygon);
					
						if($coordinates)
							{
								$preference_job_area=$points->preference_id;
								$area_name=$points->preferencehead[0]->name;
								break;

							}
						
						
					}
					$service->preference_job_area=$preference_job_area;
					$service->area_name=$area_name;
				}
				

			$service->service_status_id=1;
			$service->provider_id=$provider->id;
			$service->save();


			if($service && $provider->device_id!='')
			{
				
				
				/*$service_details=Service::with(['Provider'=> function ($query) {
				$query->selectRaw('id,name,device_id');
				
				},'Cardetails','Providerprofile'=> function ($query) {
				$query->selectRaw('provider_profile.provider_id,image,AVG(rate_value) as ratings_average,count(rate_value) as ratings_count')->leftJoin('provider_rating','provider_profile.provider_id','=','provider_rating.provider_id');
				
				}])->where('id',$service->id)->first()->toArray();*/

				

				if($provider->device_type=='ios' && $provider->device_id!=''){
					
						$arr = array('type'=>'jobcreated','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name,
						"current_time"=>date("Y-m-d H:i:s"),"request_time"=>$service->request_date,"default_cancel_duration"=>$default_cancel_duration->value,
						"auto_cancel_duration"=>$auto_cancel_duration->value

						);
						$status= $this->sendProviderAPNS("Your request has reached ".$provider->name.", please hold…",$arr,$provider->device_id);
					
					}elseif($provider->device_id!='' ){

						$arr = array('type'=>'jobcreated','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name,
						"current_time"=>date("Y-m-d H:i:s"),"request_time"=>$service->request_date,"default_cancel_duration"=>$default_cancel_duration->value,
						"auto_cancel_duration"=>$auto_cancel_duration->value
						);
						$status= $this->sendProviderFCM("Your request has reached ".$provider->name.", please hold…",$arr,$provider->device_id);
					}
				
				
			}


		
			/*
			foreach($cartype as $details){
				
					for($i=0;$i<count($details);$i++){
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){
							$car=new Servicecardetails();
							$car->service_id=$service->id;
							$car->number_of_cars=1;
							$car->option_details_id=$details[$i]['option_details'][$x];
							$car->save();
						}

					}
				

			}*/
			
			foreach($cartype as $details)
			{
			
				$stype=1;
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){

							$car=new Servicecardetails();
							$car->service_id=$service->id;
							$car->number_of_cars=1;
							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							
							$car->carwash_type=$options['serviceoption']['name'].$stype;
							$car->option_details_id=$details[$i]['option_details'][$x];
							
							$car->save();
							
						}

					$stype++;
					}
			

			}
		
		

			$pnotice=new Providernotification();
			$pnotice->provider_id=$request->input('provider_id');
			$pnotice->service_id=$service->id;
			$pnotice->date=date('Y-m-d H:i:s');
			$pnotice->details='New job received';
			$pnotice->notification_latitude=$service->latitude;
			$pnotice->notification_longitude=$service->longitude;
			$pnotice->invite_status='sent';
			$pnotice->price=$service->expected_cost;
			$pnotice->save();


			$timehistory=new Providertimehistory();
			$timehistory->provider_id=$request->input('provider_id');
			$timehistory->service_id=$service->id;
			$timehistory->save();
					
			
				if($timehistory)
				{

						//default received worktime
						
							$worktime=new Providerworktime;
							$worktime->provider_id=$request->input('provider_id');
							$worktime->provider_timehistory_id=$timehistory->id;
							$worktime->from_time=date("Y-m-d H:i:s");
							$worktime->to_time=date("Y-m-d H:i:s", strtotime('+2 hours'));
							$worktime->service_id=$service->id;
							$worktime->date=date("Y-m-d H:i:s");
							$worktime->type='jobreceived';
							$worktime->save();
				}
				
			

		
			return $service;
		}
		else
		{
			//$service->delete();
			return false;
		}
		return true;
		
         
		
		
	}

	/***********************  get appointment details through service controller for provider and customer both *******************************/

	
	protected function ServiceDetails($request)
	{
			$service_id=$request->input("service_id");
		$service_details=Service::with(['Provider'=> function ($query) {
				$query->selectRaw('id,name,device_id');
				
				},'Cardetails','Providerprofile'=> function ($query) {
				$query->selectRaw('provider_profile.provider_id,image,AVG(rate_value) as ratings_average,count(rate_value) as ratings_count')
				->leftJoin('provider_rating','provider_profile.provider_id','=','provider_rating.provider_id');
				
				}])->where('id',$service_id)->first()->toArray();



				$caroptions=array();
				foreach($service_details['cardetails'] as $carserv)
				{
					if($carserv['price']>0)
						{
						$carserv['option_details'][0]['cost']=$carserv['price'];
						}
			
							$caroptions[$carserv['carwash_type']][]=$carserv['option_details'];
				
					
				}
				$service_details['cardetails']=$caroptions;
		

				return $service_details;

	}

	/***********************  generate unique job id *******************************/

	
	private function generate_uuid()
	{
			/*return sprintf( '%04x%04x%04x',
				mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
			);*/

			return mt_rand(10000000, 99999999);


	}


	
	/***********************        customer can repeat past service done by provider         *******************************/

	
	protected function RepeatServiceRequest($request)
    {
		$customer=Customer::with("Profile")->where('phone',$request->input('phone'))->first();
		
		
		$inputs=$request->all();

		$oldservice=Service::where("id",$request->input('service_id'))->first();
		$oldservicedetails=Servicecardetails::where("service_id",$request->input('service_id'))->get();
		$duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$oldservice->type_id)->first();

		$provider=Provider::with('Profile')->where('id',$oldservice->provider_id)->first();
		//cant book provider if due is more than limit
		if(!$this->CheckIfPaidcust($provider)){
			return false;
		}
		

		$service=new Service();
		$service->type_id=$oldservice->type_id;
		
		$service->customer_id=$customer->id;
		$service->job_description=$oldservice->job_description;
		$service->request_date=date("Y-m-d H:i:s");
		$service->nationality=$oldservice->nationality;
		$service->longitude=$oldservice->longitude;
		$service->latitude=$oldservice->latitude;
		$service->address=$oldservice->address;
		$service->expected_cost=$oldservice->expected_cost;
		$service->job_duration_set_by_provider=$oldservice->job_duration_set_by_provider;

		$service->from_time=$request->input('from_time');
		$service->to_time=$request->input('to_time');;
		
		$service->job_id = strtoupper($this->generate_uuid());
		
		$service->service_status_id=3;
		//$status=$service->save();

		
		//booking part will go here after service creation
		$service=$this->bookProvider($service,$provider);

		if($service)
		{
			$polygon=Providerpreferencelocation::with('Polygon','Preferencehead')->get();
				$preference_job_area=0;
				$area_name='';
				if($polygon){
					//print_r($polygon->toArray());exit;
					$servicepoint=array("latitude"=>$service->latitude,"longitude"=>$service->longitude);
					$preference_job_area=0;
					foreach($polygon as $points){
					$coordinates=$this->FindpointInPolygon($servicepoint,$points->polygon);
					
						if($coordinates)
							{
								$preference_job_area=$points->preference_id;
								$area_name=$points->preferencehead[0]->name;
								break;

							}
						
						
					}
					$service->preference_job_area=$preference_job_area;
					$service->area_name=$area_name;
				}

			$service->service_status_id=1;
			$service->provider_id=$provider->id;
			$service->save();
			if($service && $provider->device_id!=''){
				

				if($provider->device_type=='ios' && $provider->device_id!=''){
					
						$arr = array('type'=>'jobcreated','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderAPNS("New Job Received!",$arr,$provider->device_id);
					
					}elseif($provider->device_id!='' ){

						$arr = array('type'=>'jobcreated','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderFCM("New Job Received!",$arr,$provider->device_id);
					}
					
				
				}

				
						
						
						foreach($oldservicedetails as $oldcar)
						{

							$car=new Servicecardetails();
							$car->service_id=$service->id;
							$car->number_of_cars=1;
							
							$car->carwash_type=$oldcar->carwash_type;
							$car->option_details_id=$oldcar->option_details_id;;
							
							$car->save();
							
						}

				
			

			$pnotice=new Providernotification();
			$pnotice->provider_id=$oldservice->provider_id;
			$pnotice->service_id=$service->id;
			$pnotice->date=date('Y-m-d H:i:s');
			$pnotice->details='New job received';
			$pnotice->notification_latitude=$service->latitude;
			$pnotice->notification_longitude=$service->longitude;
			$pnotice->invite_status='sent';
			$pnotice->price=$service->expected_cost;
			$pnotice->save();


			$timehistory=new Providertimehistory();
			$timehistory->provider_id=$oldservice->provider_id;
			$timehistory->service_id=$service->id;
			$timehistory->save();
					
			
				if($timehistory)
				{
					

						//default received worktime
						
							$worktime=new Providerworktime;
							$worktime->provider_id=$oldservice->provider_id;
							$worktime->provider_timehistory_id=$timehistory->id;
							$worktime->from_time=date("Y-m-d H:i:s");
							$worktime->to_time=date("Y-m-d H:i:s", strtotime('+2 hours'));
							$worktime->service_id=$service->id;
							$worktime->date=date("Y-m-d H:i:s");
							$worktime->type='jobreceived';
							$worktime->save();
				}
					

			

				
		
			return $service;
		}
		else
		{
			//$service->delete();
			return false;
		}
		return true;
		
         
		
		
	}

	/***********************        customer can update appointment details like cost         *******************************/


 //find if service is within prefered area in polygon
	private function FindpointInPolygon($point, $polygon, $pointOnVertex = true) {
		
        $this->pointOnVertex = $pointOnVertex;
 
        // Transform string coordinates into arrays with x and y values
        $point = $this->FindpointStringToCoordinates($point);
        $vertices = array(); 
        foreach ($polygon as $vertex) {
            $vertices[] = $this->FindpointStringToCoordinates($vertex); 
        }
 
        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->FindpointOnVertex($point, $vertices) == true) {
            return true;
        }
 
        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);
 
        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1]; 
            $vertex2 = $vertices[$i];
            
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) {
				// Check if point is on an horizontal polygon boundary
                return true;
            }
            
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y'])
            { 

                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 

                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return true;
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        // If the number of edges we passed through is odd, then it's in the polygon. 
        if ($intersections % 2 != 0) {
            return true;
        } else {
            return false;
        }
    }
 
	private  function FindpointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
 
    }
	
	private function FindpointStringToCoordinates($pointString) 
    {
            //$coordinates = explode(",", $pointString);
            return array("x" => trim($pointString['latitude']), "y" => trim($pointString['longitude']));
    }

    

	protected function updateBooking($request)
	{

			$phone = $request->input('phone');
			$customer=Customer::with("Profile")->where('phone',$phone)->first();
			$service=Service::where("id",$request->input('service_id'))->first();
			$booked=Providerbookedtime::where('id',$service->booking_slot_id)->first();

			$service->job_description=$request->input('job_description');
			$service->expected_cost=$request->input('expected_cost');
			//$service->from_time=$request->input('from_time');
			//$service->to_time=$request->input('to_time');

			$service->save();

			//$booked->from_time=$request->input('from_time');
			//$booked->to_time=$request->input('to_time');
			

			//$booked->save();
			$provider=Provider::with('Profile')->where('id',$service->provider_id)->first();
			if($service && $provider &&  $provider->device_id!='')
			{
				
					$msg="Appointment ".$service->job_id." Job Details Have Beed Changed by Customer";
					if($provider->device_type=='ios' && $provider->device_id!=''){
					
						$arr = array('type'=>'customerupdateservice','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderAPNS($msg,$arr,$provider->device_id);
					
					}elseif($provider->device_id!='' ){

						$arr = array('type'=>'customerupdateservice','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderFCM($msg,$arr,$provider->device_id);
					}
				
			}
			
			
			return true;
	}

	/***********************       customer can cancel booking and chages may apply     *******************************/
	

	protected function cancelBooking($request)
	{

			$phone = $request->input('phone');
			//$time = json_decode(($request->input('time')), true);
			//print_r($time);exit;
			if($request->input('reason_id')=='' || $request->input('comment')=='' || $request->input('service_id')=='') return false;

			$customer=Customer::with("Profile")->where('phone',$phone)->first();

			$service=Service::where("id",$request->input('service_id'))->where("customer_id",$customer->id)->where('service_status_id','<>',2)->where('service_status_id','<>',6)->first();

			
			if($service)
			{

				$provider=Provider::with('Profile')->where('id',$service->provider_id)->first();

				$confirmednotice=Customernotification::where('service_id',$request->input('service_id'))
				->where('customer_id',$customer->id)->where('service_notice_status','confirmed')->count();
				
				$headnotice=Customernotification::where('service_id',$request->input('service_id'))
				->where('customer_id',$customer->id)->where('service_notice_status','heading')->count();
				
				$startednotice=Customernotification::where('service_id',$request->input('service_id'))
				->where('customer_id',$customer->id)->where('service_notice_status','jobstarted')->count();
				
				if($confirmednotice>0){
				$service->service_status_id=8;
				}
				elseif($headnotice>0){
				$service->service_status_id=10;
				}
				elseif($startednotice>0){
				$service->service_status_id=11;
				}
				else{
				$service->service_status_id=2;
				}
				$id=$service->booking_slot_id;
				$service->booking_slot_id=null;
				$service->save();
				
				if($id)
				{
					//reschedule
					$this->RescheduleProviderBooking($id,$provider,$service);
					
					
					Providerbookedtime::where("id",$id)->delete();
				}
				


				$history=new CustomerCancelHistory();
				$history->customer_id=$customer->id;
				$history->service_id=$service->id;
				$history->date=date('Y-m-d H:i:s');
				$history->reason_id=$request->input('reason_id');
				$history->comment=$request->input('comment');

				

				$nocancel=Servicesettings::select('code','value')->where('code','customer_no_penalty_duration')
				->where('type_id',$service->type_id)->first();
				$date = date("Y-m-d H:i:s");
				$time = strtotime($date);
				$time = $time - ($nocancel->value * 60);
				$pasttime = date("Y-m-d H:i:s", $time);
				$duration=(time()-strtotime($service->request_date))/60;

				$maxcancel=Servicesettings::select('code','value')->where('code','customer_max_cancel_unit')
							->where('type_id',$service->type_id)->first();

							
				$pastcancelcount=CustomerCancelHistory::where('customer_id',$customer->id)->where('date','<=',$date)->where('date','>=',$pasttime)->count();
				
				$penalty_amount=0;
				
				if($nocancel->value<$duration){
				
							
							

							$moderate=Servicesettings::select('code','value')->where('code','customer_moderate_penalty_duration')
							->where('type_id',$service->type_id)->first();

							$higher=Servicesettings::select('code','value')->where('code','customer_higher_penalty_duration')
							->where('type_id',$service->type_id)->first();

							


								if($duration<$moderate->value)
								{
									
										$moderateprice=Servicesettings::select('code','value')->where('code','customer_moderate_penalty_amount')
										->where('type_id',$service->type_id)->first();

										$penalty_amount=$moderateprice->value;

								}
									elseif($duration>=$higher->value)
									{
											$higherprice=Servicesettings::select('code','value')->where('code','customer_higher_penalty_amount')
										->where('type_id',$service->type_id)->first();

										$penalty_amount=$higherprice->value;
										}


						
						

				}

				$history->penalty_amount=$penalty_amount;
				
				$history->save();

				

				if($provider->device_type=='ios' && $provider->device_id!='')
				{
					
						$arr = array('type'=>'jobcancelled','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderAPNS("Job is cancelled!",$arr,$provider->device_id);
					
					}elseif($provider->device_id!='' ){

						$arr = array('type'=>'jobcancelled','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderFCM("Job is cancelled!",$arr,$provider->device_id);
					}
				
				
				return true;
			}
			
		return false;
	}



/*
	protected function rescheduleCustomer($booking_slot_id,$provider,$service)
	{

			//reschedule
					$booktime=Providerbookedtime::where("id",$booking_slot_id)->first();
					
					$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
					$buffer_time=(int)$buffer_time->value;
					$mode=($provider->profile->transport=='walk')?'walking':'driving';


						
					$all_next_booking=Providerbookedtime::with('Service')
					->whereHas('Service', function ($query)  use ($booktime,$service){
						$query->where('service_status_id',4);
						$query->where('booking_slot_id','>',0);
						//$query->where('to_time','>',$booktime->to_time);
						$query->where('id','<>',$service->id);
					})
					->where('to_time','>',$booktime->to_time)
					->whereDate('to_time',date("Y-m-d",strtotime($booktime->to_time)))
					->where('provider_id',$provider->id)
					->orderby("from_time","asc")->get();
				
					

					if($all_next_booking){


						foreach($all_next_booking as $nbooking)
						{
							$from_time=$nbooking->from_time;
							$to_time=$nbooking->to_time;

							$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
							->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
							->where('to_time','<', $from_time)
							->where('id','<>', $booking_slot_id)
							->whereHas('Service', function ($query) use ($service){
									$query->where('service_status_id',4);
									$query->where('booking_slot_id','>',0);
									$query->where('id','>',$service->id);
									
								})
							->orderby('from_time','desc')->first();
							//print_r($prevbooked->toArray());exit;
						
							//if no booking then estimate job start time and end time
									$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
									if($provider->base_latitude && $provider->base_longitude && $provider->location_preference=='fixed')
									$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
									elseif($prevbooked)
									$origin='origins='.$prevbooked->service->latitude.','.$prevbooked->service->longitude;
									else
									$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
									
									$destination='&destinations='.$nbooking->service->latitude.','.$nbooking->service->longitude;
									
									$data=$this->httpGetGoogle($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
								
									$data=json_decode(($data),true);
								
									
									if(!isset($data['rows'][0]['elements'][0]['duration'])) continue;
									$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;

									
							if($prevbooked)
								{
									
									$heading_start=date("H:i:s", strtotime($prevbooked->to_time));
								}
							else
								$heading_start=date("H:i:s", strtotime('-'.$estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));

								$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" (('".$heading_start."' between from_time and to_time)  )")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($nbooking->service->to_time)))
									->first();
						
									if(count($freeslot)>0)
									{
										if($freeslot->from_time>$heading_start)
										{
												
												$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime(date("Y-m-d",strtotime($nbooking->service->to_time))." ".$freeslot->from_time)));

												

										}

										elseif($prevbooked)
										{
											
											$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($prevbooked->to_time)));
											
											//if($espected_request_start_time<=$nbooking->service->from_time)
											//$espected_request_start_time=$nbooking->service->from_time;

											
										}
										
										else
										{
										
										$espected_request_start_time=$nbooking->service->from_time;

										
										}
									}else
									{
										
										  $espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));
										 
									}
									

									$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
									
								

									if(strtotime($espected_request_start_time)>=strtotime($nbooking->service->from_time)  && $espected_request_end_time<=$nbooking->service->to_time)
									{
									$booking=Providerbookedtime::where("id",$nbooking->service->booking_slot_id)->first();
									//echo "id".$nbooking->service->booking_slot_id;
									$booking->from_time=$espected_request_start_time;
									
									$booking->to_time=$espected_request_end_time;
									$booking->save();
									}elseif($espected_request_start_time<$nbooking->service->from_time){
										//echo "id".$nbooking->service->booking_slot_id;
										//echo "ff";
										//if provider reach at place before arrival that means
										//more time within schedule gap so adjustment not required for next bookings
										break;
									}





									

						}		


					}
					//exit;

					
	}
	* */

	/***********************       send rating to provider for completed jobs    *******************************/
	
	protected function createRatingtoProvider($request)
	{

			$phone = $request->input('phone');
			$customer=Customer::where('phone',$phone)->first();
			$service=Service::where("id",$request->input('service_id'))->first();

			if(!$service)
			return false;

			$ratecount=ProviderRatings::where('provider_id',$service->provider_id)->where('service_id',$request->input('service_id'))->count();
			if($ratecount<=0 && $service->service_status_id=='6')
			{
				$rating=new ProviderRatings();
				$rating->provider_id=$service->provider_id;
				$rating->customer_id=$customer->id;
				$rating->service_id=$request->input('service_id');
				$rating->rate_value=$request->input('rating');
				$rating->professional=$request->input('professional');
				$rating->friendly=$request->input('friendly');
				$rating->communication=$request->input('communication');
				 
				$rating->comment=$request->input('comment');
				$rating->date=date('Y-m-d H:i:s');
				$rating->save();
				return $rating;
			}else{
				return false;
			}
			
			
			
			

	}

	/***********************       get ratings sent to provider    *******************************/
	
	protected function getRatingtoProvider($request)
	{
		if($request->input('service_id')=='' || $request->input('phone')=='') return false;
			$phone = $request->input('phone');
			$customer=Customer::where('phone',$phone)->first();
			$service=Service::where("id",$request->input('service_id'))->first();
			$ratecount=ProviderRatings::where('customer_id',$customer->id)->where('service_id',$request->input('service_id'))->count();
			if($ratecount>0 )
			{
				$rating=ProviderRatings::where('customer_id',$customer->id)->where('service_id',$request->input('service_id'))->first();
				
				return $rating;
			}else{
				return false;
			}
			
			
			
			

	}

	/***********************      create dispute to provider   *******************************/

	protected function createDispute($request)
	{

			$service=Service::where("id",$request->input('service_id'))->first();

			$count=Dispute::where('service_id',$service->id)->where('dispute_to','provider')->count();
			if($count<=0 && $service->provider_id!='')
			{
					$phone = $request->input('phone');
					$customer=Customer::with("Profile")->where('phone',$phone)->first();
					$provider=Provider::where('id',$service->provider_id)->first();
					$dispute=new Dispute;
					$dispute->service_id=$request->input('service_id');
					$dispute->customer_id=$customer->id;
					$dispute->provider_id=$service->provider_id;
					$dispute->date=date("Y-m-d H:i:s");
					$dispute->details=$request->input('details');
					$dispute->status=0;
					$dispute->dispute_to='provider';
					$dispute->save();

						if($provider->device_type=='ios' && $provider->device_id!=''){
					
							$arr = array('type'=>'dispute','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
							$status= $this->sendProviderAPNS("Job is in dispute!",$arr,$provider->device_id);
						
						}elseif($provider->device_id!='' ){

							$arr = array('type'=>'dispute','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
							$status= $this->sendProviderFCM("Job is in dispute!",$arr,$provider->device_id);	
						}
				
			}
			else
			{
				return false;
			}
		
		return true;
	}


	/***********************     verify booking slot before final booking of provider and send notification   *******************************/
	
	
	protected function bookProvider($service,$provider)
	{
		
		$mode=($provider->profile->transport=='walk')?'walking':'driving';
		$freeslot=Providerfreetime::

					where('provider_id',$provider->id)
					->where('from_time','<',date("H:i:s",strtotime($service->from_time)))
					->where('to_time','>',date("H:i:s",strtotime($service->from_time)))
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->where('time_type','free')
					->orWhere('from_time','<',date("H:i:s",strtotime($service->from_time)))
					->where('to_time','>',date("H:i:s",strtotime($service->to_time)))
					->where('provider_id',$provider->id)
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->orWhere('provider_id',$provider->id)
					->whereRaw("'".date("H:i:s",strtotime($service->to_time))."' between from_time and to_time")
					
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))

					->orWhere('provider_id',$provider->id)
					->whereRaw("from_time>='".date("H:i:s",strtotime($service->from_time))."' and to_time<='".date("H:i:s",strtotime($service->to_time))."'")
					
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))

					/*where('provider_id',$provider->id)
					->where('from_time','<=',date("H:i:s",strtotime($service->from_time)))
					->where('to_time','>=',date("H:i:s",strtotime($service->from_time)))
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->where('time_type','free')
					->orWhere('from_time','<=',date("H:i:s",strtotime($service->to_time)))
					->where('to_time','>=',date("H:i:s",strtotime($service->to_time)))
					->where('provider_id',$provider->id)
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->orWhere('provider_id',$provider->id)->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->whereRaw(" from_time <='".date("H:i:s",strtotime($service->from_time)).
					"' and to_time >= '".date("H:i:s",strtotime($service->to_time))."'")*/
					

					->first();

				
	

		$servicecount=Service::where('provider_id',$provider->id)->where('service_status_id',1)->count();
		if($servicecount>0) return false;
		
		//past booking not allowed
		if($service->from_time<date("Y-m-d H:i:s"))
		{return false;}
			
		if($freeslot)
		{
			
			
			//should not change arrival time to find the booking because booking slot will not be in unavailable time
			/*if(date("H:i:s",strtotime($service->from_time))<$freeslot->from_time)
				$service->from_time=date("Y-m-d",strtotime($service->from_time))." ".$freeslot->from_time;
				*/
			/************************************************/	
		
			$booked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
			->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
			->where('from_time','<=', $service->to_time)
			->where('from_time','>=', $service->from_time)
			->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
			->orWhere('to_time','<=', $service->to_time)
			->where('provider_id',$provider->id)
			->where('to_time','>=', $service->from_time)
			->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})
			->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))->orderby('from_time','asc');


			//print_r($booked->toArray());exit;
			$canbebooked=false;


			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();

			$buffer_time=(int)$buffer_time->value;

			

		//set booking order

			if($booked && $booked->count()>1)
			{
				
				
				$booked=$booked->first();
				//if there is booking after current request calculate duration and distance
				
				
				$working_service=Service::with("Booked")->where('provider_id',$provider->id)->where('id',$booked->service->id)->first();

				//$working_service_totime=date("Y-m-d H:i:s", strtotime('+'.$working_service->job_duration_set_by_provider.' minutes', strtotime($working_service->from_time)));
				$working_service_totime=$working_service->booked->to_time;

				
				$travel_start_time=$working_service_totime;

				$confirmed=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
				->where('from_time','<=', $service->from_time)
				->where('to_time','>=', $service->from_time)
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
				->whereHas('Service', function ($query){
					$query->where("service_status_id",4);
					$query->orwhere("service_status_id",5);
					$query->orwhere("service_status_id",9);
					 
				})
				->orWhere('from_time','<=', $service->to_time)
				->where('to_time','>=', $service->to_time)
				->where('provider_id',$provider->id)
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
				->whereHas('Service', function ($query){
					$query->where("service_status_id",4);
					$query->orwhere("service_status_id",5);
					$query->orwhere("service_status_id",9);
					 
				})
				->orWhere('provider_id',$provider->id)
				->whereRaw("'".$service->from_time."'<`from_time` and  '".$service->to_time."'>to_time")
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
				->whereHas('Service', function ($query){
					$query->where("service_status_id",4);
					$query->orwhere("service_status_id",5);
					$query->orwhere("service_status_id",9);
					 
				})
				
				->orderby('from_time','desc')->first();

				//print_r($confirmed->toArray());
				//exit;
				if(!$confirmed) return false;
				$confirmed_end=$confirmed->to_time;
				
				$duration_reach_at_confirmed_place=(strtotime($confirmed_end)-strtotime($travel_start_time))/60; //65

				
				
				//get duration between working and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$working_service->latitude.','.$working_service->longitude;
				$destination='&destinations='.$service->latitude.','.$service->longitude;
				
				$data=$this->httpGetGoogle($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				

				$working_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10
				
				
				//get duration between request and booked confirmed location
				$origin='origins='.$service->latitude.','.$service->longitude;
				$destination='&destinations='.$confirmed->service->latitude.','.$confirmed->service->longitude;
				
				$data=$this->httpGetGoogle($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				$requestto_booked_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60); //5



				

				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($travel_start_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
				$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;
			

				
				//$working_torequest_duration.'=';
				//$requestto_booked_duration.'=';
				//$request_service_duration.'=';
				
				$total_expected_duration=$buffer_time+(int)$working_torequest_duration+(int)$requestto_booked_duration+(int)$request_service_duration;
				
				

				if($total_expected_duration<$duration_reach_at_confirmed_place)
				{

					//book with ascending order
					$booked->sort_order='1';
					
					$confirmed->sort_order='3';
					$canbebooked=true;
					//$bservice=Service::where('id',$booked->service->id)->first();
					
					

					
				}else{
				//try other method
				//A>C>B
				
					
					$travel_start_time=$confirmed->to_time;
					

					$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$requestto_booked_duration.' minutes', strtotime($travel_start_time)));
					$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
					

					$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;

					$working_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($espected_request_end_time)));
	
					if($working_start_time<$working_service->to_time)
					{
						
						//book with descending order

						$booked->sort_order='3';
						
						$confirmed->sort_order='1';
						$canbebooked=true;
						
						
					}elseif($espected_request_start_time<$service->to_time ){
						
						$canbebooked=true;
						


					}
				
				


				}
				
				if($canbebooked==true && $booked && $confirmed){
					//$booked->save();
					//$confirmed->save();
				}

				
					if($booked->to_time>$confirmed->to_time)
					{
					$maxtime=$booked->to_time;
					}else{
						$maxtime=$confirmed->to_time;
					}

			}else{
				
				$canbebooked=true;
			}
			
			if($canbebooked)
			{
				
				if($service->from_time==$service->to_time) return false;
				
				if($booked && $booked->count()==1){
				
					
					$booked=$booked->first();
					$confirmed=Service::with("Booked")->where('provider_id',$provider->id)->where('id',$booked->service->id)->first();
					//print_r($booked->toArray());
					//print_r($confirmed->toArray());exit;
					//get duration between working and request location
					$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
					$origin='origins='.$confirmed->latitude.','.$confirmed->longitude;
					$destination='&destinations='.$service->latitude.','.$service->longitude;
					
					
					$data=$this->httpGetGoogle($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
					$data=json_decode(($data),true);
					if(!isset($data['rows'][0]['elements'][0]['duration']))
					return false;
					

					$confirmed_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10

					
					if($service->to_time <= $confirmed->booked->from_time){
						
						//get duration between provider and request location
						$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
						$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
						$destination='&destinations='.$service->latitude.','.$service->longitude;
						
						$data=$this->httpGetGoogle($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
						$data=json_decode(($data),true);
						if(!isset($data['rows'][0]['elements'][0]['duration']))
						return false;

						$current_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10

						//will start before arrival time
						$new_start_time=date("Y-m-d H:i:s", strtotime('-'.($current_torequest_duration+$buffer_time).' minutes', strtotime($service->from_time)));
						$new_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($new_start_time)));


						$expected_confirmed_start_time =date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($new_end_time)));

						//try A>B>C
						if($expected_confirmed_start_time<=$confirmed->booked->from_time)
							{
								$booked->sort_order='2';
						
								$order='1';
								//$booked->save();

								return $service;

							}
						else
							return false;
							



								
					}elseif($service->from_time >= $confirmed->booked->to_time){
					//try A>C>B
					
					$new_start_time=date("Y-m-d H:i:s", strtotime('+'.($confirmed_torequest_duration+$buffer_time).' minutes', strtotime($confirmed->booked->to_time)));
					

					if($new_start_time < $service->to_time)
						{
							$booked->sort_order='1';
						
							$order='2';
							//$booked->save();
								
							return $service;

						}
					else
						return false;




					}

					

					
					
					
					//$working_start_time=date("Y-m-d H:i:s", strtotime('+'.$confirmed_torequest_duration.' minutes', strtotime($espected_request_end_time)));



				}
/*
				else{

						
				
				
				$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
					->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))
					->where('to_time','<=', $service->from_time)
					->orderby('from_time','desc')->first();

					
			
			//if no booking then estimate job start time and end time
					$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
					if($provider->base_latitude && $provider->base_longitude )
					$origin='origins='.$provider->base_latitude.','.$provider->base_longitude;
					else
					$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
					$destination='&destinations='.$service->latitude.','.$service->longitude;
					
					$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
					//exit;
					$data=json_decode(($data),true);
					if(!isset($data['rows'][0]['elements'][0]['duration']))
					return false;
					
					
					$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
					//$estimated_traveltime_heading_current_location=$estimated_traveltime_heading;  
					$heading_start=date("H:i:s", strtotime('-'.$estimated_traveltime_heading.' minutes', strtotime($service->from_time)));
					$work_start=date("H:i:s", strtotime($service->from_time));
					$work_end=date("H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($work_start)));;

					$freeslot=Providerfreetime::where('provider_id',$provider->id)
					->whereRaw(" (('".$heading_start."' between from_time and to_time and '".$work_start."' between from_time and to_time and '".$work_end."' between from_time and to_time)
					)")
					->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))
					->first();
					

					
					//check if heading can start before arrival
					if(count($freeslot)>0)
					{
						return $service;;
					}

					
				
				



				}*/
				
			
			//print_r($service->toArray());exit;
				
				return $service;
			}else{
				return false;
			}

			
			
		}

		
		return false;
		
		
	}

	/***********************     validate login for customer   *******************************/
	
	
	protected function CustomerTraitLogin($request)
	{


		$phone = $request->input('phone');
		$password = $request->input('password');


		$user_arr = Customer::with("Profile")->where('phone', '=', $phone)->first();

		if($user_arr){
				$user_arr = $user_arr->toArray();

				
				$user=DB::table('customers')->where('id', $user_arr['id'])->where("sms_verified",1)->first();
				$user2=DB::table('customers')->where('id', $user_arr['id'])->first();
				
				if($user && Hash::check($password, $user->password))
					{
						
						
						if($user_arr['is_active'] == 1 && $user_arr['sms_verified'] == 1 && $user_arr['banned_duration'] <=0)
						{
							
							$new_user_arr['data']['phone'] = $user_arr['phone'];
							$new_user_arr['data']['status'] = $this->authenticate($phone,$password);
							$new_user_arr['data']['name'] = $user_arr['profile']['name'];
						
							
							############# create token -start ##############
							
							$new_token = $this->create_token_customer($request,$user_arr['id']); 
							$new_user_arr['data']['token'] = $new_token;
							$new_user_arr['success'] = 0;
							$new_user_arr['msg'] = 'Logged in successfully.';
							############# create token -end ###############
							
						}
						elseif($user_arr['banned_duration'] >0){
								$new_user_arr['success'] = 2;
								$new_user_arr['msg'] = 'Your account is banned temporarily.';

						}
						elseif($user_arr['is_active'] == 1 && $user_arr['sms_verified'] != 1){
								$new_user_arr['success'] = 2;
								$new_user_arr['msg'] = 'Your account sms not verified.';
						}
						else
						{
							$new_user_arr['success'] = 1;
							$new_user_arr['msg'] = 'Your account is inactive.';
						}
					}
					elseif($user2 && $user2->sms_verified != 1 && $user2->is_active == 1)
					{
						$new_user_arr['success'] = 2;
						$new_user_arr['msg'] = 'Your account sms not verified.';

					}else{

						
						$new_user_arr['success'] = 1;
						$new_user_arr['msg'] = 'Your phone number or password is wrong.';
					}


					if($new_user_arr['success']==2){


						$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

						$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

						$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
						$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$request->input('phone'))->where('send_date','>=',$future_time)->count();

						$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
						$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$request->input('phone'))->where('send_date','>=',$future_time)->count();

						$new_user_arr["per_min_sms"]=$per_min_sms;
						$new_user_arr["per_min_sms_count"]=$one_min_log;
						$new_user_arr["per_hour_sms"]=$per_hour_sms;
						$new_user_arr["per_hour_sms_count"]=$per_hour_log;





					}

				

			}else
			{
				$new_user_arr['success'] = 1;
				$new_user_arr['msg'] = 'Your phone number or password is wrong.';
			}

			
		
			return $new_user_arr;
			die;
		

	}

	/***********************     get distance and duration for latitude and longitude  *******************************/

	
	private function httpGetGoogle($url)
	{
		
		$ch = curl_init();  
		//echo $url=urlencode($url);
		curl_setopt($ch,CURLOPT_URL,($url));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		//curl_setopt($ch,CURLOPT_HEADER, false); 
	 
		$output=curl_exec($ch);
		$info = curl_getinfo($ch);
		
	 
		curl_close($ch);
		return $output;
	}


	/***********************    get today's booking and all past booking  *******************************/

	protected function currentBooking($request)
	{
		$datewiseService=array();
		$customer=Customer::where('phone',$request->input('phone'))->first();
		
		
		$service=Service::selectRaw("*,DATE_FORMAT(request_date, '%Y-%m-%d') as bookdate")->with(['Booked','ServiceType'=>function ($query)  {
				$query->selectRaw("id,name");

			}])->where('customer_id',$customer->id)	;
		if($request->input('type')=='current'){
			$service=$service->whereRaw(' (service_status_id=5 AND service_status_id=4 AND service_status_id=9)');

			$datewiseService=Service::selectRaw("DATE_FORMAT(request_date, '%Y-%m-%d') as date")->where('customer_id',$customer->id)->whereRaw(' (service_status_id=5 AND service_status_id=4 AND service_status_id=9)')->orderBy('id', 'desc')->groupBy("date")->get()->toArray();


		}elseif($request->input('type')=='past'){
			$service=$service->whereRaw(' (service_status_id<>5 AND service_status_id<>4 AND service_status_id<>9 AND service_status_id<>2)');

			$datewiseService=Service::selectRaw("DATE_FORMAT(request_date, '%Y-%m-%d') as date")->where('customer_id',$customer->id)->whereRaw(' (service_status_id<>5 AND service_status_id<>4 AND service_status_id<>9 AND service_status_id<>2)')->orderBy('id', 'desc')->groupBy("date")->get()->toArray();
			
			//print_r($datewiseService);exit;

		}
		$service=$service->orderBy('id', 'desc')->get();
		if($service && count($service)>0)
		{
			$service=$service->toArray();

			
			
			for($i=0;$i<count($service);$i++){

				$status=Servicestatus::where('id',$service[$i]['service_status_id'])->first();
				$service[$i]['job_status']=$status->name;
				
				if($status->id==6){
					
					$notice=Customernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","jobcompleted")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;
					
				}elseif($status->id==2 || $status->id==8 || $status->id==10 || $status->id==11){
					
					$notice=Customernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","cancelled")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;

				}
				elseif($status->id==4){
					
					$notice=Providernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","confirmed")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;

				}
				elseif($status->id==5){
					
					$notice=Providernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","jobstarted")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;

				}
				
				elseif($status->id==9){
					
					$notice=Providernotification::where("service_id",$service[$i]['id'])->where("service_notice_status","heading")->first();
					if($notice)
					$service[$i]['status_date']=$notice->date;

				}
				
				$service[$i]['rating']=ProviderRatings::where("provider_id",$service[$i]['provider_id'])->where("service_id",$service[$i]['id'])->avg('rate_value');

				$service[$i]['already_rated']=ProviderRatings::where("provider_id",$service[$i]['provider_id'])->where("service_id",$service[$i]['id'])->count();
				
				$service[$i]['provider']=Provider::leftJoin('provider_profile','providers.id','=','provider_profile.provider_id')->selectRaw('name,image,current_latitude,current_longitude,phone')->where("providers.id",$service[$i]['provider_id'])->first();

			

				 $mapurl='https://maps.googleapis.com/maps/api/staticmap?center='.$service[$i]['latitude'].",".$service[$i]['longitude'].'&zoom=11&size=600x300&
				maptype=roadmap&markers=icon:'.url("img/map_ico.png")."|".$service[$i]['latitude'].",".$service[$i]['longitude']."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k");

				$service[$i]['map_url']=$mapurl;
			

			

				for($x=0;$x<count($datewiseService);$x++){
					if($datewiseService[$x]['date']==$service[$i]['bookdate']){
						$datewiseService[$x]['service'][]=$service[$i];
					}

				}

			}

		
			//print_r($datewiseService);exit;
		return $datewiseService;
		}
		return false;
	}

	/***********************    get notification details for different service status of customer  *******************************/

	
	protected function Notification($request)
	{
		$phone = $request->input('phone');
		
		$status = $request->input('status');
		$date = $request->input('date');
		
		$service_id = $request->input('service_id');
		
		$customer=Customer::where('phone',$phone)->first();
		$notice=Customernotification::where('customer_id',$customer->id)->orderBy('id', 'desc')->skip(0)->take(1);

		if($notice)
		{
			

			if($status!='')
			$notice=$notice->where("service_notice_status",$status);

			if($date!='')
			$notice=$notice->whereDate("date",$date);

			
			if($service_id!='')
			$notice=$notice->where("service_id",$service_id);
			
			$notice=$notice->get();
			return $notice;
		}
		return false;
		

	}

	/***********************    send push notification  *******************************/

	private	function sendProviderFCM($title,$body,$id)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
				'to' => $id,
				/*'notification' => array (
						"body" => ($body),
						"title" => $title,
						"icon" => "myicon"
				),*/
				'data'=>$body
		);

		
		$fields = json_encode ( $fields );
		$headers = array (
				'Authorization: key=' . env('FCM_KEY',"AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg"),
				'Content-Type: application/json'
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		return $result;
		}

	/***********************    send push notification IOS  *******************************/

	
	private function sendProviderAPNS($title,$body,$id)
	{

		// Put your device token here (without spaces):
		$deviceToken = $id;
		// Put your private key's passphrase here:
		$passphrase = env('APNS_KEY',"xxxxxxx");
		// Put your alert message here:
		$message = $body;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck_file.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'body' => $message,
				'action-loc-key' => $title,
			),
			'badge' => 2,
			'sound' => 'oven.caf',
			);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		if (!$result)
			return false;
		else
			return $result;
		// Close the connection to the server
		fclose($fp);

	}		
/*
//push notification on laravel plugin does not required
	protected function sendProviderPushNotification($title,$body,$device_id='dnhbpZTHbo4:APA91bH2bf2SFyPotO6jxX7gHns-YXtY-DqCmEPA9X3HQlkskiAR0fDHTRSAQ_BWtZz3r1ejHIFgRVUEGWnd43Cq91S-5-LolB6CT7WhYF21qFmxdMx-DwsmBM9P0Ten6LI9OzTWynQp'){

			$optionBuiler = new OptionsBuilder();
			$optionBuiler->setTimeToLive(60*20);

			$notificationBuilder = new PayloadNotificationBuilder($title);
			$notificationBuilder->setBody(json_encode($body))
								->setSound('default');

			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData($body);

			$option = $optionBuiler->build();
			$notification = $notificationBuilder->build();
			$data = $dataBuilder->build();
			
			//$token = "a_registration_from_your_database";
			$token = $device_id;

			return $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

			 $downstreamResponse->numberSuccess();
			$downstreamResponse->numberFailure();
			$downstreamResponse->numberModification();

			//return Array - you must remove all this tokens in your database
			$downstreamResponse->tokensToDelete(); 

			//return Array (key : oldToken, value : new token - you must change the token in your database )
			$downstreamResponse->tokensToModify(); 

			//return Array - you should try to resend the message to the tokens in the array
			$downstreamResponse->tokensToRetry();



	}
*/
	

	/***********************    update device id after certain time or when device id is changed  *******************************/
		
	protected function customerDeviceUpdate($request)
	{
		$phone = $request->input('phone');
		
		$customer=Customer::where('phone',$phone)->where('sms_verified',1)->where('is_active',1)->first();

		if($customer){

			$customer->device_id=$request->input('device_id');
			$customer->save();
			return true;
		}

		return false;

	}

	/***********************    update lat lng of currect location  *******************************/

	protected function CurrentLocationUpdate($request)
	{
		$phone = $request->input('phone');
		
		$customer=Customer::where('phone',$phone)->where('sms_verified',1)->where('is_active',1)->first();

		if($customer){

			$customer->current_latitude=$request->input('latitude');
			$customer->current_longitude=$request->input('longitude');
			$customer->save();
			return true;
		}

		return false;

	}

	/*****************    update car details of appointment if customer want to modify service which includes price of a service ************************/

	
	protected function updateCustomerCarDetails($request)
	{

		if($request->input('car_type')=='' || $request->input('phone')=='' || $request->input('service_id')=='')
		return false;
		$customer=Customer::where('phone',$request->input('phone'))->first();
		$service=Service::where('id',$request->input('service_id'))->where('customer_id',$customer->id)->first();
		if(!$service)
		return false;
		$duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$service->type_id)->first();
		
		$cartype=json_decode($request->input('car_type'),true);

		$jduration=0;

		foreach($cartype as $details){
			
				
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){

							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							
							$jduration=$jduration+$options['duration'];
							
						}

				
					}
			

			}
			$old_duration=$service->job_duration_set_by_provider;

			if($jduration < $duration->value)	
			$service->job_duration_set_by_provider=$duration->value;
			else
			$service->job_duration_set_by_provider=$jduration;

			

			


			$booked=Providerbookedtime::where("id",$service->booking_slot_id)->first();

			$nextbooked=Providerbookedtime::whereDate("book_date",date("Y-m-d",strtotime($booked->from_time)))
			->where("from_time",'>',date("Y-m-d H:i:s",strtotime($booked->to_time)))->orderby("from_time","asc")->first();

			$old_to_time=$booked->to_time;
			
			$new_to_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($booked->from_time)));
			
			if(!$nextbooked){
				$booked->to_time=$new_to_time;
				$booked->save();
				$service->save();
				
				}
				elseif($nextbooked && $nextbooked->from_time>$new_to_time){
				$booked->to_time=$new_to_time;
				$booked->save();
				$service->save();
				
				}
				//if duration not enough to cover next booking
				elseif($nextbooked && $nextbooked->from_time<$new_to_time){

					//echo $nextbooked->from_time;
					//echo $new_to_time;exit;
					return false;
				}

		//if can reschedule
			$provider=Provider::where('id',$service->provider_id)->first();
			$status=$this->RescheduleProviderBooking($service->booking_slot_id,$provider,$service);

			if($status==false)
			{

				$booked->to_time=$old_to_time;
				$booked->save();

				$service->job_duration_set_by_provider=$old_duration;
				$service->save();
				return false;

			}
		Servicecardetails::where('service_id',$request->input('service_id'))->delete();	
		
		foreach($cartype as $details){
			
				$stype=1;
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){

							$car=new Servicecardetails();
							$car->service_id=$service->id;
							$car->number_of_cars=1;
							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							$car->carwash_type=$options['serviceoption']['name'].$stype;
							$car->option_details_id=$details[$i]['option_details'][$x];
							
							$car->save();
							
						}

					$stype++;
					}
			

			}

			$provider=Provider::with('Profile')->where('id',$service->provider_id)->first();
			if($service && $provider && $provider->device_id!=''){
				

				if($provider->device_type=='ios' && $provider->device_id!=''){
					
						$arr = array('type'=>'customerupdateservice','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderAPNS("Appointment Updated!",$arr,$provider->device_id);
					
					}elseif($provider->device_id!='' ){

						$arr = array('type'=>'customerupdateservice','id'=>$service->id,"provider_name"=>$provider->name,"customer_name"=>$customer->profile->name);
						$status= $this->sendProviderFCM("Appointment Updated!",$arr,$provider->device_id);
					}
				
				}
			
		
		return true;
		
	}

	/*****************    send message to customer  ************************/

	protected function sendCustomerMessage(Request $request)
	{

		if($request->input('message_type')=='' || $request->input('email')=='' || $request->input('message')=='' || $request->input('phone')==''){
		return false;
		}

		$customer=Customer::where('phone',$request->phone)->first();
        if($customer)
        {
		$message=new CustomerMessage();
		$message->customer_id=$customer->id;
		$message->subject=$request->input('message_type');
		$message->message_type=$request->input('message_type');
		$message->email=$request->input('email');
		$message->message=$request->input('message');
		$message->save();

		return true;

		}
		else
		return false;

	}
	
	/*****************    common send sms to users this function is used for homecontroller, registration and provider controller  ************************/

	protected function sendsms($phone,$msg,$device_id='')
	{

			$authheader=base64_encode('primarymobile:Pass9738');
			$res=array();

			$header[] = 'Content-type: application/json';
			$header[] = 'Authorization: Basic '.$authheader;
		
			$data = array('from'=>'Qber',"bulkId"=>"QBER-".date("Y-m-d"),'to'=>$phone,'text'=>$msg);
			$data_json = json_encode($data);
			$url='https://api.infobip.com/sms/1/text/single';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			/*$send_sms=Servicesettings::select('code','value')->where('code','send_sms')->where('type_id','1')->first();
			if($send_sms->value=='1')
			$res  = curl_exec($ch);
			curl_close($ch);

			return $res;*/

			$cansend=$this->validateSMS($phone,$msg,$device_id);

			if($cansend)
			{
			$res  = curl_exec($ch);
			curl_close($ch);
			return $res;
			}
			else
			{
			curl_close($ch);
			return false;
			}
}

/*****************    validate sms 1 / minute and 5 sms  / hour  and admin enabled sms to send************************/

	private function validateSMS($phone,$msg,$device_id)
	{

			$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

			$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

			$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
			$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR']);
			if($device_id)
			$one_min_log=$one_min_log->where("device_id",$device_id);
			else
			$one_min_log=$one_min_log->where("phone",$phone);

			$one_min_log=$one_min_log->where('send_date','>=',$future_time)->count();

			$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
			$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR']);
			if($device_id)
			$per_hour_log=$per_hour_log->where("device_id",$device_id);
			else
			$per_hour_log=$per_hour_log->where("phone",$phone);
			
			$per_hour_log=$per_hour_log->where('send_date','>=',$future_time)->count();
		
			
			$send_sms=Servicesettings::select('code','value')->where('code','send_sms')->where('type_id','1')->first();

			
			
			if($send_sms->value=='1' && $one_min_log<$per_min_sms->value && $per_hour_log<$per_hour_sms->value)
			{
				$log=new Smslog;
				$log->device_id=$device_id;
				$log->phone=$phone;
				$log->sms_content=$msg;
				$log->send_date=date("Y-m-d H:i:s");
				$log->ip=$_SERVER['REMOTE_ADDR'];
				$log->save();
				return true;
			}
			else
			return false;

	}

	/*****************    get all booked appointments of customer ************************/

	protected function getbookedAppointments($request)
	{

			$phone = $request->input('phone');
			$customer=Customer::where('phone',$phone)->first();

			$appointments=Service::with(['ServiceNotification'=> function ($query)
			{
			$query->selectRaw('service_id,date,service_notice_status')->whereRaw("service_notice_status<>''")->orderby("id","desc");
				
				},
			'Provider'=> function ($query) {
				$query->selectRaw('providers.id,name,phone,current_latitude,current_longitude,name,provider_profile.image')->leftJoin('provider_profile','providers.id','=','provider_profile.provider_id');
				
				},
			'Booked'=> function ($query) {

			}	

			])
			->leftJoin('service_types','service.type_id','=','service_types.id')
			
			->selectRaw('job_description,provider_id,service.id,expected_cost,latitude,longitude,booking_slot_id,address,service_types.name as service_name,from_time,to_time,service_status_id')
			->where("customer_id",$customer->id)
			->where(function($query)  {
						 return $query->where('service_status_id',9)
						->orWhere('service_status_id',4)
						->orWhere('service_status_id',5);
					})

			->wherenotNull('booking_slot_id')->orderby('id','desc')->get()->toArray();

			$i=0;
			
			if(count($appointments)>0)
			{
				
				foreach($appointments as $appointment)
				{

					$mapurl='https://maps.googleapis.com/maps/api/staticmap?center='.$appointment['latitude'].",".$appointment['longitude'].'&zoom=14&size=600x300&
					maptype=roadmap&markers=icon:'.url("img/map_ico.png")."|".$appointment['latitude'].",".$appointment['longitude'].'&key='.env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k");

				

					$appointments[$i]['map_url']=$mapurl;
				$i++;
				}
			}

	return $appointments;

	}

	/*****************    get single appointment details ************************/

	protected function getSingleAppointment($request)
		{

			$phone = $request->input('phone');
			$customer=Customer::where('phone',$phone)->first();

			//broadcast(new LocationUpdated($customer));exit;
			 //event(new LocationUpdated($customer));
			
			$appointments=Service::with(['Provider'=> function ($query) {
				$query->selectRaw('id,name,phone,current_latitude,current_longitude');
				
				},'Cardetails','Providerprofile'=> function ($query) {
				$query->selectRaw('provider_id,image');
				
				},
				'Booked'=> function ($query) {

				},
				'ServiceNotification'=> function ($query) {
				$query->selectRaw('service_id,date,service_notice_status')->whereRaw("service_notice_status<>''")->orderBy('id', 'desc');
				
				}])
			->selectRaw('job_description,provider_id,id,expected_cost,latitude,longitude,from_time,to_time,type_id,service_status_id,booking_slot_id')
			->where("customer_id",$customer->id)->where("id",$request->service_id)->first()->toArray();

			$caroptions=array();
			
			foreach($appointments['cardetails'] as $carserv)
			{
						$caroptions[$carserv['carwash_type']][]=$carserv['option_details'];
			
				
			}
			$appointments['cardetails']=$caroptions;
			
			$appointments['service_type']=Servicetype::select('name')->where('id',$appointments['type_id'])->first()->toArray();

			$mapurl='https://maps.googleapis.com/maps/api/staticmap?center='.$appointments['latitude'].",".$appointments['longitude'].'&zoom=14&size=600x300&
				maptype=roadmap&markers=icon:'.url("img/map_ico.png")."|".$appointments['latitude'].",".$appointments['longitude'].'&key='.env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k");

				$appointments['map_url']=$mapurl;

		return $appointments;
	}


	/***********************       reschedule booking of provider slot on cancel by customer    *******************************/
	
	private function RescheduleProviderBooking($booking_slot_id,$provider,$service,$cancel=false)
	{

				//reschedule
					//reschedule
					$booktime=Providerbookedtime::where("id",$booking_slot_id)->first();
					
					$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
					$buffer_time=(int)$buffer_time->value;
					$mode=($provider->profile->transport=='walk')?'walking':'driving';


						
					$all_next_booking=Providerbookedtime::with('Service')
					->whereHas('Service', function ($query) use ($booktime,$service){
						$query->where('service_status_id',4);
						$query->where('id','<>',$service->id);
						$query->where('booking_slot_id','>',0);
						
						
					})
					->where('to_time','>',$booktime->to_time)
					->whereDate('to_time',date("Y-m-d",strtotime($booktime->to_time)))
					->where('provider_id',$provider->id)
					->orderby("from_time","asc")->get();
					//print_r($all_next_booking->toArray());exit;
					$changebooking=array();
					
					if($all_next_booking){

						$canchange=false;						
						$i=0;
						foreach($all_next_booking as $nbooking)
						{
							$from_time=$nbooking->from_time;
							$to_time=$nbooking->to_time;

							if($cancel==true){

										$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
									->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
									->where('id','<=', $nbooking->id)
									
									->whereHas('Service', function ($query) use ($service,$nbooking){
											
											$query->where('booking_slot_id','>',0);
											$query->where('booking_slot_id','<>',$nbooking->id);
											//$query->where('id','>',$service->id);
											
										})
									->orderby('id','desc')->first();

							}else{
								$prevbooked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
								->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
								->where('id','<=', $nbooking->id)
								
								->whereHas('Service', function ($query) use ($service,$nbooking){
										$query->whereRaw('(service_status_id=4 or service_status_id=5 or service_status_id=9)');
										$query->where('booking_slot_id','>',0);
										$query->where('booking_slot_id','<>',$nbooking->id);
										//$query->where('id','>',$service->id);
										
									})
								->orderby('id','desc')->first();
						}

						//print_r($prevbooked);exit;

						
						

									
							if($prevbooked)
								{
									if($i==0){


										if($cancel==true){

												$booktime=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
												->whereDate('book_date',date("Y-m-d",strtotime($nbooking->service->to_time)))
												->where('to_time','<=', $booktime->from_time)
												
												->whereHas('Service', function ($query) use ($service,$nbooking){
														$query->whereRaw('(service_status_id=4 or service_status_id=5 or service_status_id=9)');
														$query->where('booking_slot_id','>',0);
														$query->where('booking_slot_id','<>',$nbooking->id);
														
														
													})
												->orderby('id','desc')->first();

														if(!$booktime){

															$booktime=Providerbookedtime::where("id",$booking_slot_id)->first();
															$head_start_time=date("Y-m-d H:i:s", strtotime('-'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));
															
															$freeslot=Providerfreetime::where('provider_id',$provider->id)
															->whereRaw(" ('".date("H:s:i",strtotime($head_start_time))."' between from_time and to_time
															OR '".date("H:s:i",strtotime($nbooking->service->from_time))."' between from_time and to_time )
															")
														
															->whereDate('date',date("Y-m-d",strtotime($booktime->from_time)))
															->orderby("from_time","desc")->first();
															

															if($freeslot)
															{
																if($freeslot->time_type=='free')
																$booktime->to_time=$head_start_time;
																else
																{
																 $booktime->to_time=$head_start_time=$freeslot->date." ".$freeslot->to_time;
																
																}
															

															

															
															}

														}

												

												}
												
											$heading_start=date("H:i:s", strtotime($booktime->to_time));
											
											}else{
											$heading_start=date("H:i:s", strtotime($changebooking[$i-1]['to_time']));
											}
									
								}
							else
							$heading_start=date("H:i:s", strtotime('-'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));


							

							$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" (('".$heading_start."' between from_time and to_time)  )")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($nbooking->service->to_time)))
									->first();
						
									if(count($freeslot)>0)
									{
										if($freeslot->from_time>$heading_start)
										{
												
												$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$estimated_traveltime_heading.' minutes', strtotime(date("Y-m-d",strtotime($nbooking->service->to_time))." ".$freeslot->from_time)));
											
												

										}

										elseif($prevbooked  )
										{
											if($i==0){

												
											
												
											$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($booktime->to_time)));
											
											}else{
											$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($changebooking[$i-1]['to_time'])));
											
											}
											//echo "previd".$prevbooked->service->id;
											//echo "prevtime".$prevbooked->to_time;
											//echo '----';
											//if($espected_request_start_time<=$nbooking->service->from_time)
											//$espected_request_start_time=$nbooking->service->from_time;
											
											
										}
										
										else
										{
										
										$espected_request_start_time=$nbooking->service->from_time;

										
										}
									}else
									{
										
										  $espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->estimated_traveltime_heading.' minutes', strtotime($nbooking->service->from_time)));
										 
									}
									
									
									$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$nbooking->service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
									//echo "id".$nbooking->service->id;
									//echo $espected_request_start_time;
									//echo $espected_request_end_time;
									//echo $nbooking->service->from_time;
									//echo $nbooking->service->to_time;
									//echo "---------------------------<br />";

									//$booking=Providerbookedtime::where("id",$nbooking->service->booking_slot_id)->first();

									$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" (('".date("H:i:s",strtotime($espected_request_start_time))."' between from_time and to_time and '".date("H:i:s",strtotime($espected_request_end_time))."' between from_time and to_time)  )")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($espected_request_start_time)))
									->count();

									//echo $heading_start;
									//echo $espected_request_start_time;
									//echo $nbooking->service->from_time;
									//echo $nbooking->service->to_time;
									//echo $espected_request_start_time;
									//echo $espected_request_end_time;
									//echo $freeslot;
									//echo "-----------------<br />";
									//exit;
											
									if($espected_request_start_time>=$nbooking->service->from_time && $espected_request_start_time<=$nbooking->service->to_time && $freeslot>0)
									{
									
									//echo "id".$nbooking->service->id;
									//$booking->from_time=$espected_request_start_time;
									
									//$booking->to_time=$espected_request_end_time;
									
									$changebooking[]=array("id"=>$nbooking->id,"from_time"=>$espected_request_start_time,"to_time"=>$espected_request_end_time);
									//$booking->save();
									$canchange=true;
									}elseif($espected_request_start_time<$nbooking->service->from_time){
										//echo "id".$nbooking->service->booking_slot_id;
										//echo "ff";
										//if provider reach at place before arrival that means
										//more time within schedule gap so adjustment not required for next bookings
										$canchange=false;
										break;
									}else{
										$canchange=false;
										break;
									}





							$i++;		

						}		


					}

					
					
					if($canchange==true)
					{
						//echo "ddd";
						//print_r($changebooking);
						//exit;
						foreach($changebooking as $booking){
							$book=Providerbookedtime::with('Service')->where("id",$booking['id'])->first();
							$book->heading_start_time=date("Y-m-d H:i:s", strtotime('-'.$book->service->estimated_traveltime_heading.' minutes', strtotime($booking['from_time'])));
							$book->from_time=$booking['from_time'];
							$book->to_time=$booking['to_time'];
							$book->save();

						

						}
					}elseif(count($all_next_booking)<=0){
						
						$freeslot=Providerfreetime::where('provider_id',$provider->id)
									->whereRaw(" ('".date("H:i:s",strtotime($booktime->to_time))."' between from_time and to_time) ")
									->where('time_type','free')
									->whereDate('date',date("Y-m-d",strtotime($booktime->to_time)))
									->first();

									
						/*$book=Providerbookedtime::whereRaw(" ('".($booktime->to_time)."' between from_time and to_time and id <>'".$booktime->id."' )
						OR ( to_time > '".($booktime->to_time)."'  and id <>'".$booktime->id."' )

						")
						->whereDate('to_time',date("Y-m-d",strtotime($booktime->to_time)))
						->where('provider_id',$provider->id)

						
						->count();*/
						//echo "bcount".$book;
					//exit;
						if(count($freeslot)>0) return true;


					}

					

					return $canchange;

	}



	protected function getTrackLocation($request){
		if( $request->input('phone')==''){
		return false;
		}

		$phone = $request->input('phone');
		
		$customer=Customer::with("Profile")->where('phone',$phone)->first();
		$service=Service::whereRaw("(service_status_id=5 or service_status_id=9) and customer_id=".$customer->id)->orderby("id","desc")->first();

		$provider=Provider::with("Profile")->where('id',$service->provider_id)->first();

	if($provider && $service) 
		{

			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
			
			$buffer_time=(int)$buffer_time->value;

			$customer=Customer::with("Profile")->where("id",$service->customer_id)->first();

			//duration between service and provider...provider name and image with rating
			$mode=($provider->profile->transport=='walk')?'walking':'driving';
			$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
			$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
			$destination='&destinations='.$service->latitude.','.$service->longitude;
			
			
			$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
			$data=json_decode(($data),true);
			if(!isset($data['rows'][0]['elements'][0]['duration'])) return false;

			$duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);

			
		$rating=ProviderRatings::selectRaw('AVG(rate_value) as rating, count(*) as ratecount')->where('provider_id',$provider->id)->get();
		$type=Servicetype::where("id",$service->type_id)->first();
		
		$arr = array('type'=>'livetrack',"id"=>$service->id,"provider_name"=>$provider->name,"latitude"=>$provider->current_latitude,"longitude"=>$provider->current_longitude,
		"customer_name"=>$customer->profile->name,"transport"=>$provider->profile->transport,"duration"=>$service->estimated_traveltime_heading,
		"provider_image"=>$provider->profile->image,"provider_rating"=>$rating,"service_type"=>$type->name);


		return $arr;

		}

	return false;
	}



	
   }
