<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Servicecardetails extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_car_details';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function OptionDetails(){
		return $this->hasMany('App\Models\Serviceoptiondetails', 'id','option_details_id');

	}
}
