<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Customerprofile extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_profile';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	

}
