<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Role extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin_role';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function Roleaccess(){
		return $this->hasMany('App\Models\Roleaccess', 'role_id','id');

	}
}
