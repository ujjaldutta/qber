<?php
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Customer extends Model  implements AuthenticatableContract, CanResetPasswordContract{

   use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array( 'remember_token','password','created_at','updated_at');

    
	public function Profile(){
		return $this->hasOne('App\Models\Customerprofile', 'customer_id','id');

	}
	public function Jobs(){
		return $this->hasMany('App\Models\Service', 'customer_id','id');

	}

	public function Appointments(){
		return $this->hasMany('App\Models\Appointments', 'customer_id','id');

	}
	
}
