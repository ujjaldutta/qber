<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class CompanyProvider extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_providers';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function Account(){
		return $this->hasOne('App\Models\ProviderCompanyAccount', 'provider_id','provider_id');

	}

	public function Details(){
		return $this->hasOne('App\Models\Company', 'id','company_id');

	}

}
