<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Smslog extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sms_log';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');


}
