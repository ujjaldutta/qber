<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Locationpreferencehead extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_preference_head';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');
public function Polygon(){
		return $this->hasMany('App\Models\Locationpreferencepolygon', 'preference_id','id');

	}
	

}
