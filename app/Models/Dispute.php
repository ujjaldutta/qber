<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Dispute extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dispute';
	
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');
	 public function Customer(){
		return $this->hasOne('App\Models\Customer', 'id','customer_id');

	}
	public function Provider(){
		return $this->hasOne('App\Models\Provider', 'id','provider_id');

	}
	public function Providernotification(){

			return $this->hasMany('App\Models\Providernotification', 'service_id','service_id');
	}
	public function Customernotification(){

			return $this->hasMany('App\Models\Customernotification', 'customer_id','customer_id');
	}

	public function Service(){

			return $this->hasMany('App\Models\Service', 'id','service_id');
	}

	

}
