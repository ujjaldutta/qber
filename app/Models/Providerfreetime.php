<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Providerfreetime extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_free_timeslot';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	


      protected $hidden = array('prev_book_latitude','previous_book_longitude','next_book_latitude','next_book_longitude');

}
