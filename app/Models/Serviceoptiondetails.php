<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Serviceoptiondetails extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_option_details';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id','type_id');

	public function Servicetype(){
		return $this->hasOne('App\Models\Servicetype','id','type_id');

	}

	public function Serviceoption(){
		return $this->hasOne('App\Models\Serviceoption','id','service_option_id');

	}

}
