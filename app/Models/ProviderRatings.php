<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class ProviderRatings extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_rating';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	protected $hidden = array('id','provider_id','customer_id','service_id');

}
