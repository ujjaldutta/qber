<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Service extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service';
	//public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	 public function Booked(){
		return $this->hasOne('App\Models\Providerbookedtime', 'id','booking_slot_id');

	}

	public function Status(){
		return $this->hasOne('App\Models\Servicestatus', 'service_status_id','id');

	}

	public function Provider(){
		return $this->hasOne('App\Models\Provider', 'id','provider_id');

	}

	public function Providerprofile(){
		return $this->hasOne('App\Models\Providerprofile', 'provider_id','provider_id');

	}

	public function ServiceNotification(){

		return $this->hasMany('App\Models\Providernotification', 'service_id','id');

	}

	public function Cardetails(){
		return $this->hasMany('App\Models\Servicecardetails', 'service_id','id')->with('OptionDetails');
	}

	public function ServiceType(){

		return $this->hasMany('App\Models\Servicetype', 'id','type_id');

	}

}
