<?php
namespace App\Models;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Provider extends Model  implements AuthenticatableContract, CanResetPasswordContract{

   use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'providers';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
	protected $hidden = array('password','created_at','updated_at','remember_token','sms_code','preferred_location','moneyexpvalue','priority','device_id','location_factor','api_token','provider_start_daytime','provider_end_daytime','approve_status','is_active','ready_to_serve','is_online','sms_verified','banned_duration');

	

    public function Profile(){
		return $this->hasOne('App\Models\Providerprofile', 'provider_id','id');

	}

	 public function Balance(){
		return $this->hasMany('App\Models\Providerbalancehistory', 'provider_id','id');

	}
	public function Completedservice(){
		return $this->hasMany('App\Models\Appointments', 'provider_id','id');

	}

	public function Jobs(){
		return $this->hasMany('App\Models\Service', 'provider_id','id');

	}

	public function Companyaccount(){
		return $this->hasOne('App\Models\ProviderCompanyAccount', 'provider_id','id');

	}

	public function Company(){
		return $this->hasOne('App\Models\CompanyProvider', 'provider_id','id')->with('Details');

	}

	public function Ratings(){
		return $this->hasMany('App\Models\ProviderRatings', 'provider_id','id');

	}

	
	
	

}
