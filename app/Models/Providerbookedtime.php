<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Providerbookedtime extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_booked_timeslot';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');
	public function Service(){
		return $this->hasOne('App\Models\Service', 'booking_slot_id','id');

	}
}
