<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Providersattlehistory extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_sattle_history';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function Provider(){
		return $this->hasOne('App\Models\Provider', 'id','provider_id');

	}


	public function Admin(){
		return $this->hasOne('App\Models\Admin', 'id','admin_id');

	}

	public function Company(){
		return $this->hasOne('App\Models\Company', 'id','company_id');

	}

}
