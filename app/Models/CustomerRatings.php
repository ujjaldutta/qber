<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class CustomerRatings extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_rating';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	protected $hidden = array('id','provider_id','customer_id','service_id');

    public function Rating(){
        return $this->hasOne('App\Models\CustomerRatings', 'customer_id','id');

    }

}
