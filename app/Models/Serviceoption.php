<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Serviceoption extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_option';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function OptionDetails(){
		return $this->hasMany('App\Models\Serviceoptiondetails', 'service_option_id','id');

	}
}
