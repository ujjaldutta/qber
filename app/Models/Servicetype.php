<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Servicetype extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_types';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function Servicesetting(){
		return $this->hasMany('App\Models\Servicesettings', 'type_id');

	}

	public function Options(){
		return $this->hasMany('App\Models\Serviceoption','type_id','id')->with('OptionDetails');

	}

	

}
