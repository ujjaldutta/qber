<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Appointments extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appointments';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	public function Service(){
		return $this->hasOne('App\Models\Service', 'id','service_id');

	}
	public function Provider(){
		return $this->hasOne('App\Models\Provider', 'id','provider_id');

	}

}
