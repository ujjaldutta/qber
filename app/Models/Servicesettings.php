<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Servicesettings extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_settings';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');
	public function Servicetype(){
		return $this->hasOne('App\Models\Servicetype', 'id','type_id');

	}

}
