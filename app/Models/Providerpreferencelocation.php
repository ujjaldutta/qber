<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Providerpreferencelocation extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_preference_location';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
	//protected $hidden = array('id');

	 public function Polygon(){
		return $this->hasMany('App\Models\Locationpreferencepolygon', 'preference_id','preference_id');

	}
	public function Preferencehead(){
		return $this->hasMany('App\Models\Locationpreferencehead', 'id','preference_id');

	}

}
