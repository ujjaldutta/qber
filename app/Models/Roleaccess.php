<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Roleaccess extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin_role_access';
	public $timestamps = false;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   protected $hidden = array('id', 'role_id');



}
