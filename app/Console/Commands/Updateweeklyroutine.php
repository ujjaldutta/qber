<?php
/******************* This Cron job will maintain free slots of available providers **************/

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Providerfreetime;
use App\Models\Servicesettings;
use App\Models\Provider;


class Updateweeklyroutine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateweeklyroutine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly routine update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providers=Provider::where("sms_verified",'1')->where("approve_status",'1')
       
        ->where("is_active",'1')->where("is_online",'1')->get();
	//print_r($providers);exit;
       
		foreach($providers as $provider){
			
				// echo $providers->count();exit;
			$start_time=Servicesettings::select('code','value')->where('code','allowed_start_time')->where('type_id',$provider->type_id)->first();
			if(!$start_time)
			continue;

			$start_time=date("H:i:s",strtotime($start_time->value));
			
			
			$end_time=Servicesettings::select('code','value')->where('code','allowed_end_time')->where('type_id',$provider->type_id)->first();
			$end_time=date("H:i:s",strtotime($end_time->value));
			$duration=(strtotime($end_time)-strtotime($start_time))/60;
			

		

			$freetable=Providerfreetime::where('provider_id',$provider->id)->orderby('date','asc')->orderby('to_time','asc')->get();

			if($freetable->count()<=0){
				//create default timesheet
				
				/*$dates=array(date('Y-m-d'),date('Y-m-d', strtotime("+1 days")),date('Y-m-d', strtotime("+2 days")),date('Y-m-d', strtotime("+3 days")),date('Y-m-d', strtotime("+4 days")),date('Y-m-d', strtotime("+5 days")),date('Y-m-d', strtotime("+6 days")));

				foreach($dates as $date){
					$free=Providerfreetime::where('provider_id',$provider->id)->where('weekday',strtolower(date( "l", strtotime($date))))->first();
					
					if(!$free )
					{
						

							$updatefree=new Providerfreetime;
							$updatefree->provider_id=$provider->id;
							$updatefree->date=$date;
							$updatefree->weekday=strtolower(date( "l", strtotime($date)));
							$updatefree->from_time=date("H:i:s",strtotime($start_time));
							$updatefree->to_time=date("H:i:s",strtotime($end_time));
							$updatefree->total_duration=$duration;
							$updatefree->time_type='free';
							$updatefree->save();
					}
						

				}*/

			}else{
				//check existing timesheet and update date accordingly
				$dates=array(date('Y-m-d'),date('Y-m-d', strtotime("+1 days")),date('Y-m-d', strtotime("+2 days")),date('Y-m-d', strtotime("+3 days")),date('Y-m-d', strtotime("+4 days")),date('Y-m-d', strtotime("+5 days")),date('Y-m-d', strtotime("+6 days")));

//print_r($dates);exit;
				$free=Providerfreetime::where('provider_id',$provider->id)->get();
				
				foreach($free as $rec){
					
					
				foreach($dates as $date){
						if(strtolower(date( "l", strtotime($date)))==$rec->weekday
						&& $rec->date < date('Y-m-d')
						){
							$updatefree=Providerfreetime::where('id',$rec->id)->first();
							$updatefree->date=$date;
							$updatefree->save();
						}
					}

					$updatefree=Providerfreetime::where('id',$rec->id)->first();
					$duration=(strtotime($updatefree->to_time)-strtotime($updatefree->from_time))/60;
					$updatefree->total_duration=$duration;
					//echo $rec->id.",";
					$updatefree->save();

				}
				
				foreach($dates as $date){
					$free=Providerfreetime::where('provider_id',$provider->id)->where('weekday',strtolower(date( "l", strtotime($date))))->first();
					/*
					if(!$free )
					{
						

							$updatefree=new Providerfreetime;
							$updatefree->provider_id=$provider->id;
							$updatefree->date=$date;
							$updatefree->weekday=strtolower(date( "l", strtotime($date)));
							$updatefree->from_time=date("H:i:s",strtotime($start_time));
							$updatefree->to_time=date("H:i:s",strtotime($end_time));
							$updatefree->total_duration=$duration;
							$updatefree->time_type='free';
							$updatefree->save();
					}*/
					/*elseif(!$free->total_duration){

						
							$free->total_duration=(strtotime($free->to_time)-strtotime($free->from_time))/60;
							
							$free->save();

					}*/
						

				}
			}





			
			
		}
		
    }
}
