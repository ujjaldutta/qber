<?php
/******************* This Cron job will do auto cancel of appointments **************/
/******************* Set provider status according to job status and send notification**************/
/******************************************************** Update booking slot and service***************************************/

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Provider;
use App\Models\Customer;
use App\Models\Appointments;
use App\Models\Servicesettings;
use App\Models\Service;
use App\Models\Providerfreetime;
use App\Models\Providerbookedtime;
use App\Models\Providernotification;
use DB;
use App\Traits\ProviderServiceTrait;

class Updateservice extends Command
{
	use ProviderServiceTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateservice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update different service data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        
		$service=Service::where('service_status_id',1)->get();

		

	//auto cancel the service
	foreach($service as $rec){

		$min_duration=Servicesettings::select('code','value')->where('code','provider_wait_duration')->where('type_id',$rec['type_id'])->first();
		
		$cancel_duration=Servicesettings::select('code','value')
		->where('code','provider_wait_autocancel_duration')->where('type_id',$rec['type_id'])->first();

		

				
				
				$provider=Provider::where('id',$rec['provider_id'])->first();
				$singleservice=Service::where('id',$rec['id'])->first();
				
				if($rec['provider_waitto_cancel']=='1'){
					$duration=$cancel_duration->value;
					 $futuretime=date("Y-m-d H:i:s",strtotime("+".$duration." minutes", strtotime($singleservice->provider_waitto_cancel_time)) );
					if($futuretime<date("Y-m-d H:i:s")){
						$singleservice->service_status_id=12;
						$singleservice->save();

						$provider->is_online=0;
						$provider->ready_to_serve=0;
						//$provider->remember_token='';
						//$provider->api_token='';
						$provider->save();



						$pnotice=new Providernotification();
						$pnotice->provider_id=$provider->id;
						$pnotice->service_id=$singleservice->id;
						$pnotice->date=date('Y-m-d H:i:s');
						$pnotice->details='Job Auto Cancelled';
						$pnotice->notification_latitude=$provider->current_latitude;
						$pnotice->notification_longitude=$provider->current_longitude;
						$pnotice->invite_status='cancelled';
						$pnotice->service_notice_status='cancelled';
						$pnotice->price=$singleservice->expected_cost;
						$pnotice->save();

					


						$customer=Customer::with('Profile')->where('id',$singleservice->customer_id)->first();
				
						$msg=$provider->name." did not reply, please select another provider";
						
						if($customer->device_type=='ios' && $customer->device_id!=''){
				
						$arr = array('type'=>'ignored','id'=>$singleservice->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
						$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
					
						}elseif($customer->device_id!='' ){
							$arr = array('type'=>'ignored','id'=>$singleservice->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
							$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

						}

				
					}

				}else
				{

					$duration=$min_duration->value;
					//echo '--';
					$futuretime=date("Y-m-d H:i:s",strtotime("+".$duration." seconds", strtotime($singleservice->request_date)));
					//echo "----";
					//echo $singleservice->request_date;
					//echo '------';
					//echo date("Y-m-d H:i:s");
					//echo $futuretime;
					//exit;
					if($futuretime<date("Y-m-d H:i:s")){
						$singleservice->service_status_id=12;
						$singleservice->save();

						if($provider){
						$provider->is_online=0;
						$provider->ready_to_serve=0;
						//$provider->remember_token='';
						//$provider->api_token='';
						$provider->save();
						}



						$customer=Customer::with('Profile')->where('id',$singleservice->customer_id)->first();
				
						$msg=$provider->name." did not reply, please select another provider";
						if($customer->device_type=='ios' && $customer->device_id!=''){
				
						$arr = array('type'=>'ignored','id'=>$singleservice->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
						$status= $this->sendCustomerAPNS($msg,$arr,$customer->device_id);
					
						}elseif($customer->device_id!='' ){
							$arr = array('type'=>'ignored','id'=>$singleservice->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
							$status= $this->sendCustomerFCM($msg,$arr,$customer->device_id);

						}


						
					}

				}

				

		}
			











/**************************************************************************************************************************************************/











		$pid=0;
		$count=0;
		//update heading if provider "must head now" or passed heading time
        $services=Service::where('service_status_id',4)->orWhere('service_status_id',5)->orWhere('service_status_id',9)
		->whereDate("from_time",date("Y-m-d"))
        ->orderby("from_time","asc")->get();
		
		foreach($services as $service ){
		
		$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();
		$buffer_time=(int)$buffer_time->value;
		$provider=Provider::with("Profile")->where('id',$service->provider_id)->first();
		$booked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();



		///auto cancel job of previous day if not completed
			$service_date=$service->to_time;
			
			if($service_date<date("Y-m-d H:i:s")){
				$singleservice=Service::where("id",$service->id)->first();
				if($singleservice->service_status_id==4)
				$singleservice->service_status_id=8;

				if($singleservice->service_status_id==5)
				$singleservice->service_status_id=11;
				if($singleservice->service_status_id==9)
				$singleservice->service_status_id=10;
				
				$booking_slot_id=$singleservice->booking_slot_id;
				$singleservice->booking_slot_id=null;
				$singleservice->save();
				Providerbookedtime::where('id',$booking_slot_id)->delete();

				$provider->is_online=0;
				$provider->ready_to_serve=0;
				//$provider->remember_token='';
				//$provider->api_token='';
				$provider->save();


				$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)
				->where('service_notice_status','missedappointment')->count();

				
							//send notification to end the job to provider
							if($notice<=0 ){


										$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
										$msg="You have missed appointment ".$service->job_id." deadline! Repeated misses could terminate your account!";
										
											if($provider->device_type=='ios' && $provider->device_id!=''){
										
												$arr = array('type'=>'missedappointment','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
												$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
											
											}
											elseif($provider->device_id!='' )
											{
													$arr = array('type'=>'missedappointment','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
													$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);

											}

												
												$pnotice=new Providernotification();
												$pnotice->provider_id=$provider->id;
												$pnotice->service_id=$singleservice->id;
												$pnotice->date=date('Y-m-d H:i:s');
												$pnotice->details='You have missed appointment '.$singleservice->job_id.' deadline! Repeated misses could terminate your account!';
												$pnotice->notification_latitude=$provider->current_latitude;
												$pnotice->notification_longitude=$provider->current_longitude;
												
												$pnotice->service_notice_status='missedappointment';
												$pnotice->price=$singleservice->expected_cost;
												$pnotice->save();


								}
						

				continue;
			}
		








		/***************************************************************************************************************************************/

		
		
		if($service->service_status_id==4 && $provider->is_online=='1')
		{
		if($pid!=$provider->id)
		$count=0;
			
					$booked=Providerbookedtime::with("Service")->where('provider_id',$provider->id)
					//->where('from_time','<',date("Y-m-d H:s:i"))
					->where('id',$service->booking_slot_id)->first();


					
				if($booked)
				{	
							if(time()>strtotime($booked->to_time))
							{
								$free=Providerfreetime::whereRaw("('".date("H:i:s") ."' between from_time and to_time
									and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d")."')	")->first();
								if($free && $free->time_type=='free')
								$provider->provider_status='free';
								
								if($free && $free->time_type=='unavailable')
								$provider->provider_status='unavailable';

								$provider->save();
							}





							$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)
							->where('service_notice_status','arrivaldeadline')->count();

							$alert_time=strtotime('- '.(15+$booked->service->estimated_traveltime_heading).' minutes', strtotime($booked->service->to_time));

				
									//send notification to reach the job to provider before 15 mins
									if($notice<=0 && $alert_time<time()){


										$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
										
										$msg="Possible Delay Is Expected for Appointment ".$service->job_id.", please review your schedule and make sure to arrive there before ".$booked->from_time;
										$time=strtotime('- '.($booked->service->estimated_traveltime_heading).' minutes', strtotime($booked->service->to_time));
										
											if($provider->device_type=='ios' && $provider->device_id!=''){
										
												$arr = array('type'=>'arrivaldeadline','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"time"=>$time);
												$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
											
												}
												elseif($provider->device_id!='' )
												{
													$arr = array('type'=>'arrivaldeadline','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"time"=>$time);
													$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);

												}


												
												$pnotice=new Providernotification();
												$pnotice->provider_id=$provider->id;
												$pnotice->service_id=$service->id;
												$pnotice->date=date('Y-m-d H:i:s');
												$pnotice->details="Possible Delay Is Expected for Appointment ".$service->job_id.", please review your schedule and make sure to arrive there before ".$service->to_time;
												$pnotice->notification_latitude=$provider->current_latitude;
												$pnotice->notification_longitude=$provider->current_longitude;
												
												$pnotice->service_notice_status='arrivaldeadline';
												$pnotice->price=$service->expected_cost;
												$pnotice->save();
												

									}







									
							$minutes=(int)$service->estimated_traveltime_heading;
							$head_time=strtotime('-'.$minutes.' minutes', strtotime($booked->from_time));
							
							
							
							if( $head_time<time() ){

								
								
							

								$mode=($provider->profile->transport=='walk')?'walking':'driving';

								$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
								$origin='destinations='.$service->latitude.','.$service->longitude;
								$destination='&origins='.$provider->current_latitude.','.$provider->current_longitude;
								
								$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
								$data=json_decode(($data),true);
								if(!isset($data['rows'][0]['elements'][0]['duration'])) continue;
								
								$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
								$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);
								
								$duration=(int)$duration+(int)$buffer_time;
								 $work_start_time=date("Y-m-d H:i:s", strtotime('+'.($duration).' minutes', time()));
								 $work_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes',strtotime($work_start_time)));
								//$headtime=date("Y-m-d H:i:s", strtotime('-'.$service->estimated_traveltime_heading.' minutes',strtotime($booked->from_time)));
								
								 $freeslot=Providerfreetime::where('provider_id',$provider->id)
									->where('from_time','<=',date("H:i:s",strtotime($work_start_time)))
									->where('to_time','>=',date("H:i:s",strtotime($work_end_time)))
									->where('date','=',date("Y-m-d",strtotime($work_end_time)))
									->where('time_type','free')->count()
										
									;

								//print_r( $freeslot);exit;
								//echo $work_start_time ;
								//echo $booked->service->to_time;
								//exit;
								
								if($work_start_time>=$booked->service->to_time && $work_start_time>$booked->service->from_time){
									$singleservice=Service::where("id",$service->id)->first();
									$singleservice->short_duration=1;
									$singleservice->save();

								}
									
								//change booking slot time if free slot	
								if($freeslot>0 && $work_start_time<$booked->service->to_time && $work_start_time>$booked->service->from_time && $count==0){

									//echo date("H:i:s",$head_time);
									//echo date("H:i:s",time());
									//echo $booked->from_time;
									//echo $service->id;
									//echo '-------------<br />';
							

									$pid=$provider->id;
									
									$old_head_start_time=$booked->heading_start_time;
									$booked->heading_start_time=date("Y-m-d H:i:s", strtotime('-'.$duration.' minutes', strtotime($work_start_time)));
									
									$old_to_time=$booked->to_time;
									$old_from_time=$booked->from_time;
									
									$booked->from_time=$work_start_time;
									$booked->to_time=$work_end_time;
									$booked->save();


									//$singleservice=Service::where("id",$service->id)->first();
									//$old_estimated_traveltime_heading=$singleservice->estimated_traveltime_heading;
									//$singleservice->estimated_traveltime_heading=$duration;
									//$singleservice->save();
									
									//if next all service can reschedule then update current service else revert to previous time
									$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);
									if($status==false){
									//	echo "test";exit;
									$booked->heading_start_time=$old_head_start_time;
									$booked->from_time=$old_from_time;
									$booked->to_time=$old_to_time;
									$booked->save();
									
									//$singleservice->estimated_traveltime_heading=$old_estimated_traveltime_heading;
									//$singleservice->save();

									}

									$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
									->where(DB::raw("DATE(book_date)"),"=",date("Y-m-d",strtotime($booked->to_time)))
									->where('from_time',">",$booked->from_time)->orderby('from_time','asc')->first();
									
									$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)->where('service_notice_status','must head now')->count();

				
									//send notification to start heading the job to provider
									if($notice<=0 ){


										$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();
										$msg="It’s Time to Head to ".$customer->profile->name." for Appointment ".$service->job_id."! ";
										
											if($provider->device_type=='ios' && $provider->device_id!=''){
										
												$arr = array('type'=>'mustheadnow','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"address"=>$service->area_name);
												$status= $this->sendCustomerAPNS($msg,$arr,$provider->device_id);
											
												}
												elseif($provider->device_id!='' )
												{
													$arr = array('type'=>'mustheadnow','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name,"address"=>$service->area_name);
													$status= $this->sendCustomerFCM($msg,$arr,$provider->device_id);

												}


												
												$pnotice=new Providernotification();
												$pnotice->provider_id=$provider->id;
												$pnotice->service_id=$service->id;
												$pnotice->date=date('Y-m-d H:i:s');
												$pnotice->details='Start heading soon';
												$pnotice->notification_latitude=$provider->current_latitude;
												$pnotice->notification_longitude=$provider->current_longitude;
												
												$pnotice->service_notice_status='must head now';
												$pnotice->price=$service->expected_cost;
												$pnotice->save();
												

									}


								$count++;				
								}
								
								
							}
				}
			}
			elseif($service->service_status_id==5 || $service->service_status_id==9)
			{

				
							if($booked && date("H:i")>date("H:i",strtotime($booked->to_time)) ){
								$booked->to_time=date("Y-m-d H:i:s");
								$booked->total_duration=round((strtotime($booked->to_time)-strtotime($booked->from_time))/60);
								$booked->save();
								$status=$this->TimeupdateRescheduleProvider($service->booking_slot_id,$provider,$service);
							}

					

					///if time already passed and status is still on heading or working then set the provider status
					if($booked && time()>strtotime($booked->to_time))
					{
						$free=Providerfreetime::whereRaw("('".date("H:i:s") ."' between from_time and to_time
							and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d")."')	")->first();
						if($free && $free->time_type=='free')
						$provider->provider_status='free';
						
						if($free && $free->time_type=='unavailable')
						$provider->provider_status='unavailable';

						
					}
					elseif($service->service_status_id==5)
						{

							$provider->provider_status='working';
							$booked=Providerbookedtime::where('provider_id',$provider->id)->where('id',$service->booking_slot_id)->first();

							$notice=Providernotification::where('service_id',$service->id)->where('provider_id',$provider->id)->where('service_notice_status','endsoon')->count();

				
							//send notification to end the job to provider
							if($booked && date("H:i")>=date("H:i",strtotime('-5 minutes',strtotime($booked->to_time))) && $notice<=0 ){

								$customer=Customer::with('Profile')->where('id',$service->customer_id)->first();

								

				
								if($provider->device_type=='ios' && $provider->device_id!=''){
							
									$arr = array('type'=>'endsoon','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
									$status= $this->sendCustomerAPNS("Appoinment will end soon!",$arr,$provider->device_id);
								
									}elseif($provider->device_id!='' ){
										$arr = array('type'=>'endsoon','id'=>$service->id,"customer_name"=>$customer->profile->name,"provider_name"=>$provider->name);
										$status= $this->sendCustomerFCM("Appoinment will end soon!",$arr,$provider->device_id);

									}


									$pnotice=new Providernotification();
									$pnotice->provider_id=$provider->id;
									$pnotice->service_id=$service->id;
									$pnotice->date=date('Y-m-d H:i:s');
									$pnotice->details='Job will end soon';
									$pnotice->notification_latitude=$provider->current_latitude;
									$pnotice->notification_longitude=$provider->current_longitude;
									
									$pnotice->service_notice_status='endsoon';
									$pnotice->price=$service->expected_cost;
									$pnotice->save();
						
								
							}

							
						}
						elseif($service->service_status_id==9)
						{

							$provider->provider_status='heading';
						}


					$provider->save();

			


		}

		
		}




		/* $providers=Provider::where("sms_verified",'1')->where("approve_status",'1')
        ->where("is_active",'1')->where("is_online",'1')->get();

        //updatebooking
		foreach($providers as $provider){
			$service=Service::where('service_status_id',4)->first();


		}*/

        

		$allbook=Providerbookedtime::whereDate("book_date",date("Y-m-d"))->get();

		//delete booking if service table has no slot
		foreach($allbook as $book){
			$service=Service::where("booking_slot_id",$book->id)->count();
			if($service<=0)
			Providerbookedtime::where("id",$book->id)->delete();

		}

    }

/*
	private function updateNextBookings($booking){

		foreach($booking as $futurebook){
			//if()$futurebook


		}


	}*/
    //send notification to customer
	private	function sendCustomerFCM($title,$body,$id) {
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
				'to' => $id,
				/*'notification' => array (
						"body" => ($body),
						"title" => $title,
						"icon" => "myicon"
				),*/
				'data'=>$body
		);

		

		$fields = json_encode ( $fields );
		$headers = array (
				'Authorization: key=' . env('FCM_KEY',"AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg"),
				'Content-Type: application/json'
		);
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		return $result;
		}


	private function sendCustomerAPNS($title,$body,$id){

		// Put your device token here (without spaces):
		$deviceToken = $id;
		// Put your private key's passphrase here:
		$passphrase = env('APNS_KEY',"xxxxxxx");
		// Put your alert message here:
		$message = $body;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck_file.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'body' => $message,
				'action-loc-key' => $title,
			),
			'badge' => 2,
			'sound' => 'oven.caf',
			);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		if (!$result)
			return false;
		else
			return $result;
		// Close the connection to the server
		fclose($fp);

	}

	//get distance and duration from any latitude and longitude
	private function httpGet($url)
	{
		$ch = curl_init();  
		//echo $url=urlencode($url);
		curl_setopt($ch,CURLOPT_URL,($url));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	//  curl_setopt($ch,CURLOPT_HEADER, false); 
	 
		$output=curl_exec($ch);
		$info = curl_getinfo($ch);
		
	 
		curl_close($ch);
		return $output;
	}
	
}
