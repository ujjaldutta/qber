<?php
/******************* This Cron job will update providers free timetable and schedule **************/

namespace App\Console\Commands;
use App\Models\Providerfreetime;
use App\Models\Providertimehistory;
use App\Models\Providerfreetimehistory;
use App\Models\Servicesettings;
use App\Models\Provider;
use App\Models\Providerschedule;
use Illuminate\Console\Command;

use App\Traits\ProviderServiceTrait;

class Updatetime extends Command
{
	use ProviderServiceTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updatetime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Freetime for all active providers on daily basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
     
		$time_start = microtime(true); 
		
					
						$freetime=Providerfreetime::where("provider_id",'>',0)->get();
						
						
						//$freetime=DB::select("select * from `provider_free_timeslot` where date(`date`) = '".date('Y-m-d')."' limit 0,1000");
						$totalfreetime=0;


						foreach($freetime as $free){
							
							
							$totalfreetime =(strtotime($free->to_time))-(strtotime($free->from_time));
							$totalfreetime=round(abs($totalfreetime) / 60,2);
							
							$freeslot=Providerfreetime::find($free->id);
							if($freeslot){
								$freeslot->total_duration=($totalfreetime>0)?$totalfreetime:0;

								for($i=0;$i<7;$i++){
										$date=date("Y-m-d", strtotime('+'.$i.' days', time()));
										$day=strtolower(date('l',strtotime($date)));
										if($freeslot->weekday==$day)
										$freeslot->date=$date;

									}
								
								$freeslot->save();

							}
							}
					

							
		$this->update_free_table_history();			
		
		$this->update_free_table();	
		

		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start);
		echo 'Total Execution Time:'.$execution_time.' Seconds'. PHP_EOL;
		
    }

	private function update_free_table(){
		$providers=Provider::where('is_online','1')
		->where('sms_verified','1')
		->where('approve_status','1')
		->where('is_active','1')
		->get()->toArray();
		

		$data_array=array();
		for($i=0;$i<7;$i++){
			$data_array[]=date("Y-m-d", strtotime('+'.$i.' days', time()));

		}

		

		foreach($providers as $provider){
			$start=Servicesettings::select('code','value')->where('code','allowed_start_time')->where('type_id',$provider['type_id'])->first();
			$end=Servicesettings::select('code','value')->where('code','allowed_end_time')->where('type_id',$provider['type_id'])->first();
			
			$count=Providerfreetime::where("provider_id",$provider['id'])->count();
			if($count<=0){
				

					foreach($data_array as $date)
					{
					$addfreetime=new Providerfreetime();
					$addfreetime->provider_id=$provider['id'];
					$addfreetime->date=$date;
					$addfreetime->from_time=date("Y-m-d H:i:s", strtotime($date." ".$start->value));
					$addfreetime->to_time=date("Y-m-d H:i:s", strtotime($date." ".$end->value));
					
					
					$addfreetime->time_type='free';
					$addfreetime->weekday=strtolower(date('l',strtotime($date)));
					$addfreetime->save();

					
					}


			}


		Providerfreetime::where("provider_id",$provider['id'])->where("from_time","=","to_time")->delete();


		$provider=Provider::where('id',$provider['id'])->first();
		Providerschedule::where("provider_id",$provider['id'])->delete();
		foreach($data_array as $date)
		{
		$schedule=$this->getCurrentSchedule($provider,$date." ".$start->value);

			
			for($i=0;$i<count($schedule);$i++){
				
				$pschedule=new Providerschedule;
				$pschedule->provider_id=$provider->id;
				$pschedule->date=$date;
				$pschedule->from_time=$schedule[$i]['schedule']['from_time'];
				$pschedule->to_time=$schedule[$i]['schedule']['to_time'];
				$pschedule->time_type=$schedule[$i]['schedule']['time_type'];

				if(isset($schedule[$i]['schedule']['current_work_start_time']))
				$pschedule->work_start_time=$schedule[$i]['schedule']['current_work_start_time'];
				elseif(isset($schedule[$i]['schedule']['work_start_time']))
				$pschedule->work_start_time=$schedule[$i]['schedule']['work_start_time'];

				
				if(isset($schedule[$i]['schedule']['current_work_end_time']))
				$pschedule->work_end_time=$schedule[$i]['schedule']['current_work_end_time'];
				elseif(isset($schedule[$i]['schedule']['work_end_time']))
				$pschedule->work_end_time=$schedule[$i]['schedule']['work_end_time'];
				
				if(isset($schedule[$i]['schedule']['work_duration']))
				$pschedule->work_duration=$schedule[$i]['schedule']['work_duration'];
				if(isset($schedule[$i]['schedule']['heading_time']))
				$pschedule->heading_time=$schedule[$i]['schedule']['heading_time'];
				if(isset($schedule[$i]['schedule']['service_id']))
				$pschedule->service_id=$schedule[$i]['schedule']['service_id'];
				if(isset($schedule[$i]['schedule']['job_id']))
				$pschedule->job_id=$schedule[$i]['schedule']['job_id'];
				if(isset($schedule[$i]['schedule']['shorttime']))
				$pschedule->shorttime=$schedule[$i]['schedule']['shorttime'];
				if(isset($schedule[$i]['schedule']['free_id']))
				$pschedule->free_id=$schedule[$i]['schedule']['free_id'];
				if(isset($schedule[$i]['schedule']['duration']))
				$pschedule->duration=$schedule[$i]['schedule']['duration'];
				
				$pschedule->save();

			}
		}

		
		}
		

	}
    
    private function update_free_table_history(){

		$freeslot=Providerfreetime::whereDate("date",date('Y-m-d'))->get();
		
		foreach($freeslot as $slot){
			
			$from_time=date("Y-m-d H:i:s",strtotime($slot->date." ".$slot->from_time));
			$to_time=date("Y-m-d H:i:s",strtotime($slot->date." ".$slot->to_time));
			
			$free=Providerfreetimehistory::where('provider_id',$slot->provider_id)
			->where('from_time',$from_time)
			->where('to_time',$to_time)
			->where('date',$slot->date)
			->get();

			
			if($free->count()<=0){
						
				$hslot=new Providerfreetimehistory();
				$hslot->provider_id=$slot->provider_id;
				$hslot->date=$slot->date;
				$hslot->from_time=$from_time;
				$hslot->to_time=$to_time;
				$hslot->total_duration=$slot->total_duration;
				$hslot->prev_book_latitude=$slot->prev_book_latitude;
				$hslot->previous_book_longitude=$slot->previous_book_longitude;
				$hslot->next_book_latitude =$slot->next_book_latitude ;
				$hslot->next_book_longitude=$slot->next_book_longitude ;
				$hslot->time_type=$slot->time_type ;
				$hslot->save();

			}

			//$free=Providerfreetime::where('id',$slot->id)->delete();
		}

		
	}
	
}
