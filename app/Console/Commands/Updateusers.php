<?php
/******************* This Cron job will update users status or set provider offline and update its details **************/
namespace App\Console\Commands;
use App\Models\Provider;
use App\Models\Providerfreetime;
use App\Models\Customer;
use App\Models\Appointments;
use App\Models\Providerbookedtime;
use App\Models\Service;
use App\Models\Servicesettings;
use DB;
use Illuminate\Console\Command;

class Updateusers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateusers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update different users data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providers=Provider::where("sms_verified",'1')->where("approve_status",'1')
        ->where("is_active",'1')->get();

        //expire login if more than 5 minutes and set its status
		foreach($providers as $provider){

	
			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$provider->type_id)->first();
			
			$buffer_time=(int)$buffer_time->value;
			
			$futureDate = $provider->api_token+(60*5);
			$diff=(time()-$futureDate)/60;
			$user=Provider::find($provider->id);
			if($diff>10080)
			{
				
				$user->is_online=0;
				$user->remember_token='';
				$user->api_token='';
				$user->ready_to_serve=0;
				
				
			}

		

		//if head time passed or started then update provider based on it
		$booked=Providerbookedtime::with('Service')->where("provider_id",$provider->id)
		->whereDate("book_date",date("Y-m-d"))
		->whereHas('Service', function ($query) {
				$query->where("service_status_id",4);
			})
		->orderby("from_time","asc")->first();
		
		if($booked){
			
			$service=Service::where("booking_slot_id",$booked->id)->where("provider_id",$provider->id)->first();
			
			if($service->service_status_id==4  && time()<strtotime($booked->from_time)){
				$minutes=(int)$service->estimated_traveltime_heading+$buffer_time;
				$head_time=date("H:i:s", strtotime('-'.$minutes.' minutes', strtotime($booked->from_time)));
				//date("H:i:s");
				if($head_time<date("H:i:s")){
					$user->provider_status='must head now';
				}
				
			}
			else{

				$freetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",date("Y-m-d"))
				->whereRaw("'".date("H:i:s") ."' between  from_time and to_time")->first();

				if($freetime->time_type=='free' && $user->is_online=='1' && $user->ready_to_serve=='1') $user->provider_status='free';
				elseif($freetime->time_type=='unavailable')
				$user->provider_status='break';
				else $user->provider_status='unavailable';

			}

		}else{

			$freetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",date("Y-m-d"))
			->whereRaw("'".date("H:i:s") ."' between  from_time and to_time")		
			->where('time_type','free')->count();

			$ufreetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",date("Y-m-d"))
			->whereRaw("'".date("H:i:s") ."' between  from_time and to_time")		
			->where('time_type','unavailable')->count();

			
			if($freetime>0 && $user->is_online=='1' && $user->ready_to_serve=='1')
			$user->provider_status='free';
			elseif($ufreetime>0)
			$user->provider_status='break';
			else $user->provider_status='unavailable';

		}
		
		$user->save();



		
			

		}

	
		//update job duration in appointment
			$app=Appointments::where("duration","<=",0)->get();
			
			foreach($app as $appointments){
				$appointment=Appointments::find($appointments->id);
				$appointment->duration=(strtotime($appointments->service_end_time)-strtotime($appointments->service_start_time))/60;
				$appointment->save();
			}
			

	
		
		
        
		//set customer logout after 5 mins
		$customers=Customer::where("sms_verified",'1')->where("is_active",'1')->where("is_online",'1')->get();

		foreach($customers as $customer){
			
			$futureDate = $customer->api_token+(60*5);
			$diff=(time()-$futureDate)/60;

			if($diff>10){
				//$user=Customer::find($customer->id);
				//$user->is_online=0;
				//$user->remember_token='';
				//$user->api_token='';
				//$user->save();
			}
			

		}

		//update banned customer so he can login
		$customers=Customer::where("banned_duration",">",0)->get();

		foreach($customers as $customer){

			$updatetime=$customer->updated_at;
			$currenttime=date("Y-m-d H:i:s");

			$duration=(strtotime($currenttime)-strtotime($updatetime))/3600;
			if($duration < $customer->banned_duration){
			$cust=Customer::find($customer->id);
			$cust->banned_duration=0;
			$cust->save();
			}

		}
		

        
    }

    //get distance and duration from any latitude and longitude
	private function httpGet($url)
	{
		$ch = curl_init();  
		//echo $url=urlencode($url);
		curl_setopt($ch,CURLOPT_URL,($url));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	//  curl_setopt($ch,CURLOPT_HEADER, false); 
	 
		$output=curl_exec($ch);
		$info = curl_getinfo($ch);
		
	 
		curl_close($ch);
		return $output;
	}
}
