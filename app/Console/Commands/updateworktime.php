<?php
/******************* This Cron job will update work time, priority logic and moneyvalue exp **************/
namespace App\Console\Commands;
use DB;
use Illuminate\Console\Command;
use App\Models\Provider;
use App\Models\Providerworktime;
use App\Models\Providerfreetime;
use App\Models\Providertimehistory;
use App\Models\Appointments;
use App\Models\Servicesettings;

use App\Traits\ProviderServiceTrait;

class updateworktime extends Command
{
	use ProviderServiceTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateworktime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Worktime for all active providers on daily basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		$providers=Provider::where("sms_verified",'1')->where("approve_status",'1')->where("is_active",'1')
		->where('ready_to_serve','1')->where('is_online','1')
		//->where("id",23)
		->get();
		$time_start = microtime(true); 
		foreach($providers as $provider){
			
			
			$appointments=Appointments::where('service_status_id','6')->where('provider_id',$provider->id)->get();

			//if(!$appointments)
			//continue;

			foreach($appointments as $appo){
					$timehistory=Providertimehistory::where('provider_id',$provider->id)->where('service_id',$appo->service_id)->get();
					if($timehistory->count()<=0 && $appo->service_id!='')
					{
						
						$timehistory=new Providertimehistory();
						$timehistory->provider_id=$provider->id;
						$timehistory->service_id=$appo->service_id;
						$timehistory->save();
						
						
					}

			}
			
			$timehistory=Providertimehistory::where('provider_id',$provider->id)->get();
			
				
		foreach($timehistory as $history){
			
				
						
					
						$workingtime=$this->getWorktimeDuration($history->id);
					
						$history->work_time=$workingtime;
						//echo "pid:".$provider->id;
						//echo "totaltime".$totaltime;
						//echo "duration".$duration;
						//echo '<br /> /r/n';
						$history->save();
						
						
						
				}

			//$pastdate=date('Y-m-d H:i:s', strtotime('-500 hours', time()));
	 			
		//$wprovider=DB::select('select sum(work_time) as totaltime from provider_time_log  where provider_time_log.provider_id="'.$provider->id.'" and provider_time_log.date >="'.$pastdate.'" ');
		
			//total work duration of last 500 hours
			$workingtime=$this->getworkDuration($provider->id);
		
		
			$provider->moneyexpvalue=0;
			$provider->location_factor=0;
			//if($wprovider && $wprovider[0]->totaltime>0)
			//{
				//print_r($wprovider);exit;
			$provider->work_time=$workingtime;
			//}
			//else
			//$provider->work_time=0;
			
			$provider->priority=0;

			$provider->save();
			
				
		//$this->updateworktime($provider->id);
		//if($providers->is_online=='1')
		$this->updatemoneypoints($provider->id);



		$worktime=Providerworktime::where('provider_id',$provider->id)
			->whereDate('date',date("Y-m-d"))->where("type","online")
			
			->orderby("from_time","desc")->first();
							if($worktime){
								$worktime->to_time=date("Y-m-d H:i:s");
								$worktime->save();
								}else{

									
										$timehistory=Providertimehistory::where("provider_id",$provider->id)->whereDate("date",date("Y-m-d"))->first();
										if(!$timehistory){
											$timehistory=new Providertimehistory();
											$timehistory->provider_id=$provider->id;
											$timehistory->save();
										}


									$worktime=new Providerworktime;
									$worktime->provider_id=$provider->id;
									$worktime->provider_timehistory_id=$timehistory->id;
									$worktime->from_time=date("Y-m-d H:i:s");
									$worktime->to_time=date("Y-m-d H:i:s");
									//$worktime->service_id=$service->id;
									$worktime->date=date("Y-m-d H:i:s");
									$worktime->type='online';
									$worktime->save();



								}
		
		

		}





		
		
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start);
		echo 'Total Execution Time:'.$execution_time.' Seconds'. PHP_EOL;


	
        
    }



//get money of last 500 hours
 private function  updatemoneypoints($pid){



		$priority_static=Servicesettings::where("code","priority_factor_static_A")->first();
		$priority_static_a=$priority_static->value;

		$priority_static=Servicesettings::where("code","priority_factor_static_B")->first();
		$priority_static_b=$priority_static->value;

		$location_static=Servicesettings::where("code","location_factor_static_A")->first();
		$location_static_a=$location_static->value;

		$location_static=Servicesettings::where("code","location_factor_static_B")->first();
		$location_static_b=$location_static->value;

		$pastdate=date('Y-m-d H:i:s', strtotime('-500 hours', time()));
		$sql='select * from provider_time_log  left join appointments as app on(provider_time_log.service_id=app.service_id and provider_time_log.provider_id=app.provider_id) where provider_time_log.provider_id="'.$pid.'" and provider_time_log.date >="'.$pastdate.'" and app.service_status_id=6 order by app.service_end_time asc';
		$money_provider=DB::select($sql);

		$worktime=0;
		$amount=0;
		$expval=0;
		$driveexpval=0;
		$totalworktime=0;


		
		
		$current_worktime=$this->getworkDuration($pid);


		foreach($money_provider as $rec){

			
			
			$time=DB::select('select sum(work_time) as work_time from provider_time_log  
				where provider_time_log.provider_id="'.$pid.'" and provider_time_log.date >="'.$pastdate.'" order by provider_time_log.id desc');
				
				
			$amount=$rec->paid_amount;
			//$worktime=round(abs(time() - strtotime($rec->service_end_time)) / 3600,2);
			
			$worktime=($current_worktime-$rec->work_hours_oncomplete);
			
			if($amount >0 && $worktime>0){
				
					$pow=(float)(-$worktime*$priority_static_b*pow(10, -4));
					$expval +=$amount*$priority_static_a*exp($pow);
					/*echo "Amount ".$amount;
					echo "<br />";
					echo "priority_static_a ".$priority_static_a;
					echo "<br />";
					echo "priority_static_b ".$priority_static_b;
					echo "<br />";
					echo "current work time ".$current_worktime;
					echo "<br />";
					echo "work time on job complete ".$rec->work_hours_oncomplete;
					echo "worktime ".$worktime;
					echo "<br />";
					echo "expval: ".$expval;
					echo "<br />=";*/

					
					
					
				}

				//print_r($rec);

			$drivetime=$rec->drive_time;

				if($drivetime >0 ){
				
					//$pow=(float)-$worktime*70*pow(10, -4);
					$driveexpval +=$location_static_a*exp(-$location_static_b*$drivetime);
					
					
				}

			

		}

	
			$provider=Provider::where("id",$pid)->first();
			$expval=(float)substr((string)($expval), 0, 10);
			$driveexpval=(float)substr((string)($driveexpval), 0, 10);
			
			$provider->moneyexpvalue=number_format($expval, 3, '.', '');
			$provider->location_factor=number_format($driveexpval, 3, '.', '');
			
			$num=$driveexpval*$expval;
			$provider->priority=number_format($num, 3, '.', '');;

			$provider->save();
	


 }



 private function getWorktimeDuration($id){

		//$pastdate=date('Y-m-d H:i:s', strtotime('-500 hours', time()));
		
		$data=Providerworktime::where("provider_timehistory_id",$id)
				
					->select("id","from_time","to_time")->whereRaw(" from_time is not null and to_time is not null")->orderby('from_time','asc')
					->get()->unique('from_time','to_time');
		$duration=0;
		if(count($data)>0){
						
					$i=0;
					$data=$data->toArray();

					
					$i=0;
					$totcount=count($data);
					foreach($data as $slot){
						
							$to_time=$slot['to_time'];
							$from_time=$slot['from_time'];

						
						for($j=$i+1;$j<($totcount);$j++){
							
							
								if( isset($data[$j]) && $from_time<=$data[$j]['from_time']  && $to_time>= $data[$j]['to_time'] )
								{
									
									unset($data[$j]);
									

								}
								

						}
						
						$i++;
					}

					
					$data=array_values($data);
				


					$i=0;
					foreach($data as $slot){
						if(isset($data[$i+1]) && $data[$i+1]['from_time']<=$slot['to_time'] && $data[$i+1]['to_time']<=$slot['to_time'])
							{
								
								unset($data[$i+1]);
								//break;
							}
						
							$i++;
					}
					$data=array_values($data);
					//print_r($data);
					
					
					$i=0;
					foreach($data as $slot){
							
							if(isset($data[$i+1]) && $slot['to_time'] >= $data[$i+1]['from_time'] && $data[$i+1]['to_time']>$slot['to_time'])
							{
								$data[$i+1]['from_time']=$slot['to_time'];
								
							}
						
					$i++;
					}
				//print_r($data);
					$duration=0;

					foreach($data as $slot){
						

						$duration +=strtotime($slot['to_time'])-strtotime($slot['from_time']);
					}
					$duration=(round(abs($duration ) / 60,2));

		}

		return $duration;

  }  



				
}
