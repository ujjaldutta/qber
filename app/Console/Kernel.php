<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\updateworktime',
        'App\Console\Commands\Updatetime',
        'App\Console\Commands\Updateweeklyroutine',
		'App\Console\Commands\Updateusers',
        'App\Console\Commands\Updateservice',
        'App\Console\Commands\Updatebooking',
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
	$schedule->command('command:updateworktime')->everyMinute();
	$schedule->command('command:updatetime')->everyMinute();
	$schedule->command('command:updateweeklyroutine')->everyMinute();
	$schedule->command('command:updateusers')->everyMinute();
	$schedule->command('command:updateservice')->everyMinute();	
	$schedule->command('command:updatebooking')->everyThirtyMinutes();
	
	
	//$schedule->command('command:updateweeklyroutine')->Daily();
	
         
        
    }

	

    

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
