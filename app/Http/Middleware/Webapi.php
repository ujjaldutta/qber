<?php namespace App\Http\Middleware;
use App\Helpers\Helper;
use Closure;
use Illuminate\Support\Facades\Auth;
class Webapi {

    public function handle($request, Closure $next)
    {
		 config()->set('auth.defaults.guard','api');
			
        if ( Auth::check() )
        {

			
            return $next($request);
        }else{
			//return false;
            return redirect('api/auth/login');
        }

        //return redirect('company/dashboard');

    }

}
