<?php namespace App\Http\Middleware;
use App\Helpers\Helper;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Adminlog;
class Admin {

    public function handle($request, Closure $next)
    {

        if ( Auth::check() && Auth::user()->isAdmin() )
        {

			$currentAction = \Route::currentRouteAction();
			list($controller, $method) = explode('@', $currentAction);
			$controller = strtolower(str_replace("Controller","",preg_replace('/.*\\\/', '', $controller)));
		
			
			if(!Helper::checkpermission($controller.$method)){
				
			return redirect('admin/dashboard');
			}else{

						if(Auth::user()->id!='1'){
							$log=new Adminlog();
							$log->admin_id=Auth::user()->id;
							$log->controller=$controller;
							$log->action=$method;
							$log->ip=$_SERVER['REMOTE_ADDR'];
							$log->date=date("Y-m-d H:i:s");
							$log->save();

						}

			}
		

			
            return $next($request);
        }else{
            return redirect('admin/auth/login');
        }

        return redirect('admin/dashboard');

    }

}
