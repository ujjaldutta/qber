<?php namespace App\Http\Middleware;
use App\Helpers\Helper;
use Closure;
use Illuminate\Support\Facades\Auth;
class Company {

    public function handle($request, Closure $next)
    {
		 config()->set('auth.defaults.guard','company');
			
        if ( Auth::check() && Auth::user()->isCompany() )
        {

						
            return $next($request);
        }else{
			
            return redirect('company/auth/login');
        }

        return redirect('company/dashboard');

    }

}
