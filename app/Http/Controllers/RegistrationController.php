<?php

/*************************************************** Registration for Customer and provider *******************************************************************/
namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Customerprofile;
use App\Models\Provider;
use App\Models\Providerprofile;
use App\Models\Servicesettings;
use App\Models\Providerpreferencelocation;
use App\Models\Locationpreferencehead;
use App\Models\Smslog;

use Illuminate\Http\Request;
use Response;
use Hash;
use App\Traits\CustomerServiceTrait;
use App\Traits\ProviderServiceTrait;
use Illuminate\Support\Facades\Auth;
use Session;

class RegistrationController extends Controller{

	
	use CustomerServiceTrait;
	use ProviderServiceTrait;
	
public function __construct(){
		
	}


public function index(){

	echo "nothing here";

}

/***********************  web auth not required for app *******************************/

 public function authenticate($phone,$password)
    {
        if (Auth::guard('api')->attempt(['phone' => $phone, 'password' => $password])) {
            // Authentication passed...
            return 1;
        }
    }

/********************************************************************** Provider section **********************************************************/

/***********************  check if provider setting already create email address in profile *******************************/

public function providerEmailVerify(Request $request){

	$email = $request->input('email');
	$count = Provider::where('email', '=', $email)->count();

	if($count>0){
		$response['success'] = 1;
		$response['msg'] = 'Duplicate Email.';
		

	}else{
		$response['success'] = 0;
		$response['msg'] = 'Valid Email.';

	}

	return Response::json($response);
	die;
}

/***********************  provider signup and admin approval and sms verify is required *******************************/

public function providersignup(Request $request){

	
	$password = $request->input('password');
	$phone = $request->input('phone');
	$name = $request->input('name');
	$email = $request->input('email');
	$device_id=$request->input('device_id');

	if($password=='' || $phone==''|| $name=='' || $request->input('type_id')=='' || $request->input('latitude')=='' || $request->input('longitude')=='' ||
	$request->input('date_of_birth')=='' || $request->input('gender')=='' || $request->input('nationality')=="" ||  $request->input('internet_access')==''
	|| $request->input('transport')=='' || $device_id==''
	){
		$response['success'] = 1;
		$response['msg'] = 'Invalid Details.';
		return Response::json($response);
		die;
	}

	$count = Provider::where('phone', '=', $phone)->count();

	//recreate sms unverified providers
	if($count>0){
		$provider=Provider::where('phone', '=', $phone)->first();
		if($provider->sms_verified==0){
			$provider->delete();
			$count=0;
		}
	}

	if($count<=0){

		$x = 3; 
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$smscode = rand($min, $max);

		$x = 6; // Amount of digits
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$accountno = rand($min, $max);

			$provider=new Provider();
			
			$provider->password=Hash::make($password);
			$provider->phone=$phone;
			$provider->name=$name;
			if($email!='')
			$provider->email=$email;
			$provider->type_id=$request->input('type_id');
			$provider->is_active=1;
			$provider->approve_status=0;
			$provider->sms_verified=0;
			$provider->ready_to_serve=0;

			if($request->input('latitude') && $request->input('longitude'))
				{
					
					$provider->current_latitude=$request->input('latitude');
					$provider->current_longitude=$request->input('longitude');
					$provider->base_latitude=$request->input('latitude');
					$provider->base_longitude=$request->input('longitude');
					$address=$this->getLatlngaddress($request->input('latitude'),$request->input('longitude'));
					$provider->base_address=$address['formatted_address'];
					
				}
		
			$provider->sms_code=$smscode;
			$provider->account_no=$accountno;
			$provider->location_preference='actual';
			if($request->input('device_id'))
			$provider->device_id=$request->input('device_id');
			
			$provider->save();

			$profile=new Providerprofile();
			$profile->provider_id=$provider->id;
			if ($request->input('date_of_birth'))
			$profile->date_of_birth=$request->input('date_of_birth');
			if ($request->input('gender'))
			$profile->gender=$request->input('gender');
			if ($request->input('nationality'))
			$profile->nationality=$request->input('nationality');
			if ($request->input('internet_access'))
			$profile->internet_access=$request->input('internet_access');
			if ($request->input('transport'))
			$profile->transport=$request->input('transport');

			if ($request->input('your_offer'))
			$profile->your_offer=$request->input('your_offer');
			if ($request->input('year_of_experience'))
			$profile->year_of_experience=$request->input('year_of_experience');
			if ($request->input('live_in_qatar'))
			$profile->live_in_qatar=$request->input('live_in_qatar');
			if ($request->input('work_preference'))
			$profile->work_preference=$request->input('work_preference');
			if ($request->input('language_preference'))
			$profile->language_preference=$request->input('language_preference');
			if ($request->input('work_in_company'))
			$profile->work_in_company=$request->input('work_in_company');
			if ($request->input('shop_name'))
			$profile->shop_name=$request->input('shop_name');


			$amount=Servicesettings::select('code','value')->where('code','provider_max_due_amount')
			->where('type_id',$request->input("type_id"))->first();
			$profile->max_due_amount=$amount->value;
			/*if($request->input('work_preference')){
			$preference=new Providerpreferencelocation();
			$preference->provider_id=$provider->id;
			$preference->preference_id=$request->input('work_preference');
			$preference->save();
			}*/
			/*************************************Profile Picture********************************************/
			$admin_profile_image = "";
			if ($request->hasFile('admin_profile_image')) {
				$file = $request->file('admin_profile_image');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$admin_profile_image = sha1($filename . time()) . '.' . $extension;
				$profile->admin_profile_image = "media/provider/".$admin_profile_image;
			}

			
			if ($request->hasFile('admin_profile_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('admin_profile_image')->move($destinationPath, $admin_profile_image);
			}
			/*************************************Tools Picture ********************************************/
			$tools_use_image = "";
			if ($request->hasFile('tools_use_image')) {
				$file = $request->file('tools_use_image');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$tools_use_image = sha1($filename . time()) . '.' . $extension;
				$profile->tools_use_image = "media/provider/".$tools_use_image;
			}

			
			if ($request->hasFile('tools_use_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('tools_use_image')->move($destinationPath, $tools_use_image);
			}


			/*************************************Shop Picture ********************************************/	
			
			$shop_image = "";
			if ($request->hasFile('shop_image')) {
				$file = $request->file('shop_image');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$shop_image = sha1($filename . time()) . '.' . $extension;
				$profile->shop_image = "media/provider/".$shop_image;
			}

			
			if ($request->hasFile('shop_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('shop_image')->move($destinationPath, $shop_image);
			}

			/*************************************CV File ********************************************/	
			
			$cv_file = "";
			if ($request->hasFile('cv_file')) {
				$file = $request->file('cv_file');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$cv_file = sha1($filename . time()) . '.' . $extension;
				$profile->cv_file = "media/provider/".$cv_file;
			}

			
			if ($request->hasFile('cv_file')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('cv_file')->move($destinationPath, $cv_file);
			}


			/*************************************Car Registration Image ********************************************/	
			
			$car_registration_image = "";
			if ($request->hasFile('car_registration_image')) {
				$file = $request->file('car_registration_image');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$car_registration_image = sha1($filename . time()) . '.' . $extension;
				$profile->car_registration_image = "media/provider/".$car_registration_image;
			}

			
			if ($request->hasFile('car_registration_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('car_registration_image')->move($destinationPath, $car_registration_image);
			}


			/*************************************Driving License Image ********************************************/	
			
			$driving_license_image = "";
			if ($request->hasFile('driving_license_image')) {
				$file = $request->file('driving_license_image');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$driving_license_image = sha1($filename . time()) . '.' . $extension;
				$profile->driving_license_image = "media/provider/".$driving_license_image;
			}

			
			if ($request->hasFile('driving_license_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('driving_license_image')->move($destinationPath, $driving_license_image);
			}


			/*************************************Quatar ID Image ********************************************/	
			
			$identity_image = "";
			if ($request->hasFile('identity_image')) {
				$file = $request->file('identity_image');
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension();
				$identity_image = sha1($filename . time()) . '.' . $extension;
				$profile->identity_image = "media/provider/".$identity_image;
			}

			
			if ($request->hasFile('identity_image')) {
				
				$destinationPath = public_path() . '/media/provider/' ;
				$request->file('identity_image')->move($destinationPath, $identity_image);
			}

			
			$profile->save();


			$head=Locationpreferencehead::get();
			foreach($head as $rec){
				$plocation=new Providerpreferencelocation;
				$plocation->provider_id=$provider->id;
				$plocation->preference_id=$rec->id;
				$plocation->save();

				}
			
			
			$res=$this->sendsms($phone,"Qber Code: ".$smscode,$provider->device_id);


			$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

			$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();


			$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
			$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$provider->phone)->where('send_date','>=',$future_time)->count();

			$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
			$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$provider->phone)->where('send_date','>=',$future_time)->count();

		
			

			if($res)
			{			
			$response['success'] = 0;
			$response['msg'] = 'Registration Successful';
			$response['data'] =array("id"=>$provider->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);
			}else
			{
			$response['success'] = 0;
			$response['msg'] = 'Registration Successful. Unable to send SMS';
			$response['data'] =array("id"=>$provider->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);
			}


			
		
			
	}
	else
			{
				$response['success'] = 2;
				$response['msg'] = 'Already registered. Please login.';
			}
			return Response::json($response);
			die;
}

/***********************  verify phone number of a provider *******************************/

public function verifyProviderSms(Request $request){

		$phone=$request->input('phone');
		$code=$request->input('code');

		$user_arr = Provider::where('phone', '=', $phone)->where('sms_code', '=', $code)->first();
		if(!$user_arr) return Response::json(array("success"=>1,"msg"=>"Invalid Provider"));
		if($user_arr){

			$user_arr->sms_verified=1;
			$user_arr->device_id=$request->input('device_id');
			$user_arr->device_type=$request->input('device_type');
			$user_arr->save();
			$response['success'] = 0;
			$response['msg'] = 'Your registration is successfull, Please wait till your account is approved.';
			$response['data'] = array("id"=>$user_arr->id,'phone'=>$phone);
		}else{
			$response['success'] = 2;
			$response['msg'] = 'Invalid Code.';
			$response['data'] = array("id"=>0,'phone'=>$phone);

		}
		
		return Response::json($response);
		die;
}

/***********************  resend sms code for provider by sms to verify *******************************/


public function resendProviderOtp(Request $request){
	
		$phone=$request->input('phone');
		$x = 3; // Amount of digits
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$smscode = rand($min, $max);
		
		$provider = Provider::where('phone', '=', $phone)->first();
		if(!$provider) return Response::json(array("success"=>1,"msg"=>"Invalid Provider"));
		$provider->sms_code=$smscode;
		$provider->save();

		//this function is located at customer trait
		$res=$this->sendsms($phone,"Qber Code: ".$smscode,$provider->device_id);

		$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

		$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

		$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
		$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$provider->phone)->where('send_date','>=',$future_time)->count();

		$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
		$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$provider->phone)->where('send_date','>=',$future_time)->count();

		
		if($res){
		$response['success'] = 0;
		$response['msg'] = 'Code is sent to your device';
		$response['data'] =array("id"=>$provider->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);

		}else{

		$response['success'] = 0;
		$response['msg'] = 'Max sms send limit exceed.Please try after sometime.';
		$response['data'] =array("id"=>$provider->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);

		}
		

		return Response::json($response);
		die;
		
}



/********************************************************************** Customer section **********************************************************/



/***********************  signup for customer *******************************/


public function customersignup(Request $request){
	
	//$email = $request->input('email');
	$password = $request->input('password');
	$phone = $request->input('phone');
	$device_id=$request->input('device_id');

	if( $password=='' || $phone=='' || $device_id==''){
		$response['success'] = 1;
		$response['msg'] = 'Invalid Details.';
		return Response::json($response);
		die;
	}

	$count = Customer::where('phone', '=', $phone)->count();

	//recreate sms unverified customer
	if($count>0){
		$customer=Customer::where('phone', '=', $phone)->first();
		if($customer->sms_verified==0){
			$customer->delete();
			$count=0;
		}
	}
	

	if($count<=0){

					$x = 3; 
					$min = pow(10,$x);
					$max = (pow(10,$x+1)-1);
					$smscode = rand($min, $max);


					$customer=new Customer();
					//$customer->email=$email;
					$customer->password=Hash::make($password);
					$customer->phone=$phone;
					$customer->is_active=1;
					$customer->term_accepted=1;
					$customer->sms_verified=0;
					if($request->input('device_id'))
					$customer->device_id=$request->input('device_id');
					$customer->sms_code=$smscode;
					if($request->input('latitude'))
					$customer->current_latitude=$request->input('latitude');
					if($request->input('longitude'))
					$customer->current_longitude=$request->input('longitude');
					
					$customer->save();


					$profile=new Customerprofile();
					$profile->customer_id=$customer->id;
					$profile->name=$request->input('name');
					$profile->save();
					
					//this function is located at customer trait
					$res=$this->sendsms($phone,"Qber Code: ".$smscode,$customer->device_id);



					$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

					$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

					$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
					$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();

					$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
					$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();




					if($res){
					$response['success'] = 0;
					$response['msg'] = 'Registration Successful';
					$response['data'] =array("id"=>$customer->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);
					}else{

					$response['success'] = 0;
					$response['msg'] = 'Registration Successful. Unable to send SMS';
					$response['data'] =array("id"=>$customer->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);


					}


	
	
			}
			else
			{
				$response['success'] = 2;
				$response['msg'] = 'Already registered. Please login.';
			}
			return Response::json($response);
			die;



	
}

/***********************  validate account for customer by sms *******************************/


public function verifyCustomerSms(Request $request){

		$phone=$request->input('phone');
		$code=$request->input('code');
		$user_arr = Customer::with("Profile")->where('phone', '=', $phone)->where('sms_code', '=', $code)->first();
		
		if(!$user_arr) return Response::json(array("success"=>1,"msg"=>"Invalid Customer"));
		if($user_arr){

			$user_arr->sms_verified=1;
			if($request->input('device_id'))
			$user_arr->device_id=$request->input('device_id');
			$user_arr->is_online=1;
			$user_arr->device_type=$request->input('device_type');
			$user_arr->remember_token=Session::getId();
			$user_arr->save();
			$response['success'] = 0;
			$response['msg'] = 'Your registration is successfull, Please login with your details.';
			$response['data'] = array("id"=>$user_arr->id,'phone'=>$phone,'is_active'=>$user_arr->is_active,'token'=>$user_arr->remember_token,"name"=>$user_arr->profile->name);
		}else{
			
			$response['success'] = 2;
			$response['msg'] = 'Invalid Code.';
			$response['data'] = array("id"=>0,'phone'=>$phone);
		}
		return Response::json($response);
		die;
}

/***********************  resend sms code for customer by sms *******************************/


public function resendCustomerOtp(Request $request){

	$phone=$request->input('phone');
		$x = 3; // Amount of digits
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$smscode = rand($min, $max);
		
		$customer = Customer::where('phone', '=', $phone)->first();
		if(!$customer) return Response::json(array("success"=>1,"msg"=>"Invalid Customer"));
		$customer->sms_code=$smscode;
		$customer->save();

		//this function is located at customer trait
		$res=$this->sendsms($phone,"Qber Code: ".$smscode,$customer->device_id);

		$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

		$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

		$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
		$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();

		$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
		$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();

		
		if($res){
		$response['success'] = 0;
		$response['msg'] = 'Code is sent';
		$response['data'] =array("id"=>$customer->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);
		}else{
		$response['success'] = 0;
		$response['msg'] = 'Max sms send limit exceed. Please try after sometime.';
		$response['data'] =array("id"=>$customer->id,'phone'=>$phone,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log);
		}


		

		return Response::json($response);
		die;

}



/*

private function sendsms($phone,$msg,$device_id=''){

			$authheader=base64_encode('primarymobile:Pass9738');
			$res=array();

			$header[] = 'Content-type: application/json';
			$header[] = 'Authorization: Basic '.$authheader;
		
			$data = array('from'=>'Qber',"bulkId"=>"QBER-".date("Y-m-d"),'to'=>$phone,'text'=>$msg);
			$data_json = json_encode($data);
			$url='https://api.infobip.com/sms/1/text/single';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


			$cansend=$this->validateSMS($phone,$msg,$device_id);

			if($cansend)
			{
			$res  = curl_exec($ch);
			curl_close($ch);
			return $res;
			}
			else
			{
			curl_close($ch);
			return false;
			}
}


private function validateSMS($phone,$msg,$device_id){

			$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

			$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

			$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
			$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("device_id",$device_id)->where('send_date','>=',$future_time)->count();

			$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
			$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("device_id",$device_id)->where('send_date','>=',$future_time)->count();
		
			
			$send_sms=Servicesettings::select('code','value')->where('code','send_sms')->where('type_id','1')->first();

			
			
			if($send_sms->value=='1' && $one_min_log<$per_min_sms->value && $per_hour_log<$per_hour_sms->value)
			{
				$log=new Smslog;
				$log->device_id=$device_id;
				$log->sms_content=$msg;
				$log->send_date=date("Y-m-d H:i:s");
				$log->ip=$_SERVER['REMOTE_ADDR'];
				$log->save();
				return true;
			}
			else
			return false;

}
*/

}
?>
