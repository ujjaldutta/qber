<?php
/******************* Default controller which validate login for provider and customers **************/

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Customer;
use App\Models\Provider;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


/*************************************************** Customer Section *******************************************************************/

/**************************** Create customer token at time of login *******************************************/

	public function create_token_customer($request,$user_id) //create new token
		{
			$current_date_time = date("Y-m-d H:i:s");
			
			
			$user_token = Customer::where('id', '=', $user_id)->first();;
			
			$user_token->api_token = time();
			$user_token->is_online=1;
			if($request->input('device_id')){
			$user_token->device_id=$request->input('device_id');
			$user_token->device_type=$request->input('device_type');
			}
			$user_token->remember_token=Session::getId();
			if($request->input('latitude'))
			$user_token->current_latitude=$request->input('latitude');
			if($request->input('longitude'))
			$user_token->current_longitude=$request->input('longitude');
			
			$user_token->save();
			
			
			return $user_token->remember_token;
		}


/********************************************Destroy token at time of logout********************************************/

	public function destroy_token_customer($phone) //create new token
		{
			
			
			$user_token = Customer::where('phone', '=', $phone)->first();
			
			$user_token->api_token = '';
			$user_token->is_online=0;
			$user_token->remember_token='';
			$user_token->device_id='';
			$user_token->save();
			
			
			return true;
		}

/*********************************************verify and refresh token for each service call********************************************/
	
	public function check_token_customer($request) 
		{
			$phone=$request->input('phone');
			$user = Customer::where('phone', '=', $phone)->where("remember_token",$request->input('token'))->first();;
			
			if($user)
			{

				
				
				$current_date = date("Y-m-d H:i:s");
				$api_token_expiry_date = $this->get_token_expiry_date($user->api_token);
				if(strtotime($current_date)>strtotime($api_token_expiry_date))
				{
					
					//$new_token = $this->create_token($user['id']); 
					//return $new_token;

					$current_date_time = date("Y-m-d H:i:s");
					$user_token = Customer::where('id', '=', $user->id)->first();

					if($user_token->remember_token=='')
					return false;

					//generate new token
					$user_token->api_token = time();
					$user_token->is_online=1;
					
					if($request->input('device_id')){
					$user_token->device_id=$request->input('device_id');
					$user_token->device_type=$request->input('device_type');
					}
			
					$user_token->save();
					return $user_token->remember_token;
					
					
				}
				else
				{
					return $user->remember_token;
				}
			}
			else
			{
				return false;
			}
			die;
		}


/*********************************************get future time as token********************************************/
	
	public function get_token_expiry_date($date)
		{
			
			$futureDate = $date+(60*1000);
			$new_api_token_expiry_time = date("Y-m-d H:i:s", $futureDate);
			return $new_api_token_expiry_time;
		}




/*************************************************** Provider Section *******************************************************************/





	/**************************** Create provider token at time of login *******************************************/

	public function create_token_provider($request,$user_id) //create new token
		{
			$current_date_time = date("Y-m-d H:i:s");
			
			
			$user_token = Provider::where('id', '=', $user_id)->first();;
			//generate new token
			$user_token->api_token = time();
			
			if($request->input('device_id')){
			$user_token->device_id=$request->input('device_id');
			$user_token->device_type=$request->input('device_type');
			}

			

			
			$user_token->is_online=1;
			//$user_token->ready_to_serve=1;
			if($request->input('latitude'))
			$user_token->current_latitude=$request->input('latitude');
			if($request->input('longitude'))
			$user_token->current_longitude=$request->input('longitude');
			
			$user_token->remember_token=Session::getId();
			
			$user_token->save();
			
			
			return $user_token->remember_token;
		}

/********************************************Destroy token at time of logout********************************************/


	public function destroy_token_provider($phone) 
		{
			
			
			$user_token = Provider::where('phone', '=', $phone)->first();
			
			$user_token->api_token = '';
			$user_token->is_online=0;
			$user_token->ready_to_serve=0;
			$user_token->remember_token='';
			$user_token->provider_status='unavailable';
			$user_token->device_id='';
			$user_token->save();
			
			
			return true;
		}

	/*********************************************verify and refresh token for each service call of provider********************************************/	
	
	public function check_token_provider($request) 
	{
		$phone=$request->input('phone');
		
		$user = Provider::where('phone', '=', $phone)->where('approve_status', '=', 1)->where("remember_token",$request->input('token'))->first();;
		
		if($user)
		{
			
			 $current_date = date("Y-m-d H:i:s");
			$api_token_expiry_date = $this->get_token_expiry_date($user->api_token);
			if(strtotime($current_date)>strtotime($api_token_expiry_date))
			{

				$current_date_time = date("Y-m-d H:i:s");
				$user_token = Provider::where('id', '=', $user->id)->first();

				if($user_token->remember_token=='')
				return false;
		
				$user_token->api_token = time();
				
				if($request->input('device_id')){
				$user_token->device_id=$request->input('device_id');
				$user_token->device_type=$request->input('device_type');
				}
		
				$user_token->is_online=1;
				$user_token->save();
				return $user_token->remember_token;
				//$new_token = $this->create_token($user['id']); 
				//return $new_token;
				
				
			}
			else
			{
				return $user->remember_token;
			}
		}
		else
		{
			return false;
		}
		die;
	}

    
}
