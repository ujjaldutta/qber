<?php
/*************************************************** For Customer  App *******************************************************************/
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\Customerprofile;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Appointments;
use App\Models\CustomerLocationHistory;
use App\Models\Servicetype;
use App\Traits\CustomerServiceTrait;
use App\Events\LocationUpdated;
use App\Models\Servicesettings;
use App\Models\Smslog;
use App\Models\Serviceoptiondetails;
use App\Traits\ProviderServiceTrait;


class CustomerController extends Controller {

	use CustomerServiceTrait;
	use ProviderServiceTrait;
	public function __construct()
	{
		//$this->middleware('auth:api');
	}
	
	/***********************  get profile details of a customer *******************************/

	
	public function getProfile(Request $request){
		$check=$this->check_token_customer($request);

		if(!$check){
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}

		$phone = $request->input('phone');
		$customer=Customer::with('Profile')->selectRaw("email,phone,id")->where('phone',$phone)->first();

		
		return Response::json(array("success"=>0,"msg"=>"Profile details","customer"=>$customer));
	}



	/***********************  update profile details, new phone no  of a customer *******************************/
	public function updateProfile(Request $request)
	{
		
		$check=$this->check_token_customer($request);

		if(!$check){
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}

		$status=$this->updateCustomerProfile($request);
		
		if($status['success']=='0' && $status['phone']=='1'){
		//generate random number to validate phone via sms if customer change the phone
		$x = 3; 
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$smscode = rand($min, $max);

		
		$phone = $request->input('new_phone');
		$customer = Customer::where('phone', '=', $phone)->first();
		if(!$customer) return Response::json(array("success"=>1,"msg"=>"Invalid Customer"));
		$customer->sms_code=$smscode;
		$customer->save();
		$res=$this->sendsms($phone,"Qber Code: ".$smscode,$customer->device_id);

		$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

		$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

		$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
		$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();

		$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
		$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();





			if($res){
				return Response::json(array("success"=>0,"msg"=>"Success, Please verify phone no","phone"=>1,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log));
			}else{
				return Response::json(array("success"=>0,"msg"=>"Success, Unable to send SMS","phone"=>1,"per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log));
			}

		}elseif($status['success']=='0'){
		return Response::json(array("success"=>0,"msg"=>"Profile updated successfully","phone"=>0));
		}else{
		return Response::json(array("success"=>2,"msg"=>"Invalid Phone","phone"=>0));
		}

	}

	
	/***********************  save favourite location  of a customer *******************************/
	
	public function saveLocation(Request $request){

		$check=$this->check_token_customer($request);

		if(!$check)
		{
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}

		$phone = $request->input('phone');
		$customer=Customer::where('phone',$phone)->first();
		$locationcount=CustomerLocationHistory::where('customer_id',$customer->id)->count();

		//restrict to create max 3 locations
		if($locationcount<3)
		{
			$location=new CustomerLocationHistory();
			$location->customer_id=$customer->id;
			$location->latitude=$request->latitude;
			$location->longitude=$request->longitude;
			$location->save();
			return Response::json(array("success"=>0,"msg"=>"Location Created Successfully"));
		}else
		{
			return Response::json(array("success"=>2,"msg"=>"Max Number Of Locations Already Created"));
		}
	


	}

	/***********************  list favourite location  of a customer *******************************/
	
	public function listLocation(Request $request){

		$check=$this->check_token_customer($request);

		if(!$check)
		{
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}

		$phone = $request->input('phone');
		$customer=Customer::where('phone',$phone)->first();
		$locationcount=CustomerLocationHistory::where('customer_id',$customer->id)->count();
		
		if($locationcount>0)
		{
			$location=CustomerLocationHistory::where('customer_id',$customer->id)->get();
			return Response::json(array("success"=>0,"msg"=>"Location Listed Successfully","data"=>$location));
		}else
		{
			
			return Response::json(array("success"=>2,"msg"=>"No Location Exists"));
		}
	


	}

	/***********************  delete favourite location  of a customer by location_id *******************************/

	public function deleteLocation(Request $request){
		$check=$this->check_token_customer($request);

		if(!$check){
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}

		$phone = $request->input('phone');
		$customer=Customer::where('phone',$phone)->first();


		$affectedRows =CustomerLocationHistory::where('id',$request->input('location_id'))->delete();
		if($affectedRows){
		
		return Response::json(array("success"=>0,"msg"=>"Location Deleted Successfully"));
		}else{
		return Response::json(array("success"=>2,"msg"=>"No Location Exists."));
		}

		

	}
	

	/***********************  create appointment and send notification to provider *******************************/

	public function createService(Request $request)
	{
					
		$check=$this->check_token_customer($request);

		if(!$check){
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}
		if($request->input("type_id")=='' || $request->input("from_time")=='' || $request->input("to_time")=='' 
			|| $request->input("latitude")=='' || $request->input("longitude")=='' || $request->input("car_type")==''
			)
			return Response::json(array("success"=>2,"msg"=>"Invalid Service Details"));

			
		$status=$this->SaveServiceRequest($request);
		
		if($status){



		$provider=Provider::with('Profile')->where('id',$request->input('provider_id'))->first();
			
		return Response::json(array("success"=>0,"msg"=>"Your request has reached ".$provider->name.", please hold…"));
		}else{
			return Response::json(array("success"=>2,"msg"=>"Service Request Failed. Provider is not available!"));
		}
		
	}

	/***********************  repeat previous done  appointment and send notification to provider *******************************/

	public function repeatService(Request $request)
	{
		
		$check=$this->check_token_customer($request);

		if(!$check){
			return Response::json(array("success"=>1,"msg"=>"Login fails"));
		}
		
		$status=$this->RepeatServiceRequest($request);
		
		if($status){
		return Response::json(array("success"=>0,"msg"=>"Service repeated successfully"));
		}else{
			return Response::json(array("success"=>2,"msg"=>"Service Request Failed. Provider is not available!"));
		}
		
	}

	
	/***********************  when customer open first screen can see last done 10 jobs within 50 kim radius from his location *******************************/
	
	public function pastService(Request $request)
	{
			if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$phone = $request->input('phone');
			$customer=Customer::where('phone',$phone)->first();
			

			$sql="SELECT *, (3959 * acos(cos(radians('".$customer->current_latitude."')) * cos(radians(latitude)) * cos( radians(longitude) - radians('".$customer->current_longitude."')) + sin(radians('".$customer->current_latitude."')) * sin(radians(latitude)))) AS distance
			FROM service HAVING distance < 50  and service_status_id='6' ORDER BY distance asc,id desc LIMIT 0 , 10";

			$service=DB::select($sql);

			if(count($service)>0){
				$response['success'] = 0;
				$response['data'] = $service;
				$response['msg'] = 'success';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No service';
			}
			
			
			return Response::json($response);

			

		
	}


	/***********************  get past and current appointment list *******************************/
	public function listAppointment(Request $request){

		

		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

			
			$service=$this->currentBooking($request);

			if($service){
				$response['success'] = 0;
				$response['data'] = $service;
				$response['msg'] = 'Appointment List';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No Appointments';
			}

			
			return Response::json($response);

	}

	/***********************  just to avoid error if run url frm browser *******************************/
	public function show(Request $request){

	}

	/***********************  get confirmed,heading and working appointments *******************************/
	public function bookedAppointments(Request $request)
	{
		if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$appointments=$this->getbookedAppointments($request);
			

			if(count($appointments)>0){
				$response['success'] = 0;
				$response['data'] = $appointments;
				$response['msg'] = 'Appointment list';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No appointments';
			}
			
			
			return Response::json($response);

	}

	/***********************  view single appointment details *******************************/
	
	public function viewAppointment(Request $request)
	{
		if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$appointments= $this->getSingleAppointment($request);
			
			
			if(count($appointments)>0){
				$response['success'] = 0;
				$response['data'] = $appointments;
				$response['msg'] = 'Appointment Details';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No appointments';
			}

		return Response::json($response);
	}

	/***********************  update single appointment by customer *******************************/
	
	public function updateAppointment(Request $request)
	{
		if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$status=$this->updateBooking($request);

			if($status){
				$response['success'] = 0;
				$response['msg'] = 'Appointment updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Update failed';
			}

			
			return Response::json($response);
	}


	/***********************  Cancel appointment by customer *******************************/
	public function cancelAppointment(Request $request)
	{
		if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$status=$this->cancelBooking($request);

			if($status){
				$response['success'] = 0;
				$response['msg'] = 'Appointment is cancelled successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Appoinment cancel failed';
			}

			
			return Response::json($response);
	}



	
	/***********************  send rating to provider by customer of completed appointments*******************************/
	public function sendRating(Request $request){

			if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));

				$res=$this->createRatingtoProvider($request);


				if($res){
					$response['success'] = 0;
					$response['msg'] = 'You have successfully rated provider';
					$response['data']=$res;
				}else{
					$response['success'] = 2;
					$response['msg'] = 'Unable to rate provider';
				}

			
			return Response::json($response);

	}

	/***********************  get rating of completed appointments*******************************/

	public function getRating(Request $request){

			if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

				$res=$this->getRatingtoProvider($request);


				if($res){
					$response['success'] = 0;
					$response['data']=$res;
				}else{
					$response['success'] = 2;
					$response['msg'] = 'You have not rated yet';
				}

			
			return Response::json($response);

	}

	
	
	/***********************  crate dispute to provider for  appointments*******************************/
	
	public function openDispute(Request $request){
			if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));


				$status=$this->createDispute($request);


				if($status){
					$response['success'] = 0;
					$response['msg'] = 'Dispute is created successfully';
				}else{
					$response['success'] = 2;
					$response['msg'] = 'Dispute already created Or unable to create dispute';
				}


			 
			return Response::json($response);

	}

	/***********************  get notification details of different service status *******************************/
	

	public function getNotification(Request $request){
			if(!$this->isvalid($request))
				return Response::json(array("success"=>1,"msg"=>"Login fails"));


		$notification=$this->Notification($request);
		if($notification){
				$response['success'] = 0;
				$response['data'] = $notification;
				$response['msg'] = 'Notification details';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Details.';
			}

			
			return Response::json($response);

	}

	/***********************  update device id when phone location is changed *******************************/

	public function updateDevice(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->customerDeviceUpdate($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Device Updated';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Customer';
			}

			
			return Response::json($response);
	}
	
	/***********************  update car details modified by customer *******************************/
	
	public function updateCarServiceDetails(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->updateCustomerCarDetails($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Car details updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Unable to set details';
			}

			
			return Response::json($response);
	}

	/***********************  check login validity of a customer defined in app/Http/Controllers/Controller.php *******************************/
	
	private function isvalid($request)
	{
		//$phone = $request->input('phone');
		$check=$this->check_token_customer($request);

		if(!$check){
			return false;
		}
		return true;


	}


	/***********************  update customer latitude and logitude *******************************/
	
	public function updateCustomerLocation(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		
		$status=$this->CurrentLocationUpdate($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Location Updated';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Customer';
			}

			
			return Response::json($response);
	}

	/***********************  store message in customer_message table*******************************/
	
	public function sendMessage(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		
		$status=$this->sendCustomerMessage($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Message Sent';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Customer';
			}

			
			return Response::json($response);
	}






	/***********************  this is important function to search a provider and  getProviders() is in provider trait*******************************/

	
	 public function getMatchingProviders(Request $request){
			if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
			$phone=$request->input("phone");
			$customer=Customer::where('phone',$phone)->first();
			if(!$customer)
			return Response::json(array("success"=>1,"msg"=>"Invalid Customer"));
		 

			if($request->input("type_id")=='' || $request->input("from_time")=='' || $request->input("to_time")==''
			|| $request->input("latitude")=='' || $request->input("longitude")=='' || $request->input("phone")=='' 
			)
			return Response::json(array("success"=>2,"msg"=>"Invalid Service Details"));
			//$service=Service::where('id',$request->service_id)->first();
			$service['type_id']=$request->input("type_id");
			$service['from_time']=$request->input("from_time");
			$service['to_time']=$request->input("to_time");

			//save search details for a customer
			$profile=Customerprofile::where("customer_id",$customer->id)->first();
			$profile->total_search_submit=$profile->total_search_submit+1;
			$profile->last_search=date("Y-m-d H:i:s");
			$profile->save();


			
			//$address=$this->getaddress($request->input('latitude'),$request->input('longitude'));
			$countrycode='QA';
			/*if($address)
			{
			
				
				$countrycodearr=end($address[0]->address_components);
				$countrycode=$countrycodearr->short_name;
			}*/
		
			$service['nationality']=$countrycode;
			$service['latitude']=$request->input("latitude");
			$service['longitude']=$request->input("longitude");
			$min_duration=Servicesettings::select('code','value')->where('code','min_work_hour')->where('type_id',$request->input("type_id"))->first();


		$cartype=json_decode($request->input('car_type'),true);

		$jduration=0;
		if(isset($cartype) && count($cartype)>0){
		foreach($cartype as $details){
			
				
					for($i=0;$i<count($details);$i++){
						
						
						for($x=0;$x<count($details[$i]['option_details']);$x++){

							
							$options=Serviceoptiondetails::with('Serviceoption')->where('id',$details[$i]['option_details'][$x])->first()->toArray();
							
							$jduration=$jduration+$options['duration'];
							
						}

				
					}
			

			}
		}


			if($jduration < $min_duration->value)	
			$service['job_duration_set_by_provider']=$min_duration->value;
			else
			$service['job_duration_set_by_provider']=$jduration;

			
			

			$providers=$this->getProviders($service);
			

			if($providers['providers']->count()>0){
				$response['success'] = 0;
				
				$current_time=($request->input("current_time")!='')?$request->input("current_time"):date("Y-m-d H:i:s");
				$duration=(strtotime($service['from_time'])-strtotime($current_time))/60;

						//if provider is looking for more future time like tomorrow will show in listview in customer app
						if($duration>180)
						$response['viewmode'] = 'listview';
						else
						$response['viewmode'] = 'mapview';
				
						$response['providers'] = $providers['providers'];
						$response['invalidproviders']=$providers['invalidproviders'];
						$response['msg'] = 'Provider list';
			}else{
				$response['success'] = 2;
				$response['invalidproviders']=$providers['invalidproviders'];
				$response['msg'] = 'Provider Not Found';
			}
           
		
			return Response::json($response);
	 }

	 public function TrackLocationDetails(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

		
		$res=$this->getTrackLocation($request);
		if($res){
				$response['success'] = 0;
				$response['data'] = $res;
				$response['msg'] = 'Location Details';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid details';
			}

			
			return Response::json($response);
	}

	

}
