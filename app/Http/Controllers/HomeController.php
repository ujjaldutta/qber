<?php
/******************* Few common auth functions  for provider and customers **************/

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Provider;
use Illuminate\Http\Request;
use Response;
use Hash;
use App\Models\Servicesettings;
use Mail;
use Illuminate\Support\Facades\Auth;
use App\Traits\CustomerServiceTrait;
use App\Traits\ProviderServiceTrait;
use App\Console\Kernel;
use App\Models\Smslog;


class HomeController extends Controller{

	
use CustomerServiceTrait;
use ProviderServiceTrait;

public function __construct(){
		//$this->middleware('auth:api');
	}


public function index(){

	echo "nothing here";

}


/***********************  this function is not required *******************************/
 public function authenticate($phone,$password)
    {
        if (Auth::guard('api')->attempt(['phone' => $phone, 'password' => $password])) {
            // Authentication passed...
            return 1;
        }
    }

/***********************  validate customer login *******************************/

public function customerlogin(Request $request){

	//if (Auth::guard('api')->attempt(['phone' => $request->phone, 'password' => $request->password])) {
	//}
		$res= $this->CustomerTraitLogin($request);
		return Response::json($res);

		

}


/***********************  logout customer for customer *******************************/
public function customerlogout(Request $request){

	$phone = $request->input('phone');

	$this->destroy_token_customer($phone);
	
	//Auth::logout();

	return Response::json(array("success"=>0));
}




/***********************  send forgot password link to  customer  *******************************/

public function customerForgotPassword(Request $request){

	$phone = $request->input('phone');
	$x = 3; // Amount of digits
	$min = pow(10,$x);
	$max = (pow(10,$x+1)-1);
	$smscode = rand($min, $max);
	
	$customer=Customer::where("phone",$phone)->where("sms_verified","1")->first();
	if(!$customer) return Response::json(array("success"=>1,"msg"=>"Account is invalid or does not exists"));
	
	$customer->sms_code=$smscode;
	$customer->save();
	//$url=link_to_action('HomeController@customerResetPassword', 'Click Here', [ "phone"=>base64_encode($phone),"code"=>$customer->sms_code ]);
	$url=url('customer/reset-password'). '?' . http_build_query([ "phone"=>base64_encode($phone),"code"=>$customer->sms_code]);
	$link='Forgot password link for '.$phone. ' : '.$url;
	
	$res=$this->sendsms($phone,$link,$customer->device_id);


	$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

	$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

	$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
	$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();

	$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
	$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$customer->phone)->where('send_date','>=',$future_time)->count();

	
	if($res){
			return Response::json(array("success"=>0,"link"=>$link,"msg"=>"Reset link sent to your mobile.","per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log));
		}else{
			return Response::json(array("success"=>0,"link"=>$link,"msg"=>"Unable to send SMS","per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log));
		}
}


/***********************  open reset screen to  customer to change password  *******************************/

public function customerResetPassword(Request $request){

	$phone=base64_decode($request->input('phone'));
	$code=$request->input('code');
	$title="Reset Password";
	
	$customer=Customer::where("phone",$phone)->where("sms_code",$code)->first();
	
	
	return view('customer-reset-password',  compact('title','customer','code','phone'));

}

/***********************  update password from reset screen url for customer  *******************************/

public function customerSavePassword(Request $request){

	$phone=($request->input('phone'));
	$code=$request->input('code');
	$password=$request->input('password');
	$confirm_password=$request->input('confirm_password');
	$title="Reset Password";
	$customer=Customer::where("phone",$phone)->where("sms_code",$code)->first();
	if(!$customer){
	$request->session()->flash('message', 'Invalid Customer or Password Expired!');
	return redirect()->back();
	}

	if($password=='' || strlen($password)<8){
	$request->session()->flash('message', 'Invalid Password length!');
	return redirect()->back();
	}
	if($password==$confirm_password){
			$customer->password=Hash::make($password);
			$customer->sms_code='';
			$customer->save();
			$request->session()->flash('message', 'Password Changed Successfully!');
			return redirect()->back();
	}

	

	$request->session()->flash('message', 'Password not matching!');
	$request->session()->flash('alert-class', 'alert-class');
	
			return redirect()->back();
	

}



/**************************************************************** Provider Section*************************************************************************/

/***********************  validate login for provider   *******************************/

public function providerlogin(Request $request){

		$res=$this->ProviderTraitLogin($request);
		return Response::json($res);

}



/***********************  do logout and set provider offline   *******************************/

public function providerlogout(Request $request){

	$phone = $request->input('phone');

	$this->destroy_token_provider($phone);
	
	Auth::logout();

	return Response::json(array("success"=>0));
}


/***********************  do logout and set provider offline   *******************************/

public function providerForgotPassword(Request $request){

	$phone = $request->input('phone');
	$x = 3; // Amount of digits
	$min = pow(10,$x);
	$max = (pow(10,$x+1)-1);
	$smscode = rand($min, $max);
		
	$provider=Provider::where("phone",$phone)->where("sms_verified","1")->first();
	if(!$provider) return Response::json(array("success"=>1,"msg"=>"Account is invalid or does not exists"));
	
	$provider->sms_code=$smscode;
	$provider->save();
	//$url=link_to_action('HomeController@providerResetPassword', 'Click Here', [ "phone"=>base64_encode($phone),"code"=>$provider->sms_code ]);
	$url=url('provider/reset-password'). '?' . http_build_query([ "phone"=>base64_encode($phone),"code"=>$provider->sms_code]);;
	$link='Forgot password link for '.$phone. ' : '.$url;
	
	$res=$this->sendsms($phone,$link,$provider->device_id);

	$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

	$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

	$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
	$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$provider->phone)->where('send_date','>=',$future_time)->count();

	$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
	$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("phone",$provider->phone)->where('send_date','>=',$future_time)->count();

	
	if($res){
			return Response::json(array("success"=>0,"link"=>$url,"msg"=>"Reset link sent to your mobile.","per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log));
		}else{
			return Response::json(array("success"=>0,"link"=>$url,"msg"=>"Unable to send sms.","per_min_sms"=>$per_min_sms,"per_min_sms_count"=>$one_min_log,
		"per_hour_sms"=>$per_hour_sms,"per_hour_sms_count"=>$per_hour_log));
		}

	}

/***********************  display reset password screen to provider   *******************************/

public function providerResetPassword(Request $request){

	$phone=base64_decode($request->input('phone'));
	$code=$request->input('code');
	$provider=Provider::where("phone",$phone)->where("sms_code",$code)->first();
	$title="Reset Password";

	
	
	
	return view('provider-reset-password',  compact('title','provider','code','phone'));

}

/***********************  update password to database when reset submit   *******************************/

public function providerSavePassword(Request $request){

	$phone=($request->input('phone'));
	$code=$request->input('code');
	$password=$request->input('password');
	$confirm_password=$request->input('confirm_password');
	$title="Reset Password";
	$provider=Provider::where("phone",$phone)->where("sms_code",$code)->first();
	if(!$provider){
	$request->session()->flash('message', 'Invalid Provider or Password Expired!');
	return redirect()->back();
	}
	if($password=='' || strlen($password)<8){
	$request->session()->flash('message', 'Invalid Password length!');
	return redirect()->back();
	}
	
	if($password==$confirm_password){
			$provider->password=Hash::make($password);
			$provider->sms_code='';
			$provider->save();
			$request->session()->flash('message', 'Password Changed Successfully!');
			return redirect()->back();
			
	}

	$request->session()->flash('message', 'Password not matching!');
	$request->session()->flash('alert-class', 'alert-class');
	
			return redirect()->back();
	

}

/***********************  update password to database when reset submit  from mobile  *******************************/

public function providerAPISavePassword(Request $request){

	$phone=($request->input('phone'));
	
	$password=$request->input('password');
	$current_password=$request->input('current_password');
	
	$provider=Provider::where("phone",$phone)->first();

	
	if(!$provider){
		$response['status'] = 2;
		$response['msg'] = 'Invalid Provider or Password !';
		return Response::json($response);
	}
	if($password=='' || strlen($password)<8){
		$response['status'] = 2;
		$response['msg'] = 'Invalid Password length!';
		return Response::json($response);
	
	}
	
	if($password!='' && Hash::check($current_password, $provider->password)){
			$provider->password=Hash::make($password);
			$provider->sms_code='';
			$provider->save();
			
			$response['success'] = 0;
			$response['msg'] = 'Password Changed Successfully!';
			
	}else{
		$response['success'] = 2;
		$response['msg'] = 'Current Password not matching!';
	
		}
	
	return Response::json($response);
}

/***********************  send email verify link when register or change email for provider in profile *******************************/


public function SendMailVerifiy(Request $request){
		$phone=($request->input('phone'));
		$provider=Provider::where("phone",$phone)->first();

		$x = 3; // Amount of digits
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$smscode = rand($min, $max);
		$provider->sms_code=$smscode;
		$provider->save();
		$email=$provider->email;
		if($email!=''){
		$data=array("title"=>"Verify Email","email"=>$email,"link"=>url('verify-email/'). '?' . http_build_query(['key' => $smscode]));
		
		Mail::send('emails.emailvarify', $data, function ($message) use($email) {
			$message->from('admin@qber.com', 'Qber');
			$message->subject('Verify Account');
			$message->to($email);
		});

		$response['success'] = 0;
		$response['msg'] = 'Email Sent Successfully!';

		}else{

		$response['success'] = 1;
		$response['msg'] = 'Email Not Valid!';
		}

		
		return Response::json($response);

}

/***********************  set provider email verified *******************************/

public function EmailVerified(Request $request){

	$key=($request->input('key'));
	$provider=Provider::where("sms_code",$key)->first();

	if($provider){
		$provider->sms_code='';
		$provider->email_verified=1;
		$provider->save();
		$request->session()->flash('message', 'Email Verified Successfully!');
		}else{

		$request->session()->flash('message', 'Invalid email or email already verified !');
		}
	$title="Verify Email";
	return view('emailverified',  compact('title'));
}


/***********************  fetch introduction text of app *******************************/


public function getSplashText(Request $request){
	
	$type=$request->input('type');
	if($type=='') return Response::json(array("success"=>1,"msg"=>"Please set service type"));
	$splashtext=Servicesettings::select('code','value')->where('code','splash_text')->where('type_id',$type)->first();
	if($splashtext){
		$response['success'] = 0;
		$response['data'] =$splashtext->value;
		$response['msg'] = 'success';
	}else{
		$response['success'] = 2;
		
		$response['msg'] = 'Unable to get spalash text';
	}

	return Response::json($response);

}


/*
private function sendsms($phone,$msg,$device_id=''){

			$authheader=base64_encode('primarymobile:Pass9738');
			$res=array();
			$header[] = 'Content-type: application/json';
			$header[] = 'Authorization: Basic '.$authheader;
		
			$data = array('from'=>'Qber',"bulkId"=>"QBER-".date("Y-m-d"),'to'=>$phone,'text'=>$msg);
			$data_json = json_encode($data);
			$url='https://api.infobip.com/sms/1/text/single';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$send_sms=Servicesettings::select('code','value')->where('code','send_sms')->where('type_id','1')->first();

			$cansend=$this->validateSMS($phone,$msg,$device_id);

			if($cansend)
			{
			$res  = curl_exec($ch);
			curl_close($ch);
			return $res;
			}
			else
			{
			curl_close($ch);
			return false;
			}

			
			if($send_sms->value=='1')
			$res  = curl_exec($ch);
			curl_close($ch);

			return $res;
}


private function validateSMS($phone,$msg,$device_id){

			$per_min_sms=Servicesettings::select('code','value')->where('code','per_minute_sms')->where('type_id','1')->first();

			$per_hour_sms=Servicesettings::select('code','value')->where('code','per_hour_sms')->where('type_id','1')->first();

			$future_time=date("Y-m-d  H:i:s",strtotime("-1 minute",time()));
			$one_min_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("device_id",$device_id)->where('send_date','>=',$future_time)->count();

			$future_time=date("Y-m-d  H:i:s",strtotime("-60 minutes",time()));
			$per_hour_log=Smslog::where("ip",$_SERVER['REMOTE_ADDR'])->where("device_id",$device_id)->where('send_date','>=',$future_time)->count();
		
			
			$send_sms=Servicesettings::select('code','value')->where('code','send_sms')->where('type_id','1')->first();

			
			
			if($send_sms->value=='1' && $one_min_log<$per_min_sms->value && $per_hour_log<$per_hour_sms->value)
			{
				$log=new Smslog;
				$log->device_id=$device_id;
				$log->sms_content=$msg;
				$log->send_date=date("Y-m-d H:i:s");
				$log->ip=$_SERVER['REMOTE_ADDR'];
				$log->save();
				return true;
			}
			else
			return false;

}
*/

}
?>
