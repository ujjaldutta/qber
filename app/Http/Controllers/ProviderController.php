<?php
/*************************************************** For Provider  App *******************************************************************/
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Provider;
use App\Models\Locationpreferencepolygon;
use App\Models\Locationpreferencehead;
use App\Models\Providerpreferencelocation;
use App\Models\Providerprofile;
use App\Models\ProviderMessage;
use App\Models\ProviderRatings;

use DB;

use App\Traits\ProviderServiceTrait;

class ProviderController extends Controller {

	use ProviderServiceTrait;
	public function __construct()
	{
		//$this->middleware('auth:api');
	}

	
	/***********************  set profile details of a provider *******************************/
	
	public function setProviderProfile(Request $request){
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

		$status=$this->setProviderProfileTrait($request);


        if($status){
			$response['success'] = 0;
			$response['msg'] = 'Profile Updated Successfully';
		}else{
			$response['success'] = 2;
			$response['msg'] = 'Unable to update profile';
		}
		
		return Response::json($response);
		die;
	}

	/***********************  delete any type of  profile image of a provider *******************************/
	
	public function deleteProfileImage(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		$imagetype=$request->input('image');
		//image,banner_img1,banner_img2,banner_img3,admin_profile_image,tools_use_image,shop_image,cv_file,car_registration_image,driving_license_image,identity_image
		
		$profile=Providerprofile::where("provider_id",$provider->id)->first();
		$oldimage=$profile->$imagetype;
		@unlink($oldimage);
		$profile->$imagetype='';
		$profile->save();
		$response['success'] = 0;
		
		$response['msg'] = 'Profile Updated Successfully';
		
		return Response::json($response);
		die;

	}


	/***********************  get Profile details of a provider with rating and preferred locations *******************************/

	public function getProviderProfile(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		$phone = $request->input('phone');
		$page = $request->input('page');
		$provider=Provider::with(['Profile','Ratings'=>function($query) use ($page){
			$query->selectRaw('provider_rating.id,rate_value,comment, professional,friendly,communication,date,provider_id,customer_profile.name,customer_profile.image')
			->leftJoin('customer_profile','customer_profile.customer_id','=','provider_rating.customer_id')
			->orderby('id','desc')->paginate(5);


			}])->where('phone',$phone)->first();
	
		$locations=Providerpreferencelocation::with("Preferencehead")->where('provider_id',$provider->id)->get();
		
		$response['success'] = 0;
		$response['provider'] =$provider;
		$response['work_preference'] =$locations;
		$response['profilerating']=ProviderRatings::selectRaw('AVG(rate_value) as rating, count(*) as ratecount')->where('provider_id',$provider->id)->get();
		$response['msg'] = 'Profile Details';
		
		return Response::json($response);
		die;
	}

	
	/***********************  called when provider ready and starts accepting jobs *******************************/
	
	public function ReadytoServe(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		

		$this->ReadyforJob($request);
		

		$response['success'] = 0;
		$response['msg'] = 'Status Updated Successfully';
		
		return Response::json($response);
		die;
	}

	/***********************  set preference location of a provider  *******************************/
	
	public function savePreference(Request $request){
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

		$phone = $request->input('phone');
		$preferid = $request->input('preference_id');

		$provider=Provider::where('phone',$phone)->first();
		$location = new Providerpreferencelocation();
		$location->preference_id=$preferid;
		$location->provider_id=$provider->id;
		$location->save();

		$response['success'] = 0;
		$response['msg'] = 'Location Saved Successfully';
		
		return Response::json($response);
		die;

	}

	/***********************  get preference list  *******************************/

	public function getPreference(Request $request){
		//if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		
		$locations=Locationpreferencehead::get();
		
		if($locations->count()>0){
			$response['success'] = 0;
			$response['locations'] = $locations;
			$response['msg'] = 'success';
		}else{
			$response['success'] = 2;
			$response['locations'] = $locations;
			$response['msg'] = 'No location found';
		}
		
		return Response::json($response);
	}

	/***********************  get preference list which is already set by provider *******************************/

	public function getProviderPreference(Request $request)
	{
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		
		$provider=Provider::where('phone',$phone)->first();
		//$locations=Providerpreferencelocation::with("Polygon","Preferencehead")->where('provider_id',$provider->id)->get();
		$locations=Providerpreferencelocation::with("Preferencehead")->where('provider_id',$provider->id)->get();
		
		if($locations->count()>0){
			$response['success'] = 0;
			$response['locations'] = $locations;
			$response['msg'] = 'success';
		}else{
			$response['success'] = 2;
			$response['locations'] = $locations;
			$response['msg'] = 'No location found';
		}
		
		return Response::json($response);
	}

	/***********************  get all message sent to provider by admin *******************************/

	public function getMessage(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		$messages=ProviderMessage::select('id','subject','message','created_at')->where('provider_id',$provider->id)->get();


		if($messages->count()>0){
			$response['success'] = 0;
			$response['data']=$messages;
			$response['msg'] = 'Message listed.';
		}else{
			$response['success'] = 2;
			$response['msg'] = 'No message found';
		}
		
		return Response::json($response);

	}

	/***********************  delete all message sent to provider by admin *******************************/

	public function DeleteMessage(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$id= $request->input('id');
		$affRows=ProviderMessage::where('id',$id)->delete();


		if($affRows){
			$response['success'] = 0;
			$response['msg'] = 'Message deleted successfully.';
		}else{
			$response['success'] = 2;
			$response['msg'] = 'No message found';
		}
		
		return Response::json($response);

	}

	/***********************  send rating to customer on completed jobs to customer *******************************/

	public function sendRating(Request $request){

			if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

				$res=$this->createRatingtoCustomer($request);


				if($res){
					$response['success'] = 0;
					$response['msg'] = 'You have successfully rated customer';
					$response['data']=$res;
				}else{
					$response['success'] = 2;
					$response['msg'] = 'You have already rated';
				}

			
			return Response::json($response);

	}

	/***********************  get all rating sent from customer *******************************/

	public function getRating(Request $request){

			if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

				$res=$this->getRatingtoCustomer($request);


				if($res){
					$response['success'] = 0;
					$response['data']=$res;
				}else{
					$response['success'] = 2;
					$response['msg'] = 'You have not rated yet';
				}

			
			return Response::json($response);

	}

	/***********************  list all past and current appointments of provider *******************************/

	public function listAppointment(Request $request){

		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));

			$service=$this->listBooking($request);

			if($service){
				$response['success'] = 0;
				$response['data'] = $service;
				$response['msg'] = 'success';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No Appointments';
			}

			
			return Response::json($response);

	}

	/***********************  get appointment detail by id *******************************/
	
	public function getAppointment(Request $request){



		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

			 
			$service=$this->getBooking($request);
           if($service){
				$response['success'] = 0;
				$response['data'] = $service;
				$response['msg'] = 'Appointment details';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Appointment';
			}

			
			return Response::json($response);

	}

	/***********************  accept appointment by provider sent through notification *******************************/
	
	public function acceptAppointment(Request $request){

		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$status=$this->acceptBooking($request);

			if($status){
				$response['success'] = 0;
				$response['msg'] = 'Appointment is accepted!';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Appointment booking failed';
			}

			
			return Response::json($response);

	}

	/***********************  cancel and reschedule appointment with penality charge *******************************/

	public function cancelAppointment(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$status=$this->rejectBooking($request);

			if($status){
				$response['success'] = 0;
				$response['msg'] = 'Appointment is cancelled successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Cancellation failed';
			}

			
			return Response::json($response);
	}


	/***********************  Start heading for appointment *******************************/


	public function startHeadingAppointment(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

				
			
			$res=$this->startHeading($request);

			if($res['status']){
				$response['success'] = 0;
				$response['msg'] = 'Heading Started';
			}else{
				$response['success'] = 2;
				$response['reason']=$res['reason'];
				$response['msg'] = 'Request failed';
			}

			
			return Response::json($response);
	}

	/***********************  Start working for appointment *******************************/

	public function startAppointment(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

			$res=$this->startWorking($request);

			if($res['status']){
				$response['success'] = 0;
				$response['msg'] = 'Appointment Started';
			}else{
				$response['success'] = 2;
				$response['reason']=$res['reason'];
				$response['msg'] = 'Request failed';
			}

			
			return Response::json($response);
	}

	/***********************  Completed working for appointment *******************************/
		
	public function completedAppointment(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));

			$res=$this->completedWorking($request);

			if($res['status']){
				$response['success'] = 0;
				$response['msg'] = 'Appointment completed';
			}else{
				$response['success'] = 2;
				$response['reason']=$res['reason'];
				$response['msg'] = 'Request failed';
			}

			
			return Response::json($response);
	}

	



	/***********************  get Provider preference locations,priority,transport and location *******************************/
	
	public function getSettings(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		
		$locations=Providerpreferencelocation::with('Preferencehead')->where('provider_id',$provider->id)->get();
		$preference=Locationpreferencehead::with('Polygon')->get()->toArray();
		for($i=0;$i<count($preference);$i++){
			$plocation=Providerpreferencelocation::where('preference_id',$preference[$i]['id'])->where('provider_id',$provider->id)->first();
			if($plocation){
				$preference[$i]['selected']=true;
				$preference[$i]['selected_location_id']=$plocation->id;
			}else{
				$preference[$i]['selected']=false;
			}


		}


		
		$transport=Providerprofile::select('transport')->where('provider_id',$provider->id)->first();
		$priority=$provider->location_preference;
		if($preference){
				$response['success'] = 0;
				$response['existingarea']=$locations;
				$response['base_latitude']=$provider->base_latitude;
				$response['base_longitude']=$provider->base_longitude;
				$response['base_address']=$provider->base_address;
				$response['preference']=$preference;
				$response['provider']=$transport;
				$response['priority']=$priority;
				$response['msg'] = 'Settings found';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No settings available';
			}

			
			return Response::json($response);

	}

	/***********************  set Provider preference locations,priority,transport and location *******************************/

	public function updateSettings(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();

		
		$locations=json_decode($request->input('joblocations'));
		$transport=$request->input('transport');
		$priority=$request->input('priority');
		
		$profile=Providerprofile::where('provider_id',$provider->id)->first();
		$profile->transport=$transport;
		$profile->update_settings=1;
		$profile->save();

		
		if($request->input('priority'))
		$provider->location_preference=$priority;
	

		if($request->input('base_latitude') && $request->input('base_longitude'))
		{
			$provider->base_latitude=$request->input('base_latitude');
			$provider->base_longitude=$request->input('base_longitude');
			//$address=$this->getLatlngaddress($request->input('base_latitude'),$request->input('base_longitude'));

			$provider->base_address=$request->input('base_address');
		}
		$provider->save();

		$preference=Providerpreferencelocation::where('provider_id',$provider->id)->delete();
		
		foreach($locations as $location){
			$preference=new Providerpreferencelocation;
			$preference->provider_id=$provider->id;
			$preference->preference_id=$location->id;
			$preference->save();
		}
		
		//$locations=Providerpreferencelocation::with('Polygon','Preferencehead')->where('provider_id',$provider->id)->get();
		//$preference=Locationpreferencehead::get();
		//$transport=Providerprofile::select('transport')->where('provider_id',$provider->id)->first();
		//$priority=$provider->location_preference;
		
		if($preference){
				$response['success'] = 0;
		
				$response['msg'] = 'Details updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Update failed';
			}

			
			return Response::json($response);

	}

	/***********************  get balance to pay for provider for sattlement*******************************/

	public function getBalance(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));


		$balance=$this->getProviderBalance($request);


		if($balance){
				$response['success'] = 0;
				$response['data']=$balance;
				$response['msg'] = 'Balance details';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'No balance details';
			}

			
			return Response::json($response);
		

	}

	/*********************** request for sattlement to qber*******************************/
	
	public function requestBalance(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));


		$balance=$this->requestBalanceSattle($request);


		if(isset($balance['sattle']) && isset($balance['data'])){
				$response['success'] = 0;
				$response['data'] = $balance['data'];
				$response['msg'] = 'Your request successfully placed';
			}
			elseif($balance=='nocredit'){
				$response['success'] = 2;
				$response['msg'] = 'Not enought credit balance to sattle';
			}
			else{

				$phone = $request->input('phone');
				$provider=Provider::where('phone',$phone)->first();

				$sattled=\App\Models\ProviderCompanyAccount::where('provider_id',$provider->id)
				->whereDate('arrival_time','>=',date("Y-m-d",time()))
				->where('sattled','no')->orderby('id','desc')->first();
				$response['cancel'] = false;
				if($sattled){
					$response['cancel'] = true;
				}
				$response['success'] = 2;
				$response['msg'] = 'Not allowed or already requested';
			}

			
			return Response::json($response);
		

	}


	/*********************** cancel already requested sattlement if want to pay later*******************************/

	public function cancelBalanceSattle(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));


		$balance=$this->cancelSattle($request);


		if($balance){
				$response['success'] = 0;
				
				$response['msg'] = 'Your request is cancelled';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Something is wrong !! You are not allowed to cancel';
			}

			
			return Response::json($response);
		

	}

	/*********************** provider can set wait to accept request sent by customer*******************************/

	public function waitAppointment(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));

		$wait=$this->setWaitDuration($request);
		
		
		if($wait){
				$response['success'] = 0;
				
				$response['msg'] = 'Your Wait appointment request is placed successfully!';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'You are not allowed to wait for this appointment.';
			}

			
			return Response::json($response);
	}

	/*********************** this service is called after provider dont click wait and duration 15 second passed *******************************/

	public function autocancelAppointment(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
	
		$cancel=$this->autoCancel($request);
		
		
		if($cancel){
				$response['success'] = 0;
				
				$response['msg'] = 'Your appointment request is cancelled.';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'You are not allowed to cancel.';
			}

			
			return Response::json($response);
	}

	/*********************** provider update price of a service expected_cost *******************************/

	public function updatePrice(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->updateProviderPrice($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Price is updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'You are not allowed to update';
			}

			
			return Response::json($response);
	}

	/*********************** provider update duration of a service  *******************************/
	
	public function updateJobduration(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->updateDuration($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Job duration updated';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Its less than default duration Or duration not allowed';
			}

			
			return Response::json($response);
	}

	/*********************** provider update end time of service  *******************************/

	
	public function updateBookingend(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->updateEndtime($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Booking end time updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Time';
			}

			
			return Response::json($response);
	}

	/*********************** update suv sedan details by the provider  *******************************/

	public function updateCarServiceDetails(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->updateCarDetails($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Car details updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Unable to update details';
			}

			
			return Response::json($response);
	}


	/*********************** Create dispute to customer for a service  *******************************/

	public function openDispute(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$status=$this->DisputeCustomer($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Dispute Created Successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Dispute already created Or unable to create dispute';
			}

			
			return Response::json($response);
	}

	/*********************** Update current location cotinuously for live track  *******************************/

	public function UpdateCurrentLocation(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$res=$this->updateLocation($request);
		if($res){
				$response['success'] = 0;
				$response['data'] = $res;
				$response['msg'] = 'Location updated successfully';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid details';
			}

			
			return Response::json($response);
	}

	

	/*********************** get all notfication sent to provider   *******************************/

	public function getNotification(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$notification=$this->providerNotification($request);
		if($notification){
				$response['success'] = 0;
				$response['data'] = $notification;
				$response['msg'] = 'Notification details';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid details';
			}

			
			return Response::json($response);
	}

	/*********************** When company send notification is accepted by provider   *******************************/


	public function AcceptCompanyInvite(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		
		$status=$this->providerCompanyAccept($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'You are tied with company now';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid details';
			}

			
			return Response::json($response);
	}

	/*********************** update device id  *******************************/

	public function updateDevice(Request $request){
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		
		$status=$this->providerDeviceUpdate($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Device Updated';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Provider';
			}

			
			return Response::json($response);
	}

	/*********************** send message from provider app  *******************************/

	
	public function sendMessage(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));
		
		$status=$this->sendProviderMessage($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Message Sent';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Provider';
			}

			
			return Response::json($response);
	}

	/*********************** update base latitude longitude of provider  *******************************/

	public function updateBaseLocation(Request $request)
	{
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"login fails"));

		$status=$this->providerBaselocationUpdate($request);
		if($status){
				$response['success'] = 0;
				
				$response['msg'] = 'Location Updated';
			}else{
				$response['success'] = 2;
				$response['msg'] = 'Invalid Details';
			}

			
			return Response::json($response);

	}

	
	
	private function isvalid($request){
		//$phone = $request->input('phone');
		$check=$this->check_token_provider($request);

		if(!$check){
			return false;
		}
		return true;


	}

	public function show(Request $request){

	}
	

}
