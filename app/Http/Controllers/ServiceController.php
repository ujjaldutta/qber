<?php
/******************* Few common service functions, search providers for customers **************/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Models\Servicetype;
use App\Models\Providerworktime;
use App\Models\Provider;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Customerprofile;
use App\Traits\CustomerServiceTrait;
use App\Traits\ProviderServiceTrait;
use App\Models\Servicesettings;
use App\Models\Serviceoptiondetails;


class ServiceController extends Controller
{

	use CustomerServiceTrait;
	use ProviderServiceTrait;

	/***********************  get service type of whole app *******************************/
	
	public function	servicetype (Request $request){
			//if(!$this->isvalid($request))return Response::json(array("success"=>false));
			$type=Servicetype::with(['Options','Servicesetting' => function ($query){
			$query->where('code','fixed_cost');
			$query->orWhere('code','min_cost');
			$query->orWhere('code','allowed_start_time');
			$query->orWhere('code','allowed_end_time');
			$query->orWhere('code','arrival_duration');
		}])->where('is_active','1');

		if($request->type_id){
		$type=$type->where('id',$request->type_id);
		}
		$type=$type->get()->toArray();
		
		if(count($type)<=0)
		return Response::json(array("success"=>0,"data"=>array(),"msg"=>"No service available."));
		
		$i=0;
		foreach($type as $servicetype){
			
			$setting=array();
			foreach($servicetype['servicesetting'] as $settings){
				

					$setting[$settings['code']]=$settings['value'];
			
			}
			

			$type[$i]['servicesetting']=(object)$setting;
			$i++;
		}



		
		
		if(count($type)>0){
			
		
			return Response::json(array("success"=>0,"data"=>$type,"msg"=>"All service with options."));
		}
		else{

			return Response::json(array("success"=>1,"data"=>array(),"msg"=>"No service available"));

		}

	}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


	/***********************  get notice text from setting table *******************************/
		
    public function notice(Request $request){
		
		$id=$request->input('id');

		if($id=='')return Response::json(array("success"=>2,"msg"=>"Invalid Service Type"));
		
		$type=Servicetype::with(['Servicesetting' => function ($query){
			$query->where('code','notice_text');
			$query->orWhere('code','display_notice');
		}])->where('is_active','1')->where('id',$id)->first();

		if($type)
		$type=$type->toArray();
		else
		return Response::json(array("success"=>2,"msg"=>"Invalid Service Type"));
		
		$notice=array("notice"=>$type['servicesetting'][0]['value'],"status"=>$type['servicesetting'][1]['value'],"success"=>0,"msg"=>"Success");
			return Response::json($notice);
			die;

	}

	

	/***********************  common function to get provider details *******************************/

	public function getProviderProfile(Request $request){
		$phone=$request->input("phone");
		$provider=Provider::with('Profile')->with(['Ratings' => function ($query){
			$query->leftJoin('customer_profile','provider_rating.customer_id','=','customer_profile.customer_id');
			$query->select(array('provider_rating.*','customer_profile.name'));
			
		}])->where("phone",$phone)->first();

		if($provider){
			$response['success'] = 0;
			$response['provider'] = $provider;
			$response['msg'] = 'Profile profile';
		}else{
			$response['success'] = 1;
			$response['msg'] = 'Provider Not Found';

		}

		return Response::json($response);

	 }

	 
	/***********************  display country dropdown in app *******************************/
	
	public function getCountry(Request $request){

		$country=Country::get();
		
		if($country){
			$response['success'] = 0;
			$response['provider'] = $country;
			$response['msg'] = 'Country list';
		}else{
			$response['success'] = 1;
			$response['msg'] = 'Country Not Found';

		}
		return Response::json($response);
	}

	/***********************  get detail of service for customer and provider *******************************/
	
	public function getServiceDetails(Request $request){
		
		//common function found in customer service trait
		$service_details=$this->ServiceDetails($request);
		
		
		if($service_details){
			$response['success'] = 0;
			$response['service'] = $service_details;
			$response['msg'] = 'Service Details';
		}else{
			$response['success'] = 1;
			$response['msg'] = 'Service Not Found';

		}
		return Response::json($response);
	}

	/***********************  generate cancel reason dropdown in app *******************************/
	
	public function getCancelReason(Request $request){

		$reason=$this->CancelReason();
		
		
		if($reason){
			$response['success'] = 0;
			$response['service'] = $reason;
			$response['msg'] = 'Cancel Reason List';
		}else{
			$response['success'] = 1;
			$response['msg'] = 'Reason Not Found';

		}
		return Response::json($response);
		
	}
	

	/***********************  common function to get street address by lat lng *******************************/

	 private function getaddress($lat,$lng)
	  {
		  $status='';
		 $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		 $json = @file_get_contents($url);
		 $data=json_decode($json);
		if($data)
		 $status = $data->status;
		 if($status=="OK")
		 {
			 //['results']['0']['address_components']['5']['long_name'];
		   //return $data->results[0]->formatted_address;
		   return $data->results;
		 }
		 else
		 {
		   return false;
		 }
	  }
	  
	  
	/*private function isvalid($request){
		//$phone = $request->input('phone');
		$check=$this->check_token_provider($request);

		if(!$check){
			return false;
		}
		return true;


	}*/
}
