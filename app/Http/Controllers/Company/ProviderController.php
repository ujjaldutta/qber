<?php
/*************************************************** Provider controller for company   ***************************************/
namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Provider;
use App\Models\Servicetype;
use App\Models\Servicestatus;
use App\Models\Country;
use App\Models\Company;
use App\Models\Providernotification;
use App\Models\ProviderCompanyAccount;
use App\Models\CompanyProvider;
use App\Models\Providersattlehistory;
use App\Models\Locationpreferencehead;

use Response;

use Illuminate\Support\Facades\Auth;

class ProviderController extends Controller
{
	public function __construct()
	{
		
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /***********************  List provider under company *******************************/
     
    public function index(Request $request)
    {

		
        $title="List Providers";
        $providers=Provider::leftJoin('provider_balance_history','providers.id','=','provider_balance_history.provider_id')
		->leftJoin('provider_profile','providers.id','=','provider_profile.provider_id')
		->leftJoin('provider_cancel_history','providers.id','=','provider_cancel_history.provider_id')
		->leftJoin('company_providers','providers.id','=','company_providers.provider_id')
		->leftJoin('service_types','providers.type_id','=','service_types.id')
		->leftJoin('provider_free_timeslot','providers.id','=','provider_free_timeslot.provider_id')
		->selectRaw("providers.id,providers.phone,providers.email,providers.name,provider_profile.current_credit_balance,provider_profile.max_due_amount,provider_profile.nationality,providers.approve_status,provider_profile.date_of_birth,provider_status,sms_verified,work_time,moneyexpvalue,
		TIMESTAMPDIFF( YEAR, now(),provider_profile.date_of_birth ) as age,provider_profile.gender,provider_profile.transport,is_online,ready_to_serve,
		service_types.name as service_type,preferred_location,(priority/sum(priority))*location_factor as priority_factor,providers.location_preference,
		
		(select AVG(rate_value) from provider_rating where providers.id=provider_rating.provider_id) as rating,
		(select service_status_id from service where providers.id=service.provider_id and service_status_id=5 limit 1) as job_status,
		(select (SUM(service.provider_call)/count(*)) from service where providers.id=service.provider_id and (service_status_id=2 OR service_status_id=8 OR 	service_status_id=10 OR service_status_id=11 ) )*100 as cancel_ratio,

		(select count(*) from provider_company_account where providers.id=provider_company_account.provider_id and sattled='no' and arrival_time>CURDATE()) as company_notice, 


		((SELECT count(*) from service where providers.id=service.provider_id and service_status_id='11')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_job_start_count,

		((SELECT count(*) from service where providers.id=service.provider_id and service_status_id='10')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_heading,
		

((select count(*) from service where providers.id=service.provider_id and service_status_id='8')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_confirm,

	

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='customer' and providers.id=service.provider_id )/(select count(*) from service left join dispute on(service.id=dispute.service_id) where providers.id=service.provider_id and service.service_status_id='4'  and providers.id=service.provider_id and dispute.dispute_to='customer')) as dispute_given_after_confirm,

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='provider' )/(select count(*) from service  left join dispute on(service.id=dispute.service_id) where providers.id=service.provider_id and service.service_status_id='4' and dispute.dispute_to='provider')) as dispute_received_after_confirm,
		
		(select sum(provider_cancel_history.penalty_amount) from provider_cancel_history where providers.id=provider_cancel_history.provider_id) as penalty_amount,
		
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where MONTH(date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) and providers.id=provider_balance_history.provider_id) as last_month_amount,
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where  provider_balance_history.date >= DATE(NOW()) - INTERVAL 7 DAY and providers.id=provider_balance_history.provider_id) as last_week_amount,
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) as total_amount,

		(select company.name from company inner join company_providers where company.id=company_providers.company_id and company_providers.provider_id=providers.id ) as companyname,

		(select count(*) from appointments  where providers.id=appointments.provider_id) as servicecount, (select count(*) from provider_cancel_history
		 where providers.id=provider_cancel_history.provider_id) as cancelcount")
		->groupby('providers.id');

		if($request->input('phone'))
		{
			$providers=$providers->where('providers.phone','like',$request->input('phone')."%");
		}
		if($request->input('id'))
		{
			$providers=$providers->where('providers.id','=',$request->input('id'));
		}

		
		if($request->input('name'))
		{
			$providers=$providers->where('providers.name','like',"%".$request->input('name')."%");
		}
		if($request->input('email'))
		{
			$providers=$providers->where('providers.email','like',"%".$request->input('email')."%");
		}
		
		
		if($request->input('servicetype'))
		{
			$providers=$providers->where('providers.type_id',$request->input('servicetype'));
		}
		if($request->input('nationality')){
			$providers=$providers->where('provider_profile.nationality',$request->input('nationality'));
		}

		
		if($request->input('total_come')!='')
		{
			$total_income=explode("-",$request->input('total_come'));
			
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id)>='.$total_income[0]);
			if($total_income[1]!='unlimited')
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) <='.$total_income[1]);
		}

	

		if($request->input('revenue_from_date')!=''  && $request->input('revenue_to_date')!='' && $request->input('from_income')!=''  && $request->input('to_income')!='')
		{
			$providers=$providers
			

			->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id and
			provider_balance_history.date>="'.$request->input('revenue_from_date').'" and provider_balance_history.date<="'.$request->input('revenue_to_date').'" ) between '.$request->input('from_income')." and ".$request->input('to_income')." ");
		}

		if($request->input('transport_mode'))
		{
			$providers=$providers->where('provider_profile.transport',$request->input('transport_mode'));
		}
		if($request->input('employment_type'))
		{
			if($request->input('employment_type')=='company')
			$providers=$providers->whereNotNull('company_providers.provider_id');
			else
			$providers=$providers->whereNull('company_providers.company_id');
		}

		if($request->input('current_area'))
		{
			$providers=$providers->where('preferred_location',$request->input('current_area'));
		}
		
		if($request->input('free_from_date')!='' && $request->input('free_to_date')!='')
		{
			
						
			$providers=$providers->where('provider_free_timeslot.from_time','>=',date("H:i:s",strtotime($request->input('free_from_date'))))
			->where('provider_free_timeslot.to_time','<=',date("H:i:s",strtotime($request->input('free_to_date'))))->where('time_type','free');
		}

		if($request->input('current_request'))
		{
			if($request->input('current_request')=='yes')
			$condition='>0';
			else
			$condition='=0';
			$providers=$providers->whereRaw("(select count(*) from provider_notification where providers.id=provider_notification.provider_id and invite_status='sent')".$condition);
		}

		if($request->input('account_limit'))
		{
			if($request->input('account_limit')=='yes')
			{
			$providers=$providers->whereRaw("provider_profile.current_credit_balance > provider_profile.max_due_amount and provider_profile.current_credit_balance>0");
			}else{
			$providers=$providers->whereRaw("provider_profile.current_credit_balance < provider_profile.max_due_amount and provider_profile.current_credit_balance>0");
			}
		}

		if($request->input('gender'))
		{
			$providers=$providers->where('provider_profile.gender',$request->input('gender'));
		}
		if($request->input('provider_status'))
		{
			$providers=$providers->where('providers.provider_status',$request->input('provider_status'));
		}

		if($request->input('account_status')=='active')
		{
			$providers=$providers->where('providers.is_active',1);
		}elseif($request->input('account_status')=='inactive')
		{
			$providers=$providers->where('providers.is_active',0);
		}elseif($request->input('account_status')=='approved')
		{
			$providers=$providers->where('providers.approve_status',1);
		}elseif($request->input('account_status')=='notapproved')
		{
			$providers=$providers->where('providers.approve_status',0);
		}


		
		if($request->input('age'))
		{
			$age=explode("-",$request->input('age'));
			$start = date('Y', strtotime('-'.$age[0].' years'));
			$end = date('Y', strtotime('-'.$age[1].' years'));
			
			
			$providers=$providers->where('provider_profile.date_of_birth','<=',$start)->where('provider_profile.date_of_birth','>=',$end);
		}

		$providers=$providers->where('company_providers.company_id',Auth::user()->id);
		
		
		if($request->input('sort') && $request->input('order'))
		{
			
			$providers=$providers->orderby($request->input('sort'),$request->input('order'))->paginate(100);
		}else
		{
			$providers=$providers->orderby('id','desc')->paginate(100);
		}

		$servicetype=Servicetype::get();
		$perference=Locationpreferencehead::get();


		$nationality=Country::select('code', 'countryname as name')->get();

		$total_priority=Provider::where("is_online",1)->where("ready_to_serve",1)->sum('moneyexpvalue');

				
        return view('company.provider.index',  compact('providers','title','servicetype','nationality','perference','total_priority'));





    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /***********************  Display edit provider screen  not required*******************************/
     
  /*  public function edit($id)
    {
        //
        $title="Edit Provider";
        $provider=Provider::find($id);
		
        return view('company.provider.edit',  compact('admin','title'));
    }

   */


     /***********************  Display Provider profile *******************************/

    public function profile($id){

	$provider=Provider::with('Profile','Companyaccount')->find($id);
	$title="Provider Profile";

	//print_r($provider->toArray());exit;

    return view('company.provider.profile',  compact('title','provider'));

	}

	/***********************  Display Job history of provider *******************************/

	public function jobhistory(Request $request)
	{
		$id=$request->id;
		if($id=='') return redirect('company/providers');
		$provider=Provider::leftJoin('service','providers.id','=','service.provider_id')
		->leftJoin('service_status','service.service_status_id','=','service_status.id')
		->leftJoin('appointments','appointments.service_id','=','service.id')
		->selectRaw("providers.phone,service_status.name,service.*,appointments.paid_amount as amount,appointments.date as appdate")
		;
		if($request->input('from_date') && $request->input('to_date')){
			$provider=$provider->whereBetween('appointments.date',array(date("Y-m-d H:i:s",strtotime($request->input('from_date'))),date("Y-m-d H:i:s",strtotime($request->input('to_date')))))		;
		}

		if($request->input('servicestatus')){
			$provider=$provider->where('service.service_status_id',$request->input('servicestatus'));
		}

		
		$provider=$provider->orderby('service.id','desc')->where('providers.id',$id)->paginate(100);
		$title="Provider Job History";

		$servicestatus=Servicestatus::get();

		return view('company.provider.jobhistory',  compact('title','provider','servicestatus','id'));
		

	}

	/***********************  Company can invite providers *******************************/

	public function invite(){
		
		/*$providers=Provider::with('Profile')->leftJoin('company_providers','providers.id','=','company_providers.provider_id')
		->selectRaw("providers.*")
		->whereNull('company_providers.provider_id')
		->where('approve_status','1')
		->where('sms_verified','1')
		->where('is_active','1')
		->orderby('providers.id','desc')->paginate(100);*/
		$title="Invite Provider";

		
		//print_r($providers->toArray());exit;
		

		return view('company.provider.invite',  compact('title','providers'));
		

	}

	/***********************  Send invitation to provider with notificaiton *******************************/

	public function sendinvite(Request $request){

		$phone=$request->input('phone');
		$provider=Provider::where('phone',$phone)->first();

		if(!$provider)

		{
			

			$request->session()->flash('message', 'Invalid Provider!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
		}

		
		$provider_id=$provider->id;
		//$provider_id=$request->input('provider_id');
		$company_id=Auth::user()->id;

		$invitecount=Providernotification::where("provider_id",$provider_id)->where("company_id",$company_id)->count();
		if($invitecount>0)

		{
			

			$request->session()->flash('message', 'Invitation Already Sent To Provider!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
		}
		
		$notification=new Providernotification();
		$notification->provider_id=$provider_id;
		$notification->company_id=$company_id;
		$notification->date=date('Y-m-d H:i:s');
		$notification->details='New invitation from '.Auth::user()->name;
		$notification->invite_status='sent';
		$notification->save();

		$company=Company::where("id",$company_id)->first();

		$provider=Provider::with("Profile")->where('id',$provider_id)->first();
		
		$msg=$company->name." Would like to invite you to join your company";

		if($provider->device_type=='ios' && $provider->device_id!=''){
		
			$arr = array('type'=>'companyinvite',"provider_name"=>$provider->name,"company_name"=>$company->name);
			$status= $this->sendProviderAPNS($msg,$arr,$provider->device_id);
			
			}elseif($provider->device_id!='' ){
				$arr = array('type'=>'companyinvite',"provider_name"=>$provider->name,"company_name"=>$company->name);
				$status=$this->sendProviderFCM($msg,$arr,$provider->device_id);

			}


			
		$request->session()->flash('message', 'Invitation Sent To Provider!');
		return redirect()->back();

	}



	/*************************************** Get active providers for map in ajax *******************************************/

	public function ajaxProviders(Request $request)
	{
		$term='%'.$request->term.'%';
		$providers=Provider::selectRaw('providers.id,phone as value, name')
		->leftJoin('company_providers','providers.id','=','company_providers.provider_id')
		->whereNull('company_providers.company_id')
		->whereRaw ('( name like "%'.$term.'%"   or phone like "%'.$term.'%")')->get();
		//print_r($providers->toArray());exit;
		return Response::json($providers);
	}

	/***********************  Display online providers to company  *******************************/

	public function livetrack(){
		
		
		$title="Active Providers";

		
		//print_r($providers->toArray());exit;
		

		return view('company.provider.livetrack',  compact('title','providers'));
		

	}

	/***********************  Display provider with profile  for live track *******************************/

	public function getProviders(Request $request)
	{

		$providers=Provider::with('Profile')->leftJoin('company_providers','providers.id','=','company_providers.provider_id')
		->selectRaw("providers.*")
		->whereNotNull('company_providers.provider_id')
		->where('company_providers.company_id',Auth::user()->id)
		->where('approve_status','1')
		->where('sms_verified','1')
		->where('is_active','1')
		->where('is_online','1')
		->get();

		return Response::json(array('success'=>1,'providers'=>$providers));
			die;
	}

	/***********************  Display balance sattle screen  *******************************/

	public function sattleBalance(Request $request)
	{
		$provider=Provider::leftJoin('provider_company_account','providers.id','=','provider_company_account.provider_id')->find($request->id);

		$title="Sattle Balance";

		
		//print_r($provider->toArray());exit;
		

		return view('company.provider.sattlebalance',  compact('title','provider'));
	}

	/***********************  Company can update balance of providers *******************************/

	public function updateBalance(Request $request){
		$provider=CompanyProvider::with('Account')->where('provider_id',$request->id)->first();
		//print_r($provider->toArray());exit;

		
		$account=ProviderCompanyAccount::where('provider_id',$provider->id)->first();
		$account->current_credit_balance=$account->current_credit_balance-$request->current_credit_balance;
		$account->save();


		$history=new Providersattlehistory;
        $history->admin_id=null;
        $history->company_id=Auth::user()->id;
		$history->provider_id=$request->id;
		$history->amount=$request->current_credit_balance;
		$history->date=date("Y-m-d H:i:s");
		$history->save();



		$msg=$provider->name." You have successfully settled ".$request->current_credit_balance." QAR from you account";

		if($provider->device_type=='ios' && $provider->device_id!=''){
					
				$arr = array('type'=>'balancesattled',"provider_name"=>$provider->name,"amount"=>$request->current_credit_balance);
				$status= $this->sendProviderAPNS($msg,$arr,$provider->device_id);
			
			}elseif($provider->device_id!='' ){

				$arr = array('type'=>'balancesattled',"provider_name"=>$provider->name,"amount"=>$request->current_credit_balance);
				$status= $this->sendProviderFCM($msg,$arr,$provider->device_id);
			}





		$request->session()->flash('message', 'Balance is updated!');

		 return redirect()->back();
	}

	/***********************  notification for android *******************************/

	private	function sendProviderFCM($title,$body,$id)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
				'to' => $id,
				/*'notification' => array (
						"body" => ($body),
						"title" => $title,
						"icon" => "myicon"
				),*/
				'data'=>$body
		);

		
		$fields = json_encode ( $fields );
		$headers = array (
				'Authorization: key=' . env('FCM_KEY',"AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg"),
				'Content-Type: application/json'
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		return $result;
	}

	/***********************  notification for IOS *******************************/

	private function sendProviderAPNS($title,$body,$id)
	{

		// Put your device token here (without spaces):
		$deviceToken = $id;
		// Put your private key's passphrase here:
		$passphrase = env('APNS_KEY',"xxxxxxx");
		// Put your alert message here:
		$message = $body;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck_file.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'body' => $message,
				'action-loc-key' => $title,
			),
			'badge' => 2,
			'sound' => 'oven.caf',
			);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		if (!$result)
			return false;
		else
			return $result;
		// Close the connection to the server
		fclose($fp);

	}
	

}
