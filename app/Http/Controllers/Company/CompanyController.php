<?php
/*************************************************** Company controller for company   ***************************************/
namespace App\Http\Controllers\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Company;

use App\Models\Provider;
use App\Models\Customer;
use App\Models\Service;
use App\Models\CompanyProvider;


use App\Http\Requests;

class CompanyController extends Controller
{
    /**
     * Assign company middleware for authentication.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
		{
			$this->middleware('company');
		}




	/***********************  Dashboard of company *******************************/
	 
    public function index()
    {
		
		//print_r($user);
		
		$title = "Dashboard";

       
        $service = Service::leftJoin('providers','providers.id','=','service.provider_id')
		
		->leftJoin('company_providers','service.provider_id','=','company_providers.provider_id')
		->where("company_providers.company_id",Auth::user()->id)
		->groupby('service.id')->count();
        $providers = CompanyProvider::where("company_providers.company_id",Auth::user()->id)->count();
	
	return view('company.dashboard',  compact('title','service','providers'));
	
    }


    /***********************  Profile of company *******************************/

    public function profile(){
		$user=Auth::user();

		$company=Company::find($user->id);
		$title = "Company Profile";
		

		return view('company.profile',  compact('title','company'));
	}



	/***********************  Update profile of company *******************************/


	public function updateprofile(Request $request){
		$user=Auth::user();

		$company=Company::find($user->id);
		$password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
        }
         
		if($pass)
        $company->password=$pass;
        $company->address=$request->address;
        
        $company->update();
		$request->session()->flash('message', 'Updated Successfully!');
         return redirect()->back();
		
	}

}
