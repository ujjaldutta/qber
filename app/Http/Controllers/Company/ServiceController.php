<?php
/*************************************************** Service controller for company to display which is under provider **********************************/

namespace App\Http\Controllers\Company;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\Country;
use App\Models\Service;
use App\Models\Servicestatus;
use App\Models\Servicetype;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller {

	public function __construct()
	{
		
	}

	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /***********************  List service under company *******************************/
     
    public function index(Request $request)
    {
			


         $title="Service List";
     
		$services=Service::leftJoin('providers','providers.id','=','service.provider_id')
		
		->leftJoin('appointments','appointments.service_id','=','service.id')
		->leftJoin('service_status','service_status.id','=','service.service_status_id')
		->selectRaw("service.*,providers.phone,company_providers.*,sum(appointments.paid_amount) as amount,service_status.name")
		->leftJoin('company_providers','service.provider_id','=','company_providers.provider_id')
		->groupby('service.id');

		$services=$services->where('company_providers.company_id',Auth::user()->id)
		->whereNotNull('company_providers.company_id')->where('service.service_status_id','<>',2);
		
		
		if($request->input('servicetype')){
			$services=$services->where('service.type_id',$request->input('servicetype'));
		}

		if($request->input('nationality')){
			$services=$services->where('service.nationality',$request->input('nationality'));
		}

		if($request->input('status')){
			$services=$services->where('service.service_status_id',$request->input('status'));
		}

		if($request->input('minutes')){
			$current=date("Y-m-d H:i:s");
			$pastime = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -".$request->input('minutes')." minutes"));
			
			$services=$services->whereBetween('service.request_date',array($pastime,$current));
		}
		
		
		if($request->input('sort') && $request->input('order')){
			
			$services=$services->orderby($request->input('sort'),$request->input('order'))->paginate(100);
		}else{
			$services=$services->orderby('service.id','desc')->paginate(100);
		}

		$servicetype=Servicetype::get();
		


		$nationality=Country::select('code', 'countryname as name')->get();
		$servicestatus=Servicestatus::get();


		
        return view('company.service.index',  compact('services','title','servicetype','nationality','servicestatus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /***********************  Edit service under company *******************************/

     
    public function edit($id)
    {
        $title="Edit Service";
        $service=Service::find($id);
		$servicetype=Servicetype::pluck('name', 'id');

		$country=Country::pluck('countryname', 'code as id');

		$providers=Provider::leftJoin('company_providers','providers.id','=','company_providers.provider_id')

		->pluck('providers.name','providers.id');

		
		$status=Servicestatus::pluck('name', 'id');
		
        return view('company.service.create_edit',  compact('servicetype','title','service','country','status','providers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	 /***********************  Update service under company *******************************/

    public function update(Request $request, $id)
    {

       $service=Service::find($id);
         
		
        $service->type_id=$request->type_id;
        $service->customer_id=$request->customer_id;
        if($request->provider_id)
        $service->provider_id=$request->provider_id;
        if($request->booking_slot_id)
         $service->booking_slot_id=$request->booking_slot_id;
		 if($request->provider_id)
        $service->provider_id=$request->provider_id;
        
       
        $service->job_description=$request->job_description;
		$service->request_date=$request->request_date;
        $service->latitude=$request->latitude;
		$service->longitude=$request->longitude;
		$service->from_time=$request->from_time;
		$service->to_time=$request->to_time;
		$service->nationality=$request->nationality;
		$service->team_size=$request->team_size;
		$service->job_duration_set_by_provider =$request->job_duration_set_by_provider;
        $service->expected_cost =$request->expected_cost;
        $service->service_status_id =$request->service_status_id;
        
        $service->update();
        
		$request->session()->flash('message', 'Service Updated Successfully!');
         return redirect()->back();
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /***********************  Delete service under company *******************************/

     
    public function destroy($id)
    {
        $affectedRows  = Service::where('id', '=', $id)->delete();

		return $affectedRows;
    }

  
	

}
