<?php
/*************************************************** For Dashboard in Provider  App *******************************************************************/

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Provider;
use App\Models\Providerprofile;
use App\Models\Providerfreetime;
use App\Models\Providerbookedtime;
use App\Models\Servicesettings;
use App\Models\Service;
use App\Models\Customer;
use App\Models\Providerbalancehistory;
use DB;
use Log;

use App\Traits\ProviderServiceTrait;

class ProviderDashboardController extends Controller {

	use ProviderServiceTrait;

	public function __construct()
	{
		//$this->middleware('auth:api');
	}



	/***********************  return all dashboard status like next work,break,working,heading, must head now according to current time***********************/ 

	public function getCurrentSchedule(Request $request){

		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

		$datetime = ($request->input('current_date_time'))?date("Y-m-d H:i:s",strtotime($request->input('current_date_time'))):date('Y-m-d H:i:s');
		$date = ($request->input('current_date_time'))?date("Y-m-d",strtotime($request->input('current_date_time'))):date('Y-m-d');
		$currenttime = ($request->input('current_date_time'))?date("H:i:s",strtotime($request->input('current_date_time'))):date('H:i:s');
		//Log::info('---------------------------------------------------------');
		//Log::info('Manual date input : '.$request->input('current_date_time'));
		//Log::info('Current Server Time : '.date('Y-m-d H:i:s'));
		//Log::info('Provider : '.$request->input('phone'));
		//Log::info('---------------------------------------------------------');
		$phone = $request->input('phone');
		$provider=Provider::with("Profile")->where('phone',$phone)->first();
		$mode=($provider->profile->transport=='walk')?'walking':'driving';


		$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$provider->type_id)->first();
		$buffer_time=(int)$buffer_time->value;
		
		

		$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)->where(DB::raw("DATE(book_date)"),"=",$date)->whereRaw("from_time>'".$datetime ."'")
		->whereHas('Service', function ($query) {

						
						$query->where('service_status_id',4);
						
						$query->orwhere('service_status_id',5);
						$query->orwhere('service_status_id',9);
						//$query->orwhere('service_status_id',6);
						
					})
		->orderby('from_time','asc')->first();


		

		$freetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",$date)
		->whereRaw("'".$currenttime ."' between  from_time and to_time")		
		->orderby('from_time','asc')->first();


		$result=array();
		$futuredate=date("Y-m-d",strtotime("+7 days",strtotime($date)));
		$result['total_earning']=Providerbalancehistory::where("provider_id",$provider->id)->sum("receive_amount");
		$result['due_payment']=$this->getProviderCredit($provider);
		$total_priority=Provider::where("is_online",1)->where("ready_to_serve",1)->sum('moneyexpvalue');
		//echo '<br/>--------------- ';
		//echo $provider->moneyexpvalue;
		//echo "test";
		
		if($total_priority>0 && $provider->moneyexpvalue>0)
		$result['priority']=number_format(($provider->moneyexpvalue/$total_priority)*100,2,".","");
		else
		$result['priority']=0;

		//echo $result['priority'];
		//echo '-------';exit;

		$result['online_status']=(string)$provider->is_online;
		$result['ready_to_serve']=(string)$provider->ready_to_serve;
		$result['current_position']=array("latitude"=>$provider->current_latitude,"longitude"=>$provider->current_longitude);
				
		$result['job']=array();
		$result['type']='unavailable';


		if($freetime )
		{
			$status=0;
			$hservice=Service::whereRaw("provider_id='".$provider->id."' and (service_status_id='5' )")->first();
			if($hservice)
			$status=5;
			$hservice2=Service::whereRaw("provider_id='".$provider->id."' and (service_status_id='9' )")->first();
			if($hservice2)
			$status=9;
			
			//if free then get next booking time...if any one work is in heading or working status then pick that job
			if($status>0)
			{
				$bookedcalendar=Providerbookedtime::with('Service')->whereHas('Service', function ($query) use($status){
					$query->where('service_status_id',$status);
					})
					->where('provider_id',$provider->id)->orderby('from_time','asc')->first();

			}
			else
			{
			$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)->where(DB::raw("DATE(book_date)"),"=",$date)->whereRaw("'".$datetime ."' between  from_time and to_time")
		->whereHas('Service', function ($query) {
						
						$query->where('service_status_id',4);
						
						
					})

			->orderby('from_time','asc')->first();
		}

			//print_r($bookedcalendar->toArray());exit;
			
			$result['type']=($freetime->time_type=='unavailable')?'break':$freetime->time_type;
			

						if($bookedcalendar){
							

						$freetime2=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",$date)
						->whereRaw("'".date("H:i:s",strtotime($bookedcalendar->from_time))."' between from_time and to_time")		
						->orderby('from_time','asc')->first();

						$max_head_deadline=date("Y-m-d H:i:s",strtotime("-".$bookedcalendar->service->estimated_traveltime_heading." Minutes",strtotime($bookedcalendar->service->to_time)));
						$heading_deadline=date("Y-m-d H:i:s",strtotime("-".$bookedcalendar->service->estimated_traveltime_heading." Minutes",strtotime($bookedcalendar->from_time)));


						$customer=Customer::where("id",$bookedcalendar->service->customer_id)->first();
						$result['job']=array("address"=>$bookedcalendar->service->address,"area_name"=>$bookedcalendar->service->area_name,"latitude"=>$bookedcalendar->service->latitude,"longitude"=>$bookedcalendar->service->longitude,
							"job_duration"=>$bookedcalendar->service->job_duration_set_by_provider,"id"=>$bookedcalendar->service->id,"job_id"=>$bookedcalendar->service->job_id,"customer_phone"=>$customer->phone,
							"from_time"=>$bookedcalendar->from_time,"to_time"=>$bookedcalendar->to_time,"price"=>$bookedcalendar->service->expected_cost,"service_location"=>$this->getaddress($bookedcalendar->service->latitude,$bookedcalendar->service->longitude) ,
							"service_status_id"=>$bookedcalendar->service->service_status_id,"arrival_from_time"=>$bookedcalendar->service->from_time,"arrival_to_time"=>$bookedcalendar->service->to_time
							);

						
						
							if($bookedcalendar->service->service_status_id==9){
								$result['type']='heading';
								$result['start_time']=$bookedcalendar->from_time;
								$result['arrival_deadline']=round((strtotime($bookedcalendar->service->to_time)-strtotime($currenttime))/60);

							}
							elseif($bookedcalendar->service->service_status_id==5
							//|| $bookedcalendar->service->service_status_id==6
							){
								$result['type']='working';
								$result['end_time']=$bookedcalendar->to_time;
								if($bookedcalendar->to_time<$datetime)
								$result['spent_time']=round((strtotime($datetime)-strtotime($bookedcalendar->from_time))/60);
								else
								$result['spent_time']=round((strtotime($bookedcalendar->to_time)-strtotime($bookedcalendar->from_time))/60);

								
								$result['price']=$bookedcalendar->service->expected_cost;

							}
							elseif($datetime>=$bookedcalendar->heading_start_time && $datetime<=$max_head_deadline && ($bookedcalendar->service->service_status_id==4 ))
								{
									$hservice=Service::with("Booked")->whereRaw("provider_id='".$provider->id."' and (service_status_id='5' )")->first();
									if($hservice){

										$result['type']='working';
										$result['end_time']=$hservice->booked->to_time;
										$result['spent_time']=round((strtotime($hservice->booked->to_time)-strtotime($hservice->booked->from_time))/60);
										$result['price']=$hservice->expected_cost;

										$nhead=Providerbookedtime::with('Service')
										->where('provider_id',$provider->id)->where(DB::raw("DATE(book_date)"),"=",$date)
										->whereRaw("from_time>'".$hservice->booked->to_time."'")
										->whereHas('Service', function ($query) {
											$query->where('service_status_id',4);
											})
										->orderby('from_time','asc')->first();
										if($nhead && $nhead->head_start_time<$datetime)
										$result['job']['next_head_job']=$nhead->id;

										
										

									}else{
										$result['type']='must head now';
										$provider->provider_status='must head now';
										$heading_deadline=date("Y-m-d H:i:s",strtotime("-".$bookedcalendar->service->estimated_traveltime_heading." Minutes",strtotime($bookedcalendar->service->to_time)));

									}
								}
							elseif( $bookedcalendar->service->service_status_id==6){
								$result['type']='free';
								$result['end_time']=$bookedcalendar->to_time;
								$result['spent_time']=round((strtotime($bookedcalendar->to_time)-strtotime($bookedcalendar->from_time))/60);
								
								$result['price']=$bookedcalendar->service->expected_cost;

							}
							elseif($freetime2 && $freetime2->time_type=='free' && $freetime2->id!=$freetime->id){

								
								$result['free_until']=$bookedcalendar->heading_start_time;

							}elseif($freetime2 && $freetime2->time_type!='free' && $freetime2->id!=$freetime->id){
								
								$result['free_until']=$freetime->date." ".$freetime->to_time;
							
							}elseif($freetime->time_type=='free'){
								$result['free_until']=$bookedcalendar->heading_start_time ;
							}
							


							

							
							
							
							

							$result['job']["heading_deadline"]=$heading_deadline;




							
							$nexthead_time=null;
							
							$nextbooking=Providerbookedtime::with('Service')->where('provider_id',$provider->id)->where(DB::raw("DATE(book_date)"),"=",$date)->whereRaw(" from_time>= '".$bookedcalendar->to_time ."'")->orderby('sort_order','asc')->first();

							
							if($nextbooking){

								
							$nexthead_time=$nextbooking->heading_start_time;
							}

							
							$result['job']['nextheading_deadline']=$nexthead_time;
							

						}elseif($freetime->time_type=='free'){
							
							
							
							$nextbooking=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
							->where(DB::raw("DATE(book_date)"),"=",$date)->whereRaw(" from_time>= '".$datetime ."'")
							->whereHas('Service', function ($query) {
								$query->where('service_status_id',4);
								$query->orwhere('service_status_id',5);
								$query->orwhere('service_status_id',9);
								//$query->orwhere('service_status_id',6);
								
							})
			
							->orderby('from_time','asc')->first();

							//print_r($nextbooking->toArray());exit;
							
							if($nextbooking){


							
								$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
								$origin='destinations='.$nextbooking->service->latitude.','.$nextbooking->service->longitude;
								$destination='&origins='.$provider->current_latitude.','.$provider->current_longitude;
								
								$data=$this->httpGet($url.$origin.$destination."&mode=".$mode."&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
								$data=json_decode(($data),true);
								if(!isset($data['rows'][0]['elements'][0]['duration']))
								return false;
								$duration=($data['rows'][0]['elements'][0]['duration']['value']/60);
								$distance=($data['rows'][0]['elements'][0]['distance']['value']/1000);

								$duration=(int)$duration+(int)$buffer_time;
								
								
								$nexthead_time=date("Y-m-d H:i:s",strtotime("-".$duration." Minutes",strtotime($nextbooking->from_time)));

								$max_head_deadline=date("Y-m-d H:i:s",strtotime("-".$duration." Minutes",strtotime($nextbooking->service->to_time)));
								
								$result['free_until']=$nexthead_time;
								
								$customer=Customer::where("id",$nextbooking->service->customer_id)->first();

								$result['job']=array("address"=>$nextbooking->service->address,"area_name"=>$nextbooking->service->area_name,"latitude"=>$nextbooking->service->latitude,"longitude"=>$nextbooking->service->longitude,
								"job_duration"=>$nextbooking->service->job_duration_set_by_provider,"id"=>$nextbooking->service->id,"job_id"=>$nextbooking->service->job_id,"customer_phone"=>$customer->phone,
								"from_time"=>$nextbooking->from_time,"to_time"=>$nextbooking->to_time,"price"=>$nextbooking->service->expected_cost
								,"service_location"=>$this->getaddress($nextbooking->service->latitude,$nextbooking->service->longitude) ,
								"service_status_id"=>$nextbooking->service->service_status_id,"arrival_from_time"=>$nextbooking->service->from_time,"arrival_to_time"=>$nextbooking->service->to_time
								);

								
								
								$result['job']['heading_deadline']=$max_head_deadline;

							

								if($nextbooking->service->service_status_id=='9')
								{
									
								$result['type']='heading';
								$result['arrival_deadline']=round((strtotime($nextbooking->service->to_time)-strtotime($currenttime))/60);
								
								}
								elseif($nextbooking->service->service_status_id=='5')
								$result['type']='working';
								elseif($datetime>=$nextbooking->heading_start_time && $datetime<=$max_head_deadline && ($nextbooking->service->service_status_id=='4' ))
								{

									$hservice=Service::with("Booked")->whereRaw("provider_id='".$provider->id."' and (service_status_id='5' )")->first();
									if($hservice){

										$result['type']='working';
										$result['end_time']=$hservice->booked->to_time;
										if($hservice->booked->to_time<$datetime)
										$result['spent_time']=round((strtotime($datetime)-strtotime($hservice->booked->from_time))/60);
										else
										$result['spent_time']=round((strtotime($hservice->booked->to_time)-strtotime($hservice->booked->from_time))/60);
										$result['price']=$hservice->expected_cost;


										$nhead=Providerbookedtime::with('Service')
										->where('provider_id',$provider->id)->where(DB::raw("DATE(book_date)"),"=",$date)
										->whereRaw("from_time>'".$hservice->booked->to_time."'")
										->whereHas('Service', function ($query) {
											$query->where('service_status_id',4);
											})
										->orderby('from_time','asc')->first();
										if($nhead && $nhead->head_start_time<$datetime)
										$result['job']['next_head_job']=$nhead->id;
										

									}else{
									
									$result['type']='must head now';
									$provider->provider_status='must head now';

									}
								}else{
									
									/************************previous heading missed so display next heading**************************/
									$nbooking=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
									->where(DB::raw("DATE(book_date)"),"=",$date)->whereRaw(" from_time>= '".$nextbooking->to_time ."'")
									->whereHas('Service', function ($query) {
										$query->where('service_status_id',5);
										
										
									})
					
									->orderby('from_time','asc')->first();
								
									if($nbooking){

																$result['type']='working';
																$result['end_time']=$nbooking->to_time;
																if($nbooking-to_time<$datetime)
																$result['spent_time']=round((strtotime($datetime)-strtotime($nbooking->from_time))/60);
																else
																$result['spent_time']=round(strtotime($nbooking->to_time)-strtotime($nbooking->from_time))/60;
																$result['price']=$hservice->expected_cost;


																$nhead=Providerbookedtime::with('Service')
																->where('provider_id',$provider->id)->where(DB::raw("DATE(book_date)"),"=",$date)
																->whereRaw("from_time>'".$nbooking->to_time."'")
																->whereHas('Service', function ($query) {
																	$query->where('service_status_id',4);
																	})
																->orderby('from_time','asc')->first();
																if($nhead && $nhead->head_start_time<$datetime)
																$result['job']['next_head_job']=$nhead->id;
																

														
														/**************************************************************/

												}else{

													$result['type']='free';
												}

							
									
								}
								
							
							}else{
							$result['free_until']=$freetime->date." ".$freetime->to_time;
							}
			
							
							

						}
						elseif($freetime->time_type!='free')
						{

							
							$result['from_time']=$freetime->date." ".$freetime->from_time;
							$result['to_time']=$freetime->date." ".$freetime->to_time;

							$nextbooking=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
							->where(DB::raw("DATE(book_date)"),"=",$date)->whereRaw(" from_time>= '".$date." ".$freetime->to_time ."'")
							->whereHas('Service', function ($query) {
								$query->where('service_status_id',4);
								$query->orwhere('service_status_id',5);
								$query->orwhere('service_status_id',9);
								//$query->orwhere('service_status_id',6);
								
							})
							->orderby('sort_order','asc')->first();
							
							if($nextbooking){
							$nexthead_time=$nextbooking->heading_start_time;
							}

							
							if($nextbooking){
							$customer=Customer::where("id",$nextbooking->service->customer_id)->first();

							$result['job']=array("address"=>$nextbooking->service->address,"area_name"=>$nextbooking->service->area_name,"latitude"=>$nextbooking->service->latitude,"longitude"=>$nextbooking->service->longitude,
							"job_duration"=>$nextbooking->service->job_duration_set_by_provider,"id"=>$nextbooking->service->id,"job_id"=>$nextbooking->service->job_id,"customer_phone"=>$customer->phone,
							"from_time"=>$nextbooking->from_time,"to_time"=>$nextbooking->to_time,"price"=>$nextbooking->service->expected_cost
							,"service_location"=>$this->getaddress($nextbooking->service->latitude,$nextbooking->service->longitude) ,
							"service_status_id"=>$nextbooking->service->service_status_id,"arrival_from_time"=>$nextbooking->service->from_time,"arrival_to_time"=>$nextbooking->service->to_time
							);
							$result['job']['heading_deadline']=$nexthead_time;
							}

							
							if($provider->provider_status=='heading'){
								
								$result['type']='heading';
								$result['arrival_deadline']=round((strtotime($nextbooking->service->to_time)-strtotime($currenttime))/60);
							}
							
						}

						
						$gap=0;
						if(count($result['job'])>0){
						$gap=strtotime($result['job']['heading_deadline'])-strtotime($datetime);
						$gap=round(abs($gap) / 60,2);
						}
						if($result['type']=='heading'){
							$provider->provider_status='heading';
						}elseif(count($result['job'])>0 && $gap<30 && $gap>0 && ($result['type']=='break' || $result['type']=='unavailable')){
							
							$provider->provider_status='must head now';
						}
						elseif($result['type']=='break' || $result['type']=='unavailable'){
							$provider->provider_status='break';
						}elseif($result['type']=='free'){
							$provider->provider_status='free';
						}
						
						elseif($result['type']=='working'){
							$provider->provider_status='working';
						}else{
							$provider->provider_status='unavailable';
						}
						$provider->save();


						
			$result['success']=0;	
			
		}
		elseif($bookedcalendar)
		{
			
			$customer=Customer::where("id",$bookedcalendar->service->customer_id)->first();
			$nexthead_time=null;

			$heading_deadline=date("Y-m-d H:i:s",strtotime("-".$bookedcalendar->service->estimated_traveltime_heading." Minutes",strtotime($bookedcalendar->service->to_time)));

			$freetime2=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",$date)
						->whereRaw("'".date("H:i:s",strtotime($heading_deadline))."' between from_time and to_time and time_type='free'")		
						->orderby('from_time','asc')->first();
			if($freetime2){
				$result['free_from']=$date." ".$freetime2->from_time;

			}
			
			$result['job']=array("address"=>$bookedcalendar->service->address,"area_name"=>$bookedcalendar->service->area_name,"latitude"=>$bookedcalendar->service->latitude,"longitude"=>$bookedcalendar->service->longitude,
							"job_duration"=>$bookedcalendar->service->job_duration_set_by_provider,"id"=>$bookedcalendar->service->id,"job_id"=>$bookedcalendar->service->job_id,"customer_phone"=>$customer->phone,
							"from_time"=>$bookedcalendar->from_time,"to_time"=>$bookedcalendar->to_time,"heading_deadline"=>$heading_deadline,"price"=>$bookedcalendar->service->expected_cost
							,"service_location"=>$this->getaddress($bookedcalendar->service->latitude,$bookedcalendar->service->longitude),
							"service_status_id"=>$bookedcalendar->service->service_status_id,"arrival_from_time"=>$bookedcalendar->service->from_time,"arrival_to_time"=>$bookedcalendar->service->to_time
							
							);

			
			$nexthead_time=date("Y-m-d H:i:s",strtotime("-".$bookedcalendar->service->estimated_traveltime_heading." Minutes",strtotime($bookedcalendar->service->to_time)));
			if($nexthead_time!=$heading_deadline)
			$result['job']['nextheading_deadline']=$nexthead_time;

			if($bookedcalendar->service->service_status_id==5){
				$result['type']='working';
			}elseif($bookedcalendar->service->service_status_id==9){
				$result['type']='heading';
				$result['arrival_deadline']=round((strtotime($bookedcalendar->service->to_time)-strtotime($currenttime))/60);
			}
			


						$gap=0;
						if(count($result['job'])>0){
						$gap=strtotime($nexthead_time)-strtotime($datetime);
						$gap=round(abs($gap) / 60,2);
						}
						
						if(count($result['job'])>0 && $gap<30 && $gap>0 ){
							
							$provider->provider_status='must head now';
						}
						$provider->save();

		$result['success']=0;
						
		}
		else{

			$freetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",$date)
						->where("time_type","free")		
						->orderby('from_time','asc')->first();
			if($freetime)
			$result['free_from']=$freetime->date." ".$freetime->from_time;
			$result['success']=2;
			$result['msg']="There is no schedule";
		}

		

		$headers = ['Content-type'=> 'application/json; charset=utf-8'];
		return Response::json($result, 200, $headers, JSON_FORCE_OBJECT);
    
		

	}


	/***********************  Check login validity of provider ***********************/ 
	
	private function isvalid($request){
		//$phone = $request->input('phone');
		$check=$this->check_token_provider($request);

		if(!$check){
			return false;
		}
		return true;


	}

	/***********************  get full address of given lat lng ***********************/
	

	private function getaddress($lat,$lng)
	  {
		 $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		 $json = @file_get_contents($url);
		 $result=json_decode(($json),true);
		  
		 $status = $result['status'];
		
		 if($status=="OK")
		 {
			 $result=$result['results'];

			 $location = array();

				  foreach ($result[0]['address_components'] as $component) {

					switch ($component['types']) {
					  case in_array('street_number', $component['types']):
						$location['street_number'] = $component['long_name'];
						break;
					  case in_array('route', $component['types']):
						$location['street'] = $component['long_name'];
						break;
					  case in_array('sublocality', $component['types']):
						$location['sublocality'] = $component['long_name'];
						break;
					  case in_array('locality', $component['types']):
						$location['locality'] = $component['long_name'];
						break;
					  case in_array('administrative_area_level_2', $component['types']):
						$location['admin_2'] = $component['long_name'];
						break;
					  case in_array('administrative_area_level_1', $component['types']):
						$location['admin_1'] = $component['long_name'];
						break;
					  case in_array('postal_code', $component['types']):
						$location['postal_code'] = $component['long_name'];
						break;
					  case in_array('country', $component['types']):
						$location['country'] = $component['short_name'];
						break;
					}

				  }
			
			$location['formatted_address']=$result[0]['formatted_address'];
			 //['results']['0']['address_components']['5']['long_name'];
		   //return $data->results[0]->formatted_address;
		   return $location;
		 }
		 else
		 {
		   return false;
		 }
	  }


	/***********************  get distance and duration base on source and destination lat lng ***********************/
	  	
	private function httpGetGoogle($url)
		{
			$ch = curl_init();  
			//echo $url=urlencode($url);
			curl_setopt($ch,CURLOPT_URL,($url));
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		//  curl_setopt($ch,CURLOPT_HEADER, false); 
		 
			$output=curl_exec($ch);
			$info = curl_getinfo($ch);
			
		 
			curl_close($ch);
			return $output;
		}
}
