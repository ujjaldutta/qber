<?php
/*************************************************** Service settings for different service type  *******************************************/
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Servicesettings;

class ServicesettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $title="List Service Settings";
        $settings=Servicesettings::with('Servicetype')->orderby('code','asc')->get();

       			
        return view('admin.servicesettings.index',  compact('settings','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $title="Edit Service Settings";
        $settings=Servicesettings::with('Servicetype')->find($id);

     
		
        return view('admin.servicesettings.create_edit',  compact('settings','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $setting=Servicesettings::find($id);

       $setting->code=($request->code)?$request->code:'';
       $setting->value=($request->value)?$request->value:'';

		if($request->description)
       $setting->description=$request->description;
   

        $setting->update();
        
		$request->session()->flash('message', 'Settings Updated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
