<?php
/*************************************************** Help FAQ Video CMS  Panel for admin *******************************************************************/
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Qberhelp;

class QberhelpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $title="List Help";
        $qberhelp=Qberhelp::orderby('id','desc')->get();

       			
        return view('admin.qberhelp.index',  compact('qberhelp','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Help for App";

		return view('admin.qberhelp.create_edit',  compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $type = new Qberhelp();
        $link = "";
        if ($request->hasFile('video')) {
            $file = $request->file('video');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $link = sha1($filename . time()) . '.' . $extension;
            $type->link = "media/help/".$link;
        }
        

        
        $type->type=$request->type;
        $type->question=$request->question;
        $type->answer=$request->answer;
        $type->is_active=1;
        $type->save();
		if ($request->hasFile('video')) {
            
            $destinationPath = public_path() . '/media/help/' ;
            $request->file('video')->move($destinationPath, $link);

        }
       $request->session()->flash('message', 'Help content added successfully!');
	   return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusupdate($id,$status,Request $request)
    {
		
       $type=Qberhelp::find($id);
       $type->is_active=($status=='1')?0:1;
       $type->update();
       $request->session()->flash('message', 'Help Content Updated Successfully!');
       return redirect()->back();
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Help Content";
        $qberhelp=Qberhelp::find($id);

       
		
        return view('admin.qberhelp.create_edit',  compact('qberhelp','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $type=Qberhelp::find($id);

         $video = $type->link;
        if ($request->hasFile('video')) {
            $file = $request->file('video');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $video = sha1($filename . time()) . '.' . $extension;
            $type->link = "media/help/".$video;
        }
        
        $type->type=$request->type;
        $type->question=$request->question;
        $type->answer=$request->answer;
        $type->update();
        if ($request->hasFile('video')) {
            
            $destinationPath = public_path() . '/media/video/' ;
            $request->file('video')->move($destinationPath, $video);

        }
        
		$request->session()->flash('message', 'Help Content Updated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$affectedRows  = Qberhelp::where('id', '=', $id)->first();
        @unlink($affectedRows->link);
        $affectedRows  = Qberhelp::where('id', '=', $id)->delete();

		return $affectedRows;
    }
}
