<?php

/*************************************************** Customer  Panel for admin *******************************************************************/

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Customerprofile;
use App\Models\Locationpreferencehead;
use App\Models\Service;
use App\Models\Providerworktime;
use App\Models\Providertimehistory;
use App\Models\Providerbookedtime;

use App\Models\CustomerMessage;
use App\Models\Servicestatus;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class CustomerController extends Controller {

	public function __construct()
	{
		
	}

	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title="List Customers";
        $customers=Customer::
        leftJoin('customer_profile','customers.id','=','customer_profile.customer_id')
        ->leftJoin('customer_cancel_history','customers.id','=','customer_cancel_history.customer_id')
		->leftJoin('appointments','customers.id','=','appointments.customer_id')
        ->leftJoin('customer_rating','customers.id','=','customer_rating.customer_id')
        ->leftJoin('service','customers.id','=','service.customer_id')
        
		->selectRaw("customers.*,customer_profile.total_search_submit,customer_profile.last_search,customer_profile.name,(select count(*) from service where customers.id=service.customer_id) as servicecount,
		
		(select count(*) from service where customers.id=service.customer_id and service_status_id='6') as servicecompletecount,

		
		((select count(*) from service where customers.id=service.customer_id and service_status_id='11')/(select count(*) from service where customers.id=service.customer_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_job_start_count,

		((select count(*) from service where customers.id=service.customer_id and service_status_id='10')/(select count(*) from service where customers.id=service.customer_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_heading,

		((select count(*) from service where customers.id=service.customer_id and service_status_id='8')/(select count(*) from service where customers.id=service.customer_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_confirm,

		

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='provider' and customers.id=service.customer_id )/(select count(*) from service left join dispute on(service.id=dispute.service_id) where customers.id=service.customer_id and service.service_status_id='4'  and customers.id=service.customer_id))*100 as dispute_given_after_confirm,

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='customer' and customers.id=dispute.customer_id  )/(select count(*) from service left join dispute on(service.id=dispute.service_id) where customers.id=service.customer_id and service.service_status_id='4' and customers.id=service.customer_id))*100 as dispute_received_after_confirm,

		

		(select preference_job_area from service where customers.id=service.customer_id and service_status_id='6' order by id desc limit 0,1) as preference_job_area,

		(SELECT TIMESTAMPDIFF(MINUTE, service.request_date,NOW())FROM `service` where  customers.id=service.customer_id order by service.id desc limit 0,1)/60 as last_request,

		(SELECT TIMESTAMPDIFF(MINUTE, appointments.service_end_time,NOW())FROM `service` join appointments on(service.id=appointments.service_id) where service.service_status_id=6 and customers.id=service.customer_id order by service.id desc limit 0,1)/60 as last_job_completed,
		
		 (select sum(appointments.paid_amount) from appointments where customers.id=appointments.customer_id) as amount,
		 (select AVG(rate_value) from customer_rating where customers.id=customer_rating.customer_id) as rating")
		
		;
	

		
		if($request->input('phone')){
			$customers=$customers->where('customers.phone','like',$request->input('phone'));
		}
		if($request->input('id')){
			$customers=$customers->where('customers.id','like',$request->input('id'));
		}
		if($request->input('name')){
			$customers=$customers->where('customer_profile.name','like',"%".$request->input('name')."%");
		}
		if($request->input('email')){
			$customers=$customers->where('customers.email','like',"%".$request->input('email')."%");
		}
		
		if($request->input('is_active')!=''){
			$customers=$customers->where('customers.is_active','=',(int)$request->input('is_active'));
			
		}
		if($request->input('revenue_from_date')!='' && $request->input('revenue_to_date')!='' && $request->input('from_income')!=''  && $request->input('to_income')!=''){
			$customers=$customers->where('appointments.date','>=',$request->input('revenue_from_date'))->where('appointments.date','<=',$request->input('revenue_to_date'))->whereRaw("((select sum(appointments.paid_amount) from appointments where customers.id=appointments.customer_id) between ".$request->input('from_income')." and ".$request->input('to_income').")");
			
		}

		if($request->input('rating_from')!='' && $request->input('rating_to')!='' && $request->input('rating_from')>=0 && $request->input('rating_to')>0){
			$customers=$customers->whereRaw("(select AVG(rate_value) from customer_rating where customers.id=customer_rating.customer_id) between '".$request->input('rating_from')."' and '".$request->input('rating_to')."'" );
			
		}


		if($request->input('last_appo')!=''){
			$customers=$customers->where('service.preference_job_area','=',(int)$request->input('last_appo'));
			
		}
		
		$customers->groupby('customers.id') ;

		
		if($request->input('sort') && $request->input('order')){
			
			$customers=$customers->orderby($request->input('sort'),$request->input('order'))->paginate(100);
		}else{
			$customers=$customers->orderby('customers.id','desc')->paginate(100);
		}


		$perference=Locationpreferencehead::get();
				
        return view('admin.customer.index',  compact('customers','title','perference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Customer";

		
         return view('admin.customer.create_edit',  compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		

		
		
		 if (empty($request->phone)) {
			 $request->session()->flash('message', 'Invalid Phone Number!');
			$request->session()->flash('alert-class', 'alert-danger');
			return redirect()->back();
		   } else if(!preg_match("/^[0-9]*$/",$request->phone) && strlen($request->phone)< 10)
		   {
			   $request->session()->flash('message', 'Invalid Phone Number!');
				$request->session()->flash('alert-class', 'alert-danger');
				return redirect()->back();
		   }

		   $count = Customer::where('phone', '=', $request->phone)->count();

			if($count>0){
				$request->session()->flash('message', 'Phone Number already registered!');
				$request->session()->flash('alert-class', 'alert-danger');
				return redirect()->back();
			}

		   
        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
        }
		if($pass=='' || $password==''){
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			return redirect()->back();
		 }
		  if(strlen($password)<8){
				$request->session()->flash('message', 'Password must me atleast 8 chars long!');
				$request->session()->flash('alert-class', 'alert-danger');
				return redirect()->back();
		 }

		
   
        
        $customer = new Customer();
		
        $customer->email=$request->email;
        $customer->password=$pass;
        $customer->phone=$request->phone;
        $customer->banned_duration=($request->banned_duration)?$request->banned_duration:0;
        $customer->is_active=($request->is_active)?$request->is_active:1;
        $customer->term_accepted=($request->term_accepted)?$request->term_accepted:1;
        $customer->sms_verified=($request->sms_verified)?$request->sms_verified:1;
        $customer->save();

        $profile=new Customerprofile();
		$profile->customer_id=$customer->id;
        $profile->save();

       $request->session()->flash('message', 'Customer added successfully!');
	   return redirect('admin/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function statusupdate($id,$status,Request $request)
    {
		
       $customer=Customer::find($id);
       $customer->is_active=($status=='1')?0:1;
       $customer->term_accepted=($status=='1')?0:1;
       //$customer->sms_verified=($status=='1')?0:1;
       $customer->update();
       $request->session()->flash('message', 'Customer Updated Successfully!');
       return redirect()->back();
       
    }

    public function smsverify($id,$status,Request $request)
    {
		
       $customer=Customer::find($id);
       $customer->sms_verified=$status;
       $customer->update();
       
       $request->session()->flash('message', 'Customer Updated Successfully!');
       return redirect()->back();
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Customer";
        $customer=Customer::find($id);
		
        return view('admin.customer.create_edit',  compact('customer','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

			if (empty($request->phone)) {
				 $request->session()->flash('message', 'Invalid Phone Number!');
				$request->session()->flash('alert-class', 'alert-danger');
				return redirect()->back();
			   } else if(!preg_match("/^[0-9]*$/",$request->phone) && strlen($request->phone)< 10)
			   {
				   $request->session()->flash('message', 'Invalid Phone Number!');
					$request->session()->flash('alert-class', 'alert-danger');
					return redirect()->back();
			   }



		$count = Customer::where('phone', '=', $request->phone)->where('id', '<>', $id)->count();

			if($count>0){
				$request->session()->flash('message', 'Phone Number already registered!');
				$request->session()->flash('alert-class', 'alert-danger');
				return redirect()->back();
			}

			   
       $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
		else{
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
			}

			  if(strlen($password)<8){
				$request->session()->flash('message', 'Password must me atleast 8 chars long!');
				$request->session()->flash('alert-class', 'alert-danger');
				return redirect()->back();
		 }
        }
         $customer=Customer::find($id);
         
		if($pass!=''){
         $customer->password=$pass;

			}
       
        $customer->email=$request->email;
        //$customer->is_active=($request->is_active)?$request->is_active:0;
        //$customer->term_accepted=($request->term_accepted)?$request->term_accepted:0;
        //$customer->sms_verified=($request->sms_verified)?$request->sms_verified:0;
        $customer->phone=$request->phone;
        $customer->banned_duration=($request->banned_duration)?$request->banned_duration:0;
        $customer->update();
		$request->session()->flash('message', 'Customer Updated Successfully!');
         return redirect('admin/customers');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {



        $affectedRows  = Customer::where('id', '=', $id)->delete();

		return $affectedRows;
    }



    public function profile($id){

	$customer=Customer::with('Profile')->find($id);
	$title="Customer Profile";

    return view('admin.customer.profile',  compact('title','customer'));

	}

	public function jobHistory(Request $request){
		$id=$request->id;
		if($id=='') return redirect('admin/customers');
		$customer=Customer::leftJoin('service','customers.id','=','service.customer_id')
		->leftJoin('service_status','service.service_status_id','=','service_status.id')
		->leftJoin('appointments','appointments.service_id','=','service.id')
		->selectRaw("customers.phone,service_status.name,service.*,appointments.paid_amount as amount,service.request_date as appdate")->orderby('service.id','desc');

		if($request->input('from_date') && $request->input('to_date')){
			$customer=$customer->whereBetween('appointments.date',array(date("Y-m-d H:i:s",strtotime($request->input('from_date'))),date("Y-m-d H:i:s",strtotime($request->input('to_date')))))		;
			
		}
				
		if($request->status)
		$customer=$customer->where('service.service_status_id',$request->status);

		//print_r($customer->where('customers.id',$id)->get()->toArray());exit;

		$customer=$customer->where('customers.id',$id)->paginate(100);
		
		$title="Customers Job History";


		$servicestatus=Servicestatus::get();

		return view('admin.customer.jobhistory',  compact('title','customer','servicestatus','id'));
		

	}

	public function cancelHistory(Request $request){
		$id=$request->id;
		if($id=='') return redirect('admin/customers');
		$customer=Customer::leftJoin('service','customers.id','=','service.customer_id')
		
		->Join('customer_cancel_history','customer_cancel_history.service_id','=','service.id')
		->leftJoin('cancel_reason','cancel_reason.id','=','customer_cancel_history.reason_id')
		->selectRaw("customers.phone,service.*,customer_cancel_history.penalty_amount as amount,
		customer_cancel_history.date as cancel_date,cancel_reason.title,customer_cancel_history.comment")->where('service.service_status_id',2)
		->orderby('customer_cancel_history.id','desc')->groupby('customer_cancel_history.id');

		//print_r($customer->get()->toArray());exit;
		//if($request->status)
		//$customer=$customer->where('service.service_status_id',$request->status);

		$customer=$customer->where('customers.id',$id)->paginate(100);
		
		$title="Customers Cancel History";


		//$servicestatus=Servicestatus::get();

		return view('admin.customer.cancelhistory',  compact('title','customer','id'));
		

	}

	public function ratingHistory(Request $request){
		$id=$request->id;
		if($id=='') return redirect('admin/customers');
		$customer=Customer::leftJoin('service','customers.id','=','service.customer_id')
		->leftJoin('providers','providers.id','=','service.provider_id')
		->leftJoin('customer_rating','customer_rating.service_id','=','service.id')
		->selectRaw('providers.phone as provider_phone,customer_rating.*,service.job_id')
		->where('service.service_status_id',6)
		->orderby('customer_rating.id','desc')
		->groupby('service.id');

//print_r($customer->toArray());exit;
		//if($request->status)
		//$customer=$customer->where('service.service_status_id',$request->status);

		$customer=$customer->where('customers.id',$id)->paginate(100);
		
		$title="Customers Rating History";


		//$servicestatus=Servicestatus::get();

		return view('admin.customer.ratinglist',  compact('title','customer','id'));
		

	}

	public function sendMessage(Request $request){

		$title="Send Message";

		return view('admin.customer.sendmessage',  compact('title'));
	}

	public function sendMessagePost(Request $request){

		$customer=Customer::with("Profile")->where('phone',$request->phone)->first();
        if($customer)
        {
		$message=new CustomerMessage();
		$message->customer_id=$customer->id;
		$message->message=$request->message;
		$message->subject=$request->subject;
		$message->save();

		
       
		if($customer->device_type=='ios' && $customer->device_id!=''){
		
			$arr = array('type'=>'message',"customer_name"=>$customer->profile->name,"message"=>$request->message);
			$status= $this->sendCustomerAPNS($request->subject,$arr,$customer->device_id);
			
			}elseif($customer->device_id!='' ){
				$arr = array('type'=>'message',"customer_name"=>$customer->profile->name,"message"=>$request->message);
				$status=$this->sendCustomerFCM($request->subject,$arr,$customer->device_id);

			}
        
		$request->session()->flash('message', 'Message sent Successfully!');
		}else{

		$request->session()->flash('message', 'Customer not found!');
		}

	
         return redirect()->back();

	}

	public function ajaxCustomers(Request $request){
		$term='%'.$request->term.'%';
		$customers=Customer::leftJoin('customer_profile','customers.id','=','customer_profile.customer_id')->selectRaw('customers.id,phone as value, name')->where('name','like',$term)->orWhere('phone','like',$term)->get();
		
		return Response::json($customers);
	}

	public function show(){


	}

private	function sendCustomerFCM($title,$body,$id) {
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
				'to' => $id,
				/*'notification' => array (
						"body" => ($body),
						"title" => $title,
						"icon" => "myicon"
				),*/
				'data'=>$body
		);

		
		$fields = json_encode ( $fields );
		$headers = array (
				'Authorization: key=' . env('FCM_KEY',"AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg"),
				'Content-Type: application/json'
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		return $result;
		}

private function sendCustomerAPNS($title,$body,$id){

		// Put your device token here (without spaces):
		$deviceToken = $id;
		// Put your private key's passphrase here:
		$passphrase = env('APNS_KEY',"xxxxxxx");
		// Put your alert message here:
		$message = $body;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck_file.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'body' => $message,
				'action-loc-key' => $title,
			),
			'badge' => 2,
			'sound' => 'oven.caf',
			);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		if (!$result)
			return false;
		else
			return $result;
		// Close the connection to the server
		fclose($fp);

	}	

	
}
