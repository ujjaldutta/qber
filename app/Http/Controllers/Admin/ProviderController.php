<?php
/*************************************************** Provider  Panel for admin *******************************************************************/

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\ProviderCancelHistory;
use App\Models\Providerprofile;
use App\Models\ProviderMessage;
use App\Models\Country;
use App\Models\Servicetype;
use App\Models\Servicestatus;
use App\Models\Locationpreferencehead;
use App\Models\Providerfreetime;
use App\Models\Providersattlehistory;
use App\Models\CompanyProvider;
use App\Models\ProviderCompanyAccount;
use App\Models\Smslog;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class ProviderController extends Controller {

	public function __construct()
	{
		
	}

	 /**
     * ***********************  Display providers in admin *********************
     *
     * @return \Illuminate\Http\Response
     */

     /*************************************** List Provider for admin *******************************************/
     
    public function index(Request $request)
    {
		
        $title="List Providers";
       
		$providers=Provider::leftJoin('provider_balance_history','providers.id','=','provider_balance_history.provider_id')
		->leftJoin('provider_profile','providers.id','=','provider_profile.provider_id')
		->leftJoin('provider_cancel_history','providers.id','=','provider_cancel_history.provider_id')
		->leftJoin('company_providers','providers.id','=','company_providers.provider_id')
		->leftJoin('service_types','providers.type_id','=','service_types.id')
		->leftJoin('provider_free_timeslot','providers.id','=','provider_free_timeslot.provider_id')
		->selectRaw("providers.id,providers.phone,providers.email,providers.name,provider_profile.current_credit_balance,provider_profile.max_due_amount,provider_profile.nationality,providers.approve_status,provider_profile.date_of_birth,provider_status,sms_verified,work_time,moneyexpvalue,
		TIMESTAMPDIFF( YEAR, now(),provider_profile.date_of_birth ) as age,provider_profile.gender,provider_profile.transport,is_online,ready_to_serve,
		service_types.name as service_type,preferred_location,(priority/sum(priority))*location_factor as priority_factor,providers.location_preference,
		
		(select AVG(rate_value) from provider_rating where providers.id=provider_rating.provider_id) as rating,
		(select service_status_id from service where providers.id=service.provider_id and service_status_id=5 limit 1) as job_status,
		(select (SUM(service.provider_call)/count(*)) from service where providers.id=service.provider_id and (service_status_id=2 OR service_status_id=8 OR 	service_status_id=10 OR service_status_id=11 ) )*100 as cancel_ratio,

		(select count(*) from provider_company_account where providers.id=provider_company_account.provider_id and sattled='no' and arrival_time>CURDATE()) as company_notice, 


		((SELECT count(*) from service where providers.id=service.provider_id and service_status_id='11')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_job_start_count,

		((SELECT count(*) from service where providers.id=service.provider_id and service_status_id='10')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_heading,
		

((select count(*) from service where providers.id=service.provider_id and service_status_id='8')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_confirm,

	

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='customer' and providers.id=service.provider_id )/(select count(*) from service left join dispute on(service.id=dispute.service_id) where providers.id=service.provider_id and service.service_status_id='4'  and providers.id=service.provider_id and dispute.dispute_to='customer')) as dispute_given_after_confirm,

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='provider' )/(select count(*) from service  left join dispute on(service.id=dispute.service_id) where providers.id=service.provider_id and service.service_status_id='4' and dispute.dispute_to='provider')) as dispute_received_after_confirm,
		
		(select sum(provider_cancel_history.penalty_amount) from provider_cancel_history where providers.id=provider_cancel_history.provider_id) as penalty_amount,
		
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where MONTH(date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) and providers.id=provider_balance_history.provider_id) as last_month_amount,
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where  provider_balance_history.date >= DATE(NOW()) - INTERVAL 7 DAY and providers.id=provider_balance_history.provider_id) as last_week_amount,
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) as total_amount,

		(select company.name from company inner join company_providers where company.id=company_providers.company_id and company_providers.provider_id=providers.id ) as companyname,

		(select count(*) from appointments  where providers.id=appointments.provider_id) as servicecount, (select count(*) from provider_cancel_history
		 where providers.id=provider_cancel_history.provider_id) as cancelcount")
		->groupby('providers.id');

		if($request->input('phone'))
		{
			$providers=$providers->where('providers.phone','like',$request->input('phone')."%");
		}
		if($request->input('id'))
		{
			$providers=$providers->where('providers.id','=',$request->input('id'));
		}

		
		if($request->input('name'))
		{
			$providers=$providers->where('providers.name','like',"%".$request->input('name')."%");
		}
		if($request->input('email'))
		{
			$providers=$providers->where('providers.email','like',"%".$request->input('email')."%");
		}
		
		
		if($request->input('servicetype'))
		{
			$providers=$providers->where('providers.type_id',$request->input('servicetype'));
		}
		if($request->input('nationality')){
			$providers=$providers->where('provider_profile.nationality',$request->input('nationality'));
		}

		
		if($request->input('total_come')!='')
		{
			$total_income=explode("-",$request->input('total_come'));
			
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id)>='.$total_income[0]);
			if($total_income[1]!='unlimited')
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) <='.$total_income[1]);
		}

		/*if($request->input('last_month_come')!=''){
			$total_income=explode("-",$request->input('last_month_come'));
			
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id)>='.$total_income[0]." and MONTH(provider_balance_history.date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ");
			if($total_income[1]!='unlimited')
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) <='.$total_income[1]." and MONTH(provider_balance_history.date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ");
		}*/

		if($request->input('revenue_from_date')!=''  && $request->input('revenue_to_date')!='' && $request->input('from_income')!=''  && $request->input('to_income')!='')
		{
			$providers=$providers
			//->where('provider_balance_history.date','>=',$request->input('revenue_from_date'))->where('provider_balance_history.date','<=',$request->input('revenue_to_date'))

			->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id and
			provider_balance_history.date>="'.$request->input('revenue_from_date').'" and provider_balance_history.date<="'.$request->input('revenue_to_date').'" ) between '.$request->input('from_income')." and ".$request->input('to_income')." ");
		}

		if($request->input('transport_mode'))
		{
			$providers=$providers->where('provider_profile.transport',$request->input('transport_mode'));
		}
		if($request->input('employment_type'))
		{
			if($request->input('employment_type')=='company')
			$providers=$providers->whereNotNull('company_providers.provider_id');
			else
			$providers=$providers->whereNull('company_providers.company_id');
		}

		if($request->input('current_area'))
		{
			$providers=$providers->where('preferred_location',$request->input('current_area'));
		}
		
		if($request->input('free_from_date')!='' && $request->input('free_to_date')!='')
		{
			
						
			$providers=$providers->where('provider_free_timeslot.from_time','>=',date("H:i:s",strtotime($request->input('free_from_date'))))
			->where('provider_free_timeslot.to_time','<=',date("H:i:s",strtotime($request->input('free_to_date'))))->where('time_type','free');
		}

		if($request->input('current_request'))
		{
			if($request->input('current_request')=='yes')
			$condition='>0';
			else
			$condition='=0';
			$providers=$providers->whereRaw("(select count(*) from provider_notification where providers.id=provider_notification.provider_id and invite_status='sent')".$condition);
		}

		if($request->input('account_limit'))
		{
			if($request->input('account_limit')=='yes')
			{
			$providers=$providers->whereRaw("provider_profile.current_credit_balance > provider_profile.max_due_amount and provider_profile.current_credit_balance>0");
			}else{
			$providers=$providers->whereRaw("provider_profile.current_credit_balance < provider_profile.max_due_amount and provider_profile.current_credit_balance>0");
			}
		}

		if($request->input('gender'))
		{
			$providers=$providers->where('provider_profile.gender',$request->input('gender'));
		}
		if($request->input('provider_status'))
		{
			$providers=$providers->where('providers.provider_status',$request->input('provider_status'));
		}

		if($request->input('account_status')=='active')
		{
			$providers=$providers->where('providers.is_active',1);
		}elseif($request->input('account_status')=='inactive')
		{
			$providers=$providers->where('providers.is_active',0);
		}elseif($request->input('account_status')=='approved')
		{
			$providers=$providers->where('providers.approve_status',1);
		}elseif($request->input('account_status')=='notapproved')
		{
			$providers=$providers->where('providers.approve_status',0);
		}


		
		if($request->input('age'))
		{
			$age=explode("-",$request->input('age'));
			$start = date('Y', strtotime('-'.$age[0].' years'));
			$end = date('Y', strtotime('-'.$age[1].' years'));
			
			
			$providers=$providers->where('provider_profile.date_of_birth','<=',$start)->where('provider_profile.date_of_birth','>=',$end);
		}
		
		
		if($request->input('sort') && $request->input('order'))
		{
			
			$providers=$providers->orderby($request->input('sort'),$request->input('order'))->paginate(100);
		}else
		{
			$providers=$providers->orderby('id','desc')->paginate(100);
		}

		$servicetype=Servicetype::get();
		$perference=Locationpreferencehead::get();
/*select providers.*,sum(provider_balance_history.receive_amount) as amount,(select count(*) from appointments where providers.id=appointments.provider_id) as count from `providers` left join `provider_balance_history` on `providers`.`id` = `provider_balance_history`.`provider_id` group by providers.id order by providers.`id` desc */
		
		//$providers->appends(['sort' => 'votes'])->render();

		$nationality=Country::select('code', 'countryname as name')->get();
		$total_priority=Provider::where("is_online",1)->where("ready_to_serve",1)->avg('moneyexpvalue');

		//print_r($total_priority);exit;


		
        return view('admin.provider.index',  compact('providers','title','servicetype','nationality','perference','total_priority'));
    }

    /**
     * ***********************  Create providers in admin *********************
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Provider";

		$service=Servicetype::pluck('name', 'id');
         return view('admin.provider.create_edit',  compact('title','service'));
    }

    /**
     * Store a newly created provider in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password))
        {
            if ($password === $passwordConfirmation)
            {
                $pass = bcrypt($password);
            }
        }
		if($pass=='' || $password=='')
		{
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
		 }
        
        $provider = new Provider();
		$provider->type_id=$request->type_id;
        $provider->email=$request->email;
        $provider->password=$pass;
        $provider->phone=$request->phone;
        $provider->name=$request->name;
        $provider->is_active=($request->is_active)?$request->is_active:0;
        $provider->approve_status=($request->approve_status)?$request->approve_status:0;
        $provider->sms_verified=($request->sms_verified)?$request->sms_verified:0;
        $provider->save();

        $profile=new Providerprofile();
		$profile->provider_id=$provider->id;

		$amount=Servicesettings::select('code','value')->where('code','provider_max_due_amount')
			->where('type_id',$request->input("type_id"))->first();
		$profile->max_due_amount=$amount->value;
			
        $profile->save();

       $request->session()->flash('message', 'Provider added successfully!');
	   return redirect()->back();
    }

    /**
     * Admin can activate or deactivate provider using this function
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function statusupdate($id,$status,Request $request)
    {
		
       $provider=Provider::find($id);
       $provider->approve_status=$status;
       $provider->update();
       if($status=='1')
       {

		  
				$arr = array('type'=>'activated',"provider_name"=>$provider->name,"message"=>"Congratulations, your Qber account has been activated, you may now receive jobs");
				
				$status=$this->sendsms($provider->phone,'Congratulations '.$provider->name.', your account has been approved! you may now log in and start using Qber Provider.');
				
				
				

				if($provider->device_type=='ios' && $provider->device_id!='')
				{
		
					$arr = array('type'=>'activated',"provider_name"=>$provider->name,"message"=>"Congratulations, your Qber account has been activated, you may now receive jobs");
					$status= $this->sendProviderAPNS("Account Activated",$arr,$provider->device_id);
					
				}
				elseif($provider->device_id!='' )
				{
					$arr = array('type'=>'activated',"provider_name"=>$provider->name,"message"=>"Congratulations, your Qber account has been activated, you may now receive jobs");
					$status=$this->sendProviderFCM("Account Activated",$arr,$provider->device_id);

				}

	   }
       $request->session()->flash('message', 'Provider Updated Successfully!');
       return redirect()->back();
       
    }

	/**************    Admin can set sms verification of provider *********/
	
	public function smsverify($id,$status,Request $request)
    {
		
       $provider=Provider::find($id);
       $provider->sms_verified=$status;
       $provider->update();
       
       $request->session()->flash('message', 'Provider Updated Successfully!');
       return redirect()->back();
       
    }
    /**
     * Show edit form of provider
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Provider";
        $provider=Provider::with('Profile')->find($id);
		$service=Servicetype::pluck('name', 'id');
        return view('admin.provider.create_edit',  compact('provider','title','service'));
    }

    /**
     * Update the provider resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password))
        {
            if ($password === $passwordConfirmation)
            {
                $pass = bcrypt($password);
            }
		else{
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
			}
        }
         $provider=Provider::find($id);
         
		if($pass)
        $provider->password=$pass;
        $provider->type_id=$request->type_id;
        $provider->email=$request->email;
        $provider->is_active=($request->is_active)?$request->is_active:0;
        $provider->approve_status=($request->approve_status)?$request->approve_status:0;
        $provider->sms_verified=($request->sms_verified)?$request->sms_verified:0;
        $provider->phone=$request->phone;
        $provider->name=$request->name;



        
        $provider->update();

        $profile=Providerprofile::where("provider_id",$provider->id)->first();
		$profile->max_due_amount=$request->max_due_amount;
			
        $profile->update();

        
		$request->session()->flash('message', 'Provider Updated Successfully!');
         return redirect()->back();
         
    }

    /**
     * Remove the provider if no transaction made in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows  = Provider::where('id', '=', $id)->delete();

		return $affectedRows;
    }


    /******** Display provider profile ***********************/

    public function profile($id)
    {
		
		$preference=array();
		$provider=Provider::with('Profile','Company')->find($id);
		//print_r($provider->toArray());exit;
		$title="Provider Profile";
		if(isset($provider->profile->work_preference))
		{
		$preference=Locationpreferencehead::where("id",$provider->profile->work_preference)->first();
		}

		return view('admin.provider.profile',  compact('title','provider','preference'));

	}

	/*************************************** Job history of  Provider for admin *******************************************/

	public function jobhistory(Request $request)
	{

		$id=$request->id;
		if($id=='') return redirect('admin/providers');
		$provider=Provider::leftJoin('service','providers.id','=','service.provider_id')
		->leftJoin('service_status','service.service_status_id','=','service_status.id')
		->leftJoin('appointments','appointments.service_id','=','service.id')
		->selectRaw("providers.phone,service_status.name,service.*,appointments.paid_amount as amount,appointments.date as appdate");

		if($request->input('from_date') && $request->input('to_date'))
		{
			
			$provider=$provider->whereBetween('appointments.date',array(date("Y-m-d H:i:s",strtotime($request->input('from_date'))),date("Y-m-d H:i:s",strtotime($request->input('to_date')))))		;
		}

		if($request->input('servicestatus'))
		{
			$provider=$provider->where('service.service_status_id',$request->input('servicestatus'));
		}

		$provider=$provider->orderby('service.id','desc')->where('providers.id',$id)->paginate(100);
		$title="Provider Job History";
		$servicestatus=Servicestatus::get();
		return view('admin.provider.jobhistory',  compact('title','provider','servicestatus','id'));
		

	}

	/*************************************** Job Cancel history of  Provider for admin *******************************************/

	public function cancelHistory(Request $request)
	{
		$id=$request->id;
		if($id=='') return redirect('admin/providers');
		$provider=ProviderCancelHistory::leftJoin('service','provider_cancel_history.service_id','=','service.id')
		->leftJoin('providers','providers.id','=','provider_cancel_history.provider_id')
		
		->leftJoin('cancel_reason','cancel_reason.id','=','provider_cancel_history.reason_id')
		->selectRaw("providers.phone,service.*,provider_cancel_history.penalty_amount as amount,
		provider_cancel_history.date as cancel_date,cancel_reason.title,provider_cancel_history.comment,service.service_status_id")
		->where('service.service_status_id',2)
		->orderby('provider_cancel_history.id','desc')->groupby('provider_cancel_history.id');

		//print_r($provider->get()->toArray());exit;
		//if($request->status)
		//$customer=$customer->where('service.service_status_id',$request->status);

		$provider=$provider->where('providers.id',$id)->paginate(100);
		
		$title="Providers Cancel History";


		//$servicestatus=Servicestatus::get();

		return view('admin.provider.cancelhistory',  compact('title','provider','id'));
		

	}

	/*************************************** Rating list of  Provider for admin *******************************************/

		public function ratingHistory(Request $request)
		{
		$id=$request->id;
		if($id=='') return redirect('admin/customers');
		$provider=Provider::leftJoin('service','providers.id','=','service.provider_id')
		->leftJoin('customers','customers.id','=','service.customer_id')
		->leftJoin('provider_rating','provider_rating.service_id','=','service.id')
		->selectRaw('customers.phone as customer_phone,provider_rating.*,service.job_id')
		->where('service.service_status_id',6)
		->orderby('provider_rating.id','desc')
		->groupby('service.id');

//print_r($customer->toArray());exit;
		//if($request->status)
		//$customer=$customer->where('service.service_status_id',$request->status);

		$provider=$provider->where('providers.id',$id)->paginate(100);
		
		$title="Providers Rating History";


		//$servicestatus=Servicestatus::get();

		return view('admin.provider.ratinglist',  compact('title','provider','id'));
		

	}

	/*************************************** List Balance sattle screen for provider *******************************************/

	public function sattleBalance(Request $request)
	{

			$title="Sattle Balance";
       
		$providers=Provider::leftJoin('appointments','providers.id','=','appointments.provider_id')
		->leftJoin('provider_profile','providers.id','=','provider_profile.provider_id')
		->leftJoin('provider_company_account','providers.id','=','provider_company_account.provider_id')
		->selectRaw("providers.*,sum(appointments.provider_amount) as amount,(select count(*) from appointments
		 where providers.id=appointments.provider_id) as servicecount,provider_profile.current_credit_balance as credit_amount,provider_company_account.current_credit_balance as company_credit")
		->groupby('providers.id')->where('provider_profile.current_credit_balance','>',0)->orWhere('provider_company_account.current_credit_balance','>',0);
		
		
		if($request->input('servicetype'))
		{
			$providers=$providers->where('providers.type_id',$request->input('servicetype'));
		}
		if($request->input('nationality'))
		{
			$providers=$providers->where('provider_profile.nationality',$request->input('nationality'));
		}
		
		if($request->input('sort') && $request->input('order'))
		{
			
			$providers=$providers->orderby($request->input('sort'),$request->input('order'))->paginate(100);
		}else
		{
			$providers=$providers->orderby('id','desc')->paginate(100);
		}

		$servicetype=Servicetype::get();
		


		$nationality=Country::select('code', 'countryname as name')->get();



		
        return view('admin.provider.sattlebalance',  compact('providers','title','servicetype','nationality'));

	}

	/*************************************** Sattle the balnce by admin  *******************************************/

	public function Sattle(Request $request)
	{
		$title="Update Balance";
		$provider=Provider::with('Profile')
		->selectRaw("providers.*,provider_company_account.current_credit_balance as company_credit")
		->leftJoin('provider_company_account','providers.id','=','provider_company_account.provider_id')
		->where('providers.id',$request->id)->first();
		//print_r($provider->toArray());exit;
		return view('admin.provider.sattle',  compact('title','provider'));
	}

	/*************************************** Sattle the balnce and send notificaiton to provider by admin  *******************************************/

	public function SattleUpdate(Request $request)
	{

		//$provider=Providerprofile::where('provider_id',$request->provider_id)->first();



		$company=CompanyProvider::where("provider_id",$request->provider_id)->count();
				if($company>0)
				{
					$account=ProviderCompanyAccount::where("provider_id",$request->provider_id)->first();
					$account->current_credit_balance=$account->current_credit_balance-$request->current_credit_balance;
					$account->sattled='no';
					$account->save();
				}else
				{
					$profile=Providerprofile::where("provider_id",$request->provider_id)->first();
					$profile->current_credit_balance=$profile->current_credit_balance-$request->current_credit_balance;
					$profile->save();

					$account=ProviderCompanyAccount::where("provider_id",$request->provider_id)->first();
					$account->sattled='no';
					$account->save();
				}
         
		
        //$provider->current_credit_balance=$provider->current_credit_balance-$request->current_credit_balance;
       
        //$provider->update();

        $history=new Providersattlehistory;
        $history->admin_id=Auth::user()->id;
		$history->provider_id=$request->provider_id;
		$history->amount=$request->current_credit_balance;
		$history->date=date("Y-m-d H:i:s");
		$history->save();
		
		$provider=Provider::where('id',$request->provider_id)->first();

		$msg=$provider->name." You have successfully settled ".$request->current_credit_balance." QAR from you account";

		if($provider->device_type=='ios' && $provider->device_id!='')
		{
					
				$arr = array('type'=>'balancesattled',"provider_name"=>$provider->name,"amount"=>$request->current_credit_balance);
				$status= $this->sendProviderAPNS($msg,$arr,$provider->device_id);
			
			}elseif($provider->device_id!='' )
			{

				$arr = array('type'=>'balancesattled',"provider_name"=>$provider->name,"amount"=>$request->current_credit_balance);
				$status= $this->sendProviderFCM($msg,$arr,$provider->device_id);
			}



        
		$request->session()->flash('message', 'Balance Updated Successfully!');
         return redirect()->back();


	}


	public function SattleHistory(Request $request)
	{
		$title="Sattle History";
		$provider=Providersattlehistory::with('Provider','Admin','Company')
		->where('provider_id',$request->id)->paginate(100);
		//print_r($provider->toArray());exit;
		
		return view('admin.provider.sattlehistory',  compact('title','provider'));
	}


	/*************************************** Send message to Provider for admin *******************************************/

	
	public function sendMessage(Request $request)
	{

		$title="Send Message";

		return view('admin.provider.sendmessage',  compact('title'));
	}

	/*************************************** Send the message to provider and notificaiton *******************************************/

	public function sendMessagePost(Request $request)
	{

		$provider=Provider::where('phone',$request->phone)->first();
        if($provider)
        {
		$message=new ProviderMessage();
		$message->provider_id=$provider->id;
		$message->subject=$request->subject;
		$message->message=$request->message;
		$message->save();

		/*PushNotification::app('appNameIOS')
                ->to($deviceToken)
                ->send('Hello World, i`m a push message');*/
        if($provider->device_id)
        {
       
			if($provider->device_type=='ios' && $provider->device_id!='')
			{
		
			$arr = array('type'=>'message',"provider_name"=>$provider->name,"message"=>$request->message);
			$status= $this->sendProviderAPNS("A new message has been received",$arr,$provider->device_id);
			
			}elseif($provider->device_id!='' )
			{
				$arr = array('type'=>'message',"provider_name"=>$provider->name,"message"=>$request->message);
				$status=$this->sendProviderFCM("A new message has been received",$arr,$provider->device_id);

			}
			
        
		$request->session()->flash('message', 'Message sent Successfully!');
		}
		else
		{

		$request->session()->flash('message', 'Provider not found!');
		}
		}
	
         return redirect()->back();

	}

	/*************************************** Get active providers for map in ajax *******************************************/

	public function ajaxProviders(Request $request)
	{
		$term='%'.$request->term.'%';
		$providers=Provider::selectRaw('id,phone as value, name')->where('name','like',$term)->orWhere('phone','like',$term)->get();
		//print_r($providers->toArray());exit;
		return Response::json($providers);
	}

	

	
	

	public function show()
	{


	}

	/*************************************** Get Active providers on map  *******************************************/
	
	public function activeProviders()
	{

		$title="Provider Map";
		
		$servicetype=Servicetype::get();
		$perference=Locationpreferencehead::get();
		$nationality=Country::select('code', 'countryname as name')->get();
	

		return view('admin.provider.activegmap',  compact('title','servicetype','nationality','perference'));
	}


	/*************************************** Display provider map *******************************************/

	public function getActiveProviders(Request $request)
	{




		$providers=Provider::leftJoin('provider_balance_history','providers.id','=','provider_balance_history.provider_id')
		->leftJoin('provider_profile','providers.id','=','provider_profile.provider_id')
		->leftJoin('provider_cancel_history','providers.id','=','provider_cancel_history.provider_id')
		->leftJoin('company_providers','providers.id','=','company_providers.provider_id')
		->leftJoin('service_types','providers.type_id','=','service_types.id')
		->leftJoin('provider_free_timeslot','providers.id','=','provider_free_timeslot.provider_id')
		->selectRaw("providers.id,providers.phone,providers.email,providers.name,provider_profile.current_credit_balance,provider_profile.max_due_amount,provider_profile.nationality,providers.approve_status,provider_profile.date_of_birth,provider_status,sms_verified,providers.current_latitude,providers.current_longitude,
		TIMESTAMPDIFF( YEAR, now(),provider_profile.date_of_birth ) as age,provider_profile.gender,provider_profile.transport,
		service_types.name as service_type,preferred_location,(priority/sum(priority))*location_factor as priority_factor,providers.location_preference,
		
		(select AVG(rate_value) from provider_rating where providers.id=provider_rating.provider_id) as rating,
		(select service_status_id from service where providers.id=service.provider_id and service_status_id=5 limit 1) as job_status,
		(select (SUM(service.provider_call)/count(*)) from service where providers.id=service.provider_id and (service_status_id=2 OR service_status_id=8 OR 	service_status_id=10 OR service_status_id=11 ) )*100 as cancel_ratio,

		(select count(*) from provider_notification where providers.id=provider_notification.provider_id and invite_status='sent') as company_notice, 


		((SELECT count(*) from service where providers.id=service.provider_id and service_status_id='11')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_job_start_count,

		((SELECT count(*) from service where providers.id=service.provider_id and service_status_id='10')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_heading,
		

((select count(*) from service where providers.id=service.provider_id and service_status_id='8')/(select count(*) from service where providers.id=service.provider_id and (service.service_status_id='11' OR service.service_status_id='10' OR service.service_status_id='8' OR service.service_status_id='4' OR service.service_status_id='6')))*100 as cancel_after_confirm,

	

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='customer' and providers.id=service.provider_id )/(select count(*) from service left join dispute on(service.id=dispute.service_id) where providers.id=service.provider_id and service.service_status_id='4'  and providers.id=service.provider_id and dispute.dispute_to='customer')) as dispute_given_after_confirm,

		((SELECT count(*)  FROM `service` inner join dispute on (service.id=dispute.service_id) where dispute.dispute_to='provider' )/(select count(*) from service  left join dispute on(service.id=dispute.service_id) where providers.id=service.provider_id and service.service_status_id='4' and dispute.dispute_to='provider')) as dispute_received_after_confirm,
		
		(select sum(provider_cancel_history.penalty_amount) from provider_cancel_history where providers.id=provider_cancel_history.provider_id) as penalty_amount,
		
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where MONTH(date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) and providers.id=provider_balance_history.provider_id) as last_month_amount,
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where  provider_balance_history.date >= DATE(NOW()) - INTERVAL 7 DAY and providers.id=provider_balance_history.provider_id) as last_week_amount,
		(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) as total_amount,

		(select company.name from company inner join company_providers where company.id=company_providers.company_id and company_providers.provider_id=providers.id ) as companyname,

		(select count(*) from appointments  where providers.id=appointments.provider_id) as servicecount, (select count(*) from provider_cancel_history
		 where providers.id=provider_cancel_history.provider_id) as cancelcount")
		->groupby('providers.id')->where('providers.approve_status','1')->where('providers.is_online','1')->where('providers.is_active','1');

		if($request->input('phone'))
		{
			$providers=$providers->where('providers.phone','like',$request->input('phone')."%");
		}
		if($request->input('id'))
		{
			$providers=$providers->where('providers.id','=',$request->input('id'));
		}

		
		if($request->input('name'))
		{
			$providers=$providers->where('providers.name','like',"%".$request->input('name')."%");
		}
		if($request->input('email'))
		{
			$providers=$providers->where('providers.email','like',"%".$request->input('email')."%");
		}
		
		
		if($request->input('servicetype')){
			$providers=$providers->where('providers.type_id',$request->input('servicetype'));
		}
		if($request->input('nationality')){
			$providers=$providers->where('provider_profile.nationality',$request->input('nationality'));
		}

		
		if($request->input('total_come')!='')
		{
			$total_income=explode("-",$request->input('total_come'));
			
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id)>='.$total_income[0]);
			if($total_income[1]!='unlimited')
			$providers=$providers->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id) <='.$total_income[1]);
		}

		

		if($request->input('revenue_from_date')!=''  && $request->input('revenue_to_date')!='' && $request->input('from_income')!=''  && $request->input('to_income')!='')
		{
			$providers=$providers
			->whereRaw('(select sum(provider_balance_history.receive_amount) from provider_balance_history where  providers.id=provider_balance_history.provider_id and provider_balance_history.date>="'.$request->input('revenue_from_date').'" and provider_balance_history.date<="'.$request->input('revenue_to_date').'" ) between '.$request->input('from_income')." and ".$request->input('to_income')." ");
		}

		if($request->input('transport_mode'))
		{
			$providers=$providers->where('provider_profile.transport',$request->input('transport_mode'));
		}
		if($request->input('employment_type'))
		{
			if($request->input('employment_type')=='company')
			$providers=$providers->whereNotNull('company_providers.provider_id');
			else
			$providers=$providers->whereNull('company_providers.company_id');
		}

		if($request->input('current_area'))
		{
			$providers=$providers->where('preferred_location',$request->input('current_area'));
		}
		
		if($request->input('free_from_date')!='' && $request->input('free_to_date')!='')
		{
			
						
			$providers=$providers->where('provider_free_timeslot.from_time','>=',date("H:i:s",strtotime($request->input('free_from_date'))))
			->where('provider_free_timeslot.to_time','<=',date("H:i:s",strtotime($request->input('free_to_date'))))->where('time_type','free');
		}

		if($request->input('current_request'))
		{
			if($request->input('current_request')=='yes')
			$condition='>0';
			else
			$condition='=0';
			$providers=$providers->whereRaw("(select count(*) from provider_notification where providers.id=provider_notification.provider_id and invite_status='sent')".$condition);
		}

		if($request->input('account_limit'))
		{
			if($request->input('account_limit')=='yes')
			{
			$providers=$providers->whereRaw("provider_profile.current_credit_balance > provider_profile.max_due_amount and provider_profile.current_credit_balance>0");
			}else{
			$providers=$providers->whereRaw("provider_profile.current_credit_balance < provider_profile.max_due_amount and provider_profile.current_credit_balance>0");
			}
		}

		if($request->input('gender'))
		{
			$providers=$providers->where('provider_profile.gender',$request->input('gender'));
		}
		if($request->input('provider_status'))
		{
			$providers=$providers->where('providers.provider_status',$request->input('provider_status'));
		}

		if($request->input('account_status')=='active')
		{
			$providers=$providers->where('providers.is_active',1);
		}elseif($request->input('account_status')=='inactive')
		{
			$providers=$providers->where('providers.is_active',0);
		}elseif($request->input('account_status')=='approved')
		{
			$providers=$providers->where('providers.approve_status',1);
		}elseif($request->input('account_status')=='notapproved')
		{
			$providers=$providers->where('providers.approve_status',0);
		}


		
		if($request->input('age'))
		{
			$age=explode("-",$request->input('age'));
			$start = date('Y', strtotime('-'.$age[0].' years'));
			$end = date('Y', strtotime('-'.$age[1].' years'));
			
			
			$providers=$providers->where('provider_profile.date_of_birth','<=',$start)->where('provider_profile.date_of_birth','>=',$end);
		}

		$status = $request->provider_status;
		if($status)
		$providers=$providers->where("provider_status",$status);

		$providers=$providers->get();
	

		

		
		return Response::json($providers);
	}

	/*************************************** Get Free slots of providers in admin  *******************************************/

	
	public function freeslotProviders(Request $request)
	{

			$providers=Provider::leftJoin('provider_free_timeslot','providers.id','=','provider_free_timeslot.provider_id')->where('providers.id',$request->id)->orderby('provider_free_timeslot.date','asc')->orderby('provider_free_timeslot.from_time','asc')->paginate(10);

			$title="Available Slots of Provider";
			
			return view('admin.provider.freeslot',  compact('title','providers'));
	}

	/*************************************** Download document files of providers in admin  *******************************************/

	public function getDownload(Request $request)
	{
        //PDF file is stored under project/public/download/info.pdf
        if($_SERVER['SERVER_NAME']=='uiplonline.com' || $_SERVER['SERVER_NAME']=='demo.uiplonline.com')
        	$file="./public/".$request->input('file');
        else
			$file="./".$request->input('file');
			
			return Response::download($file);
	}

	private	function sendProviderFCM($title,$body,$id)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
				'to' => $id,
				/*'notification' => array (
						"body" => ($body),
						"title" => $title,
						"icon" => "myicon"
				),*/
				'data'=>$body
		);

		
		$fields = json_encode ( $fields );
		$headers = array (
				'Authorization: key=' . env('FCM_KEY',"AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg"),
				'Content-Type: application/json'
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		return $result;
		}

	private function sendProviderAPNS($title,$body,$id)
	{

		// Put your device token here (without spaces):
		$deviceToken = $id;
		// Put your private key's passphrase here:
		$passphrase = env('APNS_KEY',"xxxxxxx");
		// Put your alert message here:
		$message = $body;
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck_file.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'body' => $message,
				'action-loc-key' => $title,
			),
			'badge' => 2,
			'sound' => 'oven.caf',
			);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		if (!$result)
			return false;
		else
			return $result;
		// Close the connection to the server
		fclose($fp);

	}

	
	private function sendsms($phone,$msg)
	{

				$authheader=base64_encode('primarymobile:Pass9738');
				$res=array();

				$header[] = 'Content-type: application/json';
				$header[] = 'Authorization: Basic '.$authheader;
			
				$data = array('from'=>'Qber',"bulkId"=>"QBER-".date("Y-m-d"),'to'=>$phone,'text'=>$msg);
				$data_json = json_encode($data);
				$url='https://api.infobip.com/sms/1/text/single';
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


				
				$res  = curl_exec($ch);
				curl_close($ch);
				
				
				
	}


	
	public function SMSLog(Request $request)
	{
		$title="SMS History";
		$sms=Smslog::paginate(100);
		//print_r($provider->toArray());exit;
		
		return view('admin.provider.smshistory',  compact('title','sms'));
	}	



}
