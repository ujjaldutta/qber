<?php

/*************************************************** Role management for subadmin  *************************************************/
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Roleaccess;
use App\Http\Requests;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $title="List Roles";
        $roles=Role::with("Roleaccess")->orderby('id','desc')->paginate(100);
		//print_r($role->toArray());exit;
		
		
        return view('admin.role.index',  compact('roles','title'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $role = new Role();
        $role->name=$request->name;
        $role->save();
		
		$access_data=array(array("role_id"=>$role->id,"component_code"=>"dashboardindex"),array("role_id"=>$role->id,"component_code"=>"profileedit"),array("role_id"=>$role->id,"component_code"=>"profileupdate"));
		//print_r($access_data);exit;
		$access = new Roleaccess();
        
        $access->insert($access_data);

        
      
	   return 1;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $access = new Roleaccess();
        $access->role_id=$request->role_id;
       
        $access->component_code=$request->component_code;
        $access->save();
		return 1;
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing admin role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Role Component";
        $role=Role::with("Roleaccess")->find($id);
		if($id==1) return redirect()->back();

		$code=array("dashboardindex"=>"dashboard","profileedit"=>"Admin Profile Edit",	   "profileupdate"=>"Admin Profile Update",
	"customerindex"=>"Customer List","customersendMessage"=>"Send Message - Customer","customercreate"=>"Create- Customer","customerstore"=>"Save New- Customer",
	"customerstatusupdate"=>"Status Update - Customer","customeredit"=>"Edit - Customer","customerupdate"=>"Update Edit- Customer",
	"customerdestroy"=>"Delete - Customer","customerprofile"=>"Profile - Customer","customerjobHistory"=>"Job History - Customer",
	"customercancelHistory"=>"Cancel History - Customer","customerratingHistory"=>"Rating History - Customer",
	"customersendMessagePost"=>"Send Message Post - Customer","customerajaxCustomers"=>"Customer Message List - Send Message ",


	"providerindex"=>"Provider List","providerindex"=>"Provider List","provideractiveProviders"=>"Provider On Map","providersattleBalance"=>"Sattle Balance",
	"providercreate"=>"Provider Create","providerstore"=>"Provider Save New","provideredit"=>"Provider Edit","providerupdate"=>"Provider Update Edit",
	"providerstatusupdate"=>"Provider Status Update","providerdestroy"=>"Provider Delete","providerprofile"=>"Provider Profile",
	"providerjobhistory"=>"Provider Job History","providercancelHistory"=>"Provider Cancel History","providerratingHistory"=>"Provider Rating History",
	"providerSattle"=>"Provider Update Sattle","providersendMessage"=>"Send Message - Provider","providersendMessagePost"=>"Provider Send Message Post",
	"providerajaxProviders"=>"Provider List - send message","providerSattleUpdate"=>"Provider - Update Sattle","provideractiveProviders"=>"Active Providers",
	"providerfreeslotProviders"=>"Free Slots - Providers","providergetDownload"=>"Download Files - Providers",

	"serviceindex"=>"List Appointments","serviceedit"=>"Edit Appointments","serviceupdate"=>"Update Appointments","servicedestroy"=>"Delete Appointments",

	
	
"disputeindex"=>"Dispute","disputestatusupdate"=>"Dispute Status","disputeedit"=>"Dispute Edit","disputeupdate"=>"Update Dispute","disputedestroy"=>"Delete Dispute",
	"servicetypeindex"=>"Service Type","servicetypecreate"=>"Service Type Create","servicetypestore"=>"Service Type Store","servicetypestatusupdate"=>"Service Type Status","servicetypeedit"=>"Service Type Edit","servicetypeupdate"=>"Service Type Update","servicetypedestroy"=>"Service Type Delete",
	
	"serviceoptionindex"=>"Service Options","serviceoptionedit"=>"Service Options Edit","serviceoptionupdate"=>"Service Options Update",
	
	"servicesettingsindex"=>"Service Settings","servicesettingsedit"=>"Service Settings Edit","servicesettingsupdate"=>"Service Settings Update",

	"preferedlocationindex"=>"Prefered Location","preferedlocationdrawpolygon"=>"Prefered Location Draw Polygon","preferedlocationcreate"=>"Prefered Location Create","preferedlocationstore"=>"Prefered Location Save","preferedlocationupdate"=>"Prefered Location Update","preferedlocationdestroy"=>"Prefered Location Delete",

	"qberhelpindex"=>"Help",
	"cancelreasonindex"=>"Cancel Reason",
	"subadminindex"=>"Sub Admin","subadmincreate"=>"Sub Admin Create","subadminstore"=>"Sub Admin Store","subadminedit"=>"Sub Admin Edit","subadminupdate"=>"Sub Admin Update","subadmindestroy"=>"Sub Admin Destroy","SMSLog"=>"SMS Log",
	
	"roleindex"=>"Role", "roleedit"=>"Role Edit", "rolesave"=>"Role Save", "roledestroyrole"=>"Role Delete","roledestroy"=>"Role Access Delete","rolestore"=>"Role Create",
	"companyindex"=>"Company","companystore"=>"Company Save","companyedit"=>"Company Edit","companyupdate"=>"Company Update","companydestroy"=>"Company Destroy"



		);
        return view('admin.role.create_edit',  compact('role','title','code'));
    }

    /**
     * This function will delete type and assigned role.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyrole(Request $request)
    {
		 $id=$request->id;
        $affectedRows  = Role::where('id', '=', $id)->where('id', '<>', '1')->delete();

		return $affectedRows;
    }

    /**
     * Remove the role .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows  = Roleaccess::where('id', '=', $id)->where('id', '<>', '1')->delete();

		return $affectedRows;
    }
}
