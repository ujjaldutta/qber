<?php
/*************************************************** Service  Panel for admin *******************************************************************/

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\Country;
use App\Models\Service;
use App\Models\Servicestatus;
use App\Models\Servicetype;
use App\Models\Locationpreferencehead;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller {

	public function __construct()
	{
		
	}

	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function index(Request $request)
    {
        $title="Appointment List";
     
		$services=Service::leftJoin('providers','providers.id','=','service.provider_id')
		->leftJoin('appointments','appointments.service_id','=','service.id')
		->leftJoin('service_status','service_status.id','=','service.service_status_id')
		->leftJoin('service_types','service_types.id','=','service.type_id')
		->leftJoin('customer_profile','customer_profile.customer_id','=','service.customer_id')
		->leftJoin('customers','customers.id','=','service.customer_id')
		
		->selectRaw("service.*,sum(appointments.paid_amount) as completed_amount,service_status.name as status_name,service_types.name as service_name,
			(select price from provider_notification where service_id=service.id and service_notice_status='heading' order by id desc limit 1) as heading_price,
			(select price from provider_notification where service_id=service.id and service_notice_status='jobstarted' order by id desc limit 1) as start_price,
			service.expected_cost as current_price,
			ROUND((TIME_TO_SEC(appointments.service_end_time) - TIME_TO_SEC(appointments.service_start_time))/60)  as total_job_duration,
			providers.name as provider_name,providers.phone as provider_phone,customer_profile.name as customer_name,customers.phone as customer_phone

		")
		->groupby('service.id');
		

		if($request->input('phone')){
			$services=$services->where('providers.phone','like',$request->input('phone'));
		}
		if($request->input('id')){
			$services=$services->where('service.id','=',$request->input('id'));
		}
		if($request->input('userid')){
			$services=$services->where('service.customer_id','=',$request->input('userid'));
		}
		
		if($request->input('servicetype')){
			$services=$services->where('service.type_id',$request->input('servicetype'));
		}

		if($request->input('nationality')){
			$services=$services->where('service.nationality',$request->input('nationality'));
		}

		if($request->input('status')){
			$services=$services->where('service.service_status_id',$request->input('status'));
		}

		if($request->input('minutes')){
			$current=date("Y-m-d H:i:s");
			$pastime = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -".$request->input('minutes')." minutes"));
			
			$services=$services->whereBetween('service.request_date',array($pastime,$current));
		}

		if($request->input('price_from')!='' && $request->input('price_to')!=''){
			$services=$services->where('service.expected_cost','>=',$request->input('price_from'))->where('service.expected_cost','<=',$request->input('price_to'));
			
		}

		if($request->input('requested_from_date')!='' && $request->input('requested_to_date')!=''){
			$services=$services->where('service.request_date','>=',$request->input('requested_from_date'))->where('service.request_date','<=',$request->input('requested_to_date'));
			
		}

		if($request->input('job_area')){
			$services=$services->where('service.preference_job_area',$request->input('job_area'));
		}
		
		
		
		if($request->input('sort') && $request->input('order')){
			
			$services=$services->orderby($request->input('sort'),$request->input('order'))->paginate(100);
		}else{
			$services=$services->orderby('id','desc')->paginate(100);
		}
		//echo $services->count();exit;
		$servicetype=Servicetype::get();
		


		$nationality=Country::select('code', 'countryname as name')->get();
		$servicestatus=Servicestatus::get();
		$perference=Locationpreferencehead::get();

		
        return view('admin.service.index',  compact('services','title','servicetype','nationality','servicestatus','perference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Service";
        $service=Service::find($id);
		$servicetype=Servicetype::pluck('name', 'id');

		$country=Country::pluck('countryname', 'code as id');
		$status=Servicestatus::pluck('name', 'id');
		
        return view('admin.service.create_edit',  compact('servicetype','title','service','country','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $service=Service::find($id);
         
		
        $service->type_id=$request->type_id;
        $service->customer_id=$request->customer_id;
        if($request->provider_id)
        $service->provider_id=$request->provider_id;
        if($request->booking_slot_id)
        $service->booking_slot_id=$request->booking_slot_id;
        $service->job_description=$request->job_description;
		$service->request_date=$request->request_date;
        $service->latitude=$request->latitude;
		$service->longitude=$request->longitude;
		$service->from_time=$request->from_time;
		$service->to_time=$request->to_time;
		$service->nationality=$request->nationality;
		$service->team_size=$request->team_size;
		$service->job_duration_set_by_provider =$request->job_duration_set_by_provider;
        $service->expected_cost =$request->expected_cost;
        $service->service_status_id =$request->service_status_id;
        
        $service->update();
        
		$request->session()->flash('message', 'Service Updated Successfully!');
         return redirect()->back();
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows  = Service::where('id', '=', $id)->delete();

		return $affectedRows;
    }

  
	

}
