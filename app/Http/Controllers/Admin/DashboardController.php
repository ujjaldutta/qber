<?php
/*************************************************** Dashboard for admin ***********************************************************/

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;

use App\Models\Provider;
use App\Models\Customer;
use App\Models\Service;

class DashboardController extends Controller{

public function __construct(){
		
	}


public function index(){

	$title = "Dashboard";

      
        $users = Customer::count();
        $service = Service::count();
        $providers = Provider::count();
		$carwash_total_priority=0;
		$roadside_total_priority=0;
		$pickdelivery_total_priority=0;
        
        $carwash_total_priority=Provider::where("type_id",1)->where("is_online",1)->where("ready_to_serve",1)->sum('moneyexpvalue');
        $count=Provider::where("type_id",1)->where("is_online",1)->where("ready_to_serve",1)->count();
		if($carwash_total_priority>0)
        $carwash_total_priority=number_format($carwash_total_priority/$count,2,".","");

        $roadside_total_priority=Provider::where("type_id",2)->where("is_online",1)->where("ready_to_serve",1)->sum('moneyexpvalue');
        $count=Provider::where("type_id",2)->where("is_online",1)->where("ready_to_serve",1)->count();
		if($roadside_total_priority>0)
        $roadside_total_priority=number_format($roadside_total_priority/$count,2,".","");

        $pickdelivery_total_priority=Provider::where("type_id",3)->where("is_online",1)->where("ready_to_serve",1)->sum('moneyexpvalue');
         $count=Provider::where("type_id",3)->where("is_online",1)->where("ready_to_serve",1)->count();
        if($pickdelivery_total_priority>0)
        $pickdelivery_total_priority=number_format($pickdelivery_total_priority/$count,2,".","");
	
	return view('admin.dashboard.index',  compact('title','users','service','providers','carwash_total_priority','roadside_total_priority','pickdelivery_total_priority'));

}

}
?>
