<?php
/*************************************************** Preferred Locations for different service type  *******************************************/
/******************** Providers can accept jobs within preferred location they selected for fixed and hopping ****************************/
namespace App\Http\Controllers\Admin;
use Response;
use File;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Locationpreferencepolygon;
use App\Models\Locationpreferencehead;
use Illuminate\Support\Facades\Auth;

class PreferedlocationController extends Controller
{
	public function __construct()
	{
		
	}

	/******* Draw and save polygon area in google map */
	public function drawpolygon(){
		
		$title="Draw Polygon";
		return view('admin.preferedlocation.polygon',  compact('title'));
	}

	public function polygondetails(Request $request){
		
	
	}
    /**
     * Display a listing of the polygon area.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title="Prefered Locations";
        $locations=Locationpreferencehead::with('Polygon')->orderby('id','asc')->paginate(100);
        
		return view('admin.preferedlocation.index',  compact('title','locations'));
    }

    /**
     * Show the form for creating a new polygon area.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Prefered Location";
		
		return view('admin.preferedlocation.polygon',  compact('title'));
    }

    /**
     * Store a newly created resource in polygon area.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
			$vertices=json_decode(($request->input('vertices')));
			
			$head=new Locationpreferencehead();
			$head->name=$request->input("name");
			$head->save();

			foreach($vertices as $rec){
				$polygon=new Locationpreferencepolygon();

				$polygon->preference_id=$head->id;
				$polygon->latitude=$rec->lat;
				$polygon->longitude=$rec->lon;
				$polygon->save();
				
			}
				//close the polygon
				$polygon=new Locationpreferencepolygon();

				$polygon->preference_id=$head->id;
				$polygon->latitude=$vertices[0]->lat;
				$polygon->longitude=$vertices[0]->lon;
				$polygon->save();
			
			$request->session()->flash('message', 'Location added successfully!');
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified polygon area.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Prefered Locations";
        $location=Locationpreferencehead::with('Polygon')->find($id);
	
        return view('admin.preferedlocation.polygon',  compact('location','title'));
      
    }

    /**
     * Update the specified resource in polygon area.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$vertices=json_decode(($request->input('vertices')));
		Locationpreferencepolygon::where('preference_id',$id)->delete();
        foreach($vertices as $rec){
				$polygon=new Locationpreferencepolygon();

				$polygon->preference_id=$id;
				$polygon->latitude=$rec->lat;
				$polygon->longitude=$rec->lon;
				$polygon->save();
				
			}
				//close the polygon
				$polygon=new Locationpreferencepolygon();

				$polygon->preference_id=$id;
				$polygon->latitude=$vertices[0]->lat;
				$polygon->longitude=$vertices[0]->lon;
				$polygon->save();
			
			$request->session()->flash('message', 'Location added successfully!');
		return redirect()->back();
    }

    /**
     * Remove the specified resource from polygon area.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows  = Locationpreferencehead::where('id', '=', $id)->first();
       
        $affectedRows  = Locationpreferencehead::where('id', '=', $id)->delete();

		return $affectedRows;
    }

     /**
     * This function will do bulk import from kml file

     */

    public function importkml(Request $request)
    {
		
        $file=$request->file('kmlFile');
		$contents = File::get($file);
        $xml = simplexml_load_string($contents);

		$placemark = $xml->Document->Folder->Placemark;
		
		foreach($placemark as $data){
			$name=$data->name;
			$polygon=explode("0.0",$data->Polygon->outerBoundaryIs->LinearRing->coordinates);

			$head=new Locationpreferencehead();
			$head->name=$name;
			$head->save();

			foreach($polygon as $rec){
				if($rec!=''){
				$area=explode(",",$rec);
				$polygon=new Locationpreferencepolygon();

				$polygon->preference_id=$head->id;
				$polygon->latitude=$area[1];
				$polygon->longitude=$area[0];
				$polygon->save();
				}
				
			}

			
			
		}
		
    }
}
