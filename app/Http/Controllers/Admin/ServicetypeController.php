<?php
/*************************************************** Manage Service type for whole app  *******************************************/
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Servicetype;
use App\Helpers\Thumbnail;
use App\Models\Servicesettings;

class ServicetypeController extends Controller
{
    /**
     * Display a listing of the service type.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="List Service Type";
        $servicetype=Servicetype::orderby('id','desc')->get();

        //print_r($servicetype);exit;
				
        return view('admin.servicetype.index',  compact('servicetype','title'));
    }

    /**
     * Show the form for creating a new service type.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Service Type";

		return view('admin.servicetype.create_edit',  compact('title'));
    }

    /**
     * Store a newly created service in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $type = new Servicetype();
        $picture = "";
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            $type->image = "media/service/".$picture;
        }
        

        
        $type->name=$request->name;
        $type->is_active=1;
        $type->save();

        $setting=new Servicesettings();
		$setting->type_id=$type->id;
		$setting->save();
        
		if ($request->hasFile('image')) {
            
            $destinationPath = public_path() . '/media/service/' ;
            $request->file('image')->move($destinationPath, $picture);

        }
       $request->session()->flash('message', 'Service Type added successfully!');
	   return redirect()->back();
    }

    /**
     * Will enable or disable a service.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusupdate($id,$status,Request $request)
    {
		
       $type=Servicetype::find($id);
       $type->is_active=($status=='1')?0:1;
       $type->update();
       $request->session()->flash('message', 'Service Updated Successfully!');
       return redirect()->back();
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Service Type";
        $type=Servicetype::find($id);

       
		
        return view('admin.servicetype.create_edit',  compact('type','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $type=Servicetype::find($id);

         $picture = $type->image;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
             $type->image = "media/service/".$picture;
        }
       
        
	
        $type->name=$request->name;
        $type->display_text=$request->display_text;
        $type->update();
        if ($request->hasFile('image')) {
            
            $destinationPath = public_path() . '/media/service/' ;
            $request->file('image')->move($destinationPath, $picture);

        }
        
		$request->session()->flash('message', 'Service Updated Successfully!');
         return redirect('admin/servicetype');
    }

    /**
     * Remove the specified service if no transaction made.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows  = Servicetype::where('id', '=', $id)->first();
        @unlink($affectedRows->image);
        $affectedRows  = Servicetype::where('id', '=', $id)->delete();

		return $affectedRows;
    }
}
