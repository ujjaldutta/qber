<?php
/******************************************* Manage Cancel reason...dropdown not using in app now ***************************************/
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Cancelreason;

class CancelreasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="List Cancel Reason";
        $reason=Cancelreason::orderby('id','desc')->get();

       			
        return view('admin.cancelreason.index',  compact('reason','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Cancel Reason for App";

		return view('admin.cancelreason.create_edit',  compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $reason = new Cancelreason();
       
        $reason->title=$request->title;
      
        $reason->is_active=1;
        $reason->save();
		
       $request->session()->flash('message', 'Cancel Reason added successfully!');
	   return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusupdate($id,$status,Request $request)
    {
		
       $reason=Cancelreason::find($id);
       $reason->is_active=($status=='1')?0:1;
       $reason->update();
       $request->session()->flash('message', 'Cancel Reason Updated Successfully!');
       return redirect()->back();
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Cancel Reason";
        $reason=Cancelreason::find($id);

       
		
        return view('admin.cancelreason.create_edit',  compact('reason','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $reason=Cancelreason::find($id);

       
        
        $reason->title=$request->title;
      
        $reason->update();
        
		$request->session()->flash('message', 'Cancel Reason Updated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$affectedRows  = Cancelreason::where('id', '=', $id)->delete();
      
		return $affectedRows;
    }
}
