<?php
/*************************************************** Manage dispute sent to providers  *******************************************/
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Servicetype;
use App\Helpers\Thumbnail;
use App\Models\Dispute;

class DisputeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="List Dispute";
        $dispute=Dispute::with('Customer','Provider')->orderby('id','desc')->get();

        return view('admin.dispute.index',  compact('dispute','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function statusupdate($id,$status,Request $request)
    {
		
       $type=Dispute::find($id);
       $type->status=($status=='1')?0:1;
       $type->update();
       $request->session()->flash('message', 'Dispute Updated Successfully!');
       return redirect()->back();
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Dispute Details";
        $dispute=Dispute::with('Providernotification','Customernotification','Service')->find($id);

       
		//print_r($dispute->toArray());exit;
        return view('admin.dispute.create_edit',  compact('dispute','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $type=Dispute::find($id);

        
        
	
        $type->details=$request->details;
        $type->update();
        
        
		$request->session()->flash('message', 'Dispute Updated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $affectedRows  = Dispute::where('id', '=', $id)->delete();

		return $affectedRows;
    }
}
