<?php
/*************************************************** Manage company from admin panel  *******************************************/
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Http\Requests;

class CompanyController extends Controller
{
    /**
     * Display a listing of the company.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="List Companies";
        $company=Company::orderby('id','desc')->paginate(100);
				
        return view('admin.company.index',  compact('company','title'));
    }

    /**
     * Show the form for creating a new company.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Company";

		
         return view('admin.company.create_edit',  compact('title'));
    }

    /**
     * Store a newly created resource in company.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
        }
		if($pass=='' || $password==''){
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
		 }
        
        $company = new Company();
        $company->email=$request->email;
        $company->password=$pass;
        $company->phone=$request->phone;
        $company->name=$request->name;
        $company->is_active=1;
        $company->save();

       $request->session()->flash('message', 'Company added successfully!');
	   return redirect()->back();
    }

    /**
     * Enable Disable  the specified company.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function statusupdate($id,$status,Request $request)
    {
		
       $company=Company::find($id);
       $company->is_active=($status=='1')?0:1;
       $company->update();
       $request->session()->flash('message', 'Company Updated Successfully!');
       return redirect()->back();
       
    }

    /**
     * Show the form for editing the specified company.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Company";
        $company=Company::find($id);
		
        return view('admin.company.create_edit',  compact('company','title'));
    }

    /**
     * Update the specified company in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
		else{
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
			}
        }
         $company=Company::find($id);
         
		if($pass)
        $company->password=$pass;
        $company->email=$request->email;
        $company->phone=$request->phone;
        $company->name=$request->name;
        $company->update();
		$request->session()->flash('message', 'Company Updated Successfully!');
         return redirect()->back();
         
    }

    /**
     * Remove the specified company from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows  = Company::where('id', '=', $id)->delete();

		return $affectedRows;
    }
}
