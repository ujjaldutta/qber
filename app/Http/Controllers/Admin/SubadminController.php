<?php
/*************************************************** Subadmin management  *************************************************/
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Admin;
use App\Models\Role;
class SubadminController extends Controller{

public function __construct(){
		
	}


public function index(){

		$title="List Admins";
        $admin=Admin::with("Roles","Roleaccess")->orderby('id','desc')->paginate(100);
		//print_r($admin->toArray());exit;
		
		
        return view('admin.subadmin.index',  compact('admin','title'));

}



public function create()
    {
		$title="Create Admin";

		$role_id=Role::pluck('name', 'id');
		//$role_id=DB::table('admin_role')->lists('id,name');
		//print_r($role_id);exit;
         return view('admin.subadmin.create_edit',  compact('title','role_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
        }
		if($pass=='' || $password==''){
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
		 }
        
        $admin = new Admin();
        $admin->email=$request->email;
        $admin->password=$pass;
        $admin->role_id=$request->role_id;
        $admin->save();

       $request->session()->flash('message', 'Admin added successfully!');
	   return redirect()->back();

               
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $title="Edit Admin";
        $admin=Admin::find($id);
		$role_id=Role::pluck('name', 'id');
        return view('admin.subadmin.create_edit',  compact('admin','title','role_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;
		$pass='';
        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $pass = bcrypt($password);
            }
		else{
			$request->session()->flash('message', 'Password not matching!');
			$request->session()->flash('alert-class', 'alert-danger');
			
			 return redirect()->back();
			}
        }
         $admin=Admin::find($id);
		if($pass)
         $admin->password=$pass;
         $admin->email=$request->email;
         $admin->role_id=$request->role_id;
        $admin->update();
		$request->session()->flash('message', 'Updated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $affectedRows  = Admin::where('id', '=', $id)->where('id', '<>', '1')->delete();

		return $affectedRows;
    }

}
?>
