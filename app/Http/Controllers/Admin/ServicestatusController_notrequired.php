<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Servicestatus;

class ServicestatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $title="List Service Status";
        $servicestatus=Servicestatus::orderby('id','desc')->get();

       			
        return view('admin.servicestatus.index',  compact('servicestatus','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title="Create Service Status";

		return view('admin.servicestatus.create_edit',  compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $reason = new Servicestatus();
       
        $reason->name=$request->name;
      
        $reason->save();
		
       $request->session()->flash('message', 'Status added successfully!');
	   return redirect()->back();
    }

 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Service Status";
        $servicestatus=Servicestatus::find($id);

       
		
        return view('admin.servicestatus.create_edit',  compact('servicestatus','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $reason=Servicestatus::find($id);

        $reason->name=$request->name;
      
        $reason->update();
        
		$request->session()->flash('message', 'Service Status Updated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$affectedRows  = Servicestatus::where('id', '=', $id)->delete();
      
		return $affectedRows;
    }
}
