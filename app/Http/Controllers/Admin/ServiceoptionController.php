<?php
/*************************************************** Service options for suv sedan car wash  *******************************************/

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Servicetype;
use App\Models\Serviceoption;
use App\Models\Serviceoptiondetails;

class ServiceoptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title="List Service Options";
        $serviceoption=Serviceoptiondetails::with("Servicetype")->orderby('id','desc')->get();

    
		//print_r($serviceoption->toArray());exit;		
        return view('admin.serviceoption.index',  compact('serviceoption','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title="Edit Service Option";
        $optiondetails=Serviceoptiondetails::find($id);

       $serviceoption=Serviceoption::where('type_id',$optiondetails->type_id)->pluck('name', 'id');
		$servicetype=Servicetype::pluck('name', 'id');
        return view('admin.serviceoption.create_edit',  compact('optiondetails','title','servicetype','serviceoption'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $type=Serviceoptiondetails::find($id);

    
        
	
        //$type->type_id=$request->type_id;
        //$type->service_option_id=$request->service_option_id;
        $type->name=$request->name;
        $type->cost=$request->cost;
        $type->duration=$request->duration;
        $type->update();
       
        
		$request->session()->flash('message', 'Service Option Upodated Successfully!');
         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
