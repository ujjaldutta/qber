<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Mail;
use Hash;
use Illuminate\Support\Facades\Password;
use App\Models\Admin;
class AdminForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		
        $this->middleware('guest');
    }


    public function getReset()
    {
		

        return view('admin.auth.reset');
    }

    public function postReset(Request $request)
   {
	$admin=Admin::where("email",$request->input('email'))->first();
	

	if($admin){
		$x = 5; // Amount of digits
		$min = pow(10,$x);
		$max = (pow(10,$x+1)-1);
		$code = rand($min, $max);
		$admin->reset_token=$code;
		$admin->save();
		$email=$admin->email;
		if($email!=''){
				$data=array("title"=>"Reset Password","email"=>$email,"link"=>url('admin/reset-password/'). '?' . http_build_query(['key' => $code]));
				
				Mail::send('emails.adminreset', $data, function ($message) use($email) {
					$message->from('admin@qber.com', 'Qber');
					$message->subject("Forgot Admin Password");
					$message->to($email);
				});
				
				$request->session()->flash('message', 'Reset Password details sent to email!');
				return redirect()->back();

			}
		

	}else{

		$request->session()->flash('message', 'Invalid Admin Email!');
		return redirect()->back();
	}
    
   }

 public function resetNewPassword(Request $request)
    {
		$key=$request->input('key');

        return view('admin.auth.changepassword',  compact('key'));
    }

  public function updatePassword(Request $request){
		$admin=Admin::where("reset_token",$request->input('key'))->first();
		$password=$request->input('password');
		$confpassword=$request->input('password_confirmation');
		if($admin && $password!='' && $password==$confpassword)
		{
			
			$admin->password=Hash::make($password);
			$admin->reset_token='';
			$admin->remember_token='';
			$admin->save();
			$request->session()->flash('message', 'Password changed successfully!');
			return redirect()->back();

		}else{
		$request->session()->flash('message', 'Invalid admin details!');
		return redirect()->back();

		}

  }
   

}
