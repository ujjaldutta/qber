<?php
/*************************************************** For Schedule Screen in Provider  App *******************************************************************/

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Provider;
use App\Models\Providerprofile;
use App\Models\Providerfreetime;
use App\Models\Providerbookedtime;
use App\Models\Servicesettings;
use App\Models\Service;
use DB;

use App\Traits\ProviderServiceTrait;

class ProviderScheduleController extends Controller {

	use ProviderServiceTrait;
	
	public function __construct()
	{
		//$this->middleware('auth:api');
	}

	/***********************  list schedule based on current time...with booked unavailable and free slot *******************************/
	
	public function getTodaySchedule(Request $request)
	{
		\Artisan::call('command:updateservice');
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

		$datetime = ($request->input('date'))?date("Y-m-d H:i:s",strtotime($request->input('date'))):date('Y-m-d H:i:s');
		$date = ($request->input('date'))?date("Y-m-d",strtotime($request->input('date'))):date('Y-m-d');
		$currenttime = ($request->input('date'))?date("H:i:s",strtotime($request->input('date'))):date('H:i:s');
		
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();
		
		
		$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
		->whereHas('Service', function ($query){
				$query->where("service_status_id",4);
				$query->orwhere("service_status_id",5);
				$query->orwhere("service_status_id",9);
				 
			})

		->where(DB::raw("DATE(book_date)"),"=",$date)->orderby('from_time','asc')->get()->toArray();


		

		


		$freetime=Providerfreetime::where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",$date)
		->whereRaw("(to_time >='".$currenttime."' )")
		
		->orderby('from_time','asc')->first();

		$schedule=array();
		
		if(count($bookedcalendar)>0){
			
			$service=Service::where('id',$bookedcalendar[0]['service']['id'])->first();
			//$status=$this->TimeupdateRescheduleProvider($bookedcalendar[0]['id'],$provider,$service);
		//if($status) echo "test";exit;

				$bookedcalendar=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
					->whereHas('Service', function ($query){
							$query->where("service_status_id",4);
							$query->orwhere("service_status_id",5);
							$query->orwhere("service_status_id",9);
							 
						})

					->where(DB::raw("DATE(book_date)"),"=",$date)->where('to_time',">=",$datetime)->orderby('from_time','asc')->get()->toArray();
				

			

			//find all free time before booking
			if($bookedcalendar && $bookedcalendar[0]['from_time']>$datetime){

				

					$freetimeslot=Providerfreetime::
					
					whereRaw(" `provider_id` = ".$provider->id." and DATE(date) = '".$date."' and to_time between '".$currenttime."' and
					'".date("H:i:s",strtotime($bookedcalendar[0]['from_time']))."' or ( `provider_id` = ".$provider->id." and DATE(date) = '".$date."'
					and '".date("H:i:s",strtotime($bookedcalendar[0]['from_time']))."' between from_time and to_time )")
					
					->orderby('from_time','asc')->get();
					

					$loop=0;
					foreach($freetimeslot as $time){

									$from_time=($loop==0 && $currenttime>$time->from_time)?$datetime:$time->date." ".$time->from_time;

									$to_time=$time->date." ".$time->to_time;
									if($to_time>$bookedcalendar[0]['from_time'])
									$to_time=$bookedcalendar[0]['from_time'];

									$duration=round(abs(strtotime($to_time) - strtotime($from_time)) / 60,2);



									if($duration>0)
									$schedule[]['schedule']=array("from_time"=>$from_time,"to_time"=>$to_time,"time_type"=>$time->time_type,"duration"=>$duration,"free_id"=>$time->id);
									$loop++;

								}

			
			
			}



			
			//find booking after first freetime

			/*************************************************Bookings***********************************************************************/
			$i=0;
		foreach($bookedcalendar as $booked){

			
			$buffer_time=0;
			if(isset($booked['service']['type_id']))
			{
			$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$booked['service']['type_id'])->first();
			
			$buffer_time=(int)$buffer_time->value;
			}



			// if its first schedule
			if( $i==0){
			
				if(($booked['service']['estimated_traveltime_heading']=='' || $booked['service']['estimated_traveltime_heading']=='0')){
				//get duration between current and first appointment
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$provider->current_latitude.','.$provider->current_longitude;
				$destination='&destinations='.$booked['service']['latitude'].','.$booked['service']['longitude'];
				
				$data=$this->httpGetGoogle($url.$origin.$destination."&mode=driving&key=".env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;
				
				$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
				}else{

				$estimated_traveltime_heading=$booked['service']['estimated_traveltime_heading'];
				}

				$travel_start_time=$booked['service']['from_time'];
			
			//fetch heading time
			}elseif($booked['service']['estimated_traveltime_heading']!='' &&  $booked['service']['estimated_traveltime_heading']!='0'){

				$estimated_traveltime_heading=$booked['service']['estimated_traveltime_heading'];



			//update heading time
			}elseif(($booked['service']['estimated_traveltime_heading']=='' || $booked['service']['estimated_traveltime_heading']=='0')

			&& $i>0 && isset($booked['service']['type_id'])){

				$source_latitude=$bookedcalendar[$i-1]['service']['latitude'];
				$source_longitude=$bookedcalendar[$i-1]['service']['longitude'];

				$destination_latitude=$booked['service']['latitude'];
				$destination_longitude=$booked['service']['longitude'];


				//get duration between current and first appointment
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$source_latitude.','.$source_longitude;
				$destination='&destinations='.$destination_latitude.','.$destination_longitude;
				
				$data=$this->httpGetGoogle($url.$origin.$destination."&mode=driving&key=". env('GOOGLE_KEY',"AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k"));
				$data=json_decode(($data),true);
				if(!isset($data['rows'][0]['elements'][0]['duration']))
				return false;

				$estimated_traveltime_heading=floor($data['rows'][0]['elements'][0]['duration']['value']/60)+$buffer_time;
				
				
			}
			if($i>0 && isset($booked['service']['type_id'])){
				$prevschedule=end($schedule);
				//if no workend time means lastone is free slot
				$travel_start_time=isset($prevschedule['schedule']['work_end_time'])?$prevschedule['schedule']['work_end_time']:$prevschedule['schedule']['to_time'];
			}

			
			if(isset($booked['service']['type_id'])){
			$service=Service::where('id',$booked['service']['id'])->first();
			if($service->estimated_traveltime_heading=='' || $service->estimated_traveltime_heading=='0')
				{
				$service->estimated_traveltime_heading=$estimated_traveltime_heading;
				$service->save();
				}
			}

			$work_duration=$booked['total_duration'];
			
			//$to_time=$booked['service']['to_time'];
			//$from_time=$booked['service']['from_time'];
			

			/*
			 * This calculation already made at accpet of booking so not required
			 $from_time=date("Y-m-d H:i:s", strtotime('+'.($buffer_time+$estimated_traveltime_heading).' minutes', strtotime($from_time)));
			$to_time=date("Y-m-d H:i:s", strtotime('+'.($work_duration).' minutes', strtotime($from_time)));
			*/
			$from_time=$booked['from_time'];
			$to_time=$booked['to_time'];




/*************************************************Freeslot between ***********************************************************************/
		

			//find free slots between
			$lastloop=count($schedule)-1;
			if(isset($schedule[$lastloop]['schedule']['work_end_time'])){
				
				$free_from_time=$schedule[$lastloop]['schedule']['work_end_time'];
				$free_to_time=$from_time;
				
				$duration=round(abs(strtotime($free_to_time) - strtotime($free_from_time)) / 60,2);

				if($free_from_time<$free_to_time){
				$betweenfreetime=Providerfreetime::whereRaw("('".date("H:i:s",strtotime($free_from_time)) ."' between from_time and to_time
					and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d",strtotime($booked['to_time']))."')
					OR  ('".date("H:i:s",strtotime($free_to_time))."' between from_time and to_time and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d",strtotime($booked['to_time']))."')
					OR( from_time >='".date("H:i:s",strtotime($free_from_time))."' and to_time<='".date("H:i:s",strtotime($free_to_time))."' and provider_id='".$provider->id."' and DATE(date)='".date("Y-m-d",strtotime($booked['to_time']))."')

					")->orderby('from_time','asc')->get()->toArray();
	
				if(count($betweenfreetime)==1 && $duration>0){
					$timetype=$betweenfreetime[0]['time_type'];
					$schedule[]['schedule']=array("from_time"=>$free_from_time,"to_time"=>$free_to_time,"time_type"=>$timetype,
					"duration"=>$duration,"free_id"=>$betweenfreetime[0]['id']);

				}
				elseif(count($betweenfreetime)>1){

					for($tloop=0;$tloop<count($betweenfreetime);$tloop++){
						
						$frto_time=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['to_time'];
						$timetype=$betweenfreetime[$tloop]['time_type'];
						
						if($tloop>0){
							$frfrom_time=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['from_time'];

						}
						if($tloop==count($betweenfreetime)-1){

							
							$frto_time=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['to_time'];

							
						}
						if($tloop==0){
							$frfrom_time=$free_from_time;
						}

						$duration=round(abs(strtotime($frto_time) - strtotime($frfrom_time)) / 60,2);

						$free_to_time2=$betweenfreetime[$tloop]["date"]." ".$betweenfreetime[$tloop]['to_time'];
							
							//check if last loop and already have booking then dont show this slot
							if($free_to_time2>$booked['service']['to_time'])
							continue;

						if($duration>0)
						$schedule[]['schedule']=array("from_time"=>$frfrom_time,"to_time"=>$frto_time,"time_type"=>$timetype,
						"duration"=>$duration,"free_id"=>$betweenfreetime[$tloop]['id']);
					}

				}

				}
			}
			/*************************************************Freeslot between End***********************************************************************/
			if($booked['service']['service_status_id']==5){
			$estimated_traveltime_heading=$booked['service']['total_traveltime'];
			}


				$schedule[]['schedule']=array("from_time"=>$booked['service']['from_time'],"to_time"=>$booked['service']['to_time'],"work_start_time"=>$from_time,"work_end_time"=>$to_time,"service_status_id"=>$booked['service']['service_status_id'],
			"work_duration"=>$work_duration,"heading_time"=>$estimated_traveltime_heading,'time_type'=>'booked',"service_id"=>$booked['service']['id'],"job_id"=>$booked['service']['job_id'],"shorttime"=>"false","can_attend"=>(($booked['service']['short_duration'])=='1')?'false':'true');
			
		$i++;

			
		
		}
//exit;	
		/*************************************************Bookings End***********************************************************************/




			//detect short time and mark
			for($i=0;$i<count($schedule);$i++){

				$previous_schedule=array();

				for($j=$i-1;$j>=0;$j--){
						if(isset($schedule[$j]['schedule']['from_time']) && isset($schedule[$j]['schedule']['service_id'])){
							$previous_schedule=$schedule[$j];
							break;

						}

				}

				
				
				
				if(isset($schedule[$i]['schedule']['work_end_time'])
				//&& isset($schedule[$i-1]['schedule']['from_time'])
				//&& isset($schedule[$i-1]['schedule']['service_id'])
				&& count($previous_schedule)>0
				&& isset($schedule[$i]['schedule']['service_id']))
				{
					
					//if previous actual_to_time > current start time (red symbol)
					//and current end time > next heading time

					$work_duration=$schedule[$i]['schedule']['work_duration'];
				
					
					if(isset($previous_schedule['schedule']['work_end_time'])){

						$actual_next_heading_time=date("Y-m-d H:i:s", strtotime('-'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($schedule[$i]['schedule']["work_start_time"])));
						//$nextschedule=$schedule[$i+1];
						
						//$next_heading_time=$nextschedule['schedule']['from_time'];
						
						
					}
					//echo $previous_schedule['schedule']['work_end_time'];
					//echo $actual_next_heading_time;
					//echo "-------------<br />";
					
					//if current schedule work start time > arrival end time then schedule should not be booked.
					
					if(isset($previous_schedule['schedule']['work_end_time']) &&
					$previous_schedule['schedule']['work_end_time']>$actual_next_heading_time
						//&& $schedule[$i]['schedule']['work_end_time'] > $next_heading_time
					){
						//print_r($previous_schedule);
						
						$schedule[$i]['schedule']["shorttime"]="true";

						if(isset($previous_schedule['schedule']['current_work_end_time']))
						$schedule[$i]['schedule']["current_work_start_time"]=date("Y-m-d H:i:s", strtotime('+'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($previous_schedule['schedule']['current_work_end_time'])));
						else
						$schedule[$i]['schedule']["current_work_start_time"]=date("Y-m-d H:i:s", strtotime('+'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($previous_schedule['schedule']['work_end_time'])));

						$schedule[$i]['schedule']["current_work_end_time"]=date("Y-m-d H:i:s", strtotime('+'.$work_duration.' minutes', strtotime($schedule[$i]['schedule']["current_work_start_time"])));
					}


				}
				//check schedule before free slot
				elseif(isset($schedule[$i]['schedule']['work_end_time'])
				&& isset($schedule[$i-2]['schedule']['from_time'])
				&& isset($schedule[$i-2]['schedule']['service_id'])
				&& isset($schedule[$i]['schedule']['service_id']))
				{
					$work_duration=$schedule[$i]['schedule']['work_duration'];
					

					if(isset($schedule[$i-2]['schedule']['work_end_time'])){
						
						$actual_next_heading_time=date("Y-m-d H:i:s", strtotime('-'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($schedule[$i]['schedule']["work_start_time"])));;
						
						
						
					}

					//if current schedule work start time > arrival end time then schedule should not be booked.
					
					if(isset($schedule[$i-2]['schedule']['work_end_time']) 
					&& $schedule[$i-2]['schedule']['work_end_time']>$actual_next_heading_time
						
					){
						
						$schedule[$i]['schedule']["shorttime"]="true";
						$schedule[$i]['schedule']["current_work_start_time"]=date("Y-m-d H:i:s", strtotime('+'.$schedule[$i]['schedule']['heading_time'].' minutes', strtotime($schedule[$i-2]['schedule']['work_end_time'])));

						$schedule[$i]['schedule']["current_work_end_time"]=date("Y-m-d H:i:s", strtotime('+'.$work_duration.' minutes', strtotime($schedule[$i]['schedule']["current_work_start_time"])));
					}

				}
				

			}


//print_r($schedule);
//exit;
/*************************************************************************************************************************************/



			

							$lastschedule=end($schedule);
							//get all next free or unavailable slots
						$nextfreetime=Providerfreetime::
						whereRaw(" ( '".date("H:i:s",strtotime($lastschedule['schedule']['work_end_time']))."' between from_time and to_time  and
						provider_id='".$provider->id."' and ".DB::raw("DATE(date)")." = '".date("Y-m-d",strtotime($lastschedule['schedule']['to_time']))."')
						OR (to_time>= '".date("H:i:s",strtotime($lastschedule['schedule']['work_end_time']))."' and
						provider_id='".$provider->id."' and ".DB::raw("DATE(date)")." = '".date("Y-m-d",strtotime($lastschedule['schedule']['to_time']))."')	
								
								")
								
								
								->orderby('from_time','asc')->get();
								$loop=0;
								foreach($nextfreetime as $time){
									$toworktime=($lastschedule['schedule']['work_end_time']);
									
									if(isset($lastschedule['schedule']['current_work_end_time'])){
										$toworktime=$lastschedule['schedule']['current_work_end_time'];
										}

									$fromfreetime=$time->date." ".$time->from_time;

									
									if($loop==0 && $toworktime>$fromfreetime)
									$fromfreetime=$toworktime;
									$schedule[]['schedule']=array("from_time"=>$fromfreetime,"to_time"=>$time->date." ".$time->to_time,"time_type"=>$time->time_type,"duration"=>$time->total_duration,"free_id"=>$time->id);
									$loop++;
								}


			


		if(count($schedule)<=0)
		{

			if($datetime>$freetime->date." ".$freetime->from_time)
			$from_time = $datetime;
			else
			$from_time = $freetime->date." ".$freetime->from_time;

			$to_time = ($freetime->date." ".$freetime->to_time);


			$duration=round(abs(strtotime($to_time) - strtotime($from_time)) / 60,2);
			
			$schedule[]['schedule']=array("from_time"=>$from_time,"to_time"=>$to_time,"duration"=>$duration,"time_type"=>$freetime->time_type);

					
								$nextfreetime=Providerfreetime::where('from_time','>=', $to_time)
								->where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",date("Y-m-d",strtotime($to_time)))
								->orderby('from_time','asc')->get();

								foreach($nextfreetime as $time){
									$schedule[]['schedule']=array("from_time"=>$time->date." ".$time->from_time,"to_time"=>$time->date." ".$time->to_time,"time_type"=>$time->time_type,"duration"=>$time->total_duration,"free_id"=>$time->id);

								}






		}

			

		}elseif($freetime){

	
			///this condition falls on no-booking
			
			if($datetime>$freetime->date." ".$freetime->from_time)
			$from_time = $datetime;
			else
			$from_time = $freetime->date." ".$freetime->from_time;

			$to_time = ($freetime->date." ".$freetime->to_time);


			$duration=round(abs(strtotime($to_time) - strtotime($from_time)) / 60,2);
			
			$schedule[]['schedule']=array("from_time"=>$from_time,"to_time"=>$to_time,"duration"=>$duration,"time_type"=>$freetime->time_type);

					
								$nextfreetime=Providerfreetime::where('from_time','>=', $to_time)
								->where('provider_id',$provider->id)->where(DB::raw("DATE(date)"),"=",date("Y-m-d",strtotime($to_time)))
								->orderby('from_time','asc')->get();

								foreach($nextfreetime as $time){
									$schedule[]['schedule']=array("from_time"=>$time->date." ".$time->from_time,"to_time"=>$time->date." ".$time->to_time,"time_type"=>$time->time_type,"duration"=>$time->total_duration,"free_id"=>$time->id);

								}

								



		}









		if(count($schedule)>0){
			$response['success'] = 0;
			$response['schedule'] = $schedule;
			$response['msg'] = 'success';
		}else{
			$response['success'] = 2;
			
			$response['msg'] = 'No schedule settled';
		}
		
		
		return Response::json($response);
	}

	/***********************  get schedule by id  (not required)*******************************/

	
	public function getScheduleById(Request $request){
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		
		$freetime=Providerfreetime::where('id',$request->input('id'))->first();


		if($freetime->count()>0){
			$response['success'] = 0;
			$response['schedule'] = $freetime;
			$response['msg'] = 'success';
		}else{
			$response['success'] = 2;
			
			$response['msg'] = 'No schedule';
		}
		
		
		return Response::json($response);
	}



	/***********************  get free schedule of a provider of one week *******************************/


	public function getSchedule(Request $request){
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		$provider=Provider::where('phone',$phone)->first();

		
		$freetime=Providerfreetime::where('provider_id',$provider->id)->orderby('from_time','asc')->groupby('weekday')->get()->toArray();
		
		$calendar=array();
		$i=0;
		foreach($freetime as $time){
			$starttime=Providerfreetime::select('from_time')->where('provider_id',$provider->id)
			->where('weekday',$time['weekday'])->orderby('from_time','asc')->first();
			
			$freetime[$i]['day_start']=$starttime->from_time;

			$starttime1=Providerfreetime::select('to_time')->where('provider_id',$provider->id)
			->where('weekday',$time['weekday'])->orderby('to_time','desc')->first();
			
			$freetime[$i]['day_end']=$starttime1->to_time;

			$singleday=Providerfreetime::where('provider_id',$provider->id)->orderby('from_time','asc')->where('weekday',$time['weekday'])->get()->toArray();
			
			$calendar[$time['weekday']]=array_merge($singleday,array("day_start"=>$starttime->from_time,"day_end"=>$starttime1->to_time));
			

			
		$i++;
		}

		if(count($calendar)>0){
			$response['success'] = 0;
			$response['schedule'] = $calendar;
			$response['msg'] = 'Success';
		}else{
			$response['success'] = 2;
			
			$response['msg'] = 'No schedule';
		}
		
		
		return Response::json($response);
	}


	
	/***********************  set schedule individually based on id (not required) *******************************/
	
	
	public function setScheduleById(Request $request){
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));

		$date = $request->input('date');
		$from_time = $request->input('from_time');
		$to_time = $request->input('to_time');
		$type = ($request->input('type'))?$request->input('type'):'free';
		if($date=='' || $from_time=='' || $to_time=='')return Response::json(array("success"=>false));
		if($date<date() || $from_time<date() || $to_time<date())return Response::json(array("success"=>false));

		
		
		$freetime=Providerfreetime::where('id',$request->input('id'))->first();
		$freetime->date=$date;
		$freetime->from_time=$from_time;
		$freetime->to_time=$to_time;
		$freetime->time_type=$type;
		$freetime->save();


		if($freetime->count()>0){
			$response['success'] = 0;
			$response['schedule'] = $freetime;
			$response['msg'] = 'Success';
		}else{
			$response['success'] = 2;
			
			$response['msg'] = 'No schedule';
		}
		
		
		return Response::json($response);
	}


	/***********************  set schedule  *******************************/
	//set schedule of one week based on free and unavailable in free slot table. cron job will set duration between from and to time.
	// if there is already booking provider cant set that time as unavailable

	/***********************  set schedule  *******************************/
	
	public function setSchedule(Request $request){
		
		if(!$this->isvalid($request))return Response::json(array("success"=>1,"msg"=>"Login fails"));
		$phone = $request->input('phone');
		$details = json_decode($request->input('details'));
		
		$provider=Provider::where('phone',$phone)->first();
		$schedules=$details->availibility;

		$canschedule=true;
		$count=0;
		
		foreach($schedules as $schedule){
			$date=date('Y-m-d',strtotime($schedule->date));
			$fdate=date("Y-m-d",strtotime("+7 day", time()));
			if($date<date("Y-m-d") || $date>$fdate){
				$canschedule=false;
				return Response::json(array("success"=>2,"msg"=>"Invalid date ".$date));
				break;
				exit;
			}elseif($count==0 && $date>date("Y-m-d")){
				$canschedule=false;
				return Response::json(array("success"=>2,"msg"=>"Invalid date ".$date));
				break;
				exit;
			}
			$isActive=$schedule->isActive;
			$startTime=$schedule->startTime;
			$endTime=$schedule->endTime;
			$i=0;
			

			$from_time=date("Y-m-d H:i:s", strtotime($date." ".$startTime));
			$to_time=date("Y-m-d H:i:s", strtotime($date." ".$endTime));

			$bookedcount=Providerbookedtime::where('provider_id',$provider->id)
				->where('heading_start_time','<=', $from_time)
				->where('to_time','>=', $from_time)
				->whereDate('book_date',date("Y-m-d",strtotime($to_time)))
				->orWhere('heading_start_time','<=', $to_time)
				->where('to_time','>=', $to_time)
				->where('provider_id',$provider->id)
				->whereDate('book_date',date("Y-m-d",strtotime($to_time)))->orderby('from_time','desc')->count();



			if($isActive=='false' && $bookedcount>0){
					$response['success'] = 2;
					$response['msg'] = 'Unable to reschedule. Booking already alloted within this time';
					$canschedule=false;
					return Response::json($response);
					break;
			}
			if(count($schedule->unavailableTime)>0)
			{
				foreach($schedule->unavailableTime as $unavailable){

							$from_time=date("Y-m-d H:i:s", strtotime($date." ".$unavailable->start));
							$to_time=date("Y-m-d H:i:s", strtotime($date." ".$unavailable->end));


							$bookedcount=Providerbookedtime::where('provider_id',$provider->id)
							->where('heading_start_time','<=', $from_time)
							->where('to_time','>=', $from_time)
							->whereDate('book_date',date("Y-m-d",strtotime($to_time)))
							->orWhere('heading_start_time','<=', $to_time)
							->where('to_time','>=', $to_time)
							->where('provider_id',$provider->id)
							->whereDate('book_date',date("Y-m-d",strtotime($to_time)))
							->orWhere('heading_start_time','<=', $from_time)
							->where('to_time','>=', $to_time)
							->where('provider_id',$provider->id)
							->whereDate('book_date',date("Y-m-d",strtotime($to_time)))

							->orderby('from_time','desc')->count();
							if($bookedcount>0){
							$response['success'] = 2;
							$response['msg'] = 'Unable to reschedule. Booking already alloted within this time.';
							$canschedule=false;
							
							return Response::json($response);
							break;
							
							}


						}

			}
			if($canschedule==false)
			break;
			$count++;
		}
		if($canschedule==false) {
			$response['success'] = 2;
			$response['msg'] = 'Unable to reschedule.';
			
			return Response::json($response);
			exit;
		}

		foreach($schedules as $schedule){
			$date=date('Y-m-d',strtotime($schedule->date));
			$fdate=date("Y-m-d",strtotime("+7 day", time()));
			if($date<date("Y-m-d") || $date>$fdate){
				return Response::json(array("success"=>2,"msg"=>"Invalid date ".$date));
				break;
				exit;
			}
			$isActive=$schedule->isActive;
			$startTime=$schedule->startTime;
			$endTime=$schedule->endTime;
			$i=0;
			if($canschedule){
			Providerfreetime::where('date',$date)->where("provider_id",$provider->id)->delete();


			if($isActive=='false'){

				
				
					$addfreetime=new Providerfreetime();
					$addfreetime->provider_id=$provider->id;
					$addfreetime->date=$date;
					$addfreetime->from_time=date("Y-m-d H:i:s", strtotime($date." ".$startTime));
					$addfreetime->to_time=date("Y-m-d H:i:s", strtotime($date." ".$endTime));
					
					if($addfreetime->from_time==$addfreetime->to_time) continue;
					$addfreetime->time_type='unavailable';
					$addfreetime->weekday=strtolower(date('l',strtotime($date)));
					$addfreetime->save();

					
			}else{

			
			$freeslot=array();
			$unavailablesolt=array();
		
				

				//arrange unavailable and free
				if(count($schedule->unavailableTime)>0)
				{
						foreach($schedule->unavailableTime as $unavailable){

							$avendtime=date("Y-m-d H:i:s", strtotime($date." ".$unavailable->start));
							$avstarttime=date("Y-m-d H:i:s", strtotime($date." ".$unavailable->end));
							
							$unavailablesolt[]=array('start'=>$avendtime,'end'=>$avstarttime);
							if($i==0){
								$freeslot[]=array('start'=>date("Y-m-d H:i:s", strtotime($date." ".$startTime)),'end'=>date("Y-m-d H:i:s", strtotime($date." ".$unavailable->start)));
							}
							elseif(count($schedule->unavailableTime)-1==$i){
							
								$freeslot[]=array('start'=>date("Y-m-d H:i:s", strtotime($date." ".$schedule->unavailableTime[$i-1]->end)),'end'=>$avendtime);
							}
							else{
								$freeslot[]=array('start'=>$date." ".$schedule->unavailableTime[$i-1]->end,'end'=>$avendtime);
							}

						$i++;
						}
						
						$freeslot[]=array('start'=>$avstarttime,'end'=>date("Y-m-d H:i:s", strtotime($date." ".$endTime)));
				}
				else
				{
					
				$freeslot[]=array('start'=>$startTime,'end'=>$endTime);
				}

				


				//setup slot per day
				foreach($unavailablesolt as $slot){
					if($date>$fdate) continue;
				$addfreetime=new Providerfreetime();
				$addfreetime->provider_id=$provider->id;
				$addfreetime->date=$date;
				$addfreetime->from_time=$slot['start'];
				$addfreetime->to_time=$slot['end'];
				$addfreetime->time_type='unavailable';
				$addfreetime->weekday=strtolower(date('l',strtotime($date)));
				$addfreetime->save();
				}
				foreach($freeslot as $slot){
					if($date>$fdate) continue;
				$addfreetime=new Providerfreetime();
				$addfreetime->provider_id=$provider->id;
				$addfreetime->date=$date;
				if($slot['start']==$slot['end']) continue;
				$addfreetime->from_time=$slot['start'];
				$addfreetime->to_time=$slot['end'];
				$addfreetime->time_type='free';
				$addfreetime->weekday=strtolower(date('l',strtotime($date)));
				$addfreetime->save();
				}

				//reschedule booking
				$booking=Providerbookedtime::whereDate("book_date",$date)->where("provider_id",$provider->id)->orderby("from_time","asc")->first();
				if($booking){
				$service=Service::where("booking_slot_id",$booking->id)->first();
				$this->TimeupdateRescheduleProvider($booking->id,$provider,$service);
				}
				
			}

		}

		}

		$response['success'] = 0;
		//$response['schedule'] = Providerfreetime::where('date',$date)->get();
		$response['msg'] = 'Schedule allocated';
		return Response::json($response);
	}

	/***********************  provider login validity *******************************/
	
	private function isvalid($request)
	{
		//$phone = $request->input('phone');
		$check=$this->check_token_provider($request);

		if(!$check){
			return false;
		}
		return true;


	}
	
}
