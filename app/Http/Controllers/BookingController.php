<?php
/*************************************************** This controller used for test service request *********************************************************/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Models\Customer;
use App\Models\Servicetype;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Providerworktime;
use App\Models\Providerfreetime;
use App\Models\Providerbookedtime;
use App\Models\Country;
use App\Models\Servicesettings;
use App\Models\Overlaptime;
use App\Traits\CustomerServiceTrait;
use App\Traits\ProviderServiceTrait;

class BookingController extends Controller
{

use CustomerServiceTrait;
use ProviderServiceTrait;

  public function __construct() {
     // $this->middleware('auth');

      }
      
	public function	servicetype (Request $request){

			$type=Servicetype::where('is_active','1')->get();
		
			return Response::json($type);
			die;

	}










 public function requestservice(Request $request){
		$title="Service Request";
        $customer=Customer::where('is_active','1')->pluck('email', 'id');
        $service=Servicetype::where('is_active','1')->pluck('name', 'id');
        $country=Country::pluck('countryname', 'code as id');

       $selectedCountry=array('QA');
		
        return view('requestservice',  compact('customer','title','service','country','selectedCountry'));
	}

 public function servicesave(Request $request){
		$status=$this->SaveServiceRequest($request);
		if($status){
		return redirect()->back();
		}
 }


 public function showproviders(Request $request){
	 
		$title="Show Providers";
		$service=Service::where('service_status_id','3')->whereNull('booking_slot_id')->whereNull('provider_id')->pluck('job_description', 'id');
		 return view('showproviders',  compact('title','service'));
 }
 
 public function listproviders(Request $request){
	
		$service=Service::where('id',$request->service_id)->first();

		

		$providers=$this->getProviders($service);
		return Response::json($providers);
 }












 	public function bookservice(Request $request){
		
		$service_id = $request->input('service');
		$provider_id = $request->input('provider');
		$provider=Provider::where('id',$provider_id)->first();
		$service=Service::where('id',$service_id)->first();
		
		if($service->provider_id!=''){
			echo "fail";die;
		}
		$freeslot=Providerfreetime::where('provider_id',$provider->id)
					->where('from_time','<=',date("H:i:s",strtotime($service->from_time)))
					->where('to_time','>=',date("H:i:s",strtotime($service->to_time)))->where('time_type','free')
					->whereDate('date',date("Y-m-d",strtotime($service->to_time)))->first();
		if($freeslot){

			$booked=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
			->where('from_time','<=', $service->from_time)
			->where('to_time','>=', $service->from_time)
			->orWhere('from_time','<=', $service->from_time)
			->where('to_time','>=', $service->from_time)
			->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))->orderby('from_time','asc')->first();







//set booking order

			if($booked){
				//if there is booking after current request calculate duration and distance
				$canbebooked=false;
				
				$working_service=Service::with("Booked")->where('provider_id',$provider->id)->where('id',$booked->service->id)->first();

				$working_service_totime=date("Y-m-d H:i:s", strtotime('+'.$working_service->job_duration_set_by_provider.' minutes', strtotime($working_service->from_time)));
				$travel_start_time=$working_service_totime;

				$confirmed=Providerbookedtime::with('Service')->where('provider_id',$provider->id)
				->where('from_time','<=', $service->from_time)
				->where('to_time','>=', $service->from_time)
				->orWhere('from_time','<=', $service->from_time)
				->where('to_time','>=', $service->from_time)
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))->orderby('from_time','desc')->first();

				
				$confirmed_end=$confirmed->to_time;
				
				$duration_reach_at_confirmed_place=(strtotime($confirmed_end)-strtotime($travel_start_time))/60; //65

				

				//get duration between working and request location
				$url='https://maps.googleapis.com/maps/api/distancematrix/json?';
				$origin='origins='.$working_service->latitude.','.$working_service->longitude;
				$destination='&destinations='.$service->latitude.','.$service->longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=driving&key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k");
				$data=json_decode(($data),true);
				

				$working_torequest_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60);  //10
				
				
				//get duration between request and booked confirmed location
				$origin='origins='.$service->latitude.','.$service->longitude;
				$destination='&destinations='.$confirmed->service->latitude.','.$confirmed->service->longitude;
				
				$data=$this->httpGet($url.$origin.$destination."&mode=driving&key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k");
				$data=json_decode(($data),true);
				
				$requestto_booked_duration=floor($data['rows'][0]['elements'][0]['duration']['value']/60); //5



				

				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($travel_start_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));
				$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;
			

				$buffer_time=Servicesettings::select('code','value')->where('code','buffer_duration')->where('type_id',$service->type_id)->first();

				$buffer_time=(int)$buffer_time->value;
				//$working_torequest_duration.'=';
				//$requestto_booked_duration.'=';
				//$request_service_duration.'=';
				
				$total_expected_duration=$buffer_time+(int)$working_torequest_duration+(int)$requestto_booked_duration+(int)$request_service_duration;



				if($total_expected_duration<$duration_reach_at_confirmed_place){

					//book with ascending order
					$booked->sort_order='1';
					
					$confirmed->sort_order='3';
					$canbebooked=true;
					
					

					
				}else{
				//try other method
				//A>C>B
				
				
				$travel_start_time=date("Y-m-d H:i:s", strtotime('+'.$confirmed->service->job_duration_set_by_provider.' minutes', strtotime($confirmed->from_time)));
				

				$espected_request_start_time=date("Y-m-d H:i:s", strtotime('+'.$requestto_booked_duration.' minutes', strtotime($travel_start_time)));
				$espected_request_end_time=date("Y-m-d H:i:s", strtotime('+'.$service->job_duration_set_by_provider.' minutes', strtotime($espected_request_start_time)));

				$request_service_duration=(strtotime($espected_request_end_time)-strtotime($espected_request_start_time))/60;

				$working_start_time=date("Y-m-d H:i:s", strtotime('+'.$working_torequest_duration.' minutes', strtotime($espected_request_end_time)));

				if($working_start_time<$working_service->to_time){
					
					//book with descending order

					$booked->sort_order='3';
					
					$confirmed->sort_order='1';
					$canbebooked=true;
					
					
				}
				
				


				}
				
				

				
				if($booked->to_time>$confirmed->to_time){
					$maxtime=$booked->to_time;
					}else{
						$maxtime=$confirmed->to_time;
					}

			}
			
			

			$book=new Providerbookedtime;
			$book->book_date=date('Y-m-d');
			$book->provider_id=$provider_id;
			$book->booked_customer_id=$service->customer_id;
			$book->from_time=$service->from_time;
			$book->to_time=$service->to_time;
			$book->total_duration=strtotime($service->to_time)-strtotime($service->from_time) / 60;
			//$book->booking_latitude=$service->latitude;
			//$book->booking_longitude=$service->longitude;
			$book->distance_duration=0;
			if($booked && $canbebooked){
				$book->sort_order=2;

				$futurebook=Providerbookedtime::where('provider_id',$provider->id)
				
				->where('to_time','>', $maxtime)
				->whereDate('book_date',date("Y-m-d",strtotime($service->to_time)))->orderby('from_time','asc')->get();
					$i=4;
					foreach($futurebook as $fbook){
						$updatefbook=Providerbookedtime::where('id',$fbook->id)->first();
						$updatefbook->sort_order=$i;
						$updatefbook->save();
					$i++;
					}
					
				$book->save();
				$booked->save();
				$confirmed->save();

				
				}
			elseif(!$booked){
				$book->save();
			}
			

			
			if($book->id){
				$service->booking_slot_id=$book->id;
				$service->service_status_id=1;
				$service->provider_id=$provider->id;
				$service->save();
			}else{
				echo 'fail';
			}
			
		}
		
		echo 'success';
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function FireEvent(Request $request)
    {
		$title="Fire Event";
        return view('fireevent',  compact('title'));
    }
    
  public function ResultEvent(Request $request)
    {
		$title="Get Event Result";
        return view('showevent',  compact('title'));
    }


    public function Laptime(){

		$data=Overlaptime::select("id","from_time","to_time")->orderby("from_time","asc")->get()->toArray();
		echo '<pre>';
		//print_r($data);
		
		$i=0;
		$totcount=count($data);
		foreach($data as $slot){
			
				$to_time=$slot['to_time'];
				$from_time=$slot['from_time'];

			
			for($j=$i+1;$j<($totcount);$j++){
				
					//echo "test".$data[$j]['id'];
					if( isset($data[$j]) && $from_time<=$data[$j]['from_time']  && $to_time>= $data[$j]['to_time'] )
					{
						
						unset($data[$j]);
						

					}
					

			}
			
			$i++;
		}

		
		$data=array_values($data);
//print_r($data);exit;
		
		$i=0;
		foreach($data as $slot){
			if(isset($data[$i+1]) && $data[$i+1]['from_time']<=$slot['to_time'] && $data[$i+1]['to_time']<=$slot['to_time'])
				{
					
					unset($data[$i+1]);
					//break;
				}
			
				$i++;
		}
		$data=array_values($data);
		//print_r($data);
		
		
		$i=0;
		foreach($data as $slot){
				
				if(isset($data[$i+1]) && $slot['to_time'] >= $data[$i+1]['from_time'] && $data[$i+1]['to_time']>$slot['to_time'])
				{
					$data[$i+1]['from_time']=$slot['to_time'];
					
				}
			
		$i++;
		}
	//print_r($data);
		$duration=0;

		foreach($data as $slot){
			

			$duration +=strtotime($slot['to_time'])-strtotime($slot['from_time']);
		}
		$duration=(round(abs($duration ) / 60,2));
		echo $duration ." Mins";

	}

	
   
}
