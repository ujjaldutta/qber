<?php

namespace App\Events;


use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Customer;

class LocationUpdated implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
      public $data;
	 // public $event;
     
      
    public function __construct()
    {
		
       $this->data = array(
            'power'=> '10'
        );
        
        
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
		// return new PrivateChannel('test-channel');
		return ['test-channel'];
		
        //return new PrivateChannel('customers.'.$this->user->id);
    }
}
