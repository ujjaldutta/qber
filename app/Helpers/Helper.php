<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;


class Helper
{
    public static function checkpermission($code)
    {
		$admin=Admin::with('Roles','Roleaccess')->find(Auth::user()->id)->toArray();
		$access=$admin['roleaccess'];
		$can_access=false;
		
		foreach($access as $component){
			
			if($component['component_code']=='all'){
				$can_access=true;
				break;
			}
			elseif(strtolower($component['component_code'])==strtolower($code)){
				
				$can_access=true;
				break;
			}


		}
        return $can_access;
    }
}
