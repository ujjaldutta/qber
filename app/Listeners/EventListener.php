<?php

namespace App\Listeners;

use App\Events\LocationUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
     use InteractsWithQueue;
     
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LocationUpdated  $event
     * @return void
     */
    public function handle(LocationUpdated $event)
    {
      
		if (true) {
            $this->release(30);
        }
    }
}
