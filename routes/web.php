<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/request-service/save', 'BookingController@servicesave');
Route::get('/request-service', 'BookingController@requestservice');
Route::get('/show-providers', 'BookingController@showproviders');
Route::post('/get-providers', 'BookingController@listproviders');

Route::post('/accept-service', 'BookingController@bookservice');

Route::get('/customer/reset-password', 'HomeController@customerResetPassword');
Route::post('/customer/savepassword', 'HomeController@customerSavePassword');

Route::get('/provider/reset-password', 'HomeController@providerResetPassword');
Route::post('/provider/savepassword', 'HomeController@providerSavePassword');
Route::get('/verify-email', 'HomeController@EmailVerified');




Route::group(['prefix' => 'admin'], function() {


	Route::get('/', 'Auth\LoginController@showLoginForm'); 

	Route::get('auth/login', 'Auth\LoginController@showLoginForm'); 
	Route::post('auth/login', 'Auth\LoginController@login');
	Route::get('auth/logout', 'Auth\LoginController@logout');
	
	Route::get('auth/forgot-password', 'Auth\AdminForgotPasswordController@getReset');
	Route::post('password/reset', 'Auth\AdminForgotPasswordController@postReset');
	Route::get('reset-password', 'Auth\AdminForgotPasswordController@resetNewPassword');
	Route::post('update-password', 'Auth\AdminForgotPasswordController@updatePassword');

});


Route::group(['prefix' => 'company'], function() {

	Route::get('/', 'Auth\CompanyLoginController@showLoginForm'); 

	Route::get('auth/login', 'Auth\CompanyLoginController@showLoginForm'); 
	Route::post('auth/login', 'Auth\CompanyLoginController@login');
	Route::get('auth/logout', 'Auth\CompanyLoginController@logout');

	Route::get('auth/forgot-password', 'Auth\CompanyForgotPasswordController@getReset');
	Route::post('password/reset', 'Auth\CompanyForgotPasswordController@postReset');
	Route::get('reset-password', 'Auth\CompanyForgotPasswordController@resetNewPassword');
	Route::post('update-password', 'Auth\CompanyForgotPasswordController@updatePassword');

});



Route::get('/fire-event', function () {
    // this fires the event
    $data = [
        'event' => 'LocationUpdated',
        'data' => [
            'power' => '100'
        ]
    ];
    
    Redis::publish('test-channel', json_encode($data));
    event(new App\Events\LocationUpdated());
    return "event fired";
});
Route::get('/fire', 'BookingController@FireEvent');
Route::get('/result-event', 'BookingController@ResultEvent');
Route::get('/laptime', 'BookingController@Laptime');










Route::group(['namespace' => 'Company','prefix' => 'company', 'middleware' => ['web','company']], function() {


	Route::get('/profile', 'CompanyController@profile');
	Route::post('/profile/update', 'CompanyController@updateprofile');

	Route::resource('dashboard', 'CompanyController');

	Route::get('providers/invite', 'ProviderController@invite');
	Route::post('providers/sendinvite', 'ProviderController@sendinvite');
	Route::get('providers/live-track', 'ProviderController@livetrack');
	Route::post('providers/get-providers', 'ProviderController@getProviders');

	Route::get('providers/profile/{id}', 'ProviderController@profile');
	Route::get('providers/jobhistory/{id}', 'ProviderController@jobhistory');

	Route::get('providers/sattlebalance/{id}', 'ProviderController@sattleBalance');
	Route::post('providers/updatebalance', 'ProviderController@updateBalance');
	Route::get('providers/ajax-list', 'ProviderController@ajaxProviders');

	Route::resource('providers', 'ProviderController');

	Route::resource('service', 'ServiceController');




});




Route::group(['namespace' => 'Admin','prefix' => 'admin', 'middleware' => ['web','admin']], function() {

	Route::get('providers/download', 'ProviderController@getDownload');
//Route::get('/', 'DashboardController@index');
	Route::get('servicetype/statusupdate/{id}/{status}', 'ServicetypeController@statusupdate');
 
	Route::resource('subadmin', 'SubadminController');
	Route::post('/role/save', 'RoleController@save');

	Route::post('role/destroyrole', ['as' => 'role/destroyrole', 'uses' => 'RoleController@destroyrole']);

	Route::get('company/statusupdate/{id}/{status}', 'CompanyController@statusupdate');
	Route::resource('company', 'CompanyController');

	Route::get('qberhelp/statusupdate/{id}/{status}', 'QberhelpController@statusupdate');
	Route::resource('qberhelp', 'QberhelpController');

	Route::get('cancelreason/statusupdate/{id}/{status}', 'CancelreasonController@statusupdate');
	Route::resource('cancelreason', 'CancelreasonController');
	Route::resource('servicestatus', 'ServicestatusController');
	Route::resource('servicesettings', 'ServicesettingsController');


	Route::resource('service-option', 'ServiceoptionController');
	
	
	Route::resource('servicetype', 'ServicetypeController');
	Route::resource('role', 'RoleController');
	Route::resource('dashboard', 'DashboardController');
	Route::resource('profile', 'ProfileController');


	Route::get('customers/statusupdate/{id}/{status}', 'CustomerController@statusupdate');
	Route::get('customers/smsverify/{id}/{status}', 'CustomerController@smsverify');
	Route::get('customers/profile/{id}', 'CustomerController@profile');
	Route::get('customers/jobhistory/{id}', 'CustomerController@jobHistory');
	Route::get('customers/cancelhistory/{id}', 'CustomerController@cancelHistory');
	Route::get('customers/ratinghistory/{id}', 'CustomerController@ratingHistory');
	Route::get('customers/ajax-list22', 'CustomerController@ajaxCustomers');
	Route::get('customers/send-message', 'CustomerController@sendMessage');
	Route::post('customers/send-message22', 'CustomerController@sendMessagePost');
	
	Route::resource('customers', 'CustomerController');

	Route::get('providers/statusupdate/{id}/{status}', 'ProviderController@statusupdate');
	Route::get('providers/smsverify/{id}/{status}', 'ProviderController@smsverify');
	Route::get('providers/profile/{id}', 'ProviderController@profile');
	Route::get('providers/jobhistory/{id}', 'ProviderController@jobhistory');
	Route::get('providers/ratinghistory/{id}', 'ProviderController@ratingHistory');
	Route::get('providers/cancelhistory/{id}', 'ProviderController@cancelHistory');
	Route::get('providers/sattlehistory/{id}', 'ProviderController@SattleHistory');
	Route::get('providers/sattle-balance', 'ProviderController@sattleBalance');
	Route::get('providers/sattle/{id}', 'ProviderController@Sattle');
	Route::post('providers/sattle-update', 'ProviderController@SattleUpdate');
	Route::get('providers/active-on-map', 'ProviderController@activeProviders');
	Route::get('providers/ajax-list', 'ProviderController@ajaxProviders');
	Route::get('providers/freeschedule/{id}', 'ProviderController@freeslotProviders');
	Route::get('smslog', 'ProviderController@SMSLog');
	Route::get('providers/send-message', 'ProviderController@sendMessage');
	Route::post('providers/send-message', 'ProviderController@sendMessagePost');
	
	Route::post('providers/get-providers', 'ProviderController@getActiveProviders');
	
	Route::resource('providers', 'ProviderController');


	Route::get('/locations/prefered-area', 'PreferedlocationController@drawpolygon');
	Route::get('/locations/prefered-details', 'PreferedlocationController@polygondetails');
	Route::post('/locations/import', 'PreferedlocationController@importkml');
	Route::resource('locations', 'PreferedlocationController');




	Route::resource('appointments', 'ServiceController');
	Route::get('dispute/statusupdate/{id}/{status}', 'DisputeController@statusupdate');
	Route::get('dispute/details/{id}', 'DisputeController@edit');
	Route::resource('dispute', 'DisputeController');


	


});




