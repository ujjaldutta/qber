<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

	//Route::get('auth/login', 'Auth\ApiLoginController@showLoginForm'); 
	//Route::post('auth/login', 'Auth\ApiLoginController@login');; 

	Route::post('home/splash-text', 'HomeController@getSplashText');
	
	Route::get('home/cronjob', function () {

		$exitCode = Artisan::call('command:updateusers');
		$exitCode = Artisan::call('command:updateworktime');
		$exitCode = Artisan::call('command:updatetime');
		$exitCode = Artisan::call('command:updateweeklyroutine');
		$exitCode = Artisan::call('command:updateservice');
		
      
	});


	

	Route::post('provider/login', 'HomeController@providerlogin');
	Route::post('provider/logout', 'HomeController@providerlogout');
	Route::post('provider/signup', 'RegistrationController@providersignup');
	Route::post('provider/verify-email', 'RegistrationController@providerEmailVerify');
	
	Route::post('provider/verify-sms', 'RegistrationController@verifyProviderSms');
	Route::post('provider/resend-verify-sms', 'RegistrationController@resendProviderOtp');
	Route::post('/provider/forgot-password', 'HomeController@providerForgotPassword');
	Route::post('/provider/send-email-verify', 'HomeController@SendMailVerifiy');

	
	Route::post('/provider/savepassword', 'HomeController@providerAPISavePassword');


	Route::post('provider/save-preference', 'ProviderController@savePreference');
	Route::post('provider/get-preference', 'ProviderController@getPreference');
	Route::post('provider/get-provider-preference', 'ProviderController@getProviderPreference');
	Route::post('provider/ready-to-serve', 'ProviderController@ReadytoServe');
	Route::post('provider/get-provider-profile', 'ProviderController@getProviderProfile');
	Route::post('provider/set-provider-profile', 'ProviderController@setProviderProfile');
	Route::post('provider/profile-delete-image', 'ProviderController@deleteProfileImage');

	Route::post('provider/schedule/get-today-schedule', 'ProviderScheduleController@getTodaySchedule');
	Route::post('provider/schedule/get-schedule-byid', 'ProviderScheduleController@getScheduleById');
	Route::post('provider/schedule/set-schedule-byid', 'ProviderScheduleController@setScheduleById');
	Route::post('provider/schedule/get-schedule', 'ProviderScheduleController@getSchedule');
	Route::post('provider/schedule/set-schedule', 'ProviderScheduleController@setSchedule');
	
	Route::post('provider/list-message', 'ProviderController@getMessage');
	Route::post('provider/delete-message', 'ProviderController@DeleteMessage');
	Route::post('provider/send-rating', 'ProviderController@sendRating');
	Route::post('provider/get-rating', 'ProviderController@getRating');
	
	Route::post('provider/list-appointment', 'ProviderController@listAppointment');
	Route::post('provider/appointment-details', 'ProviderController@getAppointment');
	Route::post('provider/cancel-appointment', 'ProviderController@cancelAppointment');
	Route::post('provider/accept-appointment', 'ProviderController@acceptAppointment');
	Route::post('provider/wait-appointment', 'ProviderController@waitAppointment');
	Route::post('provider/autocancel-appointment', 'ProviderController@autocancelAppointment');

	Route::post('provider/start-heading', 'ProviderController@startHeadingAppointment');
	Route::post('provider/start-working', 'ProviderController@startAppointment');
	Route::post('provider/completed-working', 'ProviderController@completedAppointment');
	
	Route::post('provider/get-settings', 'ProviderController@getSettings');
	Route::post('provider/update-settings', 'ProviderController@updateSettings');
	Route::post('provider/get-balance', 'ProviderController@getBalance');
	Route::post('provider/request-balance', 'ProviderController@requestBalance');
	Route::post('provider/cancel-balance-sattle', 'ProviderController@cancelBalanceSattle');
	
	Route::post('provider/update-price', 'ProviderController@updatePrice');
	Route::post('provider/update-duration', 'ProviderController@updateJobduration');
	Route::post('provider/update-endtime', 'ProviderController@updateBookingend');
	Route::post('provider/update-cardetails', 'ProviderController@updateCarServiceDetails');
	
	Route::post('provider/open-dispute', 'ProviderController@openDispute');
	Route::post('provider/live-track', 'ProviderController@UpdateCurrentLocation');
	
	Route::post('provider/get-notification', 'ProviderController@getNotification');
	Route::post('provider/accept-company-invite', 'ProviderController@AcceptCompanyInvite');
	Route::post('provider/update-device', 'ProviderController@updateDevice');
	Route::post('provider/send-message', 'ProviderController@sendMessage');

	Route::post('provider/dashboard', 'ProviderDashboardController@getCurrentSchedule');
	Route::post('provider/update-baselocation', 'ProviderController@updateBaseLocation');
	

	Route::post('service/notice', 'ServiceController@notice');
	Route::post('service/type', 'ServiceController@servicetype');
	
	Route::post('service/get-provider-profile', 'ServiceController@getProviderProfile');
	Route::post('service/overlap', 'ServiceController@overlap');
	Route::post('service/country-list', 'ServiceController@getCountry');
	Route::post('service/get-service', 'ServiceController@getServiceDetails');
	Route::post('service/cancel-reason', 'ServiceController@getCancelReason');
	






	

	Route::post('customer/login', 'HomeController@customerlogin');
	Route::post('customer/logout', 'HomeController@customerlogout');
	Route::post('customer/signup', 'RegistrationController@customersignup');
	Route::post('customer/verify-sms', 'RegistrationController@verifyCustomerSms');
	Route::post('customer/resend-verify-sms', 'RegistrationController@resendCustomerOtp');
	Route::post('/customer/forgot-password', 'HomeController@customerForgotPassword');

	Route::post('service/list-providers', 'CustomerController@getMatchingProviders');
	
	Route::post('customer/profile', 'CustomerController@getProfile');
	Route::post('customer/update-profile', 'CustomerController@updateProfile');
	Route::post('customer/past-service', 'CustomerController@pastService');	
	//Route::get('customer/list-page', 'CustomerController@listpage');
	Route::post('customer/create-service', 'CustomerController@createService');
	Route::post('customer/save-location', 'CustomerController@saveLocation');
	Route::post('customer/delete-location', 'CustomerController@deleteLocation');
	Route::post('customer/list-locations', 'CustomerController@listLocation');

	Route::post('customer/booked-appointments', 'CustomerController@bookedAppointments');
	Route::post('customer/current-service', 'CustomerController@listAppointment');
	
	Route::post('customer/view-appointment', 'CustomerController@viewAppointment');
	Route::post('customer/update-appointment', 'CustomerController@updateAppointment');
	Route::post('customer/cancel-appointment', 'CustomerController@cancelAppointment');

	Route::post('customer/send-rating', 'CustomerController@sendRating');
	Route::post('customer/get-rating', 'CustomerController@getRating');
	
	Route::post('customer/open-dispute', 'CustomerController@openDispute');
	Route::post('customer/repeat-service', 'CustomerController@repeatService');
	
	Route::post('customer/get-notification', 'CustomerController@getNotification');
	Route::post('customer/update-device', 'CustomerController@updateDevice');
	Route::post('customer/update-cardetails', 'CustomerController@updateCarServiceDetails');
	Route::post('customer/update-customer-location', 'CustomerController@updateCustomerLocation');
	Route::post('customer/send-message', 'CustomerController@sendMessage');
	Route::post('customer/live-track-details', 'CustomerController@TrackLocationDetails');
	
	Route::resource('customer', 'CustomerController');


	



Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

