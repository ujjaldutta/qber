@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('content')
<div class="login-panel">
  <p class="logo"></p>
  <div class="page-header">
    <h2 class="text-center">{{$title}}</h2>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-6"> @if(Session::has('message'))
      <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
      @endif
      
     </div>
  </div>

</div>
@endsection
{{-- Scripts --}}
@section('scripts') 

@endsection 
