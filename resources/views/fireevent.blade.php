@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('content')
   <h3>
        {{$title}}
    </h3>
    <div class="row">
		<div class="col-lg-8 col-md-6">

			@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif
						
 <div class="row">
				 <div class="form-group">
						<div class="col-lg-12">
							<div class="pull-right">
							<p id="power"></p>
							</div>						
						</div>
					</div>
			</div>

 {!! Form::close() !!}
      </div>
</div>
<div class="row">
Demo front interface
</div>
@endsection
<style>
 #map {
        height: 100%;
      }

</style>
{{-- Scripts --}}
@section('scripts')
  
 <script src="{{ asset('js/node_modules/socket.io/node_modules/socket.io-client/socket.io.js') }}"></script>
    <script>
        //var socket = io('http://localhost:3000');
       /* var socket = io('http://127.0.0.1:3000');
        socket.on('name_set', function(data) {

			console.log(data)
			})*/
        </script>  
<script>
       var counter = 30;

$(function() {
    prepare();
    update();
});

function update() {
    $("#timer").html("Refreshing in " + counter + " seconds...");
    counter--;

    if (counter == 0) {
        counter = 30;
        prepare();
    }

    setTimeout(update, 500);
}

function prepare() {
    $.ajax({
        type: "GET",
        url: "fire-event",
        contentType: "application/json; charset=utf-8",
        success: function(data){

			$("#power").html(data)
			}
        
    });
}
    </script>
  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
