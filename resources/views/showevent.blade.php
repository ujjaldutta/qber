@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('content')
   <h3>
        {{$title}}
    </h3>
    <div class="row">
		<div class="col-lg-8 col-md-6">

			@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif
						
 <div class="row">
				 <div class="form-group">
						<div class="col-lg-12">
							<div class="pull-right">
							<p id="power">0</p>
							</div>						
						</div>
					</div>
			</div>

 {!! Form::close() !!}
      </div>
</div>
<div class="row">
Demo front interface
</div>
@endsection
<style>
 #map {
        height: 100%;
      }

</style>
{{-- Scripts --}}
@section('scripts')
  
  <script src="{{ asset('js/socket.io.js') }}"></script>
    <script>
        //var socket = io('http://localhost:3000');
        var socket = io('http://127.0.0.1:3000');
        socket.on("test-channel:App\Events\LocationUpdated", function(message){
            // increase the power everytime we load test route
            console.log("test");
            $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
        });

       // socket.emit("set_name", { name: "sdfds"});

    </script>
  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
