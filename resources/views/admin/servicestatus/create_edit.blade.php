@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->
@if (isset($servicestatus))
{!! Form::model($servicestatus, array('url' => url('admin/servicestatus') . '/' . $servicestatus->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
@else
{!! Form::open(array('url' => url('admin/servicestatus'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif
   
        
       
        
        <div class="form-group  {{ $errors->has('question') ? 'has-error' : '' }}">
            {!! Form::label('name', 'Status Detail', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('name', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
            </div>
        </div>



      


        <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/servicestatus')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>
						</div>
					</div>


     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#type").select2()
            });
        </script>
 @endsection       
