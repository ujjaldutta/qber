@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
							@if(Session::has('message'))
							<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
								{{ Session::get('message') }}
							  </div>
							  
							@endif
		
				{!! Form::model($admin, array('url' => url('admin/profile') . '/' . $admin->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
					<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
						<label class="control-label" for="email">Admin Email</label>
						<div class="controls">
							{!! Form::email('email', null, array('class' => 'form-control','required' => 'required')) !!}
							<span class="help-block">{{ $errors->first('email', ':message') }}</span>
						</div>
					</div>
					<div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							{!! Form::password('password', array('class' => 'form-control required','required' => 'required')) !!}
							<span class="help-block">{{ $errors->first('password', ':message') }}</span>
						</div>
					</div>
					<div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
						<label class="control-label" for="password_confirmation">Confirm Password</label>
						<div class="controls">
							{!! Form::password('password_confirmation', array('class' => 'form-control required','required' => 'required')) !!}
							<span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
						</div>
					</div>


					 <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/dashboard')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Update
							</button>
						</div>
					</div>
    

				 {!! Form::close() !!}

			</div>
         </div>
@endsection
