@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-8 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif
<div class="row">
						<div class="form-group ">
							
							<div class="controls">
								{!! $dispute->details !!}
								
							</div>
						</div>
</div>


<div class="row">
	 <h3>
       Service Details
    </h3>
						<div class="form-group ">
							
							<div class="controls">
								@foreach($dispute->service as $data)
								<div>Job Details : {{ $data->job_description}}</div>
								<div>Requested on : {{ $data->request_date}}</div>
								<div>Location: Lat: {{ $data->latitude}} , Lng:{{ $data->longitude}}</div>
								<div>From Time : {{ $data->from_time}}</div>
								<div>To Time : {{ $data->to_time}}</div>
								<div>Duration : {{ $data->job_duration_set_by_provider}}</div>
								<div>Cost : ${{ $data->expected_cost}}</div>
								<div>Team Size : {{ $data->team_size}}</div>
								
								<hr />
								@endforeach
								
							</div>
						</div>
</div>



<div class="row">
	 <h3>
       Customer Notification
    </h3>
						<div class="form-group ">
							
							<div class="controls">
								@foreach($dispute->customernotification as $data)
								<div>Date : {{ $data->date}}</div>
								<div>Details : {{ $data->details}}</div>
								<div>Distance : {{ $data->distance_from_provider}}</div>
								<div>Duration : {{ $data->duration_from_provider}}</div>
								<div>Location: Lat:{{ $data->notification_latitude}} , Lng:{{ $data->notification_longitude}}</div>
								<hr />
								@endforeach
								
							</div>
						</div>
</div>


<div class="row">
	 <h3>
       Provider Notification
    </h3>
						<div class="form-group ">
							
							<div class="controls">
								@foreach($dispute->providernotification as $data)
								<div>Date : {{ $data->date}}</div>
								<div>Details : {{ $data->details}}</div>
								<div>Location: Lat:{{ $data->notification_latitude}} , Lng:{{ $data->notification_longitude}}</div>
								<div>Distance : {{ $data->distance_from_customer}}</div>
								<div>Duration : {{ $data->duration_from_customer}}</div>
								<div>type : {{ $data->service_notice_status}}</div>
								<hr />
								@endforeach
								
							</div>
						</div>
</div>







					<div class="form-group">
						<div class="col-md-12">
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/dispute')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span> Back
							</button>
						
							
						</div>
					</div>


   
   </div>
</div>
@endsection

@section('scripts')
     
 @endsection       
