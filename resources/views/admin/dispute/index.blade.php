@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Dispute List
            
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Customer</th>
			<th>Provider</th>
			<th>Date</th>
			<th>Dispute To</th>
            <th>Status</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($dispute as $dis)
        <tr>
			<td>{{ $dis->customer->phone }}</td>
			<td>{{ $dis->provider->phone }}</td>
			<td>{{ $dis->date }}</td>
			<td>{{ $dis->dispute_to }}</td>
			<td>@if ($dis->status=='0') Dispute Raised @else Resolved @endif</td>
			<td>

@if ($dis->status=='1')
<span data-placement="top" data-toggle="tooltip" title="Active"><button class="btn btn-link btn-xs" data-title="Status" type="button" onclick='window.location.href="{{url('admin/dispute/statusupdate')}}/{{ $dis->id }}/1"'>
		
			<span class="glyphicon glyphicon-ok"></span>
		
			</button></span>
@else


			<span data-placement="top" data-toggle="tooltip" title="Inactive"><button class="btn btn-link btn-xs" data-title="Status" type="button" onclick='window.location.href="{{url('admin/dispute/statusupdate')}}/{{ $dis->id }}/0"'>
		
			<span class="fa fa-ban"></span>
		
			</button></span>
		@endif

			

    {{Form::open(array('method'=>'DELETE','id'=>'form'.$dis->id, 'route' => array('dispute.destroy', $dis->id)))}}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>
		$(document).ready(function(){
			$('#form{{$dis->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$dis->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'dispute';
								
							  }
							});
					}
				});
			});

		});
		</script>




		<span data-placement="top" data-toggle="tooltip" title="Profile">
				<button class="btn btn-link btn-xs" data-title="Profile" type="button" onclick='window.location.href="{{url('admin/customers/'.$dis->customer_id.'/edit/')}}"'>
				
					<span class="fa fa-user" aria-hidden="true"></span>
				
					</button></span>

				<span data-placement="top" data-toggle="tooltip" title="Dispute Details">
					<button class="btn btn-link btn-xs" data-title="Dispute Details" type="button" onclick='window.location.href="{{url('admin/dispute/details')}}/{{ $dis->id }}"'>
				
					<span class="fa fa-history" aria-hidden="true"></span>
				
					</button></span>	

    
			</td>

        </tr>

        
    @endforeach
<tr>

</tr>


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
