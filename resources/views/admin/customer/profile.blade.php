@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-8">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->
@if (isset($customer))
{!! Form::model($customer, array('url' => url('admin/customers/saveprofile') . '/' . $customer->id, 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@else
{!! Form::open(array('url' => url('admin/customers/saveprofile'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif
   
        
				<div class="row">   
					<div class="col-lg-6  col-md-6">   
						
						<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
							{!! Form::label('name', 'Name', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($customer->profile)
								{!! Form::text('name', $customer->profile->name, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@else
								{!! Form::text('name', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@endif
								<span class="help-block">{{ $errors->first('name', ':message') }}</span>
							</div>
						</div>
					</div>
					
					<div class="col-lg-6  col-md-6">
						<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
							{!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('phone', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								<span class="help-block">{{ $errors->first('phone', ':message') }}</span>
							</div>
						</div>
					</div>
				</div>




				<div class="row">   
					<div class="col-lg-6  col-md-6">   
						
						<div class="form-group  ">
							{!! Form::label('age', 'Age', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($customer->profile && $customer->profile->age)
								{!! Form::text('age', $customer->profile->age, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@else
								{!! Form::text('age', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@endif
								
							</div>
						</div>
					</div>
					
					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($customer->profile && $customer->profile->image)
								<img src="{{url($customer->profile->image)}}" width="100"/>
								@endif								
							</div>
						</div>
					</div>

				</div>



				<div class="row">   
					

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('gender', 'Gender', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($customer->profile && $customer->profile->gender)
								{!! Form::text('gender', $customer->profile->gender, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@else
								{!! Form::text('gender', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@endif
								
							</div>
						</div>
					</div>

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('highest_education', 'Highest Education', array('class' => 'control-label')) !!}
							<div class="controls">

								@if($customer->profile && $customer->profile->highest_education)
								{!! Form::text('highest_education', $customer->profile->highest_education, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@else
								{!! Form::text('highest_education', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@endif
								
								
								
							</div>
						</div>
					</div>


					
				</div>




				<div class="row">   
					

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('annual_income', 'Annual Income', array('class' => 'control-label')) !!}
							<div class="controls">

								@if($customer->profile &&  $customer->profile->annual_income)
								{!! Form::text('annual_income', $customer->profile->annual_income, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@else
								{!! Form::text('annual_income', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								@endif
								
								
							</div>
						</div>
					</div>

					


					
				</div>

       <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/customers')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span> &nbsp;&nbsp;Back 
							</button>
						
							<!--<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Save
							</button>-->
						</div>
					</div>
		
					
    

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
 @endsection       
