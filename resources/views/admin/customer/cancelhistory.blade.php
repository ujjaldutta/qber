@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> {{$title}} </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
	 
    <tr>
      <th>Customer</th>
      <th>Job ID</th>
      <th>Cancel Reason</th>
      <th>Cancel Date</th>
     
      <th>Penalty</th>
      <th>Comment</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach ($customer as $cancel)
  <tr>
    <td>{{ $cancel->phone }}</td>
    <td>{{ $cancel->job_id }}</td>
    <td>{{ $cancel->title }}</td>
    <td>{{ $cancel->cancel_date }}</td>
    
    <td> ${{ (float)$cancel->amount }} </td>
    <td> {{ $cancel->comment }} </td>
  </tr>
  @endforeach 
  <tr>
<td colspan="10">{{ $customer->appends(Request::except('page'))->links() }}</td>
</tr>
  
    </tbody>
  
</table>
<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/customers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 
