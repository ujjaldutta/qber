@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Customer List
           

						
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! url('admin/customers/create') !!}"
                       class="btn btn-sm  btn-primary"><span
                                class="glyphicon glyphicon-plus-sign"></span> Add New</a>
                </div>
            </div>
        </h3>

         @if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif
    </div>
<div class="col-lg-12 customer" >
<div class="fs-whatwg" id="fs-whatwg">
    <table id="table" class="table table-striped table-hover">
        <thead>

			<tr><th >Filter By :<br/>
			<select  class="active-blk" name="is_active" onchange="setFilter(this.value,'is_active')">
			<option value="">Filter By Statuas</option>
			<option value="1" {{ Request::input('is_active')=='1' ? 'selected' : '' }}>Active</option>
			<option value="0" {{ Request::input('is_active')=='0' ? 'selected' : '' }}>Blocked</option>
			</select>
			

			

			</th>

			
      

			<th colspan="2">Revenue Generated :<br/> 					
				<div class='input-group date' id='revenue_from_datepicker'>
					
					{!! Form::text('revenue_from_date', Request::input('revenue_from_date'), array('class'=>'form-control','id'=>'revenue_from_date','placeholder'=>'Form Date'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"> </span>
					</span>
				</div>

			

			

			</th>

			<th colspan="2">
		
			 					
				<div class='input-group input-group1 date' id='revenue_to_datepicker'>
					{!! Form::text('revenue_to_date', Request::input('revenue_to_date'), array('class'=>'form-control','id'=>'revenue_to_date','placeholder'=>'To Date'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			</th>
<th colspan="2"> From Amount

				<input type="text" name="from_income" id="from_income" class="form-control" value="{{ Request::input('from_income') }}"/>

				 To Amount

				<input type="text" name="to_income" id="to_income" class="form-control" value="{{ Request::input('to_income') }}"/>

				
			  </th>
			<th colspan="3">Rating: <br >
			  					
				<select name="rating_from" id="rating_from">
				<option value="From">From</option>
				@for($i=0;$i<=5;$i++)
				<option value="{{ $i}}" {{ Request::input('rating_from')==$i ? 'selected' : '' }}>{{ $i}}</option>
				@endfor
				</select>

			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
			<select name="rating_to"  id="rating_to">
				<option value="To">To</option>
				@for($i=0;$i<=5;$i++)
				<option value="{{ $i}}" {{ Request::input('rating_to')==$i ? 'selected' : '' }}>{{ $i}}</option>
				@endfor
				</select>
			

			</th>

			<th colspan="2">Appointment Area: <br >
			  					
				<select name="last_appo" id="last_appo" onchange="setFilter(this.value,'last_appo')">
				<option value="">Select</option>
				@foreach($perference as $rec)
				<option value="{{ $rec->id}}" {{ Request::input('last_appo')==$rec->id ? 'selected' : '' }}>{{ $rec->name}}</option>
				@endforeach
				</select>
			

			</th>


			<th colspan="10">

			<input type="button" onclick="window.location.href='{{url('admin/customers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&revenue_from_date='+$('#revenue_from_date').val()+'&revenue_to_date='+$('#revenue_to_date').val()+'&rating_from='+$('#rating_from').val()+'&rating_to='+$('#rating_to').val()+'&from_income='+$('#from_income').val()+'&to_income='+$('#to_income').val()" value="Search">
			</th>


			
			<tr><th colspan="2">Search By :<br/>
			<input type="text" value="{{Request::input('phone')}}" name="phone" id="phonefilter" placeholder="Phone" >

			

			</th>

			<th colspan="2"><br/> 
			<input type="text" value="{{Request::input('id')}}" name="id" id="idfilter" placeholder="ID">

			

			</th>

			<th colspan="2">
			<input type="text" value="{{Request::input('name')}}" name="name" id="namefilter" placeholder="Name">

			

			</th>
			<th colspan="2">
			<input type="text" value="{{Request::input('email')}}" name="email" id="emailfilter" placeholder="Email">

			

			</th>


			<th colspan="12">

			<input type="button" onclick="window.location.href='{{url('admin/customers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&phone='+$('#phonefilter').val()+'&id='+$('#idfilter').val()+'&name='+$('#namefilter').val()+'&email='+$('#emailfilter').val()" value="Search">
			</th>
</tr>
			
        <tr>

<th><p>ID</p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'id', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'id', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>
 <th> <p>Name</p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>          
<th> <p>Phone </p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>
<th> <p>Email</p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'email', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'email', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>



<th><p>Total <br> Requests</p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'servicecount', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'servicecount', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>
<th><p>Completed <br> Jobs</p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'servicecompletecount', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'servicecompletecount', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>
<th><p>Revenue <br> Generated</p>

	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>

</th>

<th><p>Cancel % of <br> Started Jobs</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'cancel_after_job_start_count', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'cancel_after_job_start_count', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>

<th><p>Cancel % jobs <br> at heading</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'cancel_after_heading', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'cancel_after_heading', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>
<th><p>Cancellation % of<br> confirmed Jobs</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'cancel_after_confirm', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'cancel_after_confirm', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>

<th><p>Disputed given per <br> confirmed job percent</p>


		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'dispute_given_after_confirm', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'dispute_given_after_confirm', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>

<th><p>Disputes recived per <br> confrimed job percent</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'dispute_received_after_confirm', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'dispute_received_after_confirm', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>
<th><p>Rating</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'rating', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'rating', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>

<th><p>Last Appointment <br> Area</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'preference_job_area', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'preference_job_area', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>
<th><p>Last <br> Request</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'last_request', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'last_request', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>


<th><p>Last Job Completed</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'last_job_completed', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'last_job_completed', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>


<th><p> Is Online</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'is_online', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'is_online', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>

<th><p>Sms Verified</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'sms_verified', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'sms_verified', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>


<th><p>Total Search</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'total_search_submit', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'total_search_submit', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>


<th><p>Last Search</p>

		<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'last_search', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'last_search', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
        
</th>



<th> <p>Is Active</p>
	<a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'is_active', 'order' => 'asc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/customers/') . '?' . http_build_query(['sort' => 'is_active', 'order' => 'desc','page'=>Request::input('page')])}}">
        <i class="fa fa-chevron-down"></i></a>
</th>


            <th class="col-md-2">Action</th>
            

        </tr>
        </thead>
        <tbody>
	@foreach ($customers as $user)
        <tr>
			<td>{{ $user->id }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->phone }}</td>
			<td>{{ $user->email }}</td>
			
			<td>{{ $user->servicecount }}</td>
			<td>{{ $user->servicecompletecount }}</td>
			<td>${{ (float)$user->amount }}</td>
			<td>{{ round($user->cancel_after_job_start_count) }}%</td>
			<td>{{ round($user->cancel_after_heading) }}%</td>
			<td>{{ round($user->cancel_after_confirm) }}%</td>
			<td>{{ round($user->dispute_given_after_confirm) }}%</td>
			<td>{{ round($user->dispute_received_after_confirm) }}%</td>
			<td>{{ number_format($user->rating,2,".","") }}</td>
			<td>{{ $user->preference_job_area }}</td>
			<td>{{ (int)$user->last_request }} Hours</td>
			<td>{{ (int)$user->last_job_completed }} Hours</td>
			<td>@if ($user->is_online=='1') Online @else Offline @endif</td>

			<td>@if ($user->sms_verified=='1') <a href="{{url('admin/customers/smsverify')}}/{{ $user->id }}/0">Verified</a> @else <a href="{{url('admin/customers/smsverify')}}/{{ $user->id }}/1">Set Verify</a> @endif</td>

			<td>{{ $user->total_search_submit }}</td>
			<td>{{ $user->last_search }}</td>
			
			<td>

				@if ($user->is_active=='1')
		<a style="text-decoration:none;" class="deactivate" href="javascript:void(0)" onclick='window.location.href="{{url('admin/customers/statusupdate')}}/{{ $user->id }}/1"'>
				
					De-Activate</a>
			@else

				<a style="text-decoration:none;" href="javascript:void(0)" onclick='window.location.href="{{url('admin/customers/statusupdate')}}/{{ $user->id }}/0"'>
				Activate</a>
				@endif

			</td>
			
			<td class="action-icon">
		
<span data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/customers')}}/{{ $user->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>



			<span data-placement="top" data-toggle="tooltip" title="Profile">
				<button class="btn btn-link btn-xs" data-title="Profile" type="button" onclick='window.location.href="{{url('admin/customers/profile')}}/{{ $user->id }}"'>
				
					<span class="fa fa-user" aria-hidden="true"></span>
				
					</button></span>

				<span data-placement="top" data-toggle="tooltip" title="Job History">
					<button class="btn btn-link btn-xs" data-title="History" type="button" onclick='window.location.href="{{url('admin/customers/jobhistory')}}/{{ $user->id }}"'>
				
					<span class="fa fa-history" aria-hidden="true"></span>
				
					</button></span>


				<span data-placement="top" data-toggle="tooltip" title="Cancel History">
					<button class="btn btn-link btn-xs" data-title="Cancel History" type="button" onclick='window.location.href="{{url('admin/customers/cancelhistory')}}/{{ $user->id }}"'>
				
					<span class="fa fa-list" aria-hidden="true"></span>
				
					</button></span>


			<span data-placement="top" data-toggle="tooltip" title="Rating History">
					<button class="btn btn-link btn-xs" data-title="Rating History" type="button" onclick='window.location.href="{{url('admin/customers/ratinghistory')}}/{{ $user->id }}"'>
				
					<span class="fa fa-star-o" aria-hidden="true"></span>
				
					</button></span>

					

			

		

<span>
			

    {{Form::open(array('method'=>'DELETE','id'=>'form'.$user->id, 'route' => array('customers.destroy', $user->id)))}}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>

$(".fs-whatwg").floatingScroll();

$(".fs-whatwg-cols").on("change", "input[type='checkbox']", function (e) {
    var $checkboxes = $(e.delegateTarget).find("input[type='checkbox']"),
        index;
    if ($checkboxes.filter(":checked").length) {
        index = $checkboxes.index(e.target);
        if (index > -1) {
            $("#fs-whatwg").find("th, td")
                .filter(":nth-child(" + (index + 2) + ")")
                .toggleClass("hidden");
        }
    } else {
        $checkboxes.prop("checked", true);
        $("#fs-whatwg").find("th.hidden, td.hidden").removeClass("hidden");
    }
    // Force scrollbar update programmatically
    $(".fs-whatwg").floatingScroll("update");
});







		$(document).ready(function(){
			$('#form{{$user->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$user->id}}').serialize(),
							  success: function(result) {

								  
								 //  if (result=='1')
								   location.reload(); 
								
							  }
							});
					}
				});
			});

		});
		</script>
</span>
    
			</td>

        </tr>

        
    @endforeach
<tr>
<td colspan="30">{{ $customers->appends(Request::except('page'))->links() }}</td>
</tr>


        </tbody>
    </table>
    <div class="floatingScroll-table"></div>
    </div>
</div>




    
@endsection

{{-- Scripts --}}
@section('scripts')
<script>
	 $(function () {
                $('#revenue_from_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#revenue_to_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
             
                
            });
          

	function setFilter(value,field){

		window.location.href='{{url('admin/customers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&'+field+'='+value;
	}

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
