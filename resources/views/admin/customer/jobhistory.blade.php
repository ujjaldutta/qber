@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> Job History </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
	  <tr><th colspan="3" width="50%">Filter By : Service Status

	  <select name="status" onchange="window.location.href='{{url('admin/customers/jobhistory/'.$id.'/') . '?'
			. http_build_query(['sort' => Request::input('sort'),
			'order' => Request::input('order'),'page'=>Request::input('page'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date')])}}'+'&status='+this.value"><option value="">All Status</option>
			@foreach($servicestatus as $status)
				<option value="{{$status->id}}" {{ Request::input('status')==$status->id ? 'selected' : '' }} >{{$status->name}}</option>
			 @endforeach
			</select>

	  </th>

	  	<th>
			From Date 


											
									<div class='input-group date' id='datetimepicker1'>
									
									{!! Form::text('from_date', Request::input('from_date'), array('class'=>'form-control','id'=>'from_date'))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
			</th>





			<th>
			To Date

											
									<div class='input-group date' id='datetimepicker2'>
									
									{!! Form::text('to_date', Request::input('to_date'), array('class'=>'form-control','id'=>'to_date','onChange'=>"alert('test')"))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>

			
								
			</th>
			<th colspan="3"><input type="button" onclick="filterbydate()" value="Filter Date"/></th>

			
	 </tr>
    <tr>
      <th>Customer</th>
      <th>Job</th>
      <th>Date</th>
      <th>Duration</th>
      <th>Amount</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach ($customer as $jobs)
  <tr>
    <td>{{ $jobs->phone }}</td>
    <td>{{ $jobs->job_id }}</td>
    <td>{{ $jobs->appdate }}</td>
    <td>{{ $jobs->job_duration_set_by_provider }}</td>
    <td> ${{ (float)$jobs->amount }} </td>
    <td> {{ $jobs->name }} </td>
  </tr>
  @endforeach 
  <tr>
<td colspan="10">{{ $customer->appends(Request::except('page'))->links() }}</td>
</tr>
  
    </tbody>
  
</table>
<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/customers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

<script>
 $(function () {
                $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#datetimepicker2').datetimepicker({
					format: 'YYYY-MM-DD HH:mm'
					
						});

              
                
            });
function filterbydate(){
var url='{{url('admin/customers/jobhistory/'.$id.'/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'status'=>Request::input('status')])}}'+'&from_date='+encodeURIComponent($("#from_date").val())+'&to_date='+encodeURIComponent($("#to_date").val());

window.location.href=url;


}
</script>


@endsection 
