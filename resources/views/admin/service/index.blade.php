@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Appointment List
            
        </h3>
    </div>  
	<div class="col-lg-12 appointment" style="overflow:auto;">
	<div class="fs-whatwg" id="fs-whatwg">
    <table id="table" class="table table-striped table-hover">
        <thead>
			<tr><th colspan="2">Filter By : Service Type
					<select style="width:130px;margin-right:50px;" name="servicetype" onchange="window.location.href='{{url('admin/appointments/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality'),'status'=>Request::input('status'),'minutes'=>Request::input('minutes')])}}'+'&servicetype='+this.value"><option value="">All Service Type</option>
					@foreach($servicetype as $type)
						<option value="{{$type->id}}" {{ Request::input('servicetype')==$type->id ? 'selected' : '' }} >{{$type->name}}</option>
					 @endforeach
					</select>
			<th colspan="2">
			
			Latest Record 
						<select style="width:140px;" name="minutes" onchange="window.location.href='{{url('admin/appointments/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality'),'status'=>Request::input('status')])}}'+'&minutes='+this.value"><option value="">All Records</option>
						<option value="15" {{ Request::input('minutes')=="15" ? 'selected' : '' }}>15 Mins</option>
						<option value="30" {{ Request::input('minutes')=="30" ? 'selected' : '' }}>30 Mins</option>
						<option value="60" {{ Request::input('minutes')=="60" ? 'selected' : '' }}>1 Hr</option>
						<option value="120" {{ Request::input('minutes')=="120" ? 'selected' : '' }}>2 Hrs</option>
						<option value="240" {{ Request::input('minutes')=="240" ? 'selected' : '' }}>4 Hrs</option>
					</select>
			</th>
			
			<th colspan="2">
				Nationality
			<select  style="width:140px;"  name="nationality" onchange="window.location.href='{{url('admin/appointments/') . '?'
			. http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),
			'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'minutes'=>Request::input('minutes'),
			'status'=>Request::input('status')])}}'+'&nationality='+this.value"><option value="">All Country</option>
			@foreach($nationality as $country)
				<option value="{{$country->code}}" {{ Request::input('nationality')==$country->code ? 'selected' : '' }} >{{$country->name}}</option>
			 @endforeach
			</select>
			</th>

			<th colspan="2">
				Price Range <br/>
			<input type="text"  placeholder="From"  size="10" name="price_from" id="price_from" value=" {{ Request::input('price_from') }}"/>

			</th>

			<th colspan="2">
			<input type="text" placeholder="To" size="10" name="price_to" id="price_to" value=" {{ Request::input('price_to') }}"/>

			<input type="button" value="Go" onclick="searchPrice()">
			
			
			</th>

			<th colspan="2">
				Job Area
				<select name="job_area" id="job_area" onchange="setFilter(this.value,'job_area')">
				<option value="">Select</option>
				@foreach($perference as $rec)
				<option value="{{ $rec->id}}" {{ Request::input('job_area')==$rec->id ? 'selected' : '' }}>{{ $rec->name}}</option>
				@endforeach
				</select>

			
			</th>


			<th colspan="2">Requested: 					
				<div class='input-group date' id='requested_from_datepicker'>
					
					{!! Form::text('requested_from_date', Request::input('requested_from_date'), array('class'=>'form-control','id'=>'requested_from_date','placeholder'=>'From Date'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			

			</th>

			<th colspan="2">
		
					
				<div class='input-group date' id='requested_to_datepicker'>
					
					{!! Form::text('requested_to_date', Request::input('requested_to_date'), array('class'=>'form-control','id'=>'requested_to_date','placeholder'=>'To Date'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			</th>
			<th><input type="button" value="Go" onclick="searchDate()">
			</th>
			
			<th colspan="12">
			Status
			<select name="status" onchange="window.location.href='{{url('admin/appointments/') . '?'
			. http_build_query(['sort' => Request::input('sort'),
			'order' => Request::input('order'),'page'=>Request::input('page'),
			'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes')])}}'+'&status='+this.value"><option value="">All Status</option>
			@foreach($servicestatus as $status)
				<option value="{{$status->id}}" {{ Request::input('status')==$status->id ? 'selected' : '' }} >{{$status->name}}</option>
			 @endforeach
			</select>


			</th></tr>


<tr><th colspan="2">Search By : <br />
			<input type="text" placeholder="Phone" value="{{Request::input('phone')}}" name="phone" id="phonefilter" >

			

			</th>

			<th colspan="2">
			<input type="text" placeholder="Job ID" value="{{Request::input('id')}}" name="id" id="idfilter" >

			

			</th>

			<th colspan="2">
			<input type="text" placeholder="Customer ID"  value="{{Request::input('userid')}}" name="userid" id="useridfilter" >

			</th>
			


			<th colspan="12">

			<input type="button" onclick="window.location.href='{{url('admin/appointments/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&phone='+$('#phonefilter').val()+'&id='+$('#idfilter').val()+'&userid='+$('#useridfilter').val()" value="Search">
			</th>
</tr>




			
        <tr>
           
			<th > <p>Id</p>
				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'id', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'id', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>

			</th>
			<th><p>Service Type</p></th>

			<th><p>Status</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'service_status_id', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'service_status_id', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>
           
            <th><p>Price At <br>Completion</p>

				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'completed_amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'completed_amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>

            <th><p>Price at <br>Heading</p>

				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'heading_price', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'heading_price', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>


            <th><p>Price at <br>Job Start</p>

				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'start_price', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'start_price', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>

            <th><p>Current<br> Price</p>

				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'current_price', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'current_price', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>

            <th><p>Set Job <br>Duration</p>

				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'job_duration_set_by_provider', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'job_duration_set_by_provider', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>


            <th><p>Total Job <br>Duration</p>

				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'total_job_duration', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'total_job_duration', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>


            

            
            <th><p>Requested Arrival<br> Begin Time</p>
				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'from_time', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'from_time', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>

            <th><p>Requested Arrival<br> End Time</p>
				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'to_time', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'to_time', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>

            

             <th><p>Estimated Travel<br> time At Heading</p>
				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'estimated_traveltime_heading', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'estimated_traveltime_heading', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>


            <th><p>Total Travel<br> Time</p>
				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'total_traveltime', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'total_traveltime', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>

            <th><p>Estimated Travel Time<br> from Current Location</p>
				<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'estimated_traveltime_currentlocation', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'estimated_traveltime_currentlocation', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>
            
            <th><p>Provider Name</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'provider_name', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'provider_name', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>

            <th><p>Provider ID</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'provider_id', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'provider_id', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>

            <th><p>Provider Phone</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'provider_phone', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'provider_phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>


            <th><p>Customer Name</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'customer_name', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'customer_name', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>

            <th><p>Customer ID</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'customer_id', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'customer_id', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>

            <th><p>Customer Phone</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'customer_phone', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'customer_phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>


            <th><p>Job Requested <br> Time</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'request_date', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'request_date', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>


            <th><p>Job Area</p>
			<a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'preference_job_area', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('admin/appointments/') . '?' . http_build_query(['sort' => 'preference_job_area', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>


            
            
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($services as $service)
        <tr>
			
			<td>{{ $service->job_id }}</td>
			<td>{{ $service->service_name }}</td>
			<td>{{ strtoupper($service->status_name) }}</td>
			<td>${{ (float)$service->completed_amount }}</td>
			<td>${{ (float)$service->heading_price }}</td>
			<td>${{ (float)$service->start_price }}</td>
			<td>${{ (float)$service->current_price }}</td>
			<td>{{ $service->job_duration_set_by_provider }} Mins</td>
			<td>{{ $service->total_job_duration }} Mins</td>
			<td>{{ $service->from_time }}</td>
			<td>{{ $service->to_time }}</td>
			<td>{{ $service->estimated_traveltime_heading }} Mins</td>
			<td>{{ $service->total_traveltime }} Mins</td>
			<td>{{ $service->estimated_traveltime_currentlocation }} Mins</td>
			<td>{{ $service->provider_name }}</td>
			<td>{{ $service->provider_id }}</td>
			<td>{{ $service->provider_phone }}</td>
			<td>{{ $service->customer_name }}</td>
			<td>{{ $service->customer_id }}</td>
			<td>{{ $service->customer_phone }}</td>
			<td>{{ $service->request_date }}</td>
			<td>{{ $service->preference_job_area }}</td>
			
			<td class="action-icons">
		
<span data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/appointments')}}/{{ $service->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>

		
			


			

    {{Form::open(array('method'=>'DELETE','id'=>'form'.$service->id, 'route' => array('appointments.destroy', $service->id)))}}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>
		$(document).ready(function(){
			$('#form{{$service->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$service->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'appointments';
								
							  }
							});
					}
				});
			});

		});
		</script>

    
			</td>

        </tr>

        
    @endforeach
<tr>
<td colspan="15">{{ $services->appends(Request::except('page'))->links() }}</td>
</tr>


        </tbody>
    </table>
    <div class="floatingScroll-table"></div>
</div>
	</div>



    
@endsection

{{-- Scripts --}}
@section('scripts')
<script>

$(".fs-whatwg").floatingScroll();


$(".fs-whatwg-cols").on("change", "input[type='checkbox']", function (e) {
    var $checkboxes = $(e.delegateTarget).find("input[type='checkbox']"),
        index;
    if ($checkboxes.filter(":checked").length) {
        index = $checkboxes.index(e.target);
        if (index > -1) {
            $("#fs-whatwg").find("th, td")
                .filter(":nth-child(" + (index + 2) + ")")
                .toggleClass("hidden");
        }
    } else {
        $checkboxes.prop("checked", true);
        $("#fs-whatwg").find("th.hidden, td.hidden").removeClass("hidden");
    }

    $(".fs-whatwg").floatingScroll("update");
});




	 $(function () {
                $('#requested_from_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#requested_to_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});

              
                
            });

function searchDate(){
		window.location.href='{{url('admin/appointments/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&requested_from_date='+$("#requested_from_date").val()+'&requested_to_date='+$("#requested_to_date").val();

	}
            
	function searchPrice(){
		window.location.href='{{url('admin/appointments/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&price_from='+$("#price_from").val()+'&price_to='+$("#price_to").val();

	}

	function setFilter(value,field){

		window.location.href='{{url('admin/appointments/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&'+field+'='+value;
	}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
