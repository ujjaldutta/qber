@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->
@if (isset($service))
{!! Form::model($service, array('url' => url('admin/appointments') . '/' . $service->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
@else
{!! Form::open(array('url' => url('admin/appointments'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif
   
        
        <div class="form-group ">
            {!! Form::label('type_id', 'Service type', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::select('type_id', $servicetype) !!}
                <span class="help-block">{{ $errors->first('type_id', ':message') }}</span>
            </div>
        </div>
        
        <div class="form-group  {{ $errors->has('customer_id') ? 'has-error' : '' }}">
            {!! Form::label('customer', 'Customer Id', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_id', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('customer_id', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('provider_id') ? 'has-error' : '' }}">
            {!! Form::label('provider', 'Provider Id', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_id', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_id', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('booking_slot_id') ? 'has-error' : '' }}">
            {!! Form::label('booking_slot_id', 'Booking Slot Id', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('booking_slot_id', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('booking_slot_id', ':message') }}</span>
            </div>
        </div>
        

        <div class="form-group  {{ $errors->has('job_description') ? 'has-error' : '' }}">
            {!! Form::label('job_description', 'Job Description', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('job_description', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('job_description', ':message') }}</span>
            </div>
        </div>


        <div class="form-group  {{ $errors->has('request_date') ? 'has-error' : '' }}">
            {!! Form::label('request_date', 'Request Date', array('class' => 'control-label')) !!}
            <div class='input-group date' id='datetimepicker1'>
                {!! Form::text('request_date', null, array('class' => 'form-control','required' => 'required')) !!}
					<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>

               
                
            </div>
             <span class="help-block">{{ $errors->first('request_date', ':message') }}</span>
        </div>


        <div class="form-group  {{ $errors->has('latitude') ? 'has-error' : '' }}">
            {!! Form::label('latitude', 'Latitude', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('latitude', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('latitude', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('longitude') ? 'has-error' : '' }}">
            {!! Form::label('longitude', 'Longitude', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('longitude', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('longitude', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('from_time') ? 'has-error' : '' }}">
            {!! Form::label('from_time', 'Expected From Time', array('class' => 'control-label')) !!}
            <div class='input-group date' id='datetimepicker2'>
                {!! Form::text('from_time', null, array('class' => 'form-control','required' => 'required')) !!}
				<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
              
            </div>
              <span class="help-block">{{ $errors->first('from_time', ':message') }}</span>
        </div>

        <div class="form-group  {{ $errors->has('to_time') ? 'has-error' : '' }}">
            {!! Form::label('to_time', 'Expected To Time', array('class' => 'control-label')) !!}
            <div class='input-group date' id='datetimepicker3'>
                {!! Form::text('to_time', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
            </div>
            <span class="help-block">{{ $errors->first('to_time', ':message') }}</span>
        </div>

        <div class="form-group  {{ $errors->has('nationality') ? 'has-error' : '' }}">
            {!! Form::label('nationality', 'Nationality', array('class' => 'control-label')) !!}
            <div class="controls">
               
                {!! Form::select('nationality', $country,array("id"=>$service->nationality,"countryname"=>"df"),array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('nationality', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('team_size') ? 'has-error' : '' }}">
            {!! Form::label('team_size', 'Team Size', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('team_size', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('team_size', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('job_duration_set_by_provider') ? 'has-error' : '' }}">
            {!! Form::label('job_duration_set_by_provider', 'Job Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('job_duration_set_by_provider', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('job_duration_set_by_provider', ':message') }}</span>
            </div>
        </div>

		<div class="form-group  {{ $errors->has('expected_cost') ? 'has-error' : '' }}">
            {!! Form::label('expected_cost', 'Expected Cost', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('expected_cost', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('expected_cost', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('service_status_id') ? 'has-error' : '' }}">
            {!! Form::label('service_status_id', 'Service Status Id', array('class' => 'control-label')) !!}
            <div class="controls">
               
                {!! Form::select('service_status_id', $status,array("id"=>$service->service_status_id,"name"=>"status"),
					array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('service_status_id', ':message') }}</span>
            </div>
        </div>






        <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/appointments')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>
						</div>
					</div>

					
    

     {!! Form::close() !!}

   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
				$('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
				$('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
				$('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
				
                $("#roles").select2()
            });
        </script>
 @endsection       
