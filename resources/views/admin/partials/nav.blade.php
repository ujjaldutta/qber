<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{url('admin/dashboard')}}"></a>
    </div>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
               
                <li>
                    <a href="{{url('admin/dashboard')}}">
                        <i class="fa fa-dashboard fa-fw"></i> Dashboard
                    </a>
                </li>
               
                <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Customers
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

						@if(Helper::checkpermission('customerindex'))
                        <li>
                            <a  class="pad15" href="{{url('admin/customers')}}">
                                <i class="glyphicon glyphicon-list"></i>  Customers List
                            </a>
                        </li>
						@endif
						@if(Helper::checkpermission('customersendMessage'))
                        <li>
                            <a  class="pad15" href="{{url('admin/customers/send-message')}}">
                                <i class="glyphicon glyphicon-list"></i>  Send Message
                            </a>
                        </li>
                        @endif

                       
                    </ul>
                </li>




                 <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Providers
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

						

                        @if(Helper::checkpermission('providerindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/providers')}}">
                                <i class="glyphicon glyphicon-list"></i> Providers List
                            </a>
                        </li>
                         @endif
                         
						 @if(Helper::checkpermission('provideractiveProviders'))
                        <li>
                            <a class="pad15" href="{{url('admin/providers/active-on-map')}}">
                                <i class="glyphicon glyphicon-list"></i> Provider Map
                            </a>
                        </li>
                        @endif
                        
						 @if(Helper::checkpermission('providersattleBalance'))
                        <li>
                            <a class="pad15" href="{{url('admin/providers/sattle-balance')}}">
                                <i class="glyphicon glyphicon-list"></i> Sattle Balance
                            </a>
                        </li>
						 @endif
						  @if(Helper::checkpermission('disputeindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/dispute')}}">
                                <i class="glyphicon glyphicon-list"></i> Dispute
                            </a>
                        </li>
						@endif
						@if(Helper::checkpermission('providersendMessage'))
                         <li>
                            <a  class="pad15" href="{{url('admin/providers/send-message')}}">
                                <i class="glyphicon glyphicon-list"></i>  Send Message
                            </a>
                        </li>
                        @endif
                        
                    </ul>
                </li>


                 <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Service
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

						@if(Helper::checkpermission('serviceindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/appointments')}}">
                                <i class="glyphicon glyphicon-list"></i> Appointment List
                            </a>
                        </li>
                        @endif
                        
						@if(Helper::checkpermission('servicetypeindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/servicetype')}}">
                                <i class="glyphicon glyphicon-list"></i> Service Type
                            </a>
                        </li>
                        @endif


                        @if(Helper::checkpermission('serviceoptionindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/service-option')}}">
                                <i class="glyphicon glyphicon-list"></i> Service Option
                            </a>
                        </li>
                        @endif


                        @if(Helper::checkpermission('servicestatus'))
                       <!-- <li>
                            <a href="{{url('admin/servicestatus')}}">
                                <i class="glyphicon glyphicon-list"></i> Service Status
                            </a>
                        </li>-->
                        @endif

                         @if(Helper::checkpermission('servicesettingsindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/servicesettings')}}">
                                <i class="glyphicon glyphicon-list"></i> Service Settings
                            </a>
                        </li>
                        @endif

                        @if(Helper::checkpermission('preferedlocationindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/locations')}}">
                                <i class="glyphicon glyphicon-list"></i> Prefered Location
                            </a>
                        </li>
                        @endif

                      
                    </ul>
                </li>


                <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Contents
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse ">

						@if(Helper::checkpermission('qberhelpindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/qberhelp')}}">
                                <i class="glyphicon glyphicon-list"></i> Help Management
                            </a>
                        </li>
                        @endif

                        @if(Helper::checkpermission('cancelreasonindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/cancelreason')}}">
                                <i class="glyphicon glyphicon-list"></i> Cancel Reason
                            </a>
                        </li>
                        @endif

                      
                    </ul>
                </li>


                <li>
					
                    <a  href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Settings
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

						@if(Helper::checkpermission('subadminindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/subadmin')}}">
                                <i class="glyphicon glyphicon-list"></i>  Sub Admins
                            </a>
                        </li>
                        @endif

                        @if(Helper::checkpermission('roleindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/role')}}">
                                <i class="glyphicon glyphicon-list"></i>  Role
                            </a>
                        </li>
                        @endif
                         @if(Helper::checkpermission('companyindex'))
                        <li>
                            <a class="pad15" href="{{url('admin/company')}}">
                                <i class="glyphicon glyphicon-list"></i>  Company
                            </a>
                        </li>
                        @endif


                         @if(Helper::checkpermission('SMSLog'))
                        <li>
                            <a class="pad15" href="{{url('admin/smslog')}}">
                                <i class="glyphicon glyphicon-list"></i>  SMS Log
                            </a>
                        </li>
                        @endif

                      
                        <li>
                             <a href="{{url('admin/profile')}}/{{ Auth::user()->id }}/edit">
                                <i class="glyphicon glyphicon-bullhorn"></i> Admin Profile
                            </a>
                        </li>
                        
                    </ul>
                </li>
               
            
                <li>
                    <a href="{{ url('admin/auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
