@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-12">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif


{!! Form::model($settings, array('url' => url('admin/servicesettings') . '/' . $settings->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}

   
        
     <div class="row">   
    <div class="col-lg-6  col-md-6">
    <div class="form-group  {{ $errors->has('payment_commission_percent') ? 'has-error' : '' }}">
            {!! Form::label('payment_commission_percent', 'Payment Commission Percent', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('payment_commission_percent', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('payment_commission_percent', ':message') }}</span>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6  col-md-6">
	<div class="form-group  {{ $errors->has('service_duration') ? 'has-error' : '' }}">
            {!! Form::label('service_duration', 'Minimum Service Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('service_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('service_duration', ':message') }}</span>
            </div>
        </div>
	</div>
  </div>

   <div class="row">   
    <div class="col-lg-6  col-md-6">  
        <div class="form-group  {{ $errors->has('arrival_duration') ? 'has-error' : '' }}">
            {!! Form::label('arrival_duration', 'Minimum Arrival Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('arrival_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('arrival_duration', ':message') }}</span>
            </div>
        </div>

         </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('buffer_duration') ? 'has-error' : '' }}">
            {!! Form::label('buffer_duration', 'Buffer Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('buffer_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('buffer_duration', ':message') }}</span>
            </div>
        </div>
</div>
  </div>


    <div class="row">   
    <div class="col-lg-6  col-md-6">  
        <div class="form-group  {{ $errors->has('provider_display_limit') ? 'has-error' : '' }}">
            {!! Form::label('provider_display_limit', 'Number Of Providers To Show On Map', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_display_limit', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_display_limit', ':message') }}</span>
            </div>
        </div>

         </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('provider_max_due_amont') ? 'has-error' : '' }}">
            {!! Form::label('provider_max_due_amont', 'Max Credit Amount For Providers', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_max_due_amont', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_max_due_amont', ':message') }}</span>
            </div>
        </div>
</div>
  </div>



<div class="row">   
    <div class="col-lg-6  col-md-6">  
        <div class="form-group  {{ $errors->has('allowed_start_time') ? 'has-error' : '' }}">
            {!! Form::label('allowed_start_time', 'Min Start Hour For Provider In A Day', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('allowed_start_time', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('allowed_start_time', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('allowed_end_time') ? 'has-error' : '' }}">
            {!! Form::label('allowed_end_time', 'Max End Hour For Provider In A Day', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('allowed_end_time', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('allowed_end_time', ':message') }}</span>
            </div>
        </div>
</div>
  </div>

  

<div class="row">   
    <div class="col-lg-6  col-md-6"> 
        <div class="form-group  {{ $errors->has('customer_no_penalty_duration') ? 'has-error' : '' }}">
            {!! Form::label('customer_no_penalty_duration', 'Customer No Penalty Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_no_penalty_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_no_penalty_duration', ':message') }}</span>
            </div>
        </div>

         </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('customer_moderate_penalty_duration') ? 'has-error' : '' }}">
            {!! Form::label('customer_moderate_penalty_duration', 'Customer Moderate Penalty Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_moderate_penalty_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_moderate_penalty_duration', ':message') }}</span>
            </div>
        </div>
</div>
  </div>

  
<div class="row">   
    <div class="col-lg-6  col-md-6">
        <div class="form-group  {{ $errors->has('customer_higher_penalty_duration') ? 'has-error' : '' }}">
            {!! Form::label('customer_higher_penalty_duration', 'Customer Higher Penalty Duration', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_higher_penalty_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_higher_penalty_duration', ':message') }}</span>
            </div>
        </div>
		 </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('customer_moderate_penalty_amount') ? 'has-error' : '' }}">
            {!! Form::label('customer_moderate_penalty_amount', 'Customer Moderate Penalty Amount', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_moderate_penalty_amount', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_moderate_penalty_amount', ':message') }}</span>
            </div>
        </div>
</div>
  </div>


<div class="row">   
    <div class="col-lg-6  col-md-6">
        <div class="form-group  {{ $errors->has('customer_higher_penalty_amount') ? 'has-error' : '' }}">
            {!! Form::label('customer_higher_penalty_amount', 'Customer Higher Penalty Amount', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_higher_penalty_amount', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_higher_penalty_amount', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('customer_max_cancel_duration') ? 'has-error' : '' }}">
            {!! Form::label('customer_max_cancel_duration', 'Max Cancel Duration - Customer', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_max_cancel_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_max_cancel_duration', ':message') }}</span>
            </div>
        </div>
</div>
  </div>

<div class="row">   
    <div class="col-lg-6  col-md-6">  
        <div class="form-group  {{ $errors->has('customer_max_cancel_unit') ? 'has-error' : '' }}">
            {!! Form::label('customer_max_cancel_unit', 'Max Number Of Cancel Time - Customer', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_max_cancel_unit', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_max_cancel_unit', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">

         <div class="form-group  {{ $errors->has('provider_no_penalty_duration') ? 'has-error' : '' }}">
            {!! Form::label('provider_no_penalty_duration', 'Min No Penalty Duration - Provider', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_no_penalty_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_no_penalty_duration', ':message') }}</span>
            </div>
        </div>
</div>
  </div>

<div class="row">   
    <div class="col-lg-6  col-md-6">    
        <div class="form-group  {{ $errors->has('provider_moderate_penalty_duration') ? 'has-error' : '' }}">
            {!! Form::label('provider_moderate_penalty_duration', 'Min Moderate Penalty Duration - Provider', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_moderate_penalty_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_moderate_penalty_duration', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">

        <div class="form-group  {{ $errors->has('provider_higher_penalty_duration') ? 'has-error' : '' }}">
            {!! Form::label('provider_higher_penalty_duration', 'Min Higher Penalty Duration - Provider', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_higher_penalty_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_higher_penalty_duration', ':message') }}</span>
            </div>
        </div>
</div>
  </div>

<div class="row">   
    <div class="col-lg-6  col-md-6">     
        <div class="form-group  {{ $errors->has('provider_moderate_penalty_amount') ? 'has-error' : '' }}">
            {!! Form::label('provider_moderate_penalty_amount', 'Moderate Penalty Amount - Provider', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_moderate_penalty_amount', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_moderate_penalty_amount', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">


        <div class="form-group  {{ $errors->has('provider_higher_penalty_amount') ? 'has-error' : '' }}">
            {!! Form::label('provider_higher_penalty_amount', 'Higher Penalty Amount - Provider', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_higher_penalty_amount', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_higher_penalty_amount', ':message') }}</span>
            </div>
        </div>
</div>
  </div>


  
<div class="row">   
    <div class="col-lg-6  col-md-6"> 
        <div class="form-group  {{ $errors->has('customer_min_cancel_duration') ? 'has-error' : '' }}">
            {!! Form::label('customer_min_cancel_duration', 'Min Cancel Duration - Customer', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('customer_min_cancel_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('customer_min_cancel_duration', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">
        
        <div class="form-group  {{ $errors->has('provider_min_cancel_duration') ? 'has-error' : '' }}">
            {!! Form::label('provider_min_cancel_duration', 'Min Cancel Duration - Provider', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('provider_min_cancel_duration', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('provider_min_cancel_duration', ':message') }}</span>
            </div>
        </div>
</div>
  </div>





  <div class="row">   
    <div class="col-lg-6  col-md-6"> 
        <div class="form-group  {{ $errors->has('notice') ? 'has-error' : '' }}">
            {!! Form::label('notice', 'Notice', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::textarea('notice', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('notice', ':message') }}</span>
            </div>
        </div>
         </div>
    
    <div class="col-lg-6  col-md-6">
        
       

        <div class="form-group  {{ $errors->has('display_notice') ? 'has-error' : '' }}">
            {!! Form::label('display_notice', 'Display Notice', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::checkbox('display_notice', 1) !!}
                
                <span class="help-block">{{ $errors->first('display_notice', ':message') }}</span>
            </div>
        </div>
        
</div>
  </div>


      


        <div class="form-group">
						<div class="col-md-12">
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/servicesettings')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span> Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Save
							</button>
						</div>
					</div>

					
    

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#type").select2()
            });
        </script>
 @endsection       
