@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Service Settings List
            
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Service Type</th>
            <th>Code</th>
            <th>Value</th>
            <th>Description</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>
	@foreach ($settings as $set)
        <tr>
			<td>{{ $set->servicetype->name }}</td>
			<td>{{ $set->code }}</td>
			<td>{{ $set->value }}</td>
			<td>{{ $set->description }}</td>
			
			<td>
		
<span data-placement="top" data-toggle="tooltip" title="Active"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/servicesettings')}}/{{ $set->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>


	

    
			</td>

        </tr>

        
    @endforeach
<tr>

</tr>


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
