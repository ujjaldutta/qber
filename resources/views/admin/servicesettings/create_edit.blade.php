@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-12">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif


{!! Form::model($settings, array('url' => url('admin/servicesettings') . '/' . $settings->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}

   
        
     <div class="row">   
    <div class="col-lg-6  col-md-6">
    <div class="form-group  {{ $errors->has('code') ? 'has-error' : '' }}">
            {!! Form::label('code', 'Code', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('code', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('code', ':message') }}</span>
            </div>
        </div>
    </div>
    
    <div class="col-lg-6  col-md-6">
	<div class="form-group  {{ $errors->has('value') ? 'has-error' : '' }}">
            {!! Form::label('value', 'Value', array('class' => 'control-label')) !!}
            <div class="controls">


				@if($settings->code=='allowed_end_time' || $settings->code=='allowed_start_time')

				<div class="col-lg-6">
				<div class='input-group date' id='datetimepicker1'>
                {!! Form::text('value', null, array('class' => 'form-control','required' => 'required')) !!}
					<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>

               	</div>
               	</div>

				@else

				
				
                {!! Form::text('value', null, array('class' => 'form-control','maxlength'=>100,'required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('value', ':message') }}</span>

				@endif

				

                
            </div>
        </div>
	</div>
  

  </div>

 <div class="row">   
   <div class="col-lg-6  col-md-6">
    <div class="form-group  {{ $errors->has('description') ? 'has-error' : '' }}">
            {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('description', null, array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('description', ':message') }}</span>
            </div>
        </div>
    </div>
  </div>
      


        <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/servicesettings')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Back
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>
						</div>
					</div>


     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
        
            $(function () {
				$('#datetimepicker1').datetimepicker({format: 'HH:mm:ss'});
				
            });
        </script>
        </script>
 @endsection       
