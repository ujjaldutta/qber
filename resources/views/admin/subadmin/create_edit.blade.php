@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->
@if (isset($admin))
{!! Form::model($admin, array('url' => url('admin/subadmin') . '/' . $admin->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
@else
{!! Form::open(array('url' => url('admin/subadmin'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif
   
        
       
        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::email('email', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('email', ':message') }}</span>
            </div>
        </div>
        <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
            {!! Form::label('password', 'Password', array('class' => 'control-label')) !!}
            <div class="controls">
				@if (isset($admin))
                {!! Form::password('password', array('class' => 'form-control')) !!}
                @else
				{!! Form::password('password', array('class' => 'form-control','required' => 'required')) !!}
				@endif
                
                <span class="help-block">{{ $errors->first('password', ':message') }}</span>
            </div>
        </div>
        <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            {!! Form::label('password_confirmation', 'Confirm Password', array('class' => 'control-label')) !!}
            <div class="controls">
				@if (isset($admin))
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
				 @else
				{!! Form::password('password_confirmation', array('class' => 'form-control','required' => 'required')) !!}
				@endif
                
                <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
            </div>
        </div>

        <div class="form-group ">
            {!! Form::label('role_id', 'Role', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::select('role_id', $role_id) !!}
                <span class="help-block">{{ $errors->first('role_id', ':message') }}</span>
            </div>
        </div>
        <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/subadmin')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>
						</div>
					</div>

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
 @endsection       
