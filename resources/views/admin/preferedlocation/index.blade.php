@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Preferred Locations

            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! url('admin/locations/create') !!}"
                       class="btn btn-sm  btn-primary"><span
                                class="glyphicon glyphicon-plus-sign"></span> Add New</a>

					<input name="upload" id="upload" type="file" style="display:none;"/>
                     <button class="btn btn-sm  btn-primary" onclick="importFile()"><span class="glyphicon glyphicon-plus-sign" ></span> Import KML</button>
                </div>
            </div>
            
        </h3>
    </div>

<div id="loader" style="display:none"><img src="{{ asset('css/images/loading.gif') }}"></div>
    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
			<th>ID</th>
            <th>Locations</th>
            <th>details</th>
            
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($locations as $location)
        <tr>
			<td>{{ $location->id }}</td>
			<td>{{ $location->name }}</td>
			<td>{{ $location->polygon }}</td>
			
			
			<td width="6%">
		
<span data-placement="top" data-toggle="tooltip" title="Active"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/locations')}}/{{ $location->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>

	{{Form::open(array('method'=>'DELETE','id'=>'form'.$location->id, 'route' => array('locations.destroy', $location->id)))}}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>
		$(document).ready(function(){
			$('#form{{$location->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$location->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'locations';
								
							  }
							});
					}
				});
			});

		});
		</script>

    
			</td>

        </tr>

        
    @endforeach
<tr>

</tr>
<tr>
    <td colspan="30">{{ $locations->appends(Request::except('page'))->links() }}</td>
  </tr>

        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')
<script>
function importFile(){
$("#upload:hidden").trigger('click');
}


$("#upload").on('change',function(){

		var myFormData = new FormData();
		myFormData.append('kmlFile', $("#upload").prop('files')[0]);

		myFormData.append('_token', '{{ csrf_token()}}');
		$("#loader").show();
		$.ajax({
		  url: '{!! url("admin/locations/import") !!}',
		  type: 'POST',
		 enctype: 'multipart/form-data',
			processData: false,  // tell jQuery not to process the data
			contentType: false,   // tell jQuery not to set contentType
			dataType: "json",
		  data: myFormData,
		  success: function(data, textStatus, jqXHR)
			{
				$("#loader").hide();
			}
		});

	});

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
