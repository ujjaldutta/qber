@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')

 <h3>
        {{$title}}

        

       
    </h3>

    	<!-- ./ tabs -->
					@if (isset($location))
					{!! Form::model($location, array('url' => url('admin/locations') . '/' . $location->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
					@else
					{!! Form::open(array('url' => url('admin/locations'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
					@endif

					@if (isset($location))
					{!! Form::hidden('vertices', $location->polygon, array('class' => 'form-control','id'=>'vertices','required' => 'required')) !!}
					@else
					{!! Form::hidden('vertices', null, array('class' => 'form-control','id'=>'vertices','required' => 'required')) !!}
					@endif
					
<div class="row">
			<div class="col-lg-4 col-md-6">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

				

						<div class="form-group  ">
							{!! Form::label('name', 'Prefered Location Name', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('name', null, array('class' => 'form-control','id'=>'location_name','required' => 'required')) !!}
							
							</div>
						</div>

						

					

   
   </div>
</div>
<div class="row">
				<div class="col-lg-12 col-md-6">
							<div id="map_canvas"></div>
				</div>
</div>
<div class="row">
<div class="col-lg-4 col-md-6">
<div class="form-group" style="padding:20px;">
						<div class="col-md-12">
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/locations')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span> Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Save
							</button>
						</div>
					</div>
</div>
</div>
						
{!! Form::close() !!}










    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>


<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k&callback=initialize&libraries=drawing">
    </script>
 <script>
 var polygonBounds = [];
function initialize() {
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
			@if (isset($location))
			center: {lat: {!! $location->polygon[0]->latitude !!}, lng: {!! $location->polygon[0]->longitude !!} },
			@else
			center: {lat: 25.281039763035846, lng: 51.53070688247681 },
			@endif
          zoom: 14
        });




        var drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
           // drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
            drawingModes: ['polygon']
          },
          markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
          circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
          }

        });
        drawingManager.setMap(map);
@if (isset($location))
 var triangleCoords = [@foreach($location->polygon as $codata){lat: {{$codata->latitude}}, lng: {{$codata->longitude}}},@endforeach];

 /*triangleCoords = [{lat: 25.391179281675832, lng: 51.52587890625},
          {lat: 25.23600069980047, lng: 51.582183837890625},
          {lat: 25.140311914680755, lng: 51.35833740234375},
          {lat: 25.48790982559232, lng: 51.296539306640625},];
   */
   
        //console.log(triangleCoords);
		 var bermudaTriangle = new google.maps.Polygon({
          paths: triangleCoords,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35
        });
        bermudaTriangle.setMap(map);
			
@endif
        google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                var newShape = event.overlay;
                newShape.type = event.type;
            });

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
                //$('#vertices').val(event.overlay.getPath().getArray());
                //var polygonBounds=event.overlay.getPath();

                var vertices = event.overlay.getPath();

				
				   for (var i = 0; i < vertices.length; i++) {
				   var xy = vertices.getAt(i);
				   polygonBounds.push({ lat: xy.lat(), lon: xy.lng() });
				  }

               $("#vertices").val(JSON.stringify(polygonBounds));
               
            });

            
      }

      function savearea(){

			 $.ajax({
					  url: '{!! url("admin/polygon-details") !!}',
					   type: 'get',
					  data: {vertices:polygonBounds,name:$("#location_name").val()},
					 
					  success: function(data) {
						console.log(data)
					  },
					 
				   });

	  }


   
    </script>

@endsection
<style>
 #map_canvas {
        height: 100%;
      }

</style>



