@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->
@if (isset($type))
{!! Form::model($type, array('url' => url('admin/servicetype') . '/' . $type->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
@else
{!! Form::open(array('url' => url('admin/servicetype'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif
   
        
       
        <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('name', 'Service Type Name', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('name', null, array('class' => 'form-control','required' => 'required','readonly' => 'readonly')) !!}
                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
            </div>
        </div>


        <div class="form-group  {{ $errors->has('display_text') ? 'has-error' : '' }}">
            {!! Form::label('display_text', 'App Text', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::textarea('display_text', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('display_text', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('image') ? 'has-error' : '' }}">
            {!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
            <div class="controls">
                <input name="image" type="file" class="uploader" id="image" value="Upload"/>
                <span class="help-block">{{ $errors->first('image', ':message') }}</span>
            </div>
        </div>


      


        <div class="form-group">
						<div>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>

							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/servicetype')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Back
							</button>
						
						</div>
					</div>


     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
 @endsection       
