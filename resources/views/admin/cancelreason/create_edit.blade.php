@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
<h3> {{$title}} </h3>
<div class="row">
  <div class="col-lg-4 col-md-4"> @if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
    @endif 
    
    <!-- ./ tabs --> 
    @if (isset($reason))
    {!! Form::model($reason, array('url' => url('admin/cancelreason') . '/' . $reason->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
    @else
    {!! Form::open(array('url' => url('admin/cancelreason'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
    @endif
    <div class="form-group  {{ $errors->has('question') ? 'has-error' : '' }}"> {!! Form::label('title', 'Reason Detail', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::text('title', null, array('class' => 'form-control','required' => 'required')) !!} <span class="help-block">{{ $errors->first('title', ':message') }}</span> </div>
    </div>
    <div class="form-group">
      <div>
        <button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/cancelreason')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> Cancel </button>
        <button type="submit" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-ok-circle"></span> &nbsp;Save </button>
      </div>
    </div>
    {!! Form::close() !!} </div>
</div>
@endsection

@section('scripts') 
<script type="text/javascript">
            $(function () {
                $("#type").select2()
            });
        </script> 
@endsection 