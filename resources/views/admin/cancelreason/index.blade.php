@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Cancel Reason Contents
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! url('admin/cancelreason/create') !!}"
                       class="btn btn-sm  btn-primary"><span
                                class="glyphicon glyphicon-plus-sign"></span> Add New</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Title</th>
			
            <th>Is Active</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($reason as $content)
        <tr>
			<td>{{ $content->title }}</td>
		
			<td>@if ($content->is_active=='1') Yes @else No @endif</td>
			<td>
		
<span data-placement="top" data-toggle="tooltip" title="Active"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/cancelreason')}}/{{ $content->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>
@if ($content->is_active=='1')
<span data-placement="top" data-toggle="tooltip" title="Inactive"><button class="btn btn-link btn-xs" data-title="Status" type="button" onclick='window.location.href="{{url('admin/cancelreason/statusupdate')}}/{{ $content->id }}/1"'>
		
			<span class="glyphicon glyphicon-ok"></span>
		
			</button></span>
@else


			<span data-placement="top" data-toggle="tooltip" title="Inactive"><button class="btn btn-link btn-xs" data-title="Status" type="button" onclick='window.location.href="{{url('admin/cancelreason/statusupdate')}}/{{ $content->id }}/0"'>
		
			<span class="fa fa-ban"></span>
		
			</button></span>
		@endif

			

    {{Form::open(array('method'=>'DELETE','id'=>'form'.$content->id, 'route' => array('cancelreason.destroy', $content->id)))}}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>
		$(document).ready(function(){
			$('#form{{$content->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$content->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'cancelreason';
								
							  }
							});
					}
				});
			});

		});
		</script>

    
			</td>

        </tr>

        
    @endforeach
<tr>

</tr>


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
