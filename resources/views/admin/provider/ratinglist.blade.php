@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> {{$title}} </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
	 
    <tr>
      <th>Job</th>
      <th>Provider</th>
      <th>Date</th>
     
      <th>Rating</th>
      <th>Professional</th>
      <th>Friendly</th>
      <th>Communication</th>
      <th>Comment</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach ($provider as $rate)
  <tr>
    <td>{{ $rate->job_id }}</td>
    <td>{{ $rate->customer_phone }}</td>
    <td>{{ $rate->date }}</td>
    
    <td>{{ $rate->rate_value }} </td>
    <td>{{ $rate->professional }} </td>
    <td>{{ $rate->friendly }} </td>
    <td>{{ $rate->communication }} </td>
    <td>{{ $rate->comment }} </td>
  </tr>
  @endforeach 
  <tr>
<td colspan="10">{{ $provider->appends(Request::except('page'))->links() }}</td>
</tr>
  
    </tbody>
  
</table>
<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 
