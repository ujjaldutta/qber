@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-6">

<form name="filterform" id="filterform" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<table id="table" class="table table-striped table-hover">
  <thead>
    <tr>
      <th colspan="2">Filter By : Service Type
        <select style="width:200px;" name="servicetype" id="servicetype">
          <option value="">All Service Type</option>
          
			@foreach($servicetype as $type)
				
          <option value="{{$type->id}}" {{ Request::input('servicetype')==$type->id ? 'selected' : '' }} >{{$type->name}}</option>
          
			 @endforeach
			
        </select></th>
      <th colspan="2"> Nationality
        <select style="width:200px;" name="nationality" id="nationality">
          <option value="">All Country</option>
          
			@foreach($nationality as $country)
				
          <option value="{{$country->code}}" {{ Request::input('nationality')==$country->code ? 'selected' : '' }} >{{$country->name}}</option>
          
			 @endforeach
			
        </select>
      </th>

      <th colspan="2"> Total Income
        <select name="total_come" id="total_come">
          <option value="">Select</option>
			<option value="0-100" {{ Request::input('total_come')=='0-100' ? 'selected' : '' }} >0-100</option>
			<option value="101-500" {{ Request::input('total_come')=='101-500' ? 'selected' : '' }} >101-500</option>
			<option value="501-1000" {{ Request::input('total_come')=='501-1000' ? 'selected' : '' }} >501-1000</option>
			<option value="1001-5000" {{ Request::input('total_come')=='1001-5000' ? 'selected' : '' }} >1001-5000</option>
			<option value="5001-unlimited" {{ Request::input('total_come')=='5001-unlimited' ? 'selected' : '' }} >5001-unlimited</option>
        </select>
      </th>
   </tr>
   <tr>
    <th colspan="2">Revenue Generated :<br/> 					
				<div class='input-group date' id='revenue_from_datepicker'>
					
					

					<input type="text" name="revenue_from_date" id="revenue_from_date" class="form-control" value="{{ Request::input('revenue_from_date') }}"/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"> </span>
					</span>
				</div>

			

			

			</th>

			<th colspan="2">
		
			 					
				<div class='input-group input-group1 date' id='revenue_to_datepicker'>
				

					<input type="text" name="revenue_to_date" id="revenue_to_date" class="form-control" value="{{ Request::input('revenue_to_date') }}"/>
					
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			</th>
  
      <th colspan="2"> From Income

				<input type="text" name="from_income" id="from_income" class="form-control" value="{{ Request::input('from_income') }}"/>

				 To Income

				<input type="text" name="to_income" id="to_income" class="form-control" value="{{ Request::input('to_income') }}"/>

				
			  </th>

      
			
      <th colspan="2"> Provider Status
        <select class="provide-status" name="provider_status" id="provider_status">
        <option value="">Select</option>
        <option value="free" {{ Request::input('provider_status')=='free' ? 'selected' : '' }} >Free</option>
		<option value="break" {{ Request::input('provider_status')=='break' ? 'selected' : '' }} >Break</option>
		<option value="unavailable" {{ Request::input('provider_status')=='unavailable' ? 'selected' : '' }} >Unavailable</option>
		<option value="must head now" {{ Request::input('provider_status')=='must head now' ? 'selected' : '' }} >Must head now</option>
		<option value="heading" {{ Request::input('provider_status')=='heading' ? 'selected' : '' }} >Heading</option>
		<option value="working" {{ Request::input('provider_status')=='working' ? 'selected' : '' }} >Working</option>
			
        </select>
      </th>
      <th colspan="2"> Account Status
        <select class="provide-status" name="account_status" id="account_status">
          <option value="">Select</option>
          <option value="active" {{ Request::input('account_status')=='active' ? 'selected' : '' }}>Active</option>
          <option value="inactive" {{ Request::input('account_status')=='inactive' ? 'selected' : '' }}>Not Active</option>
          <option value="approved" {{ Request::input('account_status')=='approved' ? 'selected' : '' }}>Approved</option>
          <option value="notapproved" {{ Request::input('account_status')=='notapproved' ? 'selected' : '' }}>Not Approved</option>
			
        </select>
      </th>
</tr>
<tr>
      <th colspan="2"> Transport Mode
        <select class="provide-status" name="transport_mode" id="transport_mode">
          <option value="">Select</option>
			<option value="car" {{ Request::input('transport_mode')=='car' ? 'selected' : '' }} >Car</option>
			<option value="taxi" {{ Request::input('transport_mode')=='taxi' ? 'selected' : '' }} >Taxi</option>
			<option value="walk" {{ Request::input('transport_mode')=='walk' ? 'selected' : '' }} >Walk</option>
			
			
        </select>
      </th>

      <th colspan="2"> Type of Employment
        <select class="employment-status" name="employment_type" id="employment_type">
			<option value="">Select</option>
			<option value="individual" {{ Request::input('employment_type')=='individual' ? 'selected' : '' }}>Individual</option>
			<option value="company" {{ Request::input('employment_type')=='company' ? 'selected' : '' }}>Company</option>
        </select>
      </th>

      <th colspan="2">
				Job Area
				<select name="current_area" id="current_area" >
				<option value="">Select</option>
				@foreach($perference as $rec)
				<option value="{{ $rec->id}}" {{ Request::input('current_area')==$rec->id ? 'selected' : '' }}>{{ $rec->name}}</option>
				@endforeach
				</select>

			
			</th>


      <th colspan="2">					
				<div class='input-group date' id='free_from_datepicker'>
					
				Free From

					<input type="text" name="free_from_date" id="free_from_date" class="form-control" value="{{ Request::input('free_from_date') }}"/>
					
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			</th>

			<th colspan="2">
				Free To
				<div class='input-group date' id='free_to_datepicker'>
					
					<input type="text" name="free_to_date" id="free_to_date" class="form-control" value="{{ Request::input('free_to_date') }}"/>
					
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			</th>
			
</tr>
<tr>
			<th colspan="1">
				Currently Requesting
				<select name="current_request" id="current_request" >
				<option value="">Select</option>
				<option value="yes" {{ Request::input('current_request')=='yes' ? 'selected' : '' }}>Yes</option>
				<option value="no" {{ Request::input('current_request')=='no' ? 'selected' : '' }}>No</option>
				</select>

			
			</th>
			<th colspan="1">
				Account Limit Exceed
				<select name="account_limit" id="account_limit" >
				<option value="">Select</option>
				<option value="yes" {{ Request::input('account_limit')=='yes' ? 'selected' : '' }}>Yes</option>
				<option value="no" {{ Request::input('account_limit')=='no' ? 'selected' : '' }}>No</option>
				</select>

			
			</th>
			<th colspan="1">
				Gender
				<select name="gender" id="gender" >
				<option value="">Select</option>
				<option value="male" {{ Request::input('gender')=='male' ? 'selected' : '' }}>Male</option>
				<option value="female" {{ Request::input('gender')=='female' ? 'selected' : '' }}>Female</option>
				</select>

			
			</th>
			<th colspan="1">
				Age
				<select name="age" id="age" >
				<option value="">Select</option>
				<option value="18-25" {{ Request::input('age')=='18-25' ? 'selected' : '' }}>18-25</option>
				<option value="25-30" {{ Request::input('age')=='25-30' ? 'selected' : '' }}>25-30</option>
				<option value="30-35" {{ Request::input('age')=='30-35' ? 'selected' : '' }}>30-35</option>
				<option value="35-40" {{ Request::input('age')=='35-40' ? 'selected' : '' }}>35-40</option>
				<option value="40-50" {{ Request::input('age')=='40-50' ? 'selected' : '' }}>40-50</option>
				<option value="50-60" {{ Request::input('age')=='50-60' ? 'selected' : '' }}>50-60</option>
				
				</select>

			
			</th>
			
     
    </tr>

    <tr><th colspan="2">Search By :
			<input type="text"  placeholder="Phone"  value="{{Request::input('phone')}}" name="phone" id="phonefilter" >

			

			</th>

			<th colspan="2">
			<input type="text"  placeholder="ID"  value="{{Request::input('id')}}" name="id" id="idfilter" >

			

			</th>

			<th colspan="2">
			<input type="text" placeholder="Name" value="{{Request::input('name')}}" name="name" id="namefilter" >

			

			</th>

			<th colspan="2">
			<input type="text" placeholder="Email" value="{{Request::input('email')}}" name="email" id="emailfilter" >

			

			</th>
			


			<th colspan="13">

			<input type="button" onclick="searchProvider()" value="Search">
			</th>
</tr>

</thead>
</table>
</form>


	<div class="row">
					<div class="col-lg-12">
					<div id="map_canvas"></div>
					</div>
					</div>


      </div>
</div>

@endsection
<style>
 #map_canvas {
        height: 100%;
      }

</style>
{{-- Scripts --}}
@section('scripts')
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k&callback=initialize">
    </script>
 <script>

	 var markers=[];
    

	 $(document).ready(function(){

		
                $('#free_from_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#free_to_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});

              $('#revenue_from_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#revenue_to_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
             
                
           
            
		
				   $.ajax({
					  url: '{!! url("admin/providers/get-providers") !!}',
					   type: 'POST',
					  data:{service_type:$("#service_type").val(),provider_status:$("#provider_status").val(),_token:'{{ csrf_token() }}' },
					 
					  success: function(data) {
						addmarker(data);
					  },
					 
				   });

				   /*$("#service_type").change(function(){

							  $.ajax({
								  url: '{!! url("admin/providers/get-providers") !!}',
								   type: 'POST',
								  data:{service_type:$(this).val(),provider_status:$("#provider_status").val(),_token:'{{ csrf_token() }}' },
								 
								  success: function(data) {
									for (var i = 0; i < markers.length; i++) {
											markers[i].setMap(null); //Remove the marker from the map
										}
										markers=[];
									 if(data.length>0)
										addmarker(data);
								  },
								 
							   });
					   });*/

			
				

		 });


function searchProvider(){

							  $.ajax({
								  url: '{!! url("admin/providers/get-providers") !!}',
								   type: 'POST',
								  data:$("#filterform").serialize(),
								 
								  success: function(data) {
									for (var i = 0; i < markers.length; i++) {
											markers[i].setMap(null); //Remove the marker from the map
										}
										markers=[];
									 if(data.length>0)
										addmarker(data);
								  },
								 
							   });
					   }

					   
function addmarker(data){
var bounds = new google.maps.LatLngBounds();

var infoWindowContent=[];
								
    
   
    
    for( i = 0; i < data.length; i++ ) {
		if(!data[i]['current_latitude'] || !data[i]['current_longitude'])
			continue
		
		var content=[data[i]['name'],data[i]['current_latitude'],data[i]['current_longitude']]
		markers.push(content);
		var details
		if(data[i]['profile'])
		details=data[i]['profile']['service_details'];	

		content= ['<div class="info_content">' +
        '<h3>'+data[i]['name']+'</h3>' +
        '<p>'+details+'</p>' +
      
        '</div>'];
        
        infoWindowContent.push(content);
	}
                    

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        markers[i] = marker
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });

}
 var map;

function initialize() {
   
    var myLatlng = new google.maps.LatLng(25.29161,51.53044);
    var mapOptions = {
        
        center: myLatlng,
        zoom: 10,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    
    
}

     
    </script>
  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
