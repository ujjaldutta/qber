@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> {{$title}} </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
	 
    <tr>
      <th>Phone</th>
     
      <th>Date</th>
     
      <th>From Time</th>
     
      <th>To Time</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach ($providers as $free)
  <tr>
    <td>{{ $free->phone }}</td>
     <td>{{ $free->date }} </td>
    <td>{{ $free->from_time }}</td>
    <td>{{ $free->to_time }}</td>
    
   
   
  </tr>
  @endforeach 
  <tr>
<td colspan="10">{{ $providers->appends(Request::except('page'))->links() }}</td>
</tr>
  
    </tbody>
  
</table>
<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 
