@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> {{$title}}
   
  </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
    <tr>
      <th colspan="3">Filter By : Service Type
        <select style="width:200px;" name="servicetype" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality')])}}'+'&servicetype='+this.value">
          <option value="">All Service Type</option>
          
			@foreach($servicetype as $type)
				
          <option value="{{$type->id}}" {{ Request::input('servicetype')==$type->id ? 'selected' : '' }} >{{$type->name}}</option>
          
			 @endforeach
			
        </select></th>
      <th colspan="3"> Nationality
        <select style="width:200px;" name="nationality" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&nationality='+this.value">
          <option value="">All Country</option>
          
			@foreach($nationality as $country)
				
          <option value="{{$country->code}}" {{ Request::input('nationality')==$country->code ? 'selected' : '' }} >{{$country->name}}</option>
          
			 @endforeach
			
        </select>
      </th>
    </tr>
    <tr>
      <th>Phone <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
      <th>Name <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
      <th>Jobs <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'servicecount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'servicecount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
      <th>Earn Amount <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
     <th>Credit Amount <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'credit_amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'credit_amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

     <th>Company Credit Amount <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'company_credit', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'company_credit', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  
  @foreach ($providers as $user)
  <tr>
    <td>{{ $user->phone }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->servicecount}}</td>
    <td>${{ (float)$user->amount }}</td>
    <td>${{ (float)$user->credit_amount }}</td>
     <td>${{ (float)$user->company_credit }}</td>
    <td>

  

      <span data-placement="top" data-toggle="tooltip" title="Sattle Balance">
      <button class="btn btn-link btn-xs" data-title="Sattle Balance" type="button" onclick='window.location.href="{{url('admin/providers/sattle')}}/{{ $user->id }}"'> <span class="fa fa-user" aria-hidden="true"></span> </button>  </span>


	


      </td>
  </tr>
  @endforeach
  <tr>
    <td colspan="10">{{ $providers->appends(Request::except('page'))->links() }}</td>
  </tr>
    </tbody>
  
</table>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 
