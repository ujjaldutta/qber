@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
<h3> {{$title}} </h3>
<div class="row">
  <div class="col-lg-12 col-md-8"> @if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
    @endif 
    
    <!-- ./ tabs --> 
    @if (isset($provider))
    {!! Form::model($provider, array('url' => url('admin/providers/saveprofile') . '/' . $provider->id, 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
    @else
    {!! Form::open(array('url' => url('admin/providers/saveprofile'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
    @endif
    <div class="row">
      <div class="col-lg-6  col-md-6">
        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}"> {!! Form::label('name', 'Name', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('name', null, array('class' => 'form-control','readonly' => 'readonly')) !!} <span class="help-block">{{ $errors->first('name', ':message') }}</span> </div>
        </div>
      </div>
      <div class="col-lg-6  col-md-6">
        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}"> {!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('phone', null, array('class' => 'form-control','readonly' => 'readonly')) !!} <span class="help-block">{{ $errors->first('phone', ':message') }}</span> </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('service_details', 'Service Details', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::textarea('service_details', $provider->profile->service_details, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>

       <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('your_offer', 'Your Offer', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text(' your_offer', $provider->profile->your_offer, array('class' => 'form-control','readonly' => 'readonly')) !!}</div>
        </div>
      </div>
      
      
    </div>
    <div class="row">
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('team_size', 'Team Size', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('team_size', $provider->profile->team_size, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('nationality', 'Nationality', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('nationality', $provider->profile->nationality, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('language_preference', 'Speaking In', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('language_preference', $provider->profile->language_preference, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('transport', 'Transport', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('transport', $provider->profile->transport, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('max_purchase_limit', 'Max Purchase Limit', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('max_purchase_limit', $provider->profile->max_purchase_limit, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('current_credit_balance', 'Credit Balance', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('current_credit_balance', $provider->profile->current_credit_balance, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('image', 'Banner Image1', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->banner_img1) <img src="{{url($provider->profile->banner_img1)}}" width="200"/> @endif </div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('banner_img2', 'Banner Image2', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->banner_img2) <img src="{{url($provider->profile->banner_img2)}}" width="200"/> @endif </div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('banner_img3', 'Banner Image3', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->banner_img3) <img src="{{url($provider->profile->banner_img3)}}" width="200"/> @endif </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('year_of_experience', 'Year Of Experience', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('year_of_experience', $provider->profile->year_of_experience, array('class' => 'form-control','readonly' => 'readonly')) !!}</div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('live_in_qatar', 'Live in qatar', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('live_in_qatar', $provider->profile->live_in_qatar, array('class' => 'form-control','readonly' => 'readonly')) !!}</div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('internet_access', 'Inernet Access', array('class' => 'control-label')) !!}
          <div class="controls">{!! Form::text('internet_access', $provider->profile->internet_access, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
    </div>



    <div class="row">
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('max_due_amount', 'Max Due', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text(' max_due_amount', $provider->profile->max_due_amount, array('class' => 'form-control','readonly' => 'readonly')) !!}</div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('date_of_birth', 'Live in qatar', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('date_of_birth', $provider->profile->date_of_birth, array('class' => 'form-control','readonly' => 'readonly')) !!}
Age : {!! $age=date("Y")-$provider->profile->date_of_birth; !!}
          </div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('gender', 'Gender', array('class' => 'control-label')) !!}
          <div class="controls">{!! Form::text('gender', $provider->profile->gender, array('class' => 'form-control','readonly' => 'readonly')) !!} </div>
        </div>
      </div>
    </div>

    

    <div class="row">
     
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('shop_name', 'Shop Name', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('shop_name', $provider->profile->shop_name, array('class' => 'form-control','readonly' => 'readonly')) !!}

          </div>
        </div>
      </div>


 @if($provider->company)
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('work_in_company', 'Work In Company', array('class' => 'control-label')) !!}
          <div class="controls">{!! Form::text('work_in_company', $provider->company->details->name , array('class' => 'form-control','readonly' => 'readonly')) !!}  </div>
        </div>
      </div>
 @endif

 @if($preference)
 <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('work_preference', 'Work Preference', array('class' => 'control-label')) !!}
          <div class="controls"> {!! Form::text('work_preference', $preference->name, array('class' => 'form-control','readonly' => 'readonly')) !!}

          </div>
        </div>
      </div>
 @endif
   </div>
    


<div class="row">

	<div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('image', 'Profile Image', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->image) <img src="{{url($provider->profile->image)}}" width="300"/> @else No Image @endif </div>
        </div>
      </div>
      
      <div class="col-lg-6  col-md-6">
        <div class="form-group  "> {!! Form::label('admin_profile_image', 'Admin Profile Image', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->admin_profile_image) <img src="{{url($provider->profile->admin_profile_image)}}" width="200"/>
		@else No Image
          @endif </div>
        </div>
      </div>
      
 </div>


 <div class="row">

	 <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('tools_use_image', 'Photo of Tools Provider Use', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->tools_use_image) <img src="{{url($provider->profile->tools_use_image)}}" width="200"/>
			@else No Image
          @endif </div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('shop_image', 'Photo of Provider Shop', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->shop_image) <img src="{{url($provider->profile->shop_image)}}" width="200"/>
			@else No Image
           @endif </div>
        </div>
      </div>

      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('car_registration_image', 'Attached CV', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->cv_file) <a href="{{url('admin/providers/download').'?file='.$provider->profile->cv_file}}">Download CV</a> @else No CV @endif </div>
        </div>
      </div>
      
     
     



 </div>

  <div class="row">

	   <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('car_registration_image', 'Photo Car Registration', array('class' => 'control-label')) !!}
          <div class="controls"> @if($provider->profile->car_registration_image) <img src="{{url($provider->profile->car_registration_image)}}" width="200"/>
		@else No Image
          @endif </div>
        </div>
      </div>

	   <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('driving_license_image', 'Driving License Image', array('class' => 'control-label')) !!}
          <div class="controls">
			@if($provider->profile->driving_license_image) <a href="{{url('admin/providers/download').'?file='.$provider->profile->driving_license_image}}">Download Driving License</a>
			@else No Driving License @endif
			   </div>
        </div>
      </div>
      <div class="col-lg-3  col-md-3">
        <div class="form-group  "> {!! Form::label('identity_image', 'Qatar ID Image', array('class' => 'control-label')) !!}
          <div class="controls">
				@if($provider->profile->identity_image) <a href="{{url('admin/providers/download').'?file='.$provider->profile->identity_image}}">Download Qatar ID </a>
			@else No ID @endif
			
			   </div>
        </div>
      </div>
      
      
 </div>

    
    <div class="form-group">
      <div>
        <button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
        
        <!--<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Save
							</button>--> 
      </div>
    </div>
    {!! Form::close() !!} </div>
</div>
@endsection

@section('scripts') 
<script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script> 
@endsection 
