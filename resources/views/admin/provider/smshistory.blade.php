@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> {{$title}} </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
	 
    <tr>
      <th>Device Id</th>
      <th>Phone</th>
      <th>SMS content</th>
      <th>Date</th>
     
      <th>IP</th>

    </tr>
  </thead>
  <tbody>
  
  @foreach ($sms as $rec)
  <tr>
    <td>@if ($rec->device_id) {{ $rec->device_id }}  @else None @endif</td>
    <td>@if ($rec->phone) {{ $rec->phone }}  @else None @endif</td>
     <td>  {{ $rec->sms_content }}</td>
    <td>{{ $rec->send_date }}</td>
    
    <td> {{ $rec->ip }} </td>
   
  </tr>
  @endforeach 
  <tr>
<td colspan="10">{{ $sms->appends(Request::except('page'))->links() }}</td>
</tr>
  
    </tbody>
  
</table>
<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/dashboard')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 
