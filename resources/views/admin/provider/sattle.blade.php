@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
<h3> {{$title}} </h3>
<div class="row">
  <div class="col-lg-4 col-md-4"> @if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
    @endif 
    
  
    {!! Form::model($provider, array('url' => url('admin/providers/sattle-update'), 'method' => 'post', 'class' => 'bf')) !!}
   {!! Form::hidden('provider_id', $provider->id) !!}
 
    <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}"> {!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::text('phone', null, array('class' => 'form-control','readonly' => 'readonly')) !!} <span class="help-block">{{ $errors->first('phone', ':message') }}</span> </div>
    </div>
    
    <div class="form-group  {{ $errors->has('current_credit_balance') ? 'has-error' : '' }}">
     {!! Form::label('current_credit_balance', 'Please enter amount you want to sattle', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::text('current_credit_balance', 0, array('class' => 'form-control','required' => 'required')) !!}
      <span class="help-block">{{ $errors->first('current_credit_balance', ':message') }}</span>
		Current credit balance : @if($provider->company_credit>0)
			(Company) {{ $provider->company_credit }}
			
			@else
			{{ (float)$provider->profile->current_credit_balance }}

			@endif

      </div>
    </div>
   
   
   
    <div class="form-group">
      <div>
        <button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers/sattle-balance')}}'"> <span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Back</button>
        <button type="submit" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save</button>
      </div>
    </div>
    {!! Form::close() !!} </div>
</div>
@endsection
@section('scripts') 
<script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script> 
@endsection 
