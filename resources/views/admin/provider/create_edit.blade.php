@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
<h3> {{$title}} </h3>
<div class="row">
  <div class="col-lg-4 col-md-4"> @if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
    @endif 

    <!-- ./ tabs --> 
    @if (isset($provider))
    {!! Form::model($provider, array('url' => url('admin/providers') . '/' . $provider->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}
    @else
    {!! Form::open(array('url' => url('admin/providers'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
    @endif
    <div class="form-group "> {!! Form::label('type_id', 'Service type', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::select('type_id', $service) !!} <span class="help-block">{{ $errors->first('type_id', ':message') }}</span> </div>
    </div>
    <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}"> {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::email('email', null, array('class' => 'form-control')) !!} <span class="help-block">{{ $errors->first('email', ':message') }}</span> </div>
    </div>
    <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}"> {!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::text('phone', null, array('class' => 'form-control','required' => 'required')) !!} <span class="help-block">{{ $errors->first('phone', ':message') }}</span> </div>
    </div>
    <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}"> {!! Form::label('name', 'Provider Name', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::text('name', null, array('class' => 'form-control','required' => 'required')) !!} <span class="help-block">{{ $errors->first('name', ':message') }}</span> </div>
    </div>
    <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}"> {!! Form::label('password', 'Password', array('class' => 'control-label')) !!}
      <div class="controls"> @if (isset($provider))
        {!! Form::password('password', array('class' => 'form-control')) !!}
        @else
        {!! Form::password('password', array('class' => 'form-control','required' => 'required')) !!}
        @endif <span class="help-block">{{ $errors->first('password', ':message') }}</span> </div>
    </div>
    <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}"> {!! Form::label('password_confirmation', 'Confirm Password', array('class' => 'control-label')) !!}
      <div class="controls">
		  @if (isset($provider))
        {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
        @else
        {!! Form::password('password_confirmation', array('class' => 'form-control','required' => 'required')) !!}
        @endif <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span> </div>
    </div>

     <div class="form-group  {{ $errors->has('max_due_amount') ? 'has-error' : '' }}">
     {!! Form::label('max_due_amount', 'Max Due Amount', array('class' => 'control-label')) !!}
      <div class="controls"> {!! Form::text('max_due_amount', $provider->profile->max_due_amount, array('class' => 'form-control','required' => 'required')) !!} <span class="help-block">{{ $errors->first('phone', ':message') }}</span> </div>
      
    </div>


    
    <div class="form-group status-group  {{ $errors->has('is_active') ? 'has-error' : '' }}"> {!! Form::label('is_active', 'Is Active', array('class' => 'control-label')) !!}
      <div class="controls pull-left"> {!! Form::checkbox('is_active', 1) !!} <span class="help-block">{{ $errors->first('is_active', ':message') }}</span> </div>
    </div>
    <div class="form-group status-group   {{ $errors->has('approve_status') ? 'has-error' : '' }}"> {!! Form::label('approve_status', 'Is Approved', array('class' => 'control-label')) !!}
      <div class="controls pull-left"> {!! Form::checkbox('approve_status', 1) !!} <span class="help-block">{{ $errors->first('approve_status', ':message') }}</span> </div>
    </div>
    <div class="form-group status-group   {{ $errors->has('sms_verified') ? 'has-error' : '' }}"> {!! Form::label('sms_verified', 'SMS Verified', array('class' => 'control-label')) !!}
      <div class="controls pull-left"> {!! Form::checkbox('sms_verified', 1) !!} <span class="help-block">{{ $errors->first('sms_verified', ':message') }}</span> </div>
    </div>
    <div class="form-group status-group">
      <div>
        <button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel</button>
        <button type="submit" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save</button>
      </div>
    </div>
    {!! Form::close() !!} </div>
</div>
@endsection
@section('scripts') 
<script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script> 
@endsection 
