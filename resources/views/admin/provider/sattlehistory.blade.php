@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> {{$title}} </h3>
</div>
<table id="table" class="table table-striped table-hover">
  <thead>
	 
    <tr>
      <th>Provider</th>
      <th>Admin</th>
      <th>Company</th>
      <th>Sattle Date</th>
     
      <th>Amount</th>

    </tr>
  </thead>
  <tbody>
  
  @foreach ($provider as $sattle)
  <tr>
    <td>@if ($sattle->provider) {{ $sattle->provider->phone }}  @else None @endif</td>
    <td> @if ($sattle->admin) {{ $sattle->admin->email }}  @else None  @endif</td>
     <td> @if ($sattle->company)  {{ $sattle->company->name }}  @else None  @endif</td>
    <td>{{ $sattle->date }}</td>
    
    <td> ${{ (float)$sattle->amount }} </td>
   
  </tr>
  @endforeach 
  <tr>
<td colspan="10">{{ $provider->appends(Request::except('page'))->links() }}</td>
</tr>
  
    </tbody>
  
</table>
<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers')}}'"> <span class="glyphicon glyphicon-ban-circle"></span> &nbsp;Back </button>
@endsection

{{-- Scripts --}}
@section('scripts') 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 
