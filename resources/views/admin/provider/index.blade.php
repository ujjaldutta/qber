@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
<div class="page-header">
  <h3> Provider List
    <div class="pull-right">
      <div class="pull-right"> <a href="{!! url('admin/providers/create') !!}"
                       class="btn btn-sm  btn-primary"><span
                                class="glyphicon glyphicon-plus-sign"></span> Add New</a> </div>
    </div>
  </h3>
</div>

<div class="col-lg-12 providers" style="overflow:auto;">
<div class="fs-whatwg" id="fs-whatwg">
<table id="table" class="table table-striped table-hover">
  <thead>
    <tr>
      <th colspan="2">Filter By : Service Type
        <select style="width:200px;" name="servicetype" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality')])}}'+'&servicetype='+this.value">
          <option value="">All Service Type</option>
          
			@foreach($servicetype as $type)
				
          <option value="{{$type->id}}" {{ Request::input('servicetype')==$type->id ? 'selected' : '' }} >{{$type->name}}</option>
          
			 @endforeach
			
        </select></th>
      <th colspan="2"> Nationality
        <select style="width:200px;" name="nationality" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&nationality='+this.value">
          <option value="">All Country</option>
          
			@foreach($nationality as $country)
				
          <option value="{{$country->code}}" {{ Request::input('nationality')==$country->code ? 'selected' : '' }} >{{$country->name}}</option>
          
			 @endforeach
			
        </select>
      </th>

      <th colspan="2"> Total Income
        <select name="total_come" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&total_come='+this.value">
          <option value="">Select</option>
			<option value="0-100" {{ Request::input('total_come')=='0-100' ? 'selected' : '' }} >0-100</option>
			<option value="101-500" {{ Request::input('total_come')=='101-500' ? 'selected' : '' }} >101-500</option>
			<option value="501-1000" {{ Request::input('total_come')=='501-1000' ? 'selected' : '' }} >501-1000</option>
			<option value="1001-5000" {{ Request::input('total_come')=='1001-5000' ? 'selected' : '' }} >1001-5000</option>
			<option value="5001-unlimited" {{ Request::input('total_come')=='5001-unlimited' ? 'selected' : '' }} >5001-unlimited</option>
        </select>
      </th>
    <th colspan="2">Revenue Generated :<br/> 					
				<div class='input-group date' id='revenue_from_datepicker'>
					
					{!! Form::text('revenue_from_date', Request::input('revenue_from_date'), array('class'=>'form-control','id'=>'revenue_from_date','placeholder'=>'Form Date'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"> </span>
					</span>
				</div>

			

			

			</th>

			<th colspan="2">
		
			 					
				<div class='input-group input-group1 date' id='revenue_to_datepicker'>
					{!! Form::text('revenue_to_date', Request::input('revenue_to_date'), array('class'=>'form-control','id'=>'revenue_to_date','placeholder'=>'To Date'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			</th>
   <!-- <th colspan="2"> Last Month Income
        <select name="last_month_come" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&last_month_come='+this.value+'&revenue_from_datepicker='+$('#revenue_from_date').val()+'&revenue_to_datepicker='+$('#revenue_to_date').val()">
          <option value="">Select</option>
			<option value="0-100" {{ Request::input('last_month_come')=='0-100' ? 'selected' : '' }} >0-100</option>
			<option value="101-500" {{ Request::input('last_month_come')=='101-500' ? 'selected' : '' }} >101-500</option>
			<option value="501-1000" {{ Request::input('last_month_come')=='501-1000' ? 'selected' : '' }} >501-1000</option>
			<option value="1001-5000" {{ Request::input('last_month_come')=='1001-5000' ? 'selected' : '' }} >1001-5000</option>
			<option value="5001-unlimited" {{ Request::input('last_month_come')=='5001-unlimited' ? 'selected' : '' }} >5001-unlimited</option>
        </select>
      </th>-->

      <th colspan="2"> From Income

				<input type="text" name="from_income" id="from_income" class="form-control" value="{{ Request::input('from_income') }}"/>

				 To Income

				<input type="text" name="to_income" id="to_income" class="form-control" value="{{ Request::input('to_income') }}"/>

				
			  </th>

      
			
      <th colspan="2"> Provider Status
        <select class="provide-status" name="provider_status" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&provider_status='+this.value">
        <option value="">Select</option>
        <option value="free" {{ Request::input('provider_status')=='free' ? 'selected' : '' }} >Free</option>
		<option value="break" {{ Request::input('provider_status')=='break' ? 'selected' : '' }} >Break</option>
		<option value="unavailable" {{ Request::input('provider_status')=='unavailable' ? 'selected' : '' }} >Unavailable</option>
		<option value="must head now" {{ Request::input('provider_status')=='must head now' ? 'selected' : '' }} >Must head now</option>
		<option value="heading" {{ Request::input('provider_status')=='heading' ? 'selected' : '' }} >Heading</option>
		<option value="working" {{ Request::input('provider_status')=='working' ? 'selected' : '' }} >Working</option>
			
        </select>
      </th>
      <th colspan="2"> Account Status
        <select class="provide-status" name="account_status" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&account_status='+this.value">
          <option value="">Select</option>
          <option value="active" {{ Request::input('account_status')=='active' ? 'selected' : '' }}>Active</option>
          <option value="inactive" {{ Request::input('account_status')=='inactive' ? 'selected' : '' }}>Not Active</option>
          <option value="approved" {{ Request::input('account_status')=='approved' ? 'selected' : '' }}>Approved</option>
          <option value="notapproved" {{ Request::input('account_status')=='notapproved' ? 'selected' : '' }}>Not Approved</option>
			
        </select>
      </th>

      <th colspan="2"> Transport Mode
        <select class="provide-status" name="transport_mode" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&transport_mode='+this.value">
          <option value="">Select</option>
			<option value="car" {{ Request::input('transport_mode')=='car' ? 'selected' : '' }} >Car</option>
			<option value="taxi" {{ Request::input('transport_mode')=='taxi' ? 'selected' : '' }} >Taxi</option>
			<option value="walk" {{ Request::input('transport_mode')=='walk' ? 'selected' : '' }} >Walk</option>
			
			
        </select>
      </th>

      <th colspan="2"> Type of Employment
        <select class="employment-status" name="employment_type" onchange="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}'+'&employment_type='+this.value">
			<option value="">Select</option>
			<option value="individual" {{ Request::input('employment_type')=='individual' ? 'selected' : '' }}>Individual</option>
			<option value="company" {{ Request::input('employment_type')=='company' ? 'selected' : '' }}>Company</option>
        </select>
      </th>

      <th colspan="2">
				Job Area
				<select name="current_area" id="current_area" onchange="setFilter(this.value,'current_area')">
				<option value="">Select</option>
				@foreach($perference as $rec)
				<option value="{{ $rec->id}}" {{ Request::input('current_area')==$rec->id ? 'selected' : '' }}>{{ $rec->name}}</option>
				@endforeach
				</select>

			
			</th>


      <th colspan="2">					
				<div class='input-group date' id='free_from_datepicker'>
					
					{!! Form::text('free_from_date', Request::input('free_from_date'), array('class'=>'form-control','id'=>'free_from_date','placeholder'=>'Free From'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			</th>

			<th colspan="2">

				<div class='input-group date' id='free_to_datepicker'>
					
					{!! Form::text('free_to_date', Request::input('free_to_date'), array('class'=>'form-control','id'=>'free_to_date','placeholder'=>'Free To'))!!}
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

			

			</th>
			<th><input type="button" value="Go" onclick="searchDate()">
			</th>

			<th colspan="1">
				Currently Requesting
				<select name="current_request" id="current_request" onchange="setFilter(this.value,'current_request')">
				<option value="">Select</option>
				<option value="yes" {{ Request::input('current_request')=='yes' ? 'selected' : '' }}>Yes</option>
				<option value="no" {{ Request::input('current_request')=='no' ? 'selected' : '' }}>No</option>
				</select>

			
			</th>
			<th colspan="1">
				Account Limit Exceed
				<select name="account_limit" id="account_limit" onchange="setFilter(this.value,'account_limit')">
				<option value="">Select</option>
				<option value="yes" {{ Request::input('account_limit')=='yes' ? 'selected' : '' }}>Yes</option>
				<option value="no" {{ Request::input('account_limit')=='no' ? 'selected' : '' }}>No</option>
				</select>

			
			</th>
			<th colspan="1">
				Gender
				<select name="gender" id="gender" onchange="setFilter(this.value,'gender')">
				<option value="">Select</option>
				<option value="male" {{ Request::input('gender')=='male' ? 'selected' : '' }}>Male</option>
				<option value="female" {{ Request::input('gender')=='female' ? 'selected' : '' }}>Female</option>
				</select>

			
			</th>
			<th colspan="1">
				Age
				<select name="age" id="age" onchange="setFilter(this.value,'age')">
				<option value="">Select</option>
				<option value="18-25" {{ Request::input('age')=='18-25' ? 'selected' : '' }}>18-25</option>
				<option value="25-30" {{ Request::input('age')=='25-30' ? 'selected' : '' }}>25-30</option>
				<option value="30-35" {{ Request::input('age')=='30-35' ? 'selected' : '' }}>30-35</option>
				<option value="35-40" {{ Request::input('age')=='35-40' ? 'selected' : '' }}>35-40</option>
				<option value="40-50" {{ Request::input('age')=='40-50' ? 'selected' : '' }}>40-50</option>
				<option value="50-60" {{ Request::input('age')=='50-60' ? 'selected' : '' }}>50-60</option>
				
				</select>

			
			</th>
			
      <th colspan="12"><input type="button" onclick="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&phone='+$('#phonefilter').val()+'&id='+$('#idfilter').val()+'&name='+$('#namefilter').val()+'&email='+$('#emailfilter').val()+'&revenue_from_date='+$('#revenue_from_date').val()+'&revenue_to_date='+$('#revenue_to_date').val()+'&from_income='+$('#from_income').val()+'&to_income='+$('#to_income').val()" value="Search"> </th>
    </tr>

    <tr><th colspan="2">Search By :
			<input type="text"  placeholder="Phone"  value="{{Request::input('phone')}}" name="phone" id="phonefilter" >

			

			</th>

			<th colspan="2">
			<input type="text"  placeholder="ID"  value="{{Request::input('id')}}" name="id" id="idfilter" >

			

			</th>

			<th colspan="2">
			<input type="text" placeholder="Name" value="{{Request::input('name')}}" name="name" id="namefilter" >

			

			</th>

			<th colspan="2">
			<input type="text" placeholder="Email" value="{{Request::input('email')}}" name="email" id="emailfilter" >

			

			</th>
			


			<th colspan="13">

			<input type="button" onclick="window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&phone='+$('#phonefilter').val()+'&id='+$('#idfilter').val()+'&name='+$('#namefilter').val()+'&email='+$('#emailfilter').val()" value="Search">
			</th>
</tr>



    <tr>
		<th><p>ID </p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'id', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'id', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
		
      <th><p>Phone</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

<th><p>Email </p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'email', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'email', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
      
      <th><p>Name</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

      <th><p>Service Type</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'service_type', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'service_type', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

      <th><p>Current Area</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'preferred_location', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'preferred_location', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

       <th><p>Rating </p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'rating', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'rating', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

       <th><p>Current Priority</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'priority_factor', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'priority_factor', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

              <th><p>Exp Money Points</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'moneyexpvalue', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'moneyexpvalue', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

                     <th><p>Work Time (-500Hrs)</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'work_time', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'work_time', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

      <th><p>Job Status </p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'job_status', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'job_status', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
		<th><p>Current Balance</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'current_credit_balance', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'current_credit_balance', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

      <th><p>Balance Limit</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'max_due_amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'max_due_amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

 <th><p>Cancelled Per<br> Call %</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_ratio', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_ratio', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>


  <th><p>Cancel % of<br> started Jobs</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_after_job_start_count', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_after_job_start_count', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

  <th><p>Cancel % jobs<br> at heading</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_after_heading', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_after_heading', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

  <th><p>Cancellation % of<br> confirmed Jobs</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_after_confirm', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'cancel_after_confirm', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

   <th><p>% of disputes given<br> per confirmed job</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'dispute_given_after_confirm', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'dispute_given_after_confirm', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>

   <th><p>% of disputes recieved<br> per confirmed job</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'dispute_received_after_confirm', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'dispute_received_after_confirm', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
<th><p>Penalty Amount</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'penalty_amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'penalty_amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
 
 
      <th><p>Nationality </p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'nationality', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'nationality', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i> </a> </th>
      <th><p>Gender</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'gender', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'gender', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
          <th><p>Age </p> <p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'date_of_birth', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'date_of_birth', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a></p></th>
<th><p>Hopping/Fixed</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'location_preference', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'location_preference', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a></th>
          

      <th><p>Is Approved </p><a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'approve_status', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'approve_status', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
      <th><p>Currently Requesting<br> agent yes/no</p> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'company_notice', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'company_notice', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

 <th><p>Last Month<br> Income</p>
<a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'last_month_amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'last_month_amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

  <th><p>Last Week <br>Income</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'last_week_amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'last_week_amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

 <th><p>Mode of <br>transport</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'transport', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'transport', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>

 <th><p>Company</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'companyname', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'companyname', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
   <th><p>Sms Verified</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'sms_verified', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'sms_verified', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
    


       <th><p>Online/Offline</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'is_online', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'is_online', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
    



       <th><p>Ready to Serve</p>
 <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'ready_to_serve', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-up"></i> </a> <a href="{{url('admin/providers/') . '?' . http_build_query(['sort' => 'ready_to_serve', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype')])}}"> <i class="fa fa-chevron-down"></i></a> </th>
      <th><p>Action</p></th>
    </tr>
  </thead>
  <tbody>
  
  @foreach ($providers as $user)
  <tr>
	<td>{{ $user->id }}</td>
    <td>{{ $user->phone }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->name }}</td>
	<td>{{ $user->service_type }}</td>
    <td>{{ $user->preferred_location }}</td>
	<td>{{ number_format($user->rating,2,".","") }}</td>
	<td>@if($total_priority>0)  {{ number_format(($user->moneyexpvalue/$total_priority),2) }} @else 0 @endif </td>
	<td>{{ $user->moneyexpvalue }}</td>
	<td>{{ $user->work_time }} Hours</td>
    <td>@if($user->provider_status)  {{ ucwords($user->provider_status) }} @else Free @endif</td>
	<td>{{ number_format($user->current_credit_balance,2,".","")  }}</td>
	<td>{{ number_format($user->max_due_amount,2,".","")  }}</td>
	<td>{{ (float)$user->cancel_ratio  }}%</td>
	<td>{{ round($user->cancel_after_job_start_count)  }}%</td>
	<td>{{ round($user->cancel_after_heading)  }}%</td>
	<td>{{ round($user->cancel_after_confirm)  }}%</td>
	<td>{{ number_format($user->dispute_given_after_confirm,2,".","")  }}%</td>
	<td>{{ number_format($user->dispute_received_after_confirm,2,".","")  }}%</td>
	<td>${{ (float)$user->penalty_amount  }}</td>
    <td>{{ $user->nationality}}</td>
    <td>{{ $user->gender }}</td>
    <td>@if($user->date_of_birth>0) {{ $age=date('Y')-$user->date_of_birth }} @else 0 @endif</td>
	<td>@if ($user->location_preference=='fixed') Fixed @else Hopping @endif</td>
    <td>@if ($user->approve_status=='1') Yes @else No @endif</td>

    <td>@if ($user->company_notice>0) Yes @else No @endif</td>
    <td>${{ (float)$user->last_month_amount}}</td>
    <td>${{ (float)$user->last_week_amount}}</td>
     <td>{{ $user->transport}}</td>
    <td>@if ($user->companyname!="") {{$user->companyname}} @else N/A @endif</td>
    <td>@if ($user->sms_verified=='1') <a href="{{url('admin/providers/smsverify')}}/{{ $user->id }}/0">Verified</a> @else <a href="{{url('admin/providers/smsverify')}}/{{ $user->id }}/1">Set Verify</a> @endif</td>

 <td>@if ($user->is_online>0) Yes @else No @endif</td>
  <td>@if ($user->ready_to_serve>0) Yes @else No @endif</td>
    
    <td class="action-icon"><span data-placement="top" data-toggle="tooltip" title="Edit">
      <button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/providers')}}/{{ $user->id }}/edit"'> <span class="glyphicon glyphicon-pencil"></span></button>
      </span>
      @if ($user->approve_status=='1') <span data-placement="top" data-toggle="tooltip" title="Inactive">
      <button class="btn btn-link btn-xs" data-title="Status" type="button" onclick="approveprovider('{{ $user->id }}','0')"> <span class="glyphicon glyphicon-ok"></span> </button>
      </span>
      @else
      <span data-placement="top" data-toggle="tooltip" title="Inactive">
      <button class="btn btn-link btn-xs" data-title="Status" type="button" onclick="approveprovider('{{ $user->id }}','1')"> <span class="fa fa-ban"></span> </button>
      
      </span>
      @endif

      <span data-placement="top" data-toggle="tooltip" title="Profile">
      <button class="btn btn-link btn-xs" data-title="Profile" type="button" onclick='window.location.href="{{url('admin/providers/profile')}}/{{ $user->id }}"'> <span class="fa fa-user" aria-hidden="true"></span> </button>  </span>


	 <span data-placement="top" data-toggle="tooltip" title="Cancel History">
      <button class="btn btn-link btn-xs" data-title="Cancel History" type="button" onclick='window.location.href="{{url('admin/providers/cancelhistory')}}/{{ $user->id }}"'> <span class="fa fa-list" aria-hidden="true"></span> </button>  </span>

      <span data-placement="top" data-toggle="tooltip" title="Sattle History">
      <button class="btn btn-link btn-xs" data-title="Sattle History" type="button" onclick='window.location.href="{{url('admin/providers/sattlehistory')}}/{{ $user->id }}"'> <span class="fa fa-list" aria-hidden="true"></span> </button>  </span>
 

		<span data-placement="top" data-toggle="tooltip" title="Free Schedule">
      <button class="btn btn-link btn-xs" data-title="Free Schedule" type="button" onclick='window.location.href="{{url('admin/providers/freeschedule')}}/{{ $user->id }}"'> <span class="fa fa-clock-o" aria-hidden="true"></span> </button>  </span>


      
      <span data-placement="top" data-toggle="tooltip" title="Job History">
      <button class="btn btn-link btn-xs" data-title="History" type="button" onclick='window.location.href="{{url('admin/providers/jobhistory')}}/{{ $user->id }}"'> <span class="fa fa-history" aria-hidden="true"></span> </button>

      <span data-placement="top" data-toggle="tooltip" title="Rating History">
					<button class="btn btn-link btn-xs" data-title="Rating History" type="button" onclick='window.location.href="{{url('admin/providers/ratinghistory')}}/{{ $user->id }}"'>
				
					<span class="fa fa-star-o" aria-hidden="true"></span>
				
					</button></span>
					
      </span> {{Form::open(array('method'=>'DELETE','id'=>'form'.$user->id, 'route' => array('providers.destroy', $user->id)))}} <span data-placement="top" data-toggle="tooltip" title="Delete">
      <button class="btn btn-link btn-xs" data-title="Delete" type="submit" > <span class="glyphicon glyphicon-trash"></span></button>
      </span> {{Form::close()}} 
      <script>
		$(document).ready(function(){
			$('#form{{$user->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$user->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'providers';
								
							  }
							});
					}
				});
			});

		});
	
		
		</script></td>
  </tr>
  @endforeach
  <tr>
    <td colspan="30">{{ $providers->appends(Request::except('page'))->links() }}</td>
  </tr>
    </tbody>
  
</table>
<div class="floatingScroll-table"></div>
</div>
</div>
@endsection

{{-- Scripts --}}
@section('scripts')
<script>

function approveprovider(uid,status){
if(status=='0') var msg='Are you sure you want to disapprove?';
if(status=='1') var msg='Are you sure you want to approve?';
	BootstrapDialog.confirm(msg, function(result){
					if(result) {

							window.location.href="{{url('admin/providers/statusupdate')}}/"+uid+"/"+status

							}
	});

}
	

$(".fs-whatwg").floatingScroll();


$(".fs-whatwg-cols").on("change", "input[type='checkbox']", function (e) {
    var $checkboxes = $(e.delegateTarget).find("input[type='checkbox']"),
        index;
    if ($checkboxes.filter(":checked").length) {
        index = $checkboxes.index(e.target);
        if (index > -1) {
            $("#fs-whatwg").find("th, td")
                .filter(":nth-child(" + (index + 2) + ")")
                .toggleClass("hidden");
        }
    } else {
        $checkboxes.prop("checked", true);
        $("#fs-whatwg").find("th.hidden, td.hidden").removeClass("hidden");
    }

    $(".fs-whatwg").floatingScroll("update");
});
         
	
/**/


	 $(function () {
                $('#free_from_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#free_to_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});

              $('#revenue_from_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#revenue_to_datepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
             
                
            });


function searchDate(){
		window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&free_from_date='+$("#free_from_date").val()+'&free_to_date='+$("#free_to_date").val();

	}
	
	function setFilter(value,field){

		window.location.href='{{url('admin/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page')])}}'+'&'+field+'='+value;
	}
	
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 

@endsection 
