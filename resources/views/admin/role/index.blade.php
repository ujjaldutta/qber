@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Role Management
            <div class="pull-right">
                <div class="pull-right">
                    <a href="javascript:void(0)"
                       class="btn btn-sm  btn-primary" data-toggle="modal" data-target="#edit"><span
                                class="glyphicon glyphicon-plus-sign"></span> Add New</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            
            <th>Role Name</th>
            <th>Access Component</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($roles as $role)
        <tr>
			<td>{{ $role->name }}</td>
			<td>
				@foreach ($role->roleaccess as $access)
				{{ $access->component_code}} 
				@endforeach
				</td>
			
			<td>
		
<span data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/role')}}/{{ $role->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>


			 {!! Form::open(array('url' => url('admin/role/destroyrole'), 'method' => 'post', 'class' => 'bf','id'=>'form'.$role->id)) !!}
			  {!! Form::hidden('id', $role->id, array('class' => 'form-control')) !!}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>
		$(document).ready(function(){
			$('#form{{$role->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "post",
							  url: url,
							  data:$('#form{{$role->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'role';
								
							  }
							});
					}
				});
			});

		});
		</script>

		

			</td>

        </tr>

        
    @endforeach
<tr>
<td colspan="4">{{ $roles->links() }}</td>
</tr>


        </tbody>
    </table>












<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Add Role</h4>
      </div>
      {!! Form::open(array('url' => url('admin/role/save'), 'method' => 'post', 'id'=>'formadd','class' => 'bf', 'files'=> true)) !!}
   
       <div class="modal-body">
         
				<div class="form-group">
				
				

				{!! Form::text('name', null, array('class' => 'form-control','placeholder' => 'Role Name','required' => 'required')) !!}
				</div>
       
      </div>
      <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Save</button>
      </div>
     {!! Form::close() !!}


     <script>
		
		$(document).ready(function(){
			$('#formadd').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
						$.ajax({
							  type: "POST",
							  url: url,
							  data:$('#formadd').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'role';
								
							  }
							});
					
				});
			});

	


     </script>
      
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>




    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
