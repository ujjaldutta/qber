@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
       {!! $title !!} : {{$role->name}}

       <div class="pull-right">
                <div class="pull-right">
                    <a href="javascript:void(0)"
                       class="btn btn-sm  btn-primary" data-toggle="modal" data-target="#edit"><span
                                class="glyphicon glyphicon-plus-sign"  ></span> Add New</a>
                </div>
            </div>
            
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-12">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						@endif



						<table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Role Name</th>
            <th>Access Component</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($role->roleaccess as $access)
        <tr>
			<td>{{ $role->name }}</td>
			<td>
				
				{{ $access->component_code}} 
				
				</td>
			
			<td>


			
					 {{Form::open(array('method'=>'DELETE','id'=>'form'.$access->id, 'route' => array('role.destroy', $access->id)))}}
		
		<span data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-link btn-xs" data-title="Delete" type="submit" >
			<span class="glyphicon glyphicon-trash"></span></button></span>
		
		{{Form::close()}}
		<script>
		$(document).ready(function(){
			$('#form{{$access->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to delete?', function(result){
					if(result) {
						
						$.ajax({
							  type: "DELETE",
							  url: url,
							  data:$('#form{{$access->id}}').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'edit';
								
							  }
							});
					}
				});
			});

		});
		</script>
		


			</td>

        </tr>

        
    @endforeach


        </tbody>
    </table>



   
   </div>
</div>






<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Add Component</h4>
      </div>
      {!! Form::open(array('url' => url('admin/role'), 'method' => 'post', 'id'=>'formadd','class' => 'bf', 'files'=> true)) !!}
      {!! Form::hidden('role_id', $role->id, array('class' => 'form-control')) !!}
       <div class="modal-body">
         
				<div class="form-group">
				
				 {!! Form::select('component_code', $code, array('class' => 'form-control','placeholder' => 'Controller Name','required' => 'required')) !!}

				
				</div>
       
      </div>
      <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Save</button>
      </div>
     {!! Form::close() !!}


     <script>
		
		$(document).ready(function(){
			$('#formadd').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
						$.ajax({
							  type: "POST",
							  url: url,
							  data:$('#formadd').serialize(),
							  success: function(result) {
								   if (result > 0) window.location = 'edit';
								
							  }
							});
					
				});
			});

	


     </script>
      
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>


    
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection     
