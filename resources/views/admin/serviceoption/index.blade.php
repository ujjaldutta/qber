@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Service Option List
            
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Service Type</th>
            <th>SUV or Sedan options</th>
            <th>Value</th>
            <th>Duration</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($serviceoption as $set)
        <tr>
			<td>{{ $set->servicetype->name }}</td>
			<td>@if($set->service_option_id==1)
				Suv - 
				@else
				Sedan -
				@endif

			{{ $set->name }}</td>
			<td>{{ $set->cost }}</td>
			<td>{{ $set->duration }}</td>
			<td>
		
<span data-placement="top" data-toggle="tooltip" title="Active"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('admin/service-option')}}/{{ $set->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>


	

    
			</td>

        </tr>

        
    @endforeach
<tr>

</tr>


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
