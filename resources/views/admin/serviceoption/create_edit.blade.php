@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-12">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif


{!! Form::model($optiondetails, array('url' => url('admin/service-option') . '/' . $optiondetails->id, 'method' => 'put', 'class' => 'bf', 'files'=> true)) !!}

   
        
     <div class="row">   
					<!--<div class="col-lg-6  col-md-6">
					 <div class="form-group ">
						{!! Form::label('type_id', 'Service type', array('class' => 'control-label')) !!}
						<div class="controls">
							{!! Form::select('type_id', $servicetype) !!}
							<span class="help-block">{{ $errors->first('type_id', ':message') }}</span>
						</div>
					</div>
					</div>

					<div class="col-lg-6  col-md-6">
					 <div class="form-group ">
						{!! Form::label('service_option_id', 'Service Options', array('class' => 'control-label')) !!}
						<div class="controls">
							{!! Form::select('service_option_id', $serviceoption) !!}
							<span class="help-block">{{ $errors->first('service_option_id', ':message') }}</span>
						</div>
					</div>
					</div>-->


				 <div class="col-lg-6  col-md-6">
					<div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
							{!! Form::label('name', 'Name', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('name', null, array('class' => 'form-control','readonly'=>"readonly")) !!}
								<span class="help-block">{{ $errors->first('name', ':message') }}</span>
							</div>
						</div>
					</div>

					<div class="col-lg-6  col-md-6">
					<div class="form-group  {{ $errors->has('cost') ? 'has-error' : '' }}">
							{!! Form::label('cost', 'Cost', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('cost', null, array('class' => 'form-control')) !!}
								<span class="help-block">{{ $errors->first('cost', ':message') }}</span>
							</div>
						</div>
					</div>
					
					<div class="col-lg-6  col-md-6">
					<div class="form-group  {{ $errors->has('duration') ? 'has-error' : '' }}">
							{!! Form::label('duration', 'Duration', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('duration', null, array('class' => 'form-control')) !!}
								<span class="help-block">{{ $errors->first('duration', ':message') }}</span>
							</div>
						</div>
					</div>


					
  </div>

   

      


        <div class="form-group">
						<div>
							
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>

							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/service-option')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Back
							</button>
						</div>
					</div>

					
    

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#type").select2()
            });
        </script>
 @endsection       
