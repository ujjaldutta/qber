@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
   <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif


{!! Form::open(array('url' => url('company/providers/sendinvite'), 'method' => 'post', 'class' => 'bf')) !!}

   
        
       
        
       

        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('phone', null, array('class' => 'form-control','required' => 'required','id'=>'phone')) !!}
                <span class="help-block" id="phone-help">{{ $errors->first('phone', ':message') }}</span>
            </div>
        </div>

      

       

        <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/providers')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Back
							</button>
						
							<button type="submit" class="btn btn-sm btn-success" >
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Send
							</button>
						</div>
					</div>

    

     {!! Form::close() !!}
   </div>
</div>


    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script>
	

            $(function () {
                $( "#phone" ).autocomplete({
						source: '{{url('company/providers/ajax-list')}}'
					});
            });
</script>
@endsection
