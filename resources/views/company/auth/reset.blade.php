@extends('layouts.app')

@section('content')
<div class="login-panel">

 <div class="row">
    <p class="logo"></p>
        <div class="page-header">
            <h2>Reset Password</h2>
        </div>
      </div>  
    

    <div class="container-fluid">

		    @if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif				
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <!--<div class="panel-heading">Reset Password</div>-->
                    <div class="panel-body">
                        {!! Form::open(array('url' => url('company/password/reset'), 'method' => 'post', 'files'=> true)) !!}
                        {!! Form::hidden('_token', csrf_token() ) !!}
                        <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label('email', "E-Mail Address", array('class' => 'control-label')) !!}
                            <div class="controls">
                                {!! Form::text('email', null, array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div> 
@endsection
