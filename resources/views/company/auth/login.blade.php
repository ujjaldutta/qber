@extends('layouts.app')

{{-- Content --}}
@section('content')
<div class="login-panel login-company r">
    <div class="row">
    <p class="logo"></p>
        <div class="page-header">
            <h2>Company Login</h2>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            {!! Form::open(array('url' => url('company/auth/login'), 'method' => 'post', 'files'=> true)) !!}
            <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('email', "E-Mail Address", array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('email', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                </div>
            </div>
            <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                {!! Form::label('password', "Password", array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::password('password', array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 ">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
               <div class="col-md-6"> 
                 <a class="forgot" href="{{ url('/company/auth/forgot-password') }}">Forgot Your Password?</a>
              </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btns">
                        Login
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection

{{-- Scripts --}}
@section('scripts')
@endsection
