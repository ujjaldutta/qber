@extends('layouts.app')
	@section('title') Change Admin Password @parent
@endsection
{{-- Content --}}
@section('content')
<div class="login-panel">
<div class="row">
<p class="logo"></p>
<div class="page-header">
	<h1>Change Admin Password</h1>
</div>
</div>
    @if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif
						
{!! Form::open(array('url' => url('company/update-password'), 'method' => 'post', 'files'=> true)) !!}
{!! Form::hidden('_token', csrf_token() ) !!}
{!! Form::hidden('key', $key ) !!}
	<fieldset>
        <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
            {!! Form::label('quantity', "Password", array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::password('password', array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('password', ':message') }}</span>
            </div>
        </div>
        <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            {!! Form::label('quantity', "Confirm Password", array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
            </div>
        </div>
		<div class="form-actions form-group">
			<button type="submit" class="btn btn-primary btns"> Update </button>
		</div>
	</fieldset>
    </div>
{!! Form::close() !!}
@endsection
