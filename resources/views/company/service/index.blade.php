@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Service List
            
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
			<tr><th colspan="3">Filter By : Service Type
			<select name="servicetype" onchange="window.location.href='{{url('company/service/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality'),'status'=>Request::input('status'),'minutes'=>Request::input('minutes')])}}'+'&servicetype='+this.value"><option value="">All Service Type</option>
			@foreach($servicetype as $type)
				<option value="{{$type->id}}" {{ Request::input('servicetype')==$type->id ? 'selected' : '' }} >{{$type->name}}</option>
			 @endforeach
			</select>
<br />
			Latest Record 
				<select name="minutes" onchange="window.location.href='{{url('company/service/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality'),'status'=>Request::input('status')])}}'+'&minutes='+this.value"><option value="">All Records</option>
				<option value="15" {{ Request::input('minutes')=="15" ? 'selected' : '' }}>15 Mins</option>
				<option value="30" {{ Request::input('minutes')=="30" ? 'selected' : '' }}>30 Mins</option>
				<option value="60" {{ Request::input('minutes')=="60" ? 'selected' : '' }}>1 Hr</option>
				<option value="120" {{ Request::input('minutes')=="120" ? 'selected' : '' }}>2 Hrs</option>
				<option value="240" {{ Request::input('minutes')=="240" ? 'selected' : '' }}>4 Hrs</option>
			
			</select>

			</th>

			<th colspan="3">
				Nationality
			<select name="nationality" onchange="window.location.href='{{url('company/service/') . '?'
			. http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),
			'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'minutes'=>Request::input('minutes'),
			'status'=>Request::input('status')])}}'+'&nationality='+this.value"><option value="">All Country</option>
			@foreach($nationality as $country)
				<option value="{{$country->code}}" {{ Request::input('nationality')==$country->code ? 'selected' : '' }} >{{$country->name}}</option>
			 @endforeach
			</select>



			Status
			<select name="status" onchange="window.location.href='{{url('company/service/') . '?'
			. http_build_query(['sort' => Request::input('sort'),
			'order' => Request::input('order'),'page'=>Request::input('page'),
			'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes')])}}'+'&status='+this.value"><option value="">All Status</option>
			@foreach($servicestatus as $status)
				<option value="{{$status->id}}" {{ Request::input('status')==$status->id ? 'selected' : '' }} >{{$status->name}}</option>
			 @endforeach
			</select>


			</th></tr>
        <tr>
           
			<th>Service Name
				<a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'job_description', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>

			</th>

			 <th>Phone
				<a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>
            
            <th>Date
				<a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'request_date', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'request_date', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>
            <th>Nationality
			<a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'nationality', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'nationality', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>
            <th>Amount

				<a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>
            <th>Status
			<a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'service_status_id', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/service/') . '?' . http_build_query(['sort' => 'service_status_id', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality'),'minutes'=>Request::input('minutes'),'status'=>Request::input('status')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($services as $service)
        <tr>
			
			<td>{{ $service->job_description }}</td>
			<td>{{ $service->phone }}</td>
			<td>{{ $service->request_date }}</td>
			<td>{{ $service->nationality}}</td>
			
			<td>${{ (float)$service->amount }}</td>
			
			<td>{{ strtoupper($service->name) }}</td>
			<td>
		
<span data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-link btn-xs" data-title="Edit" type="button" onclick='window.location.href="{{url('company/service')}}/{{ $service->id }}/edit"'>
			<span class="glyphicon glyphicon-pencil"></span></button></span>

		

    
			</td>

        </tr>

        
    @endforeach
<tr>
<td colspan="6">{{ $services->appends(Request::except('page'))->links() }}</td>
</tr>


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
