<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@section('title') Qber App Admin @show</title>
    @section('meta_keywords')
        <meta name="keywords" content=" Qber App"/>
    @show @section('meta_author')
        <meta name="author" content=" Qber App"/>
    @show @section('meta_description')
        <meta name="description"
              content="Qber App Backend."/>
    @show
    <link rel="icon" type="image/png" href="{{ asset('css/images/favicon-32x32.png') }}" sizes="32x32" />
<link rel="icon" type="image/png" href="{{ asset('css/images/favicon-16x16.png') }}" sizes="16x16" />
		<link href="{{ asset('css/site.css') }}" rel="stylesheet">
        <script src="{{ asset('js/site.js') }}"></script>
        <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <script src="{{ asset('js/moment.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        

    @yield('styles')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

 <!--   <link rel="shortcut icon" href="{!! asset('assets/site/ico/favicon.ico')  !!} ">-->
</head>
<body>


<div class="container">
@yield('content')
</div>


<!-- Scripts -->
@yield('scripts')

</body>
</html>
