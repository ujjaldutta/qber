@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('content')
   <h3>
        {{$title}}
    </h3>
    <div class="row">
		<div class="col-lg-12 col-md-6">

			@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif
						
{!! Form::open(array('url' => url('request-service/save'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
				<div class="row">
					
							<div class="col-lg-3 "><div class="form-group">
							{!! Form::label('customer', 'Customer', array('class' => 'control-label')) !!}
							<div class="controls">
							    {!! Form::select('customer_id', $customer, array('class' => 'form-control','required' => 'required')) !!}
							    </div>
							
							</div></div>

							<div class="col-lg-3 "><div class="form-group">
							{!! Form::label('service', 'Service Type', array('class' => 'control-label')) !!}
							<div class="controls">			{!! Form::select('type_id', $service, array('class' => 'form-control','required' => 'required')) !!}		</div>
							
							</div></div>

							<!--<div class="col-lg-3 "><div class="form-group">
							{!! Form::label('jobtime', 'Expected Jobtime', array('class' => 'control-label')) !!}
							<div class="controls">
							    {!! Form::number('arrival_witin_time', null, array('class' => 'form-control','required' => 'required')) !!}
							    </div>
							
							</div>

							</div>-->


							<div class="col-lg-3 "><div class="form-group">
							{!! Form::label('jobdesc', 'Job Detail', array('class' => 'control-label')) !!}
							<div class="controls">
							    {!! Form::text('job_description', null, array('class' => 'form-control','required' => 'required')) !!}
							    </div>
							
							</div>

							</div>
				</div>
				<div class="row">
					
						<div class="col-lg-3 "><div class="form-group">
						{!! Form::label('cost', 'Expected Cost', array('class' => 'control-label')) !!}
						<div class="controls">			{!! Form::number('expected_cost', null, array('class' => 'form-control','required' => 'required')) !!}		</div>
						
						</div></div>
						<div class="col-lg-3 ">
							<div class="form-group">
								{!! Form::label('date', 'Date', array('class' => 'control-label')) !!}
							
								<div class='input-group date' >									
									<div class='input-group date' id='datetimepicker2'>
									
									{!! Form::text('request_date', null, array('class'=>'form-control','required' => 'required'))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>

									

  
								</div>
							</div>
						</div>
						<div class="col-lg-3 "><div class="form-group">
						{!! Form::label('Longitude', 'Longitude', array('class' => 'control-label')) !!}
						<div class="controls">			{!! Form::text('longitude', null, array('class' => 'form-control','id'=>'lngbox','required' => 'required')) !!}		</div>
						
						</div></div>
						<div class="col-lg-3 ">

							<div class="form-group">
						{!! Form::label('Latitude', 'Latitude', array('class' => 'control-label')) !!}
						<div class="controls">			{!! Form::text('latitude', null, array('class' => 'form-control','id'=>'latbox','required' => 'required')) !!}		</div>
						
						</div></div>


						
				</div>

				<div class="row">
					<div class="col-lg-3 ">
								<div class="form-group">
							{!! Form::label('nationality', 'Nationality', array('class' => 'control-label')) !!}
							<div class="controls">			{!! Form::select('nationality', $country,$selectedCountry, array('class' => 'form-control','required' => 'required')) !!}		</div>
							
							</div>
					</div>



					<div class="col-lg-3 ">
							<div class="form-group">
								{!! Form::label('from_time', 'From Time', array('class' => 'control-label')) !!}
							
								<div class='input-group date' >									
									<div class='input-group date' id='datetimepicker3'>
									
									{!! Form::text('from_time', null, array('class'=>'form-control','required' => 'required'))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>

									

  
								</div>
							</div>
						</div>



						<div class="col-lg-3 ">
							<div class="form-group">
								{!! Form::label('to_time', 'To Time', array('class' => 'control-label')) !!}
							
								<div class='input-group date' >									
									<div class='input-group date' id='datetimepicker4'>
									
									{!! Form::text('to_time', null, array('class'=>'form-control','required' => 'required'))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>

									

  
								</div>
							</div>
						</div>

							
				</div>

				<div class="row">
					<div class="col-lg-8">
					<div id="map"></div>
					</div>
				</div>

			 <div class="row">
				 <div class="form-group">
						<div class="col-lg-12">
							<div class="pull-right">
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Submit
							</button>
							</div>						
						</div>
					</div>
			</div>

 {!! Form::close() !!}
      </div>
</div>
<div class="row">
Demo front interface
</div>
@endsection
<style>
 #map {
        height: 100%;
      }

</style>
{{-- Scripts --}}
@section('scripts')
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k&callback=initialize">
    </script>
 <script>

      // global "map" variable
    var map = null;
    var marker = null;
var infowindow;
    // popup window for pin, if in use
   

    // A function to create the marker and set up the event window function 
    function createMarker(latlng, name, html) {

    var contentString = html;

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        zIndex: Math.round(latlng.lat()*-100000)<<5
        });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
        });

    google.maps.event.trigger(marker, 'click');    
    return marker;

}

function initialize() {

	 infowindow = new google.maps.InfoWindow({ 
        size: new google.maps.Size(150,50)
        });

    // the location of the initial pin
    var myLatlng = new google.maps.LatLng(25.29161,51.53044);

    // create the map
    var myOptions = {
        zoom: 19,
        center: myLatlng,
       
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById("map"), myOptions);

    // establish the initial marker/pin
    var image = '/images/googlepins/pin2.png';  
    marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      //icon: image,
      animation: google.maps.Animation.DROP,

      title:"Property Location"
    });

    // establish the initial div form fields
    formlat = document.getElementById("latbox").value = myLatlng.lat();
    formlng = document.getElementById("lngbox").value = myLatlng.lng();

    // close popup window
    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
        });

    // removing old markers/pins
    google.maps.event.addListener(map, 'click', function(event) {
        //call function to create marker
         if (marker) {
            marker.setMap(null);
            marker = null;
         }

        // Information for popup window if you so chose to have one
        /*
         marker = createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng);
        */

       // var image = '/images/googlepins/pin2.png';
        var myLatLng = event.latLng ;
        /*  
        var marker = new google.maps.Marker({
            by removing the 'var' subsquent pin placement removes the old pin icon
        */
        marker = new google.maps.Marker({   
            position: myLatLng,
            map: map,
            //icon: image,
            animation: google.maps.Animation.DROP,

            title:"Property Location"
        });

        // populate the form fields with lat & lng 
        formlat = document.getElementById("latbox").value = event.latLng.lat();
        formlng = document.getElementById("lngbox").value = event.latLng.lng();

    });

}
      $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#datetimepicker4').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
            });
    </script>
  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
