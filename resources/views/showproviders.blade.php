@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('content')
   <h3>
        {{$title}}
    </h3>
    <div class="row">
		<div class="col-lg-12 col-md-6">

			@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

						{!! Form::open(array('url' => url('get-providers'), 'method' => 'post', 'class' => 'bf','id'=>'service')) !!}
					<div class="row">
						<div class="col-lg-3 ">
									<div class="form-group">
										{!! Form::label('service', 'Service List', array('class' => 'control-label')) !!}
										<div class="controls">
											
											<select class="form-control" name="service_id" id="service_id" required="required">
												@foreach($service as $key=>$item)
												  
												  <option value="{{$key}}">{{$item}}</option>
												@endforeach
											  </select>
  
										</div>
										
								</div>
							</div>

							<div class="col-lg-3 ">
									<button type="button" class="btn btn-sm btn-success" id="showprovider"><span class="glyphicon glyphicon-ok-circle"></span>Display</button>
							</div>


						</div>
					{!! Form::close() !!}


	<div class="row">
					<div class="col-lg-8">
					<div id="map_canvas"></div>
					</div>
					</div>


      </div>
</div>
<div class="row">
Demo front interface
</div>
@endsection
<style>
 #map_canvas {
        height: 100%;
      }

</style>
{{-- Scripts --}}
@section('scripts')
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k&callback=initialize">
    </script>
 <script>

	 $(document).ready(function(){
			$('#showprovider').click(function() {
				   $.ajax({
					  url: '{!! url("get-providers") !!}',
					   type: 'POST',
					  data:$("#service").serialize() ,
					 
					  success: function(data) {
						addmarker(data);
					  },
					 
				   });
				});

		 })

function booknow(providerid){
$.ajax({
					  url: '{!! url("accept-service") !!}',
					   type: 'POST',
					  data:{service:$("#service_id").val(),provider:providerid,_token:'{{ csrf_token() }}'} ,
					 
					  success: function(data) {
						  alert(data)
						window.location.reload;
					  },
					 
				   });

}

function addmarker(data){
var bounds = new google.maps.LatLngBounds();
 // Multiple Markers
    /*var markers = [
        ['London Eye, London', 51.503454,-0.119562],
        ['Palace of Westminster, London', 51.499633,-0.124755]
    ];*/
    var markers=[];
    var infoWindowContent=[];
    var sid = document.getElementById("service_id").value;
    
    for( i = 0; i < data.length; i++ ) {
		var content=[data[i]['name'],data[i]['current_latitude'],data[i]['current_longitude']]
		markers.push(content);


		content= ['<div class="info_content">' +
        '<h3>'+data[i]['name']+'</h3>' +
        '<p>'+data[i]['profile']['service_details']+'</p>' +
        '<p><input type="button" onclick="booknow('+data[i]['id']+')" value="book now" /></p>' +
        '</div>'];
        
        infoWindowContent.push(content);
	}
                    
    // Info Window Content
 /*   var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>London Eye</h3>' +
        '<p>The London Eye is a giant Ferris wheel situated on the banks of the River Thames. The entire structure is 135 metres (443 ft) tall and the wheel has a diameter of 120 metres (394 ft).</p>' +        '</div>'],
        ['<div class="info_content">' +
        '<h3>Palace of Westminster</h3>' +
        '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
        '</div>']
    ];*/
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });

}
 var map;

function initialize() {
   
    var myLatlng = new google.maps.LatLng(25.29161,51.53044);
    var mapOptions = {
        
        center: myLatlng,
        zoom: 10,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    
    
}

      $(function () {
                
                //$('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
            });
    </script>
  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
