@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('content')
<div class="login-panel">
  <p class="logo"></p>
  <div class="page-header">
    <h2 class="text-center">{{$title}}</h2>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-6"> @if(Session::has('message'))
      <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
      @endif
      
      {!! Form::open(array('url' => url('customer/savepassword'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
      {!! Form::hidden('code', $code ) !!}
      {!! Form::hidden('phone', $phone) !!}
      <div class="row">
        <div class="col-lg-12 ">
          <div class="form-group"> {!! Form::label('password', 'Password', array('class' => 'control-label ')) !!}
            <div class="controls">
              <div class="form-control">{!! Form::password('password', null, array('class' => 'form-control','required' => 'required')) !!}</div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 ">
          <div class="form-group"> {!! Form::label('confirm_password', 'Confirm Password', array('class' => 'control-label')) !!}
            <div class="controls">
              <div class="form-control">{!! Form::password('confirm_password', null, array('class' => 'form-control','required' => 'required')) !!}</div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary btns btn-success"> <span class="glyphicon glyphicon-ok-circle"></span> &nbsp;Submit </button>
          </div>
        </div>
      </div>
      {!! Form::close() !!} </div>
  </div>

</div>
@endsection
{{-- Scripts --}}
@section('scripts') 
<script>

    </script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script> 
@endsection 