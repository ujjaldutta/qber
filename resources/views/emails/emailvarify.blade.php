<!doctype html>
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta charset="utf-8">
<title>{{$title}}</title>

</head>

<body>
<center>
  <style>
@import url('http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
</style>
  <table width="620" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td style="background-repeat:no-repeat;background-position:0 0;background-size:cover;padding-left:30px;padding-top:30px;padding-right:30px;"><table width="560" border="0" cellpadding="0" cellspacing="0" style="">
          <thead>
            <tr>
              <td align="center" colspan="2" style="padding-bottom:20px; padding-top:20px;" bgcolor="#63003d">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td>
                        <a href="" target="_blank"><img src="{{ asset('css/images/logo_small.png') }}" alt=""></a>
                      </td>
                    </tr>
                </table>
              
              </td>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#63003d">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td height="40"></td>
            </tr>
            <tr>
              <td style="padding-left:30px;padding-right:30px; padding-top: 15px;" bgcolor="#f8f8f8"><table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                 <tr>
                  <td align="left">
                  <p style="font-size:20px;line-height:22px;color:#c59d5f;font-family:'Lato', sans-serif;font-weight:700;display:block;text-align:left;margin:0;margin-bottom:20px;display:block;">Dear User,</p>
                  </td>
                  </tr> 
                  <tr>
                    <td align="center">
                      <p style="font-size:15px; line-height:20px;color:#4e4e4e;font-family:'Lato', sans-serif;font-weight:400;margin:0;margin-bottom:25px;display:block;text-align:left">Please click on the below link Or open the link to other tab to verify your account</p>
                      <div style="width:96%; font-size:16px;line-height:22px;color:#7d064f;font-family:'Lato', sans-serif;font-weight:700;background:#fff;border:1px solid #7d064f;border-radius:25px;  display: inline-block;margin-bottom:30px; ">
                        <a href="{{ $link }}" target="_blank" style="margin:0;margin-top:13px;margin-bottom:13px;margin-left:25px;margin-right:25px; text-decoration: none;color:#7d064f; line-height: 44px;"> 
                        <span>{{$link}}</span>
                        </a>
                        
                      </div>
                      </td>
                  </tr>
                  
                  
                </table></td>
            </tr>
            <tr>
              <td height="40"></td>
            </tr>
              </table>
              
              </td>
            </tr>
          </tbody>
          <tfoot>
           <tr>
              <td align="center" colspan="2" style="padding: 22px; border-top: 5px solid #FFF;" bgcolor="#63003d">
                <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td align="center" style="font-size: 18px;color: #ffffff;line-height: 22px;font-weight: 300;font-family: 'Lato', sans-serif;padding-top: 17px;padding-bottom: 17px;">Thank you, <a href="#" style="font-weight:700;color:#fff;text-decoration:none;font-weight: 400;">Qber App Team </a></td>
            </tr>
            </table>
            </td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
  </table>
</center>
</body>
</html>


