# Qber App

The goal of this project is to create a one stop solution to cover customer needs, there will be one app for general customers and one for service providers. A web panel for company admins, and a super admin web panel. The system will allow customers to review and find best available providers. It will help providers get more jobs with more efficiency. System involves complex match making formula, multiple appointments scheduling, live tracking, rating, etc.

# Platform
iOS (iPhone only) + Android + Web (Laravel + Mysql)


# Sign Up/Registration

## Customer 
Users launch the app and are able to “Register” or “Skip”. Skipping user may access the app and view all functionalities but are unable to place requests/orders. Skipping Users will receive notice to register when they try submitting a request. 

## Customers register with following
Phone Number:
Email:
Password:
Password Confirmation:
Agree to terms
Automatic sms verification is made (Customer does not type the sms code), customer can now login with either email + password or phone + password
Provider
Providers registering for the app will see graphic list of available services. Provider selects service and taps become provider button. Provider registers by a form submitted to super admin for approval, the form has multiple questions with ability to attach images 

# Customer App

Customer app main screen will have the following panels

Service list (Main screen)
Currently booked appointments panel
Profile page which include name, age, gender, education, income, etc
Appointment history panel
Help panel with embedded videos 

# General Flow of placing a service ( the flow is simple and similar to each and every service )


1. When a customer launches or logs in the app, a 3 second splash screen will appear, the screen will show a map of the user city (example Doha). the map will have pins indicating all the locations of all/certain services performed through the app in last 24 hours. 
Super admin can edit the splash screen texts  

2. Customer will see a graphic list of services available, customer selects the service. (initially most services will be hidden and super admin is able to hide and display services available for the customer)

3. Customer selects the service and inputs 3 main things:
Details (depend on service), details may include the following,
Car wash type
Number of cars
Expected job time 
Expected cost 
Nationality of provider 
Team size
Attach Picture of problem
Grocery picks up location or grocery list
Destination (taxi service)
Note these details are just information carried over to the provider, they do not effect the flow of the system


Location 
Map pin drop selection with location search functionality
Pre saved locations
Current location
In addition to typed villa/building number


Time Range and date (time range of when provider must arrive)

Arrival time range minimum range will be different for each service, example plumber minimum arrival range is 30 minutes, taxi service minimum range is 5 minutes.
Customer can book future dates of maximum 1 week
The time range graphical interface will be finalized later



4. Customer confirms all data is correct and submits, request will be sent to system

5. The system shall bring a list or map view of few selected service providers based on formula discussed below, list could range between 2 to 20 providers for example, admin will decide the number of returned providers per service, 

If the customer latest allowable time of provider arrival is within 3 hour then the customer will see a map view of providers instead of a list.



6. The customer shall receive a list/map of selected providers and is able to see
the name of provider
current distance away
rating out of 5 and how many people rated the service provider

7. The customer Is able to tap and see the profile of the provider, the profile can include service description, price for specific jobs, a few photos etc. The profile will also include past job history with previous comments and ratings 



8. The customer chooses one provider and taps confirm, the job is started and the customer lands on the detailed appointment screen. which shall include service details and call provider button, there will be a live track button, and edit appointment button

The live track button activates when the service provider taps “heading” button.


9. By default, job is started, customer will receive a confirmation or cancellation from provider within 3 minutes. After confirmation, customer have X minutes to cancel without penalty. Customer has to give reason for cancellation. 
Any editing to the appointment by customer will notify the provider, provider will be able to approve the edit, or cancel the whole booking without penalty. Minor map pin location editing of less than 200 meter difference will be allowed without notice to provider.

There will be a cancellation/editing time for the customer ranging from no penalty for “X” amount of time before service due, moderate penalty for “Y” amount of time before service and higher penalty for “Z” amount of time before service
The penalty and timings will be different for each service category and will be adjustable from the admin panel

There will be a mechanism to prevent customer from repeated cancelling, example: if customer cancel service more than X times in Y minutes then he will be banned from service for Z minutes, numbers adjustable by admin


Customer cannot have more than 1 active confirmed booking of each service


10. Customer receives notice when provider is heading to customer
11. Customer receives automatic notice when provider is 2 minutes away
12. Customer will receive work start notice 
13. After work is done, customer receive job done notice.

There will be an open dispute button in the appointment history list, the button will open a list of options and enables customer to write details. Once its sent, it will notify admin.

14. Customer gets a popup to rate service provider
15. Customer receives a custom splash screen




# Match Making Formula

System shall perform calculations to select a few service providers and present them to the customer. The system will analyze service providers following details:
Location (current location in case service provider has no appointments, future locations in case provider has multiple appointments)
Availability 
Ratings
Etc
Initially it will be just rating and location


# (Provider Flow)

Service Provider have following panels
Main screen will show provider available time range and the “Ready to Serve” button
Service Profile/Options
Task/Appointment list
Past Appointments
Balance Settling page
Help panel

The registered provider can select service options, and make a service profile page. Options depend on service and may include the following
Team size
Nationality
Ability to speak Arabic or not
Have a car or have to call friend/taxi?
Max purchasing limit (grocery service)
List of shops that willing to go to (grocery service)

restricting service to certain geo areas 

The service provider has ability to set working days and hours (Time range to accept jobs) with ability to block certain times as busy or break time

1. Service provider pushes ready to serve button

2. Once a customer chooses a provider, the provider receives job start notice and can see
The area name of the service location
The service date and time
The service details
The requesters rating 
Ability to call customer
The provider has a certain duration to cancel or confirm, otherwise booking is automatically cancelled, if provider does not respond, system will assume has away and will not receive jobs and until confirming availability


On the job confirmation screen, there will be an availability checking button that helps providers with multiple appointments immediately know if they can receive the job. The details are discussed below on the Multiple Appointments Logic

3. Once confirmed, the provider is to set expected job duration the system will automatically set provider busy for that time.


There will be a cancellation policy for the providers ranging from moderate penalty for “X” amount of time before service due, and high penalty for “Y” amount of time before service due, the penalty and timing are adjustable by super admin


Multiple Appointments Logic, the system will optimize providers schedule with most jobs possible without delaying other appointments. the match making system will take into account the 
1. Allowable time range of arrival that the customer set
2. Current Providers Schedule and Appointments job duration (the provider can set a more accurate job duration when receiving the job confirmation)
3. The estimated job duration that the customer set (before sending the request)
In case customer does not set estimated time, system will take minimum service duration example: (30 minutes minimum for home saloon, 10 minutes minimum for electrician)
Some services will have job duration time set from options selected such as carwash, example (30 min for SUV wash, 20 min for Sedan Wash)
4. Google map estimated driving time between appointment location ( plus certain buffer)

Example Scenario: Time now is 13:10, a car washer is currently doing a job for “Appointment 1” at location A, provider last updated the expected job finish time to 13:25. Provider also has another confirmed “Appointment 2” with following details
Allowable Arriving range 13:00-14:30
Expected Job duration: 40 minutes
Location B, 25 min drive time from location A
A customer is about to book a carwash service and is currently inputting the details, the following details is what he entered
Allowable arriving range 13:00-14:00
Job duration 20 min
Location C 
The Customer clicks “Find provider”. Now if system calculated that this appointment will not delay “Appointment 2” then it will display the provider to the customer. This will be done by simple backend calculations as following:
System sees that location C is is 10 minute away from from location A, and Location B is 35 minutes away from Location C, Thus system calculates that provider is able to finish appointment 1 at 13:25, arrive location C at 13:35, Finish job of location C at 13:55, Arrive “Appointment 2” at location B in 14:30 which is within allowable time range. THUS, this provider shall be displayed to the customer once he clicks find provider button. 
If Location B was more than 35 minutes away from Location C, or If the request job duration was more than 20 mins, then customer would not see this provider in the search.

4. When ready, Provider taps “On the way” button

System will remind provider to tap heading button if it detects a possible delay, this will work as follows:
 When appointment is due to start in 1 hour or less, system will start checking provider location. System will send a reminder to the provider to head to customer if detects a possible delay according to google maps ETA, so if the route to the customer + a 15 min buffer is more than the time left for reaching the appointment, then the reminder will be sent. 

5. The provider goes to the location and taps Job Start
6. After job completion, the provider sets price (if not set already) and taps Job Done
7. Provider rates customer
8. Provider will see negative balance in account, provider will stop receiving jobs if balance reaches certain amount set by admin 
Provider will have a panel to check balance history 
Provider with will have a settle balance tab which shows his account number and payment options which are:
Pay in office, provider can see the location and open hours of office
Book agent to arrive and settle balance, provider chooses his location, then selects available time. An agent with account settling privilege shall arrive and collect cash, this is done by inputting provider account code.

# Company Web Panel

A company’s administrator can log in through web, and invite already registered service providers into the company by inputting their account code, this gives the company ability to
Live track employees
Have option to redirect jobs to company agent for confirming/cancelling
View employee task list
Company admin will have analysis tools such as view income of employees/how many actual hours worked, average rating per month, weekly income etc etc

providers in a company will have 2 balances, one personal and one company balance account, the company can see that there own balance and have ability to reset it. The personal balance account remains as is. All charges are on company balance while a provider is in a company


# Super Admin Web Panel Options
Services Control
Ability to control commission rates for each service
Adjust the match making formula for each service
Ability to show and hide available services in customer service list
Ability to set number of listed providers when customer searches for each service
Ability to set allowable working hours of services such as 7am to 10pm for plumber, 6 am to 11 pm for taxi
Ability to set weather service should be near a fixed location, provider current location, next appointment location
Fixed Location: the provider chooses preferred location, match making formula will assume this as his location at all times, all other functionalities will take his actual location example: customer will see actual distance away and not preferred location distance away
Actual Location: Match making formula will take the actual provider location at all times
Next Appointment Location: Match making formula will assume provider is at the next appointment location, if provider has 2 or more confirmed appointments, then the appointment with closer due time will be taken
Set time requirements of car wash service options, example: set SUV Normal car wash to 20 minutes, Sedan exterior car wash to 12 minutes, set buffer time for each request to 10 minutes. These timings will not be shown to customer, they will just be in backend and used in match making to select the provider who has enough time in his schedule


Service Monitoring
Ability to see provider’s availability chart with special filters such as
Filtered by service
Filtered by rating
Filtered by how many hours free
Example: a chart that shows how many grocery providers of above 4 rating, free for 2 hours, were available at 11 am today

Ability to see how many customer requests were there at time spans of (15 mins, 30 mins, 1 hour, 2 hours ,4 hours etc)
Ability to see all active provider’s locations on map (filtered per service) 



Service trend analysis
Ability to view charts of the following
Number of completed services 
Average rating of providers that completed jobs 
Average rating given by customers
Average actual work time/time range % for providers above 4 rating
Total amount paid
Number of disputes
Number of requests returning zero results
Number of provider cancellation before confirming
Number of provider cancellation after confirming 
Number of provider cancellation after start job
Number of customer cancellation before job confirm
Number of customer cancellation after job confirm
Ability to filter charts per service
Ability to select dates range of chart

Company/Providers/Customers viewing/balance settling
Have a provider list that shows ratings, income, number of jobs, detailed work history, call/cancellation ratio, ability to arrange the list according to any parameter such as income, number of jobs, number of cancellation, the list can be filtered by service type. 
Ability to settle balance and charge companies and providers
Ability to view providers and customer’s personal profiles and detailed work history
Ability to manually notifications to providers/customers

View/Dispute Reports and take action
View dispute reports and ability to ban customers and providers from all or certain services/ability to charge providers
Dispute reports shall include all possible details such as reason for dispute, history of disputes of customer and provider, locations of provide and customer, heading time, job start time, job finish time, any job edits, price set, who initiated the call, etc.
Ability to manually send messages and notifications to users
View provider’s registration applications
Ability to filter provider register applications by service, date, nationality of provider etc.
Ability to accept, reject, or achieve registration requests, and send notifications to applies.
Super Admin Employees  
Ability to create sub-admin accounts and give certain privileges for admin related issued such as managing disputes, settling balance, reviewing and accepting provider registration requests
Super admin will have a log of actions taken by sub-admins(super admin employees)

# License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
