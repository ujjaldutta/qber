<?php

return [
	'driver'      => env('FCM_PROTOCOL', 'http'),
	'log_enabled' => true,

	'http' => [
		'server_key'       => env('FCM_SERVER_KEY', 'AAAA75Vo3z8:APA91bE_ZIPcyWDglwNQqbtgqRZcy7vgzcoE39SFYtNCP8iugihKaAP5ZvkhnR0IM19AeWkDLlHV2Fz9UGGYCpZqSpnSoCxoTwsa-UFfRpLDiJBDL_lJykCqpBNkQwhVvBH3ASw6Ah_tD6OOnstMYvgCj8ZNXyXPgg'),
		'sender_id'        => env('FCM_SENDER_ID', '1029003861823'),
		'server_send_url'  => 'https://fcm.googleapis.com/fcm/send',
		'server_group_url' => 'https://android.googleapis.com/gcm/notification',
		'timeout'          => 30.0, // in second
	]
];
