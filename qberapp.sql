-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 21, 2017 at 02:01 PM
-- Server version: 5.7.8-rc
-- PHP Version: 5.6.22-1+donate.sury.org~trusty+1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qberapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `role_id`, `remember_token`, `reset_token`, `created_at`, `updated_at`) VALUES
(1, 'super@admin.com', '$2y$10$IG5Fgaz/1mCYkMh9icrt0.h8VuDZuet3eno6MBYmeJbjQ8OR9zzcS', 1, '8lQCwmUyMQcxOMfvBCre2BZh0qhyzxDfpvmcbYxzBRq3SiBoFqYzWclJYgN7', '921361', '2016-09-13 10:45:09', '2016-12-06 07:58:47'),
(5, 'admin@admin.com', '$2y$10$HVQ4ckFKLRuSDp8JN6Qap.g/2lwHx11yOb1KnmCOQ4bWNh5G.7OQm', 2, 'zgLRPuiKG9SntU7nnrji7yZ8cHgBDpPfC0X9weWQYRQ3PkpLlZWufK8GjA8f', '', '2016-09-13 11:52:32', '2016-12-05 03:53:52'),
(6, 'admi5511@admin.com', '$2y$10$IG5Fgaz/1mCYkMh9icrt0.h8VuDZuet3eno6MBYmeJbjQ8OR9zzcS', 2, '', '', '2016-09-13 11:53:19', '2016-09-14 09:06:41');

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_log`
--

INSERT INTO `admin_log` (`id`, `admin_id`, `controller`, `action`, `ip`, `date`) VALUES
(1, 5, 'customer', 'index', '127.0.0.1', '2016-12-02 06:51:05'),
(2, 5, 'customer', 'edit', '127.0.0.1', '2016-12-02 06:51:12'),
(3, 5, 'dashboard', 'index', '127.0.0.1', '2016-12-02 06:51:12'),
(4, 5, 'dashboard', 'index', '127.0.0.1', '2016-12-02 06:56:17'),
(5, 5, 'dashboard', 'index', '127.0.0.1', '2016-12-02 06:57:41'),
(6, 5, 'dashboard', 'index', '127.0.0.1', '2016-12-02 06:58:46'),
(7, 5, 'dashboard', 'index', '127.0.0.1', '2016-12-02 06:59:02'),
(8, 5, 'profile', 'edit', '127.0.0.1', '2016-12-02 06:59:04'),
(9, 5, 'profile', 'update', '127.0.0.1', '2016-12-02 06:59:17'),
(10, 5, 'profile', 'edit', '127.0.0.1', '2016-12-02 06:59:17'),
(11, 5, 'customer', 'index', '127.0.0.1', '2016-12-02 06:59:22'),
(12, 5, 'dashboard', 'index', '127.0.0.1', '2016-12-05 03:53:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`id`, `name`) VALUES
(1, 'Super Admin'),
(2, 'Sub Admin');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_access`
--

CREATE TABLE `admin_role_access` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `component_code` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_access`
--

INSERT INTO `admin_role_access` (`id`, `role_id`, `component_code`) VALUES
(1, 1, 'all'),
(6, 2, 'dashboardindex'),
(7, 2, 'customerindex'),
(8, 2, 'customersendMessage'),
(11, 2, 'profileedit'),
(12, 2, 'profileupdate');

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `service_start_time` datetime DEFAULT NULL,
  `service_end_time` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL COMMENT 'in minutes',
  `service_status_id` int(11) NOT NULL,
  `paid_amount` float DEFAULT NULL,
  `provider_amount` float DEFAULT NULL,
  `booking_slot_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `service_id`, `customer_id`, `provider_id`, `date`, `service_start_time`, `service_end_time`, `duration`, `service_status_id`, `paid_amount`, `provider_amount`, `booking_slot_id`) VALUES
(9, 1, 1, 1, '2017-02-03 10:00:00', '2017-02-03 10:00:00', '2017-02-03 11:00:00', NULL, 4, NULL, NULL, 19),
(12, 2, 1, 1, '2017-02-03 12:29:00', '2017-02-03 12:29:00', '2017-02-03 13:29:00', NULL, 4, NULL, NULL, 22),
(16, 3, 1, 1, '2017-02-03 13:39:00', '2017-02-03 13:39:00', '2017-02-03 14:39:00', NULL, 4, NULL, NULL, 27),
(17, 5, 1, 1, '2017-02-03 17:39:00', '2017-02-03 17:39:00', '2017-02-03 18:39:00', NULL, 4, NULL, NULL, 28),
(18, 6, 1, 1, '2017-02-03 20:00:00', '2017-02-03 20:00:00', '2017-02-03 21:00:00', NULL, 4, NULL, NULL, 29),
(22, 7, 1, 1, '2017-02-03 19:29:00', '2017-02-03 19:29:00', '2017-02-03 20:29:00', NULL, 4, NULL, NULL, 33),
(23, 8, 1, 1, '2017-02-06 10:00:00', '2017-02-06 10:00:00', '2017-02-06 11:00:00', NULL, 4, NULL, NULL, 35),
(24, 9, 1, 1, '2017-02-06 09:29:00', '2017-02-06 09:29:00', '2017-02-06 10:29:00', NULL, 4, NULL, NULL, 36),
(26, 14, 1, 1, '2017-02-06 16:30:00', '2017-02-06 16:30:00', '2017-02-06 17:23:00', NULL, 4, NULL, NULL, 38),
(27, 15, 1, 1, '2017-02-06 17:33:00', '2017-02-06 17:33:00', '2017-02-06 18:26:00', NULL, 4, NULL, NULL, 39),
(28, 16, 1, 1, '2017-02-08 08:30:00', '2017-02-08 08:30:00', '2017-02-08 09:23:00', NULL, 4, NULL, NULL, 40),
(29, 17, 1, 1, '2017-02-08 09:33:00', '2017-02-08 09:33:00', '2017-02-08 10:26:00', NULL, 4, NULL, NULL, 41),
(30, 18, 1, 1, '2017-02-08 15:00:00', '2017-02-08 15:00:00', '2017-02-08 15:53:00', NULL, 4, NULL, NULL, 42),
(33, 19, 1, 1, '2017-02-08 10:36:00', '2017-02-08 10:36:00', '2017-02-08 11:29:00', NULL, 4, NULL, NULL, 45),
(35, 21, 1, 1, '2017-02-10 06:29:00', '2017-02-10 06:29:00', '2017-02-10 07:22:00', NULL, 4, NULL, NULL, 47),
(36, 23, 1, 1, '2017-02-09 08:30:00', '2017-02-09 08:30:00', '2017-02-09 09:23:00', NULL, 4, NULL, NULL, 48),
(37, 24, 1, 1, '2017-02-09 09:52:00', '2017-02-09 09:52:00', '2017-02-09 10:45:00', NULL, 4, NULL, NULL, 49),
(39, 25, 1, 1, '2017-02-10 09:00:00', '2017-02-10 09:00:00', '2017-02-10 09:53:00', NULL, 4, NULL, NULL, 51),
(40, 26, 1, 1, '2017-02-10 10:22:00', '2017-02-10 10:22:00', '2017-02-10 10:52:00', NULL, 4, NULL, NULL, 52),
(42, 31, 1, 1, '2017-02-17 12:22:00', '2017-02-17 12:22:00', '2017-02-17 13:15:00', NULL, 4, NULL, NULL, 56),
(46, 35, 1, 1, '2017-02-17 21:00:00', '2017-02-17 21:00:00', '2017-02-17 21:53:00', NULL, 4, NULL, NULL, 60),
(47, 33, 1, 1, '2017-02-17 17:31:00', '2017-02-17 17:31:00', '2017-02-17 18:24:00', NULL, 4, NULL, NULL, 61),
(48, 34, 1, 1, '2017-02-17 18:44:00', '2017-02-17 18:44:00', '2017-02-17 19:37:00', NULL, 4, NULL, NULL, 62),
(56, 38, 1, 1, '2017-02-19 07:00:00', '2017-02-19 07:00:00', '2017-02-19 07:53:00', NULL, 4, NULL, NULL, 70),
(58, 40, 1, 1, '2017-02-19 09:58:00', '2017-02-19 09:58:00', '2017-02-19 10:51:00', NULL, 4, NULL, NULL, 72),
(59, 39, 1, 1, '2017-02-19 08:28:00', '2017-02-19 08:28:00', '2017-02-19 09:21:00', NULL, 4, NULL, NULL, 73),
(60, 41, 1, 1, '2017-02-21 13:00:00', '2017-02-21 13:00:00', '2017-02-21 13:53:00', NULL, 4, NULL, NULL, 74),
(63, 44, 1, 1, '2017-02-21 18:46:00', '2017-02-21 18:46:00', '2017-02-21 19:39:00', NULL, 4, NULL, NULL, 77),
(64, 45, 1, 1, '2017-02-21 20:30:00', '2017-02-21 20:30:00', '2017-02-21 21:23:00', NULL, 4, NULL, NULL, 78),
(65, 46, 1, 1, '2017-02-22 07:30:00', '2017-02-22 07:30:00', '2017-02-22 08:23:00', NULL, 4, NULL, NULL, 79),
(66, 48, 1, 1, '2017-02-22 06:30:00', '2017-02-22 06:30:00', '2017-02-22 07:23:00', NULL, 4, NULL, NULL, 80);

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reason`
--

CREATE TABLE `cancel_reason` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cancel_reason`
--

INSERT INTO `cancel_reason` (`id`, `title`, `is_active`) VALUES
(1, 'Dont want to mention reason', 1),
(2, 'I need better service and timing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reset_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `email`, `phone`, `name`, `address`, `password`, `is_active`, `remember_token`, `reset_token`, `created_at`, `updated_at`) VALUES
(1, 'company@gmail.com', '987897987155', 'testproject155', 'asdfsd fasdf', '$2y$10$NJSgBgj/gZkPLIC.CoDLieMguHCK67tok8y/V7BUMx8ND.MxXesxy', 1, 'LNVbxybzyOOyJQhJkHI8xgTEBLUeUAsZr5prnTbxhh6WdbpUpvIPZo1mKncJ', NULL, '2016-10-06 07:10:52', '2016-12-06 01:48:58'),
(2, 'ujjal.unified@gmail.com', '52112454545', 'test', 'test', '4324324', 1, '', '718904', '2016-12-05 11:14:38', '2016-12-06 07:29:11');

-- --------------------------------------------------------

--
-- Table structure for table `company_providers`
--

CREATE TABLE `company_providers` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `tied_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_providers`
--

INSERT INTO `company_providers` (`id`, `company_id`, `provider_id`, `tied_on`) VALUES
(1, 1, 1, '2016-10-06 00:00:00'),
(2, 1, 2, '2016-10-03 00:00:00'),
(3, 2, 6, '2016-12-05 11:23:28');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `countrycode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countryname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `countrycode`, `countryname`, `code`) VALUES
(1, 'AFG', 'Afghanistan', 'AF'),
(2, 'ALA', 'Åland', 'AX'),
(3, 'ALB', 'Albania', 'AL'),
(4, 'DZA', 'Algeria', 'DZ'),
(5, 'ASM', 'American Samoa', 'AS'),
(6, 'AND', 'Andorra', 'AD'),
(7, 'AGO', 'Angola', 'AO'),
(8, 'AIA', 'Anguilla', 'AI'),
(9, 'ATA', 'Antarctica', 'AQ'),
(10, 'ATG', 'Antigua and Barbuda', 'AG'),
(11, 'ARG', 'Argentina', 'AR'),
(12, 'ARM', 'Armenia', 'AM'),
(13, 'ABW', 'Aruba', 'AW'),
(14, 'AUS', 'Australia', 'AU'),
(15, 'AUT', 'Austria', 'AT'),
(16, 'AZE', 'Azerbaijan', 'AZ'),
(17, 'BHS', 'Bahamas', 'BS'),
(18, 'BHR', 'Bahrain', 'BH'),
(19, 'BGD', 'Bangladesh', 'BD'),
(20, 'BRB', 'Barbados', 'BB'),
(21, 'BLR', 'Belarus', 'BY'),
(22, 'BEL', 'Belgium', 'BE'),
(23, 'BLZ', 'Belize', 'BZ'),
(24, 'BEN', 'Benin', 'BJ'),
(25, 'BMU', 'Bermuda', 'BM'),
(26, 'BTN', 'Bhutan', 'BT'),
(27, 'BOL', 'Bolivia', 'BO'),
(28, 'BES', 'Bonaire', 'BQ'),
(29, 'BIH', 'Bosnia and Herzegovina', 'BA'),
(30, 'BWA', 'Botswana', 'BW'),
(31, 'BVT', 'Bouvet Island', 'BV'),
(32, 'BRA', 'Brazil', 'BR'),
(33, 'IOT', 'British Indian Ocean Territory', 'IO'),
(34, 'VGB', 'British Virgin Islands', 'VG'),
(35, 'BRN', 'Brunei', 'BN'),
(36, 'BGR', 'Bulgaria', 'BG'),
(37, 'BFA', 'Burkina Faso', 'BF'),
(38, 'BDI', 'Burundi', 'BI'),
(39, 'KHM', 'Cambodia', 'KH'),
(40, 'CMR', 'Cameroon', 'CM'),
(41, 'CAN', 'Canada', 'CA'),
(42, 'CPV', 'Cape Verde', 'CV'),
(43, 'CYM', 'Cayman Islands', 'KY'),
(44, 'CAF', 'Central African Republic', 'CF'),
(45, 'TCD', 'Chad', 'TD'),
(46, 'CHL', 'Chile', 'CL'),
(47, 'CHN', 'China', 'CN'),
(48, 'CXR', 'Christmas Island', 'CX'),
(49, 'CCK', 'Cocos [Keeling] Islands', 'CC'),
(50, 'COL', 'Colombia', 'CO'),
(51, 'COM', 'Comoros', 'KM'),
(52, 'COK', 'Cook Islands', 'CK'),
(53, 'CRI', 'Costa Rica', 'CR'),
(54, 'HRV', 'Croatia', 'HR'),
(55, 'CUB', 'Cuba', 'CU'),
(56, 'CUW', 'Curacao', 'CW'),
(57, 'CYP', 'Cyprus', 'CY'),
(58, 'CZE', 'Czech Republic', 'CZ'),
(59, 'COD', 'Democratic Republic of the Congo', 'CD'),
(60, 'DNK', 'Denmark', 'DK'),
(61, 'DJI', 'Djibouti', 'DJ'),
(62, 'DMA', 'Dominica', 'DM'),
(63, 'DOM', 'Dominican Republic', 'DO'),
(64, 'TLS', 'East Timor', 'TL'),
(65, 'ECU', 'Ecuador', 'EC'),
(66, 'EGY', 'Egypt', 'EG'),
(67, 'SLV', 'El Salvador', 'SV'),
(68, 'GNQ', 'Equatorial Guinea', 'GQ'),
(69, 'ERI', 'Eritrea', 'ER'),
(70, 'EST', 'Estonia', 'EE'),
(71, 'ETH', 'Ethiopia', 'ET'),
(72, 'FLK', 'Falkland Islands', 'FK'),
(73, 'FRO', 'Faroe Islands', 'FO'),
(74, 'FJI', 'Fiji', 'FJ'),
(75, 'FIN', 'Finland', 'FI'),
(76, 'FRA', 'France', 'FR'),
(77, 'GUF', 'French Guiana', 'GF'),
(78, 'PYF', 'French Polynesia', 'PF'),
(79, 'ATF', 'French Southern Territories', 'TF'),
(80, 'GAB', 'Gabon', 'GA'),
(81, 'GMB', 'Gambia', 'GM'),
(82, 'GEO', 'Georgia', 'GE'),
(83, 'DEU', 'Germany', 'DE'),
(84, 'GHA', 'Ghana', 'GH'),
(85, 'GIB', 'Gibraltar', 'GI'),
(86, 'GRC', 'Greece', 'GR'),
(87, 'GRL', 'Greenland', 'GL'),
(88, 'GRD', 'Grenada', 'GD'),
(89, 'GLP', 'Guadeloupe', 'GP'),
(90, 'GUM', 'Guam', 'GU'),
(91, 'GTM', 'Guatemala', 'GT'),
(92, 'GGY', 'Guernsey', 'GG'),
(93, 'GIN', 'Guinea', 'GN'),
(94, 'GNB', 'Guinea-Bissau', 'GW'),
(95, 'GUY', 'Guyana', 'GY'),
(96, 'HTI', 'Haiti', 'HT'),
(97, 'HMD', 'Heard Island and McDonald Islands', 'HM'),
(98, 'HND', 'Honduras', 'HN'),
(99, 'HKG', 'Hong Kong', 'HK'),
(100, 'HUN', 'Hungary', 'HU'),
(101, 'ISL', 'Iceland', 'IS'),
(102, 'IND', 'India', 'IN'),
(103, 'IDN', 'Indonesia', 'ID'),
(104, 'IRN', 'Iran', 'IR'),
(105, 'IRQ', 'Iraq', 'IQ'),
(106, 'IRL', 'Ireland', 'IE'),
(107, 'IMN', 'Isle of Man', 'IM'),
(108, 'ISR', 'Israel', 'IL'),
(109, 'ITA', 'Italy', 'IT'),
(110, 'CIV', 'Ivory Coast', 'CI'),
(111, 'JAM', 'Jamaica', 'JM'),
(112, 'JPN', 'Japan', 'JP'),
(113, 'JEY', 'Jersey', 'JE'),
(114, 'JOR', 'Jordan', 'JO'),
(115, 'KAZ', 'Kazakhstan', 'KZ'),
(116, 'KEN', 'Kenya', 'KE'),
(117, 'KIR', 'Kiribati', 'KI'),
(118, 'XKX', 'Kosovo', 'XK'),
(119, 'KWT', 'Kuwait', 'KW'),
(120, 'KGZ', 'Kyrgyzstan', 'KG'),
(121, 'LAO', 'Laos', 'LA'),
(122, 'LVA', 'Latvia', 'LV'),
(123, 'LBN', 'Lebanon', 'LB'),
(124, 'LSO', 'Lesotho', 'LS'),
(125, 'LBR', 'Liberia', 'LR'),
(126, 'LBY', 'Libya', 'LY'),
(127, 'LIE', 'Liechtenstein', 'LI'),
(128, 'LTU', 'Lithuania', 'LT'),
(129, 'LUX', 'Luxembourg', 'LU'),
(130, 'MAC', 'Macao', 'MO'),
(131, 'MKD', 'Macedonia', 'MK'),
(132, 'MDG', 'Madagascar', 'MG'),
(133, 'MWI', 'Malawi', 'MW'),
(134, 'MYS', 'Malaysia', 'MY'),
(135, 'MDV', 'Maldives', 'MV'),
(136, 'MLI', 'Mali', 'ML'),
(137, 'MLT', 'Malta', 'MT'),
(138, 'MHL', 'Marshall Islands', 'MH'),
(139, 'MTQ', 'Martinique', 'MQ'),
(140, 'MRT', 'Mauritania', 'MR'),
(141, 'MUS', 'Mauritius', 'MU'),
(142, 'MYT', 'Mayotte', 'YT'),
(143, 'MEX', 'Mexico', 'MX'),
(144, 'FSM', 'Micronesia', 'FM'),
(145, 'MDA', 'Moldova', 'MD'),
(146, 'MCO', 'Monaco', 'MC'),
(147, 'MNG', 'Mongolia', 'MN'),
(148, 'MNE', 'Montenegro', 'ME'),
(149, 'MSR', 'Montserrat', 'MS'),
(150, 'MAR', 'Morocco', 'MA'),
(151, 'MOZ', 'Mozambique', 'MZ'),
(152, 'MMR', 'Myanmar [Burma]', 'MM'),
(153, 'NAM', 'Namibia', 'NA'),
(154, 'NRU', 'Nauru', 'NR'),
(155, 'NPL', 'Nepal', 'NP'),
(156, 'NLD', 'Netherlands', 'NL'),
(157, 'NCL', 'New Caledonia', 'NC'),
(158, 'NZL', 'New Zealand', 'NZ'),
(159, 'NIC', 'Nicaragua', 'NI'),
(160, 'NER', 'Niger', 'NE'),
(161, 'NGA', 'Nigeria', 'NG'),
(162, 'NIU', 'Niue', 'NU'),
(163, 'NFK', 'Norfolk Island', 'NF'),
(164, 'PRK', 'North Korea', 'KP'),
(165, 'MNP', 'Northern Mariana Islands', 'MP'),
(166, 'NOR', 'Norway', 'NO'),
(167, 'OMN', 'Oman', 'OM'),
(168, 'PAK', 'Pakistan', 'PK'),
(169, 'PLW', 'Palau', 'PW'),
(170, 'PSE', 'Palestine', 'PS'),
(171, 'PAN', 'Panama', 'PA'),
(172, 'PNG', 'Papua New Guinea', 'PG'),
(173, 'PRY', 'Paraguay', 'PY'),
(174, 'PER', 'Peru', 'PE'),
(175, 'PHL', 'Philippines', 'PH'),
(176, 'PCN', 'Pitcairn Islands', 'PN'),
(177, 'POL', 'Poland', 'PL'),
(178, 'PRT', 'Portugal', 'PT'),
(179, 'PRI', 'Puerto Rico', 'PR'),
(180, 'QAT', 'Qatari', 'QA'),
(181, 'COG', 'Republic of the Congo', 'CG'),
(182, 'REU', 'Réunion', 'RE'),
(183, 'ROU', 'Romania', 'RO'),
(184, 'RUS', 'Russia', 'RU'),
(185, 'RWA', 'Rwanda', 'RW'),
(186, 'BLM', 'Saint Barthélemy', 'BL'),
(187, 'SHN', 'Saint Helena', 'SH'),
(188, 'KNA', 'Saint Kitts and Nevis', 'KN'),
(189, 'LCA', 'Saint Lucia', 'LC'),
(190, 'MAF', 'Saint Martin', 'MF'),
(191, 'SPM', 'Saint Pierre and Miquelon', 'PM'),
(192, 'VCT', 'Saint Vincent and the Grenadines', 'VC'),
(193, 'WSM', 'Samoa', 'WS'),
(194, 'SMR', 'San Marino', 'SM'),
(195, 'STP', 'São Tomé and Príncipe', 'ST'),
(196, 'SAU', 'Saudi Arabia', 'SA'),
(197, 'SEN', 'Senegal', 'SN'),
(198, 'SRB', 'Serbia', 'RS'),
(199, 'SYC', 'Seychelles', 'SC'),
(200, 'SLE', 'Sierra Leone', 'SL'),
(201, 'SGP', 'Singapore', 'SG'),
(202, 'SXM', 'Sint Maarten', 'SX'),
(203, 'SVK', 'Slovakia', 'SK'),
(204, 'SVN', 'Slovenia', 'SI'),
(205, 'SLB', 'Solomon Islands', 'SB'),
(206, 'SOM', 'Somalia', 'SO'),
(207, 'ZAF', 'South Africa', 'ZA'),
(208, 'SGS', 'South Georgia and the South Sandwich Islands', 'GS'),
(209, 'KOR', 'South Korea', 'KR'),
(210, 'SSD', 'South Sudan', 'SS'),
(211, 'ESP', 'Spain', 'ES'),
(212, 'LKA', 'Sri Lanka', 'LK'),
(213, 'SDN', 'Sudan', 'SD'),
(214, 'SUR', 'Suriname', 'SR'),
(215, 'SJM', 'Svalbard and Jan Mayen', 'SJ'),
(216, 'SWZ', 'Swaziland', 'SZ'),
(217, 'SWE', 'Sweden', 'SE'),
(218, 'CHE', 'Switzerland', 'CH'),
(219, 'SYR', 'Syria', 'SY'),
(220, 'TWN', 'Taiwan', 'TW'),
(221, 'TJK', 'Tajikistan', 'TJ'),
(222, 'TZA', 'Tanzania', 'TZ'),
(223, 'THA', 'Thailand', 'TH'),
(224, 'TGO', 'Togo', 'TG'),
(225, 'TKL', 'Tokelau', 'TK'),
(226, 'TON', 'Tonga', 'TO'),
(227, 'TTO', 'Trinidad and Tobago', 'TT'),
(228, 'TUN', 'Tunisia', 'TN'),
(229, 'TUR', 'Turkey', 'TR'),
(230, 'TKM', 'Turkmenistan', 'TM'),
(231, 'TCA', 'Turks and Caicos Islands', 'TC'),
(232, 'TUV', 'Tuvalu', 'TV'),
(233, 'UMI', 'U.S. Minor Outlying Islands', 'UM'),
(234, 'VIR', 'U.S. Virgin Islands', 'VI'),
(235, 'UGA', 'Uganda', 'UG'),
(236, 'UKR', 'Ukraine', 'UA'),
(237, 'ARE', 'United Arab Emirates', 'AE'),
(238, 'GBR', 'United Kingdom', 'GB'),
(239, 'USA', 'United States', 'US'),
(240, 'URY', 'Uruguay', 'UY'),
(241, 'UZB', 'Uzbekistan', 'UZ'),
(242, 'VUT', 'Vanuatu', 'VU'),
(243, 'VAT', 'Vatican City', 'VA'),
(244, 'VEN', 'Venezuela', 'VE'),
(245, 'VNM', 'Vietnam', 'VN'),
(246, 'WLF', 'Wallis and Futuna', 'WF'),
(247, 'ESH', 'Western Sahara', 'EH'),
(248, 'YEM', 'Yemen', 'YE'),
(249, 'ZMB', 'Zambia', 'ZM'),
(250, 'ZWE', 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `term_accepted` tinyint(1) DEFAULT NULL,
  `sms_verified` tinyint(1) DEFAULT '0',
  `sms_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_latitude` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `current_longitude` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `banned_duration` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `api_token` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `email`, `phone`, `password`, `term_accepted`, `sms_verified`, `sms_code`, `current_latitude`, `current_longitude`, `banned_duration`, `is_active`, `remember_token`, `api_token`, `device_id`, `device_type`, `is_online`, `created_at`, `updated_at`) VALUES
(1, 'ukdds@gmail.com', '919163063153', '$2y$10$nFwBqqbUWb2p7nza5AnDk.0nIfeuxOjsmGHMTt0crUTi/.vKLBHQq', 1, 1, '', NULL, NULL, 0, 1, '42y5CiDEdKAkzWKZsArizn4aE0MkWN0r1gmzFSBX', '1487657573', 'dnhbpZTHbo4:APA91bH2bf2SFyPotO6jxX7gHns-YXtY-DqCmEPA9X3HQlkskiAR0fDHTRSAQ_BWtZz3r1ejHIFgRVUEGWnd43Cq91S-5-LolB6CT7WhYF21qFmxdMx-DwsmBM9P0Ten6LI9OzTWynQp', NULL, 1, '2016-10-07 09:35:41', '2017-02-21 06:12:53'),
(2, 'ujjal@gmail.com', '919163063154', '$2y$10$nFwBqqbUWb2p7nza5AnDk.0nIfeuxOjsmGHMTt0crUTi/.vKLBHQq', 1, 1, '3476', '25.32075293256326', '51.47715563774109', 0, 1, 'Xq7Ia6SVDbuzlrh5O9U774R6PMy0gqd2U8huGTjb', '1481792695', 'dnhbpZTHbo4:APA91bH2bf2SFyPotO6jxX7gHns-YXtY-DqCmEPA9X3HQlkskiAR0fDHTRSAQ_BWtZz3r1ejHIFgRVUEGWnd43Cq91S-5-LolB6CT7WhYF21qFmxdMx-DwsmBM9P0Ten6LI9OzTWynQp', NULL, 0, '2016-10-07 09:35:19', '2016-12-15 03:50:03'),
(5, 'test2@gmail.com', '919051333698', '$2y$10$l8Wn/BRtnZ9cgl7YJC7LyeS75ayCG5XOSv8ZsvNTNxzlCep.28.sm', 1, 1, '', NULL, NULL, NULL, 1, NULL, NULL, '', NULL, 0, '2016-11-03 14:29:09', '2016-11-03 14:29:09'),
(6, 'test4@gmail.com', '919163063152', '$2y$10$n8gXjPk9k.lo5ZuMmwbU9u1zby0G.xTQKH/4qCtspD8pnHqU0rFEe', 1, 1, '', NULL, NULL, NULL, 1, NULL, '1476858689', '', NULL, 1, '2016-11-03 14:29:09', '2017-02-10 11:29:48'),
(7, 'test33@gmail.com', '919163063165', '$2y$10$SC6oK.pwnlPmEUCQeUfSguLyr9KVBthbiYnjpwqNZuRAaI/XWMw2G', 1, 1, '7952', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '2016-11-03 14:29:09', '2016-11-03 14:29:09'),
(12, 'ujj4al2@gmail.com', '5464564654645', '$2y$10$UQ7lvLHesv2KALEyrwwOZ.bNhj8c0i37I/W1pkYn8L3HNpOHzQVl6', 0, 0, '', NULL, NULL, 0, 0, NULL, '1476709966', NULL, NULL, 1, '2016-10-14 04:14:04', '2016-10-17 07:42:46'),
(13, 'sdafsdj@min.com', '88565644444', '$2y$10$184gvyiLlkGbmqzi6JD51OfrCwRZnGwcihoiT0T1G5RZLomZ3M4sy', 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, '2016-10-14 04:14:32', '2016-10-14 04:14:32'),
(14, 'test99@gmail.com', '8565563131321', '$2y$10$aLmSt7bfNF1NZqXLdFn0bu4pK4qio7YWwYtBzmFQeDg5gcIVD2zmW', 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, '2016-11-03 14:29:09', '2016-11-03 14:29:09'),
(15, 'sdfsdfs2@gmail.com', '915465675678567', '$2y$10$WnKqH6p3M.B0Aev/yL5DPOTXPk.jeBKYuL6A67mUAbvmh0j17Dz2S', 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, '2016-10-14 04:17:16', '2016-10-14 09:00:42'),
(16, 'ujjal2@gmail.com', '918902371657', '$2y$10$wYoy2ZU.tHuTcd3MlqQ/8.fhNekBNLPrSBIGXc5CpsWiAnFztGgx6', 0, 0, '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, '2016-10-14 04:17:32', '2016-10-14 06:06:44');

-- --------------------------------------------------------

--
-- Table structure for table `customer_cancel_history`
--

CREATE TABLE `customer_cancel_history` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `date` timestamp NOT NULL,
  `comment` text CHARACTER SET latin1 NOT NULL,
  `penalty_amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_cancel_history`
--

INSERT INTO `customer_cancel_history` (`id`, `customer_id`, `service_id`, `reason_id`, `date`, `comment`, `penalty_amount`) VALUES
(2, 1, 29, 2, '2017-02-10 08:50:47', 'test', 20),
(3, 1, 38, 2, '2017-02-17 12:02:28', 'test', 0),
(4, 1, 38, 2, '2017-02-17 14:26:06', 'test', 20),
(5, 1, 42, 2, '2017-02-20 07:53:23', 'test', 20),
(6, 1, 42, 2, '2017-02-20 08:08:51', 'test', 20),
(7, 1, 42, 2, '2017-02-20 08:10:43', 'test', 20),
(8, 1, 42, 2, '2017-02-20 08:12:06', 'test', 20),
(9, 1, 42, 2, '2017-02-20 09:42:11', 'test', 20);

-- --------------------------------------------------------

--
-- Table structure for table `customer_location_history`
--

CREATE TABLE `customer_location_history` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `longitude` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='PreSave location for customer';

-- --------------------------------------------------------

--
-- Table structure for table `customer_message`
--

CREATE TABLE `customer_message` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('inbox','draft','trash','contact') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inbox',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_message`
--

INSERT INTO `customer_message` (`id`, `customer_id`, `subject`, `message`, `type`, `email`, `message_type`, `created_at`, `updated_at`) VALUES
(5, 1, 'test', 'test msg', 'inbox', 'kljsdkf@lkjdslkf.com', 'test', '2017-01-04 07:47:12', '2017-01-04 07:47:12'),
(6, 1, '', 'fsdfdsafsdf', 'inbox', NULL, NULL, '2017-01-11 06:34:53', '2017-01-11 06:34:53'),
(7, 1, '', 'asdfs dfdsaf ', 'inbox', NULL, NULL, '2017-01-11 06:35:57', '2017-01-11 06:35:57'),
(8, 1, 'test', 'test r1111 ', 'inbox', NULL, NULL, '2017-01-11 06:36:43', '2017-01-11 06:36:43'),
(9, 1, 'inbox', 'sdfds', 'inbox', 'sdfds@kldjfl.com', 'inbox', '2017-01-23 04:20:58', '2017-01-23 04:20:58'),
(10, 1, 'inbox', 'sdfds', 'inbox', 'sdfds@kldjfl.com', 'inbox', '2017-01-23 04:21:59', '2017-01-23 04:21:59');

-- --------------------------------------------------------

--
-- Table structure for table `customer_notification`
--

CREATE TABLE `customer_notification` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `date` timestamp NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_latitude` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_longitude` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance_from_provider` int(5) NOT NULL,
  `duration_from_provider` int(5) NOT NULL,
  `service_notice_status` enum('heading','confirmed','cancelled','jobstarted','jobcompleted') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_notification`
--

INSERT INTO `customer_notification` (`id`, `customer_id`, `service_id`, `date`, `details`, `notification_latitude`, `notification_longitude`, `distance_from_provider`, `duration_from_provider`, `service_notice_status`, `price`) VALUES
(1, 1, 1, '2017-02-03 05:35:45', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(2, 1, 1, '2017-02-03 05:50:13', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(3, 1, 1, '2017-02-03 06:07:54', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(4, 1, 1, '2017-02-03 06:13:53', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(5, 1, 1, '2017-02-03 06:21:14', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(6, 1, 1, '2017-02-03 06:24:10', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(7, 1, 1, '2017-02-03 06:37:11', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(8, 1, 1, '2017-02-03 06:41:21', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(9, 1, 1, '2017-02-03 08:16:01', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(10, 1, 2, '2017-02-03 08:13:01', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(11, 1, 2, '2017-02-03 08:52:10', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(12, 1, 2, '2017-02-03 09:04:24', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(13, 1, 3, '2017-02-03 09:17:02', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(14, 1, 3, '2017-02-03 09:28:39', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(15, 1, 3, '2017-02-03 09:31:01', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(16, 1, 3, '2017-02-03 10:04:47', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(17, 1, 5, '2017-02-03 10:36:10', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(18, 1, 6, '2017-02-03 10:38:33', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(19, 1, 7, '2017-02-03 11:31:24', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(20, 1, 7, '2017-02-03 11:35:23', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(21, 1, 7, '2017-02-03 11:38:25', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(22, 1, 7, '2017-02-03 11:40:30', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(23, 1, 8, '2017-02-03 13:19:51', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(24, 1, 9, '2017-02-03 13:21:17', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(26, 1, 14, '2017-02-06 13:09:44', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(27, 1, 15, '2017-02-06 13:11:54', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(28, 1, 16, '2017-02-07 08:23:31', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(29, 1, 17, '2017-02-07 08:23:53', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(30, 1, 18, '2017-02-07 08:24:35', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(31, 1, 19, '2017-02-07 08:27:03', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(32, 1, 19, '2017-02-07 09:53:05', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(33, 1, 19, '2017-02-07 11:54:17', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(35, 1, 21, '2017-02-07 13:43:27', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(36, 1, 23, '2017-02-08 05:50:43', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(37, 1, 24, '2017-02-08 06:13:43', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(38, 1, 25, '2017-02-08 10:21:02', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(39, 1, 25, '2017-02-08 10:35:16', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(40, 1, 26, '2017-02-08 10:48:45', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(41, 1, 29, '2017-02-09 13:01:06', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(42, 1, 30, '2017-02-09 13:01:38', 'Job Accepted', '25.281039763035846', '51.51070688247681', 0, 0, 'confirmed', 100),
(43, 1, 31, '2017-02-17 05:42:48', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(44, 1, 31, '2017-02-17 05:58:50', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(45, 1, 33, '2017-02-17 06:15:50', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(46, 1, 34, '2017-02-17 06:31:55', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(47, 1, 35, '2017-02-17 06:32:15', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(48, 1, 35, '2017-02-17 06:47:15', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(49, 1, 33, '2017-02-17 07:25:14', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(50, 1, 34, '2017-02-17 08:28:57', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(51, 1, 37, '2017-02-17 10:19:30', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(52, 1, 38, '2017-02-17 11:52:58', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(53, 1, 39, '2017-02-17 11:54:31', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(54, 1, 38, '2017-02-17 12:06:51', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(55, 1, 39, '2017-02-17 12:07:07', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(56, 1, 38, '2017-02-17 14:14:27', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(57, 1, 39, '2017-02-17 14:14:36', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(58, 1, 38, '2017-02-17 14:27:26', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(59, 1, 39, '2017-02-17 14:27:34', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(60, 1, 40, '2017-02-17 14:29:44', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(61, 1, 39, '2017-02-17 14:50:46', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(62, 1, 41, '2017-02-20 07:20:48', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(63, 1, 42, '2017-02-20 07:21:02', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(64, 1, 43, '2017-02-20 07:22:04', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(65, 1, 44, '2017-02-20 07:22:45', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(66, 1, 45, '2017-02-20 07:23:54', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(67, 1, 43, '2017-02-20 09:49:25', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(68, 1, 46, '2017-02-21 05:51:47', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100),
(69, 1, 48, '2017-02-21 06:13:16', 'Job Accepted', '25.240482', '51.456281', 0, 0, 'confirmed', 100);

-- --------------------------------------------------------

--
-- Table structure for table `customer_profile`
--

CREATE TABLE `customer_profile` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `age` int(4) DEFAULT NULL,
  `gender` enum('male','female') CHARACTER SET latin1 DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `highest_education` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `annual_income` float DEFAULT NULL,
  `total_search_submit` int(11) DEFAULT NULL,
  `last_search` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_profile`
--

INSERT INTO `customer_profile` (`id`, `customer_id`, `name`, `age`, `gender`, `image`, `highest_education`, `annual_income`, `total_search_submit`, `last_search`) VALUES
(1, 1, 'ud1', 33, 'male', 'media/customer/sachin.jpeg', 'dfsg', 4444, 142, '2017-02-21 09:27:50'),
(2, 2, 'ujjal dutta', 1977, 'male', 'media/customer/205bb1e8eff522e9a8924dfa119c9ca55a449cef.jpg', 'BE', 500000, NULL, NULL),
(5, 5, 'abc', 44, 'male', NULL, NULL, NULL, NULL, NULL),
(6, 6, 'ujjal2', 66, 'male', NULL, NULL, NULL, NULL, NULL),
(11, 12, 'test', 65, 'female', NULL, NULL, NULL, NULL, NULL),
(12, 13, 'test3', 55, 'female', NULL, NULL, NULL, NULL, NULL),
(13, 14, 'test6', 57, 'male', NULL, NULL, NULL, NULL, NULL),
(14, 15, 'test7', 68, 'male', NULL, NULL, NULL, NULL, NULL),
(15, 16, 'test99', 74, 'male', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_rating`
--

CREATE TABLE `customer_rating` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `rate_value` float NOT NULL,
  `professional` float DEFAULT NULL,
  `friendly` float DEFAULT NULL,
  `communication` float DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_rating`
--

INSERT INTO `customer_rating` (`id`, `customer_id`, `provider_id`, `service_id`, `rate_value`, `professional`, `friendly`, `communication`, `date`, `comment`) VALUES
(1, 1, 1, 1, 5, 4, 3, 4, '2017-02-10 00:00:00', 'fdsfds');

-- --------------------------------------------------------

--
-- Table structure for table `dispute`
--

CREATE TABLE `dispute` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dispute_to` enum('customer','provider') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `type` enum('customer','provider') COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `type`, `question`, `answer`, `link`, `is_active`) VALUES
(5, 'customer', 'How can I register under company as a provider?', 'You need to get invite from company', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` int(11) NOT NULL,
  `type_id` int(4) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `current_latitude` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `current_longitude` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `base_latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_location` int(11) DEFAULT NULL,
  `location_preference` enum('fixed','actual') CHARACTER SET latin1 DEFAULT 'actual',
  `provider_start_daytime` time DEFAULT NULL COMMENT 'based on this time freeslot will be generated',
  `provider_end_daytime` time DEFAULT NULL COMMENT 'based on this time freeslot will be generated',
  `sms_verified` tinyint(1) NOT NULL DEFAULT '0',
  `email_verified` int(11) NOT NULL,
  `sms_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approve_status` tinyint(1) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `ready_to_serve` tinyint(1) DEFAULT NULL,
  `location_factor` float DEFAULT NULL,
  `moneyexpvalue` float DEFAULT NULL,
  `priority` float DEFAULT NULL,
  `work_time` int(11) DEFAULT NULL,
  `is_online` tinyint(1) DEFAULT '0',
  `banned_duration` int(11) DEFAULT NULL,
  `account_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_status` enum('free','break','unavailable','must head now','heading','working') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `type_id`, `email`, `phone`, `name`, `password`, `current_latitude`, `current_longitude`, `base_latitude`, `base_longitude`, `base_address`, `preferred_location`, `location_preference`, `provider_start_daytime`, `provider_end_daytime`, `sms_verified`, `email_verified`, `sms_code`, `approve_status`, `remember_token`, `api_token`, `device_id`, `device_type`, `is_active`, `ready_to_serve`, `location_factor`, `moneyexpvalue`, `priority`, `work_time`, `is_online`, `banned_duration`, `account_no`, `provider_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'a@gmail.com', '9163063153', 'ujjal', '$2y$10$Ey9DhJcS7jNPcp.nZ/lZr.pWBsFnI1Ctpbi8K4uEldsYDFXPwzYhW', '25.240482', '51.456281', '25.261039763035846', '51.52070688247681', NULL, 1, 'actual', NULL, NULL, 1, 0, '', 1, 'p9tB6evzuI4p235t94pyQ1h94RWNSSiT781UIlPJ', '1487657593', '', NULL, 1, 1, 0, 0, 0, NULL, 0, NULL, '8429100', 'unavailable', '2016-09-22 10:00:32', '2017-02-21 08:16:40'),
(2, 1, 'b@gmail.com', '9163063152', 'ujjal b', '$2y$10$U0plJxx6RSBXFdaQWpnl5uB78Nb/VpjJITkc3EYW5A0gySSleZpkK', '25.281039763035846', '51.53070688247681', '25.281039763035846', '51.53070688247681', NULL, 2, 'actual', NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, 0.184264, 3960.79, 729.832, NULL, 1, NULL, '8425109', 'free', '2016-09-22 10:00:32', '2017-02-03 02:16:16'),
(3, 1, 'c@gmail.com', '9163063154', 'provider c', '$2y$10$BlgWf8O345Sa1LCKOSDCn.Vv3f.ZmwcHiZ6CAqzNHM/rcSQ8Z6gL6', '25.29956518722446', '51.51066541671753', '25.29956518722446', '51.51066541671753', NULL, 1, NULL, NULL, NULL, 1, 0, '', 1, 'ozkEff5lcNaf0YSBGLWc6TNdO1j0aUCLWtQmyriQ', '1481780791', 'dnhbpZTHbo4:APA91bH2bf2SFyPotO6jxX7gHns-YXtY-DqCmEPA9X3HQlkskiAR0fDHTRSAQ_BWtZz3r1ejHIFgRVUEGWnd43Cq91S-5-LolB6CT7WhYF21qFmxdMx-DwsmBM9P0Ten6LI9OzTWynQp', NULL, 1, 1, 0.117492, 0, 0, NULL, 0, NULL, '', 'free', '2016-09-28 09:55:44', '2017-02-09 13:02:43'),
(4, 1, 'd@gmail.com', '457454545', 'provider d', '$2y$10$JNAWz3CTTw7joe1Kp15kkuyMdZ781Ms6mvC.uugS5ZrEuTrFrFXDW', '25.274017133947062', '51.44631385803223', '25.274017133947062', '51.44631385803223', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', '1478763940', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', 'free', '2016-09-20 06:47:47', '2017-02-09 13:02:43'),
(5, 1, 'e@gmail.com', '9112345678', 'provider e', '$2y$10$KlLZQIDFvKhys2kOEwMd/uejEO45Aq2KD.bvaiAwh62LKayU5oq16', '25.32771458266232', '51.49334907531738', '25.32771458266232', '51.49334907531738', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, 'c-sAA9tfLIY:APA91bEeCItjMR1x8moD5jv9Vj7ryf2tWLVZqkaDIaqZZwHP9AUSUS8GUukGXdU4chNdoIQnLyEuSKCKwRTyEMMD_bOudl8gMjOPao4J2_XtSJC93t9nQ0smmo8j90BhtrYwOJk9WNqb', NULL, 1, 1, 0, 0, 0, NULL, 1, NULL, '', 'free', '2016-12-26 11:15:34', '2016-12-26 06:28:02'),
(6, 1, 'f@gmail.com', '7744444333', 'provider f', '$2y$10$4bfjDr4.f6vx0VNVWhst4.8jmiycl1O8q5GX2UChxLyg5Zy0.6W92', '25.33075293256326', '51.48015563774109', '25.33075293256326', '51.48015563774109', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '0jwTjbLQhDZjiaL5FB4bjaytzFMsj7Q5fA6ZLOcg', '1480936872', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', 'free', '2016-09-20 06:48:17', '2017-02-09 13:02:43'),
(7, 1, 'g@gmail.com', '5956324333', 'provider g', '$2y$10$YYHCIVMX.1bTlrUYPuhKduK53Dk7ASSdl9FosK18V36qssmH5Uk2K', '25.32975293256326', '51.47015563774109', '25.32975293256326', '51.47015563774109', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 1, NULL, '', 'free', '2016-09-20 06:48:32', '2016-09-20 06:48:32'),
(8, 1, 'h@gmail.com', '5956324333', 'provider h', '$2y$10$VMaA2Ck3/sHHA9yx0fE29eflau9lLCYsRgtF9q.2Nw69Kz/DqU4f2', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 1, NULL, '', 'free', '2016-09-20 06:48:42', '2016-09-20 06:48:42'),
(9, 1, 'j@gmail.com', '5956324333', 'provider j', '$2y$10$blP2az27tnyVOyStvNEL6O7L4Kuf7dfIpQ1D9ekF6DOeIZkbUYgxi', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-20 06:48:51', '2016-09-20 06:48:51'),
(10, 1, 'k@gmail.com', '5956324331', 'provider k', '$2y$10$zhDGz1vr7q3h/CtOaj5DCOtVkq1zZ/9SDptpFaGTPTPJBOLDEOcLC', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 1, '3146', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-11-24 07:36:49', '2016-11-24 09:27:32'),
(11, 1, 'm@gmail.com', '4578899955', 'provider m', '$2y$10$2VgSut3E9x//3bk7zTyhguUkkaqUVNG6eTwzM1wQtsqORIboeXaUi', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-20 06:49:13', '2016-09-20 06:49:13'),
(12, 1, 'n@gmail.com', '9999999999', 'provider n', '$2y$10$rkU1QXKaa7jy9deYdG6gSOifdvWywFzRaM2qoI8rUeSQ/4nff7fqe', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-20 06:49:22', '2016-09-20 06:49:22'),
(13, 1, 'p@gmail.com', '4545666666', 'provider p', '$2y$10$F7MjS7sdpCd0NpdLPl4SO.NupNKUKqVe8PQQ8GcLieoRxyPi8soGO', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-20 06:49:44', '2016-09-20 06:49:44'),
(14, 2, NULL, '999999999', 'abc', '$2y$10$odlGygM2oXSMDIG3haS4peQ/sOzcypcMP7MI7FUjAjXK1LVmNL502', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 1, '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-29 00:47:11', '2016-09-29 00:47:11'),
(15, 2, NULL, '0123456789', 'dummy name', '$2y$10$h9gLcO5nmWQSCzZte0Du5OxLTqPexR/Bv3RFhQey4/ujH.elhpeSu', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 0, '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-29 00:47:53', '2016-09-29 00:47:53'),
(16, 2, NULL, '99999991', 'abc', '$2y$10$Cj.qTuj8ROOhylqxhknTfOpvat/AIdfPUUtn0yc.41e6KDcqP6HgO', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 0, '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-29 00:51:43', '2016-09-29 00:51:43'),
(17, 2, NULL, '123456789', 'dummy name', '$2y$10$blQtTp/MRAaRXi4y16a47OxwQGm3YH4o5rfQuOTsqvA9qgLmgavya', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, 0, '', 0, '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-09-29 00:57:57', '2017-02-10 11:21:41'),
(18, 1, 'absc@gmail.com', '9163063155', 'abcd', '$2y$10$.RrvZCc6yixuLrlqkPropeTmIdRtN7JT/uQJh6bQdUWDwVhCMwsUK', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 0, 0, '2285', 0, '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, '2016-11-07 06:19:56', '2017-02-15 09:50:48'),
(19, 1, 'udutta@gmail.com', '9163063159', 'udutta', '$2y$10$F4fdHaYlpifsxWOGwjOaJut0t7g.vyHkbp309OpDJhBdp4XkPF63u', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 0, 0, '4799', 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '8425100', NULL, '2016-11-28 02:08:45', '2016-11-28 02:08:45'),
(20, 1, 'udutta2@gmail.com', '9163063165', 'udutta', '$2y$10$iutCE.PSwRAPxQn3ZPD.buj/oQCAcK/jDo2DF210U3md5lUkwS2XC', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 0, 0, '6502', 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '8729125', NULL, '2016-11-28 02:09:37', '2016-11-28 02:09:37'),
(21, 1, 'ljdsfkl@lkjdlkjf.com', '9163263595', 'test', '$2y$10$lPozGwk5lybyzbWdmyJnX.X/jv8WSXqMaUfEeXoAp5TCz1eWrjh7K', '43534534', '3454745', '43534534', '3454745', NULL, NULL, 'actual', NULL, NULL, 0, 0, '3638', 0, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 0, NULL, '7008761', NULL, '2017-02-03 07:14:45', '2017-02-15 09:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `provider_balance_history`
--

CREATE TABLE `provider_balance_history` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `receive_amount` float NOT NULL,
  `commission_amount` float NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provider_booked_timeslot`
--

CREATE TABLE `provider_booked_timeslot` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `book_date` datetime NOT NULL,
  `booked_customer_id` int(11) NOT NULL,
  `from_time` timestamp NULL DEFAULT NULL,
  `to_time` timestamp NULL DEFAULT NULL,
  `total_duration` int(11) NOT NULL COMMENT 'in minutes',
  `distance_duration` int(11) DEFAULT NULL COMMENT 'in minutes',
  `sort_order` int(11) NOT NULL DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_booked_timeslot`
--

INSERT INTO `provider_booked_timeslot` (`id`, `provider_id`, `book_date`, `booked_customer_id`, `from_time`, `to_time`, `total_duration`, `distance_duration`, `sort_order`) VALUES
(19, 1, '2017-02-03 10:00:00', 1, '2017-02-03 07:00:00', '2017-02-03 08:00:00', 60, 0, 1),
(22, 1, '2017-02-03 12:29:00', 1, '2017-02-03 09:29:00', '2017-02-03 10:29:00', 60, 0, 1),
(27, 1, '2017-02-03 13:39:00', 1, '2017-02-03 10:39:00', '2017-02-03 11:39:00', 60, 0, 1),
(28, 1, '2017-02-03 17:39:00', 1, '2017-02-03 14:39:00', '2017-02-03 15:39:00', 60, 0, 1),
(29, 1, '2017-02-03 20:00:00', 1, '2017-02-03 17:00:00', '2017-02-03 18:00:00', 60, 0, 2),
(33, 1, '2017-02-03 19:29:00', 1, '2017-02-03 16:29:00', '2017-02-03 17:29:00', 60, 0, 1),
(35, 1, '2017-02-06 10:00:00', 1, '2017-02-06 07:00:00', '2017-02-06 08:00:00', 60, 0, 2),
(36, 1, '2017-02-06 09:29:00', 1, '2017-02-06 06:29:00', '2017-02-06 07:29:00', 60, 0, 1),
(38, 1, '2017-02-06 16:30:00', 1, '2017-02-06 13:30:00', '2017-02-06 14:23:00', 53, 0, 2),
(39, 1, '2017-02-06 17:33:00', 1, '2017-02-06 14:33:00', '2017-02-06 15:26:00', 53, 0, 1),
(40, 1, '2017-02-08 08:30:00', 1, '2017-02-08 05:30:17', '2017-02-08 06:21:17', 53, 0, 2),
(41, 1, '2017-02-08 09:33:00', 1, '2017-02-08 06:33:00', '2017-02-08 07:26:00', 53, 0, 2),
(42, 1, '2017-02-08 15:00:00', 1, '2017-02-08 12:00:00', '2017-02-08 12:53:00', 53, 0, 2),
(45, 1, '2017-02-08 10:36:00', 1, '2017-02-08 07:36:00', '2017-02-08 08:29:00', 53, 0, 1),
(47, 1, '2017-02-10 06:29:00', 1, '2017-02-10 03:29:00', '2017-02-10 04:22:00', 53, 0, 1),
(48, 1, '2017-02-09 08:30:00', 1, '2017-02-09 05:30:00', '2017-02-09 06:23:00', 53, 0, 1),
(49, 1, '2017-02-09 09:52:00', 1, '2017-02-09 06:52:00', '2017-02-09 07:45:00', 53, 0, 1),
(51, 1, '2017-02-10 09:00:00', 1, '2017-02-10 06:00:00', '2017-02-10 06:53:00', 53, 0, 1),
(52, 1, '2017-02-10 10:22:00', 1, '2017-02-10 07:22:00', '2017-02-10 07:52:00', 30, 0, 1),
(56, 1, '2017-02-17 12:22:00', 1, '2017-02-17 09:51:57', '2017-02-17 10:43:49', 53, 0, 1),
(60, 1, '2017-02-17 21:00:00', 1, '2017-02-17 18:00:00', '2017-02-17 18:53:00', 53, 0, 1),
(61, 1, '2017-02-17 17:31:00', 1, '2017-02-17 14:31:00', '2017-02-17 15:24:00', 53, 0, 2),
(62, 1, '2017-02-17 18:44:00', 1, '2017-02-17 15:44:00', '2017-02-17 16:37:00', 53, 0, 1),
(70, 1, '2017-02-19 07:00:00', 1, '2017-02-19 04:00:00', '2017-02-19 04:53:00', 53, 0, 2),
(72, 1, '2017-02-19 09:58:00', 1, '2017-02-19 06:58:00', '2017-02-19 07:51:00', 53, 0, 1),
(73, 1, '2017-02-19 08:28:00', 1, '2017-02-19 05:28:00', '2017-02-19 06:21:00', 53, 0, 1),
(74, 1, '2017-02-21 13:00:00', 1, '2017-02-21 10:00:00', '2017-02-21 10:53:00', 53, 0, 1),
(77, 1, '2017-02-21 18:46:00', 1, '2017-02-21 15:50:00', '2017-02-21 16:43:00', 53, 0, 1),
(78, 1, '2017-02-21 20:30:00', 1, '2017-02-21 17:30:00', '2017-02-21 18:23:00', 53, 0, 1),
(79, 1, '2017-02-22 07:30:00', 1, '2017-02-22 04:30:00', '2017-02-22 05:23:00', 53, 0, 2),
(80, 1, '2017-02-22 06:30:00', 1, '2017-02-22 03:30:00', '2017-02-22 04:23:00', 53, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `provider_cancel_history`
--

CREATE TABLE `provider_cancel_history` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `date` timestamp NOT NULL,
  `comment` text CHARACTER SET latin1,
  `penalty_amount` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_cancel_history`
--

INSERT INTO `provider_cancel_history` (`id`, `provider_id`, `service_id`, `reason_id`, `date`, `comment`, `penalty_amount`) VALUES
(1, 1, 38, 2, '2017-02-17 12:14:25', 'test', 10),
(2, 1, 39, 2, '2017-02-17 12:15:31', 'test', 10),
(3, 1, 39, 2, '2017-02-17 14:48:02', 'test', 10),
(4, 1, 43, 2, '2017-02-20 09:46:23', 'test', 10),
(5, 1, 43, 2, '2017-02-20 09:53:09', 'test', 10);

-- --------------------------------------------------------

--
-- Table structure for table `provider_company_account`
--

CREATE TABLE `provider_company_account` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `pay_type` enum('office','agent') COLLATE utf8mb4_unicode_ci DEFAULT 'agent',
  `arrival_time` datetime DEFAULT NULL,
  `departure_time` datetime DEFAULT NULL,
  `agent_arrival_latitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_arrival_longitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_credit_balance` float DEFAULT NULL COMMENT 'balance if provider in a company',
  `sattled` enum('yes','no','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='this will be used for normal and company provider both';

--
-- Dumping data for table `provider_company_account`
--

INSERT INTO `provider_company_account` (`id`, `provider_id`, `pay_type`, `arrival_time`, `departure_time`, `agent_arrival_latitude`, `agent_arrival_longitude`, `address`, `current_credit_balance`, `sattled`) VALUES
(1, 16, 'office', '2016-11-11 11:38:50', '2016-11-09 00:00:00', '', '', 'test adr', 0, 'no'),
(2, 2, 'agent', NULL, '0000-00-00 00:00:00', NULL, NULL, 'test23 adr', 15, 'no'),
(6, 1, 'agent', '2016-12-05 19:10:00', '2016-12-05 20:10:00', '25.274017133947062', '51.44631385803223', 'test455', 88, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `provider_free_timeslot`
--

CREATE TABLE `provider_free_timeslot` (
  `id` bigint(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `weekday` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `total_duration` int(11) DEFAULT NULL COMMENT 'in minutes',
  `prev_book_latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_book_longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_book_latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_book_longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_type` enum('free','break','unavailable') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_free_timeslot`
--

INSERT INTO `provider_free_timeslot` (`id`, `provider_id`, `date`, `weekday`, `from_time`, `to_time`, `total_duration`, `prev_book_latitude`, `previous_book_longitude`, `next_book_latitude`, `next_book_longitude`, `time_type`) VALUES
(8, 2, '2017-02-24', 'friday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(9, 2, '2017-02-25', 'saturday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(10, 2, '2017-02-26', 'sunday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(11, 2, '2017-02-27', 'monday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(12, 2, '2017-02-21', 'tuesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(13, 2, '2017-02-22', 'wednesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(14, 2, '2017-02-23', 'thursday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(15, 5, '2017-02-24', 'friday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(16, 5, '2017-02-25', 'saturday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(17, 5, '2017-02-26', 'sunday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(18, 5, '2017-02-27', 'monday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(19, 5, '2017-02-21', 'tuesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(20, 5, '2017-02-22', 'wednesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(21, 5, '2017-02-23', 'thursday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(22, 7, '2017-02-24', 'friday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(23, 7, '2017-02-25', 'saturday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(24, 7, '2017-02-26', 'sunday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(25, 7, '2017-02-27', 'monday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(26, 7, '2017-02-21', 'tuesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(27, 7, '2017-02-22', 'wednesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(28, 7, '2017-02-23', 'thursday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(29, 8, '2017-02-24', 'friday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(30, 8, '2017-02-25', 'saturday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(31, 8, '2017-02-26', 'sunday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(32, 8, '2017-02-27', 'monday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(33, 8, '2017-02-21', 'tuesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(34, 8, '2017-02-22', 'wednesday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(35, 8, '2017-02-23', 'thursday', '06:00:00', '17:00:00', 660, NULL, NULL, NULL, NULL, 'free'),
(36, 1, '2017-02-24', 'friday', '11:00:00', '12:00:00', 60, NULL, NULL, NULL, NULL, 'unavailable'),
(37, 1, '2017-02-24', 'friday', '14:00:00', '17:00:00', 180, NULL, NULL, NULL, NULL, 'unavailable'),
(38, 1, '2017-02-24', 'friday', '06:00:00', '11:00:00', 300, NULL, NULL, NULL, NULL, 'free'),
(39, 1, '2017-02-24', 'friday', '12:00:00', '14:00:00', 120, NULL, NULL, NULL, NULL, 'free'),
(40, 1, '2017-02-24', 'friday', '17:00:00', '23:59:00', 419, NULL, NULL, NULL, NULL, 'free'),
(41, 1, '2017-02-25', 'saturday', '06:00:00', '23:59:00', 1079, NULL, NULL, NULL, NULL, 'unavailable'),
(42, 1, '2017-02-26', 'sunday', '14:00:00', '16:00:00', 120, NULL, NULL, NULL, NULL, 'unavailable'),
(43, 1, '2017-02-26', 'sunday', '06:00:00', '14:00:00', 480, NULL, NULL, NULL, NULL, 'free'),
(44, 1, '2017-02-26', 'sunday', '16:00:00', '23:59:00', 479, NULL, NULL, NULL, NULL, 'free'),
(45, 1, '2017-02-27', 'monday', '14:00:00', '16:00:00', 120, NULL, NULL, NULL, NULL, 'unavailable'),
(46, 1, '2017-02-27', 'monday', '19:10:00', '19:20:00', 10, NULL, NULL, NULL, NULL, 'unavailable'),
(47, 1, '2017-02-27', 'monday', '06:00:00', '14:00:00', 480, NULL, NULL, NULL, NULL, 'free'),
(48, 1, '2017-02-27', 'monday', '16:00:00', '19:10:00', 190, NULL, NULL, NULL, NULL, 'free'),
(49, 1, '2017-02-27', 'monday', '19:20:00', '23:59:00', 279, NULL, NULL, NULL, NULL, 'free'),
(50, 1, '2017-02-21', 'tuesday', '14:00:00', '16:00:00', 120, NULL, NULL, NULL, NULL, 'unavailable'),
(51, 1, '2017-02-21', 'tuesday', '06:00:00', '14:00:00', 480, NULL, NULL, NULL, NULL, 'free'),
(52, 1, '2017-02-21', 'tuesday', '16:00:00', '23:59:00', 479, NULL, NULL, NULL, NULL, 'free'),
(53, 1, '2017-02-22', 'wednesday', '06:00:00', '20:59:00', 899, NULL, NULL, NULL, NULL, 'free'),
(54, 1, '2017-02-23', 'thursday', '06:00:00', '23:59:00', 1079, NULL, NULL, NULL, NULL, 'free');

-- --------------------------------------------------------

--
-- Table structure for table `provider_free_timeslot_history`
--

CREATE TABLE `provider_free_timeslot_history` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `from_time` timestamp NOT NULL,
  `to_time` timestamp NOT NULL,
  `total_duration` int(11) DEFAULT NULL COMMENT 'in minutes',
  `prev_book_latitude` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `previous_book_longitude` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `next_book_latitude` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `next_book_longitude` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `time_type` enum('free','break','unavailable') CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='This will be backup of provider_free_timeslot';

-- --------------------------------------------------------

--
-- Table structure for table `provider_message`
--

CREATE TABLE `provider_message` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('inbox','draft','trash','contact') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inbox',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_message`
--

INSERT INTO `provider_message` (`id`, `provider_id`, `subject`, `message`, `type`, `email`, `message_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 'sadf sadfsdaf ds', 'inbox', NULL, NULL, '2017-01-10 09:05:04', '2017-01-10 09:05:04'),
(2, 1, 'test', 'sadf sadfsdaf ds', 'inbox', NULL, NULL, '2017-01-10 09:05:35', '2017-01-10 09:05:35');

-- --------------------------------------------------------

--
-- Table structure for table `provider_notification`
--

CREATE TABLE `provider_notification` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_latitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_longitude` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distance_from_customer` int(5) DEFAULT NULL,
  `duration_from_customer` int(5) DEFAULT NULL,
  `invite_status` enum('cancelled','sent','accepted') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_notice_status` enum('heading','must head now','confirmed','cancelled','jobstarted','jobcompleted','endsoon') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_notification`
--

INSERT INTO `provider_notification` (`id`, `provider_id`, `service_id`, `company_id`, `date`, `details`, `notification_latitude`, `notification_longitude`, `distance_from_customer`, `duration_from_customer`, `invite_status`, `service_notice_status`, `price`) VALUES
(7, 1, 1, NULL, '2017-02-03 06:21:14', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(12, 1, 2, NULL, '2017-02-03 08:12:36', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(14, 1, 2, NULL, '2017-02-03 08:52:10', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(15, 1, 2, NULL, '2017-02-03 09:04:24', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(16, 1, 3, NULL, '2017-02-03 09:06:24', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(17, 1, 3, NULL, '2017-02-03 09:17:02', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(22, 1, 5, NULL, '2017-02-03 10:33:34', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(23, 1, 5, NULL, '2017-02-03 10:36:10', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(24, 1, 6, NULL, '2017-02-03 10:38:10', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(25, 1, 6, NULL, '2017-02-03 10:38:32', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(26, 1, 7, NULL, '2017-02-03 10:59:31', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(27, 1, 7, NULL, '2017-02-03 11:31:24', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(31, 1, 8, NULL, '2017-02-03 13:12:49', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(32, 1, 8, NULL, '2017-02-03 13:19:50', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(33, 1, 9, NULL, '2017-02-03 13:20:47', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(34, 1, 9, NULL, '2017-02-03 13:21:17', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(36, 1, 11, NULL, '2017-02-03 13:28:52', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(40, 1, 14, NULL, '2017-02-06 13:08:31', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(41, 1, 14, NULL, '2017-02-06 13:09:44', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(42, 1, 15, NULL, '2017-02-06 13:10:31', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(43, 1, 15, NULL, '2017-02-06 13:11:54', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(44, 1, 16, NULL, '2017-02-07 08:21:31', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(45, 1, 16, NULL, '2017-02-07 08:23:31', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(46, 1, 17, NULL, '2017-02-07 08:23:37', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(47, 1, 17, NULL, '2017-02-07 08:23:53', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(48, 1, 18, NULL, '2017-02-07 08:24:20', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(49, 1, 18, NULL, '2017-02-07 08:24:34', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(50, 1, 19, NULL, '2017-02-07 08:25:24', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(51, 1, 19, NULL, '2017-02-07 08:27:03', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(52, 1, 19, NULL, '2017-02-07 09:53:05', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(53, 1, 19, NULL, '2017-02-07 11:54:17', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(56, 1, 21, NULL, '2017-02-07 13:34:10', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(57, 1, 21, NULL, '2017-02-07 13:43:27', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(58, 1, 23, NULL, '2017-02-08 05:50:03', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(59, 1, 23, NULL, '2017-02-08 05:50:43', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(60, 1, 24, NULL, '2017-02-08 05:55:06', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(61, 1, 24, NULL, '2017-02-08 06:13:43', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(62, 1, 25, NULL, '2017-02-08 10:20:34', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(63, 1, 25, NULL, '2017-02-08 10:21:02', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(64, 1, 25, NULL, '2017-02-08 10:35:16', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(65, 1, 26, NULL, '2017-02-08 10:37:01', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(66, 1, 26, NULL, '2017-02-08 10:48:45', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(67, 1, 16, NULL, '2017-02-08 10:57:17', 'Provider Started Heading.', '25.241039763035846', '51.51070688247681', 17, 21, NULL, 'heading', NULL),
(68, 1, 27, NULL, '2017-02-09 08:04:22', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(69, 1, 1, NULL, '2017-02-09 10:52:42', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(70, 1, 2, NULL, '2017-02-09 10:52:42', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(71, 1, 3, NULL, '2017-02-09 10:52:42', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(72, 1, 5, NULL, '2017-02-09 10:52:42', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(73, 1, 6, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(74, 1, 7, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(75, 1, 8, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(76, 1, 9, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(77, 1, 14, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(78, 1, 15, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(79, 1, 17, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(80, 1, 18, NULL, '2017-02-09 10:52:43', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(81, 1, 19, NULL, '2017-02-09 10:52:44', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(82, 1, 16, NULL, '2017-02-09 11:56:49', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(84, 1, 29, NULL, '2017-02-09 13:00:07', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(85, 1, 29, NULL, '2017-02-09 13:01:06', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(86, 1, 30, NULL, '2017-02-09 13:01:27', 'New job received', '25.32075293256326', '51.44815563774109', NULL, NULL, 'sent', NULL, 100),
(87, 1, 30, NULL, '2017-02-09 13:01:38', 'Job Accepted', '25.281039763035846', '51.51070688247681', NULL, NULL, 'accepted', 'confirmed', 100),
(88, 1, 23, NULL, '2017-02-10 15:16:23', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(89, 1, 24, NULL, '2017-02-10 15:16:23', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(90, 1, 21, NULL, '2017-02-13 07:11:07', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(91, 1, 25, NULL, '2017-02-13 07:11:08', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(92, 1, 26, NULL, '2017-02-13 07:11:08', 'Job Auto Cancelled', '25.281039763035846', '51.51070688247681', NULL, NULL, 'cancelled', 'cancelled', 100),
(93, 1, 31, NULL, '2017-02-17 05:41:45', 'New job received', '25.260059', '51.467428', NULL, NULL, 'sent', NULL, 100),
(95, 1, 31, NULL, '2017-02-17 05:58:50', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(97, 1, 33, NULL, '2017-02-17 06:15:15', 'New job received', '25.260059', '51.467428', NULL, NULL, 'sent', NULL, 100),
(98, 1, 33, NULL, '2017-02-17 06:15:50', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(99, 1, 34, NULL, '2017-02-17 06:26:35', 'New job received', '25.2881144', '51.548332', NULL, NULL, 'sent', NULL, 100),
(100, 1, 35, NULL, '2017-02-17 06:29:41', 'New job received', '25.2881144', '51.548332', NULL, NULL, 'sent', NULL, 100),
(101, 1, 34, NULL, '2017-02-17 06:31:55', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(102, 1, 35, NULL, '2017-02-17 06:32:15', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(103, 1, 35, NULL, '2017-02-17 06:47:15', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(104, 1, 33, NULL, '2017-02-17 07:25:14', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(105, 1, 34, NULL, '2017-02-17 08:28:57', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(106, 1, 31, NULL, '2017-02-17 09:23:24', 'Start heading soon', '25.240482', '51.456281', NULL, NULL, NULL, 'must head now', 100),
(107, 1, 36, NULL, '2017-02-17 09:45:47', 'New job received', '25.2881144', '51.548332', NULL, NULL, 'sent', NULL, 100),
(108, 1, 37, NULL, '2017-02-17 10:18:28', 'New job received', '25.2881144', '51.548332', NULL, NULL, 'sent', NULL, 100),
(109, 1, 37, NULL, '2017-02-17 10:19:30', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(110, 1, 38, NULL, '2017-02-17 11:51:34', 'New job received', '25.2881144', '51.548332', NULL, NULL, 'sent', NULL, 100),
(111, 1, 38, NULL, '2017-02-17 11:52:58', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(112, 1, 39, NULL, '2017-02-17 11:54:01', 'New job received', '25.2181144', '51.548332', NULL, NULL, 'sent', NULL, 100),
(113, 1, 39, NULL, '2017-02-17 11:54:31', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(114, 1, 38, NULL, '2017-02-17 12:06:51', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(115, 1, 39, NULL, '2017-02-17 12:07:07', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(116, 1, 38, NULL, '2017-02-17 12:14:25', 'Job Rejected', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(117, 1, 39, NULL, '2017-02-17 12:15:31', 'Job Rejected', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(118, 1, 38, NULL, '2017-02-17 14:14:27', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(119, 1, 39, NULL, '2017-02-17 14:14:36', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(120, 1, 38, NULL, '2017-02-17 14:27:26', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(121, 1, 39, NULL, '2017-02-17 14:27:34', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(122, 1, 40, NULL, '2017-02-17 14:28:49', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(123, 1, 40, NULL, '2017-02-17 14:29:44', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(124, 1, 39, NULL, '2017-02-17 14:48:02', 'Job Rejected', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(125, 1, 39, NULL, '2017-02-17 14:50:46', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(126, 1, 41, NULL, '2017-02-20 07:20:04', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(127, 1, 41, NULL, '2017-02-20 07:20:48', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(128, 1, 42, NULL, '2017-02-20 07:20:54', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(129, 1, 42, NULL, '2017-02-20 07:21:01', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(130, 1, 43, NULL, '2017-02-20 07:21:53', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(131, 1, 43, NULL, '2017-02-20 07:22:04', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(132, 1, 44, NULL, '2017-02-20 07:22:35', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(133, 1, 44, NULL, '2017-02-20 07:22:45', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(134, 1, 45, NULL, '2017-02-20 07:23:48', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(135, 1, 45, NULL, '2017-02-20 07:23:54', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(136, 1, 43, NULL, '2017-02-20 09:46:23', 'Job Rejected', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(137, 1, 43, NULL, '2017-02-20 09:49:25', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(138, 1, 43, NULL, '2017-02-20 09:53:09', 'Job Rejected', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(139, 1, 31, NULL, '2017-02-20 11:24:31', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(140, 1, 33, NULL, '2017-02-20 11:24:31', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(141, 1, 34, NULL, '2017-02-20 11:24:31', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(142, 1, 35, NULL, '2017-02-20 11:24:31', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(143, 1, 38, NULL, '2017-02-20 11:24:31', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(144, 1, 39, NULL, '2017-02-20 11:24:31', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(145, 1, 40, NULL, '2017-02-20 11:24:32', 'Job Auto Cancelled', '25.240482', '51.456281', NULL, NULL, 'cancelled', 'cancelled', 100),
(146, 1, 46, NULL, '2017-02-21 05:51:05', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(147, 1, 46, NULL, '2017-02-21 05:51:47', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100),
(149, 1, 48, NULL, '2017-02-21 06:12:55', 'New job received', '25.2181144', '51.598332', NULL, NULL, 'sent', NULL, 100),
(150, 1, 48, NULL, '2017-02-21 06:13:16', 'Job Accepted', '25.240482', '51.456281', NULL, NULL, 'accepted', 'confirmed', 100);

-- --------------------------------------------------------

--
-- Table structure for table `provider_perference_polygon`
--

CREATE TABLE `provider_perference_polygon` (
  `id` int(11) NOT NULL,
  `preference_id` int(11) NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='This table holds latigude longitude area of prefereed locati';

--
-- Dumping data for table `provider_perference_polygon`
--

INSERT INTO `provider_perference_polygon` (`id`, `preference_id`, `latitude`, `longitude`) VALUES
(41, 6, '25.2454723', '51.5680647'),
(42, 6, '25.2427551', ' 51.5637302'),
(43, 6, '25.2398438', ' 51.5515852'),
(44, 6, '25.2393391', ' 51.5466928'),
(45, 6, '25.2268001', ' 51.55725'),
(46, 6, '25.2293236', ' 51.5675497'),
(47, 6, '25.2313035', ' 51.5760469'),
(48, 6, '25.2454723', ' 51.5680647'),
(49, 7, '25.2454723', '51.5680647'),
(50, 7, '25.2590961', ' 51.5608978'),
(51, 7, '25.2633653', ' 51.5565634'),
(52, 7, '25.2578153', ' 51.5514994'),
(53, 7, '25.2542057', ' 51.5424442'),
(54, 7, '25.2542057', ' 51.5363503'),
(55, 7, '25.2517216', ' 51.5366077'),
(56, 7, '25.2393391', ' 51.5466928'),
(57, 7, '25.2398438', ' 51.5515852'),
(58, 7, '25.2427551', ' 51.5637302'),
(59, 7, '25.2454723', ' 51.5680647'),
(60, 8, '25.2393391', '51.5466928'),
(61, 8, '25.237864', ' 51.5338182'),
(62, 8, '25.2215202', ' 51.5367365'),
(63, 8, '25.2268001', ' 51.55725'),
(64, 8, '25.2393391', ' 51.5466928'),
(65, 9, '25.2542057', '51.5241623'),
(66, 9, '25.2415906', ' 51.5242481'),
(67, 9, '25.2371653', ' 51.5228748'),
(68, 9, '25.237864', ' 51.5338182'),
(69, 9, '25.2393391', ' 51.5466928'),
(70, 9, '25.2517216', ' 51.5366077'),
(71, 9, '25.2542057', ' 51.5363503'),
(72, 9, '25.2542057', ' 51.5241623'),
(73, 10, '25.2680614', '51.5502548'),
(74, 10, '25.2649566', ' 51.5465641'),
(75, 10, '25.2629384', ' 51.5407276'),
(76, 10, '25.2627056', ' 51.5378952'),
(77, 10, '25.2542057', ' 51.5363503'),
(78, 10, '25.2542057', ' 51.5424442'),
(79, 10, '25.2578153', ' 51.5514994'),
(80, 10, '25.2633653', ' 51.5565634'),
(81, 10, '25.2680614', ' 51.5502548'),
(82, 11, '25.2630161', '51.5244198'),
(83, 11, '25.2542057', ' 51.5241623'),
(84, 11, '25.2542057', ' 51.5363503'),
(85, 11, '25.2627056', ' 51.5378952'),
(86, 11, '25.2630161', ' 51.5244198'),
(87, 12, '25.3041486', '51.5179825'),
(88, 12, '25.3042845', ' 51.5151285'),
(89, 12, '25.3027133', ' 51.510043'),
(90, 12, '25.2997256', ' 51.5021038'),
(91, 12, '25.2955739', ' 51.5050222'),
(92, 12, '25.2932653', ' 51.5070821'),
(93, 12, '25.2906462', ' 51.5067387'),
(94, 12, '25.2845154', ' 51.505537'),
(95, 12, '25.2814885', ' 51.5064384'),
(96, 12, '25.2759393', ' 51.5086914'),
(97, 12, '25.2722527', ' 51.5107727'),
(98, 12, '25.2776858', ' 51.5174243'),
(99, 12, '25.2818767', ' 51.5247631'),
(100, 12, '25.2795483', ' 51.5271664'),
(101, 12, '25.2797036', ' 51.5351486'),
(102, 12, '25.2910342', ' 51.5346336'),
(103, 12, '25.3041486', ' 51.5179825'),
(104, 13, '25.2724855', '51.5272522'),
(105, 13, '25.2630161', ' 51.5244198'),
(106, 13, '25.2627056', ' 51.5378952'),
(107, 13, '25.2629384', ' 51.5407276'),
(108, 13, '25.2649566', ' 51.5465641'),
(109, 13, '25.2680614', ' 51.5502548'),
(110, 13, '25.2797036', ' 51.5351486'),
(111, 13, '25.2795483', ' 51.5271664'),
(112, 13, '25.2724855', ' 51.5272522'),
(113, 14, '25.2679061', '51.5144634'),
(114, 14, '25.2640251', ' 51.5203857'),
(115, 14, '25.2630161', ' 51.5244198'),
(116, 14, '25.2724855', ' 51.5272522'),
(117, 14, '25.2795483', ' 51.5271664'),
(118, 14, '25.2818767', ' 51.5247631'),
(119, 14, '25.2776858', ' 51.5174243'),
(120, 14, '25.2722527', ' 51.5107727'),
(121, 14, '25.2679061', ' 51.5144634'),
(122, 15, '25.2546326', '51.5069103'),
(123, 15, '25.2406589', ' 51.5179825'),
(124, 15, '25.2371653', ' 51.5228748'),
(125, 15, '25.2415906', ' 51.5242481'),
(126, 15, '25.2542057', ' 51.5241623'),
(127, 15, '25.2546326', ' 51.5069103'),
(128, 16, '25.2641804', '51.4970398'),
(129, 16, '25.2589797', ' 51.5003014'),
(130, 16, '25.2546326', ' 51.5069103'),
(131, 16, '25.2542057', ' 51.5241623'),
(132, 16, '25.2630161', ' 51.5244198'),
(133, 16, '25.2640251', ' 51.5203857'),
(134, 16, '25.2679061', ' 51.5144634'),
(135, 16, '25.2722527', ' 51.5107727'),
(136, 16, '25.2641804', ' 51.4970398'),
(137, 17, '25.2799364', '51.4861393'),
(138, 17, '25.2754736', ' 51.4888433'),
(139, 17, '25.2641804', ' 51.4970398'),
(140, 17, '25.2722527', ' 51.5107727'),
(141, 17, '25.2759393', ' 51.5086914'),
(142, 17, '25.2814885', ' 51.5064384'),
(143, 17, '25.2799364', ' 51.4861393'),
(144, 18, '25.2951472', '51.475153'),
(145, 18, '25.2919655', ' 51.4762688'),
(146, 18, '25.2880853', ' 51.4801311'),
(147, 18, '25.2799364', ' 51.4861393'),
(148, 18, '25.2814885', ' 51.5064384'),
(149, 18, '25.2845154', ' 51.505537'),
(150, 18, '25.2906462', ' 51.5067387'),
(151, 18, '25.294216', ' 51.484766'),
(152, 18, '25.2951472', ' 51.475153'),
(153, 19, '25.3070196', '51.4728355'),
(154, 19, '25.2951472', ' 51.475153'),
(155, 19, '25.294216', ' 51.484766'),
(156, 19, '25.2906462', ' 51.5067387'),
(157, 19, '25.2932653', ' 51.5070821'),
(158, 19, '25.2955739', ' 51.5050222'),
(159, 19, '25.2997256', ' 51.5021038'),
(160, 19, '25.3109768', ' 51.4966106'),
(161, 19, '25.3067092', ' 51.4882851'),
(162, 19, '25.3077179', ' 51.4785004'),
(163, 19, '25.3070196', ' 51.4728355'),
(164, 20, '25.3359585', '51.5172958'),
(165, 20, '25.3247096', ' 51.516695'),
(166, 20, '25.3202098', ' 51.5139484'),
(167, 20, '25.3164856', ' 51.5079403'),
(168, 20, '25.314585', ' 51.5061382'),
(169, 20, '25.3099681', ' 51.5149784'),
(170, 20, '25.3073299', ' 51.5143776'),
(171, 20, '25.3042845', ' 51.5151285'),
(172, 20, '25.3041486', ' 51.5179825'),
(173, 20, '25.3089594', ' 51.5184116'),
(174, 20, '25.3144683', ' 51.5233898'),
(175, 20, '25.3167184', ' 51.539011'),
(176, 20, '25.3273474', ' 51.5443325'),
(177, 20, '25.333864', ' 51.5350628'),
(178, 20, '25.3359585', ' 51.5172958'),
(179, 21, '25.3672947', '51.5227032'),
(180, 21, '25.3638823', ' 51.5215874'),
(181, 21, '25.3589185', ' 51.5209007'),
(182, 21, '25.3410008', ' 51.5211582'),
(183, 21, '25.3359585', ' 51.5172958'),
(184, 21, '25.333864', ' 51.5350628'),
(185, 21, '25.3626414', ' 51.5364361'),
(186, 21, '25.3672947', ' 51.5227032'),
(187, 22, '25.3541097', '51.5880203'),
(188, 22, '25.3840454', ' 51.5869045'),
(189, 22, '25.3840454', ' 51.5401268'),
(190, 22, '25.3733439', ' 51.5307713'),
(191, 22, '25.3626414', ' 51.5364361'),
(192, 22, '25.3513949', ' 51.5749741'),
(193, 22, '25.3541097', ' 51.5880203'),
(194, 23, '25.3840454', '51.5401268'),
(195, 23, '25.3890082', ' 51.5315437'),
(196, 23, '25.3942808', ' 51.5396118'),
(197, 23, '25.4294777', ' 51.5394402'),
(198, 23, '25.4714835', ' 51.5497398'),
(199, 23, '25.4603289', ' 51.4854505'),
(200, 23, '25.4575349', ' 51.478157'),
(201, 23, '25.4386242', ' 51.4819336'),
(202, 23, '25.4079267', ' 51.4889717'),
(203, 23, '25.393195', ' 51.4973835'),
(204, 23, '25.3894734', ' 51.4992714'),
(205, 23, '25.3778417', ' 51.4996147'),
(206, 23, '25.372103', ' 51.5068245'),
(207, 23, '25.3840454', ' 51.5401268'),
(208, 24, '25.3424746', '51.5061378'),
(209, 24, '25.3423195', ' 51.5095711'),
(210, 24, '25.3359585', ' 51.5172958'),
(211, 24, '25.3410008', ' 51.5211582'),
(212, 24, '25.3589185', ' 51.5209007'),
(213, 24, '25.3638823', ' 51.5215874'),
(214, 24, '25.364503', ' 51.5165019'),
(215, 24, '25.3603149', ' 51.5093566'),
(216, 24, '25.3620986', ' 51.5041853'),
(217, 24, '25.3665192', ' 51.5015888'),
(218, 24, '25.3650456', ' 51.4983701'),
(219, 24, '25.366209', ' 51.4961815'),
(220, 24, '25.3641925', ' 51.4949799'),
(221, 24, '25.3579878', ' 51.4947224'),
(222, 24, '25.3502315', ' 51.5008165'),
(223, 24, '25.3475942', ' 51.5042496'),
(224, 24, '25.3424746', ' 51.5061378'),
(225, 25, '25.3205977', '51.4699173'),
(226, 25, '25.3070196', ' 51.4728355'),
(227, 25, '25.3077179', ' 51.4785004'),
(228, 25, '25.3067092', ' 51.4882851'),
(229, 25, '25.3219942', ' 51.4863968'),
(230, 25, '25.3230804', ' 51.4795303'),
(231, 25, '25.3205977', ' 51.4699173'),
(232, 26, '25.3320021', '51.4643383'),
(233, 26, '25.3205977', ' 51.4699173'),
(234, 26, '25.3230804', ' 51.4795303'),
(235, 26, '25.3219942', ' 51.4863968'),
(236, 26, '25.3254854', ' 51.4856243'),
(237, 26, '25.3315366', ' 51.4823627'),
(238, 26, '25.3352603', ' 51.4822769'),
(239, 26, '25.334407', ' 51.4726639'),
(240, 26, '25.3320021', ' 51.4643383'),
(241, 27, '25.3284335', '51.5049362'),
(242, 27, '25.3225373', ' 51.4991856'),
(243, 27, '25.3179598', ' 51.4960957'),
(244, 27, '25.314585', ' 51.5061382'),
(245, 27, '25.3164856', ' 51.5079403'),
(246, 27, '25.3202098', ' 51.5139484'),
(247, 27, '25.3247096', ' 51.516695'),
(248, 27, '25.3359585', ' 51.5172958'),
(249, 27, '25.3423195', ' 51.5095711'),
(250, 27, '25.3424746', ' 51.5061378'),
(251, 27, '25.3344846', ' 51.5057945'),
(252, 27, '25.3284335', ' 51.5049362'),
(253, 28, '25.3345621', '51.4942932'),
(254, 28, '25.3352603', ' 51.4822769'),
(255, 28, '25.3315366', ' 51.4823627'),
(256, 28, '25.3254854', ' 51.4856243'),
(257, 28, '25.3219942', ' 51.4863968'),
(258, 28, '25.3067092', ' 51.4882851'),
(259, 28, '25.3109768', ' 51.4966106'),
(260, 28, '25.3142356', ' 51.4953232'),
(261, 28, '25.3179598', ' 51.4960957'),
(262, 28, '25.3225373', ' 51.4991856'),
(263, 28, '25.3284335', ' 51.5049362'),
(264, 28, '25.3345621', ' 51.4942932'),
(265, 29, '25.2568062', '51.4843369'),
(266, 29, '25.2384851', ' 51.4975548'),
(267, 29, '25.231187', ' 51.5045929'),
(268, 29, '25.2255966', ' 51.5092278'),
(269, 29, '25.2371653', ' 51.5228748'),
(270, 29, '25.2406589', ' 51.5179825'),
(271, 29, '25.2546326', ' 51.5069103'),
(272, 29, '25.2589797', ' 51.5003014'),
(273, 29, '25.2641804', ' 51.4970398'),
(274, 29, '25.2568062', ' 51.4843369'),
(275, 30, '25.2120856', '51.4970398'),
(276, 30, '25.2255966', ' 51.5092278'),
(277, 30, '25.231187', ' 51.5045929'),
(278, 30, '25.2384851', ' 51.4975548'),
(279, 30, '25.2568062', ' 51.4843369'),
(280, 30, '25.2489279', ' 51.4715885'),
(281, 30, '25.2291683', ' 51.4889717'),
(282, 30, '25.2221802', ' 51.4827919'),
(283, 30, '25.2120856', ' 51.4970398'),
(284, 31, '25.3100457', '51.4074326'),
(285, 31, '25.3073299', ' 51.4064884'),
(286, 31, '25.3102211', ' 51.3981417'),
(287, 31, '25.3001912', ' 51.3936996'),
(288, 31, '25.2968543', ' 51.4064884'),
(289, 31, '25.2923535', ' 51.4062309'),
(290, 31, '25.263637', ' 51.4064026'),
(291, 31, '25.2667418', ' 51.4139557'),
(292, 31, '25.2667418', ' 51.4204788'),
(293, 31, '25.2695361', ' 51.4312935'),
(294, 31, '25.2718639', ' 51.4316368'),
(295, 31, '25.2830398', ' 51.4319802'),
(296, 31, '25.3001135', ' 51.4300919'),
(297, 31, '25.3100457', ' 51.4074326'),
(298, 32, '25.349068', '51.4785862'),
(299, 32, '25.3352603', ' 51.4822769'),
(300, 32, '25.3345621', ' 51.4942932'),
(301, 32, '25.3284335', ' 51.5049362'),
(302, 32, '25.3344846', ' 51.5057945'),
(303, 32, '25.3424746', ' 51.5061378'),
(304, 32, '25.3475942', ' 51.5042496'),
(305, 32, '25.3502315', ' 51.5008165'),
(306, 32, '25.3486026', ' 51.4981556'),
(307, 32, '25.3483699', ' 51.4883709'),
(308, 32, '25.349068', ' 51.4785862'),
(309, 33, '25.3515501', '51.4544678'),
(310, 33, '25.3320021', ' 51.4643383'),
(311, 33, '25.334407', ' 51.4726639'),
(312, 33, '25.3352603', ' 51.4822769'),
(313, 33, '25.349068', ' 51.4785862'),
(314, 33, '25.3543424', ' 51.4787579'),
(315, 33, '25.358298', ' 51.4773846'),
(316, 33, '25.3515501', ' 51.4544678'),
(317, 34, '25.3823394', '51.478157'),
(318, 34, '25.3803233', ' 51.4790154'),
(319, 34, '25.3634169', ' 51.4790154'),
(320, 34, '25.3634169', ' 51.4881134'),
(321, 34, '25.3580654', ' 51.4877701'),
(322, 34, '25.3579878', ' 51.4947224'),
(323, 34, '25.3641925', ' 51.4949799'),
(324, 34, '25.3682254', ' 51.4977264'),
(325, 34, '25.3741194', ' 51.4977264'),
(326, 34, '25.3778417', ' 51.4996147'),
(327, 34, '25.3894734', ' 51.4992714'),
(328, 34, '25.393195', ' 51.4973835'),
(329, 34, '25.3823394', ' 51.478157'),
(330, 35, '25.3648905', '51.447773'),
(331, 35, '25.3515501', ' 51.4544678'),
(332, 35, '25.358298', ' 51.4773846'),
(333, 35, '25.3597717', ' 51.4768696'),
(334, 35, '25.3607024', ' 51.4790154'),
(335, 35, '25.3634169', ' 51.4790154'),
(336, 35, '25.3803233', ' 51.4790154'),
(337, 35, '25.3823394', ' 51.478157'),
(338, 35, '25.3648905', ' 51.447773'),
(339, 36, '25.3579878', '51.4833069'),
(340, 36, '25.3580654', ' 51.4877701'),
(341, 36, '25.3634169', ' 51.4881134'),
(342, 36, '25.3634169', ' 51.4790154'),
(343, 36, '25.3607024', ' 51.4790154'),
(344, 36, '25.3597717', ' 51.4768696'),
(345, 36, '25.358298', ' 51.4773846'),
(346, 36, '25.3579878', ' 51.4833069'),
(347, 37, '25.358298', '51.4773846'),
(348, 37, '25.3543424', ' 51.4787579'),
(349, 37, '25.349068', ' 51.4785862'),
(350, 37, '25.3483699', ' 51.4883709'),
(351, 37, '25.3486026', ' 51.4981556'),
(352, 37, '25.3502315', ' 51.5008165'),
(353, 37, '25.3579878', ' 51.4947224'),
(354, 37, '25.3580654', ' 51.4877701'),
(355, 37, '25.3579878', ' 51.4833069'),
(356, 37, '25.358298', ' 51.4773846'),
(357, 38, '25.2106878', '51.4064026'),
(358, 38, '25.2035433', ' 51.3943863'),
(359, 38, '25.1488586', ' 51.4345551'),
(360, 38, '25.1723201', ' 51.4697456'),
(361, 38, '25.1926705', ' 51.4554977'),
(362, 38, '25.212085', ' 51.4417664'),
(363, 38, '25.2249755', ' 51.4316368'),
(364, 38, '25.2106878', ' 51.4064026'),
(365, 39, '25.1580262', '51.38134'),
(366, 39, '25.1239938', ' 51.3979912'),
(367, 39, '25.1145133', ' 51.4181596'),
(368, 39, '25.1268683', ' 51.4454106'),
(369, 39, '25.1488586', ' 51.4345551'),
(370, 39, '25.2035433', ' 51.3943863'),
(371, 39, '25.1987283', ' 51.3854599'),
(372, 39, '25.1692129', ' 51.4010811'),
(373, 39, '25.1580262', ' 51.38134'),
(374, 40, '25.2619293', '51.3447762'),
(375, 40, '25.2158129', ' 51.3497543'),
(376, 40, '25.2318878', ' 51.3944698'),
(377, 40, '25.2367771', ' 51.4045143'),
(378, 40, '25.263637', ' 51.4064026'),
(379, 40, '25.2706227', ' 51.3825417'),
(380, 40, '25.2619293', ' 51.3447762'),
(381, 41, '25.1723201', '51.3272667'),
(382, 41, '25.1856803', ' 51.3631439'),
(383, 41, '25.1928258', ' 51.3579941'),
(384, 41, '25.2158129', ' 51.3497543'),
(385, 41, '25.2619293', ' 51.3447762'),
(386, 41, '25.2522237', ' 51.2990254'),
(387, 41, '25.2077368', ' 51.3171387'),
(388, 41, '25.1723201', ' 51.3272667'),
(389, 42, '25.2367771', '51.4045143'),
(390, 42, '25.2457828', ' 51.4200497'),
(391, 42, '25.2521485', ' 51.4309502'),
(392, 42, '25.2695361', ' 51.4312935'),
(393, 42, '25.2667418', ' 51.4204788'),
(394, 42, '25.2667418', ' 51.4139557'),
(395, 42, '25.263637', ' 51.4064026'),
(396, 42, '25.2367771', ' 51.4045143'),
(397, 43, '25.2158129', '51.3497543'),
(398, 43, '25.1928258', ' 51.3579941'),
(399, 43, '25.1856803', ' 51.3631439'),
(400, 43, '25.1987283', ' 51.3854599'),
(401, 43, '25.2035433', ' 51.3943863'),
(402, 43, '25.2106878', ' 51.4064026'),
(403, 43, '25.2318878', ' 51.3944698'),
(404, 43, '25.2158129', ' 51.3497543'),
(405, 44, '25.1856803', '51.3631439'),
(406, 44, '25.1580262', ' 51.38134'),
(407, 44, '25.1692129', ' 51.4010811'),
(408, 44, '25.1987283', ' 51.3854599'),
(409, 44, '25.1856803', ' 51.3631439'),
(410, 45, '25.2521485', '51.4309502'),
(411, 45, '25.2457828', ' 51.4200497'),
(412, 45, '25.2367771', ' 51.4045143'),
(413, 45, '25.2318878', ' 51.3944698'),
(414, 45, '25.2106878', ' 51.4064026'),
(415, 45, '25.2249755', ' 51.4316368'),
(416, 45, '25.2521485', ' 51.4309502'),
(417, 46, '25.2214037', '51.4687157'),
(418, 46, '25.2221802', ' 51.4827919'),
(419, 46, '25.2291683', ' 51.4889717'),
(420, 46, '25.2489279', ' 51.4715885'),
(421, 46, '25.2444061', ' 51.4634553'),
(422, 46, '25.2249755', ' 51.4316368'),
(423, 46, '25.212085', ' 51.4417664'),
(424, 46, '25.2214037', ' 51.4687157'),
(425, 47, '25.2641804', '51.4970398'),
(426, 47, '25.2754736', ' 51.4888433'),
(427, 47, '25.2695283', ' 51.4756551'),
(428, 47, '25.264925', ' 51.4640406'),
(429, 47, '25.2610583', ' 51.4544008'),
(430, 47, '25.2520336', ' 51.4583276'),
(431, 47, '25.2444061', ' 51.4634553'),
(432, 47, '25.2489279', ' 51.4715885'),
(433, 47, '25.2568062', ' 51.4843369'),
(434, 47, '25.2641804', ' 51.4970398'),
(435, 48, '25.3179598', '51.4960957'),
(436, 48, '25.3142356', ' 51.4953232'),
(437, 48, '25.3109768', ' 51.4966106'),
(438, 48, '25.2997256', ' 51.5021038'),
(439, 48, '25.3027133', ' 51.510043'),
(440, 48, '25.3042845', ' 51.5151285'),
(441, 48, '25.3073299', ' 51.5143776'),
(442, 48, '25.3099681', ' 51.5149784'),
(443, 48, '25.314585', ' 51.5061382'),
(444, 48, '25.3179598', ' 51.4960957'),
(445, 49, '25.2718639', '51.4316368'),
(446, 49, '25.2695361', ' 51.4312935'),
(447, 49, '25.2521485', ' 51.4309502'),
(448, 49, '25.2557888', ' 51.4403576'),
(449, 49, '25.2610583', ' 51.4544008'),
(450, 49, '25.2657716', ' 51.4519358'),
(451, 49, '25.2682942', ' 51.4473009'),
(452, 49, '25.270157', ' 51.43713'),
(453, 49, '25.2729124', ' 51.43713'),
(454, 49, '25.2718639', ' 51.4316368'),
(455, 50, '25.3001135', '51.4300919'),
(456, 50, '25.2967768', ' 51.4469147'),
(457, 50, '25.3043814', ' 51.4536953'),
(458, 50, '25.3079507', ' 51.4549828'),
(459, 50, '25.3195891', ' 51.4487171'),
(460, 50, '25.3256406', ' 51.4471722'),
(461, 50, '25.3228477', ' 51.4226246'),
(462, 50, '25.3179598', ' 51.4206505'),
(463, 50, '25.3146235', ' 51.4223671'),
(464, 50, '25.3100457', ' 51.4074326'),
(465, 50, '25.3001135', ' 51.4300919'),
(466, 51, '25.3001135', '51.4300919'),
(467, 51, '25.2830398', ' 51.4319802'),
(468, 51, '25.2718639', ' 51.4316368'),
(469, 51, '25.2729124', ' 51.43713'),
(470, 51, '25.270157', ' 51.43713'),
(471, 51, '25.2682942', ' 51.4473009'),
(472, 51, '25.2657716', ' 51.4519358'),
(473, 51, '25.2610583', ' 51.4544008'),
(474, 51, '25.264925', ' 51.4640406'),
(475, 51, '25.2695283', ' 51.4756551'),
(476, 51, '25.2754736', ' 51.4888433'),
(477, 51, '25.2799364', ' 51.4861393'),
(478, 51, '25.2880853', ' 51.4801311'),
(479, 51, '25.2919655', ' 51.4762688'),
(480, 51, '25.2951472', ' 51.475153'),
(481, 51, '25.2967768', ' 51.4469147'),
(482, 51, '25.3001135', ' 51.4300919'),
(483, 52, '25.3195891', '51.4487171'),
(484, 52, '25.3079507', ' 51.4549828'),
(485, 52, '25.3043814', ' 51.4536953'),
(486, 52, '25.2967768', ' 51.4469147'),
(487, 52, '25.2951472', ' 51.475153'),
(488, 52, '25.3070196', ' 51.4728355'),
(489, 52, '25.3205977', ' 51.4699173'),
(490, 52, '25.3195891', ' 51.4487171'),
(491, 53, '25.3228477', '51.4226246'),
(492, 53, '25.3256406', ' 51.4471722'),
(493, 53, '25.3195891', ' 51.4487171'),
(494, 53, '25.3205977', ' 51.4699173'),
(495, 53, '25.3320021', ' 51.4643383'),
(496, 53, '25.3515501', ' 51.4544678'),
(497, 53, '25.3586858', ' 51.4508629'),
(498, 53, '25.3516276', ' 51.4285469'),
(499, 53, '25.3407681', ' 51.4321518'),
(500, 53, '25.3228477', ' 51.4226246'),
(501, 54, '25.3586858', '51.4508629'),
(502, 54, '25.3648905', ' 51.447773'),
(503, 54, '25.3791601', ' 51.4405632'),
(504, 54, '25.3673723', ' 51.4113808'),
(505, 54, '25.3488353', ' 51.419878'),
(506, 54, '25.3516276', ' 51.4285469'),
(507, 54, '25.3586858', ' 51.4508629'),
(508, 55, '25.3791601', '51.4405632'),
(509, 55, '25.4393217', ' 51.4104366'),
(510, 55, '25.4321132', ' 51.3825416'),
(511, 55, '25.3673723', ' 51.4113808'),
(512, 55, '25.3791601', ' 51.4405632'),
(513, 56, '25.4126558', '51.4234829'),
(514, 56, '25.3791601', ' 51.4405632'),
(515, 56, '25.3648905', ' 51.447773'),
(516, 56, '25.3823394', ' 51.478157'),
(517, 56, '25.3899386', ' 51.4651108'),
(518, 56, '25.4212609', ' 51.4466572'),
(519, 56, '25.4126558', ' 51.4234829'),
(520, 57, '25.237864', '51.5338182'),
(521, 57, '25.2371653', ' 51.5228748'),
(522, 57, '25.2255966', ' 51.5092278'),
(523, 57, '25.2181424', ' 51.522274'),
(524, 57, '25.2215202', ' 51.5367365'),
(525, 57, '25.237864', ' 51.5338182'),
(526, 58, '25.2255966', '51.5092278'),
(527, 58, '25.2120856', ' 51.4970398'),
(528, 58, '25.206339', ' 51.4872551'),
(529, 58, '25.1810199', ' 51.5027905'),
(530, 58, '25.2000486', ' 51.5287113'),
(531, 58, '25.2181424', ' 51.522274'),
(532, 58, '25.2255966', ' 51.5092278'),
(533, 59, '25.1810199', '51.5027905'),
(534, 59, '25.206339', ' 51.4872551'),
(535, 59, '25.1926705', ' 51.4554977'),
(536, 59, '25.1723201', ' 51.4697456'),
(537, 59, '25.1810199', ' 51.5027905'),
(538, 60, '25.2000486', '51.5287113'),
(539, 60, '25.2139127', ' 51.5726974'),
(540, 60, '25.218453', ' 51.5851021'),
(541, 60, '25.2313035', ' 51.5760469'),
(542, 60, '25.2268001', ' 51.55725'),
(543, 60, '25.2215202', ' 51.5367365'),
(544, 60, '25.2181424', ' 51.522274'),
(545, 60, '25.2000486', ' 51.5287113'),
(546, 61, '25.2821094', '51.537466'),
(547, 61, '25.2797036', ' 51.5351486'),
(548, 61, '25.2680614', ' 51.5502548'),
(549, 61, '25.2633653', ' 51.5565634'),
(550, 61, '25.2626279', ' 51.5633869'),
(551, 61, '25.2836617', ' 51.5533448'),
(552, 61, '25.2837006', ' 51.5496969'),
(553, 61, '25.2821094', ' 51.537466'),
(554, 62, '25.3095025', '51.5583229'),
(555, 62, '25.3041486', ' 51.5179825'),
(556, 62, '25.2910342', ' 51.5346336'),
(557, 62, '25.2797036', ' 51.5351486'),
(558, 62, '25.2821094', ' 51.537466'),
(559, 62, '25.2837006', ' 51.5496969'),
(560, 62, '25.2836617', ' 51.5533448'),
(561, 62, '25.3033726', ' 51.5641594'),
(562, 62, '25.3095025', ' 51.5583229'),
(563, 63, '25.2836617', '51.5533448'),
(564, 63, '25.2770648', ' 51.5627861'),
(565, 63, '25.276211', ' 51.5893078'),
(566, 63, '25.3001911', ' 51.5840721'),
(567, 63, '25.3033726', ' 51.5641594'),
(568, 63, '25.2836617', ' 51.5533448'),
(569, 64, '25.276211', '51.5893078'),
(570, 64, '25.2770648', ' 51.5627861'),
(571, 64, '25.2836617', ' 51.5533448'),
(572, 64, '25.2626279', ' 51.5633869'),
(573, 64, '25.2633653', ' 51.5565634'),
(574, 64, '25.2590961', ' 51.5608978'),
(575, 64, '25.2454723', ' 51.5680647'),
(576, 64, '25.2313035', ' 51.5760469'),
(577, 64, '25.2394168', ' 51.5911102'),
(578, 64, '25.276211', ' 51.5893078'),
(579, 65, '25.3001911', '51.5840721'),
(580, 65, '25.276211', ' 51.5893078'),
(581, 65, '25.2394168', ' 51.5911102'),
(582, 65, '25.2510617', ' 51.6374588'),
(583, 65, '25.3043038', ' 51.6122246'),
(584, 65, '25.3001911', ' 51.5840721'),
(585, 66, '25.218453', '51.5851021'),
(586, 66, '25.2139127', ' 51.5726974'),
(587, 66, '25.2075815', ' 51.576004'),
(588, 66, '25.2114643', ' 51.586647'),
(589, 66, '25.218453', ' 51.5851021'),
(590, 67, '25.16797', '51.5814972'),
(591, 67, '25.1668824', ' 51.621151'),
(592, 67, '25.1963984', ' 51.620636'),
(593, 67, '25.1951558', ' 51.6048431'),
(594, 67, '25.1922044', ' 51.5969467'),
(595, 67, '25.1892531', ' 51.5873337'),
(596, 67, '25.16797', ' 51.5814972'),
(597, 68, '25.1668824', '51.621151'),
(598, 68, '25.16797', ' 51.5814972'),
(599, 68, '25.1337849', ' 51.5816689'),
(600, 68, '25.1326971', ' 51.621151'),
(601, 68, '25.1668824', ' 51.621151'),
(602, 69, '25.1791557', '51.5169526'),
(603, 69, '25.1386025', ' 51.5171242'),
(604, 69, '25.1337849', ' 51.5816689'),
(605, 69, '25.16797', ' 51.5814972'),
(606, 69, '25.1892531', ' 51.5873337'),
(607, 69, '25.186771', ' 51.5526562'),
(608, 69, '25.1791557', ' 51.5169526'),
(609, 70, '25.3153218', '51.3688088'),
(610, 70, '25.3114048', ' 51.3894514'),
(611, 70, '25.3102211', ' 51.3981417'),
(612, 70, '25.3073299', ' 51.4064884'),
(613, 70, '25.3100457', ' 51.4074326'),
(614, 70, '25.3146235', ' 51.4223671'),
(615, 70, '25.3179598', ' 51.4206505'),
(616, 70, '25.3228477', ' 51.4226246'),
(617, 70, '25.3407681', ' 51.4321518'),
(618, 70, '25.3516276', ' 51.4285469'),
(619, 70, '25.3488353', ' 51.419878'),
(620, 70, '25.3519393', ' 51.3952872'),
(621, 70, '25.3584532', ' 51.3672637'),
(622, 70, '25.3238562', ' 51.3710403'),
(623, 70, '25.3153218', ' 51.3688088'),
(624, 71, '25.3823394', '51.478157'),
(625, 71, '25.393195', ' 51.4973835'),
(626, 71, '25.4079267', ' 51.4889717'),
(627, 71, '25.4386242', ' 51.4819336'),
(628, 71, '25.4575349', ' 51.478157'),
(629, 71, '25.4578466', ' 51.4694878'),
(630, 71, '25.454125', ' 51.4331818'),
(631, 71, '25.4330434', ' 51.4352417'),
(632, 71, '25.4212609', ' 51.4466572'),
(633, 71, '25.3899386', ' 51.4651108'),
(634, 71, '25.3823394', ' 51.478157'),
(635, 72, '25.4321132', '51.3825416'),
(636, 72, '25.4393217', ' 51.4104366'),
(637, 72, '25.5130099', ' 51.4098358'),
(638, 72, '25.5136295', ' 51.3815117'),
(639, 72, '25.5052635', ' 51.3533592'),
(640, 72, '25.4321132', ' 51.3825416'),
(641, 73, '25.4126558', '51.4234829'),
(642, 73, '25.4212609', ' 51.4466572'),
(643, 73, '25.4330434', ' 51.4352417'),
(644, 73, '25.454125', ' 51.4331818'),
(645, 73, '25.5128549', ' 51.4252853'),
(646, 73, '25.5130099', ' 51.4098358'),
(647, 73, '25.4393217', ' 51.4104366'),
(648, 73, '25.4126558', ' 51.4234829'),
(649, 74, '25.0669411', '51.4881134'),
(650, 74, '24.9580476', ' 51.5069962'),
(651, 74, '24.95618', ' 51.6093063'),
(652, 74, '25.0579222', ' 51.6745377'),
(653, 74, '25.0669411', ' 51.4881134'),
(654, 75, '24.95618', '51.6093063'),
(655, 75, '24.9580476', ' 51.5069962'),
(656, 75, '24.838465', ' 51.4668274'),
(657, 75, '24.8291177', ' 51.5107727'),
(658, 75, '24.95618', ' 51.6093063'),
(659, 76, '25.6142862', '51.5608978'),
(660, 76, '25.7331067', ' 51.6357422'),
(661, 76, '25.7881448', ' 51.4606476'),
(662, 76, '25.6514303', ' 51.3933563'),
(663, 76, '25.6291452', ' 51.4812469'),
(664, 76, '25.6260498', ' 51.4929199'),
(665, 76, '25.6142862', ' 51.5608978'),
(666, 77, '25.3840454', '51.5401268'),
(667, 77, '25.372103', ' 51.5068245'),
(668, 77, '25.3778417', ' 51.4996147'),
(669, 77, '25.3741194', ' 51.4977264'),
(670, 77, '25.3682254', ' 51.4977264'),
(671, 77, '25.366209', ' 51.4961815'),
(672, 77, '25.3650456', ' 51.4983701'),
(673, 77, '25.3665192', ' 51.5015888'),
(674, 77, '25.3620986', ' 51.5041853'),
(675, 77, '25.3603149', ' 51.5093566'),
(676, 77, '25.364503', ' 51.5165019'),
(677, 77, '25.3638823', ' 51.5215874'),
(678, 77, '25.3672947', ' 51.5227032'),
(679, 77, '25.3626414', ' 51.5364361'),
(680, 77, '25.3733439', ' 51.5307713'),
(681, 77, '25.3840454', ' 51.5401268'),
(682, 78, '25.3153218', '51.3688088'),
(683, 78, '25.3238562', ' 51.3710403'),
(684, 78, '25.3263388', ' 51.3648605'),
(685, 78, '25.3406129', ' 51.3315582'),
(686, 78, '25.3564366', ' 51.2989426'),
(687, 78, '25.369156', ' 51.2570572'),
(688, 78, '25.384976', ' 51.2230682'),
(689, 78, '25.3769112', ' 51.2141418'),
(690, 78, '25.3604697', ' 51.2385178'),
(691, 78, '25.3489904', ' 51.2910461'),
(692, 78, '25.3285111', ' 51.3288116'),
(693, 78, '25.3153218', ' 51.3688088'),
(694, 79, '25.2075815', '51.576004'),
(695, 79, '25.1892531', ' 51.5873337'),
(696, 79, '25.1922044', ' 51.5969467'),
(697, 79, '25.1951558', ' 51.6048431'),
(698, 79, '25.2148811', ' 51.5954018'),
(699, 79, '25.2114643', ' 51.586647'),
(700, 79, '25.2075815', ' 51.576004'),
(701, 80, '25.2148811', '51.5954018'),
(702, 80, '25.1951558', ' 51.6048431'),
(703, 80, '25.1963984', ' 51.620636'),
(704, 80, '25.2173659', ' 51.6208935'),
(705, 80, '25.2148811', ' 51.5954018'),
(706, 81, '25.2521485', '51.4309502'),
(707, 81, '25.2249755', ' 51.4316368'),
(708, 81, '25.2444061', ' 51.4634553'),
(709, 81, '25.2520336', ' 51.4583276'),
(710, 81, '25.2610583', ' 51.4544008'),
(711, 81, '25.2557888', ' 51.4403576'),
(712, 81, '25.2521485', ' 51.4309502'),
(713, 82, '25.212085', '51.4417664'),
(714, 82, '25.1926705', ' 51.4554977'),
(715, 82, '25.206339', ' 51.4872551'),
(716, 82, '25.2120856', ' 51.4970398'),
(717, 82, '25.2221802', ' 51.4827919'),
(718, 82, '25.2214037', ' 51.4687157'),
(719, 82, '25.212085', ' 51.4417664'),
(720, 83, '25.4578466', '51.4694878'),
(721, 83, '25.4575349', ' 51.478157'),
(722, 83, '25.4603289', ' 51.4854505'),
(723, 83, '25.4674541', ' 51.4812469'),
(724, 83, '25.5058832', ' 51.4613342'),
(725, 83, '25.5275717', ' 51.4537811'),
(726, 83, '25.5498757', ' 51.4572144'),
(727, 83, '25.5864205', ' 51.4764404'),
(728, 83, '25.6260498', ' 51.4929199'),
(729, 83, '25.6291452', ' 51.4812469'),
(730, 83, '25.5913749', ' 51.4620209'),
(731, 83, '25.5467782', ' 51.4407349'),
(732, 83, '25.5139394', ' 51.4448547'),
(733, 83, '25.4804718', ' 51.4627075'),
(734, 83, '25.4578466', ' 51.4694878'),
(735, 84, '25.384976', '51.2230682'),
(736, 84, '25.4131985', ' 51.2350845'),
(737, 84, '25.4411044', ' 51.16745'),
(738, 84, '25.4026546', ' 51.1468506'),
(739, 84, '25.3173391', ' 51.1842728'),
(740, 84, '25.3015103', ' 51.2498474'),
(741, 84, '25.3167184', ' 51.2927628'),
(742, 84, '25.3489904', ' 51.2910461'),
(743, 84, '25.3604697', ' 51.2385178'),
(744, 84, '25.3769112', ' 51.2141418'),
(745, 84, '25.384976', ' 51.2230682'),
(746, 85, '25.3285111', '51.3288116'),
(747, 85, '25.3081835', ' 51.329155'),
(748, 85, '25.296544', ' 51.3866615'),
(749, 85, '25.3114048', ' 51.3894514'),
(750, 85, '25.3153218', ' 51.3688088'),
(751, 85, '25.3285111', ' 51.3288116'),
(752, 86, '25.2313035', '51.5760469'),
(753, 86, '25.218453', ' 51.5851021'),
(754, 86, '25.2114643', ' 51.586647'),
(755, 86, '25.2148811', ' 51.5954018'),
(756, 86, '25.2173659', ' 51.6208935'),
(757, 86, '25.2510617', ' 51.6374588'),
(758, 86, '25.2394168', ' 51.5911102'),
(759, 86, '25.2313035', ' 51.5760469'),
(760, 87, '25.1791557', '51.5169526'),
(761, 87, '25.186771', ' 51.5526562'),
(762, 87, '25.1892531', ' 51.5873337'),
(763, 87, '25.2075815', ' 51.576004'),
(764, 87, '25.2139127', ' 51.5726974'),
(765, 87, '25.2000486', ' 51.5287113'),
(766, 87, '25.1810199', ' 51.5027905'),
(767, 87, '25.1791557', ' 51.5169526'),
(768, 88, '25.1268683', '51.4454106'),
(769, 88, '25.1386025', ' 51.5171242'),
(770, 88, '25.1791557', ' 51.5169526'),
(771, 88, '25.1810199', ' 51.5027905'),
(772, 88, '25.1723201', ' 51.4697456'),
(773, 88, '25.1488586', ' 51.4345551'),
(774, 88, '25.1268683', ' 51.4454106'),
(775, 89, '25.385513593025458', '51.52587890625'),
(776, 89, '25.22660084866949', '51.620635986328125'),
(777, 89, '25.137120995993193', '51.3885498046875'),
(778, 89, '25.374967257874435', '51.20452880859375'),
(779, 89, '25.385513593025458', '51.52587890625');

-- --------------------------------------------------------

--
-- Table structure for table `provider_preference_head`
--

CREATE TABLE `provider_preference_head` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='This table holds area name  for preffered location';

--
-- Dumping data for table `provider_preference_head`
--

INSERT INTO `provider_preference_head` (`id`, `name`) VALUES
(6, 'Al Thumama East'),
(7, 'Old Airport'),
(8, 'Al Thumama West'),
(9, 'Nuaija'),
(10, 'Al Hilal East'),
(11, 'Al Hilal West'),
(12, 'Mushaireb'),
(13, 'Najma/Mansoura'),
(14, 'Al Muntazah'),
(15, 'Fireej Al Ali'),
(16, 'Al Asiri'),
(17, 'Almirqab Aljadeed (Sadd)'),
(18, 'Al Sadd'),
(19, 'Bin Omran'),
(20, 'Al Dafna'),
(21, 'Katara'),
(22, 'Pearl'),
(23, 'Lusail'),
(24, 'Onaiza North'),
(25, 'Madinat Khalifa South'),
(26, 'Madinat Khalifa North'),
(27, 'Onaiza'),
(28, 'South Al Markhiya'),
(29, 'Al Mamoura'),
(30, 'Abo Hamour'),
(31, 'Muaither North'),
(32, 'Al Markhiya'),
(33, 'Al Duhail'),
(34, 'Qatar University'),
(35, 'Duhail North'),
(36, 'North Atlantic'),
(37, 'Duhail East'),
(38, 'Old Industrial City'),
(39, 'New Industrial'),
(40, 'Muaither West'),
(41, 'Al Mirath'),
(42, 'Muaither'),
(43, 'Al Sailiya'),
(44, 'Abu Nakhla'),
(45, 'Muaither South'),
(46, 'Ain Khaled'),
(47, 'Al Waab'),
(48, 'Wadi Al Sail'),
(49, 'Aspire'),
(50, 'Education City'),
(51, 'Muraikh'),
(52, 'Al Luqta'),
(53, 'Al Gharafa'),
(54, 'Izghawa'),
(55, 'Umm Slal Imhammad'),
(56, 'Al Khaisa'),
(57, 'South Al Mamoura'),
(58, 'Abo Hamour Cemetry'),
(59, 'Karwa School'),
(60, 'South F Ring Road'),
(61, 'Umm Ghuwailina'),
(62, 'Mueseum Area'),
(63, 'Ras Abu Aboud'),
(64, 'Airport (Old)'),
(65, 'Hamad Int Airport'),
(66, 'Barwa Village'),
(67, 'Wakra North'),
(68, 'Wakra South'),
(69, 'Al Wukair'),
(70, 'Bani Hajar'),
(71, 'North Al Khaisa'),
(72, 'Umm Slal Ali'),
(73, 'Al Sakhama'),
(74, 'Messaid City'),
(75, 'Sealine'),
(76, 'Alkhor City'),
(77, 'Lagoona, Bay Area'),
(78, 'Dukhan Road'),
(79, 'Wakra Road'),
(80, 'Ras Abu Fontas Factory'),
(81, 'Aziziyah'),
(82, 'Ain Khaled South'),
(83, 'Alkhor Road'),
(84, 'Shahaniya'),
(85, 'Emiri Palace'),
(86, 'South Airport Desert'),
(87, 'South Thumama Desert'),
(88, 'South Industrial Desert'),
(89, 'qatar');

-- --------------------------------------------------------

--
-- Table structure for table `provider_preference_location`
--

CREATE TABLE `provider_preference_location` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `preference_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Preferred Location for Provider';

--
-- Dumping data for table `provider_preference_location`
--

INSERT INTO `provider_preference_location` (`id`, `provider_id`, `preference_id`) VALUES
(1, 21, 6),
(2, 21, 7),
(3, 21, 8),
(4, 21, 9),
(5, 21, 10),
(6, 21, 11),
(7, 21, 12),
(8, 21, 13),
(9, 21, 14),
(10, 21, 15),
(11, 21, 16),
(12, 21, 17),
(13, 21, 18),
(14, 21, 19),
(15, 21, 20),
(16, 21, 21),
(17, 21, 22),
(18, 21, 23),
(19, 21, 24),
(20, 21, 25),
(21, 21, 26),
(22, 21, 27),
(23, 21, 28),
(24, 21, 29),
(25, 21, 30),
(26, 21, 31),
(27, 21, 32),
(28, 21, 33),
(29, 21, 34),
(30, 21, 35),
(31, 21, 36),
(32, 21, 37),
(33, 21, 38),
(34, 21, 39),
(35, 21, 40),
(36, 21, 41),
(37, 21, 42),
(38, 21, 43),
(39, 21, 44),
(40, 21, 45),
(41, 21, 46),
(42, 21, 47),
(43, 21, 48),
(44, 21, 49),
(45, 21, 50),
(46, 21, 51),
(47, 21, 52),
(48, 21, 53),
(49, 21, 54),
(50, 21, 55),
(51, 21, 56),
(52, 21, 57),
(53, 21, 58),
(54, 21, 59),
(55, 21, 60),
(56, 21, 61),
(57, 21, 62),
(58, 21, 63),
(59, 21, 64),
(60, 21, 65),
(61, 21, 66),
(62, 21, 67),
(63, 21, 68),
(64, 21, 69),
(65, 21, 70),
(66, 21, 71),
(67, 21, 72),
(68, 21, 73),
(69, 21, 74),
(70, 21, 75),
(71, 21, 76),
(72, 21, 77),
(73, 21, 78),
(74, 21, 79),
(75, 21, 80),
(76, 21, 81),
(77, 21, 82),
(78, 21, 83),
(79, 21, 84),
(80, 21, 85),
(81, 21, 86),
(82, 21, 87),
(83, 21, 88),
(84, 1, 89),
(85, 2, 89),
(86, 3, 89),
(87, 4, 89),
(88, 5, 89),
(89, 6, 89),
(90, 7, 89);

-- --------------------------------------------------------

--
-- Table structure for table `provider_profile`
--

CREATE TABLE `provider_profile` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_details` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_size` int(11) DEFAULT NULL,
  `nationality` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_preference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `own_car` tinyint(1) DEFAULT NULL,
  `max_purchase_limit` float DEFAULT NULL,
  `current_credit_balance` float DEFAULT NULL,
  `max_due_amount` float DEFAULT NULL,
  `banner_img1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_img2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_img3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` int(11) DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internet_access` enum('mobile','wifi','mobile+wifi') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transport` enum('car','taxi','walk') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_offer` text COLLATE utf8mb4_unicode_ci,
  `year_of_experience` text COLLATE utf8mb4_unicode_ci,
  `live_in_qatar` text COLLATE utf8mb4_unicode_ci,
  `work_preference` int(11) DEFAULT NULL,
  `work_in_company` int(11) DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `admin_profile_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tools_use_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car_registration_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_license_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_profile`
--

INSERT INTO `provider_profile` (`id`, `provider_id`, `service_details`, `image`, `team_size`, `nationality`, `language_preference`, `own_car`, `max_purchase_limit`, `current_credit_balance`, `max_due_amount`, `banner_img1`, `banner_img2`, `banner_img3`, `date_of_birth`, `gender`, `internet_access`, `transport`, `your_offer`, `year_of_experience`, `live_in_qatar`, `work_preference`, `work_in_company`, `shop_name`, `admin_profile_image`, `tools_use_image`, `shop_image`, `cv_file`, `car_registration_image`, `driving_license_image`, `identity_image`) VALUES
(1, 1, 'excellent service', 'media/provider/17e9249c93ab8686c201847f9ddd399dea111a29.jpg', 4, 'QA', 'arbic', 1, 200, 98, 200, '', NULL, NULL, 2001, 'male', 'mobile', 'taxi', 'dfd sadsf', '2 years', '3 years', 1, 0, 'test', '', '', '', 'media/provider/wireframes1.docx', '', 'media/provider/wireframes2.docx', 'media/provider/wireframes3.docx'),
(2, 2, 'very good car service provider', NULL, 2, 'QA', NULL, 1, NULL, NULL, 200, NULL, NULL, NULL, 0, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(3, 8, 'A one service provider', NULL, 3, 'IN', NULL, 1, NULL, NULL, 200, NULL, NULL, NULL, 1981, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(4, 3, 'ddfdsafasd sadfsadf', 'media/provider/97d769f791b28749d2d2dcfd27e87a960300acb9.jpeg', 3, 'QA', NULL, 1, NULL, NULL, 200, NULL, NULL, 'media/provider/banners/95c4351e1482eb235b312838ad6f21de1560943f.jpg', 1976, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(5, 4, 'Excellent Service4', NULL, 4, 'QA', NULL, 1, NULL, NULL, 200, NULL, NULL, NULL, 1995, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(6, 5, 'Excellent Service5', NULL, 5, 'QA', NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, 1970, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(7, 6, 'Excellent Service6', NULL, 4, 'QA', NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, 1960, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(8, 7, 'Excellent Service7', NULL, 10, 'QA', NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, 1981, 'male', 'mobile', 'car', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(9, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, 1983, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 19, NULL, NULL, NULL, 'QA', NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 20, NULL, NULL, NULL, 'QA', NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 21, NULL, NULL, NULL, 'QA', NULL, NULL, NULL, NULL, 200, NULL, NULL, NULL, 15, 'male', '', 'car', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provider_rating`
--

CREATE TABLE `provider_rating` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `rate_value` float NOT NULL,
  `professional` float DEFAULT NULL,
  `friendly` float DEFAULT NULL,
  `communication` float DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_rating`
--

INSERT INTO `provider_rating` (`id`, `provider_id`, `customer_id`, `service_id`, `rate_value`, `professional`, `friendly`, `communication`, `date`, `comment`) VALUES
(1, 1, 1, 1, 5, 5, 3, 7, '2017-02-10 00:00:00', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `provider_sattle_history`
--

CREATE TABLE `provider_sattle_history` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `provider_schedule`
--

CREATE TABLE `provider_schedule` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `from_time` datetime NOT NULL,
  `to_time` datetime NOT NULL,
  `work_start_time` datetime DEFAULT NULL,
  `work_end_time` datetime DEFAULT NULL,
  `work_duration` int(11) DEFAULT NULL,
  `heading_time` int(11) DEFAULT NULL,
  `time_type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `job_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shorttime` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_id` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_schedule`
--

INSERT INTO `provider_schedule` (`id`, `provider_id`, `date`, `from_time`, `to_time`, `work_start_time`, `work_end_time`, `work_duration`, `heading_time`, `time_type`, `service_id`, `job_id`, `shorttime`, `free_id`, `duration`) VALUES
(1, 2, '2017-02-21', '2017-02-21 06:00:00', '2017-02-21 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(2, 2, '2017-02-22', '2017-02-22 06:00:00', '2017-02-22 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(3, 2, '2017-02-23', '2017-02-23 06:00:00', '2017-02-23 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(4, 2, '2017-02-24', '2017-02-24 06:00:00', '2017-02-24 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(5, 2, '2017-02-25', '2017-02-25 06:00:00', '2017-02-25 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(6, 2, '2017-02-26', '2017-02-26 06:00:00', '2017-02-26 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(7, 2, '2017-02-27', '2017-02-27 06:00:00', '2017-02-27 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(8, 5, '2017-02-21', '2017-02-21 06:00:00', '2017-02-21 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(9, 5, '2017-02-22', '2017-02-22 06:00:00', '2017-02-22 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(10, 5, '2017-02-23', '2017-02-23 06:00:00', '2017-02-23 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(11, 5, '2017-02-24', '2017-02-24 06:00:00', '2017-02-24 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(12, 5, '2017-02-25', '2017-02-25 06:00:00', '2017-02-25 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(13, 5, '2017-02-26', '2017-02-26 06:00:00', '2017-02-26 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(14, 5, '2017-02-27', '2017-02-27 06:00:00', '2017-02-27 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(15, 7, '2017-02-21', '2017-02-21 06:00:00', '2017-02-21 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(16, 7, '2017-02-22', '2017-02-22 06:00:00', '2017-02-22 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(17, 7, '2017-02-23', '2017-02-23 06:00:00', '2017-02-23 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(18, 7, '2017-02-24', '2017-02-24 06:00:00', '2017-02-24 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(19, 7, '2017-02-25', '2017-02-25 06:00:00', '2017-02-25 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(20, 7, '2017-02-26', '2017-02-26 06:00:00', '2017-02-26 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(21, 7, '2017-02-27', '2017-02-27 06:00:00', '2017-02-27 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(22, 8, '2017-02-21', '2017-02-21 06:00:00', '2017-02-21 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(23, 8, '2017-02-22', '2017-02-22 06:00:00', '2017-02-22 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(24, 8, '2017-02-23', '2017-02-23 06:00:00', '2017-02-23 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(25, 8, '2017-02-24', '2017-02-24 06:00:00', '2017-02-24 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(26, 8, '2017-02-25', '2017-02-25 06:00:00', '2017-02-25 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(27, 8, '2017-02-26', '2017-02-26 06:00:00', '2017-02-26 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL),
(28, 8, '2017-02-27', '2017-02-27 06:00:00', '2017-02-27 17:00:00', NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provider_time_history`
--

CREATE TABLE `provider_time_history` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `work_time` float DEFAULT NULL,
  `free_time` float DEFAULT NULL,
  `drive_time` float DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='This will keep work and free hours on dailiy basis';

--
-- Dumping data for table `provider_time_history`
--

INSERT INTO `provider_time_history` (`id`, `provider_id`, `service_id`, `work_time`, `free_time`, `drive_time`, `date`) VALUES
(1, 1, 2, NULL, NULL, NULL, '2017-02-03 08:12:36'),
(2, 1, 3, 120, NULL, NULL, '2017-02-03 09:06:24'),
(4, 1, 5, 120, NULL, NULL, '2017-02-03 10:33:34'),
(5, 1, 6, 120, NULL, NULL, '2017-02-03 10:38:10'),
(6, 1, 7, 120, NULL, NULL, '2017-02-03 10:59:31'),
(7, 1, 8, 120, NULL, NULL, '2017-02-03 13:12:49'),
(8, 1, 9, 120, NULL, NULL, '2017-02-03 13:20:47'),
(10, 1, 11, 120, NULL, NULL, '2017-02-03 13:28:53'),
(13, 1, 14, 120, NULL, NULL, '2017-02-06 13:08:31'),
(14, 1, 15, 120, NULL, NULL, '2017-02-06 13:10:31'),
(15, 1, 16, 2.17, NULL, NULL, '2017-02-07 08:21:31'),
(16, 1, 17, 120, NULL, NULL, '2017-02-07 08:23:37'),
(17, 1, 18, 120, NULL, NULL, '2017-02-07 08:24:21'),
(18, 1, 19, 120, NULL, NULL, '2017-02-07 08:25:24'),
(20, 1, 21, 120, NULL, NULL, '2017-02-07 13:34:10'),
(22, 1, 23, 120, NULL, NULL, '2017-02-08 05:50:03'),
(23, 1, 24, 120, NULL, NULL, '2017-02-08 05:55:06'),
(24, 1, 25, 120, NULL, NULL, '2017-02-08 10:20:34'),
(25, 1, 26, 120, NULL, NULL, '2017-02-08 10:37:01'),
(26, 1, 27, 120, NULL, NULL, '2017-02-09 08:04:22'),
(28, 1, 29, 120, NULL, NULL, '2017-02-09 13:00:08'),
(29, 1, 30, 120, NULL, NULL, '2017-02-09 13:01:27'),
(30, 1, 31, 120, NULL, NULL, '2017-02-17 05:41:45'),
(32, 1, 33, 120, NULL, NULL, '2017-02-17 06:15:15'),
(33, 1, 34, 120, NULL, NULL, '2017-02-17 06:26:35'),
(34, 1, 35, 120, NULL, NULL, '2017-02-17 06:29:41'),
(35, 1, 36, 120, NULL, NULL, '2017-02-17 09:45:47'),
(36, 1, 37, 120, NULL, NULL, '2017-02-17 10:18:28'),
(37, 1, 38, 120, NULL, NULL, '2017-02-17 11:51:34'),
(38, 1, 39, 120, NULL, NULL, '2017-02-17 11:54:01'),
(39, 1, 40, 120, NULL, NULL, '2017-02-17 14:28:49'),
(40, 1, 41, 120, NULL, NULL, '2017-02-20 07:20:04'),
(41, 1, 42, 120, NULL, NULL, '2017-02-20 07:20:54'),
(42, 1, 43, 120, NULL, NULL, '2017-02-20 07:21:53'),
(43, 1, 44, 120, NULL, NULL, '2017-02-20 07:22:35'),
(44, 1, 45, 120, NULL, NULL, '2017-02-20 07:23:48'),
(45, 1, 46, 120, NULL, NULL, '2017-02-21 05:51:05'),
(47, 1, 48, 120, NULL, NULL, '2017-02-21 06:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `provider_worktime`
--

CREATE TABLE `provider_worktime` (
  `id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `provider_timehistory_id` int(11) NOT NULL,
  `from_time` datetime DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('heading','working','online','jobreceived') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='This table will track work and online status of provider';

--
-- Dumping data for table `provider_worktime`
--

INSERT INTO `provider_worktime` (`id`, `provider_id`, `provider_timehistory_id`, `from_time`, `to_time`, `service_id`, `date`, `type`) VALUES
(2, 1, 2, '2017-02-03 12:06:24', '2017-02-03 14:06:24', 3, '2017-02-03 09:06:24', 'jobreceived'),
(4, 1, 4, '2017-02-03 13:33:34', '2017-02-03 15:33:34', 5, '2017-02-03 10:33:34', 'jobreceived'),
(5, 1, 5, '2017-02-03 13:38:10', '2017-02-03 15:38:10', 6, '2017-02-03 10:38:10', 'jobreceived'),
(6, 1, 6, '2017-02-03 13:59:31', '2017-02-03 15:59:31', 7, '2017-02-03 10:59:31', 'jobreceived'),
(7, 1, 7, '2017-02-03 16:12:49', '2017-02-03 18:12:49', 8, '2017-02-03 13:12:49', 'jobreceived'),
(8, 1, 8, '2017-02-03 16:20:47', '2017-02-03 18:20:47', 9, '2017-02-03 13:20:47', 'jobreceived'),
(10, 1, 10, '2017-02-03 16:28:53', '2017-02-03 18:28:53', 11, '2017-02-03 13:28:53', 'jobreceived'),
(13, 1, 13, '2017-02-06 16:08:31', '2017-02-06 18:08:31', 14, '2017-02-06 13:08:31', 'jobreceived'),
(14, 1, 14, '2017-02-06 16:10:31', '2017-02-06 18:10:31', 15, '2017-02-06 13:10:31', 'jobreceived'),
(15, 1, 15, '2017-02-07 11:21:31', '2017-02-07 13:21:31', 16, '2017-02-07 08:21:31', 'jobreceived'),
(16, 1, 16, '2017-02-07 11:23:37', '2017-02-07 13:23:37', 17, '2017-02-07 08:23:37', 'jobreceived'),
(17, 1, 17, '2017-02-07 11:24:21', '2017-02-07 13:24:21', 18, '2017-02-07 08:24:21', 'jobreceived'),
(18, 1, 18, '2017-02-07 11:25:24', '2017-02-07 13:25:24', 19, '2017-02-07 08:25:24', 'jobreceived'),
(20, 1, 20, '2017-02-07 16:34:10', '2017-02-07 18:34:10', 21, '2017-02-07 13:34:10', 'jobreceived'),
(22, 1, 22, '2017-02-08 08:50:03', '2017-02-08 10:50:03', 23, '2017-02-08 05:50:03', 'jobreceived'),
(23, 1, 23, '2017-02-08 08:55:06', '2017-02-08 10:55:06', 24, '2017-02-08 05:55:06', 'jobreceived'),
(24, 1, 24, '2017-02-08 13:20:34', '2017-02-08 15:20:34', 25, '2017-02-08 10:20:34', 'jobreceived'),
(25, 1, 25, '2017-02-08 13:37:01', '2017-02-08 15:37:01', 26, '2017-02-08 10:37:01', 'jobreceived'),
(26, 1, 15, '2017-02-08 13:57:17', '2017-02-08 14:07:17', 16, '2017-02-08 10:57:17', 'heading'),
(27, 1, 26, '2017-02-09 11:04:22', '2017-02-09 13:04:22', 27, '2017-02-09 08:04:22', 'jobreceived'),
(29, 1, 28, '2017-02-09 16:00:08', '2017-02-09 18:00:08', 29, '2017-02-09 13:00:08', 'jobreceived'),
(30, 1, 29, '2017-02-09 16:01:27', '2017-02-09 18:01:27', 30, '2017-02-09 13:01:27', 'jobreceived'),
(31, 1, 28, '2017-02-09 16:05:10', '2017-02-09 16:25:10', 29, '2017-02-09 13:05:10', 'heading'),
(32, 1, 30, '2017-02-17 08:41:45', '2017-02-17 10:41:45', 31, '2017-02-17 05:41:45', 'jobreceived'),
(34, 1, 32, '2017-02-17 09:15:15', '2017-02-17 11:15:15', 33, '2017-02-17 06:15:15', 'jobreceived'),
(35, 1, 33, '2017-02-17 09:26:35', '2017-02-17 11:26:35', 34, '2017-02-17 06:26:35', 'jobreceived'),
(36, 1, 34, '2017-02-17 09:29:41', '2017-02-17 11:29:41', 35, '2017-02-17 06:29:41', 'jobreceived'),
(37, 1, 35, '2017-02-17 12:45:47', '2017-02-17 14:45:47', 36, '2017-02-17 09:45:47', 'jobreceived'),
(38, 1, 36, '2017-02-17 13:18:28', '2017-02-17 15:18:28', 37, '2017-02-17 10:18:28', 'jobreceived'),
(39, 1, 37, '2017-02-17 14:51:34', '2017-02-17 16:51:34', 38, '2017-02-17 11:51:34', 'jobreceived'),
(40, 1, 38, '2017-02-17 14:54:01', '2017-02-17 16:54:01', 39, '2017-02-17 11:54:01', 'jobreceived'),
(41, 1, 39, '2017-02-17 17:28:49', '2017-02-17 19:28:49', 40, '2017-02-17 14:28:49', 'jobreceived'),
(42, 1, 40, '2017-02-20 10:20:04', '2017-02-20 12:20:04', 41, '2017-02-20 07:20:04', 'jobreceived'),
(43, 1, 41, '2017-02-20 10:20:54', '2017-02-20 12:20:54', 42, '2017-02-20 07:20:54', 'jobreceived'),
(44, 1, 42, '2017-02-20 10:21:53', '2017-02-20 12:21:53', 43, '2017-02-20 07:21:53', 'jobreceived'),
(45, 1, 43, '2017-02-20 10:22:35', '2017-02-20 12:22:35', 44, '2017-02-20 07:22:35', 'jobreceived'),
(46, 1, 44, '2017-02-20 10:23:48', '2017-02-20 12:23:48', 45, '2017-02-20 07:23:48', 'jobreceived'),
(47, 1, 45, '2017-02-21 08:51:05', '2017-02-21 10:51:05', 46, '2017-02-21 05:51:05', 'jobreceived'),
(49, 1, 47, '2017-02-21 09:12:55', '2017-02-21 11:12:55', 48, '2017-02-21 06:12:55', 'jobreceived');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL COMMENT 'service type id',
  `customer_id` int(11) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `booking_slot_id` int(11) DEFAULT NULL,
  `job_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci,
  `request_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preference_job_area` int(11) DEFAULT NULL,
  `from_time` datetime DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  `estimated_traveltime_heading` int(11) DEFAULT NULL,
  `total_traveltime` int(11) DEFAULT NULL,
  `estimated_traveltime_currentlocation` int(11) DEFAULT NULL,
  `nationality` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_size` int(4) DEFAULT NULL,
  `job_duration_set_by_provider` int(11) DEFAULT NULL COMMENT 'in minutes',
  `short_duration` int(5) NOT NULL DEFAULT '0' COMMENT 'If provider unable to do service due to time shortage',
  `provider_call` int(11) DEFAULT NULL,
  `expected_cost` float DEFAULT NULL,
  `provider_waitto_cancel` int(11) NOT NULL,
  `provider_waitto_cancel_time` timestamp NULL DEFAULT NULL,
  `service_status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `type_id`, `customer_id`, `provider_id`, `booking_slot_id`, `job_id`, `job_description`, `request_date`, `latitude`, `longitude`, `preference_job_area`, `from_time`, `to_time`, `estimated_traveltime_heading`, `total_traveltime`, `estimated_traveltime_currentlocation`, `nationality`, `address`, `team_size`, `job_duration_set_by_provider`, `short_duration`, `provider_call`, `expected_cost`, `provider_waitto_cancel`, `provider_waitto_cancel_time`, `service_status_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 19, '21731491', NULL, '2017-02-03 06:40:29', '25.32075293256326', '51.44815563774109', NULL, '2017-02-03 10:00:00', '2017-02-03 11:00:00', 10, NULL, 10, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(2, 1, 1, 1, 22, '28593120', NULL, '2017-02-03 08:12:35', '25.32075293256326', '51.44815563774109', NULL, '2017-02-03 12:00:00', '2017-02-03 13:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(3, 1, 1, 1, 27, '59756898', NULL, '2017-02-03 09:06:22', '25.32075293256326', '51.44815563774109', NULL, '2017-02-03 13:00:00', '2017-02-03 14:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(5, 1, 1, 1, 28, '80454695', NULL, '2017-02-03 10:33:32', '25.32075293256326', '51.44815563774109', NULL, '2017-02-03 17:10:00', '2017-02-03 18:10:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(6, 1, 1, 1, 29, '18534665', NULL, '2017-02-03 10:38:08', '25.32075293256326', '51.44815563774109', NULL, '2017-02-03 20:00:00', '2017-02-03 21:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(7, 1, 1, 1, 33, '41264254', NULL, '2017-02-03 10:59:29', '25.32075293256326', '51.44815563774109', NULL, '2017-02-03 19:00:00', '2017-02-03 20:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(8, 1, 1, 1, 35, '69741637', NULL, '2017-02-03 13:12:48', '25.32075293256326', '51.44815563774109', NULL, '2017-02-06 10:00:00', '2017-02-06 11:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(9, 1, 1, 1, 36, '26511882', NULL, '2017-02-03 13:20:44', '25.32075293256326', '51.44815563774109', NULL, '2017-02-06 09:00:00', '2017-02-06 10:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(11, 1, 1, 1, NULL, '54819269', NULL, '2017-02-03 13:28:52', '25.32075293256326', '51.44815563774109', NULL, '2017-02-06 13:00:00', '2017-02-06 14:00:00', NULL, NULL, NULL, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 60, 0, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(14, 1, 1, 1, 38, '55931620', NULL, '2017-02-06 13:08:30', '25.32075293256326', '51.44815563774109', NULL, '2017-02-06 16:30:00', '2017-02-06 17:30:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(15, 1, 1, 1, 39, '18072820', NULL, '2017-02-06 13:10:29', '25.32075293256326', '51.44815563774109', NULL, '2017-02-06 17:00:00', '2017-02-06 19:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(16, 1, 1, 1, 40, '51096055', NULL, '2017-02-07 08:21:30', '25.32075293256326', '51.44815563774109', NULL, '2017-02-08 08:30:00', '2017-02-08 09:30:00', 29, 21, 21, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(17, 1, 1, 1, 41, '17863824', NULL, '2017-02-07 08:23:35', '25.32075293256326', '51.44815563774109', NULL, '2017-02-08 09:00:00', '2017-02-08 12:30:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(18, 1, 1, 1, 42, '58727532', NULL, '2017-02-07 08:24:20', '25.32075293256326', '51.44815563774109', NULL, '2017-02-08 15:00:00', '2017-02-08 16:30:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(19, 1, 1, 1, 45, '54481326', NULL, '2017-02-07 08:25:23', '25.32075293256326', '51.44815563774109', NULL, '2017-02-08 10:00:00', '2017-02-08 13:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(21, 1, 1, 1, 47, '55350395', NULL, '2017-02-07 13:34:09', '25.32075293256326', '51.44815563774109', NULL, '2017-02-10 06:00:00', '2017-02-10 06:30:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(23, 1, 1, 1, 48, '86385815', NULL, '2017-02-08 05:50:02', '25.32075293256326', '51.44815563774109', NULL, '2017-02-09 08:30:00', '2017-02-09 09:30:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(24, 1, 1, 1, 49, '72767865', NULL, '2017-02-08 05:55:05', '25.32075293256326', '51.44815563774109', NULL, '2017-02-09 09:25:00', '2017-02-09 10:25:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(25, 1, 1, 1, 51, '54631281', NULL, '2017-02-08 10:20:33', '25.32075293256326', '51.44815563774109', NULL, '2017-02-10 09:00:00', '2017-02-10 10:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(26, 1, 1, 1, 52, '76047525', NULL, '2017-02-08 10:37:00', '25.32075293256326', '51.44815563774109', NULL, '2017-02-10 10:10:00', '2017-02-10 11:10:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 30, 0, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(27, 1, 1, 1, NULL, '95265779', NULL, '2017-02-09 08:04:20', '25.32075293256326', '51.44815563774109', NULL, '2017-02-10 10:10:00', '2017-02-10 11:10:00', NULL, NULL, NULL, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 12, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(29, 1, 1, 1, NULL, '71260005', NULL, '2017-02-09 13:00:07', '25.32075293256326', '51.44815563774109', NULL, '2017-02-09 16:10:00', '2017-02-09 17:10:00', 29, 0, 0, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(30, 1, 1, 1, NULL, '82085020', NULL, '2017-02-09 13:01:25', '25.32075293256326', '51.44815563774109', NULL, '2017-02-09 17:00:00', '2017-02-09 18:00:00', 29, NULL, 29, 'QA', 'Al Gharrafa St, Ar-Rayyan, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-17 09:45:09'),
(31, 1, 1, 1, 56, '19579874', NULL, '2017-02-17 05:41:44', '25.260059', '51.467428', 89, '2017-02-17 12:00:00', '2017-02-17 13:00:00', 22, NULL, 22, 'QA', 'Al Sidr St, Doha, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-20 11:24:31'),
(33, 1, 1, 1, 61, '72909874', NULL, '2017-02-17 06:15:14', '25.260059', '51.567428', 89, '2017-02-17 17:00:00', '2017-02-17 18:00:00', 31, NULL, 31, 'QA', 'Al Sidr St, Doha, Qatar', NULL, 53, 1, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-20 11:24:31'),
(34, 1, 1, 1, 62, '51912245', NULL, '2017-02-17 06:26:33', '25.2881144', '51.548332', 89, '2017-02-17 18:00:00', '2017-02-17 19:00:00', 20, NULL, 29, 'QA', 'Museum Park St, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-20 11:24:31'),
(35, 1, 1, 1, 60, '35977913', NULL, '2017-02-17 06:29:40', '25.2881144', '51.548332', 89, '2017-02-17 21:00:00', '2017-02-17 22:00:00', 24, NULL, 29, 'QA', 'Museum Park St, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 09:45:09', '2017-02-20 11:24:31'),
(36, 1, 1, 1, NULL, '27241217', NULL, '2017-02-17 09:45:45', '25.2881144', '51.548332', NULL, '2017-02-17 20:00:00', '2017-02-17 21:00:00', NULL, NULL, NULL, 'QA', 'Museum Park St, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 2, '2017-02-17 09:45:47', '2017-02-17 09:45:47'),
(37, 1, 1, 1, NULL, '84491277', NULL, '2017-02-17 10:18:27', '25.2881144', '51.548332', 89, '2017-02-20 06:00:00', '2017-02-20 07:00:00', 24, NULL, 29, 'QA', 'Museum Park St, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 2, '2017-02-17 10:18:28', '2017-02-17 10:19:30'),
(38, 1, 1, 1, 70, '56117106', NULL, '2017-02-17 11:51:33', '25.2881144', '51.548332', 89, '2017-02-19 07:00:00', '2017-02-19 08:00:00', 24, NULL, 29, 'QA', 'Museum Park St, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 11:51:34', '2017-02-20 11:24:31'),
(39, 1, 1, 1, 73, '57537460', NULL, '2017-02-17 11:54:00', '25.2181144', '51.548332', 89, '2017-02-19 07:00:00', '2017-02-19 09:00:00', 35, NULL, 36, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 11:54:01', '2017-02-20 11:24:31'),
(40, 1, 1, 1, 72, '50125300', NULL, '2017-02-17 14:28:48', '25.2181144', '51.598332', 89, '2017-02-19 09:00:00', '2017-02-19 11:00:00', 37, NULL, 43, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-17 14:28:49', '2017-02-20 11:24:31'),
(41, 1, 1, 1, 74, '44066341', NULL, '2017-02-20 07:20:03', '25.2181144', '51.598332', 89, '2017-02-21 13:00:00', '2017-02-21 14:00:00', 40, NULL, 44, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 4, '2017-02-20 07:20:03', '2017-02-20 07:20:48'),
(42, 1, 1, 1, NULL, '51079618', NULL, '2017-02-20 07:20:53', '25.2181144', '51.598332', 89, '2017-02-21 16:00:00', '2017-02-21 17:00:00', 40, NULL, 44, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-20 07:20:54', '2017-02-20 09:42:10'),
(43, 1, 1, 1, NULL, '74031147', NULL, '2017-02-20 07:21:52', '25.3181144', '51.598332', 0, '2017-02-21 16:00:00', '2017-02-21 18:30:00', 33, NULL, 42, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 8, '2017-02-20 07:21:53', '2017-02-20 09:53:07'),
(44, 1, 1, 1, 77, '43781970', NULL, '2017-02-20 07:22:34', '25.2981144', '51.598332', 89, '2017-02-21 18:50:00', '2017-02-21 19:50:00', 10, NULL, 44, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 4, '2017-02-20 07:22:35', '2017-02-20 07:22:45'),
(45, 1, 1, 1, 78, '23840361', NULL, '2017-02-20 07:23:47', '25.2881144', '51.598332', 89, '2017-02-21 20:30:00', '2017-02-21 21:30:00', 40, NULL, 44, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 4, '2017-02-20 07:23:48', '2017-02-20 07:23:54'),
(46, 1, 1, 1, 79, '17128157', NULL, '2017-02-21 05:51:04', '25.2181144', '51.598332', 89, '2017-02-22 07:30:00', '2017-02-22 08:30:00', 36, NULL, 44, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 4, '2017-02-21 05:51:04', '2017-02-21 05:51:47'),
(48, 1, 1, 1, 80, '29471012', NULL, '2017-02-21 06:12:53', '25.2181144', '51.598332', 89, '2017-02-22 06:30:00', '2017-02-22 07:30:00', 54, NULL, 44, 'QA', 'Unnamed Road, Doha, Qatar', NULL, 53, 0, NULL, 100, 0, NULL, 4, '2017-02-21 06:12:54', '2017-02-21 06:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `service_car_details`
--

CREATE TABLE `service_car_details` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `carwash_type` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `number_of_cars` int(11) DEFAULT NULL,
  `car_no` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `option_details_id` int(11) NOT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_car_details`
--

INSERT INTO `service_car_details` (`id`, `service_id`, `carwash_type`, `number_of_cars`, `car_no`, `option_details_id`, `price`) VALUES
(1, 1, 'Suv1', 1, NULL, 1, NULL),
(2, 1, 'Suv1', 1, NULL, 5, NULL),
(3, 1, 'Suv2', 1, NULL, 1, NULL),
(4, 1, 'Suv2', 1, NULL, 4, NULL),
(5, 1, 'Sedan1', 1, NULL, 2, NULL),
(6, 1, 'Sedan1', 1, NULL, 6, NULL),
(7, 2, 'Suv1', 1, NULL, 1, NULL),
(8, 2, 'Suv1', 1, NULL, 5, NULL),
(9, 2, 'Suv2', 1, NULL, 1, NULL),
(10, 2, 'Suv2', 1, NULL, 4, NULL),
(11, 2, 'Sedan1', 1, NULL, 2, NULL),
(12, 2, 'Sedan1', 1, NULL, 6, NULL),
(13, 3, 'Suv1', 1, NULL, 1, NULL),
(14, 3, 'Suv1', 1, NULL, 5, NULL),
(15, 3, 'Suv2', 1, NULL, 1, NULL),
(16, 3, 'Suv2', 1, NULL, 4, NULL),
(17, 3, 'Sedan1', 1, NULL, 2, NULL),
(18, 3, 'Sedan1', 1, NULL, 6, NULL),
(25, 5, 'Suv1', 1, NULL, 1, NULL),
(26, 5, 'Suv1', 1, NULL, 5, NULL),
(27, 5, 'Suv2', 1, NULL, 1, NULL),
(28, 5, 'Suv2', 1, NULL, 4, NULL),
(29, 5, 'Sedan1', 1, NULL, 2, NULL),
(30, 5, 'Sedan1', 1, NULL, 6, NULL),
(31, 6, 'Suv1', 1, NULL, 1, NULL),
(32, 6, 'Suv1', 1, NULL, 5, NULL),
(33, 6, 'Suv2', 1, NULL, 1, NULL),
(34, 6, 'Suv2', 1, NULL, 4, NULL),
(35, 6, 'Sedan1', 1, NULL, 2, NULL),
(36, 6, 'Sedan1', 1, NULL, 6, NULL),
(37, 7, 'Suv1', 1, NULL, 1, NULL),
(38, 7, 'Suv1', 1, NULL, 5, NULL),
(39, 7, 'Suv2', 1, NULL, 1, NULL),
(40, 7, 'Suv2', 1, NULL, 4, NULL),
(41, 7, 'Sedan1', 1, NULL, 2, NULL),
(42, 7, 'Sedan1', 1, NULL, 6, NULL),
(43, 8, 'Suv1', 1, NULL, 1, NULL),
(44, 8, 'Suv1', 1, NULL, 5, NULL),
(45, 8, 'Suv2', 1, NULL, 1, NULL),
(46, 8, 'Suv2', 1, NULL, 4, NULL),
(47, 8, 'Sedan1', 1, NULL, 2, NULL),
(48, 8, 'Sedan1', 1, NULL, 6, NULL),
(49, 9, 'Suv1', 1, NULL, 1, NULL),
(50, 9, 'Suv1', 1, NULL, 5, NULL),
(51, 9, 'Suv2', 1, NULL, 1, NULL),
(52, 9, 'Suv2', 1, NULL, 4, NULL),
(53, 9, 'Sedan1', 1, NULL, 2, NULL),
(54, 9, 'Sedan1', 1, NULL, 6, NULL),
(61, 11, 'Suv1', 1, NULL, 1, NULL),
(62, 11, 'Suv1', 1, NULL, 5, NULL),
(63, 11, 'Suv2', 1, NULL, 1, NULL),
(64, 11, 'Suv2', 1, NULL, 4, NULL),
(65, 11, 'Sedan1', 1, NULL, 2, NULL),
(66, 11, 'Sedan1', 1, NULL, 6, NULL),
(79, 14, 'Suv1', 1, NULL, 1, NULL),
(80, 14, 'Suv1', 1, NULL, 5, NULL),
(81, 14, 'Suv2', 1, NULL, 1, NULL),
(82, 14, 'Suv2', 1, NULL, 4, NULL),
(83, 14, 'Sedan1', 1, NULL, 2, NULL),
(84, 14, 'Sedan1', 1, NULL, 6, NULL),
(85, 15, 'Suv1', 1, NULL, 1, NULL),
(86, 15, 'Suv1', 1, NULL, 5, NULL),
(87, 15, 'Suv2', 1, NULL, 1, NULL),
(88, 15, 'Suv2', 1, NULL, 4, NULL),
(89, 15, 'Sedan1', 1, NULL, 2, NULL),
(90, 15, 'Sedan1', 1, NULL, 6, NULL),
(91, 16, 'Suv1', 1, NULL, 1, NULL),
(92, 16, 'Suv1', 1, NULL, 5, NULL),
(93, 16, 'Suv2', 1, NULL, 1, NULL),
(94, 16, 'Suv2', 1, NULL, 4, NULL),
(95, 16, 'Sedan1', 1, NULL, 2, NULL),
(96, 16, 'Sedan1', 1, NULL, 6, NULL),
(97, 17, 'Suv1', 1, NULL, 1, NULL),
(98, 17, 'Suv1', 1, NULL, 5, NULL),
(99, 17, 'Suv2', 1, NULL, 1, NULL),
(100, 17, 'Suv2', 1, NULL, 4, NULL),
(101, 17, 'Sedan1', 1, NULL, 2, NULL),
(102, 17, 'Sedan1', 1, NULL, 6, NULL),
(103, 18, 'Suv1', 1, NULL, 1, NULL),
(104, 18, 'Suv1', 1, NULL, 5, NULL),
(105, 18, 'Suv2', 1, NULL, 1, NULL),
(106, 18, 'Suv2', 1, NULL, 4, NULL),
(107, 18, 'Sedan1', 1, NULL, 2, NULL),
(108, 18, 'Sedan1', 1, NULL, 6, NULL),
(109, 19, 'Suv1', 1, NULL, 1, NULL),
(110, 19, 'Suv1', 1, NULL, 5, NULL),
(111, 19, 'Suv2', 1, NULL, 1, NULL),
(112, 19, 'Suv2', 1, NULL, 4, NULL),
(113, 19, 'Sedan1', 1, NULL, 2, NULL),
(114, 19, 'Sedan1', 1, NULL, 6, NULL),
(121, 21, 'Suv1', 1, NULL, 1, NULL),
(122, 21, 'Suv1', 1, NULL, 5, NULL),
(123, 21, 'Suv2', 1, NULL, 1, NULL),
(124, 21, 'Suv2', 1, NULL, 4, NULL),
(125, 21, 'Sedan1', 1, NULL, 2, NULL),
(126, 21, 'Sedan1', 1, NULL, 6, NULL),
(127, 23, 'Suv1', 1, NULL, 1, NULL),
(128, 23, 'Suv1', 1, NULL, 5, NULL),
(129, 23, 'Suv2', 1, NULL, 1, NULL),
(130, 23, 'Suv2', 1, NULL, 4, NULL),
(131, 23, 'Sedan1', 1, NULL, 2, NULL),
(132, 23, 'Sedan1', 1, NULL, 6, NULL),
(133, 24, 'Suv1', 1, NULL, 1, NULL),
(134, 24, 'Suv1', 1, NULL, 5, NULL),
(135, 24, 'Suv2', 1, NULL, 1, NULL),
(136, 24, 'Suv2', 1, NULL, 4, NULL),
(137, 24, 'Sedan1', 1, NULL, 2, NULL),
(138, 24, 'Sedan1', 1, NULL, 6, NULL),
(139, 25, 'Suv1', 1, NULL, 1, NULL),
(140, 25, 'Suv1', 1, NULL, 5, NULL),
(141, 25, 'Suv2', 1, NULL, 1, NULL),
(142, 25, 'Suv2', 1, NULL, 4, NULL),
(143, 25, 'Sedan1', 1, NULL, 2, NULL),
(144, 25, 'Sedan1', 1, NULL, 6, NULL),
(145, 26, 'Suv1', 1, NULL, 1, NULL),
(146, 26, 'Suv1', 1, NULL, 5, NULL),
(147, 26, 'Suv2', 1, NULL, 1, NULL),
(148, 26, 'Suv2', 1, NULL, 4, NULL),
(149, 26, 'Sedan1', 1, NULL, 2, NULL),
(150, 26, 'Sedan1', 1, NULL, 6, NULL),
(151, 27, 'Suv1', 1, NULL, 1, NULL),
(152, 27, 'Suv1', 1, NULL, 5, NULL),
(153, 27, 'Suv2', 1, NULL, 1, NULL),
(154, 27, 'Suv2', 1, NULL, 4, NULL),
(155, 27, 'Sedan1', 1, NULL, 2, NULL),
(156, 27, 'Sedan1', 1, NULL, 6, NULL),
(163, 29, 'Suv1', 1, NULL, 1, NULL),
(164, 29, 'Suv1', 1, NULL, 5, NULL),
(165, 29, 'Suv2', 1, NULL, 1, NULL),
(166, 29, 'Suv2', 1, NULL, 4, NULL),
(167, 29, 'Sedan1', 1, NULL, 2, NULL),
(168, 29, 'Sedan1', 1, NULL, 6, NULL),
(169, 30, 'Suv1', 1, NULL, 1, NULL),
(170, 30, 'Suv1', 1, NULL, 5, NULL),
(171, 30, 'Suv2', 1, NULL, 1, NULL),
(172, 30, 'Suv2', 1, NULL, 4, NULL),
(173, 30, 'Sedan1', 1, NULL, 2, NULL),
(174, 30, 'Sedan1', 1, NULL, 6, NULL),
(175, 31, 'Suv1', 1, NULL, 1, NULL),
(176, 31, 'Suv1', 1, NULL, 5, NULL),
(177, 31, 'Suv2', 1, NULL, 1, NULL),
(178, 31, 'Suv2', 1, NULL, 4, NULL),
(179, 31, 'Sedan1', 1, NULL, 2, NULL),
(180, 31, 'Sedan1', 1, NULL, 6, NULL),
(187, 33, 'Suv1', 1, NULL, 1, NULL),
(188, 33, 'Suv1', 1, NULL, 5, NULL),
(189, 33, 'Suv2', 1, NULL, 1, NULL),
(190, 33, 'Suv2', 1, NULL, 4, NULL),
(191, 33, 'Sedan1', 1, NULL, 2, NULL),
(192, 33, 'Sedan1', 1, NULL, 6, NULL),
(193, 34, 'Suv1', 1, NULL, 1, NULL),
(194, 34, 'Suv1', 1, NULL, 5, NULL),
(195, 34, 'Suv2', 1, NULL, 1, NULL),
(196, 34, 'Suv2', 1, NULL, 4, NULL),
(197, 34, 'Sedan1', 1, NULL, 2, NULL),
(198, 34, 'Sedan1', 1, NULL, 6, NULL),
(199, 35, 'Suv1', 1, NULL, 1, NULL),
(200, 35, 'Suv1', 1, NULL, 5, NULL),
(201, 35, 'Suv2', 1, NULL, 1, NULL),
(202, 35, 'Suv2', 1, NULL, 4, NULL),
(203, 35, 'Sedan1', 1, NULL, 2, NULL),
(204, 35, 'Sedan1', 1, NULL, 6, NULL),
(205, 36, 'Suv1', 1, NULL, 1, NULL),
(206, 36, 'Suv1', 1, NULL, 5, NULL),
(207, 36, 'Suv2', 1, NULL, 1, NULL),
(208, 36, 'Suv2', 1, NULL, 4, NULL),
(209, 36, 'Sedan1', 1, NULL, 2, NULL),
(210, 36, 'Sedan1', 1, NULL, 6, NULL),
(211, 37, 'Suv1', 1, NULL, 1, NULL),
(212, 37, 'Suv1', 1, NULL, 5, NULL),
(213, 37, 'Suv2', 1, NULL, 1, NULL),
(214, 37, 'Suv2', 1, NULL, 4, NULL),
(215, 37, 'Sedan1', 1, NULL, 2, NULL),
(216, 37, 'Sedan1', 1, NULL, 6, NULL),
(217, 38, 'Suv1', 1, NULL, 1, NULL),
(218, 38, 'Suv1', 1, NULL, 5, NULL),
(219, 38, 'Suv2', 1, NULL, 1, NULL),
(220, 38, 'Suv2', 1, NULL, 4, NULL),
(221, 38, 'Sedan1', 1, NULL, 2, NULL),
(222, 38, 'Sedan1', 1, NULL, 6, NULL),
(223, 39, 'Suv1', 1, NULL, 1, NULL),
(224, 39, 'Suv1', 1, NULL, 5, NULL),
(225, 39, 'Suv2', 1, NULL, 1, NULL),
(226, 39, 'Suv2', 1, NULL, 4, NULL),
(227, 39, 'Sedan1', 1, NULL, 2, NULL),
(228, 39, 'Sedan1', 1, NULL, 6, NULL),
(229, 40, 'Suv1', 1, NULL, 1, NULL),
(230, 40, 'Suv1', 1, NULL, 5, NULL),
(231, 40, 'Suv2', 1, NULL, 1, NULL),
(232, 40, 'Suv2', 1, NULL, 4, NULL),
(233, 40, 'Sedan1', 1, NULL, 2, NULL),
(234, 40, 'Sedan1', 1, NULL, 6, NULL),
(235, 41, 'Suv1', 1, NULL, 1, NULL),
(236, 41, 'Suv1', 1, NULL, 5, NULL),
(237, 41, 'Suv2', 1, NULL, 1, NULL),
(238, 41, 'Suv2', 1, NULL, 4, NULL),
(239, 41, 'Sedan1', 1, NULL, 2, NULL),
(240, 41, 'Sedan1', 1, NULL, 6, NULL),
(241, 42, 'Suv1', 1, NULL, 1, NULL),
(242, 42, 'Suv1', 1, NULL, 5, NULL),
(243, 42, 'Suv2', 1, NULL, 1, NULL),
(244, 42, 'Suv2', 1, NULL, 4, NULL),
(245, 42, 'Sedan1', 1, NULL, 2, NULL),
(246, 42, 'Sedan1', 1, NULL, 6, NULL),
(247, 43, 'Suv1', 1, NULL, 1, NULL),
(248, 43, 'Suv1', 1, NULL, 5, NULL),
(249, 43, 'Suv2', 1, NULL, 1, NULL),
(250, 43, 'Suv2', 1, NULL, 4, NULL),
(251, 43, 'Sedan1', 1, NULL, 2, NULL),
(252, 43, 'Sedan1', 1, NULL, 6, NULL),
(253, 44, 'Suv1', 1, NULL, 1, NULL),
(254, 44, 'Suv1', 1, NULL, 5, NULL),
(255, 44, 'Suv2', 1, NULL, 1, NULL),
(256, 44, 'Suv2', 1, NULL, 4, NULL),
(257, 44, 'Sedan1', 1, NULL, 2, NULL),
(258, 44, 'Sedan1', 1, NULL, 6, NULL),
(259, 45, 'Suv1', 1, NULL, 1, NULL),
(260, 45, 'Suv1', 1, NULL, 5, NULL),
(261, 45, 'Suv2', 1, NULL, 1, NULL),
(262, 45, 'Suv2', 1, NULL, 4, NULL),
(263, 45, 'Sedan1', 1, NULL, 2, NULL),
(264, 45, 'Sedan1', 1, NULL, 6, NULL),
(265, 46, 'Suv1', 1, NULL, 1, NULL),
(266, 46, 'Suv1', 1, NULL, 5, NULL),
(267, 46, 'Suv2', 1, NULL, 1, NULL),
(268, 46, 'Suv2', 1, NULL, 4, NULL),
(269, 46, 'Sedan1', 1, NULL, 2, NULL),
(270, 46, 'Sedan1', 1, NULL, 6, NULL),
(277, 48, 'Suv1', 1, NULL, 1, NULL),
(278, 48, 'Suv1', 1, NULL, 5, NULL),
(279, 48, 'Suv2', 1, NULL, 1, NULL),
(280, 48, 'Suv2', 1, NULL, 4, NULL),
(281, 48, 'Sedan1', 1, NULL, 2, NULL),
(282, 48, 'Sedan1', 1, NULL, 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_image`
--

CREATE TABLE `service_image` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `service_option`
--

CREATE TABLE `service_option` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_option`
--

INSERT INTO `service_option` (`id`, `type_id`, `name`) VALUES
(1, 1, 'Suv'),
(2, 1, 'Sedan');

-- --------------------------------------------------------

--
-- Table structure for table `service_option_details`
--

CREATE TABLE `service_option_details` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `service_option_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` float NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_option_details`
--

INSERT INTO `service_option_details` (`id`, `type_id`, `service_option_id`, `name`, `cost`, `duration`) VALUES
(1, 1, 1, 'Pressure & Soap Wash', 50, 13),
(2, 1, 2, 'Pressure & Soap Wash', 35, 9),
(4, 1, 1, 'Interior Vacuum & Cleaning', 23, 8),
(5, 1, 1, 'Tyre Shining', 7, 4),
(6, 1, 2, 'Interior Vacuum & Cleaning', 18, 6),
(7, 1, 2, 'Tyre Shining', 17, 4);

-- --------------------------------------------------------

--
-- Table structure for table `service_settings`
--

CREATE TABLE `service_settings` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_settings`
--

INSERT INTO `service_settings` (`id`, `type_id`, `code`, `value`, `description`) VALUES
(1, 1, 'payment_commission_percent', '5', NULL),
(3, 1, 'arrival_duration', '40', NULL),
(4, 1, 'buffer_duration', '10', NULL),
(5, 1, 'provider_display_limit', '4', NULL),
(6, 1, 'provider_max_due_amount', '200', NULL),
(7, 1, 'allowed_start_time', '06:00:00', NULL),
(8, 1, 'allowed_end_time', '17:00:00', NULL),
(9, 1, 'customer_no_penalty_duration', '5', NULL),
(10, 1, 'customer_moderate_penalty_duration', '10', NULL),
(11, 1, 'customer_higher_penalty_duration', '20', NULL),
(12, 1, 'customer_moderate_penalty_amount', '10', NULL),
(13, 1, 'customer_higher_penalty_amount', '20', NULL),
(14, 1, 'customer_max_cancel_duration', '15', NULL),
(15, 1, 'customer_max_cancel_unit', '5', NULL),
(16, 1, 'provider_no_penalty_duration', '10', NULL),
(17, 1, 'provider_moderate_penalty_duration', '20', NULL),
(18, 1, 'provider_higher_penalty_duration', '10', NULL),
(19, 1, 'provider_moderate_penalty_amount', '13', NULL),
(20, 1, 'provider_max_cancel_unit', '3', NULL),
(21, 1, 'provider_higher_penalty_amount', '10', NULL),
(22, 1, 'customer_min_cancel_duration', '10', NULL),
(23, 1, 'provider_min_cancel_duration', '25', NULL),
(24, 1, 'notice_text', 'Kindly note that delivery and setup charges are 18 QAR', NULL),
(25, 1, 'display_notice', '1', NULL),
(26, 1, 'splash_text', 'Welcome to qber', NULL),
(27, 1, 'fixed_cost', '18', NULL),
(28, 1, 'min_cost', '50', NULL),
(29, 1, 'send_sms', '1', NULL),
(30, 1, 'min_work_hour', '30', NULL),
(31, 1, 'provider_wait_duration', '15', 'Provider Wait Duration(in seconds)'),
(32, 1, 'provider_wait_autocancel_duration', '3', 'Provider Confirmation Duration (in minutes)'),
(33, 1, 'radius_range', '30', NULL),
(34, 1, 'per_minute_sms', '1', NULL),
(35, 1, 'per_hour_sms', '5', NULL),
(36, 1, 'priority_factor_static_A', '10', NULL),
(37, 1, 'priority_factor_static_B', '70', NULL),
(38, 1, 'location_factor_static_A', '4.3', NULL),
(39, 1, 'location_factor_static_B', '0.09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_status`
--

CREATE TABLE `service_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'status name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_status`
--

INSERT INTO `service_status` (`id`, `name`) VALUES
(1, 'requestreceived'),
(2, 'rejected'),
(3, 'requested'),
(4, 'confirmed'),
(5, 'working'),
(6, 'completed'),
(8, 'cancelled after confirming'),
(9, 'heading'),
(10, 'cancelled after heading'),
(11, 'cancelled after job start'),
(12, 'Ignored');

-- --------------------------------------------------------

--
-- Table structure for table `service_types`
--

CREATE TABLE `service_types` (
  `id` int(5) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_types`
--

INSERT INTO `service_types` (`id`, `name`, `image`, `display_text`, `is_active`) VALUES
(1, 'Car Wash', 'media/service/f1d5d231cfea074f1d732b86ea9870e6bf6af224.png', 'welcome1', 1),
(2, 'Roadside Assistance', 'media/service/5ddcf04288a07d86d8ae1b059ac9abcba1946f7a.png', 'welcome2', 1),
(3, 'Pick n\' Deliver', 'media/service/95ab4dfcdde0e4e06545478154a4a3462ef2c6a6.png', 'welcome', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_log`
--

CREATE TABLE `sms_log` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_log`
--

INSERT INTO `sms_log` (`id`, `phone`, `sms_content`, `send_date`, `ip`) VALUES
(6, '9163063153', 'abcd', '2017-02-10 10:18:04', '127.0.0.1'),
(7, '9163063153', 'abcd', '2017-02-10 10:22:49', '127.0.0.1'),
(8, '9163063153', 'abcd', '2017-02-10 10:25:09', '127.0.0.1'),
(9, '9163063153', 'abcd', '2017-02-10 10:38:00', '127.0.0.1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_role_access`
--
ALTER TABLE `admin_role_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_status_id` (`service_status_id`),
  ADD KEY `booking_slot_id` (`booking_slot_id`);

--
-- Indexes for table `cancel_reason`
--
ALTER TABLE `cancel_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `company_providers`
--
ALTER TABLE `company_providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `customer_cancel_history`
--
ALTER TABLE `customer_cancel_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `reason_id` (`reason_id`);

--
-- Indexes for table `customer_location_history`
--
ALTER TABLE `customer_location_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `customer_message`
--
ALTER TABLE `customer_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`customer_id`);

--
-- Indexes for table `customer_notification`
--
ALTER TABLE `customer_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `customer_profile`
--
ALTER TABLE `customer_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `customer_rating`
--
ALTER TABLE `customer_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `dispute`
--
ALTER TABLE `dispute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `provider_balance_history`
--
ALTER TABLE `provider_balance_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `provider_booked_timeslot`
--
ALTER TABLE `provider_booked_timeslot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `booked_customer_id` (`booked_customer_id`);

--
-- Indexes for table `provider_cancel_history`
--
ALTER TABLE `provider_cancel_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `reason_id` (`reason_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id_2` (`service_id`),
  ADD KEY `reason_id_2` (`reason_id`);

--
-- Indexes for table `provider_company_account`
--
ALTER TABLE `provider_company_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `provider_id_2` (`provider_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `provider_free_timeslot`
--
ALTER TABLE `provider_free_timeslot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `provider_free_timeslot_history`
--
ALTER TABLE `provider_free_timeslot_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `provider_message`
--
ALTER TABLE `provider_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `provider_notification`
--
ALTER TABLE `provider_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `provider_perference_polygon`
--
ALTER TABLE `provider_perference_polygon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `preference_id` (`preference_id`);

--
-- Indexes for table `provider_preference_head`
--
ALTER TABLE `provider_preference_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_preference_location`
--
ALTER TABLE `provider_preference_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `preference_id` (`preference_id`);

--
-- Indexes for table `provider_profile`
--
ALTER TABLE `provider_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `provider_rating`
--
ALTER TABLE `provider_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `provider_sattle_history`
--
ALTER TABLE `provider_sattle_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_schedule`
--
ALTER TABLE `provider_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_time_history`
--
ALTER TABLE `provider_time_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `provider_worktime`
--
ALTER TABLE `provider_worktime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_timehistory_id` (`provider_timehistory_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jobid` (`job_id`),
  ADD KEY `id` (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `booking_slot_id` (`booking_slot_id`),
  ADD KEY `service_status_id` (`service_status_id`);

--
-- Indexes for table `service_car_details`
--
ALTER TABLE `service_car_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `option_details_id` (`option_details_id`);

--
-- Indexes for table `service_image`
--
ALTER TABLE `service_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_option`
--
ALTER TABLE `service_option`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `service_option_details`
--
ALTER TABLE `service_option_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `service_option_id` (`service_option_id`);

--
-- Indexes for table `service_settings`
--
ALTER TABLE `service_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `service_status`
--
ALTER TABLE `service_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_types`
--
ALTER TABLE `service_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sms_log`
--
ALTER TABLE `sms_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin_role_access`
--
ALTER TABLE `admin_role_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `cancel_reason`
--
ALTER TABLE `cancel_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company_providers`
--
ALTER TABLE `company_providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `customer_cancel_history`
--
ALTER TABLE `customer_cancel_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `customer_location_history`
--
ALTER TABLE `customer_location_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_message`
--
ALTER TABLE `customer_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `customer_notification`
--
ALTER TABLE `customer_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `customer_profile`
--
ALTER TABLE `customer_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `customer_rating`
--
ALTER TABLE `customer_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dispute`
--
ALTER TABLE `dispute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `provider_balance_history`
--
ALTER TABLE `provider_balance_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `provider_booked_timeslot`
--
ALTER TABLE `provider_booked_timeslot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `provider_cancel_history`
--
ALTER TABLE `provider_cancel_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `provider_company_account`
--
ALTER TABLE `provider_company_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `provider_free_timeslot`
--
ALTER TABLE `provider_free_timeslot`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `provider_free_timeslot_history`
--
ALTER TABLE `provider_free_timeslot_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `provider_message`
--
ALTER TABLE `provider_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `provider_notification`
--
ALTER TABLE `provider_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `provider_perference_polygon`
--
ALTER TABLE `provider_perference_polygon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=780;
--
-- AUTO_INCREMENT for table `provider_preference_head`
--
ALTER TABLE `provider_preference_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `provider_preference_location`
--
ALTER TABLE `provider_preference_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `provider_profile`
--
ALTER TABLE `provider_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `provider_rating`
--
ALTER TABLE `provider_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `provider_sattle_history`
--
ALTER TABLE `provider_sattle_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `provider_schedule`
--
ALTER TABLE `provider_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `provider_time_history`
--
ALTER TABLE `provider_time_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `provider_worktime`
--
ALTER TABLE `provider_worktime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `service_car_details`
--
ALTER TABLE `service_car_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;
--
-- AUTO_INCREMENT for table `service_image`
--
ALTER TABLE `service_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_option`
--
ALTER TABLE `service_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `service_option_details`
--
ALTER TABLE `service_option_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `service_settings`
--
ALTER TABLE `service_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `service_status`
--
ALTER TABLE `service_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `service_types`
--
ALTER TABLE `service_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sms_log`
--
ALTER TABLE `sms_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `admin_role_access`
--
ALTER TABLE `admin_role_access`
  ADD CONSTRAINT `admin_role_access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `appointments_ibfk_3` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `appointments_ibfk_4` FOREIGN KEY (`service_status_id`) REFERENCES `service_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `appointments_ibfk_5` FOREIGN KEY (`booking_slot_id`) REFERENCES `provider_booked_timeslot` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `company_providers`
--
ALTER TABLE `company_providers`
  ADD CONSTRAINT `company_providers_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `company_providers_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_cancel_history`
--
ALTER TABLE `customer_cancel_history`
  ADD CONSTRAINT `customer_cancel_history_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_cancel_history_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_cancel_history_ibfk_3` FOREIGN KEY (`reason_id`) REFERENCES `cancel_reason` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_location_history`
--
ALTER TABLE `customer_location_history`
  ADD CONSTRAINT `customer_location_history_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_message`
--
ALTER TABLE `customer_message`
  ADD CONSTRAINT `customer_message_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_notification`
--
ALTER TABLE `customer_notification`
  ADD CONSTRAINT `customer_notification_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_notification_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_profile`
--
ALTER TABLE `customer_profile`
  ADD CONSTRAINT `customer_profile_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer_rating`
--
ALTER TABLE `customer_rating`
  ADD CONSTRAINT `customer_rating_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_rating_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_rating_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dispute`
--
ALTER TABLE `dispute`
  ADD CONSTRAINT `dispute_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dispute_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dispute_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `providers`
--
ALTER TABLE `providers`
  ADD CONSTRAINT `providers_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `service_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_balance_history`
--
ALTER TABLE `provider_balance_history`
  ADD CONSTRAINT `provider_balance_history_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_balance_history_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_booked_timeslot`
--
ALTER TABLE `provider_booked_timeslot`
  ADD CONSTRAINT `provider_booked_timeslot_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_booked_timeslot_ibfk_2` FOREIGN KEY (`booked_customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_cancel_history`
--
ALTER TABLE `provider_cancel_history`
  ADD CONSTRAINT `provider_cancel_history_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_cancel_history_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_cancel_history_ibfk_3` FOREIGN KEY (`reason_id`) REFERENCES `cancel_reason` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_company_account`
--
ALTER TABLE `provider_company_account`
  ADD CONSTRAINT `provider_company_account_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_free_timeslot`
--
ALTER TABLE `provider_free_timeslot`
  ADD CONSTRAINT `provider_free_timeslot_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_free_timeslot_history`
--
ALTER TABLE `provider_free_timeslot_history`
  ADD CONSTRAINT `provider_free_timeslot_history_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_message`
--
ALTER TABLE `provider_message`
  ADD CONSTRAINT `provider_message_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_notification`
--
ALTER TABLE `provider_notification`
  ADD CONSTRAINT `provider_notification_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_notification_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_perference_polygon`
--
ALTER TABLE `provider_perference_polygon`
  ADD CONSTRAINT `provider_perference_polygon_ibfk_1` FOREIGN KEY (`preference_id`) REFERENCES `provider_preference_head` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_preference_location`
--
ALTER TABLE `provider_preference_location`
  ADD CONSTRAINT `provider_preference_location_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_preference_location_ibfk_2` FOREIGN KEY (`preference_id`) REFERENCES `provider_preference_head` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_profile`
--
ALTER TABLE `provider_profile`
  ADD CONSTRAINT `provider_profile_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_rating`
--
ALTER TABLE `provider_rating`
  ADD CONSTRAINT `provider_rating_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_rating_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_rating_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_time_history`
--
ALTER TABLE `provider_time_history`
  ADD CONSTRAINT `provider_time_history_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_time_history_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provider_worktime`
--
ALTER TABLE `provider_worktime`
  ADD CONSTRAINT `provider_worktime_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_worktime_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `provider_worktime_ibfk_4` FOREIGN KEY (`provider_timehistory_id`) REFERENCES `provider_time_history` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `service_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `service_ibfk_3` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_ibfk_4` FOREIGN KEY (`booking_slot_id`) REFERENCES `provider_booked_timeslot` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_ibfk_5` FOREIGN KEY (`service_status_id`) REFERENCES `service_status` (`id`);

--
-- Constraints for table `service_car_details`
--
ALTER TABLE `service_car_details`
  ADD CONSTRAINT `service_car_details_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_car_details_ibfk_2` FOREIGN KEY (`option_details_id`) REFERENCES `service_option_details` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `service_image`
--
ALTER TABLE `service_image`
  ADD CONSTRAINT `service_image_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_option`
--
ALTER TABLE `service_option`
  ADD CONSTRAINT `service_option_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `service_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_option_details`
--
ALTER TABLE `service_option_details`
  ADD CONSTRAINT `service_option_details_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `service_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_option_details_ibfk_2` FOREIGN KEY (`service_option_id`) REFERENCES `service_option` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_settings`
--
ALTER TABLE `service_settings`
  ADD CONSTRAINT `service_settings_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `service_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
