package customView;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Pallab on 9/27/2016.
 */
public class HelveticaneueRegularTextView extends TextView {
    private  Context mContext;
    public HelveticaneueRegularTextView(Context context) {
        super(context);
        this.mContext=context;
        setUp();
    }

    public HelveticaneueRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext=context;
        setUp();
    }

    public HelveticaneueRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext=context;
        setUp();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HelveticaneueRegularTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext=context;
        setUp();
    }
    private void setUp() {
        setTypeface(Typeface.createFromAsset(mContext.getAssets(), "HelveticaNeue.otf"));
    }
}
