package com.uipl.qber.Model;

/**
 * Created by richa on 15/12/16.
 */
public class WorkSubHeadingDetails {

    private String workSubHeadingId;
    private String workSubHeadingName;
    private double workSubHeadingPrice;
    private boolean isSelected;

    public String getWorkSubHeadingName() {
        return workSubHeadingName;
    }

    public void setWorkSubHeadingName(String workSubHeadingName) {
        this.workSubHeadingName = workSubHeadingName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getWorkSubHeadingId() {
        return workSubHeadingId;
    }

    public void setWorkSubHeadingId(String workSubHeadingId) {
        this.workSubHeadingId = workSubHeadingId;
    }

    public double getWorkSubHeadingPrice() {
        return workSubHeadingPrice;
    }

    public void setWorkSubHeadingPrice(double workSubHeadingPrice) {
        this.workSubHeadingPrice = workSubHeadingPrice;
    }
}
