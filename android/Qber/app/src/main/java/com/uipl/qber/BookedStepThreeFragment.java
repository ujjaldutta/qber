package com.uipl.qber;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;


import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.TypedfaceTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * fragment to display arrival time details
 */
public class BookedStepThreeFragment extends Fragment implements View.OnClickListener {

    View v;
    private RelativeLayout rlAlert, rl_date;
    private TypedfaceTextView tvFromTime, tvToTime, tvStartAmPm, tvEndAmPm, tv_date, tvSubmit;
    ImageView ivStartUp, ivStartDown, ivEndUp, ivEndDown, iv_calender;
    Calendar calenderTempTime1, calenderTempTime2, calenderTempTime3, calenderCurrentTime, calendarStartTime, calendarEndTime, calenderMinTime, calenderMaxTime;
    int interval = 10;
    DialogView dialogView;
    ServiceType serviceType = GlobalVariable.serviceType;
    String allowed_start_time = serviceType.getAllowedStartTime(), allowed_end_time = serviceType.getAllowedEndTime();
    long defaultTimeGap = 180, minTimeGap = Long.parseLong(serviceType.getArrivalDuration()), minGapRuleBypassed = 20;
    MediaPlayer mp;
    boolean currentDateBookingAvailibility = true, currentDateBookingAvailibilityChangable = true;
    String day, month, year, currentDate;
    private TypedfaceTextView tvAlert;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_booked_step_three, container, false);
        initViews();
        calenderCurrentTime = Calendar.getInstance();
        calenderCurrentTime.set(2016, 2, 7);
        setTimings();
        setListener();
        return v;
    }

    // Set initial times to textview and enabling the up/down Arrow
    private void setTimings() {
        calendarStartTime = Calendar.getInstance();
        calendarStartTime.set(2016, 2, 7);
        calendarEndTime = Calendar.getInstance();
        calendarEndTime.set(2016, 2, 7);
        calenderMinTime = Calendar.getInstance();
        calenderMinTime.set(2016, 2, 7);
        calenderMaxTime = Calendar.getInstance();
        calenderMaxTime.set(2016, 2, 7);

        calenderTempTime1 = Calendar.getInstance();
        calenderTempTime1.set(2016, 2, 7);
        calenderTempTime2 = Calendar.getInstance();
        calenderTempTime2.set(2016, 2, 7);
        calenderTempTime3 = Calendar.getInstance();
        calenderTempTime3.set(2016, 2, 7);

        String[] arr_start_time = allowed_start_time.split(":");
        calenderMinTime.set(2016, 2, 7, Integer.parseInt(arr_start_time[0]),
                Integer.parseInt(arr_start_time[1]), 0);
        String[] arr_end_time = allowed_end_time.split(":");
        calenderMaxTime.set(2016, 2, 7, Integer.parseInt(arr_end_time[0]),
                Integer.parseInt(arr_end_time[1]), 0);

        calenderTempTime1.set(2016, 2, 7, Integer.parseInt(arr_end_time[0]),
                Integer.parseInt(arr_end_time[1]) - (int) minGapRuleBypassed, 0);
        calenderTempTime2.set(2016, 2, 7, Integer.parseInt(arr_end_time[0]),
                Integer.parseInt(arr_end_time[1]) - ((int) minGapRuleBypassed) + interval, 0);

        // Check current time is before Mintime, if yes then-- startTime=CurrentTime, endTime=currentTime+minGap
        if (calenderCurrentTime.before(calenderMinTime)) {
            calendarStartTime.set(2016, 2, 7, Integer.parseInt(arr_start_time[0]),
                    Integer.parseInt(arr_start_time[1]), 0);
            calendarEndTime.set(2016, 2, 7, calendarStartTime.get(Calendar.HOUR_OF_DAY) + 3,
                    calendarStartTime.get(Calendar.MINUTE), 0);

            ivStartDown.setEnabled(false);
            ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));

        }
        // Check current time is after Mintime, if yes then-- startTime=CurrentTime, endTime=currentTime+minGap and Date=nextDate
        else if (calenderCurrentTime.equals(calenderTempTime1) || calenderCurrentTime.after(calenderTempTime1)) {
            calendarStartTime.set(2016, 2, 7, Integer.parseInt(arr_start_time[0]),
                    Integer.parseInt(arr_start_time[1]), 0);
            calendarEndTime.set(2016, 2, 7, calendarStartTime.get(Calendar.HOUR_OF_DAY) + 3,
                    calendarStartTime.get(Calendar.MINUTE), 0);

            ivStartDown.setEnabled(false);
            ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));

            // Setting Next Date
            String currentDate = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(currentDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, 1);  // number of days to add
            currentDate = sdf.format(c.getTime());

            if (currentDateBookingAvailibilityChangable) {
                tv_date.setText(currentDate);
                currentDateBookingAvailibility = false;
            }
        }
        // Now Current time is between Mintime & maxTime, So---startTime & minTime=CurrentTime
        else {
            ivStartDown.setEnabled(false);
            ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));

            // Minute is being rounded to ten(if not)
            if ((calenderCurrentTime.get(Calendar.MINUTE) % 10) != 0) {
                calenderMinTime.set(2016, 2, 7, calenderCurrentTime.get(Calendar.HOUR_OF_DAY),
                        (calenderCurrentTime.get(Calendar.MINUTE) + (10 - (calenderCurrentTime.get(Calendar.MINUTE) % 10))), 0);
                calendarStartTime.set(2016, 2, 7, calenderCurrentTime.get(Calendar.HOUR_OF_DAY),
                        (calenderCurrentTime.get(Calendar.MINUTE) + (10 - (calenderCurrentTime.get(Calendar.MINUTE) % 10))), 0);
            } else {
                calenderMinTime.set(2016, 2, 7, calenderCurrentTime.get(Calendar.HOUR_OF_DAY),
                        calenderCurrentTime.get(Calendar.MINUTE), 0);
                calendarStartTime.set(2016, 2, 7, calenderCurrentTime.get(Calendar.HOUR_OF_DAY),
                        calenderCurrentTime.get(Calendar.MINUTE), 0);
            }

            // If diff between (maxTime-startTime) is greater than the defaultTimeGap, then endTime should be startTime+minGap
            // if less then endTime=maxTime and minTimeGap=minGapRuleBypassed
            long Difference = calenderMaxTime.getTimeInMillis() - calendarStartTime.getTimeInMillis();
            if (Difference >= defaultTimeGap) {
                calendarEndTime.set(2016, 2, 7, calendarStartTime.get(Calendar.HOUR_OF_DAY) + 3,
                        calendarStartTime.get(Calendar.MINUTE), 0);
            } else {
                minTimeGap = minGapRuleBypassed * 60000;
                System.out.println("Difference = " + Difference);
                System.out.println("minTimeGap = " + minTimeGap);
                if (Difference >= minTimeGap) {
                    ivStartUp.setEnabled(false);
                    ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));

                    ivEndDown.setEnabled(false);
                    ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                }
                calendarEndTime.set(2016, 2, 7, calenderMaxTime.get(Calendar.HOUR_OF_DAY),
                        calenderMaxTime.get(Calendar.MINUTE), 0);
                ivEndUp.setEnabled(false);
                ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
            }

            calenderTempTime3.set(2016, 2, 7, calendarEndTime.get(Calendar.HOUR_OF_DAY),
                    calendarEndTime.get(Calendar.MINUTE), 0);
            calenderTempTime3.set(Calendar.MINUTE, -20);

            long Difference2 = calenderTempTime3.getTimeInMillis() - calendarStartTime.getTimeInMillis();
            if (Difference2 > 0) {
                ivStartUp.setEnabled(true);
                ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                ivEndDown.setEnabled(true);
                ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));
            } else {
                ivStartUp.setEnabled(false);
                ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));

                ivEndDown.setEnabled(false);
                ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
            }

        }


        tvFromTime.setText(new SimpleDateFormat("hh:mm").format(calendarStartTime.getTime()));
        String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarStartTime.getTime());
        timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
        tvStartAmPm.setText(timeDay);


        tvToTime.setText(new SimpleDateFormat("hh:mm").format(calendarEndTime.getTime()));
        String timeDay2 = new SimpleDateFormat("hh:mm-a").format(calendarEndTime.getTime());
        timeDay2 = timeDay2.substring(timeDay2.lastIndexOf("-") + 1);
        tvEndAmPm.setText(timeDay2);

        currentDateBookingAvailibilityChangable = false;


        System.out.println("calenderCurrentTime.getTime() = " + calenderCurrentTime.getTime());
        System.out.println("calendarStartTime.getTime() = " + calendarStartTime.getTime());
        System.out.println("calendarEndTime.getTime() = " + calendarEndTime.getTime());
        System.out.println("calenderMinTime.getTime() = " + calenderMinTime.getTime());
        System.out.println("calenderMaxTime.getTime() = " + calenderMaxTime.getTime());

    }

    private void setListener() {
        ivStartUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound();
                ivStartDown.setEnabled(true);
                ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                calendarStartTime.add(Calendar.MINUTE, interval);
                Date startTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarStartTime.getTime()), "hh:mm a");
                Date maxTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMaxTime.getTime()), "hh:mm a");
                tvFromTime.setText(new SimpleDateFormat("hh:mm").format(calendarStartTime.getTime()));
                String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarStartTime.getTime());
                timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
                tvStartAmPm.setText(timeDay);
                long diff = calendarEndTime.getTimeInMillis() - calendarStartTime.getTimeInMillis();
                System.out.println("diff = " + diff);
                if (diff <= minTimeGap) {
                    ivStartUp.setEnabled(false);
                    ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));

                    ivEndDown.setEnabled(false);
                    ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                }
                if (startTIME.compareTo(maxTIME) >= 0) {
                    ivStartUp.setEnabled(false);
                    ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
                }
            }
        });

        ivStartDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound();
                ivStartUp.setEnabled(true);
                ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                ivEndDown.setEnabled(true);
                ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                calendarStartTime.add(Calendar.MINUTE, -interval);
                Date startTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarStartTime.getTime()), "hh:mm a");
                Date minTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMinTime.getTime()), "hh:mm a");
                tvFromTime.setText(new SimpleDateFormat("hh:mm").format(calendarStartTime.getTime()));
                String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarStartTime.getTime());
                timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
                tvStartAmPm.setText(timeDay);
                if (startTIME.compareTo(minTIME) <= 0) {
                    ivStartDown.setEnabled(false);
                    ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                }
            }
        });


        ivEndUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound();
                ivEndDown.setEnabled(true);
                ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                ivStartUp.setEnabled(true);
                ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                calendarEndTime.add(Calendar.MINUTE, interval);
                Date startTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarEndTime.getTime()), "hh:mm a");
                Date maxTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMaxTime.getTime()), "hh:mm a");
                tvToTime.setText(new SimpleDateFormat("hh:mm").format(calendarEndTime.getTime()));
                String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarEndTime.getTime());
                timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
                tvEndAmPm.setText(timeDay);
                if (startTIME.compareTo(maxTIME) >= 0) {
                    ivEndUp.setEnabled(false);
                    ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
                }
            }
        });
        ivEndDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound();
                ivEndUp.setEnabled(true);
                ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                calendarEndTime.add(Calendar.MINUTE, -interval);

                Date startTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarEndTime.getTime()), "hh:mm a");
                Date minTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMinTime.getTime()), "hh:mm a");
                tvToTime.setText(new SimpleDateFormat("hh:mm").format(calendarEndTime.getTime()));
                String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarEndTime.getTime());
                timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
                tvEndAmPm.setText(timeDay);
                long diff = calendarEndTime.getTimeInMillis() - calendarStartTime.getTimeInMillis();
                if (diff <= minTimeGap) {
                    ivEndDown.setEnabled(false);
                    ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));

                    ivStartUp.setEnabled(false);
                    ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
                }
                if (startTIME.compareTo(minTIME) <= 0) {
                    ivEndDown.setEnabled(false);
                    ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                }

            }
        });

        tvFromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> arr = getArrayListContent(1, calenderMinTime, calenderMaxTime, minTimeGap);
                showTimeDialog(1, arr);
            }
        });

        tvToTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> arr = getArrayListContent(2, calenderMinTime, calenderMaxTime, minTimeGap);
                showTimeDialog(2, arr);
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validation()) {
                    //Toast.makeText(getActivity(),"Coming soon",Toast.LENGTH_SHORT).show();
                    String endTime = new SimpleDateFormat("HH:mm:ss").format(calendarEndTime.getTime());
                    String startTime = new SimpleDateFormat("HH:mm:ss").format(calendarStartTime.getTime());

                    String date = tv_date.getText().toString().trim();

                    if (date.equals("Today")) {
                        Calendar c = Calendar.getInstance();
                        date = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
                    } else {
                        date = date.replace("/", "-");
                    }
                    GlobalVariable.serviceStartDate = date + " " + startTime;
                    GlobalVariable.serviceEndDate = date + " " + endTime;
                    startActivity(new Intent(getActivity(), ProviderListingActivity.class));
                }

            }
        });
    }

    public boolean Validation() {
        if (GlobalVariable.totalServicesAmount <= Integer.parseInt(serviceType.getMinCost())) {
            dialogView.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.minimum_order_amount_should_be) + " " + serviceType.getMinCost());
            return false;
        } else if (!GlobalVariable.setLocation) {
            dialogView.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.select_service_location));
            return false;
        }
        return true;
    }

    private ArrayList<String> getArrayListContent(int type, Calendar MinTime, Calendar MaxTime, long minTimeGap) {
        ArrayList<String> arr = new ArrayList<>();


        Calendar calenderMinTime1 = Calendar.getInstance();
        calenderMinTime1.set(Calendar.HOUR_OF_DAY, MinTime.get(Calendar.HOUR_OF_DAY));
        calenderMinTime1.set(Calendar.MINUTE, MinTime.get(Calendar.MINUTE));

        Calendar calenderMaxTime1 = Calendar.getInstance();
        calenderMaxTime1.set(Calendar.HOUR_OF_DAY, MaxTime.get(Calendar.HOUR_OF_DAY));
        calenderMaxTime1.set(Calendar.MINUTE, MaxTime.get(Calendar.MINUTE));

        int minutes = (int) ((minTimeGap / (1000 * 60)) % 60);
        int hours = (int) ((minTimeGap / (1000 * 60 * 60)) % 24);

        if (type == 1) {
            calenderMaxTime1.add(Calendar.HOUR_OF_DAY, -hours);
            calenderMaxTime1.add(Calendar.MINUTE, -minutes);
        } else {
            calenderMinTime1.add(Calendar.HOUR_OF_DAY, hours);
            calenderMinTime1.add(Calendar.MINUTE, minutes);
        }

        do {
            arr.add(new SimpleDateFormat("hh:mm a").format(calenderMinTime1.getTime()));
            calenderMinTime1.add(Calendar.MINUTE, 30);
        } while (calenderMinTime1.compareTo(calenderMaxTime1) <= 0);

        return arr;
    }

    /**
     * Method to initialize variable
     */
    private void initViews() {
        rlAlert = (RelativeLayout) v.findViewById(R.id.rlAlert);
        rl_date = (RelativeLayout) v.findViewById(R.id.rl_date);
        rl_date.setOnClickListener(this);
        tvAlert = (TypedfaceTextView) v.findViewById(R.id.tvAlert);
        if (!GlobalVariable.notice.equals("")) {
            rlAlert.setVisibility(View.VISIBLE);
            tvAlert.setText(GlobalVariable.notice);
        } else {
            rlAlert.setVisibility(View.GONE);
        }
        tvFromTime = (TypedfaceTextView) v.findViewById(R.id.tvFromTime);
        tvToTime = (TypedfaceTextView) v.findViewById(R.id.tvToTime);
        tvStartAmPm = (TypedfaceTextView) v.findViewById(R.id.tvStartAmPm);
        tvEndAmPm = (TypedfaceTextView) v.findViewById(R.id.tvEndAmPm);
        tv_date = (TypedfaceTextView) v.findViewById(R.id.tv_date);
        tvSubmit = (TypedfaceTextView) v.findViewById(R.id.tvSubmit);

        ivStartUp = (ImageView) v.findViewById(R.id.ivStartUp);
        ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

        ivStartDown = (ImageView) v.findViewById(R.id.ivStartDown);
        ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

        ivEndUp = (ImageView) v.findViewById(R.id.ivEndUp);
        ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

        ivEndDown = (ImageView) v.findViewById(R.id.ivEndDown);
        ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

        iv_calender = (ImageView) v.findViewById(R.id.iv_calender);
        iv_calender.setOnClickListener(this);

        dialogView = new DialogView(getActivity());
        mp = MediaPlayer.create(getActivity(), R.raw.tick);

        defaultTimeGap = defaultTimeGap * 60000;
        minTimeGap = minTimeGap * 60000;
    }

    /**
     * @param context
     * @param header
     * @return Method to show date picker dialog
     */
    public String showDatePickerDialog(Context context, String header) {
        final DatePicker datePicker = new DatePicker(context);
        if (year != null)
            datePicker.updateDate(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day));
        long now = System.currentTimeMillis() - 1000;
        if (currentDateBookingAvailibility) {
            datePicker.setMinDate(now);
            datePicker.setMaxDate(now + (1000 * 60 * 60 * 24 * 6)); //After 7 Days from Now
        } else {
            datePicker.setMinDate(now + (1000 * 60 * 60 * 24 * 1)); //After 1 Days from Now
            datePicker.setMaxDate(now + (1000 * 60 * 60 * 24 * 7)); //After 7 Days from Now
        }

        datePicker.setCalendarViewShown(false);

        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage("");
        adb.setPositiveButton("Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                int datePickerYear = datePicker.getYear();
                int datePickerMonth = datePicker.getMonth();
                int datePickerDate = datePicker.getDayOfMonth();
                Calendar c = Calendar.getInstance();
                c.set(datePickerYear, datePickerMonth, datePickerDate, 0, 0, 0);
                dialog.dismiss();
                day = "" + datePicker.getDayOfMonth();
                int y = datePicker.getMonth() + 1;
                month = "" + y;
                year = "" + datePicker.getYear();

                if (day.length() < 2)
                    day = "0" + day;
                if (month.length() < 2)
                    month = "0" + month;

                String date = year + "/" + month + "/" + day;
                if (date != null && date != "") {
                    currentDate = new SimpleDateFormat("yyyy/MM/dd").format(new Date());

                    ivStartUp.setEnabled(true);
                    ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                    ivStartDown.setEnabled(true);
                    ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                    ivEndUp.setEnabled(true);
                    ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                    ivEndDown.setEnabled(true);
                    ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                    if (date.equals(currentDate)) {
                        tv_date.setText("Today");
                        calenderCurrentTime = Calendar.getInstance();
                        calenderCurrentTime.set(2016, 2, 7);
                        setTimings();
                    } else {
                        tv_date.setText(date);
                        calenderCurrentTime = Calendar.getInstance();
                        Calendar temp3 = Calendar.getInstance();
                        String[] arr = allowed_start_time.split(":");
                        temp3.set(2016, 2, 7, Integer.parseInt(arr[0]), Integer.parseInt(arr[1]), 00);
                        temp3.add(Calendar.MINUTE, -5);
                        calenderCurrentTime = temp3.getInstance();
                        setTimings();

                    }
                }

            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        adb.setView(datePicker);
        adb.show();
        return "";
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_date:
                showDatePickerDialog(getActivity(), "Select Date");
                break;
            case R.id.iv_calender:
                showDatePickerDialog(getActivity(), "Select Date");
                break;
        }
    }

    /**
     * @param type
     * @param arr  Dialog to show timing details
     */
    private void showTimeDialog(final int type, ArrayList<String> arr) {
        final Dialog builderSingle = new Dialog(getActivity());
        builderSingle.setTitle("Select One Time");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = inflater.inflate(R.layout.list_time, null);
        builderSingle.setContentView(convertView);

        ListView lv = (ListView) convertView.findViewById(R.id.lv);
        final ArrayAdapter<String> ArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arr);
        lv.setAdapter(ArrayAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (type == 1) {
                    ivStartUp.setEnabled(true);
                    ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                    ivStartDown.setEnabled(true);
                    ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                    String strName = ArrayAdapter.getItem(position);
                    String startTIME24 = DateFormatter.Convert12to24(strName);
                    String arr[] = startTIME24.split(":");

                    // Setting StartTIme
                    calendarStartTime.set(2016, 2, 7, Integer.parseInt(arr[0]),
                            Integer.parseInt(arr[1]), 0);
                    tvFromTime.setText(new SimpleDateFormat("hh:mm").format(calendarStartTime.getTime()));
                    String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarStartTime.getTime());
                    timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
                    tvStartAmPm.setText(timeDay);


                    // Setting EndTime Depending on StartTime
                    int x = ((int) minTimeGap) / 60000;
                    long diff = calendarEndTime.getTimeInMillis() - calendarStartTime.getTimeInMillis();
                    System.out.println("diff = " + diff);
                    if (diff < (int) minTimeGap) {
                        calendarEndTime.set(2016, 2, 7, Integer.parseInt(arr[0]),
                                Integer.parseInt(arr[1]), 0);
                        calendarEndTime.add(Calendar.MINUTE, x);
                        tvToTime.setText(new SimpleDateFormat("hh:mm").format(calendarEndTime.getTime()));
                        String timeDay2 = new SimpleDateFormat("hh:mm-a").format(calendarEndTime.getTime());
                        timeDay2 = timeDay2.substring(timeDay2.lastIndexOf("-") + 1);
                        tvEndAmPm.setText(timeDay2);
                    }


                    // Setting StartTime Arrow
                    Date startTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarStartTime.getTime()), "hh:mm a");
                    Date minTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMinTime.getTime()), "hh:mm a");
                    if (startTIME.compareTo(minTIME) <= 0) {
                        ivStartDown.setEnabled(false);
                        ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                    }
                    if (diff <= ((int) minTimeGap) + interval) {
                        ivStartUp.setEnabled(false);
                        ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));

                        ivEndDown.setEnabled(false);
                        ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                    } else {
                        ivStartUp.setEnabled(true);
                        ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                        ivEndDown.setEnabled(true);
                        ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));
                    }


                    // Setting EndTime Arrow
                    Date endTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarEndTime.getTime()), "hh:mm a");
                    Date maxTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMaxTime.getTime()), "hh:mm a");
                    if (endTIME.compareTo(maxTIME) >= 0) {
                        ivEndUp.setEnabled(false);
                        ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
                    }


                } else {
                    ivEndUp.setEnabled(true);
                    ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));

                    ivEndDown.setEnabled(true);
                    ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico2));

                    String strName = ArrayAdapter.getItem(position);
                    String startTIME24 = DateFormatter.Convert12to24(strName);
                    String arr[] = startTIME24.split(":");

                    // Setting EndTime
                    calendarEndTime.set(2016, 2, 7, Integer.parseInt(arr[0]),
                            Integer.parseInt(arr[1]), 0);
                    tvToTime.setText(new SimpleDateFormat("hh:mm").format(calendarEndTime.getTime()));
                    String timeDay = new SimpleDateFormat("hh:mm-a").format(calendarEndTime.getTime());
                    timeDay = timeDay.substring(timeDay.lastIndexOf("-") + 1);
                    tvEndAmPm.setText(timeDay);


                    // Setting StartTime Depending on Endtime
                    int x = ((int) minTimeGap) / 60000;
                    long diff = calendarEndTime.getTimeInMillis() - calendarStartTime.getTimeInMillis();
                    System.out.println("diff = " + diff);
                    if (diff < (int) minTimeGap) {
                        calendarStartTime.set(2016, 2, 7, Integer.parseInt(arr[0]),
                                Integer.parseInt(arr[1]), 0);
                        calendarStartTime.add(Calendar.MINUTE, -x);
                        tvFromTime.setText(new SimpleDateFormat("hh:mm").format(calendarStartTime.getTime()));
                        String timeDay2 = new SimpleDateFormat("hh:mm-a").format(calendarStartTime.getTime());
                        timeDay2 = timeDay2.substring(timeDay2.lastIndexOf("-") + 1);
                        tvStartAmPm.setText(timeDay2);
                    }

                    // Setting EndTime Arrow
                    if (diff <= minTimeGap) {
                        ivEndDown.setEnabled(false);
                        ivEndDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));

                        ivStartUp.setEnabled(false);
                        ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
                    } else {
                        ivStartUp.setEnabled(true);
                        ivStartUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_ico1));
                    }

                    Date endTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calendarEndTime.getTime()), "hh:mm a");
                    Date maxTIME = DateFormatter.getDateFromString(new SimpleDateFormat("hh:mm a").format(calenderMaxTime.getTime()), "hh:mm a");

                    if (endTIME.compareTo(maxTIME) >= 0) {
                        ivEndUp.setEnabled(false);
                        ivEndUp.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico1));
                    }

                    long diff2 = calendarStartTime.getTimeInMillis() - calenderMinTime.getTimeInMillis();
                    if (diff2 <= interval) {
                        ivStartDown.setEnabled(false);
                        ivStartDown.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_grey_ico2));
                    }
                }
                builderSingle.dismiss();
            }
        });


        builderSingle.show();
    }

    /**
     * Method to play sound
     */
    private void playSound() {
        try {
            if (mp.isPlaying()) {
                mp.stop();
                mp.release();
                mp = MediaPlayer.create(getActivity(), R.raw.tick);
            }
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
