package com.uipl.qber.Model;

import java.util.ArrayList;

/**
 * Created by richa on 12/12/16.
 */
public class WorkHeadingDetails {

    private String carWashType;
    private String workHeadingId;
    private String workHeadingName;
    private double workHeadingPrice;
    private ArrayList<WorkSubHeadingDetails> subHeadingDetailsArrayList = new ArrayList<>();

    public WorkHeadingDetails() {
        super();
        // TODO Auto-generated constructor stub
    }


    public WorkHeadingDetails(String carWashType, String workHeadingId, String workHeadingName, double workHeadingPrice, ArrayList<WorkSubHeadingDetails> subHeadingDetailsArrayList) {
        this.carWashType = carWashType;
        this.workHeadingId = workHeadingId;
        this.workHeadingName = workHeadingName;
        this.workHeadingPrice = workHeadingPrice;
        this.subHeadingDetailsArrayList = subHeadingDetailsArrayList;
    }

    public String getWorkHeadingName() {
        return workHeadingName;
    }

    public void setWorkHeadingName(String workHeadingName) {
        this.workHeadingName = workHeadingName;
    }

    public double getWorkHeadingPrice() {
        return workHeadingPrice;
    }

    public void setWorkHeadingPrice(double workHeadingPrice) {
        this.workHeadingPrice = workHeadingPrice;
    }

    public ArrayList<WorkSubHeadingDetails> getSubHeadingDetailsArrayList() {
        return subHeadingDetailsArrayList;
    }

    public void setSubHeadingDetailsArrayList(ArrayList<WorkSubHeadingDetails> subHeadingDetailsArrayList) {
        this.subHeadingDetailsArrayList = subHeadingDetailsArrayList;
    }

    public String getWorkHeadingId() {
        return workHeadingId;
    }

    public void setWorkHeadingId(String workHeadingId) {
        this.workHeadingId = workHeadingId;
    }

    public String getCarWashType() {
        return carWashType;
    }

    public void setCarWashType(String carWashType) {
        this.carWashType = carWashType;
    }
}
