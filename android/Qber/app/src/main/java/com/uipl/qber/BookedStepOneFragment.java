package com.uipl.qber;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.uipl.qber.base.GlobalVariable;

import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.utils.DialogView;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Fragment to display Car Wash details
 */
public class BookedStepOneFragment extends Fragment {

    View view;
    ScrollView svScroll;
    TextView tvSuvCount, tvSedunCount, tvTotal, tvNext, tvNoteText;
    Spinner spnSuv, spnSedun;
    ServiceType serviceType = GlobalVariable.serviceType;
    int totalSuvCount = 1, totalSedunCount = 1;
    private LinearLayout llSuv, llSedun, llAdableSuv, llAdableSedun;
    private DialogView dialogView;
    private int qarColor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_book_step_one, container, false);
        initViews();
        setListener();
        return view;
    }

    /**
     * Method to set item click listener
     */
    private void setListener() {
        spnSuv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                totalSuvCount = Integer.parseInt(parent.getItemAtPosition(position).toString());
                tvSuvCount.setText(String.valueOf(totalSuvCount));
                addViewSuv();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnSedun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                totalSedunCount = Integer.parseInt(parent.getItemAtPosition(position).toString());
                tvSedunCount.setText(String.valueOf(totalSedunCount));
                addViewSedun();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        llSuv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spnSuv.performClick();
            }
        });

        llSedun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spnSedun.performClick();
            }
        });

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkAddView()) {
                    dialogView.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_service));
                } else {
                    if (GlobalVariable.totalServicesAmount <= Integer.parseInt(serviceType.getMinCost())) {
                        dialogView.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.minimum_order_amount_should_be) + " " + serviceType.getMinCost());

                    } else {
                        BookedActivity.viewpagerBook.setCurrentItem(1);
                    }
                }
            }
        });
    }

    /**
     * Method for initialize variable
     */
    private void initViews() {
        GlobalVariable.suvList = new ArrayList<>();
        GlobalVariable.sedanList = new ArrayList<>();

        dialogView = new DialogView(getActivity());
        qarColor = getResources().getColor(R.color.grey);
        llSuv = (LinearLayout) view.findViewById(R.id.llSuv);
        llSedun = (LinearLayout) view.findViewById(R.id.llSedun);

        llAdableSuv = (LinearLayout) view.findViewById(R.id.llAdableSuv);
        llAdableSedun = (LinearLayout) view.findViewById(R.id.llAdableSedun);

        svScroll = (ScrollView) view.findViewById(R.id.svScroll);
        tvSuvCount = (TextView) view.findViewById(R.id.tvSuvCount);
        tvSedunCount = (TextView) view.findViewById(R.id.tvSedunCount);
        tvTotal = (TextView) view.findViewById(R.id.tvTotal);
        tvNext = (TextView) view.findViewById(R.id.tvNext);

        spnSuv = (Spinner) view.findViewById(R.id.spnSuv);
        spnSedun = (Spinner) view.findViewById(R.id.spnSedun);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity(), R.array.item_count, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSuv.setAdapter(adapter);
        spnSedun.setAdapter(adapter);
        tvNoteText = (TextView) view.findViewById(R.id.tvNoteText);
        tvNoteText.setText(Html.fromHtml("<big>" + getString(R.string.please_note_that_delivery_and_set_up_fees_are) + " " + serviceType.getFixedCost() + "</big><small>" + getString(R.string.qar) + "</small>"));
    }

    /**
     * Method to add SUV item view
     */
    private void addViewSuv() {
        if (llAdableSuv.getChildCount() < totalSuvCount) {
            // Added
            int extraAdable = totalSuvCount - llAdableSuv.getChildCount();
            int alreadyAdded = llAdableSuv.getChildCount();
            for (int i = 0; i < extraAdable; i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View myView = inflater.inflate(R.layout.inflate_care_feature, null);

                final LinearLayout llExpandable = (LinearLayout) myView.findViewById(R.id.llExpandable);
                final ImageView imv_expand = (ImageView) myView.findViewById(R.id.imv_expand);

                TextView tv_careName = (TextView) myView.findViewById(R.id.tv_careName);
                tv_careName.setText(getString(R.string.suv) + " " + (alreadyAdded + i + 1));
                final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
                cb_totalAmount.setTag(GlobalVariable.suvList.size());

                final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
                cb_pressureWash.setTag(GlobalVariable.suvList.size());
                final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
                cb_interior.setTag(GlobalVariable.suvList.size());
                final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
                cb_tyre.setTag(GlobalVariable.suvList.size());
                TextView tv_pressureWashPrice = (TextView) myView.findViewById(R.id.tv_pressureWashPrice);
                TextView tv_interiorPrice = (TextView) myView.findViewById(R.id.tv_interiorPrice);
                TextView tv_tyrePrice = (TextView) myView.findViewById(R.id.tv_tyrePrice);
                ImageView imvHelppressure = (ImageView) myView.findViewById(R.id.imvHelppressure);
                ImageView imvHelpInterior = (ImageView) myView.findViewById(R.id.imvHelpInterior);

                if (serviceType != null) {
                    ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                    optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();

                    int total = Integer.parseInt(optDetails.get(0).get("cost")) + Integer.parseInt(optDetails.get(1).get("cost")) + Integer.parseInt(optDetails.get(2).get("cost"));
                    cb_totalAmount.setText(Html.fromHtml("<b><big>" + String.valueOf(total) + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font></b>"));
                    tv_pressureWashPrice.setText(Html.fromHtml("<big>" + optDetails.get(0).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_interiorPrice.setText(Html.fromHtml("<big>" + optDetails.get(1).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_tyrePrice.setText(Html.fromHtml("<big>" + optDetails.get(2).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    hashMap.put("0", optDetails.get(0).get("sub_service_id"));
                    hashMap.put("1", optDetails.get(1).get("sub_service_id"));
                    hashMap.put("2", optDetails.get(2).get("sub_service_id"));
                    GlobalVariable.suvList.add(hashMap);
                }

                cb_totalAmount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_totalAmount.isChecked()) {
                            cb_totalAmount.setChecked(true);
                            cb_pressureWash.setChecked(true);
                            cb_interior.setChecked(true);
                            cb_tyre.setChecked(true);
                            GlobalVariable.suvList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                            GlobalVariable.suvList.get(pos).put("1", optDetails.get(1).get("sub_service_id"));
                            GlobalVariable.suvList.get(pos).put("2", optDetails.get(2).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            cb_pressureWash.setChecked(false);
                            cb_interior.setChecked(false);
                            cb_tyre.setChecked(false);
                            GlobalVariable.suvList.get(pos).put("0", "");
                            GlobalVariable.suvList.get(pos).put("1", "");
                            GlobalVariable.suvList.get(pos).put("2", "");
                        }

                        calculateTotalAmount();
                    }
                });

                cb_pressureWash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_pressureWash.isChecked()) {
                            GlobalVariable.suvList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            GlobalVariable.suvList.get(pos).put("0", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_interior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_interior.isChecked()) {
                            GlobalVariable.suvList.get(pos).put("1", optDetails.get(1).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            GlobalVariable.suvList.get(pos).put("1", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_tyre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_tyre.isChecked()) {
                            GlobalVariable.suvList.get(pos).put("2", optDetails.get(2).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            GlobalVariable.suvList.get(pos).put("2", "");
                        }
                        calculateTotalAmount();
                    }
                });

                imv_expand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (llExpandable.getVisibility() == View.VISIBLE) {
                            llExpandable.setVisibility(View.GONE);
                            imv_expand.setImageResource(R.drawable.plus_ico);
                        } else {
                            llExpandable.setVisibility(View.VISIBLE);
                            imv_expand.setImageResource(R.drawable.minus_ico);
                        }
                    }
                });

                imvHelppressure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogView.showCustomSingleButtonDialog(getActivity(), "", getResources().getString(R.string.help_msg1));
                    }
                });

                imvHelpInterior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogView.showCustomSingleButtonDialog(getActivity(), "", getResources().getString(R.string.help_msg2));
                    }
                });


                llAdableSuv.addView(myView);
            }
        } else {
            // Removed
            int extraRemovable = llAdableSuv.getChildCount() - totalSuvCount;
            for (int i = 0; i < extraRemovable; i++) {
                llAdableSuv.removeViewAt(llAdableSuv.getChildCount() - 1);
                GlobalVariable.suvList.remove(GlobalVariable.suvList.size() - 1);
            }
        }


        calculateTotalAmount();

    }

    /**
     * Method to add Sedan item view
     */
    private void addViewSedun() {
        if (llAdableSedun.getChildCount() < totalSedunCount) {
            // Added
            int extraAdable = totalSedunCount - llAdableSedun.getChildCount();
            int alreadyAdded = llAdableSedun.getChildCount();

            for (int i = 0; i < extraAdable; i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View myView = inflater.inflate(R.layout.inflate_care_feature, null);

                final LinearLayout llExpandable = (LinearLayout) myView.findViewById(R.id.llExpandable);
                final ImageView imv_expand = (ImageView) myView.findViewById(R.id.imv_expand);

                TextView tv_careName = (TextView) myView.findViewById(R.id.tv_careName);
                tv_careName.setText(getString(R.string.sedan) + " " + (alreadyAdded + i + 1));
                final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
                cb_totalAmount.setTag(GlobalVariable.sedanList.size());


                final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
                cb_pressureWash.setTag(GlobalVariable.sedanList.size());
                final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
                cb_interior.setTag(GlobalVariable.sedanList.size());
                final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
                cb_tyre.setTag(GlobalVariable.sedanList.size());
                TextView tv_pressureWashPrice = (TextView) myView.findViewById(R.id.tv_pressureWashPrice);
                TextView tv_interiorPrice = (TextView) myView.findViewById(R.id.tv_interiorPrice);
                TextView tv_tyrePrice = (TextView) myView.findViewById(R.id.tv_tyrePrice);

                ImageView imvHelppressure = (ImageView) myView.findViewById(R.id.imvHelppressure);
                ImageView imvHelpInterior = (ImageView) myView.findViewById(R.id.imvHelpInterior);

                if (serviceType != null) {
                    ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                    optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                    int total = Integer.parseInt(optDetails.get(0).get("cost")) + Integer.parseInt(optDetails.get(1).get("cost")) + Integer.parseInt(optDetails.get(2).get("cost"));
                    cb_totalAmount.setText(Html.fromHtml("<b><big>" + String.valueOf(total) + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font></b>"));
                    tv_pressureWashPrice.setText(Html.fromHtml("<big>" + optDetails.get(0).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_interiorPrice.setText(Html.fromHtml("<big>" + optDetails.get(1).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_tyrePrice.setText(Html.fromHtml("<big>" + optDetails.get(2).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    hashMap.put("0", optDetails.get(0).get("sub_service_id"));
                    hashMap.put("1", optDetails.get(1).get("sub_service_id"));
                    hashMap.put("2", optDetails.get(2).get("sub_service_id"));
                    GlobalVariable.sedanList.add(hashMap);
                }

                cb_totalAmount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_totalAmount.isChecked()) {
                            cb_totalAmount.setChecked(true);
                            cb_pressureWash.setChecked(true);
                            cb_interior.setChecked(true);
                            cb_tyre.setChecked(true);
                            GlobalVariable.sedanList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                            GlobalVariable.sedanList.get(pos).put("1", optDetails.get(1).get("sub_service_id"));
                            GlobalVariable.sedanList.get(pos).put("2", optDetails.get(2).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            cb_pressureWash.setChecked(false);
                            cb_interior.setChecked(false);
                            cb_tyre.setChecked(false);
                            GlobalVariable.sedanList.get(pos).put("0", "");
                            GlobalVariable.sedanList.get(pos).put("1", "");
                            GlobalVariable.sedanList.get(pos).put("2", "");
                        }

                        calculateTotalAmount();
                    }
                });

                cb_pressureWash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_pressureWash.isChecked()) {
                            GlobalVariable.sedanList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            GlobalVariable.sedanList.get(pos).put("0", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_interior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_interior.isChecked()) {
                            GlobalVariable.sedanList.get(pos).put("1", optDetails.get(0).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            GlobalVariable.sedanList.get(pos).put("1", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_tyre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_tyre.isChecked()) {
                            GlobalVariable.sedanList.get(pos).put("2", optDetails.get(0).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            GlobalVariable.sedanList.get(pos).put("2", "");
                        }
                        calculateTotalAmount();
                    }
                });

                imv_expand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (llExpandable.getVisibility() == View.VISIBLE) {
                            llExpandable.setVisibility(View.GONE);
                            imv_expand.setImageResource(R.drawable.plus_ico);
                        } else {
                            llExpandable.setVisibility(View.VISIBLE);
                            imv_expand.setImageResource(R.drawable.minus_ico);
                        }
                    }
                });

                imvHelppressure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogView.showCustomSingleButtonDialog(getActivity(), "", getResources().getString(R.string.help_msg1));
                    }
                });

                imvHelpInterior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogView.showCustomSingleButtonDialog(getActivity(), "", getResources().getString(R.string.help_msg2));
                    }
                });

                llAdableSedun.addView(myView);
            }

        } else {
            // Removed
            int extraRemovable = llAdableSedun.getChildCount() - totalSedunCount;
            for (int i = 0; i < extraRemovable; i++) {
                llAdableSedun.removeViewAt(llAdableSedun.getChildCount() - 1);
                GlobalVariable.sedanList.remove(GlobalVariable.sedanList.size() - 1);
            }
        }

        calculateTotalAmount();
    }

    /**
     * Method to calculate TotalAmount
     */
    private void calculateTotalAmount() {
        int totalAmount = 0;

        int countSuv = llAdableSuv.getChildCount();
        for (int i = 0; i < countSuv; i++) {
            View myView = llAdableSuv.getChildAt(i);
            final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
            final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
            final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
            final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
            ArrayList<HashMap<String, String>> optDetailsSuv = new ArrayList<>();
            optDetailsSuv = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();

            if (cb_totalAmount.isChecked()) {
                int total = Integer.parseInt(optDetailsSuv.get(0).get("cost")) + Integer.parseInt(optDetailsSuv.get(1).get("cost")) + Integer.parseInt(optDetailsSuv.get(2).get("cost"));
                totalAmount = totalAmount + total;
            } else {
                if (cb_pressureWash.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSuv.get(0).get("cost"));
                }
                if (cb_interior.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSuv.get(1).get("cost"));
                }
                if (cb_tyre.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSuv.get(2).get("cost"));
                }
            }
        }

        int countSedun = llAdableSedun.getChildCount();
        for (int i = 0; i < countSedun; i++) {
            View myView = llAdableSedun.getChildAt(i);
            final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
            final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
            final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
            final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
            ArrayList<HashMap<String, String>> optDetailsSedun = new ArrayList<>();
            optDetailsSedun = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
            if (cb_totalAmount.isChecked()) {
                int total = Integer.parseInt(optDetailsSedun.get(0).get("cost")) + Integer.parseInt(optDetailsSedun.get(1).get("cost")) + Integer.parseInt(optDetailsSedun.get(2).get("cost"));
                totalAmount = totalAmount + total;
            } else {
                if (cb_pressureWash.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSedun.get(0).get("cost"));
                }
                if (cb_interior.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSedun.get(1).get("cost"));
                }
                if (cb_tyre.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSedun.get(2).get("cost"));
                }
            }
        }
        totalAmount = totalAmount + Integer.parseInt(serviceType.getFixedCost());

        tvTotal.setText(Html.fromHtml("<b><big>" + getString(R.string.total) + " " + totalAmount + "</big><small>" + getString(R.string.qar) + "</small></b>"));
        GlobalVariable.totalServicesAmount = totalAmount;
    }

    public boolean checkAddView() {
        if (llAdableSedun.getChildCount() == 0 && llAdableSuv.getChildCount() == 0) {
            return false;
        }
        return true;
    }

}
