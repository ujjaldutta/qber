package com.uipl.qber;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.Preferences;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by richa on 15/12/16.
 */
public class FirebaseIDService extends FirebaseInstanceIdService {

    private static final String TAG = "FirebaseIDService";

    RequestQueue mQueue;
    Preferences pref;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        GlobalVariable.FIREBASE_TOKEN = refreshedToken;
        Log.e(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        mQueue = Volley.newRequestQueue(this);
        pref = new Preferences(this);
        String phone = pref.getStringPreference(this, Preferences.PHONE);
        String loginToken = pref.getStringPreference(this, Preferences.TOKEN);
        if (phone != null && !phone.toString().trim().equals("")) {
            volleyUpdateDeviceToken(phone,loginToken, token);
        }
    }

    private void volleyUpdateDeviceToken(final String mobile, final String token,final String deviceId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_DEVICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, response);
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                }
                            } else {
                                Log.d(TAG, "response null");
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error instanceof NoConnectionError) {
                    Log.e(TAG, "NoConnectionError");
                } else if (error instanceof com.android.volley.TimeoutError) {
                    Log.e(TAG, "com.android.volley.TimeoutError");
                } else {
                    Log.e(TAG, "onErrorResponse : else");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token",  token);
                params.put("device_id", deviceId);
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }
}
