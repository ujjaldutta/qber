package com.uipl.qber;

/**
 * Created by pallab on 9/1/17.
 */

public interface ServiceStatusChange {
    void onServicesStatusChange(String id, String type, String providerName, String msg);
}
