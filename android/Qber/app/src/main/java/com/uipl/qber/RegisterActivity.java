package com.uipl.qber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.GPSTracker;
import com.uipl.qber.utils.InputVadidation;
import com.uipl.qber.utils.Permission;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.SnackBarCustom;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Activity to display registration details
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etMobile,etName,etPassword,et_cnfmPassword;
    private Switch switch_compat;
    private Button btnRegister;
    private DialogView dialogview;
    private Preferences pref;
    private RequestQueue mQueue;
    private String TAG="RegisterActivity";
    private TextView tvCountryCode,term_con;
    private ImageButton imgBtnBack;
    private GPSTracker gpsTracker;
    double latitude=0.0,longitude=0.0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        dialogview = new DialogView(RegisterActivity.this);
        pref = new Preferences(RegisterActivity.this);

        mQueue = Volley.newRequestQueue(this);

        PermissionChecking();
        initView();
    }

    /**
     * Method to initialize variable
     */
    private void initView() {
        imgBtnBack=(ImageButton)findViewById(R.id.imgBtnBack);
        imgBtnBack.setOnClickListener(this);
        tvCountryCode=(TextView)findViewById(R.id.tvCountryCode);
        tvCountryCode.setText("+"+GlobalVariable.ISD_CODE);
        etMobile=(EditText)findViewById(R.id.etMobile);
        etName=(EditText)findViewById(R.id.etName);
        etPassword=(EditText)findViewById(R.id.etPassword);
        et_cnfmPassword=(EditText)findViewById(R.id.et_cnfmPassword);
        switch_compat=(Switch)findViewById(R.id.switch_compat);
        btnRegister=(Button)findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        term_con=(TextView)findViewById(R.id.term_con);
        term_con.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnRegister:
                if(Validation()){
                    if(latitude!=0.0 && longitude!=0.0) {
                        volleyServiceRegister();
                    }else {
                        showCustomDialog(RegisterActivity.this,getResources().getString(R.string.sorry),"Unable to fetch location, Press ok to retry");
                    }
                }
                break;

            case R.id.imgBtnBack:
                onBackPressed();
                break;
            case R.id.term_con:
                Toast.makeText(RegisterActivity.this,"Coming soon",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * @return true if all data are valid
     * Method to check validation
     */
    public boolean Validation(){

        if (etMobile.getText().toString().trim().isEmpty()) {
            etMobile.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etMobile, getResources().getString(R.string.please_enter_mobile_number));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }else  if (etMobile.getText().toString().length()!=8) {
            etMobile.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etMobile,  getResources().getString(R.string.phone_should_have_8_numbers));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }
        else  if (etName.getText().toString().trim().isEmpty()) {
            etName.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etName,  getResources().getString(R.string.please_enter_name));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etName, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else  if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etPassword,  getResources().getString(R.string.please_enter_password));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }else  if (etPassword.getText().toString().length()<8) {
            etPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etPassword,  getResources().getString(R.string.password_length_should_be_minimum_8_characters));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }
        else if (et_cnfmPassword.getText().toString().trim().isEmpty()) {
            et_cnfmPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(et_cnfmPassword,  getResources().getString(R.string.please_enter_confirm_password));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et_cnfmPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }
       else if (!InputVadidation.isPasswordMatches(etPassword,et_cnfmPassword)) {
            et_cnfmPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(et_cnfmPassword,  getResources().getString(R.string.password_mismatch));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et_cnfmPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }else if (!switch_compat.isChecked()) {
            SnackBarCustom.showSnackWithActionButton(et_cnfmPassword,  getResources().getString(R.string.accept_terms_and_conditions));

            return false;
        }


        return true;
    }

    /**
     * Method to call web service to register a user
     */
    private void volleyServiceRegister() {
        dialogview.showCustomSpinProgress(RegisterActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SIGNUP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject dataObj = object.optJSONObject("data");
                                    pref.storeStringPreference(RegisterActivity.this, Preferences.USER_ID, dataObj.optString("id"));
                                    startActivity(new Intent(RegisterActivity.this, MobileVerificationActivity.class).putExtra("PHONE",etMobile.getText().toString().trim()));
                                    finish();
                                }else  {
                                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone",GlobalVariable.ISD_CODE+etMobile.getText().toString().trim());
                params.put("name",etName.getText().toString().trim());
                params.put("password",etPassword.getText().toString().trim());
                params.put("latitude",String.valueOf(latitude));
                params.put("longitude",String.valueOf(longitude));
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }


    /**
     * Method to get current latitude and longitude
     */
    public void getLocation() {
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
                 latitude = gpsTracker.getLatitude();
                 longitude = gpsTracker.getLongitude();


        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(RegisterActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
           getLocation();
        } else {
            ActivityCompat.requestPermissions(RegisterActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    dialogview.showToast(RegisterActivity.this, getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }

    public void showCustomDialog(Context context, String header,
                                 String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                PermissionChecking();


            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

}
