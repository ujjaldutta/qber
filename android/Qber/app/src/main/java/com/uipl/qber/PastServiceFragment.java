package com.uipl.qber;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.uipl.qber.Adapter.PastServiceAdapter;

import com.uipl.qber.Model.PastService;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PastServiceFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, VolleyInterface {

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    TextView tvEmptyView;
    SwipeRefreshLayout swipeRefreshLayout;
    private DialogView dialogView;
    ArrayList<PastService> pastServiceArrayList = new ArrayList<>();
    PastServiceAdapter adapter;
    RequestQueue mQueue;
    final static String TAG = "PastServiceFragment";
    private CustomVolley customVolley;
    private Preferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_past_service, container, false);
        ((MenuActivity) getActivity()).tvTitle.setText("Past Appointment");
        dialogView = new DialogView(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());
        initViews(v);
        volleyPastListService();
        return v;
    }

    private void checkAdapterIsEmpty() {
        if (adapter.getItemCount() == 0) {
            tvEmptyView.setVisibility(View.VISIBLE);
        } else {
            tvEmptyView.setVisibility(View.GONE);
        }
    }

    void initViews(View view) {
        mQueue = Volley.newRequestQueue(getActivity());
        customVolley = new CustomVolley(getActivity());
        pref = new Preferences(getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srlPastService);
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setRefreshing(false);
        tvEmptyView = (TextView) view.findViewById(R.id.tvEmptyView);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvPastService);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new PastServiceAdapter(getActivity(), pastServiceArrayList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onRefresh() {
        tvEmptyView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (customVolley != null)
            customVolley.cancelRequest(ConFig_URL.URL_LIST_SERVICE);
    }

    private void volleyPastListService() {
        HashMap<String, String> params = getPastServiceParams();
        customVolley.ServiceRequest(this, ConFig_URL.URL_LIST_SERVICE, params);
    }

    private HashMap<String, String> getPastServiceParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
        params.put("type", "past");
        Log.d(TAG, params.toString());
        return params;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        if (Type.equals(ConFig_URL.URL_LIST_SERVICE)) {
            try {
                System.out.println("result = " + response);
                JSONObject object = new JSONObject(response);
                pastServiceArrayList.clear();

                if (object != null) {
                    if (object.optString("success").toString().trim().equals("0")) {
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataArrayObj = dataArray.getJSONObject(i);

                            JSONArray serviceArray = dataArrayObj.optJSONArray("service");
                            for (int j = 0; j < serviceArray.length(); j++) {
                                JSONObject serviceArrayObj = serviceArray.getJSONObject(j);
                                PastService pastService = new PastService();

                                JSONArray serviceTypeArray = serviceArrayObj.getJSONArray("service_type");
                                for (int k = 0; k < serviceTypeArray.length(); k++) {
                                    JSONObject serviceTypeObj = serviceTypeArray.getJSONObject(k);
                                    pastService.setServiceName(serviceTypeObj.optString("name"));
                                    pastService.setServiceCode(serviceArrayObj.optString("expected_cost"));
                                    pastService.setArrivalDate(DateFormatter.getFormattedTime("yyyy-MM-dd", "MMM dd, yyyy", serviceArrayObj.optString("bookdate")));
                                    pastService.setArrivalTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "h:mm a", serviceArrayObj.optString("from_time")));
                                    pastService.setDepartTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "h:mm a", serviceArrayObj.optString("to_time")));
                                    pastService.setRating(serviceArrayObj.optString("rating"));
                                    pastService.setAlreadyRated(serviceArrayObj.optString("already_rated"));
                                    pastService.setStatusId(serviceArrayObj.optString("service_status_id"));
                                    pastService.setServiceId(serviceArrayObj.optString("id"));
                                    JSONObject providerObj = serviceArrayObj.getJSONObject("provider");
                                    if (providerObj != null) {
                                        pastService.setName(providerObj.optString("name"));
                                        pastService.setPic(providerObj.optString("image"));

                                    }
                                    pastServiceArrayList.add(pastService);
                                    adapter.notifyDataSetChanged();
                                    checkAdapterIsEmpty();
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                dialogView.showCustomSingleButtonDialog(getActivity(),
                        getActivity().getResources().getString(R.string.sorry), getActivity().getResources().getString(R.string.internal_error));
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK && data != null) {
            String serviceId = data.getStringExtra("service_id");
            if (serviceId != null && !serviceId.equals("")) {
                RefreshList(serviceId);
            }


        }
    }

    private void RefreshList(String serviceId) {
        if (pastServiceArrayList.size() != 0 && adapter != null) {
            for (int i = 0; i < pastServiceArrayList.size(); i++) {
                if (pastServiceArrayList.get(i).getServiceId().equals(serviceId)) {
                    pastServiceArrayList.get(i).setAlreadyRated("1");
                    break;
                }
            }
            adapter.notifyDataSetChanged();
        }
    }
}