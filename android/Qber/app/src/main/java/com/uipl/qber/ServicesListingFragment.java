package com.uipl.qber;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.Model.SubServiceType;
import com.uipl.qber.base.ResetGlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Permission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Fragment to dispaly service list
 */
public class ServicesListingFragment extends Fragment implements View.OnClickListener {


    private DialogView dialogView;
    private TabsPagerAdapter mAdapter;
    private ViewPager viewPager;
    private Button btnChoose;
    private ImageView imgTab1,imgTab2,imgTab3;
    private DialogView dialogview;
    private RequestQueue mQueue;
    public static String TAG="ServicesListingFragment";

    public ServicesListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_services_listingfragment, container, false);
        ((MenuActivity)getActivity()).tvTitle.setText(getString(R.string.services));
        initViews(view);
        dialogView=new DialogView(getActivity());
        viewPager=(ViewPager)view.findViewById(R.id.pager);
        new setAdapterTask().execute();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                if (position == 0) {

                    imgTab1.setImageResource(R.drawable.ic_service1a);
                    imgTab2.setImageResource(R.drawable.ic_service2);
                    imgTab3.setImageResource(R.drawable.ic_service3);


                } else if (position == 1) {

                    imgTab1.setImageResource(R.drawable.ic_service1);
                    imgTab2.setImageResource(R.drawable.ic_service2a);
                    imgTab3.setImageResource(R.drawable.ic_service3);

                }else if(position==2){

                    imgTab1.setImageResource(R.drawable.ic_service1);
                    imgTab2.setImageResource(R.drawable.ic_service2);
                    imgTab3.setImageResource(R.drawable.ic_service3a);

                }
                /*check service active or not*/
                if(GlobalVariable.serviceTypesArray.size()>position){
                    ServiceType serviceType=GlobalVariable.serviceTypesArray.get(position);
                    if(serviceType.isActiveStatus()){
                        btnChoose.setEnabled(true);
                        btnChoose.setBackgroundColor(getActivity().getResources().getColor(R.color.purple));
                    }else {
                        btnChoose.setEnabled(false);
                        btnChoose.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
                    }
                }else {
                    btnChoose.setEnabled(false);
                    btnChoose.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        return view;
    }

    /**
     * @param view
     * Method to initialize variable
     */
    private void initViews(View view) {

        dialogview=new DialogView(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());

        btnChoose=(Button)view.findViewById(R.id.btnChoose);
        btnChoose.setEnabled(false);
        btnChoose.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        btnChoose.setOnClickListener(this);


        imgTab1=(ImageView)view.findViewById(R.id.imgTab1);
        imgTab2=(ImageView)view.findViewById(R.id.imgTab2);
        imgTab3=(ImageView)view.findViewById(R.id.imgTab3);
        imgTab1.setOnClickListener(this);
        imgTab2.setOnClickListener(this);
        imgTab3.setOnClickListener(this);
        ActiveStatusCheck();

    }

    /**
     * Method to check service activation status
     */
    public void ActiveStatusCheck(){
        if(GlobalVariable.serviceTypesArray.size()>0){
            ServiceType serviceType=GlobalVariable.serviceTypesArray.get(0);
            if(serviceType.isActiveStatus()){
                btnChoose.setEnabled(true);
                btnChoose.setBackgroundColor(getActivity().getResources().getColor(R.color.purple));
            }else {
                btnChoose.setEnabled(false);
                btnChoose.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(mAdapter!=null && viewPager!=null){
            new setAdapterTask().execute();

        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnChoose:

                checkPermissionForLocation();
                break;

            case R.id.imgTab1:
                viewPager.setCurrentItem(0);
                imgTab1.setImageResource(R.drawable.ic_service1a);
                imgTab2.setImageResource(R.drawable.ic_service2);
                imgTab3.setImageResource(R.drawable.ic_service3);
                break;
            case R.id.imgTab2:
                viewPager.setCurrentItem(1);
                imgTab1.setImageResource(R.drawable.ic_service1);
                imgTab2.setImageResource(R.drawable.ic_service2a);
                imgTab3.setImageResource(R.drawable.ic_service3);
                break;
            case R.id.imgTab3:
                viewPager.setCurrentItem(2);
                imgTab1.setImageResource(R.drawable.ic_service1);
                imgTab2.setImageResource(R.drawable.ic_service2);
                imgTab3.setImageResource(R.drawable.ic_service3a);
                break;
        }
    }



    /**
     * Method to check permission for location
     */
    private void checkPermissionForLocation() {
        if (Permission.selfPermissionGranted(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)&& Permission.selfPermissionGranted(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION)) {
            volleyTypeTask();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.location_permission), Toast.LENGTH_SHORT).show();
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d("Phone call","allow");
            volleyTypeTask();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.location_permission), Toast.LENGTH_SHORT).show();
        }
        return;

    }

    /**
     * Method to call web services for service type
     */
    private void volleyTypeTask() {
         dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray=object.optJSONArray("data");
                                    if(jsonArray.length()>0){
                                        JSONObject serviceObj = jsonArray.optJSONObject(0);
                                        ServiceType serviceType = new ServiceType();

                                        serviceType.setServiceId(serviceObj.optString("id"));
                                        serviceType.setImageUrl(serviceObj.optString("image"));
                                        serviceType.setDisplayText(serviceObj.optString("display_text"));
                                        if(serviceObj.optString("is_active").equals("1")) {
                                            serviceType.setActiveStatus(true);
                                        }else {
                                            serviceType.setActiveStatus(false);
                                        }
                                        serviceType.setServiceName(serviceObj.optString("name"));
                                        if(serviceObj.has("servicesetting")&& serviceObj.get("servicesetting") instanceof JSONObject)
                                        {
                                            JSONObject serviceSettingObj = serviceObj.optJSONObject("servicesetting");
                                            serviceType.setFixedCost(serviceSettingObj.optString("fixed_cost"));
                                            serviceType.setArrivalDuration(serviceSettingObj.optString("arrival_duration"));
                                            serviceType.setAllowedStartTime(serviceSettingObj.optString("allowed_start_time"));
                                            serviceType.setAllowedEndTime(serviceSettingObj.optString("allowed_end_time"));
                                            serviceType.setMinCost(serviceSettingObj.optString("min_cost"));
                                        }

                                        JSONArray optionArray = serviceObj.optJSONArray("options");

                                        ArrayList<SubServiceType> subServiceTypesArray = new ArrayList<>();
                                        if(optionArray!=null) {
                                            for (int j = 0; j < optionArray.length(); j++) {
                                                JSONObject subServiceObj = optionArray.optJSONObject(j);
                                                SubServiceType subServiceType = new SubServiceType();
                                                subServiceType.setName(subServiceObj.optString("name"));
                                                JSONArray optDetailsArray = subServiceObj.optJSONArray("option_details");
                                                ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                                                for (int k = 0; k < optDetailsArray.length(); k++) {
                                                    JSONObject optDetailsObj = optDetailsArray.optJSONObject(k);
                                                    HashMap<String, String> hashMap = new HashMap<>();
                                                    hashMap.put("sub_service_id", optDetailsObj.optString("id"));
                                                    hashMap.put("name", optDetailsObj.optString("name"));
                                                    hashMap.put("cost", optDetailsObj.optString("cost"));
                                                    optDetails.add(hashMap);
                                                }
                                                subServiceType.setOptionDetailsArray(optDetails);
                                                subServiceTypesArray.add(subServiceType);

                                            }
                                        }

                                        serviceType.setsubServicesArray(subServiceTypesArray);
                                        GlobalVariable.serviceType=serviceType;
                                        if(!getActivity().isFinishing()) {
                                            volleyNoticeTask();
                                        }
                                    }else {
                                        dialogview.dismissCustomSpinProgress();
                                        dialogview.showCustomSingleButtonDialog(getActivity(),
                                                getResources().getString(R.string.sorry), object.optString("msg"));
                                    }



                                }else {
                                    dialogview.dismissCustomSpinProgress();
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }


                        } catch (JSONException e) {
                             dialogview.dismissCustomSpinProgress();
                            // TODO Auto-generated catch block
                           dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry),getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type_id", "1");
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * Method to call web services for services notice
     */
    private void volleyNoticeTask() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_NOTICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                 GlobalVariable.notice=object.optString("notice");
                                   int currentPosition=viewPager.getCurrentItem();
                                    ResetGlobalVariable.BookedService();
                                    if(currentPosition==0) {
                                        startActivity(new Intent(getActivity(), BookedActivity.class));
                                    }else {
                                        Toast.makeText(getActivity(),"Coming soon",Toast.LENGTH_SHORT).show();
                                    }

                                }else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry),getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("id", "1");
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * AsyncTask to set viewPager adapter
     */
    private class setAdapterTask extends AsyncTask<Void,Void,Void> {
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mAdapter = new TabsPagerAdapter(getActivity().getSupportFragmentManager());
            viewPager.setAdapter(mAdapter);
            if(imgTab1!=null&& imgTab2!=null && imgTab3!=null) {
                imgTab1.setImageResource(R.drawable.ic_service1a);
                imgTab2.setImageResource(R.drawable.ic_service2);
                imgTab3.setImageResource(R.drawable.ic_service3);
            }
            if (btnChoose!=null){
                ActiveStatusCheck();
            }
            mAdapter.notifyDataSetChanged();
        }
    }


    /**
     * FragmentStatePagerAdapter for customize ViewPager Adapter
     */
    /*..................*/
    public class TabsPagerAdapter extends FragmentStatePagerAdapter {

        public TabsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int index) {

            switch (index) {
                case 0:
                    ViewPagerServiceFragment fragment1=ViewPagerServiceFragment.newInstance(index);
                    return fragment1;

                case 1:
                    ViewPagerServiceFragment fragment2=ViewPagerServiceFragment.newInstance(index);
                    return fragment2;

                case 2:
                    ViewPagerServiceFragment fragment3=ViewPagerServiceFragment.newInstance(index);
                    return fragment3;
            }

            return null;
        }

        @Override
        public int getCount() {
            // get item count - equal to number of tabs
            return 3;
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity()!=null){
            ((MenuActivity)getActivity()).tvTitle.setText(getString(R.string.menu));
        }
    }


}
