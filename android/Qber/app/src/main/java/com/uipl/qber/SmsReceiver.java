package com.uipl.qber;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.uipl.qber.base.GlobalVariable;

/**
 * Created by pallab on 4/10/16.
 * BroadcastReceiver to get SMS
 */

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.containsKey("pdus")) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdus[0]);
            String senderNumber = sms.getOriginatingAddress();
            String message = sms.getMessageBody();
            try
            {
                if (senderNumber .contains("QBER")|| message.contains("Qber Code:"))
                {
                    GlobalVariable.SMS_CODE=message.substring(message.lastIndexOf(":")+1);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
