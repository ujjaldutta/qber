package com.uipl.qber;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.DirectionsJSONParser;
import com.uipl.qber.utils.GPSTracker;
import com.uipl.qber.utils.LocationChangeInterface;
import com.uipl.qber.utils.Permission;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.PushHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrackLocationActivity extends FragmentActivity implements OnMapReadyCallback, LocationChangeInterface {

    private GoogleMap mMap;
    private RequestQueue mQueue;
    private DialogView dialogView;
    private String TAG = "TrackLocationActivity";
    private String providerName="";
    private String serviceId="";
    private GPSTracker gpsTracker;
    ParserTask parserTask;
    Marker m1, m2;
    LatLng dest, origin;
    List<Marker> markersList = new ArrayList<>();
    LatLngBounds.Builder builder;
    CameraUpdate cu;
    DownloadTask downloadTask;
    boolean pathDrawFlag=false;
    private Marker destMarker;
    private ImageButton imgBtnBack;
    private String routMode="driving";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_location);
        if(getIntent().hasExtra("provider_name")){
            providerName=getIntent().getStringExtra("provider_name");
        } if(getIntent().hasExtra("service_id")){
            serviceId=getIntent().getStringExtra("service_id");
        }

        initView();
        gpsTracker = new GPSTracker(this);
        PushHandler pushHandler=new PushHandler();
        pushHandler.setOnLocationChange(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
       /* CustomFirebaseMessagingService customFirebaseMessagingService = new CustomFirebaseMessagingService();
        customFirebaseMessagingService.onLocationChangeListener(this);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        GlobalVariable.TrackLocationActvityFlag=true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        GlobalVariable.TrackLocationActvityFlag=false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(downloadTask!=null){
            downloadTask.cancel(true);
        }
        if(parserTask!=null){
            parserTask.cancel(true);
        }
    }

    private void initView() {
        dialogView = new DialogView(this);
        mQueue = Volley.newRequestQueue(this);
        dialogView.showCustomSpinProgress(TrackLocationActivity.this);
        imgBtnBack=(ImageButton)findViewById(R.id.imgBtnBack);
        Toast.makeText(TrackLocationActivity.this,getResources().getString(R.string.fetching_provider),Toast.LENGTH_LONG).show();
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
       /* LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
        PermissionChecking();
        getLocation();
    }

    @Override
    public void onLocationChange(final Map<String, String> data) {
//        {customer_name=udddddd dbhjubfushdbg, latitude=22.5771526, provider_name=ujjal, type=livetrack, longitude=88.4321345}
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String provider_name = data.get("provider_name");
                String latitude = data.get("latitude");
                String longitude = data.get("longitude");
                String id = data.get("id");
                String transportMode=data.get("transport");

                if (provider_name == null || provider_name.equals(providerName) && id.equals(serviceId)  ) {
                    dialogView.dismissCustomSpinProgress();

                    if (mMap != null && latitude != null && !latitude.equals("") && longitude != null && !longitude.equals("")) {
                       // mMap.clear();
                        LatLng newLatLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                        dest=newLatLng;
                        if (!pathDrawFlag && origin != null && dest != null) {
                            if(transportMode!=null && !transportMode.equals("") && !transportMode.equals("null")){
                                if(transportMode.toLowerCase().equals("walk")){
                                    routMode="walking";
                                }else {
                                    routMode="driving";
                                }
                            }
                       destMarker= mMap.addMarker(new MarkerOptions().position(newLatLng).title(provider_name).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map1)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(newLatLng));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newLatLng, 12.0f));

                            pathDrawFlag=true;
                            String url = getDirectionsUrl(origin, dest);
                            if (downloadTask != null && downloadTask.getStatus() != AsyncTask.Status.FINISHED) {
                                downloadTask.cancel(true);
                            }
                            downloadTask = new DownloadTask();
                            downloadTask.execute(url);
                        }else if(destMarker!=null) {
                            destMarker.setPosition(newLatLng);
                        }
                    }
                }


            }
        });


    }



    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(TrackLocationActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            ActivityCompat.requestPermissions(TrackLocationActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    dialogView.showToast(TrackLocationActivity.this, getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }

    /**
     * Method to get current latitude and longitude
     */
    public void getLocation() {
        if (gpsTracker.canGetLocation()) {

                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();
                mMap.clear();
                LatLng latLng = new LatLng(latitude, longitude);
                origin=latLng;
               // Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map1)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));

                System.out.println("latitude = " + latitude);
                System.out.println("longitude = " + longitude);

        } else {
            gpsTracker.showSettingsAlert();
        }
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (parserTask != null && parserTask.getStatus() != AsyncTask.Status.FINISHED) {
                parserTask.cancel(true);
            }
            parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            if (result != null) {
                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = result.get(i);
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(getResources().getColor(R.color.route_color));
                }
                if (result.size() < 1) {
                    dialogView.dismissCustomSpinProgress();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.unable_to_draw_path), Toast.LENGTH_LONG).show();
                    if (mMap != null && origin != null) {
                        mMap.addMarker(new MarkerOptions().position(origin).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

                    }
                    if (mMap != null && dest != null) {
                        mMap.addMarker(new MarkerOptions().position(dest).title(""));
                    }
                    return;
                }

                dialogView.dismissCustomSpinProgress();
                drawPolyLine(lineOptions);

            } else {
                dialogView.dismissCustomSpinProgress();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.unable_to_draw_path), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void drawPolyLine(PolylineOptions lineOptions) {
        m1 = mMap.addMarker(new MarkerOptions().position(origin).title(""));
        m2 = mMap.addMarker(new MarkerOptions().position(dest).title(""));

        markersList.add(m1);
        markersList.add(m2);
        m1.setVisible(false);
        m2.setVisible(false);
        mMap.addPolyline(lineOptions);
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }
        /**initialize the padding for map boundary*/
        int padding = 40;
        LatLngBounds bounds = builder.build();
        /**create the camera with bounds and padding to set into map*/
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.animateCamera(cu);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
        });
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
       // String mode = "mode=walking";
        String mode = "mode="+routMode;
        String parameters = str_origin + "&" + str_dest + "&" + mode + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

}
