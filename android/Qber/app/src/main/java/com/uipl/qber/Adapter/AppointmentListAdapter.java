package com.uipl.qber.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.AppointmentDetailsActivity;
import com.uipl.qber.Model.BookedAppointment;
import com.uipl.qber.R;
import com.uipl.qber.TrackLocationActivity;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;

import java.util.ArrayList;

/**
 * Created by Pallab on 9/23/2016.
 */
public class AppointmentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<BookedAppointment> bookedAppointList = new ArrayList<>();
    Activity context;
    ImageLoader imageLoader;
    DialogView dialogView;
    private String TAG = "AppointmentDetails";
    DisplayImageOptions options;
    RequestQueue mQueue;

    public AppointmentListAdapter(Activity mContext, ArrayList<BookedAppointment> mArrayList) {
        this.context = mContext;
        this.bookedAppointList = mArrayList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.profile_placeholder)
                // .showImageForEmptyUri(R.drawable.profile_placeholder)
                //.showImageOnFail(R.drawable.profile_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public int getItemCount() {
        return bookedAppointList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        viewHolder = new AppointmentHolder(inflater.inflate(R.layout.inflate_booked_appointment, viewGroup, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        final BookedAppointment appointment = (BookedAppointment) bookedAppointList.get(position);

        AppointmentHolder holder = (AppointmentHolder) viewHolder;
//        holder.tvName.setText("Raju Team");
        final String serviceid = appointment.getServiceId();
        Log.e("tag1", serviceid);

        imageLoader.displayImage(ConFig_URL.IMAGE_URL + appointment.getPic(), holder.ivPic, options);
        imageLoader.displayImage(appointment.getMapPic(), holder.ivMapImage, options);
        holder.tvName.setText(appointment.getName());
        holder.tvServiceName.setText(appointment.getServiceName());
//       holder.tvServiceCode.setText(appointment.getServiceCode());
        holder.tvServiceCode.setText((Html.fromHtml("<b><big>" + appointment.getServiceCode() + "</big><font><small>" + context.getString(R.string.qar) + "</small></font></b>")));
        holder.tvStatus.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        holder.tvStatus.setText(appointment.getStatus());
        holder.tvArrivalTime.setText(appointment.getArrivalTime().toUpperCase());
        holder.tvArrivalDate.setText(appointment.getArrivalDate());
        holder.btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalVariable.EditAppointmentFlag = false;
                Intent intent = new Intent(context, AppointmentDetailsActivity.class);
                intent.putExtra("service_id", appointment.getServiceId());
                context.startActivity(intent);

            }
        });
        if (appointment.getStatus().toLowerCase().equals("heading")) {
            holder.btnTrackLive.setEnabled(true);
            holder.btnTrackLive.setBackgroundResource(R.drawable.rounded_corner_purple);
            holder.btnTrackLive.setTextColor(context.getResources().getColor(R.color.purple));
        } else {
            holder.btnTrackLive.setEnabled(false);
            holder.btnTrackLive.setBackgroundResource(R.drawable.rounded_corner_grey);
            holder.btnTrackLive.setTextColor(context.getResources().getColor(R.color.black));
        }
        holder.btnTrackLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, TrackLocationActivity.class)
                        .putExtra("provider_name", appointment.getName()).putExtra("service_id", appointment.getServiceId()));
            }
        });


    }

    class AppointmentHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvServiceName, tvServiceCode, tvArrivalDate, tvArrivalTime, tvStatus;
        ImageView ivPic, ivMapImage;
        Button btnTrackLive, btnDetails;
        CardView llMain;

        public AppointmentHolder(View v) {
            super(v);
            ivPic = (ImageView) v.findViewById(R.id.ivPic);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvName.setTypeface(Typeface.DEFAULT_BOLD);
            tvServiceName = (TextView) v.findViewById(R.id.tvServiceName);
            tvServiceCode = (TextView) v.findViewById(R.id.tvServiceCode);
            tvServiceCode.setTypeface(Typeface.DEFAULT_BOLD);
            tvArrivalDate = (TextView) v.findViewById(R.id.tvArrivalDate);
            tvArrivalDate.setTypeface(Typeface.DEFAULT_BOLD);
            tvArrivalTime = (TextView) v.findViewById(R.id.tvArrivalTime);
            tvArrivalTime.setTypeface(Typeface.DEFAULT_BOLD);
            tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            tvStatus.setTypeface(Typeface.DEFAULT_BOLD);
            ivMapImage = (ImageView) v.findViewById(R.id.ivMapImage);
            btnTrackLive = (Button) v.findViewById(R.id.btnTrackLive);
            btnDetails = (Button) v.findViewById(R.id.btnDetails);
            llMain = (CardView) v.findViewById(R.id.cardView);
        }
    }

}

