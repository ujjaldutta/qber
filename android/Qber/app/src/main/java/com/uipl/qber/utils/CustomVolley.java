package com.uipl.qber.utils;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.R;
import com.uipl.qber.RegisterLoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pallab on 1/12/16.
 */

public class CustomVolley {
    DialogView dialogView;
    Activity mActivity;
    private String TAG = "CustomVolley";
    private Preferences pref;
    private RequestQueue mQueue;
    VolleyInterface volleyInterface;
    String SERVICE_URL = "";
    HashMap<String, String> params=new HashMap<>();

    public CustomVolley(Activity activity) {
        this.mActivity = activity;
        pref = new Preferences(activity);
        mQueue = Volley.newRequestQueue(activity);
        dialogView = new DialogView(activity);
    }

    public void ServiceRequest(VolleyInterface volleyInterface, String Url, HashMap<String, String> params) {
        this.volleyInterface = volleyInterface;
        this.SERVICE_URL = Url;
        this.params = params;
        ServiceCall();
    }

    private void ServiceCall() {
        dialogView.showCustomSpinProgress(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVICE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    mActivity.startActivity(new Intent(mActivity, RegisterLoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else if (object.optString("success").equals("0")) {
                                    volleyInterface.VolleyResponse(response, SERVICE_URL);
                                } else {
                                    dialogView.showCustomSingleButtonDialog(mActivity,
                                            mActivity.getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(mActivity,
                                    mActivity.getResources().getString(R.string.sorry), mActivity.getResources().getString(R.string.internal_error));

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(mActivity,
                            mActivity.getResources().getString(R.string.sorry), mActivity.getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(mActivity,
                            mActivity.getResources().getString(R.string.sorry), mActivity.getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(mActivity,
                            mActivity.getResources().getString(R.string.sorry), mActivity.getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> mParams = new HashMap<>();
                if(!params.containsKey("phone")){
                   params.put("phone",pref.getStringPreference(mActivity,Preferences.PHONE));
                }
                if(!params.containsKey("token")){
                   params.put("token",pref.getStringPreference(mActivity,Preferences.TOKEN));
                }
                mParams=params;

                return mParams;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(SERVICE_URL);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    public void cancelRequest(String TAG){
        if (mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }


}
