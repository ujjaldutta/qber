package com.uipl.qber.utils;

import com.uipl.qber.ServiceStatusChange;
import com.uipl.qber.base.GlobalVariable;

import java.util.Map;

/**
 * Created by pallab on 9/1/17.
 */

public  class PushHandler {
    private static ServiceStatusChange serviceStatusChange=null;
    private static LocationChangeInterface locationChangeInterface=null;

    public static void onServiceStatusChange(String id, String type, String providerName,String msg){
        if(serviceStatusChange!=null && GlobalVariable.ProviderActivityFlag){
            serviceStatusChange.onServicesStatusChange(id,type,providerName,msg);
        }
    }public static void onLocationChange(Map<String, String> data){
        if(locationChangeInterface!=null && GlobalVariable.TrackLocationActvityFlag){
            locationChangeInterface.onLocationChange(data);
        }
    }
    public void setOnServicesStatusChange(ServiceStatusChange servicesStatus){
        this.serviceStatusChange=servicesStatus;
    }
    public void setOnLocationChange(LocationChangeInterface locationChangeInterface){
        this.locationChangeInterface=locationChangeInterface;
    }
}
