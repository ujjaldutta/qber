package com.uipl.qber;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.InputVadidation;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.SnackBarCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Activity for verify mobile number
 */
public class MobileVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvResendOTP;
    private Button btnVerifyCode;
    private EditText etOTP;
    private String smsCode="";
    private DialogView dialogview;
    private Preferences pref;
    private String TAG="MobileVerification";
    private RequestQueue mQueue;
    private String phone;
    private CountDownTimer timer;
    private ImageButton imgBtnBack;
    private View viewUnderline;
    private String FireBaseKey="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        if(getIntent().hasExtra("PHONE")){
            phone=getIntent().getExtras().getString("PHONE");
        }
        initView();
        dialogview = new DialogView(MobileVerificationActivity.this);
        dialogview.showCustomSpinProgress(this);
        new getFirebaseToken().execute();

    }


    /**
     * Method to check verify code
     */
    public void checkVerifyCode(){
        tvResendOTP.setEnabled(false);
        tvResendOTP.setAllCaps(false);
        timer= new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                tvResendOTP.setText(getString(R.string.waiting_smg)+ millisUntilFinished / 1000);
                viewUnderline.setVisibility(View.GONE);

                if (!GlobalVariable.SMS_CODE.equals("") ){
                    etOTP.setText(GlobalVariable.SMS_CODE);
                    tvResendOTP.setEnabled(true);
                    tvResendOTP.setText(getString(R.string.resend_verification_code));
                    viewUnderline.setVisibility(View.VISIBLE);
                    cancel();
                }
            }

            public void onFinish() {
                tvResendOTP.setEnabled(true);
                tvResendOTP.setText(getString(R.string.resend_verification_code));
                viewUnderline.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    /**
     * Method to initialize variable
     */
    private void initView() {

        pref = new Preferences(MobileVerificationActivity.this);
        mQueue = Volley.newRequestQueue(this);
        etOTP=(EditText)findViewById(R.id.etOTP);
        btnVerifyCode=(Button)findViewById(R.id.btnVerifyCode);
        imgBtnBack=(ImageButton)findViewById(R.id.imgBtnBack);
        imgBtnBack.setOnClickListener(this);
        tvResendOTP=(TextView)findViewById(R.id.tvResendOTP);
        viewUnderline=(View)findViewById(R.id.viewUnderline);
        btnVerifyCode.setOnClickListener(this);
        tvResendOTP.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnVerifyCode:
                if (!InputVadidation.isEditTextHasvalue(etOTP)) {
                    etOTP.requestFocus();
                    SnackBarCustom.showSnackWithActionButton(etOTP, getResources().getString(R.string.please_enter_OTP));
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etOTP, InputMethodManager.SHOW_IMPLICIT);
                }else {
                    volleyServiceMobileVerify();
                }
                break;
            case R.id.tvResendOTP:
               volleyServiceResendCode();
                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }

    /**
     * Method for call web services to verify mobile number
     */
    private void volleyServiceMobileVerify() {
        dialogview.showCustomSpinProgress(MobileVerificationActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SMS_VERIFY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject dataObj = object.optJSONObject("data");
                                    pref.storeStringPreference(MobileVerificationActivity.this, Preferences.USER_ID, dataObj.optString("id"));
                                    pref.storeStringPreference(MobileVerificationActivity.this, Preferences.PHONE,dataObj.optString("phone"));
                                    pref.storeStringPreference(MobileVerificationActivity.this, Preferences.TOKEN, dataObj.optString("token"));

                                    startActivity(new Intent(MobileVerificationActivity.this, MenuActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }else  {
                                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone",GlobalVariable.ISD_CODE+phone);
                params.put("code",etOTP.getText().toString().trim());
                params.put("device_id",FireBaseKey);
                params.put("device_type", "android");
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    /**
     * Method to call web services for resend mobile verification code
     */
    private void volleyServiceResendCode() {
        dialogview.showCustomSpinProgress(MobileVerificationActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_RESEND_VERIFY_CODE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("1")) {
                                   checkVerifyCode();
                                }else  {
                                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(MobileVerificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone",GlobalVariable.ISD_CODE+phone);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    class getFirebaseToken extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            return refreshedToken;
        }

        @Override
        protected void onPostExecute(String s) {
            dialogview.dismissCustomSpinProgress();
            if (s != null && !s.toString().equals("")) {
                FireBaseKey=s.toString();
            }
            checkVerifyCode();
            super.onPostExecute(s);
        }
    }
}
