package com.uipl.qber.Model;

/**
 * Created by richa on 17/10/16.
 */
public class Timeline {

    private String startTime;
    private String endTime;
    private boolean active;
    private float diffInHours;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public float getDiffInHours() {
        return diffInHours;
    }

    public void setDiffInHours(float diffInHours) {
        this.diffInHours = diffInHours;
    }
}
