package com.uipl.qber.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.uipl.qber.R;

/**
 * Created by abhijit on 19/9/16.
 */
public class SupportClass {

    Activity _activity;
    Context _context;


    //Constructor for Activity Call...
    public SupportClass(Activity activity) {
        this._activity = activity;


    }

    //Constructor for Context Call
    public SupportClass(Context context) {
        this._context = context;
    }


    public void intentActivityForward(Class<?> secondActivity) {
        Intent nextActivity = new Intent(_activity, secondActivity);
        _activity.startActivity(nextActivity);
        _activity.overridePendingTransition(R.anim.open_translate,
                R.anim.close_scale);


    }

    // intent will finish the calling activity...
    public void intentActivityForword_WithFinish(Class<?> secondActivity) {
        Intent nextActivity = new Intent(_activity, secondActivity);
        _activity.startActivity(nextActivity);
        _activity.overridePendingTransition(R.anim.open_translate,
                R.anim.close_scale);
        _activity.finish();
    }

    public void intentActivityBackword_WithFinish(Class<?> secondActivity) {
        Intent nextActivity = new Intent(_activity, secondActivity);

        _activity.startActivity(nextActivity);
        _activity.overridePendingTransition(R.anim.open_scale, R.anim.close_translate);
        _activity.finish();
    }


    public void intentActivityBackword_WithFinishAllActivity(Class<?> secondActivity) {
        Intent nextActivity = new Intent(_activity, secondActivity);
        nextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _activity.startActivity(nextActivity);
        _activity.overridePendingTransition(R.anim.open_scale, R.anim.close_translate);
        _activity.finish();
    }

    public void setBackgroundColor(String str, TextView txt) {

        if (str.equals("A") || str.equals("B") || str.equals("C") ) {
            // back groung Red
            txt.setBackgroundResource(R.drawable.red_circle);

        }
        if (str.equals("D") || str.equals("E") || str.equals("F")) {
            // back groung Yellow
            txt.setBackgroundResource(R.drawable.yellow_circle);

        }
        if (str.equals("G") || str.equals("H") || str.equals("I")) {
            // back groung Blue
            txt.setBackgroundResource(R.drawable.blue_circle);

        }

        if (str.equals("J") || str.equals("K") || str.equals("L")) {
            // back groung green
            txt.setBackgroundResource(R.drawable.green_circle);
        }

        if (str.equals("M") || str.equals("N") || str.equals("O")) {
            // back groung green
            txt.setBackgroundResource(R.drawable.red_circle);
        }

    }
}
