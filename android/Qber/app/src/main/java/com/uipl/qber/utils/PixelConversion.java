package com.uipl.qber.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by Ankan on 13-Oct-16.
 */
public class PixelConversion {
    public static int dpToPx(Context mContext, int dp) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

    public static int pxToDp(Context mContext, int px) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        return (int) ((px / displayMetrics.density) + 0.5);
    }
}
