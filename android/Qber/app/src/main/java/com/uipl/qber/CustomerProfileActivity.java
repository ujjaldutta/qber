package com.uipl.qber;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.Model.Reviews_Model;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.SupportClass;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by abhijit on 30/9/16.
 */
public class CustomerProfileActivity extends Activity implements View.OnClickListener, VolleyInterface {

    final String TAG = "CustomerProfileActivity";
    Button btn_assign_job;
    TextView tvProfileName, tvDesc, tvToolbarTitle, tvReviews, tvNoReview;
    ImageView ivProfile, ivBanner1, ivBanner2, ivBanner3;
    Toolbar toolbar;
    ImageButton ibBack;
    ScrollView svMain;
    LinearLayout llReviews;
    SupportClass supportclass;
    RequestQueue mQueue;
    DialogView dialogView;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    ArrayList<Reviews_Model> review_list = new ArrayList<Reviews_Model>();
    String phone = "", bannerImage1 = "", bannerImage2 = "", bannerImage3 = "", profileImage = "";
    private String provider_id = "";
    private Preferences pref;
    CustomVolley customVolley;
    private boolean asignJobFlag = false;
    private int currentPosition = -1;
    private String callFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);
        if (getIntent().hasExtra("phone")) {
            phone = getIntent().getStringExtra("phone");
        }
        if (getIntent().hasExtra("provider_id")) {
            provider_id = getIntent().getStringExtra("provider_id");
        }
        if (getIntent().hasExtra("assign_job_flag")) {
            asignJobFlag = getIntent().getBooleanExtra("assign_job_flag", false);
        }
        if (getIntent().hasExtra("currentPosition")) {
            currentPosition = getIntent().getIntExtra("currentPosition", -1);
        }
        if (getIntent().hasExtra("Call_From")) {
            callFrom = getIntent().getStringExtra("Call_From");
        }
        initViews();
        initToolbar();

        volleyGetProfile();

    }

    private void volleyGetProfile() {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        svMain.setVisibility(View.VISIBLE);
                        try {
                            Log.e(TAG, response);
                            JSONObject object = new JSONObject(response);
                            review_list.clear();
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject providerObj = object.optJSONObject("provider");
                                    String providerName=providerObj.optString("name");
                                    if(providerName!=null && !providerName.equals("null")) {
                                        tvProfileName.setText(providerName);
                                    }
                                    JSONObject profileObj = providerObj.optJSONObject("profile");
                                    String description=profileObj.optString("service_details");
                                    if(description!=null && !description.equals("null")) {
                                        tvDesc.setText(description);
                                    }else {
                                        tvDesc.setText("");
                                    }
                                    profileImage = profileObj.optString("image");
                                    imageLoader.displayImage(ConFig_URL.IMAGE_URL + profileObj.optString("image"), ivProfile, options);
                                    bannerImage1 = profileObj.optString("banner_img1");
                                    bannerImage2 = profileObj.optString("banner_img2");
                                    bannerImage3 = profileObj.optString("banner_img3");
                                    if (bannerImage1 != null && !bannerImage1.toString().trim().equals("") && !bannerImage1.toString().trim().equals("null")) {
                                        imageLoader.displayImage(ConFig_URL.IMAGE_URL + bannerImage1, ivBanner1, options);
                                    } else {
                                        ivBanner1.setVisibility(View.GONE);
                                    }
                                    if (bannerImage2 != null && !bannerImage2.toString().trim().equals("") && !bannerImage2.toString().trim().equals("null")) {
                                        imageLoader.displayImage(ConFig_URL.IMAGE_URL + bannerImage2, ivBanner2, options);
                                    } else {
                                        ivBanner2.setVisibility(View.GONE);
                                    }
                                    if (bannerImage3 != null && !bannerImage3.toString().trim().equals("") && !bannerImage3.toString().trim().equals("null")) {
                                        imageLoader.displayImage(ConFig_URL.IMAGE_URL + bannerImage3, ivBanner3, options);
                                    } else {
                                        ivBanner3.setVisibility(View.GONE);
                                    }
                                    JSONArray reviewArray = providerObj.optJSONArray("ratings");
                                    for (int i = 0; i < reviewArray.length(); i++) {
                                        JSONObject reviewObj = reviewArray.getJSONObject(i);
                                        Reviews_Model review = new Reviews_Model();
                                        review.setName(reviewObj.optString("name"));
                                        review.setComments(reviewObj.optString("comment"));
                                        review.setDate(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", reviewObj.optString("date")));
                                        String r = reviewObj.optString("rate_value");
                                        if (r != null && !r.toString().trim().equals("") && !r.toString().trim().equals("null")) {
                                            review.setRateValue(Float.parseFloat(r));
                                        } else {
                                            review.setRateValue(0.0f);
                                        }
                                        review_list.add(review);
                                    }
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(CustomerProfileActivity.this);
                                    builder.setMessage(object.optString("msg"))
                                            .setTitle(getResources().getString(R.string.sorry))
                                            .setCancelable(false)
                                            .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    onBackPressed();
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                                if (review_list.isEmpty()) {
                                    llReviews.setVisibility(View.GONE);
                                    tvNoReview.setVisibility(View.VISIBLE);
                                } else {

                                    tvNoReview.setVisibility(View.GONE);
                                    setReviews();
                                }
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(CustomerProfileActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(CustomerProfileActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(CustomerProfileActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(CustomerProfileActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phone);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    void setReviews() {
        llReviews.removeAllViewsInLayout();
        for (int i = 0; i < review_list.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.customer_reviews_item, llReviews, false);
            ImageView iv_first_alphabet = (ImageView) view.findViewById(R.id.iv_first_alphabet);
            TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
            RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            TextView tv_comments = (TextView) view.findViewById(R.id.tv_comments);
            TextView tv_date = (TextView) view.findViewById(R.id.tv_date);

            //get first letter of each String item
            String firstLetter = String.valueOf(review_list.get(i).getName().charAt(0));
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder().buildRound(firstLetter.toUpperCase(), color); // radius in px
            iv_first_alphabet.setImageDrawable(drawable);

            tv_name.setText(review_list.get(i).getName());
            ratingBar.setRating(review_list.get(i).getRateValue());
            tv_comments.setText(review_list.get(i).getComments());
            tv_date.setText(review_list.get(i).getDate());
            llReviews.addView(view);
        }
    }

    private void initViews() {

        supportclass = new SupportClass(CustomerProfileActivity.this);
        mQueue = Volley.newRequestQueue(this);
        dialogView = new DialogView(this);
        pref = new Preferences(this);
        customVolley = new CustomVolley(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        btn_assign_job = (Button) findViewById(R.id.btn_assign_job);

        tvProfileName = (TextView) findViewById(R.id.tv_profile_name);
        tvDesc = (TextView) findViewById(R.id.tv_profile_details);
        tvReviews = (TextView) findViewById(R.id.tv_Reviews);
        tvNoReview = (TextView) findViewById(R.id.tvNoReview);
        ivProfile = (ImageView) findViewById(R.id.iv_profileimage);
        ivBanner1 = (ImageView) findViewById(R.id.ivBanner1);
        ivBanner2 = (ImageView) findViewById(R.id.ivBanner2);
        ivBanner3 = (ImageView) findViewById(R.id.ivBanner3);
        svMain = (ScrollView) findViewById(R.id.svMain);
        llReviews = (LinearLayout) findViewById(R.id.llReviews);

        ivProfile.setOnClickListener(this);
        ivBanner1.setOnClickListener(this);
        ivBanner2.setOnClickListener(this);
        ivBanner3.setOnClickListener(this);
        btn_assign_job.setOnClickListener(this);

        if (asignJobFlag) {
            btn_assign_job.setVisibility(View.GONE);
        } else {
            btn_assign_job.setVisibility(View.VISIBLE);
        }
        if (provider_id.equals("")) {
            btn_assign_job.setVisibility(View.GONE);
        }

    }

    void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        ibBack = (ImageButton) toolbar.findViewById(R.id.ibLeft);

        tvToolbarTitle.setVisibility(View.VISIBLE);
        ibBack.setVisibility(View.VISIBLE);

        tvToolbarTitle.setText(getResources().getString(R.string.profile));
        ibBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ibLeft:
                onBackPressed();
                break;

            case R.id.iv_profileimage:
                startActivity(new Intent(this, FullViewBannerImageActivity.class).putExtra("image", profileImage));
                break;
            case R.id.ivBanner1:
                startActivity(new Intent(this, FullViewBannerImageActivity.class).putExtra("image", bannerImage1));
                break;

            case R.id.ivBanner2:
                startActivity(new Intent(this, FullViewBannerImageActivity.class).putExtra("image", bannerImage2));
                break;

            case R.id.ivBanner3:
                startActivity(new Intent(this, FullViewBannerImageActivity.class).putExtra("image", bannerImage3));
                break;

            case R.id.btn_assign_job:
                asignJobFlag = true;
                btn_assign_job.setVisibility(View.GONE);
                VolleyAssignJob();
                break;


        }
    }

    private void VolleyAssignJob() {
        HashMap<String, String> params = getParameter();
        customVolley.ServiceRequest(this, ConFig_URL.URL_CREATE_SERVICE, params);
    }

    private HashMap<String, String> getParameter() {
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
        String car_type = getCarType();
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(CustomerProfileActivity.this, Preferences.PHONE));
        params.put("type_id", GlobalVariable.serviceType.getServiceId());
        params.put("request_date", currentDate);
        params.put("latitude", String.valueOf(GlobalVariable.locationLat));
        params.put("longitude", String.valueOf(GlobalVariable.locationLng));
        params.put("expected_cost", String.valueOf(GlobalVariable.totalServicesAmount));
        params.put("from_time", GlobalVariable.serviceStartDate);
        params.put("to_time", GlobalVariable.serviceEndDate);
        params.put("provider_id", provider_id);
        params.put("car_type", car_type);
        return params;
    }

    private String getCarType() {
        String car_type = "";
        try {
            JSONObject mainObj = new JSONObject();
        /*SUV*/
            JSONArray suvArray = new JSONArray();
            for (int i = 0; i < GlobalVariable.suvList.size(); i++) {
                JSONObject suvObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!GlobalVariable.suvList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("0")));
                }
                if (!GlobalVariable.suvList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("1")));
                }
                if (!GlobalVariable.suvList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    suvObject.put("option_details", optionDetails);
                }
                if(suvObject.length()!=0) {
                    suvArray.put(suvObject);
                }

            }
            mainObj.put("suv", suvArray);
            /*Sedan*/
            JSONArray sedanArray = new JSONArray();
            for (int i = 0; i < GlobalVariable.sedanList.size(); i++) {
                JSONObject sedanObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!GlobalVariable.sedanList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("0")));
                }
                if (!GlobalVariable.sedanList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("1")));
                }
                if (!GlobalVariable.sedanList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    sedanObject.put("option_details", optionDetails);
                }
                if(sedanObject.length()!=0) {
                    sedanArray.put(sedanObject);
                }
            }
            mainObj.put("sedan", sedanArray);
            car_type = mainObj.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return car_type;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        try {
            JSONObject object = new JSONObject(response);
            if (Type.equals(ConFig_URL.URL_CREATE_SERVICE)) {
                if (object.optString("success").equals("0")) {
                    showCustomSingleButtonDialog(CustomerProfileActivity.this,
                            getResources().getString(R.string.success), object.optString("msg"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void showCustomSingleButtonDialog(final Context context, String header,
                                             String msg) {
        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.app.AlertDialog alert = adb.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        if (callFrom.equals("AppointmentDetailsActivity")) {
            super.onBackPressed();
        } else {
            Intent intent = new Intent();
            intent.putExtra("assign_job_flag", asignJobFlag);
            intent.putExtra("currentPosition", currentPosition);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }
}
