package com.uipl.qber.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "QBER_CUSTOMER";

	// Contacts table name

	private static final String FAVORITE_LOCATION = "FAVORITE_LOCATION";

	// Contacts Table Columns names
	private static final String KEY_ID = "id";
	private static final String ADDRESS_NAME = "addressName";
	private static final String LAT = "lat";
	private static final String LNG = "lng";


	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + FAVORITE_LOCATION + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + ADDRESS_NAME + " TEXT," + LAT + " TEXT," + LNG + " TEXT"
				+ ")";
		db.execSQL(CREATE_CONTACTS_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + FAVORITE_LOCATION);

		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new contact
	public void addAddress(String addressName,String lat,String lng) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ADDRESS_NAME, addressName);
		values.put(LAT, lat);
		values.put(LNG, lng);

		// Inserting Row
		db.insert(FAVORITE_LOCATION, null, values);
		db.close(); // Closing database connection
	}


	// Getting All Contacts
	public ArrayList<HashMap<String,String>> getAllAddress() {
		ArrayList<HashMap<String,String>> addressList = new ArrayList<>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + FAVORITE_LOCATION ;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				HashMap<String,String>hashMa=new HashMap<>();
				hashMa.put("addressName",cursor.getString(cursor.getColumnIndex(ADDRESS_NAME)));
				hashMa.put("lat",cursor.getString(cursor.getColumnIndex(LAT)));
				hashMa.put("lng",cursor.getString(cursor.getColumnIndex(LNG)));
				addressList.add(hashMa);
			} while (cursor.moveToNext());
		}


		return addressList;
	}

	// Updating single contact
	public int updateAddress(int key,String addressName,String lat,String lng) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ADDRESS_NAME, addressName);
		values.put(LAT, lat);
		values.put(LNG, lng);

		// updating row
		int a= db.update(FAVORITE_LOCATION, values, KEY_ID + " = ?",
				new String[] { String.valueOf(key) });
		return a;
	}
	// Deleting single contact
	public boolean deleteAddress(String addressName) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(FAVORITE_LOCATION, ADDRESS_NAME + " = ?",
				new String[] { addressName });
		db.close();
		return true;
	}


	// Getting contacts Count
	/*public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_SEARCH;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		//cursor.close();

		// return count
		return cursor.getString();
	}*/

}
