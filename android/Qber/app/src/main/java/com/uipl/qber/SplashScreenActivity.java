package com.uipl.qber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.Model.SubServiceType;
import com.uipl.qber.utils.Preferences;

import io.fabric.sdk.android.Fabric;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Splash Screen
 */
public class SplashScreenActivity extends AppCompatActivity {

    Handler handler;
    private long TIME=4000;
    private Preferences pref;
    private RequestQueue mQueue;
    private String TAG="SplashScreenActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        pref=new Preferences(SplashScreenActivity.this);
        mQueue = Volley.newRequestQueue(this);
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                volleyTypeTask();
            }
        },TIME);
    }

    /**
     * Check and redirect next page
     */
    public void gotoNext(){
        if(pref.getStringPreference(SplashScreenActivity.this,Preferences.PHONE).equals("") && !pref.getBooleanPreference(SplashScreenActivity.this,Preferences.SKIP)) {
            startActivity(new Intent(SplashScreenActivity.this, RegisterLoginActivity.class));
            finish();
        }else {
            startActivity(new Intent(SplashScreenActivity.this, MenuActivity.class));
            finish();
        }

    }

    /**
     * Call Web Services for  get all services type.
     */
    private void volleyTypeTask() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<ServiceType>serviceTypesArray=new ArrayList<>();
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray=object.optJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject serviceObj = jsonArray.optJSONObject(i);
                                        ServiceType serviceType = new ServiceType();

                                        serviceType.setServiceId(serviceObj.optString("id"));
                                        serviceType.setImageUrl(serviceObj.optString("image"));
                                        serviceType.setDisplayText(serviceObj.optString("display_text"));
                                        if(serviceObj.optString("is_active").equals("1")) {
                                            serviceType.setActiveStatus(true);
                                        }else {
                                            serviceType.setActiveStatus(false);
                                        }
                                        serviceType.setServiceName(serviceObj.optString("name"));
                                        if(serviceObj.has("servicesetting")&& serviceObj.get("servicesetting") instanceof JSONObject)
                                        {
                                        JSONObject serviceSettingObj = serviceObj.optJSONObject("servicesetting");
                                        serviceType.setFixedCost(serviceSettingObj.optString("fixed_cost"));
                                        serviceType.setArrivalDuration(serviceSettingObj.optString("arrival_duration"));
                                        serviceType.setAllowedStartTime(serviceSettingObj.optString("allowed_start_time"));
                                        serviceType.setAllowedEndTime(serviceSettingObj.optString("allowed_end_time"));
                                        serviceType.setMinCost(serviceSettingObj.optString("min_cost"));
                                        }

                                        JSONArray optionArray = serviceObj.optJSONArray("options");

                                        ArrayList<SubServiceType> subServiceTypesArray = new ArrayList<>();
                                        if(optionArray!=null) {
                                            for (int j = 0; j < optionArray.length(); j++) {
                                                JSONObject subServiceObj = optionArray.optJSONObject(j);
                                                SubServiceType subServiceType = new SubServiceType();
                                                subServiceType.setName(subServiceObj.optString("name"));
                                                JSONArray optDetailsArray = subServiceObj.optJSONArray("option_details");
                                                ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                                                for (int k = 0; k < optDetailsArray.length(); k++) {
                                                    JSONObject optDetailsObj = optDetailsArray.optJSONObject(k);
                                                    HashMap<String, String> hashMap = new HashMap<>();
                                                    hashMap.put("name", optDetailsObj.optString("name"));
                                                    hashMap.put("cost", optDetailsObj.optString("cost"));
                                                    optDetails.add(hashMap);
                                                }
                                                subServiceType.setOptionDetailsArray(optDetails);
                                                subServiceTypesArray.add(subServiceType);

                                            }
                                        }

                                        serviceType.setsubServicesArray(subServiceTypesArray);
                                        serviceTypesArray.add(serviceType);
                                    }
                                    GlobalVariable.serviceTypesArray=serviceTypesArray;
                                    gotoNext();

                                }else {
                                   showCustomSingleButtonDialog(SplashScreenActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));  }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                           showCustomSingleButtonDialog(SplashScreenActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(error instanceof NoConnectionError) {
                    showCustomSingleButtonDialog(SplashScreenActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    showCustomSingleButtonDialog(SplashScreenActivity.this,
                            getResources().getString(R.string.sorry),getResources().getString(R.string.timeout_error));
                } else {
                    showCustomSingleButtonDialog(SplashScreenActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }
    public void showCustomSingleButtonDialog(Context context, String header,
                                             String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                finish();
                dialog.dismiss();


            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }
}
