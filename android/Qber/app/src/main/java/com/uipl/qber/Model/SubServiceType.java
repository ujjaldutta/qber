package com.uipl.qber.Model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pallab on 14/10/16.
 * Model class for hold sub services details
 */

public class SubServiceType {
    private String name;
    private ArrayList<HashMap<String, String>> optionDetailsArray;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOptionDetailsArray(ArrayList<HashMap<String, String>> optionDetailsArray) {
        this.optionDetailsArray = optionDetailsArray;
    }

    public ArrayList<HashMap<String, String>> getOptionDetailsArray() {
        return optionDetailsArray;
    }
}
