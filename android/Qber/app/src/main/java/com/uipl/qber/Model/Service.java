package com.uipl.qber.Model;

import java.util.ArrayList;

/**
 * Created by richa on 22/9/16.
 */
public class Service {

    private String pic;
    private String name;
    private String distance;
    private String carServiceId;
    private String typeId;
    private String email;
    private String phone;
    private String locationPreference;
    private String bannedDuration;
    private String providerStartDatetime;
    private String providerEndDatetime;
    private boolean active;
    private boolean approveStatus;
    private boolean smsVerified;
    private boolean readyToServe;
    private boolean online;
    private Profile profile;
    private Double currentLat;
    private Double currentLng;
    private ArrayList<Timeline> timelines = new ArrayList<>();
    private String ratingString;
    private String totalRatingCount;
    private Float ratingFloat;
    private String providerDistance;
    private boolean asignJobFlag;

    public ArrayList<Timeline> getTimelines() {
        return timelines;
    }

    public void setTimelines(ArrayList<Timeline> timelines) {
        this.timelines = timelines;
    }

    public Double getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(Double currentLat) {
        this.currentLat = currentLat;
    }

    public Double getCurrentLng() {
        return currentLng;
    }

    public void setCurrentLng(Double currentLng) {
        this.currentLng = currentLng;
    }

    public String getRatingString() {
        return ratingString;
    }

    public void setRatingString(String ratingString) {
        this.ratingString = ratingString;
    }

    public String getTotalRatingCount() {
        return totalRatingCount;
    }

    public void setTotalRatingCount(String totalRatingCount) {
        this.totalRatingCount = totalRatingCount;
    }

    public Float getRatingFloat() {
        return ratingFloat;
    }

    public void setRatingFloat(Float ratingFloat) {
        this.ratingFloat = ratingFloat;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCarServiceId() {
        return carServiceId;
    }

    public void setCarServiceId(String carServiceId) {
        this.carServiceId = carServiceId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocationPreference() {
        return locationPreference;
    }

    public void setLocationPreference(String locationPreference) {
        this.locationPreference = locationPreference;
    }

    public String getBannedDuration() {
        return bannedDuration;
    }

    public void setBannedDuration(String bannedDuration) {
        this.bannedDuration = bannedDuration;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(boolean approveStatus) {
        this.approveStatus = approveStatus;
    }

    public boolean isSmsVerified() {
        return smsVerified;
    }

    public void setSmsVerified(boolean smsVerified) {
        this.smsVerified = smsVerified;
    }

    public boolean isReadyToServe() {
        return readyToServe;
    }

    public void setReadyToServe(boolean readyToServe) {
        this.readyToServe = readyToServe;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getProviderStartDatetime() {
        return providerStartDatetime;
    }

    public void setProviderStartDatetime(String providerStartDatetime) {
        this.providerStartDatetime = providerStartDatetime;
    }

    public String getProviderEndDatetime() {
        return providerEndDatetime;
    }

    public void setProviderEndDatetime(String providerEndDatetime) {
        this.providerEndDatetime = providerEndDatetime;
    }

    public void setProviderDistance(String providerDistance) {
        this.providerDistance = providerDistance;
    }

    public String getProviderDistance() {
        return providerDistance;
    }

    public void setAsignJobFlag(boolean asignJobFlag) {
        this.asignJobFlag = asignJobFlag;
    }

    public boolean isAsignJobFlag() {
        return asignJobFlag;
    }
}
