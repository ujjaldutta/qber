package com.uipl.qber;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.uipl.qber.utils.Preferences;

public class RegisterLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button tvRegister,tvLogin;
    private TextView tvMakingHeader,tvSkip;
    Preferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_login);
        pref=new Preferences(this);
        tvSkip=(TextView)findViewById(R.id.tvSkip);
        tvSkip.setOnClickListener(this);
        tvMakingHeader=(TextView)findViewById(R.id.tvMakingHeader);
        tvMakingHeader.setTypeface(null, Typeface.BOLD);
        tvRegister=(Button)findViewById(R.id.tvRegister);
        tvLogin=(Button)findViewById(R.id.tvLogin);
        tvRegister.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvLogin:
                startActivity(new Intent(RegisterLoginActivity.this,LoginActivity.class));
                break;
            case R.id.tvRegister:
                startActivity(new Intent(RegisterLoginActivity.this,RegisterActivity.class));
                break;
            case R.id.tvSkip:
                pref.storeBooleanPreference(RegisterLoginActivity.this,Preferences.SKIP,true);
                startActivity(new Intent(RegisterLoginActivity.this,MenuActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                break;
        }
    }
}
