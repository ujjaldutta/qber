package com.uipl.qber;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.Model.ServiceType;


/**
 * A simple {@link Fragment} subclass.
 * Fragment to display service
 */
public class ViewPagerServiceFragment extends Fragment {


    private ImageView imgPic;
    private TextView tvServiceDes;
    private TextView tvServiceName;
    private int index;
    ImageLoader imageLoader;
    DisplayImageOptions options;


    public ViewPagerServiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_view_pager, container, false);
        tvServiceName=(TextView)view.findViewById(R.id.tvServiceName);
        tvServiceDes=(TextView)view.findViewById(R.id.tvServiceDes);
        imgPic=(ImageView)view.findViewById(R.id.imgPic);
        index=getArguments().getInt("index");
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        options = new DisplayImageOptions.Builder()

                .showImageOnFail(R.drawable.service_img).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        if(GlobalVariable.serviceTypesArray.size()>index){
            ServiceType serviceType=GlobalVariable.serviceTypesArray.get(index);
                tvServiceName.setText(serviceType.getServiceName());
                tvServiceDes.setText(serviceType.getDisplayText());
           imageLoader.displayImage(ConFig_URL.IMAGE_URL+serviceType.getImageUrl(), imgPic, options);
        }else {
            if (index == 0) {
                tvServiceName.setText(getString(R.string.car_wash));
                imgPic.setImageResource(R.drawable.service_img);
            } else if (index == 1) {
                tvServiceName.setText(getString(R.string.roadside_assistance));
                imgPic.setImageResource(R.drawable.service_banner2);
            } else if (index == 2) {
                tvServiceName.setText(getString(R.string.pick_n_deliver));
                imgPic.setImageResource(R.drawable.service_banner3);
            }
        }


        return view;
    }

    /**
     * @param index tab index
     * @return
     * Method to create new instance
     */
    public static ViewPagerServiceFragment newInstance(int index) {
        ViewPagerServiceFragment f = new ViewPagerServiceFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

}
