package com.uipl.qber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.Adapter.AppointmentDetailsAdapter;

import com.uipl.qber.Model.Reason;
import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.Model.SubServiceType;
import com.uipl.qber.Model.WorkHeadingDetails;
import com.uipl.qber.Model.WorkSubHeadingDetails;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Permission;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AppointmentDetailsActivity extends AppCompatActivity implements View.OnClickListener, VolleyInterface {
    private TextView tvTitle, tvServiceName, tvArrivalDate, tvStatus, tvArrivalTime, tvTotal, btnCancelAppointment, btnEditAppointment;
    private ImageView ivPic, btnBack;
    private Button btnTrackLive, btnViewProfile;
    private LinearLayout llCallNow, llProductList, llayout;
    private DialogView dialogView;
    private RequestQueue mQueue;
    public String TAG = "AppointmentDetailsActivity", total;
    private CustomVolley customVolley;
    private Preferences pref;
    ImageLoader imageLoader;
    DisplayImageOptions options, mapOptions;
    ArrayList<WorkHeadingDetails> suvArrList = new ArrayList<>();
    ArrayList<WorkHeadingDetails> sedanArrList = new ArrayList<>();
    ArrayList<Reason> reasonArrList = new ArrayList<>();
    double suvTotal, sedanTotal;
    AlertDialog alertDialog;
    private LayoutInflater inflater;
    private String serviceid = "";
    ServiceType serviceType = new ServiceType();
    ImageView ivMapImage;
    private String providerPhone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);
        mQueue = Volley.newRequestQueue(this);
        inflater = LayoutInflater.from(this);
        if (getIntent().hasExtra("service_id")) {
            serviceid = getIntent().getStringExtra("service_id");
        }
        initView();
        vollyBookedDetails();
    }


    private void checkPermissionForPhoneCall() {
        if (Permission.selfPermissionGranted(this, android.Manifest.permission.CALL_PHONE)) {
            phoneCall();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, 1);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.phone_call_permission), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            phoneCall();
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.phone_call_permission), Toast.LENGTH_SHORT).show();
        }
        return;
    }

    private void initView() {
        customVolley = new CustomVolley(this);
        pref = new Preferences(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        mapOptions = new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.profile_placeholder)
                //.showImageForEmptyUri(R.drawable.profile_placeholder)
                //.showImageOnFail(R.drawable.profile_placeholder)
                .cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvServiceName = (TextView) findViewById(R.id.tvServiceName);
        tvArrivalDate = (TextView) findViewById(R.id.tvArrivalDate);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvStatus.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        tvArrivalTime = (TextView) findViewById(R.id.tvArrivalTime);
        ivPic = (ImageView) findViewById(R.id.ivPic);
        btnBack = (ImageView) findViewById(R.id.imgBtnBack);
        btnTrackLive = (Button) findViewById(R.id.btnTrackLive);
        btnTrackLive.setOnClickListener(this);
        btnViewProfile = (Button) findViewById(R.id.btnViewProfile);
        btnCancelAppointment = (TextView) findViewById(R.id.btnCancelAppointment);
        btnEditAppointment = (TextView) findViewById(R.id.btnEditAppointment);
        dialogView = new DialogView(this);
        llayout = (LinearLayout) findViewById(R.id.llayout);
        llayout.setVisibility(View.GONE);
        llCallNow = (LinearLayout) findViewById(R.id.llCallNow);
        llCallNow.setOnClickListener(this);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        llProductList = (LinearLayout) findViewById(R.id.llProductList);
        btnCancelAppointment.setOnClickListener(this);
        btnEditAppointment.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnViewProfile.setOnClickListener(this);

        ivMapImage = (ImageView) findViewById(R.id.ivMapImage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (GlobalVariable.EditAppointmentFlag) {
            GlobalVariable.EditAppointmentFlag = false;
            vollyBookedDetails();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (customVolley != null)
            customVolley.cancelRequest(ConFig_URL.URL_BOOKING_DETAILS);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llCallNow:
                checkPermissionForPhoneCall();
                break;
            case R.id.btnCancelAppointment:
                CancelAppointmentDialog(AppointmentDetailsActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_cancel_appointment));

                break;
            case R.id.btnEditAppointment:
                System.out.println("CL SED :" + sedanArrList.size());
                System.out.println("CL SUV :" + suvArrList.size());
                GlobalVariable.editserviceType = serviceType;
                Intent editintent = new Intent(this, EditBookedActivity.class);
                startActivity(editintent);
                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;
            case R.id.btnViewProfile:
                Intent intent = new Intent(this, CustomerProfileActivity.class);
                intent.putExtra("phone", providerPhone);
                intent.putExtra("Call_From", TAG);
                startActivity(intent);
                break;
            case R.id.btnTrackLive:
                startActivity(new Intent(AppointmentDetailsActivity.this, TrackLocationActivity.class)
                        .putExtra("provider_name", tvTitle.getText().toString()).putExtra("service_id", serviceid));

                break;
        }
    }

    public void phoneCall() {
        if(providerPhone!=null && !providerPhone.equals("")) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" +"+"+ providerPhone));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        }
    }

    private void vollyBookedDetails() {
        HashMap<String, String> params = getBookedDetails();
        customVolley.ServiceRequest(this, ConFig_URL.URL_BOOKING_DETAILS, params);

    }

    private HashMap<String, String> getBookedDetails() {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(this, Preferences.PHONE));
        params.put("service_id", serviceid);
        return params;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        if (Type.equals(ConFig_URL.URL_BOOKING_DETAILS)) {
            try {
                System.out.println("result = " + response);
                JSONObject object = new JSONObject(response);
                suvArrList.clear();
                sedanArrList.clear();
                if (object != null) {
                    if (object.optString("success").toString().trim().equals("0")) {
                        JSONObject dataObj = object.getJSONObject("data");
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = null, todayDate = null, tomorrowDate = null;
                        try {
                            date = sdf.parse(dataObj.optString("from_time"));
                            todayDate = sdf.parse(sdf.format(calendar.getTime()));
                            calendar.add(Calendar.DATE, 1);
                            tomorrowDate = sdf.parse(sdf.format(calendar.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (date != null && todayDate != null && tomorrowDate != null) {
                            if (date.compareTo(todayDate) == 0) {
                                tvArrivalDate.setText("Today");
                            } else if (date.compareTo(tomorrowDate) == 0) {
                                tvArrivalDate.setText("Tomorrow");
                            } else {
                                tvArrivalDate.setText(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", dataObj.optString("from_time")));
                            }
                        } else {
                            tvArrivalDate.setText(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", dataObj.optString("from_time")));
                        }
                        tvArrivalTime.setText(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "h:mm a", dataObj.optString("from_time")).toUpperCase());
                        imageLoader.displayImage(dataObj.optString("map_url"), ivMapImage, mapOptions);
                        serviceType.setServiceId(dataObj.optString("id"));

                        JSONObject providerObj = dataObj.optJSONObject("provider");
                        tvTitle.setText(providerObj.optString("name"));
                        providerPhone = providerObj.optString("phone");
                        if(providerPhone!=null && !providerPhone.equals("") && !providerPhone.equals("null")){
                            llCallNow.setVisibility(View.VISIBLE);
                        }else {
                            llCallNow.setVisibility(View.GONE);
                        }
                        JSONObject providerProfileObj = dataObj.optJSONObject("providerprofile");
                        imageLoader.displayImage(ConFig_URL.IMAGE_URL + providerProfileObj.optString("image"), ivPic, options);
                        JSONArray serviceNotificationArray = dataObj.optJSONArray("service_notification");
                        if (serviceNotificationArray != null && serviceNotificationArray.length() > 0) {
                            JSONObject serviceNotificationObject = serviceNotificationArray.optJSONObject(0);
                            if (!serviceNotificationObject.optString("service_notice_status").equalsIgnoreCase("null"))
                                tvStatus.setText(serviceNotificationObject.optString("service_notice_status"));
                            String status = serviceNotificationObject.optString("service_notice_status");
                            if (status != null && !status.equals("") && !status.equals("null") && status.length() > 1) {
                                status = status.substring(0, 1).toUpperCase() + status.substring(1);
                                tvStatus.setText(status);
                            } else {
                                tvStatus.setText("");
                            }
                            if (serviceNotificationObject.optString("service_notice_status").toLowerCase().equals("heading")) {
                                btnTrackLive.setEnabled(true);
                                btnTrackLive.setBackgroundResource(R.drawable.rounded_corner_purple);
                                btnTrackLive.setTextColor(getResources().getColor(R.color.purple));
                            } else {
                                btnTrackLive.setEnabled(false);
                                btnTrackLive.setBackgroundResource(R.drawable.rounded_corner_grey);
                                btnTrackLive.setTextColor(getResources().getColor(R.color.black));
                            }

                        }
                        JSONObject serviceTypeObj = dataObj.getJSONObject("service_type");
                        serviceType.setServiceName(serviceTypeObj.optString("name"));
                        tvServiceName.setText(serviceTypeObj.optString("name"));
                        JSONObject carDetailsJsonObj = dataObj.optJSONObject("cardetails");

                        if (carDetailsJsonObj != null && carDetailsJsonObj.length() > 0) {
                            JSONArray nameArr = carDetailsJsonObj.names();

                            for (int j = 0; j < carDetailsJsonObj.length(); j++) {
                                suvTotal = 0;
                                sedanTotal = 0;
                                WorkHeadingDetails workHeadingDetails = new WorkHeadingDetails();
                                String arrName = nameArr.opt(j).toString();
                                JSONArray carJsonArr = carDetailsJsonObj.optJSONArray(arrName);
                                int length = arrName.length();
                                if (length > 0) {
                                    arrName = arrName.substring(0, length - 1) + " " + arrName.charAt(length - 1);
                                    workHeadingDetails.setWorkHeadingName(arrName);
                                    ArrayList<WorkSubHeadingDetails> workSubHeadingDetailsArrayList = new ArrayList<>();
                                    boolean isSuv = false;
                                    if (length == 4) {
                                        isSuv = true;
                                    }
                                    for (int k = 0; k < carJsonArr.length(); k++) {
                                        JSONArray jsonArr = carJsonArr.optJSONArray(k);
                                        JSONObject jsonObj = jsonArr.optJSONObject(0);
                                        WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                        workSubHeadingDetails.setWorkSubHeadingId(jsonObj.optString("id"));
                                        workSubHeadingDetails.setWorkSubHeadingName(jsonObj.optString("name"));
                                        workSubHeadingDetails.setWorkSubHeadingPrice(Double.parseDouble(jsonObj.optString("cost")));
                                        workSubHeadingDetails.setSelected(true);
                                        workSubHeadingDetailsArrayList.add(workSubHeadingDetails);
                                        if (isSuv) {
                                            suvTotal = suvTotal + Double.parseDouble(jsonObj.optString("cost"));
                                        } else
                                            sedanTotal = sedanTotal + Double.parseDouble(jsonObj.optString("cost"));
                                    }
                                    workHeadingDetails.setSubHeadingDetailsArrayList(workSubHeadingDetailsArrayList);
                                    if (length == 4) {
                                        workHeadingDetails.setWorkHeadingPrice(suvTotal);
                                        suvArrList.add(workHeadingDetails);
                                    } else {
                                        workHeadingDetails.setWorkHeadingPrice(sedanTotal);
                                        sedanArrList.add(workHeadingDetails);
                                    }
                                }
                            }
                        }
                        serviceType.setCarDetailsSuv(suvArrList);
                        serviceType.setCarDetailsSedan(sedanArrList);
                        double total = 0;
                        llProductList.removeAllViews();
                        System.out.println("SUV :" + suvArrList.size());
                        for (int i = 0; i < suvArrList.size(); i++) {
                            View appointment = inflater.inflate(R.layout.inflate_appointment_details, null);
                            ImageView ivProductPic = (ImageView) appointment.findViewById(R.id.ivProductPic);
                            TextView tvServiceAmount = (TextView) appointment.findViewById(R.id.tvServiceAmount);
                            TextView tvServiceName = (TextView) appointment.findViewById(R.id.tvServiceName);
                            tvServiceName.setText(suvArrList.get(i).getWorkHeadingName().toUpperCase());
                            tvServiceAmount.setText(GlobalVariable.decimalFormat.format(suvArrList.get(i).getWorkHeadingPrice()));
                            total = total + suvArrList.get(i).getWorkHeadingPrice();
                            llProductList.addView(appointment);
                        }
                        System.out.println("SED :" + sedanArrList.size());
                        for (int i = 0; i < sedanArrList.size(); i++) {
                            View appointment = inflater.inflate(R.layout.inflate_appointment_details, null);
                            ImageView ivProductPic = (ImageView) appointment.findViewById(R.id.ivProductPic);
                            TextView tvServiceAmount = (TextView) appointment.findViewById(R.id.tvServiceAmount);
                            TextView tvServiceName = (TextView) appointment.findViewById(R.id.tvServiceName);
                            total = total + sedanArrList.get(i).getWorkHeadingPrice();
                            tvServiceName.setText(sedanArrList.get(i).getWorkHeadingName());
                            ivProductPic.setImageResource(R.drawable.car_ico2);
                            tvServiceAmount.setText(GlobalVariable.decimalFormat.format(sedanArrList.get(i).getWorkHeadingPrice()));
                            llProductList.addView(appointment);
                        }
                        llayout.setVisibility(View.VISIBLE);
                        tvTotal.setText((Html.fromHtml("<b><big>" + "Total " + GlobalVariable.decimalFormat.format(total) + "</big><font><small>" + getResources().getString(R.string.qar) + "</small></font></b>")));
                    }
                }
            } catch (JSONException e) {

                dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                        AppointmentDetailsActivity.this.getResources().getString(R.string.sorry), AppointmentDetailsActivity.this.getResources().getString(R.string.internal_error));
            }
        }
    }

    void showReasonDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialog);

        final EditText etComment = (EditText) dialog.findViewById(R.id.etComment);
        final Spinner spinnerReason = (Spinner) dialog.findViewById(R.id.spinnerReason);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        ArrayList<String> tempArr = new ArrayList<>();
        for (int i = 0; i < reasonArrList.size(); i++) {
            tempArr.add(reasonArrList.get(i).getName());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tempArr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerReason.setAdapter(spinnerArrayAdapter);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etComment.getText() == null || etComment.getText().toString().trim().equals("")) {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.comment_error));
                } else {
                    volleyRejectRequest(pref.getStringPreference(AppointmentDetailsActivity.this, Preferences.PHONE), reasonArrList.get(spinnerReason.getSelectedItemPosition()).getId(),
                            etComment.getText().toString().trim());
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialog != null)
                    alertDialog.dismiss();
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void volleyRejectRequest(final String mobile, final String reasonId, final String comment) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_CANCEL_REQUEST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    if (alertDialog != null)
                                        alertDialog.dismiss();
                                    GlobalVariable.cancelAppointment=true;
                                    Toast.makeText(AppointmentDetailsActivity.this, responseJsonObj.optString("msg"), Toast.LENGTH_SHORT).show();
                                    finish();
                                } else if(responseJsonObj.optString("success").equals("1")){
                                    pref.clearAllPref();

                                    startActivity(new Intent(AppointmentDetailsActivity.this, RegisterLoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

                                }else {
                                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("service_id", serviceid);
                params.put("reason_id", reasonId);
                params.put("comment", comment);
                params.put("token", pref.getStringPreference(AppointmentDetailsActivity.this,Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyGetReasonsRequest(final String typeId) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REASON_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            reasonArrList.clear();
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JSONArray serviceJsonArr = responseJsonObj.optJSONArray("service");
                                    for (int i = 0; i < serviceJsonArr.length(); i++) {
                                        JSONObject jsonObj = serviceJsonArr.optJSONObject(i);
                                        Reason reason = new Reason();
                                        reason.setId(jsonObj.optString("id"));
                                        reason.setName(jsonObj.optString("title"));
                                        String isActive = jsonObj.optString("is_active");
                                        if (isActive != null && !isActive.toString().trim().equals("")) {
                                            if (isActive.toString().trim().equals("1")) {
                                                reason.setActive(true);
                                            } else {
                                                reason.setActive(false);
                                            }
                                        } else {
                                            reason.setActive(false);
                                        }
                                        reasonArrList.add(reason);
                                    }
                                    showReasonDialog();
                                } else {
                                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(AppointmentDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type_id", typeId);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    public void CancelAppointmentDialog(Context context, String header,
                                        String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                volleyGetReasonsRequest("1");
                dialog.dismiss();
            }
        });
        adb.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = adb.create();
        alert.show();
    }

}
