package com.uipl.qber.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.Model.WorkSubHeadingDetails;
import com.uipl.qber.R;

import java.util.ArrayList;

/**
 * Created by Pallab on 9/26/2016.
 */
public class AppointmentDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<WorkSubHeadingDetails> workSubHeadingDetails = new ArrayList<>();
    Context context;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    private double suvAServiceAmount,suvBServiceAmount,sedanAServiceAmount,total;
    private String totalS;

    public AppointmentDetailsAdapter(Context mContext, ArrayList<WorkSubHeadingDetails> mArrayList) {
        this.context = mContext;
        this.workSubHeadingDetails = mArrayList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public int getItemCount() {
        return workSubHeadingDetails.size();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        viewHolder = new AppointmentHolder(inflater.inflate(R.layout.inflate_appointment_details, viewGroup, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        WorkSubHeadingDetails wdetails = (WorkSubHeadingDetails) workSubHeadingDetails.get(position);

        AppointmentHolder holder = (AppointmentHolder) viewHolder;

    /*   imageLoader.displayImage(appointment.getPic(), holder.ivPic, options);
       imageLoader.displayImage(appointment.getMapPic(), holder.ivMapImage, options);
      holder.tvName.setText(appointment.getName());
       holder.tvServiceName.setText(appointment.getServiceName());
       holder.tvServiceCode.setText(appointment.getServiceCode());
       holder.tvStatus.setText(appointment.getStatus());
       holder.tvArrivalTime.setText(appointment.getArrivalTime());
      holder.tvArrivalDate.setText(appointment.getArrivalDate());*/
//        holder.tvServiceAmount.setText(appointment.getCost());
//        holder.tvServiceName.setText(wdetails.getWorkSubHeadingName());
//        holder.tvServiceAmount.setText((int) wdetails.getWorkSubHeadingPrice());

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
//        if (position==0)
//        {
////          holder.tvServiceName.setText("SUV A");
//            holder.tvServiceName.setText(wdetails.getWorkSubHeadingName());
////            holder.tvServiceAmount.setText("75QAR");
////            holder.tvServiceAmount.setText(appointment.getCost().toString().trim());
//
//            //suvAServiceAmount=Double.parseDouble(holder.tvServiceAmount.getText().toString().trim());
//        }
//        if (position==1)
//        {
//            holder.tvServiceName.setText("SUV B");
//            holder.tvServiceAmount.setText("100QAR");
//            //suvBServiceAmount=Double.parseDouble(holder.tvServiceAmount.getText().toString().trim());
//        }
//        if (position==2)
//        {
//            holder.tvServiceName.setText("Sedan A");
//            holder.tvServiceAmount.setText("90QAR");
//            //sedanAServiceAmount=Double.parseDouble(holder.tvServiceAmount.getText().toString().trim());
//        }
////        total=suvAServiceAmount+suvBServiceAmount+sedanAServiceAmount;
////        totalS=String.valueOf(total);
        for (int i=0;i<workSubHeadingDetails.size();i++)
        {

        }

    }

    class AppointmentHolder extends RecyclerView.ViewHolder {

        TextView tvServiceAmount, tvServiceName;
        ImageView ivEdit, ivProductPic;


        public AppointmentHolder(View v) {
            super(v);
//            ivEdit = (ImageView) v.findViewById(R.id.ivEdit);
            ivProductPic = (ImageView) v.findViewById(R.id.ivProductPic);
            tvServiceAmount = (TextView) v.findViewById(R.id.tvServiceAmount);
            tvServiceName = (TextView) v.findViewById(R.id.tvServiceName);

        }
    }
}