package com.uipl.qber;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.Model.SubServiceType;
import com.uipl.qber.Model.WorkHeadingDetails;
import com.uipl.qber.Model.WorkSubHeadingDetails;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditBookedActivity extends AppCompatActivity implements VolleyInterface, View.OnClickListener {

    private LinearLayout llSuv, llSedun, llAdableSuv, llAdableSedun;
    ScrollView svScroll;
    TextView tvSuvCount, tvSedunCount, tvTotal, tvUpdate, tvNoteText;
    Spinner spnSuv, spnSedun;
    ServiceType serviceType ;
    int totalSuvCount = 1, totalSedunCount = 1;
    private DialogView dialogview;
    private int qarColor;
    private RequestQueue mQueue;
    private Preferences pref;
    CustomVolley customVolley;
    private String TAG="EditBookedActivity";
    public static ArrayList<HashMap<String,String>> suvList=new ArrayList<>();
    public static ArrayList<HashMap<String,String>> sedanList=new ArrayList<>();
    /*for edit*/
    ServiceType editServiceType = new ServiceType();
    ArrayList<WorkHeadingDetails> editSuvArrList = new ArrayList<>();
    ArrayList<WorkHeadingDetails> editSedanArrList = new ArrayList<>();
    boolean suvFistLoad=true,sedanFistLoad=true;
    private Toolbar toolbar;
    private TextView tvToolbarTitle;
    private ImageButton ibBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_booked);
        initToolbar();
        initViews();
        volleyTypeTask();

    }
    void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        ibBack = (ImageButton) toolbar.findViewById(R.id.ibLeft);
        ibBack.setVisibility(View.VISIBLE);

        tvToolbarTitle.setVisibility(View.VISIBLE);
        ibBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.edit_service));
        ibBack.setOnClickListener(this);
    }
    private void initViews() {
        suvList = new ArrayList<>();
        sedanList = new ArrayList<>();
        mQueue = Volley.newRequestQueue(EditBookedActivity.this);
        pref=new Preferences(this);
        dialogview = new DialogView(EditBookedActivity.this);
        customVolley=new CustomVolley(EditBookedActivity.this);

        qarColor = getResources().getColor(R.color.grey);
        llSuv = (LinearLayout) findViewById(R.id.llSuv);
        llSedun = (LinearLayout)findViewById(R.id.llSedun);

        llAdableSuv = (LinearLayout) findViewById(R.id.llAdableSuv);
        llAdableSedun = (LinearLayout) findViewById(R.id.llAdableSedun);

        svScroll = (ScrollView) findViewById(R.id.svScroll);
        tvSuvCount = (TextView) findViewById(R.id.tvSuvCount);
        tvSedunCount = (TextView) findViewById(R.id.tvSedunCount);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvUpdate = (TextView) findViewById(R.id.tvUpdate);

        spnSuv = (Spinner) findViewById(R.id.spnSuv);
        spnSedun = (Spinner) findViewById(R.id.spnSedun);
        tvNoteText = (TextView) findViewById(R.id.tvNoteText);
    }

    /**
     * Method to call web services for service type
     */
    private void volleyTypeTask() {
        dialogview.showCustomSpinProgress(EditBookedActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray=object.optJSONArray("data");
                                    if(jsonArray.length()>0){
                                        JSONObject serviceObj = jsonArray.optJSONObject(0);
                                        serviceType = new ServiceType();

                                        serviceType.setServiceId(serviceObj.optString("id"));
                                        serviceType.setImageUrl(serviceObj.optString("image"));
                                        serviceType.setDisplayText(serviceObj.optString("display_text"));
                                        if(serviceObj.optString("is_active").equals("1")) {
                                            serviceType.setActiveStatus(true);
                                        }else {
                                            serviceType.setActiveStatus(false);
                                        }
                                        serviceType.setServiceName(serviceObj.optString("name"));
                                        if(serviceObj.has("servicesetting")&& serviceObj.get("servicesetting") instanceof JSONObject)
                                        {
                                            JSONObject serviceSettingObj = serviceObj.optJSONObject("servicesetting");
                                            serviceType.setFixedCost(serviceSettingObj.optString("fixed_cost"));
                                            serviceType.setArrivalDuration(serviceSettingObj.optString("arrival_duration"));
                                            serviceType.setAllowedStartTime(serviceSettingObj.optString("allowed_start_time"));
                                            serviceType.setAllowedEndTime(serviceSettingObj.optString("allowed_end_time"));
                                            serviceType.setMinCost(serviceSettingObj.optString("min_cost"));
                                        }

                                        JSONArray optionArray = serviceObj.optJSONArray("options");

                                        ArrayList<SubServiceType> subServiceTypesArray = new ArrayList<>();
                                        if(optionArray!=null) {
                                            for (int j = 0; j < optionArray.length(); j++) {
                                                JSONObject subServiceObj = optionArray.optJSONObject(j);
                                                SubServiceType subServiceType = new SubServiceType();
                                                subServiceType.setName(subServiceObj.optString("name"));
                                                JSONArray optDetailsArray = subServiceObj.optJSONArray("option_details");
                                                ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                                                for (int k = 0; k < optDetailsArray.length(); k++) {
                                                    JSONObject optDetailsObj = optDetailsArray.optJSONObject(k);
                                                    HashMap<String, String> hashMap = new HashMap<>();
                                                    hashMap.put("sub_service_id", optDetailsObj.optString("id"));
                                                    hashMap.put("name", optDetailsObj.optString("name"));
                                                    hashMap.put("cost", optDetailsObj.optString("cost"));
                                                    optDetails.add(hashMap);
                                                }
                                                subServiceType.setOptionDetailsArray(optDetails);
                                                subServiceTypesArray.add(subServiceType);

                                            }
                                        }

                                        serviceType.setsubServicesArray(subServiceTypesArray);
                                       // GlobalVariable.serviceType=serviceType;
                                        setupData();
                                       
                                    }else {
                                        dialogview.dismissCustomSpinProgress();
                                        dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                                                getResources().getString(R.string.sorry), object.optString("msg"));
                                    }



                                }else {
                                    dialogview.dismissCustomSpinProgress();
                                    dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }


                        } catch (JSONException e) {
                            dialogview.dismissCustomSpinProgress();
                            // TODO Auto-generated catch block
                            dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                            getResources().getString(R.string.sorry),getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type_id", "1");
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    public void setupData(){
        ArrayAdapter adapter = ArrayAdapter.createFromResource(EditBookedActivity.this, R.array.item_count, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSuv.setAdapter(adapter);
        spnSedun.setAdapter(adapter);
        tvNoteText.setText(Html.fromHtml("<big>" + getString(R.string.please_note_that_delivery_and_set_up_fees_are) + " " + serviceType.getFixedCost() + "</big><small>" + getString(R.string.qar) + "</small>"));
       /*for edit*/
        editServiceType = GlobalVariable.editserviceType;
        editSuvArrList.addAll(editServiceType.getCarDetailsSuv());
        totalSuvCount=editSuvArrList.size();
        spnSuv.setSelection(totalSuvCount);
        tvSuvCount.setText(String.valueOf(totalSuvCount));
        addViewSuv(true);
        editSedanArrList.addAll(editServiceType.getCarDetailsSedan());
        totalSedunCount=editSedanArrList.size();
        spnSedun.setSelection(totalSedunCount);
        tvSedunCount.setText(String.valueOf(totalSedunCount));
        addViewSedun(true);
        setListener();
        dialogview.dismissCustomSpinProgress();
    }

    /**
     * Method to set item click listener
     */
    private void setListener() {
        spnSuv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!suvFistLoad) {
                    totalSuvCount = Integer.parseInt(parent.getItemAtPosition(position).toString());
                    tvSuvCount.setText(String.valueOf(totalSuvCount));
                    addViewSuv(false);
                }else {
                    suvFistLoad=false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnSedun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!sedanFistLoad) {
                    totalSedunCount = Integer.parseInt(parent.getItemAtPosition(position).toString());
                    tvSedunCount.setText(String.valueOf(totalSedunCount));
                    addViewSedun(false);
                }else {
                    sedanFistLoad=false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        llSuv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spnSuv.performClick();
            }
        });

        llSedun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spnSedun.performClick();
            }
        });

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkAddView()){
                    dialogview.showCustomSingleButtonDialog(EditBookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_service));
                }else {
                    if (GlobalVariable.totalServicesAmount <= Integer.parseInt(serviceType.getMinCost())) {
                        dialogview.showCustomSingleButtonDialog(EditBookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.minimum_order_amount_should_be) + " " + serviceType.getMinCost());

                    } else {

                        HashMap<String, String> params=getParams();
                        customVolley.ServiceRequest(EditBookedActivity.this,ConFig_URL.URL_UPDATE_CAR_DETAILS,params);
                    }
                }

            }
        });
    }

    protected HashMap<String, String> getParams() {
        String car_type=getCarType();
        HashMap<String, String> params = new HashMap<>();
        params.put("phone",pref.getStringPreference(EditBookedActivity.this,Preferences.PHONE) );
        params.put("service_id",editServiceType.getServiceId() );
        params.put("car_type",car_type );
        Log.d(TAG, params.toString());
        return params;
    }

    /**
     * Method to add SUV item view
     * @param flag
     */
    private void addViewSuv(boolean flag) {
        if (llAdableSuv.getChildCount() < totalSuvCount) {
            // Added
            int extraAdable = totalSuvCount - llAdableSuv.getChildCount();
            int alreadyAdded = llAdableSuv.getChildCount();
            for (int i = 0; i < extraAdable; i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View myView = inflater.inflate(R.layout.inflate_care_feature, null);

                final LinearLayout llExpandable = (LinearLayout) myView.findViewById(R.id.llExpandable);
                final ImageView imv_expand = (ImageView) myView.findViewById(R.id.imv_expand);

                TextView tv_careName = (TextView) myView.findViewById(R.id.tv_careName);
                tv_careName.setText(getString(R.string.suv) + " " + (alreadyAdded + i + 1));
                final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
                cb_totalAmount.setTag(suvList.size());

                final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
                cb_pressureWash.setTag(suvList.size());
                final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
                cb_interior.setTag(suvList.size());
                final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
                cb_tyre.setTag(suvList.size());
                TextView tv_pressureWashPrice = (TextView) myView.findViewById(R.id.tv_pressureWashPrice);
                TextView tv_interiorPrice = (TextView) myView.findViewById(R.id.tv_interiorPrice);
                TextView tv_tyrePrice = (TextView) myView.findViewById(R.id.tv_tyrePrice);
                ImageView imvHelppressure = (ImageView) myView.findViewById(R.id.imvHelppressure);
                ImageView imvHelpInterior = (ImageView) myView.findViewById(R.id.imvHelpInterior);

                if (serviceType != null) {
                    ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                    optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();

                    int total = Integer.parseInt(optDetails.get(0).get("cost")) + Integer.parseInt(optDetails.get(1).get("cost")) + Integer.parseInt(optDetails.get(2).get("cost"));
                    cb_totalAmount.setText(Html.fromHtml("<b><big>" + String.valueOf(total) + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font></b>"));
                    tv_pressureWashPrice.setText(Html.fromHtml("<big>" + optDetails.get(0).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_interiorPrice.setText(Html.fromHtml("<big>" + optDetails.get(1).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_tyrePrice.setText(Html.fromHtml("<big>" + optDetails.get(2).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));


                if(flag){
                    cb_pressureWash.setChecked(false);
                    cb_interior.setChecked(false);
                    cb_tyre.setChecked(false);
                    hashMap.put("0", "");
                    hashMap.put("1", "");
                    hashMap.put("2", "");
                    ArrayList<WorkSubHeadingDetails> subServiceArrayList= editSuvArrList.get(i).getSubHeadingDetailsArrayList();
                    for(int j=0;j<subServiceArrayList.size();j++){
                        if(subServiceArrayList.get(j).getWorkSubHeadingId().equals(optDetails.get(0).get("sub_service_id"))){
                            hashMap.put("0", optDetails.get(0).get("sub_service_id"));
                            cb_pressureWash.setChecked(true);
                        }else if(subServiceArrayList.get(j).getWorkSubHeadingId().equals(optDetails.get(1).get("sub_service_id"))){
                            hashMap.put("1", optDetails.get(1).get("sub_service_id"));
                            cb_interior.setChecked(true);
                        }else if(subServiceArrayList.get(j).getWorkSubHeadingId().equals(optDetails.get(2).get("sub_service_id"))){
                            hashMap.put("2", optDetails.get(2).get("sub_service_id"));
                            cb_tyre.setChecked(true);
                        }
                    }
                    if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                            cb_tyre.isChecked()) {
                        cb_totalAmount.setChecked(true);

                    }else {
                        cb_totalAmount.setChecked(false);
                    }

                }else {
                    hashMap.put("0", optDetails.get(0).get("sub_service_id"));
                    hashMap.put("1", optDetails.get(1).get("sub_service_id"));
                    hashMap.put("2", optDetails.get(2).get("sub_service_id"));

                }
                    suvList.add(hashMap);
                }

                cb_totalAmount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_totalAmount.isChecked()) {
                            cb_totalAmount.setChecked(true);
                            cb_pressureWash.setChecked(true);
                            cb_interior.setChecked(true);
                            cb_tyre.setChecked(true);
                            suvList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                            suvList.get(pos).put("1", optDetails.get(1).get("sub_service_id"));
                            suvList.get(pos).put("2", optDetails.get(2).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            cb_pressureWash.setChecked(false);
                            cb_interior.setChecked(false);
                            cb_tyre.setChecked(false);
                            suvList.get(pos).put("0", "");
                            suvList.get(pos).put("1", "");
                            suvList.get(pos).put("2", "");
                        }

                        calculateTotalAmount();
                    }
                });

                cb_pressureWash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if(cb_pressureWash.isChecked()){
                            suvList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                        }
                        else {
                            cb_totalAmount.setChecked(false);
                            suvList.get(pos).put("0", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_interior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if(cb_interior.isChecked()){
                            suvList.get(pos).put("1", optDetails.get(1).get("sub_service_id"));
                        }
                        else {
                            cb_totalAmount.setChecked(false);
                            suvList.get(pos).put("1", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_tyre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if(cb_tyre.isChecked()){
                            suvList.get(pos).put("2", optDetails.get(2).get("sub_service_id"));
                        }
                        else {
                            cb_totalAmount.setChecked(false);
                            suvList.get(pos).put("2", "");
                        }
                        calculateTotalAmount();
                    }
                });

                imv_expand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (llExpandable.getVisibility() == View.VISIBLE) {
                            llExpandable.setVisibility(View.GONE);
                            imv_expand.setImageResource(R.drawable.plus_ico);
                        } else {
                            llExpandable.setVisibility(View.VISIBLE);
                            imv_expand.setImageResource(R.drawable.minus_ico);
                        }
                    }
                });

                imvHelppressure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogview.showCustomSingleButtonDialog(EditBookedActivity.this, "", getResources().getString(R.string.help_msg1));
                    }
                });

                imvHelpInterior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogview.showCustomSingleButtonDialog(EditBookedActivity.this, "", getResources().getString(R.string.help_msg2));
                    }
                });


                llAdableSuv.addView(myView);
            }
        } else {
            // Removed
            int extraRemovable = llAdableSuv.getChildCount() - totalSuvCount;
            for (int i = 0; i < extraRemovable; i++) {
                llAdableSuv.removeViewAt(llAdableSuv.getChildCount() - 1);
                suvList.remove(suvList.size()-1);
            }
        }


        calculateTotalAmount();

    }

    /**
     * Method to add Sedan item view
     * @param flag
     */
    private void addViewSedun(boolean flag) {
        if (llAdableSedun.getChildCount() < totalSedunCount) {
            // Added
            int extraAdable = totalSedunCount - llAdableSedun.getChildCount();
            int alreadyAdded = llAdableSedun.getChildCount();

            for (int i = 0; i < extraAdable; i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View myView = inflater.inflate(R.layout.inflate_care_feature, null);

                final LinearLayout llExpandable = (LinearLayout) myView.findViewById(R.id.llExpandable);
                final ImageView imv_expand = (ImageView) myView.findViewById(R.id.imv_expand);

                TextView tv_careName = (TextView) myView.findViewById(R.id.tv_careName);
                tv_careName.setText(getString(R.string.sedan) + " " + (alreadyAdded + i + 1));
                final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
                cb_totalAmount.setTag(sedanList.size());


                final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
                cb_pressureWash.setTag(sedanList.size());
                final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
                cb_interior.setTag(sedanList.size());
                final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
                cb_tyre.setTag(sedanList.size());
                TextView tv_pressureWashPrice = (TextView) myView.findViewById(R.id.tv_pressureWashPrice);
                TextView tv_interiorPrice = (TextView) myView.findViewById(R.id.tv_interiorPrice);
                TextView tv_tyrePrice = (TextView) myView.findViewById(R.id.tv_tyrePrice);

                ImageView imvHelppressure = (ImageView) myView.findViewById(R.id.imvHelppressure);
                ImageView imvHelpInterior = (ImageView) myView.findViewById(R.id.imvHelpInterior);

                if (serviceType != null) {
                    ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                    optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                    int total = Integer.parseInt(optDetails.get(0).get("cost")) + Integer.parseInt(optDetails.get(1).get("cost")) + Integer.parseInt(optDetails.get(2).get("cost"));
                    cb_totalAmount.setText(Html.fromHtml("<b><big>" + String.valueOf(total) + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font></b>"));
                    tv_pressureWashPrice.setText(Html.fromHtml("<big>" + optDetails.get(0).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_interiorPrice.setText(Html.fromHtml("<big>" + optDetails.get(1).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));
                    tv_tyrePrice.setText(Html.fromHtml("<big>" + optDetails.get(2).get("cost") + "</big><font color=" + qarColor + "><small>" + getString(R.string.qar) + "</small></font>"));



                if(flag){
                    cb_pressureWash.setChecked(false);
                    cb_interior.setChecked(false);
                    cb_tyre.setChecked(false);
                    hashMap.put("0", "");
                    hashMap.put("1", "");
                    hashMap.put("2", "");
                    ArrayList<WorkSubHeadingDetails> subServiceArrayList= editSedanArrList.get(i).getSubHeadingDetailsArrayList();
                    for(int j=0;j<subServiceArrayList.size();j++){
                        if(subServiceArrayList.get(j).getWorkSubHeadingId().equals(optDetails.get(0).get("sub_service_id"))){
                            hashMap.put("0", optDetails.get(0).get("sub_service_id"));
                            cb_pressureWash.setChecked(true);
                        }else if(subServiceArrayList.get(j).getWorkSubHeadingId().equals(optDetails.get(1).get("sub_service_id"))){
                            hashMap.put("1", optDetails.get(1).get("sub_service_id"));
                            cb_interior.setChecked(true);
                        }else if(subServiceArrayList.get(j).getWorkSubHeadingId().equals(optDetails.get(2).get("sub_service_id"))){
                            hashMap.put("2", optDetails.get(2).get("sub_service_id"));
                            cb_tyre.setChecked(true);
                        }
                    }
                    if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                            cb_tyre.isChecked()) {
                        cb_totalAmount.setChecked(true);

                    }else {
                        cb_totalAmount.setChecked(false);
                    }
                }else {
                    hashMap.put("0", optDetails.get(0).get("sub_service_id"));
                    hashMap.put("1", optDetails.get(1).get("sub_service_id"));
                    hashMap.put("2", optDetails.get(2).get("sub_service_id"));
                }
                    sedanList.add(hashMap);
                }

                cb_totalAmount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_totalAmount.isChecked()) {
                            cb_totalAmount.setChecked(true);
                            cb_pressureWash.setChecked(true);
                            cb_interior.setChecked(true);
                            cb_tyre.setChecked(true);
                            sedanList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                            sedanList.get(pos).put("1", optDetails.get(1).get("sub_service_id"));
                            sedanList.get(pos).put("2", optDetails.get(2).get("sub_service_id"));
                        } else {
                            cb_totalAmount.setChecked(false);
                            cb_pressureWash.setChecked(false);
                            cb_interior.setChecked(false);
                            cb_tyre.setChecked(false);
                            sedanList.get(pos).put("0", "");
                            sedanList.get(pos).put("1", "");
                            sedanList.get(pos).put("2", "");
                        }

                        calculateTotalAmount();
                    }
                });

                cb_pressureWash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if(cb_pressureWash.isChecked()){
                            sedanList.get(pos).put("0", optDetails.get(0).get("sub_service_id"));
                        }
                        else {
                            cb_totalAmount.setChecked(false);
                            sedanList.get(pos).put("0", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_interior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if(cb_interior.isChecked()){
                            sedanList.get(pos).put("1", optDetails.get(0).get("sub_service_id"));
                        }else {
                            cb_totalAmount.setChecked(false);
                            sedanList.get(pos).put("1", "");
                        }
                        calculateTotalAmount();
                    }
                });
                cb_tyre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                        optDetails = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
                        if (cb_pressureWash.isChecked() && cb_interior.isChecked() &&
                                cb_tyre.isChecked()) {
                            cb_totalAmount.setChecked(true);

                        }
                        if (cb_tyre.isChecked()){
                            sedanList.get(pos).put("2", optDetails.get(0).get("sub_service_id"));
                        }
                        else {
                            cb_totalAmount.setChecked(false);
                            sedanList.get(pos).put("2", "");
                        }
                        calculateTotalAmount();
                    }
                });

                imv_expand.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (llExpandable.getVisibility() == View.VISIBLE) {
                            llExpandable.setVisibility(View.GONE);
                            imv_expand.setImageResource(R.drawable.plus_ico);
                        } else {
                            llExpandable.setVisibility(View.VISIBLE);
                            imv_expand.setImageResource(R.drawable.minus_ico);
                        }
                    }
                });

                imvHelppressure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogview.showCustomSingleButtonDialog(EditBookedActivity.this, "", getResources().getString(R.string.help_msg1));
                    }
                });

                imvHelpInterior.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogview.showCustomSingleButtonDialog(EditBookedActivity.this, "", getResources().getString(R.string.help_msg2));
                    }
                });

                llAdableSedun.addView(myView);
            }

        } else {
            // Removed
            int extraRemovable = llAdableSedun.getChildCount() - totalSedunCount;
            for (int i = 0; i < extraRemovable; i++) {
                llAdableSedun.removeViewAt(llAdableSedun.getChildCount() - 1);
                sedanList.remove(sedanList.size()-1);
            }
        }

        calculateTotalAmount();
    }

    /**
     * Method to calculate TotalAmount
     */
    private void calculateTotalAmount() {
        int totalAmount = 0;

        int countSuv = llAdableSuv.getChildCount();
        for (int i = 0; i < countSuv; i++) {
            View myView = llAdableSuv.getChildAt(i);
            final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
            final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
            final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
            final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
            ArrayList<HashMap<String, String>> optDetailsSuv = new ArrayList<>();
            optDetailsSuv = serviceType.getSubServicesArray().get(0).getOptionDetailsArray();

            if (cb_totalAmount.isChecked()) {
                int total = Integer.parseInt(optDetailsSuv.get(0).get("cost")) + Integer.parseInt(optDetailsSuv.get(1).get("cost")) + Integer.parseInt(optDetailsSuv.get(2).get("cost"));
                totalAmount = totalAmount + total;
            } else {
                if (cb_pressureWash.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSuv.get(0).get("cost"));
                }
                if (cb_interior.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSuv.get(1).get("cost"));
                }
                if (cb_tyre.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSuv.get(2).get("cost"));
                }
            }
        }

        int countSedun = llAdableSedun.getChildCount();
        for (int i = 0; i < countSedun; i++) {
            View myView = llAdableSedun.getChildAt(i);
            final CheckBox cb_totalAmount = (CheckBox) myView.findViewById(R.id.cb_totalAmount);
            final CheckBox cb_pressureWash = (CheckBox) myView.findViewById(R.id.cb_pressureWash);
            final CheckBox cb_interior = (CheckBox) myView.findViewById(R.id.cb_interior);
            final CheckBox cb_tyre = (CheckBox) myView.findViewById(R.id.cb_tyre);
            ArrayList<HashMap<String, String>> optDetailsSedun = new ArrayList<>();
            optDetailsSedun = serviceType.getSubServicesArray().get(1).getOptionDetailsArray();
            if (cb_totalAmount.isChecked()) {
                int total = Integer.parseInt(optDetailsSedun.get(0).get("cost")) + Integer.parseInt(optDetailsSedun.get(1).get("cost")) + Integer.parseInt(optDetailsSedun.get(2).get("cost"));
                totalAmount = totalAmount + total;
            } else {
                if (cb_pressureWash.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSedun.get(0).get("cost"));
                }
                if (cb_interior.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSedun.get(1).get("cost"));
                }
                if (cb_tyre.isChecked()) {
                    totalAmount = totalAmount + Integer.parseInt(optDetailsSedun.get(2).get("cost"));
                }
            }
        }
        totalAmount = totalAmount + Integer.parseInt(serviceType.getFixedCost());

        tvTotal.setText(Html.fromHtml("<b><big>" + getString(R.string.total) + " " + totalAmount + "</big><small>" + getString(R.string.qar) + "</small></b>"));
        GlobalVariable.totalServicesAmount = totalAmount;
    }


    private String getCarType() {
        String car_type = "";
        try {
            JSONObject mainObj = new JSONObject();
        /*SUV*/
            JSONArray suvArray = new JSONArray();
            for (int i = 0; i < suvList.size(); i++) {
                JSONObject suvObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!suvList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(suvList.get(i).get("0")));
                }
                if (!suvList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(suvList.get(i).get("1")));
                }
                if (!suvList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(suvList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    suvObject.put("option_details", optionDetails);
                }
                if(suvObject.length()!=0) {
                    suvArray.put(suvObject);
                }

            }
            mainObj.put("suv", suvArray);
            /*Sedan*/
            JSONArray sedanArray = new JSONArray();
            for (int i = 0; i <sedanList.size(); i++) {
                JSONObject sedanObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!sedanList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(sedanList.get(i).get("0")));
                }
                if (!sedanList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(sedanList.get(i).get("1")));
                }
                if (!sedanList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(sedanList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    sedanObject.put("option_details", optionDetails);
                }
                if(sedanObject.length()!=0) {
                    sedanArray.put(sedanObject);
                }

            }
            mainObj.put("sedan", sedanArray);
            car_type = mainObj.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return car_type;
    }

    public boolean checkAddView(){
        if(llAdableSedun.getChildCount()==0 && llAdableSuv.getChildCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        try {
            JSONObject object = new JSONObject(response);
            if (Type.equals(ConFig_URL.URL_UPDATE_CAR_DETAILS)) {
                if (object.optString("success").equals("0")) {
                    GlobalVariable.EditAppointmentFlag=true;
                    dialogview.showCustomSingleButtonDialog(EditBookedActivity.this,
                            getResources().getString(R.string.success), object.optString("msg"));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ibLeft:
                onBackPressed();
                break;
        }
    }
}
