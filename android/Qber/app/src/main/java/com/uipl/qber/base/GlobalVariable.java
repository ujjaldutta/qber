package com.uipl.qber.base;

import com.uipl.qber.Model.ServiceType;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pallab on 28/9/16.
 */

public  class GlobalVariable {
    public static int Connection_Time_Out=120000;
    public static String ISD_CODE="91";
//   public static String ISD_CODE="974";
    public static String SMS_CODE="";
    public static String FIREBASE_TOKEN="";

    public static String notice="";
    public static ServiceType serviceType;
    public static ServiceType editserviceType;
    public static int totalServicesAmount=0;
    public static ArrayList<ServiceType> serviceTypesArray=new ArrayList<>();
    /*booked service*/
    public static ArrayList<HashMap<String,String>> suvList=new ArrayList<>();
    public static ArrayList<HashMap<String,String>> sedanList=new ArrayList<>();
    public static boolean setLocation=false;
    public static Double locationLat=0.0;
    public static Double locationLng=0.0;
    public static String serviceStartDate="";
    public static String serviceEndDate="";

    public static DecimalFormat decimalFormat = new DecimalFormat("#0.00");
    public static boolean ProviderActivityFlag=false;
    public static boolean TrackLocationActvityFlag=false;
    public static boolean EditAppointmentFlag=false;

    /*TrackLocationActivity*/
    public static String destLabel,originLabel,estimatedTime;

    public static boolean cancelAppointment=false;
}
