package com.uipl.qber.Model;

import java.util.ArrayList;

/**
 * Created by pallab on 14/10/16.
 * Modal class for hold service details
 */

public class ServiceType {
    private String serviceId;
    private String serviceName;
    private ArrayList<SubServiceType> subServicesArray;
    private String fixedCost;
    private String arrivalDuration;
    private String allowedStartTime;
    private String allowedEndTime;
    private String minCost;
    private String imageUrl;
    private String displayText;
    private boolean activeStatus;
    private ArrayList<WorkHeadingDetails> carDetailsSuv;
    private ArrayList<WorkHeadingDetails> carDetailsSedan;

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setsubServicesArray(ArrayList<SubServiceType> subServicesArray) {
        this.subServicesArray = subServicesArray;
    }

    public ArrayList<SubServiceType> getSubServicesArray() {
        return subServicesArray;
    }

    public void setSubServicesArray(ArrayList<SubServiceType> subServicesArray) {
        this.subServicesArray = subServicesArray;
    }

    public void setFixedCost(String fixedCost) {
        this.fixedCost = fixedCost;
    }

    public String getFixedCost() {
        return fixedCost;
    }

    public void setArrivalDuration(String arrivalDuration) {
            this.arrivalDuration = arrivalDuration;
    }

    public String getArrivalDuration() {
        return arrivalDuration;
    }

    public void setAllowedStartTime(String allowedStartTime) {
        this.allowedStartTime = allowedStartTime;
    }

    public String getAllowedStartTime() {
        return allowedStartTime;
    }

    public void setAllowedEndTime(String allowedEndTime) {
        this.allowedEndTime = allowedEndTime;
    }

    public String getAllowedEndTime() {
        return allowedEndTime;
    }

    public void setMinCost(String minCost) {
        this.minCost = minCost;
    }

    public String getMinCost() {
        return minCost;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public boolean isActiveStatus() {
        return activeStatus;
    }

    public void setCarDetailsSuv(ArrayList<WorkHeadingDetails> carDetailsSuv) {
        this.carDetailsSuv = carDetailsSuv;
    }

    public ArrayList<WorkHeadingDetails> getCarDetailsSuv() {
        return carDetailsSuv;
    }

    public void setCarDetailsSedan(ArrayList<WorkHeadingDetails> carDetailsSedan) {
        this.carDetailsSedan = carDetailsSedan;
    }

    public ArrayList<WorkHeadingDetails> getCarDetailsSedan() {
        return carDetailsSedan;
    }
}
