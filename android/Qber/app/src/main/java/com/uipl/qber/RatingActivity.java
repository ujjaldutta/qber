package com.uipl.qber;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.uipl.qber.R.id.imgBtnBack;
import static com.uipl.qber.R.string.mobile;

public class RatingActivity extends AppCompatActivity implements View.OnClickListener, VolleyInterface {
    ImageView ivPic;
    TextView tvName;
    EditText etComments;
    RatingBar ratingBar, professionalRatingBar, friendlyRatingBar, communicationRatingBar;
    private Button btnSubmit;
    private String TAG = "RatingActivity";
    private DialogView dialogview;
    private RequestQueue mQueue;
    ImageButton imgCross, imgBtnBack;
    private CustomVolley customVolley;
    private Preferences pref;
    private String serviceid = "";
    private String imageUrl = "";
    private String name = "";
    ImageLoader imageLoader;
    DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        if (getIntent().hasExtra("service_id")) {
            serviceid = getIntent().getStringExtra("service_id");
        }
        if (getIntent().hasExtra("image_url")) {
            imageUrl = getIntent().getStringExtra("image_url");
        }
        if (getIntent().hasExtra("name")) {
            name = getIntent().getStringExtra("name");
        }
        initView();
    }

    private void initView() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        customVolley = new CustomVolley(this);
        pref = new Preferences(this);
        dialogview = new DialogView(this);
        mQueue = Volley.newRequestQueue(this);
        ivPic = (ImageView) findViewById(R.id.ivPic);
        imageLoader.displayImage(ConFig_URL.IMAGE_URL + imageUrl, ivPic, options);
        tvName = (TextView) findViewById(R.id.tvName);
        tvName.setText(name);
        etComments = (EditText) findViewById(R.id.etComments);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        professionalRatingBar = (RatingBar) findViewById(R.id.professionalRatingBar);
        friendlyRatingBar = (RatingBar) findViewById(R.id.friendlyRatingBar);
        communicationRatingBar = (RatingBar) findViewById(R.id.communicationRatingBar);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        imgCross = (ImageButton) findViewById(R.id.img_cross);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        btnSubmit.setOnClickListener(this);
        imgCross.setOnClickListener(this);
        imgBtnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                volleyRatingTask();
                break;
            case R.id.img_cross:
                onBackPressed();
                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }

    private void volleyRatingTask() {
        HashMap<String, String> params = getRateServiceParams();
        customVolley.ServiceRequest(this, ConFig_URL.URL_RATING, params);
//       
    }

    private HashMap<String, String> getRateServiceParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(this, Preferences.PHONE));
        params.put("service_id", serviceid);
        params.put("rating", String.valueOf(ratingBar.getRating()));
        params.put("comment", etComments.getText().toString().trim());
        params.put("professional", String.valueOf(professionalRatingBar.getRating()));
        params.put("friendly", String.valueOf(friendlyRatingBar.getRating()));
        params.put("communication", String.valueOf(communicationRatingBar.getRating()));
        Log.d(TAG, params.toString());
        return params;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        if (Type.equals(ConFig_URL.URL_RATING)) {
            try {
                System.out.println("result = " + response);
                JSONObject object = new JSONObject(response);
                if (object != null) {
                    if (object.optString("success").toString().trim().equals("0")) {
                        CustomDialog(RatingActivity.this, getResources().getString(R.string.app_name), object.optString("msg"));
                    }
                }
            } catch (JSONException e) {
                dialogview.showCustomSingleButtonDialog(RatingActivity.this,
                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    public void CustomDialog(Context context, String header,
                             String msg) {
        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                Intent intent = new Intent();
                intent.putExtra("service_id", serviceid);
                setResult(Activity.RESULT_OK, intent);
                finish();
                dialog.dismiss();


            }
        });


        android.app.AlertDialog alert = adb.create();
        alert.show();
    }
}
