package com.uipl.qber;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.uipl.qber.utils.PushHandler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

/**
 * Created by richa on 15/12/16.
 */
public class CustomFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCMService";
    private ServiceStatusChange serviceStatusChange = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "Data: " + remoteMessage.getData());
        if (remoteMessage.getData() != null) {

            Map<String, String> data = remoteMessage.getData();
            String type = "", id = "", providerName = "";
            id = data.get("id");
            type = data.get("type");
            providerName = data.get("provider_name");

            if (id != null && !id.toString().trim().equals("") && type != null && !type.toString().trim().equals("")) {
                String msg = "";
                if (type.equals("jobaccepted")) {
                    msg = String.format(getResources().getString(R.string.jobaccepted), providerName);
                    sendNotification(msg, id, type,providerName);
                    PushHandler.onServiceStatusChange(id, type, providerName, msg);
                } else if (type.equals("jobwait")) {
                    msg = String.format(getResources().getString(R.string.jobwait), providerName);
                    sendNotification(msg, id, type, providerName);
                    PushHandler.onServiceStatusChange(id, type, providerName, msg);
                } else if (type.equals("rejected")) {
                    msg = getResources().getString(R.string.rejected);
                    sendNotification(msg, id, type, providerName);
                    PushHandler.onServiceStatusChange(id, type, providerName, msg);
                } else if (type.equals("heading")) {
                    msg = String.format(getResources().getString(R.string.heading), providerName);
                    sendNotification(msg, id, type, providerName);
                } else if (type.equals("working")) {
                    msg = String.format(getResources().getString(R.string.working), providerName);
                    sendNotification(msg, id, type, providerName);
                } else if (type.equals("jobcompleted")) {
                    msg = String.format(getResources().getString(R.string.jobcompleted), providerName);
                    sendNotification(msg, id, type, providerName);
                } else if (type.equals("jobupdate")) {
                    msg = String.format(getResources().getString(R.string.jobupdate), providerName);
                    sendNotification(msg, id, type, providerName);
                } else if (type.equals("customerupdateservice")) {
                    msg = String.format(getResources().getString(R.string.jobupdate), providerName);
                    sendNotification(msg, id, type, providerName);
                } else if (type.equals("ignored")) {
                    msg = String.format(getResources().getString(R.string.ignored), providerName);
                    sendNotification(msg, id, type, providerName);
                }else if (type.equals("cancelled")) {
                    msg = String.format(getResources().getString(R.string.canceled), providerName);
                    sendNotification(msg, id, type, providerName);
                } else if (type.equals("livetrack")) {
                    PushHandler.onLocationChange(data);
                }

            }
        }
    }

    private void sendNotification(String messageBody, String id, String type, String providerName) {
        int icon = R.drawable.ic_app_logo2;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, getResources().getString(R.string.app_name), when);
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push_notification);
        contentView.setTextViewText(R.id.notification_time, getCurrentTime());
        contentView.setTextViewText(R.id.notification_text, messageBody);

        notification.contentView = contentView;
        PendingIntent resultPendingIntent=null;
        if(type.equals("heading")) {
            Intent cIntent = new Intent(this, TrackLocationActivity.class)
                    .putExtra("provider_name", providerName)
                    .putExtra("service_id", id);
            resultPendingIntent = PendingIntent.getActivity(this, 0, cIntent, PendingIntent.FLAG_ONE_SHOT);

        }else{
            resultPendingIntent = PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        }
            notification.contentIntent = resultPendingIntent;
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(Integer.parseInt(id), notification);
    }


    String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        String outputDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        outputDate = dateFormat.format(c.getTime());
        return outputDate;
    }

    private static int generateRandomInt() {
        final Random myRandom = new Random();
        return myRandom.nextInt();
    }

    public void setOnServicesStatusChange(ServiceStatusChange servicesStatus) {
        this.serviceStatusChange = servicesStatus;
    }
}
