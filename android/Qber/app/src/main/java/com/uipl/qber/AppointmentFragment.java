package com.uipl.qber;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.uipl.qber.Adapter.AppointmentListAdapter;
import com.uipl.qber.Model.BookedAppointment;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppointmentFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, VolleyInterface {
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    TextView tvEmptyView;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<BookedAppointment> bookedAppointList = new ArrayList<>();
    AppointmentListAdapter adapter;
    RequestQueue mQueue;
    public static String TAG = "AppointmentFragment";
    private DialogView dialogview;
    private CustomVolley customVolley;
    private Preferences pref;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date todayDate = null, tomorrowDate = null;

    public AppointmentFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);
        ((MenuActivity) getActivity()).tvTitle.setText("Booked Appointment");
        initViews(view);
        setDate();
        volleyBookTask();
        return view;
    }

    private void setDate() {
        try {
            Calendar calendar = Calendar.getInstance();
            todayDate = sdf.parse(sdf.format(calendar.getTime()));
            calendar.add(Calendar.DATE, 1);
            tomorrowDate = sdf.parse(sdf.format(calendar.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void checkAdapterIsEmpty() {
        if (adapter.getItemCount() == 0) {
            tvEmptyView.setVisibility(View.VISIBLE);
        } else {
            tvEmptyView.setVisibility(View.GONE);
        }
    }

    void initViews(View view) {
        mQueue = Volley.newRequestQueue(getActivity());
        dialogview = new DialogView(getActivity());
        customVolley = new CustomVolley(getActivity());
        pref = new Preferences(getActivity());

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srlCarService);
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setRefreshing(false);
        tvEmptyView = (TextView) view.findViewById(R.id.tvEmptyView);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvCarService);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AppointmentListAdapter(getActivity(), bookedAppointList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onRefresh() {
        tvEmptyView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(GlobalVariable.cancelAppointment){
            GlobalVariable.cancelAppointment=false;
            volleyBookTask();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        customVolley.cancelRequest(ConFig_URL.URL_BOOKED_SERVICE);
    }

    private void volleyBookTask() {
        HashMap<String, String> params = getBookedServiceParams();
        customVolley.ServiceRequest(this, ConFig_URL.URL_BOOKED_SERVICE, params);
    }

    private HashMap<String, String> getBookedServiceParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
        return params;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        if (Type.equals(ConFig_URL.URL_BOOKED_SERVICE)) {
            try {
                System.out.println("result = " + response);
                JSONObject object = new JSONObject(response);
                if (object != null) {
                    if (object.optString("success").equals("0")) {
                        bookedAppointList.clear();
                        JSONArray dataArray = object.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            BookedAppointment bookedAppointment = new BookedAppointment();
                            bookedAppointment.setServiceName(dataObj.optString("service_name"));
                            bookedAppointment.setServiceId(dataObj.optString("id"));
                            JSONObject providerObj = dataObj.getJSONObject("provider");
                            bookedAppointment.setName(providerObj.optString("name"));
                            Date date = null;
                            try {
                                date = sdf.parse(dataObj.optString("from_time"));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (date != null && todayDate != null && tomorrowDate != null) {
                                if (date.compareTo(todayDate) == 0) {
                                    bookedAppointment.setArrivalDate("Today");
                                } else if (date.compareTo(tomorrowDate) == 0) {
                                    bookedAppointment.setArrivalDate("Tomorrow");
                                } else {
                                    bookedAppointment.setArrivalDate(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", dataObj.optString("from_time")));
                                }
                            } else {
                                bookedAppointment.setArrivalDate(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", dataObj.optString("from_time")));
                            }
                            bookedAppointment.setArrivalTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "h:mm a", dataObj.optString("from_time")));
                            bookedAppointment.setMapPic(dataObj.optString("map_url"));
                            bookedAppointment.setPic(providerObj.optString("image"));
                            bookedAppointment.setServiceCode(dataObj.optString("expected_cost"));
                            JSONArray serviceNotificationArray = dataObj.getJSONArray("service_notification");
                            if (serviceNotificationArray != null && serviceNotificationArray.length() > 0) {
                                JSONObject serviceNotificationObject = serviceNotificationArray.getJSONObject(0);
                                String status = serviceNotificationObject.optString("service_notice_status");
                                if (status != null && !status.equals("") && !status.equals("null") && status.length() > 1) {
                                    status = status.substring(0, 1).toUpperCase() + status.substring(1);
                                    bookedAppointment.setStatus(status);
                                } else {
                                    bookedAppointment.setStatus("");
                                }
                                bookedAppointment.setServiceId(serviceNotificationObject.optString("service_id"));
                            }
                            bookedAppointList.add(bookedAppointment);
                            adapter.notifyDataSetChanged();
                            if (bookedAppointList.size() != 0) {
                                MenuActivity.tvCounter.setVisibility(View.VISIBLE);
                                MenuActivity.tvCounter.setText(String.valueOf(bookedAppointList.size()));
                            } else {
                                MenuActivity.tvCounter.setVisibility(View.GONE);
                                MenuActivity.tvCounter.setText("");
                            }
                            checkAdapterIsEmpty();

                        }

                    }

                }
            } catch (JSONException e) {
                dialogview.showCustomSingleButtonDialog(getActivity(),
                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
            }
        }
    }


}

