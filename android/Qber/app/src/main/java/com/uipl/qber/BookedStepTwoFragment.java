package com.uipl.qber;

import android.*;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.FlowLayout;
import com.uipl.qber.utils.GPSTracker;
import com.uipl.qber.utils.Permission;
import com.uipl.qber.utils.PixelConversion;
import com.uipl.qber.utils.TypedfaceTextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Fragment to display Location
 */
public class BookedStepTwoFragment extends Fragment implements com.google.android.gms.maps.OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {

    private static final int ACTIVITY_REQUEST_CODE_FOR_MAP = 102;
    View v;
    FlowLayout flFlow;
    RelativeLayout imvMapLayer;
    TypedfaceTextView tvNext;
    ArrayList<String> item = new ArrayList<>();
    String currentType = "Select from map";
    int Width = 0;
    GPSTracker gpsTracker;
    Double lat = 0.0, lng = 0.0;
    Double currentLat = 0.0, currentLng = 0.0;
    private GoogleMap mMap;
    private String address = "";
    private TextView tvTitle;
    private CustomInfoWindow customInfoWindow;
    private Marker marker;
    private boolean addressFlag = false;
    private LinearLayout llMainInfoWindow;
    private DialogView dialogView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_booked_step_two, container, false);

        init();
        inflateItemInFlow();
        setListener();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return v;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.clear();
        LatLng latLng = new LatLng(currentLat, currentLng);
        marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map1)).snippet(getString(R.string.set_service_location)));
        customInfoWindow = new CustomInfoWindow();
        mMap.setInfoWindowAdapter(customInfoWindow);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (lat == 0.0 && lng == 0.0) {
                    lat = currentLat;
                    lng = currentLng;
                }
                if (address.equals(getString(R.string.set_service_location))) {
                    address = "";
                }
                startActivityForResult(new Intent(getActivity(), LocationSelectMapActivity.class).putExtra("LAT", String.valueOf(lat)).putExtra("LNG", String.valueOf(lng)).putExtra("ADDRESS", address), ACTIVITY_REQUEST_CODE_FOR_MAP);
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        PermissionChecking();
        marker.showInfoWindow();
        mMap.setOnMapClickListener(this);
        getLocation();
    }

    /**
     * Method for setOnclickListener
     */
    private void setListener() {
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!GlobalVariable.setLocation) {
                    dialogView.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.select_service_location));

                } else {
                    BookedActivity.viewpagerBook.setCurrentItem(2);
                }
            }
        });
    }

    /**
     * Method to add Location item
     */
    private void inflateItemInFlow() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Width = size.x - PixelConversion.dpToPx(getActivity(), 30);
        Width = Width / 2;
        final int itemSize = item.size();


        for (int i = 0; i < itemSize; i++) {
            final int pos = i;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View myView = inflater.inflate(R.layout.item_locations, null);

            final RelativeLayout rlMain = (RelativeLayout) myView.findViewById(R.id.rlMain);
            rlMain.getLayoutParams().width = Width;
            final ImageView imvCheck = (ImageView) myView.findViewById(R.id.imvCheck);
            final TypedfaceTextView tvName = (TypedfaceTextView) myView.findViewById(R.id.tvName);
            tvName.setText(item.get(pos));

            if (currentType.equals(item.get(pos))) {
                rlMain.setBackground(getActivity().getResources().getDrawable(R.drawable.maroon_oval));
                imvCheck.setBackgroundResource(R.drawable.check_ico2);
                tvName.setTextColor(getActivity().getResources().getColor(R.color.white));
            } else {
                rlMain.setBackground(getActivity().getResources().getDrawable(R.drawable.rounded_border_gray));
                imvCheck.setBackgroundResource(R.drawable.check_ico1);
                tvName.setTextColor(getActivity().getResources().getColor(R.color.text_color_4));
            }

            rlMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentType = item.get(pos);
                    for (int j = 0; j < itemSize; j++) {
                        if (currentType.equals(item.get(j))) {
                            View view = flFlow.getChildAt(j);
                            final RelativeLayout rlMain = (RelativeLayout) view.findViewById(R.id.rlMain);
                            final ImageView imvCheck = (ImageView) view.findViewById(R.id.imvCheck);
                            final TypedfaceTextView tvName = (TypedfaceTextView) view.findViewById(R.id.tvName);
                            rlMain.setBackground(getActivity().getResources().getDrawable(R.drawable.maroon_oval));
                            imvCheck.setBackgroundResource(R.drawable.check_ico2);
                            tvName.setTextColor(getActivity().getResources().getColor(R.color.white));
                        } else {
                            View view = flFlow.getChildAt(j);
                            final RelativeLayout rlMain = (RelativeLayout) view.findViewById(R.id.rlMain);
                            final ImageView imvCheck = (ImageView) view.findViewById(R.id.imvCheck);
                            final TypedfaceTextView tvName = (TypedfaceTextView) view.findViewById(R.id.tvName);
                            rlMain.setBackground(getActivity().getResources().getDrawable(R.drawable.rounded_border_gray));
                            imvCheck.setBackgroundResource(R.drawable.check_ico1);
                            tvName.setTextColor(getActivity().getResources().getColor(R.color.text_color_4));
                        }
                    }

                    if (!currentType.equals("Select from map")) {
                        imvMapLayer.setVisibility(View.VISIBLE);

                        if (currentType.equals("Use my location")) {
                            getLocation();
                        } else {
                        }
                    } else {
                        if (lat == 0.0 && lng == 0.0) {
                            lat = currentLat;
                            lng = currentLng;
                        }
                        mapCameraMove(lat, lng, address);
                        imvMapLayer.setVisibility(View.GONE);
                    }
                }
            });

            flFlow.addView(myView);
        }
    }

    /**
     * Method to initialize variable
     */
    private void init() {
        item.add("Select from map");
        item.add("Use my location");
        dialogView = new DialogView(getActivity());

        flFlow = (FlowLayout) v.findViewById(R.id.flFlow);
        imvMapLayer = (RelativeLayout) v.findViewById(R.id.imvMapLayer);
        imvMapLayer.setOnClickListener(this);
        tvNext = (TypedfaceTextView) v.findViewById(R.id.tvNext);

        gpsTracker = new GPSTracker(getActivity());
    }

    /**
     * Method get current lat lng
     */
    public void getLocation() {
        if (gpsTracker.canGetLocation()) {
            currentLat = gpsTracker.getLatitude();
            currentLng = gpsTracker.getLongitude();
            mapCameraMove(currentLat, currentLng, "");


        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    /**
     * @param Lat     latitude
     * @param Lng     longitude
     * @param address location address
     *                Method to move map camera base on lat lng
     */
    public void mapCameraMove(Double Lat, Double Lng, String address) {
        LatLng latLng = new LatLng(Lat, Lng);
        if (address.equals("")) {
            address = getString(R.string.set_service_location);
        }
        if (currentType.equals("Use my location") || !address.equals(getString(R.string.set_service_location))) {
            GlobalVariable.setLocation = true;
            GlobalVariable.locationLat = Lat;
            GlobalVariable.locationLng = Lng;
        } else {
            GlobalVariable.setLocation = false;
        }
        if (mMap != null) {
            mMap.clear();
            if (customInfoWindow != null && address.equals(getString(R.string.set_service_location))) {
                addressFlag = false;
            } else if (customInfoWindow != null) {
                addressFlag = true;
            }
            marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map1)).snippet(address));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 8.0f));
            marker.showInfoWindow();
            System.out.println("latitude = " + lat);
            System.out.println("longitude = " + lng);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_REQUEST_CODE_FOR_MAP && resultCode == Activity.RESULT_OK) {
            if (data != null && data.hasExtra("lat") && data.hasExtra("lng")) {
                lat = data.getDoubleExtra("lat", lat);
                lng = data.getDoubleExtra("lng", lng);
                address = data.getStringExtra("address");
                if (address.equals("")) {
                    address = getString(R.string.set_service_location);
                }
                if (currentType.equals("Use my location") || !address.equals(getString(R.string.set_service_location))) {
                    GlobalVariable.setLocation = true;
                    GlobalVariable.locationLat = lat;
                    GlobalVariable.locationLng = lng;
                } else {
                    GlobalVariable.setLocation = false;
                }
                LatLng latLng = new LatLng(lat, lng);
                mMap.clear();
                if (customInfoWindow != null && address.equals(getString(R.string.set_service_location))) {
                    addressFlag = false;
                } else if (customInfoWindow != null) {
                    addressFlag = true;
                }
                marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map1)).snippet(address));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));
                marker.showInfoWindow();
            }
        }

    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    dialogView.showToast(getActivity(), getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (marker != null) {
            marker.showInfoWindow();
        }
        if (lat == 0.0 && lng == 0.0) {
            lat = currentLat;
            lng = currentLng;
        }
        if (address.equals(getString(R.string.set_service_location))) {
            address = "";
        }
        startActivityForResult(new Intent(getActivity(), LocationSelectMapActivity.class).putExtra("LAT", String.valueOf(lat)).putExtra("LNG", String.valueOf(lng)).putExtra("ADDRESS", address), ACTIVITY_REQUEST_CODE_FOR_MAP);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imvMapLayer:
                break;
        }
    }

    /**
     * InfoWindowAdapter to set custom infoWindow
     */
    public class CustomInfoWindow implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.infowindowlayout, null);
            String title = marker.getSnippet();
            llMainInfoWindow = (LinearLayout) v.findViewById(R.id.llMainInfoWindow);
            if (addressFlag) {
                llMainInfoWindow.setBackgroundResource(R.drawable.pointer320);
            } else {
                llMainInfoWindow.setBackgroundResource(R.drawable.pointer_green);
            }
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
            return v;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }
}
