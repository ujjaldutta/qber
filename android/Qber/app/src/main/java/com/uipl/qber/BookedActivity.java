package com.uipl.qber;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;

import com.uipl.qber.Adapter.TabsPagerAdapter;
import com.uipl.qber.Model.ServiceType;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.CommonFunction;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.TypedfaceTextView;


/**
 * Activity for display services listing
 */
public class BookedActivity extends FragmentActivity {
    ImageButton ibBack, ibHelp;
    TypedfaceTextView tvHeader;
    public static ViewPager viewpagerBook;
    TabsPagerAdapter mAdapter;
    TypedfaceTextView tvOne, tvTwo, tvThree;
    ServiceType serviceType = GlobalVariable.serviceType;
    private DialogView dialogView;

    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        init();
        setListener();
    }

    /**
     * Method call when change tab
     */
    private void setListener() {
        viewpagerBook.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    tvOne.setTextColor(getResources().getColor(R.color.white));
                    tvOne.setBackground(getResources().getDrawable(R.drawable.maroon_oval));
                    tvTwo.setTextColor(getResources().getColor(R.color.text_color_2));
                    tvTwo.setBackground(getResources().getDrawable(R.drawable.white_solid_round));
                    tvThree.setTextColor(getResources().getColor(R.color.text_color_2));
                    tvThree.setBackground(getResources().getDrawable(R.drawable.white_solid_round));
                    tvHeader.setText(getResources().getText(R.string.car_wash));
                    ibHelp.setVisibility(View.GONE);
                } else if (position == 1) {
                    tvOne.setTextColor(getResources().getColor(R.color.text_color_2));
                    tvOne.setBackground(getResources().getDrawable(R.drawable.white_solid_round));
                    tvTwo.setTextColor(getResources().getColor(R.color.white));
                    tvTwo.setBackground(getResources().getDrawable(R.drawable.maroon_oval));
                    tvThree.setTextColor(getResources().getColor(R.color.text_color_2));
                    tvThree.setBackground(getResources().getDrawable(R.drawable.white_solid_round));
                    tvHeader.setText(getResources().getText(R.string.select_location));
                    ibHelp.setVisibility(View.VISIBLE);
                } else if (position == 2) {
                    tvOne.setTextColor(getResources().getColor(R.color.text_color_2));
                    tvOne.setBackground(getResources().getDrawable(R.drawable.white_solid_round));
                    tvTwo.setTextColor(getResources().getColor(R.color.text_color_2));
                    tvTwo.setBackground(getResources().getDrawable(R.drawable.white_solid_round));
                    tvThree.setTextColor(getResources().getColor(R.color.white));
                    tvThree.setBackground(getResources().getDrawable(R.drawable.maroon_oval));
                    tvHeader.setText(getResources().getText(R.string.arrival_time));
                    ibHelp.setVisibility(View.GONE);
                }
                CommonFunction.hideKeyboard(BookedActivity.this);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tvOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewpagerBook.setCurrentItem(0);
            }
        });


        tvTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookedStepOneFragment bookedStepOneFragment = null;
                if (mAdapter != null) {
                    bookedStepOneFragment = (BookedStepOneFragment) mAdapter.getFragment(0);
                }
                if (bookedStepOneFragment != null && !bookedStepOneFragment.checkAddView()) {
                    dialogView.showCustomSingleButtonDialog(BookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_service));
                } else {
                    if (GlobalVariable.totalServicesAmount <= Integer.parseInt(serviceType.getMinCost())) {
                        dialogView.showCustomSingleButtonDialog(BookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.minimum_order_amount_should_be) + " " + serviceType.getMinCost());

                    } else {
                        viewpagerBook.setCurrentItem(1);
                    }
                }
            }
        });

        tvThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BookedStepOneFragment bookedStepOneFragment = null;
                if (mAdapter != null) {
                    bookedStepOneFragment = (BookedStepOneFragment) mAdapter.getFragment(0);
                }
                if (bookedStepOneFragment != null && !bookedStepOneFragment.checkAddView()) {
                    dialogView.showCustomSingleButtonDialog(BookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_service));
                } else {
                    if (GlobalVariable.totalServicesAmount <= Integer.parseInt(serviceType.getMinCost())) {
                        dialogView.showCustomSingleButtonDialog(BookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.minimum_order_amount_should_be) + " " + serviceType.getMinCost());

                    } else if (!GlobalVariable.setLocation) {
                        dialogView.showCustomSingleButtonDialog(BookedActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.select_service_location));

                    } else {
                        viewpagerBook.setCurrentItem(2);
                    }
                }
            }
        });

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewpagerBook != null) {
                    int viewPosition = viewpagerBook.getCurrentItem();
                    if (viewPosition < 1) {
                        finish();
                    } else {
                        viewpagerBook.setCurrentItem(viewPosition - 1);
                    }

                } else {
                    finish();
                }
            }
        });

        ibHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogView.showCustomSingleButtonDialog(BookedActivity.this, "", getResources().getString(R.string.help_msg3));
            }
        });
    }

    /**
     * Method for initialize variable
     */
    private void init() {
        dialogView = new DialogView(this);
        ibBack = (ImageButton) findViewById(R.id.ibBack);
        ibHelp = (ImageButton) findViewById(R.id.ibHelp);
        tvHeader = (TypedfaceTextView) findViewById(R.id.tvHeader);
        viewpagerBook = (ViewPager) findViewById(R.id.viewpagerBook);
        viewpagerBook.setOffscreenPageLimit(3);
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        viewpagerBook.setAdapter(mAdapter);
        tvOne = (TypedfaceTextView) findViewById(R.id.tvOne);
        tvTwo = (TypedfaceTextView) findViewById(R.id.tvTwo);
        tvThree = (TypedfaceTextView) findViewById(R.id.tvThree);
    }


}
