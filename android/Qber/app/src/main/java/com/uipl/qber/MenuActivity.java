package com.uipl.qber;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MenuActivity extends AppCompatActivity
        implements View.OnClickListener {
    public static ImageButton imgBtnMenu, imgBtnDone;
    int selectPosition = 0;
    Preferences pref;
    public TextView tvTitle;
    private LinearLayout llMainMenu, llCurrentServices, llPastServices, llContactUs, llProfile, llVideoTutorial, llLogout;
    private TextView tvMenuMain, tvCurrentServices, tvPastServices, tvContactUs, tvProfile, tvVideoTutorial, tvMenuLogout;
    private RequestQueue mQueue;
    private DialogView dialogview;
    private String TAG = "MenuActivity";
    public static TextView tvCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        pref = new Preferences(this);
        mQueue = Volley.newRequestQueue(this);
        dialogview = new DialogView(MenuActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        initViews();
        toolbarSetup();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);

        ServicesListingFragment servicesListingFragment = new ServicesListingFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, servicesListingFragment, ServicesListingFragment.TAG).addToBackStack(ServicesListingFragment.TAG).commit();

    }

    /**
     * Method to setup toolbar
     */
    private void toolbarSetup() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        imgBtnDone = (ImageButton) findViewById(R.id.imgBtnDone);
        imgBtnMenu = (ImageButton) findViewById(R.id.imgBtnMenu);
        imgBtnMenu.setOnClickListener(this);

    }

    /**
     * Initialize variable
     */
    private void initViews() {

        llMainMenu = (LinearLayout) findViewById(R.id.llMainMenu);
        tvMenuMain = (TextView) findViewById(R.id.tvMenuMain);
        llMainMenu.setOnClickListener(this);

        tvCounter=(TextView)findViewById(R.id.tvCounter);

        llCurrentServices = (LinearLayout) findViewById(R.id.llCurrentServices);
        tvCurrentServices = (TextView) findViewById(R.id.tvCurrentServices);
        llCurrentServices.setOnClickListener(this);

        llPastServices = (LinearLayout) findViewById(R.id.llPastServices);
        tvPastServices = (TextView) findViewById(R.id.tvPastServices);
        llPastServices.setOnClickListener(this);

        llContactUs = (LinearLayout) findViewById(R.id.llContactUs);
        tvContactUs = (TextView) findViewById(R.id.tvContactUs);
        llContactUs.setOnClickListener(this);

        llProfile = (LinearLayout) findViewById(R.id.llProfile);
        tvProfile = (TextView) findViewById(R.id.tvProfile);
        llProfile.setOnClickListener(this);

        llVideoTutorial = (LinearLayout) findViewById(R.id.llVideoTutorial);
        tvVideoTutorial = (TextView) findViewById(R.id.tvVideoTutorial);
        llVideoTutorial.setOnClickListener(this);

        llLogout = (LinearLayout) findViewById(R.id.llLogout);
        tvMenuLogout = (TextView) findViewById(R.id.tvMenuLogout);
        llLogout.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBtnMenu:
                closeNavigationView();
                break;
            case R.id.llMainMenu:
                closeNavigationView();
                ServicesListingFragment servicesListingFragment = new ServicesListingFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, servicesListingFragment, ServicesListingFragment.TAG).addToBackStack(ServicesListingFragment.TAG).commit();

                break;
            case R.id.llCurrentServices:
//                Toast.makeText(MenuActivity.this,"coming Soon",Toast.LENGTH_SHORT).show();
                closeNavigationView();
                GlobalVariable.EditAppointmentFlag=false;
                AppointmentFragment appointFragment = new AppointmentFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, appointFragment,AppointmentFragment.TAG).addToBackStack(AppointmentFragment.TAG).commit();

                break;
            case R.id.llPastServices:
//                Toast.makeText(MenuActivity.this,"coming Soon",Toast.LENGTH_SHORT).show();
                closeNavigationView();
                PastServiceFragment pastServiceFragment = new PastServiceFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, pastServiceFragment,PastServiceFragment.TAG).addToBackStack(PastServiceFragment.TAG).commit();

                break;
            case R.id.llContactUs:
                Toast.makeText(MenuActivity.this,"coming Soon",Toast.LENGTH_SHORT).show();
                break;
            case R.id.llProfile:
                Toast.makeText(MenuActivity.this,"coming Soon",Toast.LENGTH_SHORT).show();
                break;
            case R.id.llVideoTutorial:
                Toast.makeText(MenuActivity.this,"coming Soon",Toast.LENGTH_SHORT).show();
                break;
            case R.id.llLogout:
                LogOutDialog(MenuActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_logout));
                break;
        }
    }

    /**
     * Method use to open/close Navigation View
     */
    public void closeNavigationView() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }


    public void LogOutDialog(Context context, String header,
                             String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!pref.getBooleanPreference(MenuActivity.this,Preferences.SKIP)) {
                    VolleyLogoutTask();
                }else {
                    pref.clearAllPref();
                    selectPosition = 0;
                    startActivity(new Intent(MenuActivity.this, RegisterLoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

                }
                dialog.dismiss();


            }
        });
        adb.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();


            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

    private void VolleyLogoutTask() {
        dialogview.showCustomSpinProgress(MenuActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_LOGOUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    pref.clearAllPref();
                                    selectPosition = 0;
                                    startActivity(new Intent(MenuActivity.this, RegisterLoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

                                } else {
                                    dialogview.showCustomSingleButtonDialog(MenuActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }


                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(MenuActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(MenuActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(MenuActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(MenuActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(MenuActivity.this, Preferences.PHONE));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment=getFragmentManager().findFragmentById(R.id.fragment_container);
        if(fragment!=null && fragment instanceof PastServiceFragment){
            fragment.onActivityResult(requestCode,resultCode,data);
        }
    }
}
