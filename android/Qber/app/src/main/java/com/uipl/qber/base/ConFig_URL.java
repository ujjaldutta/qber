package com.uipl.qber.base;

/**
 * Created by abhijit on 19/9/16.
 */
public class ConFig_URL {
    public static final String BASEURL = "http://uiplonline.com/qber-app/master/api/customer/";
    public static final String IMAGE_URL = "http://uiplonline.com/qber-app/master/";
    public static final String SERVICE_BASE_URL = "http://uiplonline.com/qber-app/master/api/service/";

    /*public static final String BASEURL = "http://demo.uiplonline.com/qber-app/dev/api/customer/";
    public static final String SERVICE_BASE_URL = "http://demo.uiplonline.com/qber-app/dev/api/service/";
    public static final String IMAGE_URL = "http://demo.uiplonline.com/qber-app/dev/public/";*/


    public static final String URL_LOGIN = BASEURL + "login";
    public static final String URL_SIGNUP = BASEURL + "signup";
    public static final String URL_LOGOUT = BASEURL + "logout";
    public static final String URL_SMS_VERIFY = BASEURL + "verify-sms";
    public static final String URL_RESEND_VERIFY_CODE = BASEURL + "resend-verify-sms";
    public static final String URL_FORGOT_PASSWORD = BASEURL + "forgot-password";
    public static final String URL_CREATE_SERVICE = BASEURL + "create-service";
    public static final String URL_UPDATE_DEVICE = BASEURL + "update-device";

    public static final String URL_RATING = BASEURL+ "send-rating";
    public static final String URL_OPEN_DISPUTE=BASEURL+"open-dispute";
    public static final String URL_BOOKING_DETAILS=BASEURL+"view-appointment";
    public static final String URL_REPEAT=BASEURL+"repeat-service";
    public static final String URL_LIST_SERVICE=BASEURL+"current-service";
    public static final String URL_BOOKED_SERVICE=BASEURL+"booked-appointments";
    public static final String URL_CANCEL_REQUEST=BASEURL+"cancel-appointment";
    public static final String URL_UPDATE_CAR_DETAILS=BASEURL+"update-cardetails";

    public static final String URL_SERVICE_NOTICE = SERVICE_BASE_URL + "notice";
    public static final String URL_SERVICE_TYPE = SERVICE_BASE_URL + "type";
    public static final String URL_LIST_PROVIDERS = SERVICE_BASE_URL + "list-providers";
    public static final String URL_GET_PROFILE = SERVICE_BASE_URL + "get-provider-profile";
    public static final String URL_REASON_LIST = SERVICE_BASE_URL + "cancel-reason";



}
