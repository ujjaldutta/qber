package com.uipl.qber.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

	public static String getFormattedTime(String inputPattern,
			String outputPattern, String time) {
		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern,
				Locale.getDefault());
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern,
				Locale.getDefault());

		Date date = null;
		String str = "";

		try {
			date = inputFormat.parse(time);
			str = outputFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return str;
	}

	public static String Convert24to12(String time) {
		String convertedTime = "";
		try {
			SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a",
					Locale.getDefault());
			SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm",
					Locale.getDefault());
			Date date = parseFormat.parse(time);
			convertedTime = displayFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedTime;
	}

	public static String Convert12to24(String time) {
		String convertedTime = "";
		try {
			SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm",
					Locale.getDefault());
			SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a",
					Locale.getDefault());
			Date date = parseFormat.parse(time);
			convertedTime = displayFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedTime;
	}

	public static Date getDateFromString(String mDate, String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern,
				Locale.getDefault());
		Date convertedDate = new Date();
		try {
			convertedDate = dateFormat.parse(mDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return convertedDate;
	}
}
