package com.uipl.qber.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

/**
 * Created by Ankan on 3/25/2016.
 */
public class CorrectifyOrrientation {

    public static Bitmap decodeFile(String path) {

        int orientation;
        Bitmap bitmap = null;
        Bitmap outputBitmap = null;

        try {

            if (path == null) {

                return null;
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 6;
            bitmap = BitmapFactory.decodeFile(path, options);
//            for (options.inSampleSize = 1; options.inSampleSize <= 32; options.inSampleSize++) {
//                try {
//                    bitmap = BitmapFactory.decodeFile(path, options);
//                    Log.d("TAG_LOG", "Decoded successfully for sampleSize " + options.inSampleSize);
//                    break;
//                } catch (OutOfMemoryError outOfMemoryError) {
//// If an OutOfMemoryError occurred, we continue with for loop and next inSampleSize value
//                    Log.e("TAG_LOG", "outOfMemoryError while reading file for sampleSize " + options.inSampleSize
//                            + " retrying with higher value");
//                }
//            }

            ExifInterface exif = new ExifInterface(path);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.e("orientation", "" + orientation);
            Matrix m = new Matrix();

            if ((orientation == 3)) {

                m.setRotate(180);

//               if(m.preRotate(90)){
                Log.e("in orientation", "" + orientation);

                outputBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != null)
                    bitmap.recycle();
                return outputBitmap;
            } else if (orientation == 6) {

                m.setRotate(90);

                Log.e("in orientation", "" + orientation);

                outputBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != null)
                    bitmap.recycle();
                return outputBitmap;
            } else if (orientation == 8) {

                m.setRotate(270);

                Log.e("in orientation", "" + orientation);

                outputBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != null)
                    bitmap.recycle();
                return outputBitmap;
            }
            return bitmap;
        } catch (Exception e) {
        }
        return null;
    }
}
