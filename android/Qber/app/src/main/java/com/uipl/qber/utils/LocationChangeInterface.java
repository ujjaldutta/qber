package com.uipl.qber.utils;

import java.util.Map;

/**
 * Created by pallab on 6/1/17.
 */

public interface LocationChangeInterface {
    void onLocationChange(Map<String, String> data);

}
