package com.uipl.qber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.InputVadidation;
import com.uipl.qber.utils.SnackBarCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Activity for forgot password
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton imgBtnBack;
    private EditText etMobile;
    private Button btnSubmit;
    private String TAG="ForgotPasswordActivity";
    private DialogView dialogview;

    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initView();
    }

    /**
     * Method to initialize variable
     */
    private void initView() {
        dialogview = new DialogView(ForgotPasswordActivity.this);
        mQueue = Volley.newRequestQueue(this);
        imgBtnBack=(ImageButton)findViewById(R.id.imgBtnBack);
        imgBtnBack.setOnClickListener(this);
        etMobile=(EditText)findViewById(R.id.etMobile);
        btnSubmit=(Button)findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                if (!InputVadidation.isEditTextHasvalue(etMobile)) {
                    etMobile.requestFocus();
                    SnackBarCustom.showSnackWithActionButton(etMobile, getResources().getString(R.string.please_enter_mobile_number));
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
                }else {
                   volleyServiceForgotPassword();
                }
                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }

    /**
     * Method to call web services for forgot password
     */
    private void volleyServiceForgotPassword() {
        dialogview.showCustomSpinProgress(ForgotPasswordActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_FORGOT_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    showCustomSingleButtonDialog(ForgotPasswordActivity.this,
                                            getResources().getString(R.string.success), object.optString("msg"));
                                }else  {
                                    dialogview.showCustomSingleButtonDialog(ForgotPasswordActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dialogview.showCustomSingleButtonDialog(ForgotPasswordActivity.this,
                                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if(error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(ForgotPasswordActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                }else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(ForgotPasswordActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(ForgotPasswordActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", GlobalVariable.ISD_CODE+etMobile.getText().toString().trim());
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    /**
     * @param context Context
     * @param header Header of Dialog
     * @param msg Display Message
     *  Method to display custom single button dialog
     */
    public void showCustomSingleButtonDialog(Context context, String header,
                                             String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                onBackPressed();


            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }
}


