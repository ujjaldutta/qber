package com.uipl.qber;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.GPSTracker;
import com.uipl.qber.utils.Permission;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.SnackBarCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private EditText etMobile, etPassword;
    private Button btnLogin;
    private TextView tvForgotPassword;
    private DialogView dialogview;
    String str_message = "";
    private String TAG = "LoginActivity";
    private Preferences pref;
    private RequestQueue mQueue;
    private ImageButton imgBtnBack;
    String callingActivity = "";
    private GPSTracker gpsTracker;
    double latitude=0.0,longitude=0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref = new Preferences(LoginActivity.this);
        if (getIntent().hasExtra(ProviderListingActivity.TAG)) {
            callingActivity = getIntent().getStringExtra(ProviderListingActivity.TAG);
        }

        PermissionChecking();
        initView();
    }

    private void initView() {
        dialogview = new DialogView(LoginActivity.this);
        mQueue = Volley.newRequestQueue(this);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        imgBtnBack.setOnClickListener(this);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);

        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (Validation()) {
                    dialogview.showCustomSpinProgress(LoginActivity.this);
                   new getFirebaseToken().execute();
                }
                break;
            case R.id.tvForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }

    /**
     * @param mobile
     * @param password Method to call web service for login
     * @param fireBaseKey
     */
    private void volleyCustomerLoginTask(final String mobile, final String password, final String fireBaseKey) {
       // dialogview.showCustomSpinProgress(LoginActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            System.out.println("result = " + response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject dataObj = object.optJSONObject("data");
                                    pref.storeStringPreference(LoginActivity.this, Preferences.USER_ID, dataObj.optString("id"));
                                    pref.storeStringPreference(LoginActivity.this, Preferences.PHONE, dataObj.optString("phone"));
                                    pref.storeStringPreference(LoginActivity.this, Preferences.TOKEN, dataObj.optString("token"));
                                    if (!callingActivity.equals(ProviderListingActivity.TAG)) {
                                        startActivity(new Intent(LoginActivity.this, MenuActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    }
                                    finish();
                                } else if (object.optString("success").equals("2")) {
                                    startActivity(new Intent(LoginActivity.this, MobileVerificationActivity.class).putExtra("SMS_CODE", "").putExtra("PHONE", etMobile.getText().toString().trim()));
                                } else {
                                    str_message = object.optString("msg");
                                    dialogview.showCustomSingleButtonDialog(LoginActivity.this,
                                            getResources().getString(R.string.sorry), str_message);
                                }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            Log.e("Error", "Error");

                            dialogview.showCustomSingleButtonDialog(LoginActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(LoginActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(LoginActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(LoginActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", GlobalVariable.ISD_CODE + mobile);
                params.put("password", password);
                params.put("device_id", fireBaseKey);
                params.put("device_type", "android");
                if(latitude!=0.0 && longitude!=0.0) {
                    params.put("latitude", String.valueOf(latitude));
                    params.put("longitude", String.valueOf(longitude));
                }
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * @return true if all the data are valid
     * Method to check validation
     */
    public boolean Validation() {
        if (etMobile.getText().toString().trim().isEmpty()) {
            SnackBarCustom.showSnackWithActionButton(etMobile, getResources().getString(R.string.please_enter_mobile_number));
            etMobile.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etPassword, getResources().getString(R.string.please_enter_password));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);

            return false;
        }

        return true;
    }

    class getFirebaseToken extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e(TAG, "Refreshed token: " + refreshedToken);
            return refreshedToken;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null && !s.toString().equals("")) {
                    volleyCustomerLoginTask(etMobile.getText().toString().trim(), etPassword.getText().toString().trim(), s.toString());

            } else {
                dialogview.dismissCustomSpinProgress();
                Log.e(TAG, "null firebase token");
            }
            super.onPostExecute(s);
        }
    }


    /**
     * Method to get current latitude and longitude
     */
    public void getLocation() {
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();


        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(LoginActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

            getLocation();
        } else {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getLocation();
                } else {
                    dialogview.showToast(LoginActivity.this, getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }



}
