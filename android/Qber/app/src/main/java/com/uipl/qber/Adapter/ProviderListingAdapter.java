package com.uipl.qber.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.CustomerProfileActivity;
import com.uipl.qber.Model.Service;
import com.uipl.qber.ProviderListingActivity;
import com.uipl.qber.R;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ProviderListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements VolleyInterface {

    private static final int ACTIVITY_RETURN_REQUEST_CODE = 101;
    private final Preferences pref;
    ArrayList<Service> serviceArrayList = new ArrayList<>();
    Activity context;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    CustomVolley customVolley;
    DialogView dialogView;

    public ProviderListingAdapter(Activity mContext, ArrayList<Service> mArrayList) {
        this.context = mContext;
        this.serviceArrayList = mArrayList;
        dialogView = new DialogView(mContext);
        this.customVolley = new CustomVolley(mContext);
        this.pref = new Preferences(mContext);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public int getItemCount() {
        return serviceArrayList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        viewHolder = new ProviderHolder(inflater.inflate(R.layout.inflate_service, viewGroup, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        final Service service = (Service) serviceArrayList.get(position);

        ProviderHolder holder = (ProviderHolder) viewHolder;
        if (service.getProfile().getImage() != null) {
            imageLoader.displayImage(service.getProfile().getImage(), holder.ivPic, options);
        }
        holder.tvName.setText(service.getName());
        if (Float.parseFloat(service.getRatingString().toString()) > 0) {
            holder.tvRatingNum.setText("(" + service.getRatingString() + ")");
        } else {
            holder.tvRatingNum.setText("No Ratings");
        }
        if (Integer.parseInt(service.getTotalRatingCount().toString()) > 1) {
            holder.tvTotalRatingCount.setText(service.getTotalRatingCount() + " Ratings");
        } else if (Integer.parseInt(service.getTotalRatingCount().toString()) == 1) {
            holder.tvTotalRatingCount.setText(service.getTotalRatingCount() + " Rating");
        } else holder.tvTotalRatingCount.setText("");
        holder.ratingBar.setRating(service.getRatingFloat());
        holder.tvDistance.setText(context.getString(R.string.currently) + " " + service.getProviderDistance() + context.getString(R.string.km_away));

        holder.btnMoreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivityForResult(new Intent(context, CustomerProfileActivity.class)
                                .putExtra("phone", serviceArrayList.get(position).getPhone())
                                .putExtra("provider_id", serviceArrayList.get(position).getCarServiceId())
                                .putExtra("assign_job_flag", serviceArrayList.get(position).isAsignJobFlag())
                                .putExtra("currentPosition", position)
                        , ACTIVITY_RETURN_REQUEST_CODE);
            }
        });

        if (service.isAsignJobFlag()) {
            holder.btnAssignJob.setVisibility(View.GONE);
        } else {
            holder.btnAssignJob.setVisibility(View.VISIBLE);
        }
        holder.btnAssignJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProviderListingActivity) context).setAsignJobFlag(position);
                VolleyAssignJob(position);


            }
        });

    }

    private void VolleyAssignJob(int position) {
        HashMap<String, String> params = getParameter(position);
        customVolley.ServiceRequest(this, ConFig_URL.URL_CREATE_SERVICE, params);
    }

    private HashMap<String, String> getParameter(int position) {
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
        String car_type = getCarType();
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(context, Preferences.PHONE));
        params.put("type_id", GlobalVariable.serviceType.getServiceId());
        params.put("request_date", currentDate);
        params.put("latitude", String.valueOf(GlobalVariable.locationLat));
        params.put("longitude", String.valueOf(GlobalVariable.locationLng));
        params.put("expected_cost", String.valueOf(GlobalVariable.totalServicesAmount));
        params.put("from_time", GlobalVariable.serviceStartDate);
        params.put("to_time", GlobalVariable.serviceEndDate);
        params.put("provider_id", serviceArrayList.get(position).getCarServiceId());
        params.put("car_type", car_type);
        return params;
    }

    private String getCarType() {
        String car_type = "";
        try {
            JSONObject mainObj = new JSONObject();
        /*SUV*/
            JSONArray suvArray = new JSONArray();
            for (int i = 0; i < GlobalVariable.suvList.size(); i++) {
                JSONObject suvObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!GlobalVariable.suvList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("0")));
                }
                if (!GlobalVariable.suvList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("1")));
                }
                if (!GlobalVariable.suvList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    suvObject.put("option_details", optionDetails);
                }
                if(suvObject.length()!=0) {
                    suvArray.put(suvObject);
                }


            }
            mainObj.put("suv", suvArray);
            /*Sedan*/
            JSONArray sedanArray = new JSONArray();
            for (int i = 0; i < GlobalVariable.sedanList.size(); i++) {
                JSONObject sedanObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!GlobalVariable.sedanList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("0")));
                }
                if (!GlobalVariable.sedanList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("1")));
                }
                if (!GlobalVariable.sedanList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    sedanObject.put("option_details", optionDetails);
                }
                if(sedanObject.length()!=0) {
                    sedanArray.put(sedanObject);
                }

            }
            mainObj.put("sedan", sedanArray);
            car_type = mainObj.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return car_type;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        try {
            JSONObject object = new JSONObject(response);
            if (Type.equals(ConFig_URL.URL_CREATE_SERVICE)) {
                if (object.optString("success").equals("0")) {
                    showCustomSingleButtonDialog(context,
                            context.getResources().getString(R.string.success), object.optString("msg"));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }


    }

    public void showCustomSingleButtonDialog(final Context context, String header,
                                             String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

    class ProviderHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvDistance, tvRatingNum, tvTotalRatingCount;
        ImageView ivPic;
        TextView btnAssignJob, btnMoreInfo;
        RatingBar ratingBar;
        CardView cardView;
        LinearLayout llMainView;

        public ProviderHolder(View v) {
            super(v);
            llMainView = (LinearLayout) v.findViewById(R.id.llMainView);
            tvDistance = (TextView) v.findViewById(R.id.tvDistance);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvRatingNum = (TextView) v.findViewById(R.id.tvRatingValue);
            tvTotalRatingCount = (TextView) v.findViewById(R.id.tvTotalRatingCount);
            ivPic = (ImageView) v.findViewById(R.id.ivPic);
            btnAssignJob = (TextView) v.findViewById(R.id.btnAssignJob);
            btnMoreInfo = (TextView) v.findViewById(R.id.btnMoreInfo);
            cardView = (CardView) v.findViewById(R.id.cardView);
            ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
        }
    }
}