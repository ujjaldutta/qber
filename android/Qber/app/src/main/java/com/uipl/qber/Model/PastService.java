package com.uipl.qber.Model;

/**
 * Created by richa on 23/9/16.
 */
public class PastService {

    private String pic;
    private String name;
    private String arrivalDate;
    private String arrivalTime;
    private String serviceName;
    private String serviceCode;
    private String status;
    private String departTime;
    private String rating;
    private String alreadyRated;
    private String serviceId;
    private String statusId;

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDepartTime(String departTime){
        this.departTime=departTime;
    }
    public String getDepartTime()
    {
        return departTime;
    }


    public void setRating(String rating) {
        this.rating = rating;
    }
    public String getRating()
    {
        return rating;
    }

    public void setAlreadyRated(String alreadyRated) {
        this.alreadyRated = alreadyRated;
    }

    public String getAlreadyRated() {
        return alreadyRated;
    }


    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusId() {
        return statusId;
    }
}
