package com.uipl.qber;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qber.Adapter.ProviderListingAdapter;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.base.GlobalVariable;
import com.uipl.qber.Model.Profile;
import com.uipl.qber.Model.Service;
import com.uipl.qber.Model.Timeline;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DateFormatter;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.GPSTracker;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.PushHandler;
import com.uipl.qber.utils.ServiceStatusChange;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by richa on 22/9/16.
 */
public class ProviderListingActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, VolleyInterface, com.uipl.qber.ServiceStatusChange {

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    TextView tvEmptyView;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<Service> serviceArrayList = new ArrayList<>();
    ProviderListingAdapter adapter;
    RequestQueue mQueue;
    DialogView dialogView;
    GPSTracker gpsTracker;
    protected ImageLoader imageLoader;
    DisplayImageOptions options;
    Toolbar toolbar;
    TextView tvToolbarTitle, tvName, tvAverageRating, tvTotalRatings;
    RatingBar ratingBar;
    ImageButton ibBack;
    ImageView ivPic;
    TextView btnMoreInfo, btnPick;
    private CardView cardView;
    private Animation slide_down;
    private Animation slide_up;
    private GoogleMap mMap;
    TableRow trTimeline;
    RelativeLayout rlListing, rlMapView;
    String startTimeRequested = "10:30 AM", endTimeRequested = "10:00 PM";
    Marker lastMarker = null;
    int lastPosition = -1, currentPosition = -1;
    final static String TAG = "ProviderListing";
    private Preferences pref;
    private int LOGIN_RETURN_CODE = 103;
    private CustomVolley customVolley;
    private int ACTIVITY_RETURN_REQUEST_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_listing);
        pref = new Preferences(ProviderListingActivity.this);
        if (pref.getStringPreference(ProviderListingActivity.this, Preferences.PHONE).equals("")) {
            startActivityForResult(new Intent(ProviderListingActivity.this, LoginActivity.class).putExtra("CallingActivity", TAG), LOGIN_RETURN_CODE);
        } else {
            initViews();
            dialogView.showCustomSpinProgress(this);
            volleyProviderServicesListing();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        GlobalVariable.ProviderActivityFlag = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        GlobalVariable.ProviderActivityFlag = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_RETURN_CODE) {
            if (!pref.getStringPreference(ProviderListingActivity.this, Preferences.PHONE).equals("")) {
                initViews();
                dialogView.showCustomSpinProgress(this);
                volleyProviderServicesListing();
            } else {
                finish();
            }
        } else if (requestCode == ACTIVITY_RETURN_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            int pos = data.getIntExtra("currentPosition", -1);
            boolean assignJobFlag = data.getBooleanExtra("assign_job_flag", false);
            if (serviceArrayList != null && pos > -1 && pos < serviceArrayList.size() && assignJobFlag) {
                serviceArrayList.get(pos).setAsignJobFlag(assignJobFlag);
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }

        }
    }

    private void volleyProviderServicesListing() {
        final String currentTime = getCurrentTime();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_LIST_PROVIDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d(TAG, response);
                            serviceArrayList.clear();
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray = object.optJSONArray("providers");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject mainJsonObj = jsonArray.getJSONObject(i);
                                        Service service = new Service();
                                        service.setAsignJobFlag(false);
                                        service.setCarServiceId(mainJsonObj.optString("id"));
                                        service.setTypeId(mainJsonObj.optString("type_id"));
                                        service.setEmail(mainJsonObj.optString("email"));
                                        service.setPhone(mainJsonObj.optString("phone"));
                                        service.setName(mainJsonObj.optString("name"));
                                        service.setLocationPreference(mainJsonObj.optString("location_preference"));
                                        service.setProviderStartDatetime(mainJsonObj.optString("provider_start_daytime"));
                                        service.setProviderEndDatetime(mainJsonObj.optString("provider_end_daytime"));
                                        service.setProviderDistance(mainJsonObj.optString("distance"));
                                        service.setBannedDuration(mainJsonObj.optString("banned_duration"));
                                        String lat = mainJsonObj.optString("current_latitude");
                                        String lng = mainJsonObj.optString("current_longitude");
                                        if (lat != null && !lat.toString().trim().equals("") && !lat.toString().trim().equalsIgnoreCase("null")) {
                                            service.setCurrentLat(Double.parseDouble(lat));
                                        }
                                        if (lng != null && !lng.toString().trim().equals("") && !lng.toString().trim().equalsIgnoreCase("null")) {
                                            service.setCurrentLng(Double.parseDouble(lng));
                                        }
                                        String ratingAverage = mainJsonObj.optString("ratings_average");
                                        DecimalFormat df = new DecimalFormat("0.##");
                                        if (ratingAverage != null && !ratingAverage.toString().trim().equals("") && !ratingAverage.toString().trim().equalsIgnoreCase("null")) {
                                            ratingAverage = df.format(Float.parseFloat(ratingAverage));
                                            service.setRatingFloat(Float.parseFloat(ratingAverage));
                                            service.setRatingString(ratingAverage);
                                        } else {
                                            service.setRatingFloat(0f);
                                            service.setRatingString("0");
                                        }
                                        service.setTotalRatingCount(mainJsonObj.optString("ratings_count"));
                                        if (mainJsonObj.optString("sms_verified") != null && !mainJsonObj.optString("sms_verified").toString().equals("") && mainJsonObj.optString("sms_verified").toString().trim().equals("1"))
                                            service.setSmsVerified(true);
                                        else service.setSmsVerified(false);
                                        if (mainJsonObj.optString("approve_status") != null && !mainJsonObj.optString("approve_status").toString().equals("") && mainJsonObj.optString("approve_status").toString().trim().equals("1"))
                                            service.setApproveStatus(true);
                                        else service.setApproveStatus(false);
                                        if (mainJsonObj.optString("is_active") != null && !mainJsonObj.optString("is_active").toString().equals("") && mainJsonObj.optString("is_active").toString().trim().equals("1"))
                                            service.setActive(true);
                                        else service.setActive(false);
                                        if (mainJsonObj.optString("ready_to_serve") != null && !mainJsonObj.optString("ready_to_serve").toString().equals("") && mainJsonObj.optString("ready_to_serve").toString().trim().equals("1"))
                                            service.setReadyToServe(true);
                                        else service.setReadyToServe(false);
                                        if (mainJsonObj.optString("is_online") != null && !mainJsonObj.optString("is_online").toString().equals("") && mainJsonObj.optString("is_online").toString().trim().equals("1"))
                                            service.setOnline(true);
                                        else service.setOnline(false);

                                        JSONArray timeslotArray = mainJsonObj.optJSONArray("booked_timeslot");
                                        int length = timeslotArray.length();
                                        ArrayList<Timeline> timelineArrayList = new ArrayList<>();
                                        String lastEndTime = "";
                                        try {
                                            for (int a = 0; a < timeslotArray.length(); a++) {
                                                JSONObject timeslotObj = timeslotArray.getJSONObject(a);
                                                Date fromTimeAllowed = null, toTimeAllowed = null, toTimeRequested = null, fromTimeRequested = null, lastETime = null;
                                                SimpleDateFormat format24 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                SimpleDateFormat format12 = new SimpleDateFormat("hh:mm a");

                                                String fromTime = format12.format(format24.parse(timeslotObj.optString("from_time")));
                                                String toTime = format12.format(format24.parse(timeslotObj.optString("to_time")));
                                                if (a == 0 || a == length - 1) {
                                                    fromTimeAllowed = format12.parse(fromTime);
                                                    toTimeAllowed = format12.parse(toTime);
                                                    toTimeRequested = format12.parse(endTimeRequested);
                                                    fromTimeRequested = format12.parse(startTimeRequested);
                                                    if (lastEndTime != null && !lastEndTime.toString().equals("")) {
                                                        lastETime = format12.parse(lastEndTime);
                                                    }

                                                    if (fromTimeAllowed != null && fromTimeRequested != null && toTimeAllowed != null && toTimeRequested != null) {
                                                        if (a == 0 && fromTimeRequested.compareTo(fromTimeAllowed) > 0) {
                                                            fromTime = startTimeRequested;
                                                        } else if (toTimeAllowed.compareTo(toTimeRequested) > 0) {
                                                            toTime = endTimeRequested;
                                                        }
                                                    }
                                                }

                                                if (a == length - 1) {
                                                    if (fromTimeAllowed.compareTo(fromTimeRequested) > 0) {
                                                        timelineArrayList.add(setTimeline(startTimeRequested, fromTime, false));
                                                    }
                                                    timelineArrayList.add(setTimeline(fromTime, toTime, true));
                                                    if (toTimeRequested.compareTo(toTimeAllowed) > 0) {
                                                        timelineArrayList.add(setTimeline(toTime, endTimeRequested, false));
                                                    }
                                                } else if (a == length - 1) {
                                                    timelineArrayList.add(setTimeline(fromTime, toTime, true));
                                                    if (endTimeRequested.compareTo(toTime) > 0) {
                                                        timelineArrayList.add(setTimeline(toTime, endTimeRequested, true));
                                                    }
                                                } else {
                                                    lastEndTime = toTime;
                                                    if (fromTime.compareTo(lastEndTime) > 0) {
                                                        timelineArrayList.add(setTimeline(lastEndTime, fromTime, false));
                                                    }
                                                    timelineArrayList.add(setTimeline(fromTime, toTime, true));
                                                }
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        service.setTimelines(timelineArrayList);

                                        Profile p = new Profile();
                                        JSONObject profileJsonObj = mainJsonObj.optJSONObject("profile");
                                        p.setUserId(profileJsonObj.optString("id"));
                                        p.setProviderId(profileJsonObj.optString("provider_id"));
                                        p.setServiceDetails(profileJsonObj.optString("service_details"));
                                        p.setImage(ConFig_URL.IMAGE_URL + profileJsonObj.optString("image"));
                                        p.setTeamSize(profileJsonObj.optString("team_size"));
                                        p.setNationality(profileJsonObj.optString("nationality"));
                                        if (profileJsonObj.optString("own_car") != null && !profileJsonObj.optString("own_car").toString().equals("") && profileJsonObj.optString("own_car").toString().trim().equals("1"))
                                            p.setOwnCar(true);
                                        else p.setOwnCar(false);
                                        if (profileJsonObj.optString("arabic_speaking") != null && !profileJsonObj.optString("arabic_speaking").toString().equals("") && profileJsonObj.optString("arabic_speaking").toString().trim().equals("1"))
                                            p.setArabicSpeaking(true);
                                        else p.setArabicSpeaking(false);
                                        p.setMaxPurchaseLimit(profileJsonObj.optString("max_purchase_limit"));
                                        p.setCurrentCreditBalance(profileJsonObj.optString("current_credit_balance"));
                                        p.setBannerImage1(ConFig_URL.IMAGE_URL + profileJsonObj.optString("banner_img1"));
                                        p.setBannerImage2(ConFig_URL.IMAGE_URL + profileJsonObj.optString("banner_img2"));
                                        p.setBannerImage3(ConFig_URL.IMAGE_URL + profileJsonObj.optString("banner_img3"));
                                        service.setProfile(p);

                                        serviceArrayList.add(service);
                                    }


                                    if (object.optString("viewmode").equals("listview")) {
                                        rlListing.setVisibility(View.VISIBLE);
                                        rlMapView.setVisibility(View.GONE);
                                    } else if (object.optString("viewmode").equals("mapview")) {
                                        rlListing.setVisibility(View.GONE);
                                        rlMapView.setVisibility(View.VISIBLE);
                                        Service mySelf = new Service();
                                        mySelf.setName("Me");
                                        mySelf.setCurrentLat(gpsTracker.getLatitude());
                                        mySelf.setCurrentLng(gpsTracker.getLongitude());
                                        serviceArrayList.add(mySelf);
                                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
                                        mapFragment.getMapAsync(ProviderListingActivity.this);
                                    }
                                } else {
                                    dialogView.dismissCustomSpinProgress();
                                    dialogView.showCustomSingleButtonDialog(ProviderListingActivity.this, getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                                initToolbar();
                            }
                            adapter.notifyDataSetChanged();
                            dialogView.dismissCustomSpinProgress();
                            checkAdapterIsEmpty();
                            swipeRefreshLayout.setRefreshing(false);
                        } catch (JSONException e) {
                            dialogView.dismissCustomSpinProgress();
                            swipeRefreshLayout.setRefreshing(false);
                            dialogView.showCustomSingleButtonDialog(ProviderListingActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(ProviderListingActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(ProviderListingActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(ProviderListingActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(ProviderListingActivity.this, Preferences.PHONE));
                params.put("token", pref.getStringPreference(ProviderListingActivity.this, Preferences.TOKEN));
                params.put("type_id", GlobalVariable.serviceType.getServiceId());
                params.put("from_time", String.valueOf(GlobalVariable.serviceStartDate));
                params.put("to_time", String.valueOf(GlobalVariable.serviceEndDate));
                params.put("latitude", String.valueOf(GlobalVariable.locationLat));
                params.put("longitude", String.valueOf(GlobalVariable.locationLng));
                params.put("current_time", currentTime);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private String getCurrentTime() {
        String time = "";
        Calendar calendar = Calendar.getInstance();
        time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
        return time;
    }

    Timeline setTimeline(final String startTimeString, final String endTimeString, boolean isActive) {
        Timeline t1 = new Timeline();
        t1.setStartTime(startTimeString);
        t1.setEndTime(endTimeString);
        t1.setActive(isActive);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");  //2016-10-17 10:00:00
        Date startTime = null, endTime = null;
        try {
            startTime = format.parse(startTimeString);
            endTime = format.parse(endTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long difference = endTime.getTime() - startTime.getTime();
        float diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(difference);
        float d = diffInMinutes % 60;
        float diffInHours = TimeUnit.MILLISECONDS.toHours(difference);
        if (d > 0) {
            diffInHours = diffInHours + (d / 60);
        }
        BigDecimal bd = new BigDecimal(Float.toString(diffInHours));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        t1.setDiffInHours(bd.floatValue());
        return t1;
    }

    void inflateTimeLine(ArrayList<Timeline> timeArray) {
        trTimeline.removeAllViewsInLayout();
        float weightSum = 0;
        if (timeArray.size() > 0) {
            for (int i = 0; i < timeArray.size(); i++) {
                Timeline timeline = new Timeline();
                timeline.setActive(timeArray.get(i).isActive());
                timeline.setStartTime(timeArray.get(i).getStartTime());
                timeline.setEndTime(timeArray.get(i).getEndTime());
                timeline.setDiffInHours(timeArray.get(i).getDiffInHours());
                weightSum = weightSum + timeline.getDiffInHours();
                addTimeLine(timeline.getStartTime(), timeline.getEndTime(), timeline.isActive(), timeline.getDiffInHours());
            }
            Log.d(TAG, "weightSum: " + weightSum);
            trTimeline.setWeightSum(weightSum);
        } else {
            addTimeLine("", "", false, 1.0f);
            trTimeline.setWeightSum(1.0f);
        }
    }

    void addTimeLine(String startTime, String endTime, boolean isActive, float weight) {
        customView.HelveticaneueRegularTextView tv = new customView.HelveticaneueRegularTextView(this);
        TableRow.LayoutParams lparams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        lparams.weight = weight;
        tv.setGravity(Gravity.CENTER);
        tv.setLayoutParams(lparams);
        tv.setAllCaps(true);
        if (startTime.equals("") && endTime.equals("")) {
            tv.setText("Available");
        } else {
            tv.setText(startTime + " - " + endTime);
        }
        tv.setTextColor(getResources().getColor(R.color.white));
        tv.setTextSize(5.0f);
        tv.setPadding(0, 5, 0, 5);
        if (isActive) {
            tv.setBackgroundResource(R.drawable.timeline_red_rounded);
        } else {
            tv.setBackgroundResource(R.color.transparent);
        }
        trTimeline.addView(tv);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mapMarkerViewSetup();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.hideInfoWindow();
                if (!marker.getTitle().toString().trim().equalsIgnoreCase("Me")) {
                    if (tvName.getText() == null || !tvName.getText().toString().trim().equals(marker.getTitle()) || cardView.getVisibility() == View.GONE) {
                        if (lastMarker != null) {
                            lastMarker.remove();
                            lastMarker = null;
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(new LatLng(serviceArrayList.get(lastPosition).getCurrentLat(), serviceArrayList.get(lastPosition).getCurrentLng()));
                            String title = serviceArrayList.get(lastPosition).getName();
                            markerOptions.title(title);
                            View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                            TextView tv = (TextView) v.findViewById(R.id.tvTitle);
                            LinearLayout llTitle = (LinearLayout) v.findViewById(R.id.llTitle);
                            tv.setText(title);
                            ImageView iv = (ImageView) v.findViewById(R.id.ivMarker);
                            IconGenerator generator = new IconGenerator(ProviderListingActivity.this);
                            llTitle.setBackgroundResource(R.drawable.title_bg_grey);
                            iv.setImageResource(R.drawable.ic_map1);
                            generator.setBackground(null);
                            generator.setContentView(v);
                            Bitmap icon = generator.makeIcon();
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                            mMap.addMarker(markerOptions);
                        }
                        for (int i = 0; i < serviceArrayList.size(); i++) {
                            if (serviceArrayList.get(i).getName().toString().trim().equals(marker.getTitle())) {
                                if (marker != null) {
                                    marker.remove();
                                }
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(new LatLng(serviceArrayList.get(i).getCurrentLat(), serviceArrayList.get(i).getCurrentLng()));
                                String title = serviceArrayList.get(i).getName();
                                markerOptions.title(title);
                                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                                TextView tv = (TextView) v.findViewById(R.id.tvTitle);
                                LinearLayout llTitle = (LinearLayout) v.findViewById(R.id.llTitle);
                                tv.setText(title);
                                ImageView iv = (ImageView) v.findViewById(R.id.ivMarker);
                                IconGenerator generator = new IconGenerator(ProviderListingActivity.this);
                                llTitle.setBackgroundResource(R.drawable.title_bg_red);
                                iv.setImageResource(R.drawable.ic_map3);
                                generator.setBackground(null);
                                generator.setContentView(v);
                                Bitmap icon = generator.makeIcon();
                                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                                marker = mMap.addMarker(markerOptions);
                                currentPosition = i;
                                setProvider(serviceArrayList.get(i));
                                inflateTimeLine(serviceArrayList.get(i).getTimelines());
                                lastMarker = marker;
                                lastPosition = i;
                            }
                        }
                        cardView.startAnimation(slide_up);
                        cardView.setVisibility(View.VISIBLE);
                    }
                }
                return true;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (cardView.getVisibility() == View.VISIBLE) {
                    if (lastMarker != null) {
                        lastMarker.remove();
                        lastMarker = null;
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(new LatLng(serviceArrayList.get(lastPosition).getCurrentLat(), serviceArrayList.get(lastPosition).getCurrentLng()));
                        String title = serviceArrayList.get(lastPosition).getName();
                        markerOptions.title(title);
                        View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                        TextView tv = (TextView) v.findViewById(R.id.tvTitle);
                        LinearLayout llTitle = (LinearLayout) v.findViewById(R.id.llTitle);
                        tv.setText(title);
                        ImageView iv = (ImageView) v.findViewById(R.id.ivMarker);
                        IconGenerator generator = new IconGenerator(ProviderListingActivity.this);
                        llTitle.setBackgroundResource(R.drawable.title_bg_grey);
                        iv.setImageResource(R.drawable.ic_map1);
                        generator.setBackground(null);
                        generator.setContentView(v);
                        Bitmap icon = generator.makeIcon();
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                        mMap.addMarker(markerOptions);
                    }
                    cardView.startAnimation(slide_down);
                    cardView.setVisibility(View.GONE);
                }
            }
        });

        dialogView.dismissCustomSpinProgress();
    }

    public void mapMarkerViewSetup() {
        for (int i = 0; i < serviceArrayList.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(new LatLng(serviceArrayList.get(i).getCurrentLat(), serviceArrayList.get(i).getCurrentLng()));
            String title = serviceArrayList.get(i).getName();
            markerOptions.title(title);
            View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
            TextView tv = (TextView) v.findViewById(R.id.tvTitle);
            LinearLayout llTitle = (LinearLayout) v.findViewById(R.id.llTitle);
            tv.setText(title);
            ImageView iv = (ImageView) v.findViewById(R.id.ivMarker);
            IconGenerator generator = new IconGenerator(this);

            if (title.toString().trim().equalsIgnoreCase("Me")) {
                LatLng myLatLng = new LatLng(serviceArrayList.get(i).getCurrentLat(), serviceArrayList.get(i).getCurrentLng());
                CameraUpdate center = CameraUpdateFactory.newLatLng(myLatLng);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
                mMap.moveCamera(center);
                mMap.animateCamera(zoom);
                llTitle.setBackgroundResource(R.drawable.title_bg_blue);
                iv.setImageResource(R.drawable.ic_map2);
            } else {
                llTitle.setBackgroundResource(R.drawable.title_bg_grey);
                iv.setImageResource(R.drawable.ic_map1);
            }
            generator.setBackground(null);
            generator.setContentView(v);
            Bitmap icon = generator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

            mMap.addMarker(markerOptions);
        }
    }

    void setProvider(Service service) {
        tvName.setText(service.getName());
        if (Float.parseFloat(service.getRatingString().toString()) > 0) {
            tvAverageRating.setText("(" + service.getRatingString() + ")");
        } else {
            tvAverageRating.setText("No Ratings");
        }
        if (Integer.parseInt(service.getTotalRatingCount().toString()) > 1) {
            tvTotalRatings.setText(service.getTotalRatingCount() + " Ratings");
        } else if (Integer.parseInt(service.getTotalRatingCount().toString()) == 1) {
            tvTotalRatings.setText(service.getTotalRatingCount() + " Rating");
        } else tvTotalRatings.setText("");
        ratingBar.setRating(service.getRatingFloat());
        imageLoader.displayImage(service.getProfile().getImage(), ivPic, options);
        if (service.isAsignJobFlag()) {
            btnPick.setVisibility(View.GONE);
        } else {
            btnPick.setVisibility(View.VISIBLE);
        }
    }

    private void checkAdapterIsEmpty() {
        if (adapter.getItemCount() == 0) {
            tvEmptyView.setVisibility(View.VISIBLE);
        } else {
            tvEmptyView.setVisibility(View.GONE);
        }
    }

    void initViews() {
        mQueue = Volley.newRequestQueue(this);
        dialogView = new DialogView(this);
        gpsTracker = new GPSTracker(this);
        customVolley = new CustomVolley(this);
        PushHandler functionInterface = new PushHandler();
        functionInterface.setOnServicesStatusChange(this);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        tvName = (TextView) findViewById(R.id.tvName);
        tvAverageRating = (TextView) findViewById(R.id.tvAverageRating);
        tvTotalRatings = (TextView) findViewById(R.id.tvTotalRatingCount);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ivPic = (ImageView) findViewById(R.id.ivPic);
        btnMoreInfo = (TextView) findViewById(R.id.btnMoreInfo);
        btnPick = (TextView) findViewById(R.id.btnPick);

        cardView = (CardView) findViewById(R.id.cardView);
        slide_down = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slide_down.setRepeatCount(Animation.INFINITE);
        slide_up.setRepeatMode(Animation.INFINITE);

        trTimeline = (TableRow) findViewById(R.id.trTimeLine);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srlAppointment);
        tvEmptyView = (TextView) findViewById(R.id.tvEmptyView);
        recyclerView = (RecyclerView) findViewById(R.id.rvCarService);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout.setEnabled(false);
        adapter = new ProviderListingAdapter(this, serviceArrayList);
        recyclerView.setAdapter(adapter);

        rlListing = (RelativeLayout) findViewById(R.id.rlListing);
        rlMapView = (RelativeLayout) findViewById(R.id.rlMapView);

        cardView.setOnClickListener(this);
        btnPick.setOnClickListener(this);
        btnMoreInfo.setOnClickListener(this);

    }

    void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        ibBack = (ImageButton) toolbar.findViewById(R.id.ibLeft);

        tvToolbarTitle.setVisibility(View.VISIBLE);
        ibBack.setVisibility(View.VISIBLE);

        if (rlListing.getVisibility() == View.VISIBLE) {
            tvToolbarTitle.setText(getResources().getString(R.string.car_washer_list));
        } else {
            tvToolbarTitle.setText(getResources().getString(R.string.choose_provider));
        }
        ibBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ibLeft:
                onBackPressed();
                break;

            case R.id.btnMoreInfo:
                if (lastMarker != null) {
                    lastMarker.remove();
                    lastMarker = null;
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(serviceArrayList.get(lastPosition).getCurrentLat(), serviceArrayList.get(lastPosition).getCurrentLng()));
                    String title = serviceArrayList.get(lastPosition).getName();
                    markerOptions.title(title);
                    View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                    TextView tv = (TextView) v.findViewById(R.id.tvTitle);
                    LinearLayout llTitle = (LinearLayout) v.findViewById(R.id.llTitle);
                    tv.setText(title);
                    ImageView iv = (ImageView) v.findViewById(R.id.ivMarker);
                    IconGenerator generator = new IconGenerator(ProviderListingActivity.this);
                    llTitle.setBackgroundResource(R.drawable.title_bg_grey);
                    iv.setImageResource(R.drawable.ic_map1);
                    generator.setBackground(null);
                    generator.setContentView(v);
                    Bitmap icon = generator.makeIcon();
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                    mMap.addMarker(markerOptions);
                }
                cardView.startAnimation(slide_down);
                cardView.setVisibility(View.GONE);
                startActivityForResult(new Intent(this, CustomerProfileActivity.class)
                                .putExtra("phone", serviceArrayList.get(currentPosition).getPhone())
                                .putExtra("provider_id", serviceArrayList.get(currentPosition).getCarServiceId())
                                .putExtra("assign_job_flag", serviceArrayList.get(currentPosition).isAsignJobFlag())
                                .putExtra("currentPosition", currentPosition)
                        , ACTIVITY_RETURN_REQUEST_CODE);
                break;
            case R.id.btnPick:
                serviceArrayList.get(currentPosition).setAsignJobFlag(true);
                TextView btnPick2 = (TextView) view.findViewById(R.id.btnPick);
                btnPick2.setVisibility(View.GONE);
                VolleyAssignJob(currentPosition);
                break;
            case R.id.cardView:
                break;
        }
    }


    private void VolleyAssignJob(int position) {
        HashMap<String, String> params = getParameter(position);
        customVolley.ServiceRequest(this, ConFig_URL.URL_CREATE_SERVICE, params);
    }

    private HashMap<String, String> getParameter(int position) {
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
        String car_type = getCarType();
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", pref.getStringPreference(ProviderListingActivity.this, Preferences.PHONE));
        params.put("type_id", GlobalVariable.serviceType.getServiceId());
        params.put("request_date", currentDate);
        params.put("latitude", String.valueOf(GlobalVariable.locationLat));
        params.put("longitude", String.valueOf(GlobalVariable.locationLng));
        params.put("expected_cost", String.valueOf(GlobalVariable.totalServicesAmount));
        params.put("from_time", GlobalVariable.serviceStartDate);
        params.put("to_time", GlobalVariable.serviceEndDate);
        params.put("provider_id", serviceArrayList.get(position).getCarServiceId());
        params.put("car_type", car_type);
        return params;
    }

    private String getCarType() {
        String car_type = "";
        try {
            JSONObject mainObj = new JSONObject();
        /*SUV*/
            JSONArray suvArray = new JSONArray();
            for (int i = 0; i < GlobalVariable.suvList.size(); i++) {
                JSONObject suvObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!GlobalVariable.suvList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("0")));
                }
                if (!GlobalVariable.suvList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("1")));
                }
                if (!GlobalVariable.suvList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.suvList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    suvObject.put("option_details", optionDetails);
                }
                if(suvObject.length()!=0) {
                    suvArray.put(suvObject);
                }

            }
            mainObj.put("suv", suvArray);
            /*Sedan*/
            JSONArray sedanArray = new JSONArray();
            for (int i = 0; i < GlobalVariable.sedanList.size(); i++) {
                JSONObject sedanObject = new JSONObject();
                JSONArray optionDetails = new JSONArray();
                if (!GlobalVariable.sedanList.get(i).get("0").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("0")));
                }
                if (!GlobalVariable.sedanList.get(i).get("1").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("1")));
                }
                if (!GlobalVariable.sedanList.get(i).get("2").equals("")) {
                    optionDetails.put(Integer.parseInt(GlobalVariable.sedanList.get(i).get("2")));
                }
                if (optionDetails.length() > 0) {
                    sedanObject.put("option_details", optionDetails);
                }
                if(sedanObject.length()!=0) {
                    sedanArray.put(sedanObject);
                }

            }
            mainObj.put("sedan", sedanArray);
            car_type = mainObj.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return car_type;
    }

    @Override
    public void VolleyResponse(String response, String Type) {
        try {
            JSONObject object = new JSONObject(response);
            if (Type.equals(ConFig_URL.URL_CREATE_SERVICE)) {
                if (object.optString("success").equals("0")) {
                    showCustomSingleButtonDialog(ProviderListingActivity.this,
                            getResources().getString(R.string.success), object.optString("msg"));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }


    }

    public void showCustomSingleButtonDialog(final Context context, String header,
                                             String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

    @Override
    public void onRefresh() {
        volleyProviderServicesListing();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mQueue != null) {
            mQueue.cancelAll(TAG);
        }
    }

    public void setAsignJobFlag(int position) {
        if (position < serviceArrayList.size()) {
            serviceArrayList.get(position).setAsignJobFlag(true);
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onServicesStatusChange(final String id, final String type, final String providerName, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

//stuff that updates ui
                servicesStatusChange(id, type, providerName, msg);

            }
        });

    }

    private void servicesStatusChange(String id, String type, String providerName, String msg) {
        if (type.equals("jobaccepted") || type.equals("rejected")) {
            if (serviceArrayList != null) {
                int pos = -1;
                for (int i = 0; i < serviceArrayList.size(); i++) {
                    if (serviceArrayList.get(i).getName().equals(providerName)) {
                        pos = i;
                        break;
                    }
                }
                if (pos > -1) {
                    serviceArrayList.remove(pos);
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                if (mMap != null) {
                    mMap.clear();
                    mapMarkerViewSetup();
                    if (tvName.getText().toString().equals(providerName)) {
                        cardView.setVisibility(View.GONE);
                    }
                }
            }
        }
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }


}
