package com.uipl.qber.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import com.uipl.qber.Model.PastService;
import com.uipl.qber.PastServiceFragment;
import com.uipl.qber.R;
import com.uipl.qber.RatingActivity;
import com.uipl.qber.base.ConFig_URL;
import com.uipl.qber.utils.CustomVolley;
import com.uipl.qber.utils.DialogView;
import com.uipl.qber.utils.Preferences;
import com.uipl.qber.utils.VolleyInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.uipl.qber.R.string.arrival_time;
import static com.uipl.qber.R.string.mobile;


public class PastServiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements VolleyInterface {


    ArrayList<PastService> pastServiceArrayList = new ArrayList<>();
    Activity context;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    private DialogView dialogView;
    private String TAG = "PastServiceAdapter";
    private RequestQueue mQueue;
    public CustomVolley customVolley;
    private Preferences pref;
    String serviceid;
    private String arrivaltime;
    private String departtime;


    public PastServiceAdapter(Activity mContext, ArrayList<PastService> mArrayList) {


        this.context = mContext;
        this.customVolley = new CustomVolley(context);
        this.pref=new Preferences(context);
        dialogView = new DialogView(mContext);
        this.pastServiceArrayList = mArrayList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public int getItemCount() {
        return pastServiceArrayList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        viewHolder = new PastServiceHolder(inflater.inflate(R.layout.inflate_past_service, viewGroup, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        final PastService pastService = (PastService) pastServiceArrayList.get(position);

        final PastServiceHolder holder = (PastServiceHolder) viewHolder;

        imageLoader.displayImage(ConFig_URL.IMAGE_URL + pastService.getPic(), holder.ivPic, options);
        serviceid=pastService.getServiceId();
        arrivaltime=pastService.getArrivalTime();
        departtime=pastService.getDepartTime();
        holder.tvName.setText(pastService.getName());
        holder.tvServiceName.setText(pastService.getServiceName());
        holder.tvServiceCode.setText(pastService.getServiceCode());
        holder.tvServiceCode.setText((Html.fromHtml("<b><big>"+pastService.getServiceCode()+"</big><font><small>"+ context.getString(R.string.qar) +"</small></font></b>")));
        holder.tvArrivalTime.setText(pastService.getArrivalTime().toUpperCase() + " - " + pastService.getDepartTime().toUpperCase());
        holder.tvArrivalDate.setText(pastService.getArrivalDate());
        if(!pastService.getRating().equals("") && !pastService.getRating().equals("null"))
        holder.ratingbar.setRating(Float.parseFloat(pastService.getRating()));
        if(pastService.getAlreadyRated().equals("0")&& pastService.getStatusId().equals("6")){
            holder.btnRate.setEnabled(true);
            holder.btnRate.setBackgroundResource(R.drawable.rounded_corner_purple);
            holder.btnRate.setTextColor(context.getResources().getColor(R.color.purple));
        }
        else
        {
            holder.btnRate.setEnabled(false);
            holder.btnRate.setBackgroundResource(R.drawable.rounded_corner_grey);
            holder.btnRate.setTextColor(context.getResources().getColor(R.color.grey));
        }
        holder.btnOpenDispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btnOpenDispute:

                        ((InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE))
                                .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle(R.string.enter_disput);
                        final EditText input = new EditText(context);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT);
                        input.setLayoutParams(lp);
                        input.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        int maxLength = 500;
                        InputFilter[] fArray = new InputFilter[1];
                        fArray[0] = new InputFilter.LengthFilter(maxLength);
                        input.setFilters(fArray);
                        alertDialog.setView(input);
                        alertDialog.setCancelable(true);
                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                InputMethodManager imm = (InputMethodManager)
                                context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(
                                        input.getWindowToken(), 0);

                                dialog.dismiss();
                            }
                        });
                        alertDialog.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        if(input.getText().toString().trim().length() > 0){
                                            volleyDisputTask(input.getText().toString().trim());

                                        }else{
                                            Toast.makeText(context, "Dispute cant be blank", Toast.LENGTH_LONG).show();
                                        }

                                        InputMethodManager imm = (InputMethodManager)
                                                context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(
                                                input.getWindowToken(), 0);


                                        dialog.dismiss();

                                    }
                                }
                        );
                        alertDialog.show();
                        break;
                }
            }
        });

            holder.btnRate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.btnRate:
                            Intent intent = new Intent(context, RatingActivity.class);
                            intent.putExtra("service_id",pastService.getServiceId());
                            intent.putExtra("image_url",pastService.getPic());
                            intent.putExtra("name",pastService.getName());
                            context.startActivityForResult(intent,101);
                            break;
                    }
                }
            });


    }
    private void volleyDisputTask(String comment) {

        HashMap<String, String> params = getDisputTask(comment);

        customVolley.ServiceRequest(this, ConFig_URL.URL_OPEN_DISPUTE, params);

    }
    private HashMap<String, String> getDisputTask(String comment) {
        HashMap<String, String> params = new HashMap<>();
//        params.put("phone", "919163063153");
        params.put("phone",pref.getStringPreference(context, Preferences.PHONE));
        params.put("service_id",serviceid);
        params.put("details",comment);
        Log.d(TAG, params.toString());
        return params;

    }


    @Override
    public void VolleyResponse(String response, String Type) {

            if (Type.equals(ConFig_URL.URL_OPEN_DISPUTE)) {
                try {
                    System.out.println("result = " + response);
                    JSONObject object = new JSONObject(response);
                    if (object != null) {
                        if (object.optString("success").toString().trim().equals("0")) {
//                            dialogView.showCustomSingleButtonDialog(context, "DONE !", object.optString("msg"));
                            Toast.makeText(context, "Dispute Raised", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {

                    dialogView.showCustomSingleButtonDialog(context,
                            context.getResources().getString(R.string.sorry), context.getResources().getString(R.string.internal_error));

                }
            }


        }
    class PastServiceHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvServiceName, tvServiceCode, tvArrivalDate, tvArrivalTime, tvStatus;
        ImageView ivPic;
        Button btnOpenDispute, btnRate, btnRepeat;
        CardView cardView;
        RatingBar ratingbar;

        public PastServiceHolder(View v) {
            super(v);
            tvServiceName = (TextView) v.findViewById(R.id.tvServiceName);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvServiceCode = (TextView) v.findViewById(R.id.tvServiceCode);
            tvArrivalDate = (TextView) v.findViewById(R.id.tvArrivalDate);
            tvArrivalTime = (TextView) v.findViewById(R.id.tvArrivalTime);
            tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            ivPic = (ImageView) v.findViewById(R.id.ivPic);
            btnOpenDispute = (Button) v.findViewById(R.id.btnOpenDispute);
            btnRate = (Button) v.findViewById(R.id.btnRate);
            btnRepeat = (Button) v.findViewById(R.id.btnRepeat);
            cardView = (CardView) v.findViewById(R.id.cardView);
            ratingbar = (RatingBar) v.findViewById(R.id.ratingbar1);
        }

    }

    }




//        private void volleyRepeatTask() {
//            dialogView.showCustomSpinProgress(context);
//            HashMap<String, String> paramsRepeat = getRepeatTask();
//            customVolley.ServiceRequest(this, ConFig_URL.URL_REPEAT, paramsRepeat);
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REPEAT,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            dialogView.dismissCustomSpinProgress();
//                            try {
//                                System.out.println("result = " + response);
//                                JSONObject object = new JSONObject(response);
//                                if (object != null) {
//                                    if (object.optString("success").toString().trim().equals("0"))
//                                    {
//                                        dialogView.showCustomSingleButtonDialog(context,"DONE !",object.optString("msg"));
//                                    }
//                                    else if (object.optString("success").toString().trim().equals("1")) {
//
//                                        dialogView.showCustomSingleButtonDialog(context,"SORRY !",object.optString("msg"));
//                                        //JSONObject dataObj = object.optJSONObject("data");
//                                    }
//                                    else if (object.optString("success").toString().trim().equals("2")) {
//
//                                        dialogView.showCustomSingleButtonDialog(context, "SORRY !", object.optString("msg"));
//                                    }
//                                    else {
//                                        dialogView.showCustomSingleButtonDialog(context,
//                                                "SORRY", object.optString("msg"));
//                                    }
//                                }
//                            } catch (JSONException e) {
//
//                                dialogView.showCustomSingleButtonDialog(context,
//                                        context.getResources().getString(R.string.sorry), context.getResources().getString(R.string.internal_error));
//
//                            }
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                    dialogView.dismissCustomSpinProgress();
//                    if(error instanceof NoConnectionError) {
//                        dialogView.showCustomSingleButtonDialog(context,
//                                context.getResources().getString(R.string.sorry),context. getResources().getString(R.string.no_net_connection));
//                    }else if (error instanceof com.android.volley.TimeoutError) {
//                        dialogView.showCustomSingleButtonDialog(context,
//                                context.getResources().getString(R.string.sorry),context.getResources().getString(R.string.timeout_error));
//                    } else {
//                        dialogView.showCustomSingleButtonDialog(context,
//                                context.getResources().getString(R.string.sorry),context.getResources().getString(R.string.internal_error));
//                    }
//                }
//            }) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<>();
//                    params.put("phone","919163063153");
//                    params.put("service_id", "2");
//                    params.put("from_time","" );
//                    params.put("to_time","");
//                    Log.d(TAG, params.toString());
//                    return params;
//                }
//            };
//            stringRequest.setShouldCache(false);
//            stringRequest.setTag(TAG);
//            stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            mQueue.add(stringRequest);
//        }
//
//        }
//
//        private HashMap<String, String> getRepeatTask() {
//            HashMap<String, String> params = new HashMap<>();
//            params.put("phone", "919163063153");
//            params.put("service_id", "2");
//            params.put("from_time", "");
//            params.put("to_time", "");
//            Log.d(TAG, params.toString());
//            return params;
//
//        }












