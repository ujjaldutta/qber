package com.uipl.qber.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.uipl.qber.BookedStepOneFragment;
import com.uipl.qber.BookedStepThreeFragment;
import com.uipl.qber.BookedStepTwoFragment;

import java.nio.DoubleBuffer;
import java.util.HashMap;

/**
 * Created by Ankan on 05-Oct-16.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {


    private HashMap<Integer,Fragment> mPagerFragment=new HashMap<>();

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        switch (index) {
            case 0:
                BookedStepOneFragment bookedStepOneFragment=  new BookedStepOneFragment();
                mPagerFragment.put(index,bookedStepOneFragment);
                return bookedStepOneFragment;
            case 1:
                BookedStepTwoFragment bookedStepTwoFragment=  new BookedStepTwoFragment();
                mPagerFragment.put(index,bookedStepTwoFragment);
                return bookedStepTwoFragment;
            case 2:
                BookedStepThreeFragment bookedStepThreeFragment=  new BookedStepThreeFragment();
                mPagerFragment.put(index,bookedStepThreeFragment);
                return bookedStepThreeFragment;
        }
        return null;
    }


    @Override
    public int getCount() {
        return 3;
    }

 public Fragment getFragment(int key ){
     return mPagerFragment.get(key);

 }

}