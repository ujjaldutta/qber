package com.uipl.qberprovider;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.uipl.qberprovider.base.Constants;
import com.uipl.qberprovider.base.GlobalVariable;

import java.util.concurrent.TimeUnit;

/**
 * Created by richa on 12/1/17.
 */
public class PushNotificationTimerService extends Service {

    CountDownTimer timer_13seconds;
    final String TAG = "TimerService";
    Handler handler = new Handler();

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            timer_13seconds = new CountDownTimer(Constants.Timer.wait_timer, 1000) {

                public void onTick(long millisUntilFinished) {
                    Intent intent = new Intent(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION);
                    intent.setAction(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION);
                    intent.putExtra("isTimerRunning", true);
                    intent.putExtra("time", "" + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + ":" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));
                    sendBroadcast(intent);
                }

                public void onFinish() {
                    Intent intent = new Intent(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION);
                    intent.setAction(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION);
                    intent.putExtra("isTimerRunning", false);
                    sendBroadcast(intent);
                    if (GlobalVariable.pushNotificationServiceId != null && !GlobalVariable.pushNotificationServiceId.toString().trim().equals("")) {
                        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notifManager.cancel(Integer.parseInt(GlobalVariable.pushNotificationServiceId));
                        GlobalVariable.pushNotificationServiceId = "";
                    }
                    stopSelf();
                }
            }.start();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        handler.removeCallbacks(runnable);
        handler.post(runnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer_13seconds != null) {
            timer_13seconds.cancel();
        }
        handler.removeCallbacks(runnable);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
