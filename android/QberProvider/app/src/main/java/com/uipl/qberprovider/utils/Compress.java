package com.uipl.qberprovider.utils;

/**
 * Created by pallab on 4/11/16.
 */

public interface Compress {
    void getCompressData(String imagePath, String picImageFor);
}
