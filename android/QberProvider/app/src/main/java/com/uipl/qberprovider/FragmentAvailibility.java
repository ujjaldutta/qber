package com.uipl.qberprovider;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.DayState;
import com.uipl.qberprovider.model.Timeline;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import customView.TypedfaceTextView;

/**
 * Created by Ankan on 28-Sep-16.
 */
public class FragmentAvailibility extends Fragment {

    View v;

    TypedfaceTextView tvDay;
    ToggleButton cbAvailable;
    RelativeLayout rlAddUnavailable, rlExpand;
    LinearLayout llMain, llProgress, llUnavailable, llAdableUnavailable;
    TableRow trTimeline, trWorkingTime;
    TextView tv1, tv2;
    Spinner spnFromTime, spnToTime;
    ImageView imvDropDown;
    Button btnUseOther;

    String workingStartTime = "06:00 AM", workingEndTime = "11:59 PM";
    String breakStartTime = "", breakEndTime = "";
    final String startTime = "06:00 AM", endTime = "11:59 PM";
    int minAvailableTimeGap = 30, minBreakTimeGap = 10, fragmentPosition;
    float workingTimeWeightSum = 0;
    boolean isBreakTimesSet = false;

    ArrayList<Timeline> oldArrList = new ArrayList<>();

    SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

    ArrayList<String> spinnerStartTimes = new ArrayList<>();
    ArrayList<String> spinnerEndTimes = new ArrayList<>();

    final String TAG = "FragmentAvailibility";

    public static FragmentAvailibility newInstance(int position, String workingStartTime, String workingEndTime) {
        FragmentAvailibility f = new FragmentAvailibility();
        Bundle b = new Bundle();
        b.putInt("position", position);
        b.putString("workingStartTime", workingStartTime);
        b.putString("workingEndTime", workingEndTime);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_availibility_item, container, false);

        initViews();

        fragmentPosition = getArguments().getInt("position");
        workingStartTime = getArguments().getString("workingStartTime");
        workingEndTime = getArguments().getString("workingEndTime");

        saveArrayList();

        setDay(fragmentPosition);

        setListener();

        setInitialSetup();

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        // TODO Auto-generated method stub
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (GlobalVariable.containedWeeks.contains(GlobalVariable.weekDaysArr.get(FragmentAvailibilitySetup.myviewpager.getCurrentItem()))) {
                GlobalVariable.containedWeeks.remove(GlobalVariable.weekDaysArr.get(FragmentAvailibilitySetup.myviewpager.getCurrentItem()));
            }
        }
    }

    void saveArrayList() {
        oldArrList.clear();
        oldArrList.addAll(GlobalVariable.timelinesArrayList.get(fragmentPosition));
    }

    Timeline setTimeline(final String startTimeString, final String endTimeString, boolean isActive, boolean isAvailable, int breakTimePosition) {
        Timeline t1 = new Timeline();
        t1.setStartTime(startTimeString);
        t1.setEndTime(endTimeString);
        t1.setActive(isActive);
        t1.setAvailable(isAvailable);
        t1.setBreakTimePosition(breakTimePosition);
        t1.setWorkingStartTime(workingStartTime);
        t1.setWorkingEndTime(workingEndTime);
        Date startTime = null, endTime = null;
        try {
            startTime = date24Format.parse(date24Format.format(date12Format.parse(startTimeString)));
            endTime = date24Format.parse(date24Format.format(date12Format.parse(endTimeString)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        t1.setStartDateTime(startTime);
        t1.setEndDateTime(endTime);
        t1.setDiffInHours(getDiffInHours(startTime, endTime));
        return t1;
    }

    float getDiffInHours(Date startTime, Date endTime) {
        float result = 0;
        long difference = endTime.getTime() - startTime.getTime();
        float diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(difference);
        float d = diffInMinutes % 60;
        float diffInHours = TimeUnit.MILLISECONDS.toHours(difference);
        if (d > 0) {
            diffInHours = diffInHours + (d / 60);
        }
        BigDecimal bd = new BigDecimal(Float.toString(diffInHours));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        result = bd.floatValue();
        if (result >= 0) {
            return result;
        } else {
            return 0;
        }
    }

    void inflateTimeline(ArrayList<Timeline> workingTimeIntervals) {
        trTimeline.setBackgroundResource(R.drawable.timeline_grey_rounded);

        if (!workingTimeIntervals.isEmpty()) {

            float totalWeightSum = 0;
            int size = workingTimeIntervals.size();

            Date sTime = null, eTime = null;
            try {
                sTime = date24Format.parse(date24Format.format(date12Format.parse(startTime)));
                eTime = date24Format.parse(date24Format.format(date12Format.parse(endTime)));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            totalWeightSum = getDiffInHours(sTime, workingTimeIntervals.get(0).getStartDateTime());
            tv1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 10, totalWeightSum));

            trWorkingTime.removeAllViewsInLayout();
            workingTimeWeightSum = getDiffInHours(workingTimeIntervals.get(0).getStartDateTime(), workingTimeIntervals.get(size - 1).getEndDateTime());
            trWorkingTime.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 10, workingTimeWeightSum));
            totalWeightSum = totalWeightSum + workingTimeWeightSum;
            trWorkingTime.setWeightSum(workingTimeWeightSum);
            trWorkingTime.setBackgroundResource(R.drawable.timeline_green_rounded);
            for (int i = 0; i < workingTimeIntervals.size(); i++) {
                Timeline timeline = workingTimeIntervals.get(i);
                addTimeLine(timeline.getDiffInHours(), timeline.isAvailable());
            }

            float ws = 0;
            ws = getDiffInHours(workingTimeIntervals.get(size - 1).getEndDateTime(), eTime);
            tv2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 10, ws));
            totalWeightSum = totalWeightSum + ws;
            trTimeline.setWeightSum(totalWeightSum);
        } else {
            trWorkingTime.removeAllViewsInLayout();
            trWorkingTime.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 10, 0));
        }
    }

    void addBreakTime(String breakStartTime, String breakEndTime, int lastPos) {
        ArrayList<Timeline> arr = new ArrayList<>();

        arr.addAll(deleteBreakTimeIfAlreadyExists(GlobalVariable.timelinesArrayList.get(fragmentPosition), lastPos));
        arr.add(setTimeline(breakStartTime, breakEndTime, true, false, lastPos));
        Collections.sort(arr, new IntervalStartComparator());
        GlobalVariable.timelinesArrayList.get(fragmentPosition).clear();
        GlobalVariable.timelinesArrayList.get(fragmentPosition).addAll(arr);

        inflateTimeline(merge(arr));
    }

    void deleteBreakTime(int lastPos) {
        ArrayList<Timeline> arr = new ArrayList<>();
        ArrayList<Timeline> breakTimes = new ArrayList<>();

        arr.addAll(deleteBreakTimeIfAlreadyExists(GlobalVariable.timelinesArrayList.get(fragmentPosition), lastPos));
        breakTimes.addAll(getAllBreakTimes(arr));
        Collections.sort(arr, new IntervalStartComparator());
        GlobalVariable.timelinesArrayList.get(fragmentPosition).clear();
        GlobalVariable.timelinesArrayList.get(fragmentPosition).addAll(deleteAllBreakTimes(arr));

        addBreatimesAgain(breakTimes);
        inflateTimeline(merge(arr));
    }

    ArrayList<Timeline> getAllBreakTimes(ArrayList<Timeline> arr) {
        ArrayList<Timeline> arrTemp = new ArrayList<>();
        for (Timeline t : arr) {
            if (!t.isAvailable()) {
                arrTemp.add(t);
            }
        }
        return arrTemp;
    }

    ArrayList<Timeline> deleteAllBreakTimes(ArrayList<Timeline> arr) {
        ArrayList<Timeline> arrTemp = new ArrayList<>();
        for (Timeline t : arr) {
            if (t.isAvailable()) {
                arrTemp.add(t);
            }
        }
        return arrTemp;
    }

    void addBreatimesAgain(ArrayList<Timeline> breakTimes) {
        llAdableUnavailable.removeAllViews();
        for (Timeline t : breakTimes) {
            addUnavailableLayout(llAdableUnavailable.getChildCount(), t.getStartTime(), t.getEndTime());
        }
    }

    void changeStartEndTime() {
        ArrayList<Timeline> intervals = new ArrayList<>();
        Timeline t = new Timeline();
        t = setTimeline(workingStartTime, workingEndTime, true, true, -1);
        intervals.add(t);

        GlobalVariable.timelinesArrayList.get(fragmentPosition).clear();
        GlobalVariable.timelinesArrayList.get(fragmentPosition).addAll(intervals);

        inflateTimeline(intervals);
    }

    public ArrayList<Timeline> merge(ArrayList<Timeline> intervals) {

        if (intervals.size() == 0) {
            Timeline t = new Timeline();
            t = setTimeline(workingStartTime, workingEndTime, true, true, -1);
            intervals.add(t);
            return intervals;
        } else {
            ArrayList<Timeline> result = new ArrayList<>();
            try {
                Date workingStart = null, workingEnd = null;
                workingStart = date12Format.parse(workingStartTime);
                workingEnd = date12Format.parse(workingEndTime);
                for (int i = 0; i < intervals.size(); i++) {
                    Timeline current = intervals.get(i);
                    Date lastStart = null, lastEnd = null, currentStart = null, currentEnd = null;
                    currentStart = date12Format.parse(current.getStartTime());
                    currentEnd = date12Format.parse(current.getEndTime());
                    if (i == 0) {
                        int compare = workingStart.compareTo(currentStart);
                        if (compare == 0) {
                            result.add(intervals.get(i));
                        } else if (compare < 0) {
                            result.add(setTimeline(workingStartTime, current.getEndTime(), true, true, -1));
                            result.add(current);
                        }
                    } else {
                        int resultSize = result.size();
                        Timeline last = result.get(resultSize - 1);
                        lastStart = date12Format.parse(last.getStartTime());
                        lastEnd = date12Format.parse(last.getEndTime());
                        int compareStarts = lastStart.compareTo(currentStart);
                        int compareEnds = lastEnd.compareTo(currentEnd);
                        if (last.isAvailable() && current.isAvailable()) {
                            result.remove(resultSize - 1);
                            result.add(setTimeline(last.getStartTime(), current.getEndTime(), true, true, -1));
                        } else if (!last.isAvailable() && !current.isAvailable()) {
                            if (compareStarts == 0 && compareEnds == 0) {
                                continue;
                            } else if (compareStarts == 0 || compareEnds == 0 || (compareStarts < 0 && compareEnds < 0)) {
                                result.remove(resultSize - 1);
                                result.add(setTimeline(last.getStartTime(), current.getEndTime(), true, false, 0));
                            } else if (compareStarts < 0 && compareEnds > 0) {
                                continue;
                            }
                        } else if (last.isAvailable() && !current.isAvailable()) {
                            if (compareStarts == 0 && compareEnds == 0) {
                                result.remove(resultSize - 1);
                                result.add(current);
                            } else {
                                result.remove(resultSize - 1);
                                resultSize = result.size();
                                if (resultSize <= 0) {
                                    int compare = workingStart.compareTo(currentStart);
                                    if (compare == 0) {
                                        result.add(intervals.get(i));
                                    } else if (compare < 0) {
                                        result.add(setTimeline(workingStartTime, current.getStartTime(), true, true, -1));
                                    }
                                }
                                resultSize = result.size();
                                Timeline last2 = result.get(resultSize - 1);
                                lastStart = date12Format.parse(last2.getStartTime());
                                lastEnd = date12Format.parse(last2.getEndTime());
                                int compare = currentStart.compareTo(lastEnd);
                                if (!last2.isAvailable()) {
                                    if (compare > 0) {
                                        result.add(setTimeline(last2.getEndTime(), current.getStartTime(), true, true, -1));
                                        result.add(current);
                                        result.add(setTimeline(current.getEndTime(), last.getEndTime(), true, true, -1));
                                    } else if (compare == 0) {
                                        result.add(current);
                                        result.add(setTimeline(current.getEndTime(), last.getEndTime(), true, true, -1));
                                    } else {
                                        result.add(setTimeline(last2.getStartTime(), current.getEndTime(), true, false, 0));
                                        result.add(setTimeline(current.getEndTime(), last.getEndTime(), true, true, -1));
                                    }
                                } else {
                                    if (compare == 0) {
                                        result.add(current);
                                        result.add(setTimeline(current.getEndTime(), last.getEndTime(), true, true, -1));
                                    } else {
                                        result.remove(resultSize - 1);
                                        result.add(setTimeline(last2.getStartTime(), current.getStartTime(), true, true, -1));
                                        result.add(current);
                                        result.add(setTimeline(current.getEndTime(), last.getEndTime(), true, true, -1));
                                    }
                                }
                            }
                        } else if (!last.isAvailable() && current.isAvailable()) {
                            if ((compareStarts == 0 && compareEnds == 0) || (compareStarts < 0 && compareEnds > 0)
                                    || (compareStarts == 0 && compareEnds > 0) || (compareEnds == 0 && compareStarts < 0)) {
                                continue;
                            } else if (compareStarts == 0 && compareEnds < 0) {
                                result.add(setTimeline(last.getEndTime(), current.getEndTime(), true, true, -1));
                            } else if (compareStarts < 0 && compareEnds < 0 && currentStart.compareTo(lastEnd) < 0) {
                                result.add(setTimeline(last.getEndTime(), current.getEndTime(), true, true, -1));
                            } else if (currentStart.compareTo(lastEnd) > 0) {
                                result.add(setTimeline(last.getStartTime(), current.getEndTime(), true, true, -1));
                            }
                        }
                    }
                }

                int resultSize = result.size();
                Timeline last = result.get(resultSize - 1);
                Date lastStart = null, lastEnd = null;
                lastStart = date12Format.parse(last.getStartTime());
                lastEnd = date12Format.parse(last.getEndTime());
                int compareEnds = lastEnd.compareTo(workingEnd);
                if (compareEnds < 0) {
                    if (last.isAvailable()) {
                        result.remove(resultSize - 1);
                        result.add(setTimeline(last.getStartTime(), workingEndTime, true, true, -1));
                    } else {
                        result.add(setTimeline(last.getEndTime(), workingEndTime, true, true, -1));
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return checkForDuplicates(result);
        }

    }

    ArrayList<Timeline> checkForDuplicates(ArrayList<Timeline> arrTemp) {
        ArrayList<Timeline> result = new ArrayList<>();
        for (Timeline t : arrTemp) {
            boolean isFound = false;
            for (Timeline t2 : result) {
                if ((t.getStartTime().equals(t2.getStartTime()) && t.getEndTime().equals(t2.getEndTime()) && t.isAvailable() == t2.isAvailable())
                        || t2.getDiffInHours() <= 0) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                result.add(t);
            }
        }
        return result;
    }

    class IntervalStartComparator implements java.util.Comparator<Timeline> {

        @Override
        public int compare(Timeline o1, Timeline o2) {
            int compareStarts = o1.getStartDateTime().compareTo(o2.getStartDateTime());
            int compareEnds = o1.getEndDateTime().compareTo(o2.getEndDateTime());
            if (compareStarts > 0)
                return 1;
            else if (compareStarts < 0)
                return -1;
            else {
                if (compareEnds > 0)
                    return 1;
                else if (compareEnds < 0)
                    return -1;
            }
            return 0;
        }
    }

    void addTimeLine(float weight, boolean isAvailable) {
        TextView tv = new TextView(getActivity());
        tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, weight));
        if (isAvailable) {
            tv.setBackgroundResource(0);
        } else {
            tv.setBackgroundResource(R.drawable.timeline_red_rounded);
        }
        trWorkingTime.addView(tv);
    }

    private boolean setInitialSetup() {

        Date sDateTime = null, lastStartDateTime = null;
        try {
            sDateTime = date12Format.parse(startTime);
            lastStartDateTime = date12Format.parse(endTime);
            Calendar c = Calendar.getInstance();
            c.setTime(lastStartDateTime);
            c.add(Calendar.MINUTE, -minAvailableTimeGap);
            lastStartDateTime = c.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        spinnerStartTimes.clear();

        do {
            spinnerStartTimes.add(date12Format.format(sDateTime).toUpperCase());
            Calendar c = Calendar.getInstance();
            c.setTime(sDateTime);
            c.add(Calendar.MINUTE, minAvailableTimeGap);
            sDateTime = c.getTime();
        } while (sDateTime.compareTo(lastStartDateTime) < 0);
        spinnerStartTimes.add("11:30 PM");

        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_work_time, spinnerStartTimes);
        adp.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spnFromTime.setAdapter(adp);
        boolean isSet = false;
        for (int i = 0; i < spinnerStartTimes.size(); i++) {
            if (workingStartTime.equalsIgnoreCase(spinnerStartTimes.get(i))) {
                spnFromTime.setSelection(i);
                isSet = true;
                break;
            }
        }
        if (!isSet) {
            spnFromTime.setSelection(0);
        }

        cbAvailable.setChecked(GlobalVariable.isAvailableArray[fragmentPosition]);

        return true;
    }

    void setEndTimeSpinner() {
        Date eDateTime = null, lastEndDateTime = null;
        try {
            eDateTime = date12Format.parse(workingStartTime);
            Calendar c = Calendar.getInstance();
            c.setTime(eDateTime);
            c.add(Calendar.MINUTE, minAvailableTimeGap);
            eDateTime = c.getTime();
            lastEndDateTime = date12Format.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        spinnerEndTimes.clear();

        if (!workingStartTime.equalsIgnoreCase("11:30 PM")) {
            do {
                spinnerEndTimes.add(date12Format.format(eDateTime).toUpperCase());
                Calendar c = Calendar.getInstance();
                c.setTime(eDateTime);
                c.add(Calendar.MINUTE, minAvailableTimeGap);
                eDateTime = c.getTime();
            } while (eDateTime.compareTo(lastEndDateTime) < 0);
        }
        spinnerEndTimes.add("12:00 AM");

        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_work_time, spinnerEndTimes);
        adp.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spnToTime.setAdapter(adp);
        boolean isSet = false;
        for (int i = 0; i < spinnerEndTimes.size(); i++) {
            if (workingEndTime.equalsIgnoreCase(spinnerEndTimes.get(i))) {
                spnToTime.setSelection(i);
                isSet = true;
                break;
            }
        }
        if (!isSet) {
            spnToTime.setSelection(spinnerEndTimes.size() - 1);
        }
    }

    private void setListener() {
        btnUseOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeekdaysSelectionDialog();
            }
        });

        cbAvailable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                GlobalVariable.isAvailableArray[fragmentPosition] = isChecked;
                if (isChecked) {
                    llMain.setVisibility(View.VISIBLE);
                } else {
                    llMain.setVisibility(View.GONE);
                }
            }
        });

        spnFromTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (llAdableUnavailable.getChildCount() > 0) {
                    llAdableUnavailable.removeAllViews();
                }
                workingStartTime = parent.getItemAtPosition(position).toString();
                setEndTimeSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnToTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (llAdableUnavailable.getChildCount() > 0) {
                    llAdableUnavailable.removeAllViews();
                }
                workingEndTime = parent.getItemAtPosition(position).toString();
                if (workingEndTime.equalsIgnoreCase("12:00 AM")) {
                    workingEndTime = endTime;
                }
                changeStartEndTime();
                if (!isBreakTimesSet) {
                    if (!oldArrList.isEmpty()) {
                        for (Timeline t : oldArrList) {
                            if (!t.isAvailable()) {
                                addUnavailableLayout(llAdableUnavailable.getChildCount(), t.getStartTime(), t.getEndTime());
                            }
                        }
                        isBreakTimesSet = true;
                    } else {
                        isBreakTimesSet = true;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rlAddUnavailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUnavailableLayout(llAdableUnavailable.getChildCount(), "02:00 PM", "04:00 PM");
            }
        });

        rlExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llAdableUnavailable.isShown()) {
                    llAdableUnavailable.setVisibility(View.GONE);
                    imvDropDown.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.arrow_upword));
                } else {
                    llAdableUnavailable.setVisibility(View.VISIBLE);
                    imvDropDown.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.arrow_downward));
                }
            }
        });
    }

    private void showWeekdaysSelectionDialog() {
        GlobalVariable.showingWeeArr.clear();

        for (int i = 0; i < GlobalVariable.weeArr.length; i++) {
            if (i != FragmentAvailibilitySetup.myviewpager.getCurrentItem()) {
                GlobalVariable.showingWeeArr.add(GlobalVariable.weeArr[i]);
            }
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_set_other_weekdays);

        final CheckBox rbOne = (CheckBox) dialog.findViewById(R.id.rbOne);
        rbOne.setText(GlobalVariable.showingWeeArr.get(0));
        final CheckBox rbTwo = (CheckBox) dialog.findViewById(R.id.rbTwo);
        rbTwo.setText(GlobalVariable.showingWeeArr.get(1));
        final CheckBox rbThree = (CheckBox) dialog.findViewById(R.id.rbThree);
        rbThree.setText(GlobalVariable.showingWeeArr.get(2));
        final CheckBox rbFour = (CheckBox) dialog.findViewById(R.id.rbFour);
        rbFour.setText(GlobalVariable.showingWeeArr.get(3));
        final CheckBox rbFive = (CheckBox) dialog.findViewById(R.id.rbFive);
        rbFive.setText(GlobalVariable.showingWeeArr.get(4));
        final CheckBox rbSix = (CheckBox) dialog.findViewById(R.id.rbSix);
        rbSix.setText(GlobalVariable.showingWeeArr.get(5));

        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbOne.isChecked() || rbTwo.isChecked() || rbThree.isChecked() || rbFour.isChecked() || rbFive.isChecked() || rbSix.isChecked()) {
                    GlobalVariable.DAYState.clear();

                    int count = llAdableUnavailable.getChildCount();
                    for (int i = 0; i < count; i++) {
                        View view = llAdableUnavailable.getChildAt(i);
                        Spinner spnFrom = (Spinner) view.findViewById(R.id.spnFromTime);
                        Spinner spnTo = (Spinner) view.findViewById(R.id.spnToTime);
                        GlobalVariable.DAYState.add(new DayState(spnFrom.getSelectedItemPosition(), spnTo.getSelectedItemPosition()));
                    }

                    ArrayList<Timeline> arr = new ArrayList<>();
                    boolean isAvailable = cbAvailable.isChecked();
                    GlobalVariable.isAvailableArray[fragmentPosition] = cbAvailable.isChecked();

                    arr.addAll(GlobalVariable.timelinesArrayList.get(fragmentPosition));

                    if (rbOne.isChecked()) {
                        String text = rbOne.getText().toString();
                        GlobalVariable.containedWeeks.add(text);
                        changeArrayList(arr, text, isAvailable);
                    }
                    if (rbTwo.isChecked()) {
                        String text = rbTwo.getText().toString();
                        GlobalVariable.containedWeeks.add(text);
                        changeArrayList(arr, text, isAvailable);
                    }
                    if (rbThree.isChecked()) {
                        String text = rbThree.getText().toString();
                        GlobalVariable.containedWeeks.add(text);
                        changeArrayList(arr, text, isAvailable);
                    }
                    if (rbFour.isChecked()) {
                        String text = rbFour.getText().toString();
                        GlobalVariable.containedWeeks.add(text);
                        changeArrayList(arr, text, isAvailable);
                    }
                    if (rbFive.isChecked()) {
                        String text = rbFive.getText().toString();
                        GlobalVariable.containedWeeks.add(text);
                        changeArrayList(arr, text, isAvailable);
                    }
                    if (rbSix.isChecked()) {
                        String text = rbSix.getText().toString();
                        GlobalVariable.containedWeeks.add(text);
                        changeArrayList(arr, text, isAvailable);
                    }

                    FragmentAvailibilitySetup parentFragment = (FragmentAvailibilitySetup) getFragmentManager().findFragmentByTag(FragmentAvailibilitySetup.TAG);
                    if (parentFragment != null) {
                        parentFragment.resetFragments(fragmentPosition);
                    }
//                    FragmentAvailibilitySetup fragmentAvailibilitySetup = new FragmentAvailibilitySetup();
//                    fragmentAvailibilitySetup.resetFragments(fragmentPosition);

                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    void changeArrayList(ArrayList<Timeline> arr, String dayText, boolean isAvailable) {

        if (dayText.equalsIgnoreCase("SUN")) {
            GlobalVariable.timelinesArrayList.get(0).clear();
            GlobalVariable.timelinesArrayList.get(0).addAll(arr);
            GlobalVariable.isAvailableArray[0] = isAvailable;
        } else if (dayText.equalsIgnoreCase("MON")) {
            GlobalVariable.timelinesArrayList.get(1).clear();
            GlobalVariable.timelinesArrayList.get(1).addAll(arr);
            GlobalVariable.isAvailableArray[1] = isAvailable;
        } else if (dayText.equalsIgnoreCase("TUE")) {
            GlobalVariable.timelinesArrayList.get(2).clear();
            GlobalVariable.timelinesArrayList.get(2).addAll(arr);
            GlobalVariable.isAvailableArray[2] = isAvailable;
        } else if (dayText.equalsIgnoreCase("WED")) {
            GlobalVariable.timelinesArrayList.get(3).clear();
            GlobalVariable.timelinesArrayList.get(3).addAll(arr);
            GlobalVariable.isAvailableArray[3] = isAvailable;
        } else if (dayText.equalsIgnoreCase("THU")) {
            GlobalVariable.timelinesArrayList.get(4).clear();
            GlobalVariable.timelinesArrayList.get(4).addAll(arr);
            GlobalVariable.isAvailableArray[4] = isAvailable;
        } else if (dayText.equalsIgnoreCase("FRI")) {
            GlobalVariable.timelinesArrayList.get(5).clear();
            GlobalVariable.timelinesArrayList.get(5).addAll(arr);
            GlobalVariable.isAvailableArray[5] = isAvailable;
        } else if (dayText.equalsIgnoreCase("SAT")) {
            GlobalVariable.timelinesArrayList.get(6).clear();
            GlobalVariable.timelinesArrayList.get(6).addAll(arr);
            GlobalVariable.isAvailableArray[6] = isAvailable;
        }

    }

    public boolean getOverlapstatus() {

        ArrayList<Timeline> arr = new ArrayList<>();
        ArrayList<Timeline> arrUnavailableTime = new ArrayList<>();
        ArrayList<Timeline> arrAllTime = new ArrayList<>();

        arr.addAll(GlobalVariable.timelinesArrayList.get(fragmentPosition));

        int size = arr.size();
        for (int i = 0; i < size; i++) {
            Timeline time = arr.get(i);
            if (time.isActive() && !time.isAvailable()) {
                arrUnavailableTime.add(time);
            }
            arrAllTime.add(time);
        }

        if (checkIsOvelap(arrUnavailableTime)) {
            return true;
        } else {
            return false;
        }
    }

    ArrayList<Timeline> deleteBreakTimeIfAlreadyExists(ArrayList<Timeline> arrTemp, int breakTimePos) {
        int currentPosition = 0;


        for (Timeline t : arrTemp) {
            if (t.getBreakTimePosition() == breakTimePos) {
                arrTemp.remove(currentPosition);
                break;
            }
            currentPosition++;
        }

        return arrTemp;
    }

    private void addUnavailableLayout(int currentLayoutPosition, final String breakStime, final String breakEtime) {
        if (!llAdableUnavailable.isShown()) {
            llAdableUnavailable.setVisibility(View.VISIBLE);
            imvDropDown.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.arrow_downward));
        }

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.item_time, null);

        final Spinner spnFrom = (Spinner) view.findViewById(R.id.spnFromTime);

        Date sDateTime = null, eDateTime = null;

        ArrayList<String> sTimes = new ArrayList<>();
        final ArrayList<String> eTimes = new ArrayList<>();

        try {
            sDateTime = date12Format.parse(workingStartTime);
            Calendar c = Calendar.getInstance();
            c.setTime(date12Format.parse(workingEndTime));
            c.add(Calendar.MINUTE, -minBreakTimeGap);
            if (workingEndTime.equalsIgnoreCase("11:59 PM")) {
                c.add(Calendar.MINUTE, 1);
            }
            eDateTime = c.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        do {
            sTimes.add(date12Format.format(sDateTime).toUpperCase());
            Calendar c2 = Calendar.getInstance();
            c2.setTime(sDateTime);
            c2.add(Calendar.MINUTE, minBreakTimeGap);
            sDateTime = c2.getTime();
        } while (sDateTime.compareTo(eDateTime) <= 0);

        if (workingEndTime.equalsIgnoreCase("12:00 AM")) {
            sTimes.add("11:50 PM");
        }

        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_work_time, sTimes);
        adp.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spnFrom.setAdapter(adp);
        if (breakStime != null && !breakStime.toString().equals("")) {
            boolean isSet = false;
            for (int i = 0; i < sTimes.size(); i++) {
                if (sTimes.get(i).equalsIgnoreCase(breakStime)) {
                    spnFrom.setSelection(i);
                    isSet = true;
                    break;
                }
            }
            if (!isSet) {
                spnFrom.setSelection(0);
            }
        } else {
            spnFrom.setSelection(0);
        }
        spnFrom.setTag(currentLayoutPosition);

        final Spinner spnTo = (Spinner) view.findViewById(R.id.spnToTime);
        spnTo.setTag(currentLayoutPosition);

        ImageView imvRemove = (ImageView) view.findViewById(R.id.imvRemove);
        imvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteBreakTime(Integer.valueOf(spnFrom.getTag().toString()));
                llAdableUnavailable.removeView(view);
            }
        });

        spnFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                breakStartTime = parent.getItemAtPosition(position).toString();

                eTimes.clear();
                Calendar c = Calendar.getInstance();
                Date sDateTime = null, eDateTime = null;
                try {
                    c.setTime(date12Format.parse(breakStartTime));
                    c.add(Calendar.MINUTE, minBreakTimeGap);
                    sDateTime = date12Format.parse(date12Format.format(c.getTime()));
                    c = Calendar.getInstance();
                    c.setTime(date12Format.parse(workingEndTime));
                    c.add(Calendar.MINUTE, -minBreakTimeGap);
                    if (workingEndTime.equalsIgnoreCase("11:59 PM")) {
                        c.add(Calendar.MINUTE, 1);
                    }
                    eDateTime = c.getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!breakStartTime.equalsIgnoreCase("11:50 PM")) {
                    do {
                        eTimes.add(date12Format.format(sDateTime).toUpperCase());
                        Calendar c2 = Calendar.getInstance();
                        c2.setTime(sDateTime);
                        c2.add(Calendar.MINUTE, minBreakTimeGap);
                        sDateTime = c2.getTime();
                    } while (sDateTime.compareTo(eDateTime) <= 0);
                }
                eTimes.add("12:00 AM");
                ArrayAdapter<String> adp2 = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_work_time, eTimes);
                adp2.setDropDownViewResource(android.R.layout.simple_list_item_1);
                spnTo.setAdapter(adp2);
                if (breakEtime != null && !breakEtime.toString().equals("")) {
                    boolean isSet = false;
                    for (int i = 0; i < eTimes.size(); i++) {
                        if (eTimes.get(i).equalsIgnoreCase(breakEtime)) {
                            spnTo.setSelection(i);
                            isSet = true;
                            break;
                        }
                    }
                    if (!isSet) {
                        spnTo.setSelection(0);
                    }
                } else {
                    spnTo.setSelection(0);
                }

                int lastStartBreakTimePos = -1;
                if (spnFrom.getTag() != null) {
                    lastStartBreakTimePos = Integer.valueOf(spnFrom.getTag().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                breakEndTime = parent.getItemAtPosition(position).toString();
                if (breakEndTime.equalsIgnoreCase("12:00 AM")) {
                    breakEndTime = "11:59 PM";
                }

                int lastEndBreakTimePos = -1;
                if (spnTo.getTag() != null) {
                    lastEndBreakTimePos = Integer.valueOf(spnTo.getTag().toString());
                }

                addBreakTime(spnFrom.getSelectedItem().toString(), breakEndTime, lastEndBreakTimePos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        llAdableUnavailable.addView(view);
    }

    private void initViews() {

        tvDay = (TypedfaceTextView) v.findViewById(R.id.tvDay);

        cbAvailable = (ToggleButton) v.findViewById(R.id.cbAvailable);

        rlAddUnavailable = (RelativeLayout) v.findViewById(R.id.rlAddUnavailable);
        rlExpand = (RelativeLayout) v.findViewById(R.id.rlExpand);

        spnFromTime = (Spinner) v.findViewById(R.id.spnFromTime);
        spnToTime = (Spinner) v.findViewById(R.id.spnToTime);

        llMain = (LinearLayout) v.findViewById(R.id.llMain);
        llProgress = (LinearLayout) v.findViewById(R.id.llProgress);
        llUnavailable = (LinearLayout) v.findViewById(R.id.llUnavailable);
        llAdableUnavailable = (LinearLayout) v.findViewById(R.id.llAdableUnavailable);

        trTimeline = (TableRow) v.findViewById(R.id.trTimeLine);
        trWorkingTime = (TableRow) v.findViewById(R.id.trWorkingTime);

        tv1 = (TextView) v.findViewById(R.id.tv1);
        tv2 = (TextView) v.findViewById(R.id.tv2);

        imvDropDown = (ImageView) v.findViewById(R.id.imvDropDown);

        btnUseOther = (Button) v.findViewById(R.id.btnUseOther);
    }

    private boolean checkIsOvelap(ArrayList<Timeline> arrAllTime) {
        int count = arrAllTime.size();
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
                if (i != j) {
                    if ((arrAllTime.get(i).getStartDateTime()).compareTo(arrAllTime.get(j).getEndDateTime()) >= 0
                            && (arrAllTime.get(i).getStartDateTime()).compareTo(arrAllTime.get(j).getEndDateTime()) <= 0) {
                        return true;
                    } else if ((arrAllTime.get(i).getEndDateTime()).compareTo(arrAllTime.get(j).getStartDateTime()) >= 0
                            && (arrAllTime.get(i).getEndDateTime()).compareTo(arrAllTime.get(j).getEndDateTime()) <= 0) {
                        return true;
                    }
                }
            }
        }


        return false;
    }

    public void setDay(int day) {
        switch (day) {
            case 0:
                tvDay.setText("SUNDAY");
                break;
            case 1:
                tvDay.setText("MONDAY");
                break;
            case 2:
                tvDay.setText("TUESDAY");
                break;
            case 3:
                tvDay.setText("WEDNESDAY");
                break;
            case 4:
                tvDay.setText("THURSDAY");
                break;
            case 5:
                tvDay.setText("FRIDAY");
                break;
            case 6:
                tvDay.setText("SATURDAY");
                break;
            default:
                tvDay.setText("SUNDAY");
        }
    }
}