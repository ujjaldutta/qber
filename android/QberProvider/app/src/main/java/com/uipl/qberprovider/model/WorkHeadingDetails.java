package com.uipl.qberprovider.model;

import java.util.ArrayList;

/**
 * Created by richa on 12/12/16.
 */
public class WorkHeadingDetails {

    private String carWashType;
    private String workHeadingId;
    private String workHeadingName;
    private double workHeadingPrice;
    private ArrayList<WorkSubHeadingDetails> subHeadingDetailsArrayList = new ArrayList<>();

    public String getWorkHeadingName() {
        return workHeadingName;
    }

    public void setWorkHeadingName(String workHeadingName) {
        this.workHeadingName = workHeadingName;
    }

    public ArrayList<WorkSubHeadingDetails> getSubHeadingDetailsArrayList() {
        return subHeadingDetailsArrayList;
    }

    public void setSubHeadingDetailsArrayList(ArrayList<WorkSubHeadingDetails> subHeadingDetailsArrayList) {
        this.subHeadingDetailsArrayList = subHeadingDetailsArrayList;
    }

    public String getWorkHeadingId() {
        return workHeadingId;
    }

    public void setWorkHeadingId(String workHeadingId) {
        this.workHeadingId = workHeadingId;
    }

    public String getCarWashType() {
        return carWashType;
    }

    public void setCarWashType(String carWashType) {
        this.carWashType = carWashType;
    }

    public double getWorkHeadingPrice() {
        return workHeadingPrice;
    }

    public void setWorkHeadingPrice(double workHeadingPrice) {
        this.workHeadingPrice = workHeadingPrice;
    }
}
