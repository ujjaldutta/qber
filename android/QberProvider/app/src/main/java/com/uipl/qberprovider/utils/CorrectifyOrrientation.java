package com.uipl.qberprovider.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

/**
 * Created by Ankan on 3/25/2016.
 */
public class CorrectifyOrrientation {

    public static Bitmap decodeFile(String path) {

        int orientation;
        Bitmap bitmap = null;
        Bitmap outputBitmap = null;

        try {

            if (path == null) {

                return null;
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 6;
            bitmap = BitmapFactory.decodeFile(path, options);

            ExifInterface exif = new ExifInterface(path);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Matrix m = new Matrix();

            if ((orientation == 3)) {

                m.setRotate(180);

                outputBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != null)
                    bitmap.recycle();
                return outputBitmap;
            } else if (orientation == 6) {

                m.setRotate(90);

                outputBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != null)
                    bitmap.recycle();
                return outputBitmap;
            } else if (orientation == 8) {

                m.setRotate(270);

                outputBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
                if (bitmap != null)
                    bitmap.recycle();
                return outputBitmap;
            }
            return bitmap;
        } catch (Exception e) {
        }
        return null;
    }
}
