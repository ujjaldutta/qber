package com.uipl.qberprovider.utils;

import android.widget.EditText;
import android.widget.Spinner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by debasmita on 13/5/15.
 */
public class InputVadidation {


    public static boolean isEditTextContainEmail(EditText argEditText) {

        try {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(argEditText.getText().toString())
                    .matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    //^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$
    public static boolean isPasswordMatches(EditText pass, EditText conf) {

        boolean check = false;
        if(pass.getText().toString().equals(conf.getText().toString()))
        {
            check=true;
        }

        return check;
    }
    public static boolean isPasswordLengthCheck(EditText pass) {

        boolean check = false;
        if(pass.getText().toString().length()>=8)
        {
            check=true;
        }

        return check;
    }
    public static boolean isEditTextHasvalue(EditText edt)
    {
        boolean check;
        if(edt.getText().toString().trim().equals("") )
        {
            check=false;
        }
        else
            check=true;

        return check;

    }

    public static boolean isEditTextHasvalue2(EditText edt)
    {
        boolean check;
        if(edt.getText().toString().trim().equals("") ||edt.getText().toString().trim().equals("Min $") || edt.getText().toString().trim().equals("Max $") )
        {
            check=false;
        }
        else
            check=true;

        return check;

    }

    public static boolean isSpinnerSelected(Spinner spin)
    {
        boolean check;
        if(spin.getSelectedItemPosition()<1)
        {
            check=false;
        }
        else
            check=true;
        return check;
    }

    public static boolean isStringNullOrEmpty(String str) {
        boolean result = false;
        if (str == null || str.equals("")) {
            result = true;
        }
        return result;
    }
}
