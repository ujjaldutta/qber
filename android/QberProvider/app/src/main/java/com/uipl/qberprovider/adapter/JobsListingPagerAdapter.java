package com.uipl.qberprovider.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.uipl.qberprovider.CurrentJobsListingFragment;
import com.uipl.qberprovider.PastJobsListingFragment;
import com.uipl.qberprovider.R;

/**
 * Created by richa on 25/11/16.
 */
public class JobsListingPagerAdapter extends FragmentStatePagerAdapter {

    String tabTitles[] = new String[]{"Current Jobs", "Past Jobs"};
    Context context;

    public JobsListingPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new CurrentJobsListingFragment();
            case 1:
                return new PastJobsListingFragment();
            default:
                return null;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    public View getTabView(int position) {
        View tab = LayoutInflater.from(context).inflate(R.layout.inflate_job_tab, null);
        TextView tv = (TextView) tab.findViewById(R.id.custom_text);
        tv.setText(tabTitles[position]);
        return tab;
    }

}