package com.uipl.qberprovider.model;

import java.util.ArrayList;

/**
 * Created by richa on 14/10/16.
 */
public class Profile {

    private String userId;
    private String typeId;
    private String name;
    private String email;
    private String phone;
    private Double currentLat;
    private Double currentLng;
    private String providerId;
    private String serviceDetails;
    private String image;
    private String teamSize;
    private String nationality;
    private String maxPurchaseLimit;
    private String currentCreditBalance;
    private String bannerImage1;
    private String bannerImage2;
    private String bannerImage3;
    private boolean ownCar;
    private boolean arabicSpeaking;
    private String ratingString;
    private String totalRatingCount;
    private Float ratingFloat;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(String serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(String teamSize) {
        this.teamSize = teamSize;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMaxPurchaseLimit() {
        return maxPurchaseLimit;
    }

    public void setMaxPurchaseLimit(String maxPurchaseLimit) {
        this.maxPurchaseLimit = maxPurchaseLimit;
    }

    public String getCurrentCreditBalance() {
        return currentCreditBalance;
    }

    public void setCurrentCreditBalance(String currentCreditBalance) {
        this.currentCreditBalance = currentCreditBalance;
    }

    public String getBannerImage1() {
        return bannerImage1;
    }

    public void setBannerImage1(String bannerImage1) {
        this.bannerImage1 = bannerImage1;
    }

    public String getBannerImage2() {
        return bannerImage2;
    }

    public void setBannerImage2(String bannerImage2) {
        this.bannerImage2 = bannerImage2;
    }

    public String getBannerImage3() {
        return bannerImage3;
    }

    public void setBannerImage3(String bannerImage3) {
        this.bannerImage3 = bannerImage3;
    }

    public boolean isOwnCar() {
        return ownCar;
    }

    public void setOwnCar(boolean ownCar) {
        this.ownCar = ownCar;
    }

    public boolean isArabicSpeaking() {
        return arabicSpeaking;
    }

    public void setArabicSpeaking(boolean arabicSpeaking) {
        this.arabicSpeaking = arabicSpeaking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRatingString() {
        return ratingString;
    }

    public void setRatingString(String ratingString) {
        this.ratingString = ratingString;
    }

    public String getTotalRatingCount() {
        return totalRatingCount;
    }

    public void setTotalRatingCount(String totalRatingCount) {
        this.totalRatingCount = totalRatingCount;
    }

    public Float getRatingFloat() {
        return ratingFloat;
    }

    public void setRatingFloat(Float ratingFloat) {
        this.ratingFloat = ratingFloat;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(Double currentLat) {
        this.currentLat = currentLat;
    }

    public Double getCurrentLng() {
        return currentLng;
    }

    public void setCurrentLng(Double currentLng) {
        this.currentLng = currentLng;
    }
}
