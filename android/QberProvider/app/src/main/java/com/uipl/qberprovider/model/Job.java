package com.uipl.qberprovider.model;

import java.util.Date;

/**
 * Created by richa on 25/11/16.
 */
public class Job {

    private String id;
    private String headerLocation;
    private String fullAddress;
    private String status;
    private String dateString;
    private Date date;
    private String startTime;
    private String arrivalRangeStartTime;
    private String arrivalRangeEndTime;
    private String price;
    private String jobDuration;
    private String finishTime;
    private int type;
    private String numOfJobs;
    private String customerPhoneNumber;
    private String customerName;
    private String lat;
    private String lng;

    public String getHeaderLocation() {
        return headerLocation;
    }

    public void setHeaderLocation(String headerLocation) {
        this.headerLocation = headerLocation;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getArrivalRangeStartTime() {
        return arrivalRangeStartTime;
    }

    public void setArrivalRangeStartTime(String arrivalRangeStartTime) {
        this.arrivalRangeStartTime = arrivalRangeStartTime;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getJobDuration() {
        return jobDuration;
    }

    public void setJobDuration(String jobDuration) {
        this.jobDuration = jobDuration;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getArrivalRangeEndTime() {
        return arrivalRangeEndTime;
    }

    public void setArrivalRangeEndTime(String arrivalRangeEndTime) {
        this.arrivalRangeEndTime = arrivalRangeEndTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNumOfJobs() {
        return numOfJobs;
    }

    public void setNumOfJobs(String numOfJobs) {
        this.numOfJobs = numOfJobs;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
