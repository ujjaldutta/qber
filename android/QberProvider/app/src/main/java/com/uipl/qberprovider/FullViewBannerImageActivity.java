package com.uipl.qberprovider;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FullViewBannerImageActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivBannerImage;
    ImageButton ibBack;
    ProgressBar progressBar;

    ImageLoader imageLoader;
    DisplayImageOptions options;

    String image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_view_banner_image);
        image = getIntent().getStringExtra("image");
        initViews();

    }

    void initViews() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        ivBannerImage = (ImageView) findViewById(R.id.ivBannerImage);
        File file = new File(image);
        Uri uri = Uri.fromFile(file);
        ivBannerImage.setImageURI(uri);
        ibBack = (ImageButton) findViewById(R.id.ibBack);

        ibBack.setOnClickListener(this);
    }

    private void loadImageFromStorage(String path) {

        try {
            File f = new File(path, "profile.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            ivBannerImage.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ibBack:
                onBackPressed();
                break;
        }
    }

}
