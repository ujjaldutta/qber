package com.uipl.qberprovider.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.JobDetailsActivity;
import com.uipl.qberprovider.LoginRegisterActivity;
import com.uipl.qberprovider.R;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.Job;
import com.uipl.qberprovider.utils.CommonMethods;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by richa on 25/11/16.
 */
public class CurrentJobsAdapter extends RecyclerView.Adapter<CurrentJobsAdapter.CurrentJobsViewHolder> {

    ArrayList<Job> jobArrayList = new ArrayList<>();
    Activity activity;
    DialogView dialogview;
    Preferences pref;
    private RequestQueue mQueue;
    private String TAG = "CurrentJobAdapter";
    private String str_START = "";
    final int INTENT_RELOAD = 2;
    boolean isAm = false;
    int currentPos = 0;
    SimpleDateFormat time12format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    SimpleDateFormat time24format = new SimpleDateFormat("HH:mm:ss");
    boolean callPermission = false;

    public static class CurrentJobsViewHolder extends RecyclerView.ViewHolder {

        TextView tvNum, tvHeaderLocation, tvFullAddress, tvStatus, tvDate, tvArrivalStartTime, tvArrivalAmPm, tvArrivalEndTime, tvArrivalEndAmPm, tvPrice, tvFinishTime,
                tvFinishTimeAmPm, tvDurationHourValue, tvDurationHour, tvDurationMinValue, tvDurationMin, tvSubStatus;
        LinearLayout llLocation, llCallCustomer, llDetails;
        ImageView imgEditPrice, imgEditJobDuration, imgEditFinishTime;
        TableRow trDate, trArrivalTimeRange, trPrice, trJobDuration, trFinishTime;


        public CurrentJobsViewHolder(View v) {
            super(v);

            tvNum = (TextView) v.findViewById(R.id.tvNum);
            tvHeaderLocation = (TextView) v.findViewById(R.id.tvHeaderLocation);
            tvFullAddress = (TextView) v.findViewById(R.id.tvFullAddress);
            tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            tvSubStatus = (TextView) v.findViewById(R.id.tvSubStatus);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvArrivalStartTime = (TextView) v.findViewById(R.id.tvArrivalStartTime);
            tvArrivalAmPm = (TextView) v.findViewById(R.id.tvArrivalStartAmPm);
            tvArrivalEndTime = (TextView) v.findViewById(R.id.tvArrivalEndTime);
            tvArrivalEndAmPm = (TextView) v.findViewById(R.id.tvArrivalEndAmPm);
            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            tvFinishTime = (TextView) v.findViewById(R.id.tvFinishTime);
            tvFinishTimeAmPm = (TextView) v.findViewById(R.id.tvFinishAmPm);
            tvDurationHourValue = (TextView) v.findViewById(R.id.tvDurationHourValue);
            tvDurationHour = (TextView) v.findViewById(R.id.tvDurationHour);
            tvDurationMinValue = (TextView) v.findViewById(R.id.tvDurationMinValue);
            tvDurationMin = (TextView) v.findViewById(R.id.tvDurationMin);

            imgEditPrice = (ImageView) v.findViewById(R.id.imgEditPrice);
            imgEditJobDuration = (ImageView) v.findViewById(R.id.imgEditJobDuration);
            imgEditFinishTime = (ImageView) v.findViewById(R.id.imgEditFinishTime);

            llLocation = (LinearLayout) v.findViewById(R.id.llLocation);
            llCallCustomer = (LinearLayout) v.findViewById(R.id.llCallCustomer);
            llDetails = (LinearLayout) v.findViewById(R.id.llDetails);

            trDate = (TableRow) v.findViewById(R.id.trDate);
            trArrivalTimeRange = (TableRow) v.findViewById(R.id.trArrivalTimeRange);
            trPrice = (TableRow) v.findViewById(R.id.trPrice);
            trJobDuration = (TableRow) v.findViewById(R.id.trJobDuration);
            trFinishTime = (TableRow) v.findViewById(R.id.trFinishTime);
        }
    }

    public CurrentJobsAdapter(ArrayList<Job> mJobArrayList, Activity mContext, boolean mCallPermission) {
        this.jobArrayList = mJobArrayList;
        this.activity = mContext;
        this.dialogview = new DialogView(mContext);
        this.pref = new Preferences(mContext);
        mQueue = Volley.newRequestQueue(mContext);
        this.callPermission = mCallPermission;
    }

    @Override
    public CurrentJobsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_current_job, parent, false);
        CurrentJobsViewHolder vh = new CurrentJobsViewHolder(v);
        return vh;
    }

    public void setCallPermission(boolean callPermission1) {
        this.callPermission = callPermission1;
    }

    @Override
    public void onBindViewHolder(CurrentJobsViewHolder holder, final int position) {
        final Job job = jobArrayList.get(position);
        final String customerPhoneNumber = job.getCustomerPhoneNumber();
        holder.tvNum.setText(String.valueOf(position + 1));
        holder.tvDate.setText(job.getDateString());
        if (job.getHeaderLocation() != null && !job.getHeaderLocation().toString().trim().equals("")) {
            holder.tvHeaderLocation.setText(activity.getResources().getString(R.string.at) + " " + job.getHeaderLocation());
        }
        if (job.getFullAddress() != null && !job.getFullAddress().toString().trim().equals("")) {
            holder.tvFullAddress.setVisibility(View.VISIBLE);
            holder.tvFullAddress.setText(job.getFullAddress());
        } else {
            holder.tvFullAddress.setVisibility(View.GONE);
        }
        holder.tvPrice.setText(GlobalVariable.decimalFormat.format(Double.parseDouble(job.getPrice())));

        String status = job.getStatus();
        String subStatus = "";
        final String subStatusArr[] = status.split(" ");
        if (subStatusArr[0].toString().trim().equalsIgnoreCase("confirmed") || subStatusArr[0].toString().trim().equalsIgnoreCase("working")
                || subStatusArr[0].toString().trim().equalsIgnoreCase("heading")) {

            holder.imgEditFinishTime.setVisibility(View.VISIBLE);
            holder.imgEditJobDuration.setVisibility(View.VISIBLE);
            holder.imgEditPrice.setVisibility(View.VISIBLE);
            holder.trArrivalTimeRange.setVisibility(View.VISIBLE);
            holder.trJobDuration.setVisibility(View.VISIBLE);
            holder.trFinishTime.setVisibility(View.VISIBLE);

            String jobDuration = job.getJobDuration();
            if (jobDuration != null && !jobDuration.toString().trim().equals("")) {
                int time = Integer.parseInt(jobDuration);
                int hour = time / 60;
                int min = time % 60;
                if (hour > 0) {
                    holder.tvDurationHour.setVisibility(View.VISIBLE);
                    holder.tvDurationHourValue.setVisibility(View.VISIBLE);
                    holder.tvDurationHourValue.setText(String.valueOf(hour));
                } else {
                    holder.tvDurationHour.setVisibility(View.GONE);
                    holder.tvDurationHourValue.setVisibility(View.GONE);
                }
                holder.tvDurationMinValue.setText(String.valueOf(min));
            } else {
                holder.tvDurationHour.setVisibility(View.GONE);
                holder.tvDurationHourValue.setVisibility(View.GONE);
                holder.tvDurationMinValue.setText(jobDuration);
            }

            String sTime = job.getArrivalRangeStartTime();
            String eTime = job.getArrivalRangeEndTime();
            String finshTime = job.getFinishTime();
            holder.tvArrivalStartTime.setText(sTime.substring(0, 5));
            holder.tvArrivalAmPm.setText(sTime.substring(6));
            holder.tvArrivalEndTime.setText(eTime.substring(0, 5));
            holder.tvArrivalEndAmPm.setText(eTime.substring(6));
            if (finshTime != null && finshTime.length() > 6) {
                holder.tvFinishTime.setText(finshTime.substring(0, 5));
                holder.tvFinishTimeAmPm.setText(finshTime.substring(6));
            }
        } else {
            holder.imgEditFinishTime.setVisibility(View.INVISIBLE);
            holder.imgEditJobDuration.setVisibility(View.INVISIBLE);
            holder.imgEditPrice.setVisibility(View.INVISIBLE);
            holder.trArrivalTimeRange.setVisibility(View.GONE);
            holder.trJobDuration.setVisibility(View.GONE);
            holder.trFinishTime.setVisibility(View.GONE);
        }
        if (subStatusArr != null && subStatusArr.length > 1) {
            holder.tvStatus.setText(subStatusArr[0]);
            holder.tvSubStatus.setVisibility(View.VISIBLE);
            for (int i = 1; i < subStatusArr.length; i++) {
                if (i == 1) {
                    subStatus = subStatusArr[i].substring(0, 1).toUpperCase() + subStatusArr[i].substring(1);
                } else {
                    subStatus = subStatus + " " + subStatusArr[i].substring(0, 1).toUpperCase() + subStatusArr[i].substring(1);
                }
            }
            holder.tvSubStatus.setText(subStatus);
        } else {
            holder.tvStatus.setText(job.getStatus());
            holder.tvSubStatus.setVisibility(View.GONE);
        }

        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (subStatusArr[0].toString().trim().equalsIgnoreCase("confirmed") || subStatusArr[0].toString().trim().equalsIgnoreCase("working")
                        || subStatusArr[0].toString().trim().equalsIgnoreCase("heading")) {
                    activity.startActivityForResult(new Intent(activity, JobDetailsActivity.class)
                            .putExtra("serviceId", job.getId())
                            .putExtra("isEditable", true), INTENT_RELOAD);
                } else {
                    activity.startActivityForResult(new Intent(activity, JobDetailsActivity.class)
                            .putExtra("serviceId", job.getId())
                            .putExtra("isEditable", false), INTENT_RELOAD);
                }
            }
        });

        holder.llCallCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!callPermission) {
                    Toast.makeText(activity, activity.getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                } else {
                    if (customerPhoneNumber != null && !customerPhoneNumber.toString().trim().equals("")) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+" + customerPhoneNumber));
                        activity.startActivity(intent);
                    }
                }
            }
        });

        holder.llLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String label = job.getHeaderLocation() + "," + job.getFullAddress();
                String uriBegin = "geo:" + job.getLat() + "," + job.getLng();
                String query = job.getLat() + "," + job.getLng() + "(" + label + ")";
                String encodedQuery = Uri.encode(query);
                String uriString = uriBegin + "?q=" + encodedQuery;
                Uri uri = Uri.parse(uriString);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
            }
        });

        holder.imgEditJobDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPos = position;
                showCustomEditDurationDialog(activity, job.getId(), job.getJobDuration());
            }
        });
        holder.imgEditFinishTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPos = position;
                showCustomEditFinishDialog(activity, job.getId(), job.getFinishTime());
            }
        });
        holder.imgEditPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPos = position;
                showCustomEditPriceDialog(activity, job.getId(), job.getPrice());
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobArrayList.size();
    }

    public void showCustomEditPriceDialog(final Context context, final String serviceid, final String price) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_edit_price);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);
        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
        final EditText editPrice = (EditText) dialog.findViewById(R.id.editPrice);
        editPrice.setText(price);
        editPrice.setSelection(price.length());

        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String price = "";
                if (editPrice.getText() != null && !editPrice.getText().toString().trim().equals("")) {
                    price = editPrice.getText().toString().trim();
                    volleyUpdatePriceTask(serviceid, price);
                    dialog.dismiss();
                } else {
                    dialogview.showCustomSingleButtonDialog(activity, activity.getResources().getString(R.string.sorry),
                            activity.getResources().getString(R.string.enter_valid_price));
                }
            }
        });

        dialog.show();
    }

    public void showCustomEditDurationDialog(final Context context, final String serviceid, final String duration) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_edit_duration);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);
        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);

        final TextView tvHR = (TextView) dialog.findViewById(R.id.tvHR);
        final TextView tvMIN = (TextView) dialog.findViewById(R.id.tvMIN);


        if (Integer.parseInt(duration) > 60
                && Integer.parseInt(duration) % 60 != 0) {
            int hours = Integer.parseInt(duration) / 60; //since both are ints, you get an int
            int minutes = Integer.parseInt(duration) % 60;
            tvHR.setText("" + hours);
            tvMIN.setText("" + minutes);

        } else if (Integer.parseInt(duration) % 60 == 0) {
            int hours = Integer.parseInt(duration) / 60;
            tvHR.setText("" + hours);
            tvMIN.setText("0");
        } else {
            tvHR.setText("0");
            tvMIN.setText(duration);
        }

        tvHR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberPicker(context, Integer.parseInt(tvHR.getText().toString()), 23, tvHR);
            }
        });

        tvMIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberPicker(context, Integer.parseInt(tvMIN.getText().toString()), 59, tvMIN);
            }
        });

        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.hideSoftKeyboard(context, v);
                int updated_duration = (Integer.parseInt(tvHR.getText().toString()) * 60)
                        + Integer.parseInt(tvMIN.getText().toString());
                if (updated_duration <= 0) {
                    dialogview.showCustomSingleButtonDialog(activity, activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.enter_valid_time));
                } else if (updated_duration != Integer.parseInt(duration)) {
                    volleyUpdateDurationTask(serviceid, String.valueOf(updated_duration));
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    public void showCustomEditFinishDialog(final Context context, final String serviceid, String from_time) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_edit_breaktime);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        final TextView tvStartTime = (TextView) dialog.findViewById(R.id.tvStartTime);
        final TextView tvStartPM = (TextView) dialog.findViewById(R.id.tvStartPM);
        final TextView tvStartAM = (TextView) dialog.findViewById(R.id.tvStartAM);

        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);
        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);

        if (from_time.substring(from_time.length() - 2).equalsIgnoreCase("PM")) {
            tvStartPM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
            tvStartPM.setTextColor(context.getResources().getColor(R.color.white));
            isAm = false;
        } else if (from_time.substring(from_time.length() - 2).equalsIgnoreCase("AM")) {
            tvStartAM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
            tvStartAM.setTextColor(context.getResources().getColor(R.color.white));
            isAm = true;
        }

        tvStartTime.setText(from_time.substring(0, from_time.length() - 3));
        str_START = from_time;

        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(context, "Select Start Time", str_START, tvStartTime, tvStartPM, tvStartAM);
            }
        });

        tvStartPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAm = false;
                tvStartPM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                tvStartAM.setBackgroundResource(R.drawable.full_white_rounded_bg);
                tvStartPM.setTextColor(context.getResources().getColor(R.color.white));
                tvStartAM.setTextColor(context.getResources().getColor(R.color.black));
            }
        });

        tvStartAM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAm = true;
                tvStartAM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                tvStartPM.setBackgroundResource(R.drawable.full_white_rounded_bg);
                tvStartPM.setTextColor(context.getResources().getColor(R.color.black));
                tvStartAM.setTextColor(context.getResources().getColor(R.color.white));
            }
        });

        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String finishTime = "";
                finishTime = tvStartTime.getText().toString();
                if (isAm) {
                    finishTime = finishTime + " AM";
                } else {
                    finishTime = finishTime + " PM";
                }
                String time = "";
                try {
                    time = time24format.format(time12format.parse(finishTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                volleyUpdateFinishTimeTask(serviceid, time);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showNumberPicker(Context context, int presetValue, int totalValue, final TextView textView) {
        final Dialog npdialog = new Dialog(context);
        // to remove the dialog title...
        npdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        npdialog.setContentView(R.layout.number_picker_dialog);
        npdialog.setCanceledOnTouchOutside(false);
        npdialog.setCancelable(false);
        // dialog.setTitle("Contacts");
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(npdialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        npdialog.show();
        npdialog.getWindow().setAttributes(lp);
        npdialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);

        final NumberPicker np = (NumberPicker) npdialog
                .findViewById(R.id.numberPicker);

        np.setMinValue(0);
        np.setMaxValue(totalValue);
        np.setValue(presetValue);
        np.setWrapSelectorWheel(false);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        Button btnCancel = (Button) npdialog
                .findViewById(R.id.btnCancel);
        Button btnDone = (Button) npdialog
                .findViewById(R.id.btnDone);

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker,
                                      int oldVal, int newVal) {
                // TODO Auto-generated method stub
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                npdialog.dismiss();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("V:" + np.getValue());
                textView.setText("" + np.getValue());
                npdialog.dismiss();
            }
        });

    }

    private void showTimePicker(final Context mContext, final String title, String time, final TextView setTime, final TextView pm, final TextView am) {

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);


        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String format;
                if (selectedHour == 0) {
                    selectedHour += 12;
                    format = "AM";
                } else if (selectedHour == 12) {
                    format = "PM";
                } else if (selectedHour > 12) {
                    selectedHour -= 12;
                    format = "PM";
                } else {
                    format = "AM";
                }

                String min = "";
                if (selectedMinute < 10)
                    min = "0" + selectedMinute;
                else
                    min = String.valueOf(selectedMinute);

                setTime.setText(selectedHour + ":" + min);

                if (format.equalsIgnoreCase("AM")) {
                    am.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                    am.setTextColor(mContext.getResources().getColor(R.color.white));
                    pm.setBackgroundResource(R.drawable.full_white_rounded_bg);
                    pm.setTextColor(mContext.getResources().getColor(R.color.black));
                } else if (format.equalsIgnoreCase("PM")) {
                    pm.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                    pm.setTextColor(mContext.getResources().getColor(R.color.white));
                    am.setBackgroundResource(R.drawable.full_white_rounded_bg);
                    am.setTextColor(mContext.getResources().getColor(R.color.black));
                }
                str_START = selectedHour + ":" + min + " " + format;


            }
        }, hour, minute, false);//Yes 12 hour time
        mTimePicker.setTitle(title);
        mTimePicker.show();
    }

    private void volleyUpdateDurationTask(final String serviceId, final String mDuration) {
        dialogview.showCustomSpinProgress(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_DURATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    Toast.makeText(activity, object.optString("msg"), Toast.LENGTH_SHORT).show();
                                    Job job = new Job();
                                    job = jobArrayList.get(currentPos);
                                    job.setJobDuration(mDuration);
                                    jobArrayList.set(currentPos, job);
                                    notifyItemChanged(currentPos);
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    activity.startActivity(new Intent(activity, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(activity,
                                            activity.getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(activity,
                                    activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(activity, Preferences.PHONE));
                params.put("token", pref.getStringPreference(activity, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("duration", mDuration);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyUpdateFinishTimeTask(final String serviceId, final String endTime) {
        dialogview.showCustomSpinProgress(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_END_TIME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    Toast.makeText(activity, object.optString("msg"), Toast.LENGTH_SHORT).show();
                                    Job job = new Job();
                                    job = jobArrayList.get(currentPos);
                                    try {
                                        job.setFinishTime(time12format.format(time24format.parse(endTime)));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    jobArrayList.set(currentPos, job);
                                    notifyItemChanged(currentPos);
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    activity.startActivity(new Intent(activity, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(activity, activity.getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(activity,
                                    activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(activity, Preferences.PHONE));
                params.put("token", pref.getStringPreference(activity, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("to_time", endTime);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyUpdatePriceTask(final String serviceId, final String price) {
        dialogview.showCustomSpinProgress(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_PRICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    Toast.makeText(activity, object.optString("msg"), Toast.LENGTH_SHORT).show();
                                    Job job = new Job();
                                    job = jobArrayList.get(currentPos);
                                    job.setPrice(price);
                                    jobArrayList.set(currentPos, job);
                                    notifyItemChanged(currentPos);
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    activity.startActivity(new Intent(activity, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(activity,
                                            activity.getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(activity, activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(activity, Preferences.PHONE));
                params.put("token", pref.getStringPreference(activity, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("price", price);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }
}
