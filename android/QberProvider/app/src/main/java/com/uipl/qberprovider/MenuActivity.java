package com.uipl.qberprovider;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.utils.CommonMethods;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MenuActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener, View.OnClickListener {

    private ListView ivMenuList;
    int selectPosition = 0, lastPosition = 0;
    String[] menuList = {"Dashboard", "Schedule", "Jobs", "Profile", "Availability", "Setting", "Payment", "Inbox", "Contact Us", "Tutorial", "Logout"};
    int[] selectedMenuIcon = {R.drawable.dashboard_hv, R.drawable.menu_schedule_hover, R.drawable.job_hv, R.drawable.profile_hv,
            R.drawable.clock_hv, R.drawable.setting_hv, R.drawable.payment_hv,
            R.drawable.email_hv, R.drawable.phone_hv, R.drawable.play_hv, R.drawable.log_out_hv};
    int[] menuIcon = {R.drawable.dashboard, R.drawable.menu_schedule, R.drawable.job, R.drawable.profile, R.drawable.clock,
            R.drawable.setting, R.drawable.payment, R.drawable.email, R.drawable.phone, R.drawable.play, R.drawable.log_out};
    private MenuListAdapter adapterMenuList;
    private Toolbar toolbar;
    public static TextView tvToolbarTitle;
    public static ImageButton ibSave, ibMenu;
    private Preferences pref;
    public String titleText = "";
    private boolean menuClickFlag = false;

    DialogView dialogView;
    RequestQueue mQueue;
    final String TAG = "MenuActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        pref = new Preferences(MenuActivity.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        initToolbar();
        initView();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                CommonMethods.hideSoftKeyboard(MenuActivity.this, drawerView);
                super.onDrawerOpened(drawerView);
                titleText = tvToolbarTitle.getText().toString();
                tvToolbarTitle.setText(getResources().getString(R.string.main_menu));
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                tvToolbarTitle.setText(titleText);
                if (menuClickFlag) {
                    loadFragment(selectPosition);
                }
                menuClickFlag = false;
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);

        titleText = getString(R.string.dashboard);
        tvToolbarTitle.setText(titleText);
        DashboardFragment dashboardFragment = new DashboardFragment();
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, dashboardFragment, DashboardFragment.TAG).commit();


    }


    void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        ibMenu = (ImageButton) toolbar.findViewById(R.id.ibLeft);
        ibMenu.setVisibility(View.VISIBLE);
        ibMenu.setImageResource(R.drawable.main_menu);
        ibMenu.setOnClickListener(this);
        ibSave = (ImageButton) toolbar.findViewById(R.id.ibRight);
    }

    private void initView() {

        mQueue = Volley.newRequestQueue(this);
        dialogView = new DialogView(this);

        ivMenuList = (ListView) findViewById(R.id.ivMenuList);
        adapterMenuList = new MenuListAdapter(this, menuList, selectedMenuIcon, menuIcon);
        ivMenuList.setAdapter(adapterMenuList);
        ivMenuList.setOnItemClickListener(this);

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
        android.support.v4.app.Fragment supportFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (fragment != null && fragment instanceof DashboardFragment) {
            if (supportFragment != null && supportFragment instanceof FragmentAvailibilitySetup) {
                titleText = getString(R.string.dashboard);
                selectPosition = 0;
                adapterMenuList.notifyDataSetChanged();
                tvToolbarTitle.setText(titleText);
            /*first remove getSupportFragment*/
                getSupportFragmentManager().beginTransaction().remove(supportFragment).commit();
                DashboardFragment dashboardFragment = new DashboardFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, dashboardFragment, DashboardFragment.TAG).commit();
            } else {
                super.onBackPressed();
            }
        } else {
            titleText = getString(R.string.dashboard);
            selectPosition = 0;
            adapterMenuList.notifyDataSetChanged();
            tvToolbarTitle.setText(titleText);
            /*first remove getSupportFragment*/
            if (supportFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(supportFragment).commit();
            }
            ibSave.setVisibility(View.GONE);
            DashboardFragment dashboardFragment = new DashboardFragment();
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, dashboardFragment, DashboardFragment.TAG).commit();

        }
    }

    /**
     * Method use to open/close Navigation View
     */
    public void closeNavigationView() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        lastPosition = selectPosition;
        CommonMethods.hideSoftKeyboard(this, view);
        view.setSelected(true);
        selectPosition = i;
        adapterMenuList.notifyDataSetChanged();
        menuClickFlag = true;
        switch (i) {
            case 0:
                titleText = getString(R.string.dashboard);
                break;
            case 1:
                titleText = getString(R.string.schedule);
                break;
            case 2:
                titleText = getResources().getString(R.string.jobs);
                break;
            case 3:
                titleText = getString(R.string.profile);
                break;
            case 4:
                titleText = getString(R.string.availability);
                break;
            case 5:
                titleText = getString(R.string.setting);
                break;
            case 6:
                titleText = getString(R.string.balance_settling);
                break;
            case 7:
                break;
            case 8:
                break;
            case 9:
                break;
            case 10:
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void loadFragment(int i) {
        if (i != 10) {
            android.support.v4.app.Fragment supportFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (supportFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(supportFragment).commit();
            }
        }
        switch (i) {
            case 0:
                ibSave.setVisibility(View.GONE);
                DashboardFragment dashboardFragment = new DashboardFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, dashboardFragment, DashboardFragment.TAG).commit();
                break;
            case 1:
                ibSave.setVisibility(View.GONE);
                ScheduleFragment scheduleFragment = new ScheduleFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, scheduleFragment, ScheduleFragment.TAG).commit();
                break;
            case 2:
                ibSave.setVisibility(View.GONE);
                JobsListingFragment jobsListingFragment = new JobsListingFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, jobsListingFragment, JobsListingFragment.TAG).commit();
                break;
            case 3:
                ProfileFragment profileFragment = new ProfileFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, profileFragment, ProfileFragment.TAG).commit();
                break;
            case 4:
                ibSave.setVisibility(View.GONE);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentAvailibilitySetup(), FragmentAvailibilitySetup.TAG).commit();
                break;
            case 5:
                SettingFragment settingFragment = new SettingFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, settingFragment, SettingFragment.TAG).commit();
                break;
            case 6:
                ibSave.setVisibility(View.GONE);
                BalanceSettlingFragment balanceSettlingFragment = new BalanceSettlingFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, balanceSettlingFragment, BalanceSettlingFragment.TAG).commit();
                break;
            case 7:
                Toast.makeText(MenuActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
                break;
            case 8:
                Toast.makeText(MenuActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
                break;
            case 9:
                Toast.makeText(MenuActivity.this, "Coming soon", Toast.LENGTH_SHORT).show();
                break;
            case 10:
                LogOutDialog(MenuActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_logout));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof JobsListingFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibLeft:
                closeNavigationView();
                break;
        }
    }

    public class MenuListAdapter extends BaseAdapter {

        private final String[] menuList;
        private final int[] selectedMenuIcon;
        private final int[] menuIcon;
        private final MenuActivity menuActivity;
        LayoutInflater inflater;

        public MenuListAdapter(MenuActivity menuActivity, String[] menuList, int[] selectedMenuIcon, int[] menuIcon) {
            this.menuActivity = menuActivity;
            this.menuList = menuList;
            this.selectedMenuIcon = selectedMenuIcon;
            this.menuIcon = menuIcon;
            inflater = LayoutInflater.from(menuActivity);

        }

        @Override
        public int getCount() {
            return menuList.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.inflate_menu_list, viewGroup, false);
            }
            LinearLayout llHeader = (LinearLayout) view.findViewById(R.id.llHeader);
            ImageView imgMenuIcon = (ImageView) view.findViewById(R.id.imgMenuIcon);
            TextView tvMenuName = (TextView) view.findViewById(R.id.tvMenuName);
            TextView tvDot = (TextView) view.findViewById(R.id.tvDot);
            tvMenuName.setText(menuList[i]);
            if (selectPosition == i) {
                imgMenuIcon.setImageResource(selectedMenuIcon[i]);
                tvMenuName.setTextColor(menuActivity.getResources().getColor(R.color.white));
                llHeader.setBackgroundColor(menuActivity.getResources().getColor(R.color.theme_color));
                tvDot.setVisibility(View.VISIBLE);
            } else {
                imgMenuIcon.setImageResource(menuIcon[i]);
                tvMenuName.setTextColor(menuActivity.getResources().getColor(R.color.nav_text_color));
                llHeader.setBackgroundColor(menuActivity.getResources().getColor(android.R.color.transparent));
                tvDot.setVisibility(View.INVISIBLE);
            }
            return view;
        }
    }

    public void LogOutDialog(Context context, String header, String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                volleyLogoutRequest(pref.getStringPreference(MenuActivity.this, Preferences.PHONE));
            }
        });
        adb.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                selectPosition = lastPosition;
                adapterMenuList.notifyDataSetChanged();
            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

    private void volleyLogoutRequest(final String mobile) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_LOGOUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(MenuActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(MenuActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogView.showCustomSingleButtonDialog(MenuActivity.this,
                                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(MenuActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(MenuActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(MenuActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(MenuActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(MenuActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(MenuActivity.this, Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }
}
