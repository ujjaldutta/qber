package com.uipl.qberprovider.model;

import java.util.Date;

/**
 * Created by richa on 17/10/16.
 */
public class Timeline {

    private String startTime;
    private String endTime;
    private Date startDateTime;
    private Date endDateTime;
    private boolean active;
    private boolean available;
    private float diffInHours;
    private int breakTimePosition;
    private String workingStartTime;
    private String workingEndTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public float getDiffInHours() {
        return diffInHours;
    }

    public void setDiffInHours(float diffInHours) {
        this.diffInHours = diffInHours;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getBreakTimePosition() {
        return breakTimePosition;
    }

    public void setBreakTimePosition(int breakTimePosition) {
        this.breakTimePosition = breakTimePosition;
    }

    public String getWorkingStartTime() {
        return workingStartTime;
    }

    public void setWorkingStartTime(String workingStartTime) {
        this.workingStartTime = workingStartTime;
    }

    public String getWorkingEndTime() {
        return workingEndTime;
    }

    public void setWorkingEndTime(String workingEndTime) {
        this.workingEndTime = workingEndTime;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }
}
