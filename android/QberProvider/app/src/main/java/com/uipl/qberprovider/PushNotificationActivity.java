package com.uipl.qberprovider;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.Constants;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.JobDetails;
import com.uipl.qberprovider.model.Reason;
import com.uipl.qberprovider.model.WorkHeadingDetails;
import com.uipl.qberprovider.model.WorkSubHeadingDetails;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PushNotificationActivity extends AppCompatActivity {

    DialogView dialogView;
    RequestQueue mQueue;
    Preferences pref;

    TextView tvName, tvRatingCount, tvTotalReviews, tvDate, tvArrivalTime, tvLocation, tvTotalAmount, tvWait, tvConfirm, tvHeading, tvDistance;
    LinearLayout llReject, llWait, llConfirm, llCallCustomer, llMain;
    TableLayout tlAddable;
    ImageView ivProfileImg;
    RatingBar ratingBar;
    AlertDialog alertDialog;
    Dialog dataDialog;

    ArrayList<WorkHeadingDetails> suvArrList = new ArrayList<>();
    ArrayList<WorkHeadingDetails> sedanArrList = new ArrayList<>();
    ArrayList<Reason> reasonArrList = new ArrayList<>();

    public static final String TAG = "PushNotifActivity";
    String serviceId = "", type = "";
    double totalPrice = 0;
    boolean autoCancelService = true;
    JobDetails jobDetails = new JobDetails();

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    CountDownTimer timer_13seconds, timer_5minute;

    BroadcastReceiver pushNotificationTimerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION)) {
                updateUI(intent);
            }
        }
    };

    private void updateUI(Intent intent) {
        boolean isTimerRunning = intent.getBooleanExtra("isTimerRunning", false);
        if (isTimerRunning) {
            String time = intent.getStringExtra("time");
            if (tvWait != null) {
                tvWait.setText(time);
            }
        } else {
            if (autoCancelService) {
                autoCancelService = false;
                mQueue.cancelAll(TAG);
                onBackPressed();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_notification);

        IntentFilter pushNotificationTimerFilter = new IntentFilter(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION);
        pushNotificationTimerFilter.addAction(Constants.PushNotificationTimerBroadcast.PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION);
        registerReceiver(pushNotificationTimerReceiver, pushNotificationTimerFilter);

        initViews();

        GlobalVariable.reloadJobsListingFragment = true;

        serviceId = getIntent().getStringExtra("serviceId");
        type = getIntent().getStringExtra("type"); // type = jobcreated

        volleyGetJobDetails(pref.getStringPreference(this, Preferences.PHONE), serviceId);
    }

    void initViews() {
        dialogView = new DialogView(this);
        mQueue = Volley.newRequestQueue(this);
        pref = new Preferences(this);
    }

    private void volleyGetJobDetails(final String mobile, final String serviceId) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_JOB_DETAILS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);

                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    jobDetails = new JobDetails();

                                    JSONObject dataJsonObj = responseJsonObj.optJSONObject("data");
                                    jobDetails.setJobId(dataJsonObj.optString("job_id"));
                                    jobDetails.setLocation(dataJsonObj.optString("address"));
                                    jobDetails.setStartTime(dataJsonObj.optString("from_time"));
                                    jobDetails.setEndTime(dataJsonObj.optString("to_time"));
                                    jobDetails.setCustomerRating(dataJsonObj.optString("rating"));
                                    jobDetails.setCustomerReviewsCount(dataJsonObj.optString("ratecount"));
                                    jobDetails.setDistance(dataJsonObj.optString("distance"));
                                    jobDetails.setCustomerName(dataJsonObj.optString("customer_name"));
                                    jobDetails.setCustomerPhoneNumber(dataJsonObj.optString("customer_phone"));

                                    JSONObject carDetailsJsonObj = dataJsonObj.optJSONObject("cardetails");

                                    if (carDetailsJsonObj != null && carDetailsJsonObj.length() > 0) {
                                        JSONArray nameArr = carDetailsJsonObj.names();
                                        for (int k = 0; k < nameArr.length(); k++) {
                                            Log.e(TAG, nameArr.opt(k).toString());
                                        }
                                        for (int j = 0; j < carDetailsJsonObj.length(); j++) {
                                            WorkHeadingDetails workHeadingDetails = new WorkHeadingDetails();
                                            String arrName = nameArr.opt(j).toString();
                                            JSONArray carJsonArr = carDetailsJsonObj.optJSONArray(arrName);
                                            int length = arrName.length();
                                            if (length > 0) {
                                                arrName = arrName.substring(0, length - 1) + " " + arrName.charAt(length - 1);
                                                workHeadingDetails.setWorkHeadingName(arrName);
                                                ArrayList<WorkSubHeadingDetails> workSubHeadingDetailsArrayList = new ArrayList<>();
                                                double totalPrice = 0;
                                                for (int k = 0; k < carJsonArr.length(); k++) {
                                                    JSONArray jsonArr = carJsonArr.optJSONArray(k);
                                                    JSONObject jsonObj = jsonArr.optJSONObject(0);
                                                    WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                                    workSubHeadingDetails.setWorkSubHeadingId(jsonObj.optString("id"));
                                                    workSubHeadingDetails.setWorkSubHeadingName(jsonObj.optString("name"));
                                                    totalPrice = totalPrice + Double.parseDouble(jsonObj.optString("cost"));
                                                    workSubHeadingDetails.setWorkSubHeadingPrice(Double.parseDouble(jsonObj.optString("cost")));
                                                    workSubHeadingDetails.setSelected(true);
                                                    workSubHeadingDetailsArrayList.add(workSubHeadingDetails);
                                                }
                                                workHeadingDetails.setSubHeadingDetailsArrayList(workSubHeadingDetailsArrayList);
                                                workHeadingDetails.setWorkHeadingPrice(totalPrice);
                                                if (length == 4) {
                                                    suvArrList.add(workHeadingDetails);
                                                } else {
                                                    sedanArrList.add(workHeadingDetails);
                                                }
                                            }
                                        }
                                    }
                                    showDialog();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(PushNotificationActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(PushNotificationActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void showDialog() {

        dataDialog = new Dialog(this);
        dataDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dataDialog.setContentView(R.layout.dialog_push_notification);
        dataDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dataDialog.setCancelable(true);
        dataDialog.setCanceledOnTouchOutside(false);

        dataDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                onBackPressed();
            }
        });

        ivProfileImg = (ImageView) dataDialog.findViewById(R.id.ivProfileImg);
        tvHeading = (TextView) dataDialog.findViewById(R.id.tvHeading);
        tvName = (TextView) dataDialog.findViewById(R.id.tvName);
        tvRatingCount = (TextView) dataDialog.findViewById(R.id.tvRateCount);
        tvTotalReviews = (TextView) dataDialog.findViewById(R.id.tvTotalReviews);
        tvDate = (TextView) dataDialog.findViewById(R.id.tvDate);
        tvArrivalTime = (TextView) dataDialog.findViewById(R.id.tvArrivalTime);
        tvLocation = (TextView) dataDialog.findViewById(R.id.tvLocation);
        tvDistance = (TextView) dataDialog.findViewById(R.id.tvDistance);
        tvTotalAmount = (TextView) dataDialog.findViewById(R.id.tvTotalPrice);
        tvWait = (TextView) dataDialog.findViewById(R.id.tvWait);
        tvConfirm = (TextView) dataDialog.findViewById(R.id.tvConfirm);
        llReject = (LinearLayout) dataDialog.findViewById(R.id.llReject);
        llWait = (LinearLayout) dataDialog.findViewById(R.id.llWait);
        llConfirm = (LinearLayout) dataDialog.findViewById(R.id.llConfirm);
        llCallCustomer = (LinearLayout) dataDialog.findViewById(R.id.llCallCustomer);
        llMain = (LinearLayout) dataDialog.findViewById(R.id.llMain);
        tlAddable = (TableLayout) dataDialog.findViewById(R.id.tlAddable);
        ratingBar = (RatingBar) dataDialog.findViewById(R.id.ratingBar);

        llCallCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
                } else {
                    if (jobDetails.getCustomerPhoneNumber() != null && !jobDetails.getCustomerPhoneNumber().toString().trim().equals("")) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + jobDetails.getCustomerPhoneNumber().toString().trim()));
                        startActivity(intent);
                    }
                }
            }
        });

        llReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoCancelService = false;
                volleyGetReasonsRequest(Constants.typeId.carWashType);
            }
        });

        llWait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoCancelService = false;
                GlobalVariable.pushNotificationServiceId = "";
                volleyWaitRequest(pref.getStringPreference(PushNotificationActivity.this, Preferences.PHONE));
            }
        });

        llConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoCancelService = false;
                volleyConfirmRequest(pref.getStringPreference(PushNotificationActivity.this, Preferences.PHONE));
            }
        });

        setData();

        dataDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (jobDetails.getCustomerPhoneNumber() != null && !jobDetails.getCustomerPhoneNumber().toString().trim().equals("")) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + jobDetails.getCustomerPhoneNumber().toString().trim()));
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    void setData() {
        tvHeading.setText(getResources().getString(R.string.new_job_received) + "!");
        tvName.setText(jobDetails.getCustomerName());

        String rating = "", reviewCount = "";
        rating = jobDetails.getCustomerRating();
        reviewCount = jobDetails.getCustomerReviewsCount();

        if (rating != null && !rating.toString().trim().equals("") && !rating.toString().trim().equals("null")) {
            float ratingFloat = Float.parseFloat(rating);
            if (Float.compare(ratingFloat, 0) > 0) {
                ratingBar.setRating(ratingFloat);
                tvRatingCount.setText(String.valueOf(ratingFloat));
            } else {
                ratingBar.setRating(0);
                tvRatingCount.setText("0");
            }
        } else {
            ratingBar.setRating(0);
            tvRatingCount.setText("0");
        }

        if (reviewCount != null && !reviewCount.toString().trim().equals("") && !reviewCount.toString().trim().equals("null")) {
            int reviewCountIn = Integer.parseInt(reviewCount);
            if (reviewCountIn <= 1) {
                tvTotalReviews.setText("(" + reviewCount + " " + getResources().getString(R.string.review) + ")");
            } else {
                tvTotalReviews.setText("(" + reviewCount + " " + getResources().getString(R.string.reviews).toLowerCase() + ")");
            }
        } else {
            tvTotalReviews.setText("(0 " + getResources().getString(R.string.review) + ")");
        }

        Date date1 = null, date2 = null;
        try {
            date1 = sdf.parse(jobDetails.getStartTime());
            date2 = sdf.parse(jobDetails.getEndTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);

        tvDate.setText(Html.fromHtml(new SimpleDateFormat("EEEE").format(calendar1.getTime()) + ", " + new SimpleDateFormat("dd").format(calendar1.getTime())
                + "<sup><small>" + getDateSuffix(calendar1.get(Calendar.DATE)) + " </small></sup>" + new SimpleDateFormat("MMM").format(calendar1.getTime())));

        tvArrivalTime.setText(new SimpleDateFormat("hh:mm a").format(calendar1.getTime()) + " - " + new SimpleDateFormat("hh:mm a").format(calendar2.getTime()));


        String address = jobDetails.getLocation();
        if (address != null && !address.toString().trim().equals("")) {
            String addressArr[] = address.split(", ");
            if (addressArr != null && addressArr.length > 0) {
                tvLocation.setText(addressArr[0]);
                tvDistance.setText("(" + jobDetails.getDistance() + " km)");
            }
        }

        int numOfCars = suvArrList.size() + sedanArrList.size();
        totalPrice = 0;

        if (!suvArrList.isEmpty()) {
            for (int i = 0; i < suvArrList.size(); i++) {
                int size = suvArrList.get(i).getSubHeadingDetailsArrayList().size();
                if (size == 3) {
                    LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View view = headingInflater.inflate(R.layout.inflate_job_confirmation_car_details, null);
                    TextView tvCarName = (TextView) view.findViewById(R.id.tvCarName);
                    TextView tvServiceName = (TextView) view.findViewById(R.id.tvServiceName);
                    TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);

                    tvCarName.setText(suvArrList.get(i).getWorkHeadingName());
                    tvServiceName.setText(getResources().getString(R.string.complete_wash_n_shine));
                    totalPrice = totalPrice + suvArrList.get(i).getWorkHeadingPrice();
                    tvPrice.setText("" + GlobalVariable.decimalFormat.format(suvArrList.get(i).getWorkHeadingPrice()));

                    tlAddable.addView(view);
                } else {
                    for (int j = 0; j < size; j++) {
                        WorkSubHeadingDetails swhd = new WorkSubHeadingDetails();
                        swhd = suvArrList.get(i).getSubHeadingDetailsArrayList().get(j);
                        LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View view = headingInflater.inflate(R.layout.inflate_job_confirmation_car_details, null);
                        TextView tvCarName = (TextView) view.findViewById(R.id.tvCarName);
                        TextView tvServiceName = (TextView) view.findViewById(R.id.tvServiceName);
                        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
                        if (j == 0) {
                            tvCarName.setVisibility(View.VISIBLE);
                            tvCarName.setText(suvArrList.get(i).getWorkHeadingName());
                        } else {
                            tvCarName.setVisibility(View.INVISIBLE);
                        }
                        tvServiceName.setText(swhd.getWorkSubHeadingName());
                        totalPrice = totalPrice + swhd.getWorkSubHeadingPrice();
                        tvPrice.setText("" + GlobalVariable.decimalFormat.format(swhd.getWorkSubHeadingPrice()));

                        tlAddable.addView(view);
                    }
                }
            }
        }

        if (!sedanArrList.isEmpty()) {
            for (int i = 0; i < sedanArrList.size(); i++) {
                int size = sedanArrList.get(i).getSubHeadingDetailsArrayList().size();
                if (size == 3) {
                    LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View view = headingInflater.inflate(R.layout.inflate_job_confirmation_car_details, null);
                    TextView tvCarName = (TextView) view.findViewById(R.id.tvCarName);
                    TextView tvServiceName = (TextView) view.findViewById(R.id.tvServiceName);
                    TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);

                    tvCarName.setText(sedanArrList.get(i).getWorkHeadingName());
                    tvServiceName.setText(getResources().getString(R.string.complete_wash_n_shine));
                    totalPrice = totalPrice + sedanArrList.get(i).getWorkHeadingPrice();
                    tvPrice.setText("" + GlobalVariable.decimalFormat.format(sedanArrList.get(i).getWorkHeadingPrice()));

                    tlAddable.addView(view);
                } else {
                    for (int j = 0; j < size; j++) {
                        WorkSubHeadingDetails swhd = new WorkSubHeadingDetails();
                        swhd = sedanArrList.get(i).getSubHeadingDetailsArrayList().get(j);
                        LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View view = headingInflater.inflate(R.layout.inflate_job_confirmation_car_details, null);
                        TextView tvCarName = (TextView) view.findViewById(R.id.tvCarName);
                        TextView tvServiceName = (TextView) view.findViewById(R.id.tvServiceName);
                        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
                        if (j == 0) {
                            tvCarName.setVisibility(View.VISIBLE);
                            tvCarName.setText(sedanArrList.get(i).getWorkHeadingName());
                        } else {
                            tvCarName.setVisibility(View.INVISIBLE);
                        }
                        tvServiceName.setText(swhd.getWorkSubHeadingName());
                        totalPrice = totalPrice + swhd.getWorkSubHeadingPrice();
                        tvPrice.setText("" + GlobalVariable.decimalFormat.format(swhd.getWorkSubHeadingPrice()));

                        tlAddable.addView(view);
                    }
                }
            }
        }

        //extras : deliver & setup fees
        LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = headingInflater.inflate(R.layout.inflate_job_confirmation_car_details, null);
        TextView tvCarName = (TextView) view.findViewById(R.id.tvCarName);
        TextView tvServiceName = (TextView) view.findViewById(R.id.tvServiceName);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);
        tvCarName.setText(getResources().getString(R.string.extras));
        tvServiceName.setText(getResources().getString(R.string.delivery_setup_fees));
        totalPrice = totalPrice + 18;
        tvPrice.setText(GlobalVariable.decimalFormat.format(18));
        tlAddable.addView(view);

        tvTotalAmount.setText("" + GlobalVariable.decimalFormat.format(totalPrice));
    }

    public String getDateSuffix(int day) {
        switch (day) {
            case 1:
            case 21:
            case 31:
                return ("st");

            case 2:
            case 22:
                return ("nd");

            case 3:
            case 23:
                return ("rd");

            default:
                return ("th");
        }
    }

    private void volleyGetReasonsRequest(final String typeId) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REASON_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            reasonArrList.clear();
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JSONArray serviceJsonArr = responseJsonObj.optJSONArray("service");
                                    for (int i = 0; i < serviceJsonArr.length(); i++) {
                                        JSONObject jsonObj = serviceJsonArr.optJSONObject(i);
                                        Reason reason = new Reason();
                                        reason.setId(jsonObj.optString("id"));
                                        reason.setName(jsonObj.optString("title"));
                                        String isActive = jsonObj.optString("is_active");
                                        if (isActive != null && !isActive.toString().trim().equals("")) {
                                            if (isActive.toString().trim().equals("1")) {
                                                reason.setActive(true);
                                            } else {
                                                reason.setActive(false);
                                            }
                                        } else {
                                            reason.setActive(false);
                                        }
                                        reasonArrList.add(reason);
                                    }
                                    showReasonDialog();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(PushNotificationActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type_id", typeId);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    void showReasonDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_reason, null);
        dialogBuilder.setView(dialog);

        final EditText etComment = (EditText) dialog.findViewById(R.id.etComment);
        final Spinner spinnerReason = (Spinner) dialog.findViewById(R.id.spinnerReason);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);

        ArrayList<String> tempArr = new ArrayList<>();
        for (int i = 0; i < reasonArrList.size(); i++) {
            tempArr.add(reasonArrList.get(i).getName());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tempArr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerReason.setAdapter(spinnerArrayAdapter);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etComment.getText() == null || etComment.getText().toString().trim().equals("")) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.comment_error));
                } else {
                    volleyRejectRequest(pref.getStringPreference(PushNotificationActivity.this, Preferences.PHONE), reasonArrList.get(spinnerReason.getSelectedItemPosition()).getId(),
                            etComment.getText().toString().trim());
                }
            }
        });

        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alertDialog != null)
                    alertDialog.dismiss();
            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void volleyWaitRequest(final String mobile) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_WAIT_APPOINTMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    tvWait.setVisibility(View.GONE);
                                    llWait.setEnabled(false);
                                    tvConfirm.setVisibility(View.VISIBLE);
                                    timer5minute();
                                    Toast.makeText(PushNotificationActivity.this, responseJsonObj.optString("msg"), Toast.LENGTH_SHORT).show();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(PushNotificationActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(PushNotificationActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    void timer5minute() {
        timer_5minute = new CountDownTimer(Constants.Timer.confirm_timer, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000) % 60;
                int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                tvConfirm.setText("" + minutes + ":" + seconds);
            }

            public void onFinish() {
                tvConfirm.setVisibility(View.GONE);
                llConfirm.setEnabled(false);
                onBackPressed();
                if (autoCancelService) {
                    autoCancelService = false;
                    onBackPressed();
                }
            }
        }.start();
    }

    private void volleyConfirmRequest(final String mobile) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_ACCEPT_APPOINTMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    if (timer_5minute != null) {
                                        timer_5minute.cancel();
                                    }
                                    Toast.makeText(PushNotificationActivity.this, responseJsonObj.optString("msg"), Toast.LENGTH_SHORT).show();
                                    setResult(Activity.RESULT_OK);
                                    onBackPressed();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(PushNotificationActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(PushNotificationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(PushNotificationActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyRejectRequest(final String mobile, final String reasonId, final String comment) {
        dialogView.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REJECT_APPOINTMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    if (timer_13seconds != null) {
                                        timer_13seconds.cancel();
                                    }
                                    if (timer_5minute != null) {
                                        timer_5minute.cancel();
                                    }
                                    if (alertDialog != null)
                                        alertDialog.dismiss();
                                    Toast.makeText(PushNotificationActivity.this, "Appointment is rejected successfully!", Toast.LENGTH_SHORT).show();
                                    setResult(Activity.RESULT_OK);
                                    onBackPressed();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(PushNotificationActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    Log.e(TAG, "success != 0");
                                    onBackPressed();
                                }
                            } else {
                                Log.e(TAG, "json null");
                                onBackPressed();
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "json exception");
                            onBackPressed();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, "volley error");
                dialogView.dismissCustomSpinProgress();
                onBackPressed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(PushNotificationActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("reason_id", reasonId);
                params.put("comment", comment);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancel(Integer.parseInt(serviceId));
        finishAll();
    }

    void finishAll() {
        if (timer_13seconds != null) {
            timer_13seconds.cancel();
        }
        if (timer_5minute != null) {
            timer_5minute.cancel();
        }
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        if (dataDialog != null) {
            dataDialog.dismiss();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancel(Integer.parseInt(serviceId));
        unregisterReceiver(pushNotificationTimerReceiver);
    }
}
