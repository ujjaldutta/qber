package com.uipl.qberprovider;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.uipl.qberprovider.adapter.CommonAdapter;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.RegistrationDetails;
import com.uipl.qberprovider.utils.CaptureImage;
import com.uipl.qberprovider.utils.CommonMethods;
import com.uipl.qberprovider.utils.CompressionImage;
import com.uipl.qberprovider.utils.CorrectifyOrrientation;
import com.uipl.qberprovider.utils.DataBaseHandeller;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.GPSTracker;
import com.uipl.qberprovider.utils.InputVadidation;
import com.uipl.qberprovider.utils.MultiSpinner;
import com.uipl.qberprovider.utils.MultipartRequest;
import com.uipl.qberprovider.utils.Permission;
import com.uipl.qberprovider.utils.Preferences;
import com.uipl.qberprovider.utils.SnackBarCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, MultiSpinner.MultiSpinnerListener {

    private ImageButton imgBtnBack;
    private EditText etName, etEmail, etMobile, etTellUsAns, etShopName, etPassword, etConfirmPassword;
    private Spinner spBirthYear, spNationality, spYouPrefer, spLiveInQatar, spYearExp;
    MultiSpinner spLanguages;
    private TextView tvMale, tvFemale, tvISDCode, tvProfilePic, tvProfileSelect, tvPhotoTools, tvPhotoToolsSelect, tvPhotoShop,
            tvPhotoShopSelect, tvAttachCV, tvAttachCVSelect, tvCarRegistration, tvCarRegistrationSelect, tvDrivingLicense,
            tvDrivingLicenseSelect, tvIDImage, tvIDImageSelect, tvShopName;
    ImageView ivOptionalInfo;

    private LinearLayout llOptionalInformation, llAdditionalView;
    private RadioButton rbMobileInternet, rbWifi, rbMobileWifiInternet, rbCarMotorcycle, rbTaxiFriendCar, rbWalkBicycle, RdBtnYes, RdBtnNo;
    boolean optionalInfoFlag = false;
    ArrayList<String> birthYear = new ArrayList<>();
    ArrayList<String> expYear = new ArrayList<>();
    ArrayList<HashMap<String, String>> nationalityArray = new ArrayList<>();
    ArrayList<HashMap<String, String>> preferLocation = new ArrayList<>();
    ArrayList<String> languageList = new ArrayList<>();
    boolean[] selectLanguageList;

    private Button btnBack, btnNext;
    private DialogView dialogview;
    private Preferences pref;
    private RequestQueue mQueue;
    private String TAG = "RegistrationActivity";
    private String typeId = "", gender = "male";
    /*capture image*/
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "QberCustomer";
    private static int LOAD_IMAGE_RESULTS_FROM_CAMERA = 100;
    private static int LOAD_IMAGE_RESULTS_FROM_GALLERY = 200;
    private int PICKFILE_RESULT_CODE = 300;
    private String ImagePath = "", picImageFor = "", language = "";
    private File fileProfile, fileTools, fileShop, fileCV, fileRegister, fileLicense, fileID;
    private RadioGroup rgIsCompany, rgInternet, rgTransport;
    boolean submitFlag = false;
    DataBaseHandeller dataBaseHandeller;
    private ScrollView svMain;
    private ImageView imgNationality, imgYouPrefer, imgLanguage;

    GPSTracker gpsTracker;
    final int CAMERA_STORAGE_PERMISSION = 1, REGISTRATION_PERMISSION = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        dataBaseHandeller = new DataBaseHandeller(this);
        if (getIntent().hasExtra("type_id")) {
            typeId = getIntent().getStringExtra("type_id");
        }
        dialogview = new DialogView(this);
        pref = new Preferences(RegistrationActivity.this);
        mQueue = Volley.newRequestQueue(this);
        birthYear = getBirthYear();
        expYear = getExpYear();
        languageArraySetUp();
        initView();
        volleyNationalityTask();
    }

    private void languageArraySetUp() {
        languageList.add("English");
        languageList.add("Qatari");
        languageList.add("Hindi");
        selectLanguageList = new boolean[languageList.size()];
        for (int i = 0; i < selectLanguageList.length; i++) {
            if (i == 0) {
                selectLanguageList[i] = true;
                language = languageList.get(i);
            } else {
                selectLanguageList[i] = false;
            }
        }
    }

    /**
     * Method Initialize variable
     */
    private void initView() {
        gpsTracker = new GPSTracker(this);
        svMain = (ScrollView) findViewById(R.id.svMain);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        imgBtnBack.setOnClickListener(this);
        etName = (EditText) findViewById(R.id.etName);
        spBirthYear = (Spinner) findViewById(R.id.spBirthYear);
        spBirthYear.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(RegistrationActivity.this, view);
                return false;
            }
        });
        spNationality = (Spinner) findViewById(R.id.spNationality);
        imgNationality = (ImageView) findViewById(R.id.imgNationality);
        imgNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spNationality.performClick();
            }
        });
        spNationality.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(RegistrationActivity.this, view);
                return false;
            }
        });
        etEmail = (EditText) findViewById(R.id.etEmail);
        tvMale = (TextView) findViewById(R.id.tvMale);
        tvMale.setOnClickListener(this);
        tvFemale = (TextView) findViewById(R.id.tvFemale);
        tvFemale.setOnClickListener(this);
        tvISDCode = (TextView) findViewById(R.id.tvISDCode);
        tvISDCode.setText("+" + GlobalVariable.ISD_CODE);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);

        rgInternet = (RadioGroup) findViewById(R.id.rgInternet);
        rbMobileInternet = (RadioButton) findViewById(R.id.rbMobileInternet);
        rbWifi = (RadioButton) findViewById(R.id.rbWifi);
        rbMobileWifiInternet = (RadioButton) findViewById(R.id.rbMobileWifiInternet);
        rbMobileInternet.setChecked(true);

        rgTransport = (RadioGroup) findViewById(R.id.rgTransport);
        rbCarMotorcycle = (RadioButton) findViewById(R.id.rbCarMotorcycle);
        rbTaxiFriendCar = (RadioButton) findViewById(R.id.rbTaxiFriendCar);
        rbWalkBicycle = (RadioButton) findViewById(R.id.rbWalkBicycle);
        rbCarMotorcycle.setChecked(true);

        // Optional Information
        ivOptionalInfo = (ImageView) findViewById(R.id.ivOptionalInfo);
        llOptionalInformation = (LinearLayout) findViewById(R.id.llOptionalInformation);
        llAdditionalView = (LinearLayout) findViewById(R.id.llAdditionalView);
        llAdditionalView.setVisibility(View.GONE);
        llOptionalInformation.setOnClickListener(this);
        etTellUsAns = (EditText) findViewById(R.id.etTellUsAns);
        spYearExp = (Spinner) findViewById(R.id.spYearExp);
        spYearExp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(RegistrationActivity.this, view);
                return false;
            }
        });
        spLiveInQatar = (Spinner) findViewById(R.id.spLiveInQatar);
        spLiveInQatar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(RegistrationActivity.this, view);
                return false;
            }
        });
        spYouPrefer = (Spinner) findViewById(R.id.spYouPrefer);
        imgYouPrefer = (ImageView) findViewById(R.id.imgYouPrefer);
        imgYouPrefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spYouPrefer.performClick();
            }
        });
        spYouPrefer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(RegistrationActivity.this, view);
                return false;
            }
        });
        spLanguages = (MultiSpinner) findViewById(R.id.spLanguages);
        imgLanguage = (ImageView) findViewById(R.id.imgLanguage);
        imgLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spLanguages.performClick();
            }
        });
        spLanguages.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(RegistrationActivity.this, view);
                return false;
            }
        });
        rgIsCompany = (RadioGroup) findViewById(R.id.rgIsCompany);
        rgIsCompany.setOnCheckedChangeListener(this);
        RdBtnYes = (RadioButton) findViewById(R.id.RdBtnYes);
        RdBtnNo = (RadioButton) findViewById(R.id.RdBtnNo);
        etShopName = (EditText) findViewById(R.id.etShopName);
        tvShopName = (TextView) findViewById(R.id.tvShopName);
        RdBtnNo.setChecked(true);

        tvProfilePic = (TextView) findViewById(R.id.tvProfilePic);
        tvProfileSelect = (TextView) findViewById(R.id.tvProfileSelect);
        tvProfileSelect.setOnClickListener(this);
        tvPhotoTools = (TextView) findViewById(R.id.tvPhotoTools);
        tvPhotoToolsSelect = (TextView) findViewById(R.id.tvPhotoToolsSelect);
        tvPhotoToolsSelect.setOnClickListener(this);
        tvPhotoShop = (TextView) findViewById(R.id.tvPhotoShop);
        tvPhotoShopSelect = (TextView) findViewById(R.id.tvPhotoShopSelect);
        tvPhotoShopSelect.setOnClickListener(this);
        tvAttachCV = (TextView) findViewById(R.id.tvAttachCV);
        tvAttachCVSelect = (TextView) findViewById(R.id.tvAttachCVSelect);
        tvAttachCVSelect.setOnClickListener(this);
        tvCarRegistration = (TextView) findViewById(R.id.tvCarRegistration);
        tvCarRegistrationSelect = (TextView) findViewById(R.id.tvCarRegistrationSelect);
        tvCarRegistrationSelect.setOnClickListener(this);
        tvDrivingLicense = (TextView) findViewById(R.id.tvDrivingLicense);
        tvDrivingLicenseSelect = (TextView) findViewById(R.id.tvDrivingLicenseSelect);
        tvDrivingLicenseSelect.setOnClickListener(this);
        tvIDImage = (TextView) findViewById(R.id.tvIDImage);
        tvIDImageSelect = (TextView) findViewById(R.id.tvIDImageSelect);
        tvIDImageSelect.setOnClickListener(this);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);


    }

    private void setUpRegData(RegistrationDetails registrationDetails) {
        if (registrationDetails.getName() != null && !registrationDetails.getName().equals(""))
            etName.setText(registrationDetails.getName());

        String yearPos = registrationDetails.getYearPosition();
        if (yearPos != null && !yearPos.equals("") && Integer.parseInt(yearPos) < birthYear.size()) {
            spBirthYear.setSelection(Integer.parseInt(yearPos));
        }
        String countryPos = registrationDetails.getCountryPosition();
        if (countryPos != null && !countryPos.equals("") && Integer.parseInt(countryPos) < nationalityArray.size()) {
            spNationality.setSelection(Integer.parseInt(countryPos));
        }
        if (registrationDetails.getEmail() != null && !registrationDetails.getEmail().equals(""))
            etEmail.setText(registrationDetails.getEmail());
        if (registrationDetails.getGender() != null && !registrationDetails.getGender().equals("")) {
            if (registrationDetails.getGender().equals("male")) {
                tvMale.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvMale.setTextColor(getResources().getColor(R.color.white));
                tvFemale.setBackgroundResource(R.color.transparent);
                tvFemale.setTextColor(getResources().getColor(R.color.black));
                gender = "male";
            } else if (registrationDetails.getGender().equals("female")) {
                tvFemale.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvFemale.setTextColor(getResources().getColor(R.color.white));
                tvMale.setBackgroundResource(R.color.transparent);
                tvMale.setTextColor(getResources().getColor(R.color.black));
                gender = "female";
            }
        }
        if (registrationDetails.getPhone() != null && !registrationDetails.getPhone().equals(""))
            etMobile.setText(registrationDetails.getPhone());

        String internetPos = registrationDetails.getInternetAccessPosition();
        if (internetPos != null && !internetPos.equals("")) {
            ((RadioButton) rgInternet.getChildAt(Integer.parseInt(internetPos))).setChecked(true);
        }
        String transportPos = registrationDetails.getTransportPosition();
        if (transportPos != null && !transportPos.equals("")) {
            ((RadioButton) rgTransport.getChildAt(Integer.parseInt(transportPos))).setChecked(true);
        }

        if (registrationDetails.getYourOffer() != null && !registrationDetails.getYourOffer().equals(""))
            etTellUsAns.setText(registrationDetails.getYourOffer());
        String yearExpPos = registrationDetails.getYearOfExperience();
        if (yearExpPos != null && !yearExpPos.equals("") && Integer.parseInt(yearExpPos) < expYear.size()) {
            spYearExp.setSelection(Integer.parseInt(yearExpPos));
        }

        String LiveInQatarPos = registrationDetails.getLiveInQatar();
        if (LiveInQatarPos != null && !LiveInQatarPos.equals("") && Integer.parseInt(LiveInQatarPos) < expYear.size()) {
            spLiveInQatar.setSelection(Integer.parseInt(LiveInQatarPos));
        }

        String workPrePos = registrationDetails.getWorkPreferencePosition();
        if (workPrePos != null && !workPrePos.equals("") && Integer.parseInt(workPrePos) < preferLocation.size()) {
            spYouPrefer.setSelection(Integer.parseInt(workPrePos));
        }
        String lanPos = registrationDetails.getLanguagePreferencePosition();
        if (lanPos != null && !lanPos.equals("")) {
            String[] str = lanPos.split(",");
            for (int i = 0; i < selectLanguageList.length; i++) {
                selectLanguageList[i] = false;
            }
            for (int i = 0; i < str.length; i++) {
                language = lanPos;
                for (int j = 0; j < languageList.size(); j++) {
                    if (str[i].toString().trim().equals(languageList.get(j).toString().trim())) {
                        selectLanguageList[j] = true;
                        break;
                    }
                }
            }
            spLanguages.setItems(languageList, selectLanguageList, this);
        }
        if (registrationDetails.getIsShop() != null && !registrationDetails.getIsShop().equals("")) {
            if (registrationDetails.getIsShop().equals("yes")) {
                RdBtnYes.setChecked(true);
                if (registrationDetails.getShopName() != null && !registrationDetails.getShopName().equals(""))
                    etShopName.setText(registrationDetails.getShopName());
            } else {
                RdBtnNo.setChecked(true);
            }
        }
        try {
            String adminPicPtah = registrationDetails.getAdminPicPath();
            if (adminPicPtah != null && !adminPicPtah.toString().trim().equals("")) {
                String imgName = adminPicPtah.substring(adminPicPtah.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvProfilePic.setText(imgName);
                }
                fileProfile = new File(adminPicPtah);
            }
        } catch (Exception e) {
            fileProfile = null;
        }

        try {
            String tools_use_image = registrationDetails.getToolsPicPath();
            if (tools_use_image != null && !tools_use_image.toString().trim().equals("") && !tools_use_image.toString().trim().equals("null")) {
                String imgName = tools_use_image.substring(tools_use_image.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvPhotoTools.setText(imgName);
                }
                fileTools = new File(tools_use_image);
            }
        } catch (Exception e) {
            fileTools = null;
        }

        try {
            String shop_image = registrationDetails.getShopPicPath();
            if (shop_image != null && !shop_image.toString().trim().equals("") && !shop_image.toString().trim().equals("null")) {
                String imgName = shop_image.substring(shop_image.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvPhotoShop.setText(imgName);
                }
                fileShop = new File(shop_image);
            }
        } catch (Exception e) {
            fileShop = null;
        }

        try {
            String cv_file = registrationDetails.getCVFilePath();
            if (cv_file != null && !cv_file.toString().trim().equals("") && !cv_file.toString().trim().equals("null")) {
                String imgName = cv_file.substring(cv_file.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvAttachCV.setText(imgName);
                }
                fileCV = new File(cv_file);
            }
        } catch (Exception e) {
            fileCV = null;
        }

        try {
            String car_registration_image = registrationDetails.getRegisterPicPath();
            if (car_registration_image != null && !car_registration_image.toString().trim().equals("") && !car_registration_image.toString().trim().equals("null")) {
                String imgName = car_registration_image.substring(car_registration_image.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvCarRegistration.setText(imgName);
                }
                fileRegister = new File(car_registration_image);
            }
        } catch (Exception e) {
            fileRegister = null;
        }

        try {
            String driving_license_image = registrationDetails.getDrivingLicensePath();
            if (driving_license_image != null && !driving_license_image.toString().trim().equals("") && !driving_license_image.toString().trim().equals("null")) {
                String imgName = driving_license_image.substring(driving_license_image.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvDrivingLicense.setText(imgName);
                }
                fileLicense = new File(driving_license_image);
            }
        } catch (Exception e) {
            fileLicense = null;
        }

        try {
            String identity_image = registrationDetails.getIdentityPicPath();
            if (identity_image != null && !identity_image.toString().trim().equals("") && !identity_image.toString().trim().equals("null")) {
                String imgName = identity_image.substring(identity_image.lastIndexOf("/") + 1);
                if (imgName != null) {
                    tvIDImage.setText(imgName);
                }
                fileID = new File(identity_image);
            }
        } catch (Exception e) {
            fileID = null;
        }


    }

    private ArrayList<String> getBirthYear() {
        ArrayList<String> yearList = new ArrayList<>();
        yearList.add("Select birth year");
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = year - 18; i > year + 18 - 100; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    private ArrayList<String> getExpYear() {
        ArrayList<String> yearList = new ArrayList<>();
        yearList.add("Less than 1 year");
        yearList.add("1-2");
        yearList.add("2-4");
        yearList.add("4-8");
        yearList.add("8-15");
        yearList.add("15-25");
        yearList.add(" 25+");
        return yearList;
    }

    public void spinnerSetUp() {
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.spinner_item, birthYear);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spBirthYear.setAdapter(arrayAdapter);

        CommonAdapter arrayAdapter2 = new CommonAdapter(this, nationalityArray);
        spNationality.setAdapter(arrayAdapter2);
        CommonAdapter arrayAdapter3 = new CommonAdapter(this, preferLocation);
        spYouPrefer.setAdapter(arrayAdapter3);
        spLanguages.setItems(languageList, selectLanguageList, this);

        ArrayAdapter arrayAdapter4 = new ArrayAdapter(this, R.layout.spinner_item, expYear);
        arrayAdapter4.setDropDownViewResource(R.layout.spinner_item);
        spYearExp.setAdapter(arrayAdapter4);
        spYearExp.setSelection(0);
        ArrayAdapter arrayAdapter5 = new ArrayAdapter(this, R.layout.spinner_item, expYear);
        arrayAdapter5.setDropDownViewResource(R.layout.spinner_item);
        spLiveInQatar.setAdapter(arrayAdapter4);
        spLiveInQatar.setSelection(0);

        RegistrationDetails registrationDetails = dataBaseHandeller.getRegData();
        if (registrationDetails != null) {
            setUpRegData(registrationDetails);
        }
    }

    class getFirebaseToken extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            GlobalVariable.FIREBASE_TOKEN = refreshedToken;
            return refreshedToken;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null && !s.toString().equals("")) {
                volleyRegistrationTask();
            } else {
                dialogview.dismissCustomSpinProgress();
            }
            super.onPostExecute(s);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBtnBack:
                onBackPressed();
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnNext:
                if (Validation()) {
                    submitFlag = true;
                    dataBaseHandeller.delete();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, REGISTRATION_PERMISSION);
                    } else {
                        gpsTracker = new GPSTracker(this);
                        double lat = gpsTracker.getLatitude();
                        double lng = gpsTracker.getLongitude();
                        if (!gpsTracker.isGPSEnabled) {
                            gpsTracker.showSettingsAlert();
                        } else if (Double.compare(lat, 0.0) == 0 && Double.compare(lng, 0.0) == 0) {
                            dialogview.showCustomSingleButtonDialog(this, getResources().getString(R.string.sorry), getResources().getString(R.string.location_permission_error));
                        } else {
                            if (Validation()) {
                                dialogview.showCustomSpinProgress(RegistrationActivity.this);
                                new getFirebaseToken().execute();
                            }
                        }
                    }
                }
                break;
            case R.id.llOptionalInformation:
                if (optionalInfoFlag) {
                    ivOptionalInfo.setImageResource(R.drawable.plus);
                    llAdditionalView.setVisibility(View.GONE);
                    optionalInfoFlag = false;
                } else {
                    ivOptionalInfo.setImageResource(R.drawable.minus);
                    llAdditionalView.setVisibility(View.VISIBLE);
                    optionalInfoFlag = true;
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            svMain.smoothScrollTo(0, findViewById(R.id.llOptionalInformation).getTop());

                        }
                    });
                }
                break;
            case R.id.tvMale:
                tvMale.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvMale.setTextColor(getResources().getColor(R.color.white));
                tvFemale.setBackgroundResource(R.color.transparent);
                tvFemale.setTextColor(getResources().getColor(R.color.black));
                gender = "male";
                break;
            case R.id.tvFemale:
                tvFemale.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvFemale.setTextColor(getResources().getColor(R.color.white));
                tvMale.setBackgroundResource(R.color.transparent);
                tvMale.setTextColor(getResources().getColor(R.color.black));
                gender = "female";
                break;
            case R.id.tvProfileSelect:
                picImageFor = "Profile";
                PermissionChecking();
                break;
            case R.id.tvPhotoToolsSelect:
                picImageFor = "Tools";
                PermissionChecking();
                break;
            case R.id.tvPhotoShopSelect:
                picImageFor = "Shop";
                PermissionChecking();
                break;
            case R.id.tvAttachCVSelect:
                picImageFor = "CV";
                OpenFile();
                break;
            case R.id.tvCarRegistrationSelect:
                picImageFor = "Register";
                PermissionChecking();
                break;
            case R.id.tvDrivingLicenseSelect:
                picImageFor = "License";
                OpenFile();
                break;
            case R.id.tvIDImageSelect:
                picImageFor = "ID";
                OpenFile();
                break;
        }
    }

    public void OpenFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*|application/pdf/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Choose a Files"),
                    PICKFILE_RESULT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            dialogview.showToast(RegistrationActivity.this, "Please install a Files Manager.");
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == R.id.RdBtnYes) {
            tvShopName.setVisibility(View.VISIBLE);
            etShopName.setVisibility(View.VISIBLE);
        } else if (i == R.id.RdBtnNo) {
            tvShopName.setVisibility(View.GONE);
            etShopName.setVisibility(View.GONE);

        }
    }

    /**
     * Method to call web services for country list
     */
    private void volleyNationalityTask() {
        dialogview.showCustomSpinProgress(RegistrationActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_COUNTRY_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    HashMap<String, String> hashMap1 = new HashMap<>();
                                    hashMap1.put("name", getString(R.string.select_nationality));
                                    hashMap1.put("code", "");
                                    nationalityArray.add(hashMap1);
                                    JSONArray jsonArray = object.optJSONArray("provider");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.optJSONObject(i);
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("name", obj.optString("countryname"));
                                        hashMap.put("code", obj.optString("code"));
                                        nationalityArray.add(hashMap);
                                    }
                                    volleyPreferLocationTask();
                                } else {
                                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.dismissCustomSpinProgress();
                            // TODO Auto-generated catch block
                            dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * Method to call web services for Prefer location
     */
    private void volleyPreferLocationTask() {
        //dialogview.showCustomSpinProgress(RegistrationActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_PREFERENCE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray = object.optJSONArray("locations");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.optJSONObject(i);
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("name", obj.optString("name"));
                                        hashMap.put("id", obj.optString("id"));
                                        preferLocation.add(hashMap);
                                    }
                                    spinnerSetUp();
                                } else {
                                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * Method to call web services for Registration
     */
    private void volleyRegistrationTask() {
        dialogview.showCustomSpinProgress(RegistrationActivity.this);
        String internetAccess = getInternetAccess();
        String transport = getTransport();
        String isShop = getShopflag();
        HashMap<String, File> fileParams = new HashMap<>();
        if (fileProfile != null) {
            fileParams.put("admin_profile_image", fileProfile);
        }
        if (fileTools != null) {
            fileParams.put("tools_use_image", fileTools);
        }
        if (fileShop != null) {
            fileParams.put("shop_image", fileShop);
        }
        if (fileCV != null) {
            fileParams.put("cv_file", fileCV);
        }
        if (fileRegister != null) {
            fileParams.put("car_registration_image", fileRegister);
        }
        if (fileLicense != null) {
            fileParams.put("driving_license_image", fileLicense);
        }
        if (fileID != null) {
            fileParams.put("identity_image", fileID);
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("device_id", GlobalVariable.FIREBASE_TOKEN);
        params.put("phone", GlobalVariable.ISD_CODE + etMobile.getText().toString());
        params.put("password", etPassword.getText().toString());
        params.put("latitude", String.valueOf(gpsTracker.getLatitude()));
        params.put("longitude", String.valueOf(gpsTracker.getLongitude()));
        params.put("name", etName.getText().toString());
        params.put("email", etEmail.getText().toString());
        params.put("type_id", typeId);
        int pos = spNationality.getSelectedItemPosition();
        if (pos > -1 && pos < nationalityArray.size()) {
            params.put("nationality", nationalityArray.get(pos).get("code"));
        }
        params.put("date_of_birth", spBirthYear.getSelectedItem().toString());
        params.put("gender", gender);
        params.put("internet_access", internetAccess);
        params.put("transport", transport);
        params.put("your_offer", etTellUsAns.getText().toString());
        params.put("year_of_experience", spYearExp.getSelectedItem().toString());
        params.put("live_in_qatar", spLiveInQatar.getSelectedItem().toString());
        int pos2 = spYouPrefer.getSelectedItemPosition();
        if (pos2 > -1 && pos < preferLocation.size()) {
            params.put("work_preference", preferLocation.get(pos2).get("id"));
        }
        params.put("language_preference", language);
        params.put("work_in_company", isShop);
        if (isShop.equals("yes")) {
            params.put("shop_name", etShopName.getText().toString());
        }

        MultipartRequest request = new MultipartRequest(ConFig_URL.URL_SIGNUP, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialogview.dismissCustomSpinProgress();
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("success").equals("0")) {
                        JSONObject obj = object.optJSONObject("data");
                        startActivity(new Intent(RegistrationActivity.this, MobileVerificationActivity.class).putExtra("PHONE", etMobile.getText().toString().trim()));
                        finish();
                    } else {
                        dialogview.showCustomSingleButtonDialog(RegistrationActivity.this,
                                getResources().getString(R.string.sorry), object.optString("msg"));
                    }

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                }
            }
        }, fileParams, params);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        Volley.newRequestQueue(RegistrationActivity.this).add(request);
    }

    private String getShopflag() {
        String str = "";
        int radioButtonID = rgIsCompany.getCheckedRadioButtonId();
        View radioButton = rgIsCompany.findViewById(radioButtonID);
        int idx = rgIsCompany.indexOfChild(radioButton);
        if (idx == 0) {
            str = "yes";
        } else if (idx == 1) {
            str = "no";
        }

        return str;
    }

    private String getTransport() {
        String str = "";
        int radioButtonID = rgTransport.getCheckedRadioButtonId();
        View radioButton = rgTransport.findViewById(radioButtonID);
        int idx = rgTransport.indexOfChild(radioButton);
        if (idx == 0) {
            str = "car";
        } else if (idx == 1) {
            str = "taxi";
        } else if (idx == 2) {
            str = "walk";

        }

        return str;
    }

    private String getInternetAccess() {
        String str = "";
        int radioButtonID = rgInternet.getCheckedRadioButtonId();
        View radioButton = rgInternet.findViewById(radioButtonID);
        int idx = rgInternet.indexOfChild(radioButton);
        if (idx == 0) {
            str = "mobile";
        } else if (idx == 1) {
            str = "wifi";
        } else if (idx == 2) {
            str = "mobile+wifi";

        }

        return str;
    }

    /**
     * @return true if all the data are valid
     * Method to check validation
     */
    public boolean Validation() {
        if (etName.getText().toString().trim().isEmpty()) {
            SnackBarCustom.showSnackWithActionButton(etName, getResources().getString(R.string.please_enter_name));
            etName.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etName, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (spBirthYear.getSelectedItem().toString().equals(getString(R.string.select_birth_year))) {
            SnackBarCustom.showSnackWithActionButton(spBirthYear, getResources().getString(R.string.select_birth_year));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(spBirthYear, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (nationalityArray.get(spNationality.getSelectedItemPosition()).get("name").equals(getString(R.string.select_nationality))) {
            SnackBarCustom.showSnackWithActionButton(spNationality, getResources().getString(R.string.select_nationality));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(spNationality, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (!etEmail.getText().toString().trim().isEmpty() && !InputVadidation.isEditTextContainEmail(etEmail)) {
            etEmail.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etEmail, getResources().getString(R.string.please_enter_valid_email));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etMobile.getText().toString().trim().isEmpty()) {
            etMobile.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etMobile, getResources().getString(R.string.please_enter_mobile_number));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etPassword, getResources().getString(R.string.please_enter_password));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etPassword.getText().toString().length() < 8) {
            etPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etPassword, getResources().getString(R.string.password_length_should_be_minimum_8_characters));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etConfirmPassword.getText().toString().trim().isEmpty()) {
            etConfirmPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etConfirmPassword, getResources().getString(R.string.please_enter_confirm_password));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etConfirmPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (!InputVadidation.isPasswordMatches(etPassword, etConfirmPassword)) {
            etConfirmPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etConfirmPassword, getResources().getString(R.string.password_mismatch));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etConfirmPassword, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }

        return true;
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(RegistrationActivity.this, android.Manifest.permission.CAMERA)) {
            if (CaptureImage.isDeviceSupportCamera(RegistrationActivity.this)) {
                captureImage();
            } else {
                dialogview.showToast(RegistrationActivity.this, "Camera not supported");
            }
        } else {
            ActivityCompat.requestPermissions(RegistrationActivity.this,
                    new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_STORAGE_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (CaptureImage.isDeviceSupportCamera(RegistrationActivity.this)) {
                        captureImage();
                    } else {
                        dialogview.showToast(RegistrationActivity.this, "Camera not supported");
                    }
                } else {
                    dialogview.showToast(RegistrationActivity.this, getResources().getString(R.string.app_permission));
                }
            case REGISTRATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsTracker = new GPSTracker(this);
                    double lat = gpsTracker.getLatitude();
                    double lng = gpsTracker.getLongitude();
                    if (!gpsTracker.isGPSEnabled) {
                        gpsTracker.showSettingsAlert();
                    } else if (Double.compare(lat, 0.0) == 0 && Double.compare(lng, 0.0) == 0) {
                        dialogview.showCustomSingleButtonDialog(this, getResources().getString(R.string.sorry), getResources().getString(R.string.location_permission_error));
                    } else {
                        if (Validation()) {
                            dialogview.showCustomSpinProgress(RegistrationActivity.this);
                            new getFirebaseToken().execute();
                        }
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void captureImage() {


        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle(getResources().getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    fileUri = CaptureImage.getOutputMediaFileUri(RegistrationActivity.this,
                            MEDIA_TYPE_IMAGE, IMAGE_DIRECTORY_NAME, MEDIA_TYPE_IMAGE);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, LOAD_IMAGE_RESULTS_FROM_CAMERA);


                } else if (items[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, getResources().getString(R.string.select_file)),
                            LOAD_IMAGE_RESULTS_FROM_GALLERY);

                }
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == LOAD_IMAGE_RESULTS_FROM_CAMERA
                && resultCode == Activity.RESULT_OK) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Bitmap bmp = BitmapFactory.decodeFile(
                        CaptureImage.getPath(RegistrationActivity.this, fileUri),
                        options);
                ImagePath = CaptureImage.getPath(RegistrationActivity.this,
                        fileUri);
                File file = new File(ImagePath);
                int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
                CompressionImage imageCompression = new CompressionImage(RegistrationActivity.this, ImagePath, picImageFor);
                imageCompression.CompressSize();
                Bitmap bitmap = CorrectifyOrrientation.decodeFile(ImagePath);
            } else {
                Bitmap bmp = BitmapFactory.decodeFile(
                        fileUri.getPath(), options);

                ImagePath = CaptureImage.getPath(RegistrationActivity.this,
                        fileUri);
                File file = new File(ImagePath);
                int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
                CompressionImage imageCompression = new CompressionImage(RegistrationActivity.this, ImagePath, picImageFor);
                imageCompression.CompressSize();
                Bitmap bitmap = CorrectifyOrrientation.decodeFile(ImagePath);
            }
        } else if (requestCode == LOAD_IMAGE_RESULTS_FROM_GALLERY && resultCode == Activity.RESULT_OK && data != null) {
            //iv_pdf = (TouchImageView) findViewById(R.id.iv_pdf);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Uri pickedImage = data.getData();
            ImagePath = CaptureImage.getPath(RegistrationActivity.this,
                    pickedImage);
            File file = new File(ImagePath);
            int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
            CompressionImage imageCompression = new CompressionImage(RegistrationActivity.this, ImagePath, picImageFor);
            imageCompression.CompressSize();
            Bitmap bitmap = CorrectifyOrrientation.decodeFile(ImagePath);
        } else if (requestCode == PICKFILE_RESULT_CODE
                && resultCode == Activity.RESULT_OK && data != null) {

            Uri pickedImage = data.getData();
            ImagePath = CaptureImage.getPath(RegistrationActivity.this,
                    pickedImage);
            setImagePath(ImagePath, picImageFor);
        }
    }

    public void setImagePath(String imagePath, String picImageFor) {
        if (picImageFor.equals("Profile") && imagePath.length() > 0) {
            fileProfile = new File(imagePath);
            String fileName = fileProfile.getName();
            if (fileName != null)
                tvProfilePic.setText(fileName);
        } else if (picImageFor.equals("Tools") && imagePath.length() > 0) {
            fileTools = new File(imagePath);
            String fileName = fileTools.getName();
            if (fileName != null)
                tvPhotoTools.setText(fileName);
        } else if (picImageFor.equals("Shop") && imagePath.length() > 0) {
            fileShop = new File(imagePath);
            String fileName = fileShop.getName();
            if (fileName != null)
                tvPhotoShop.setText(fileName);
        } else if (picImageFor.equals("CV") && imagePath.length() > 0) {
            fileCV = new File(imagePath);
            String fileName = fileCV.getName();
            if (fileName != null)
                tvAttachCV.setText(fileName);
        } else if (picImageFor.equals("Register") && imagePath.length() > 0) {
            fileRegister = new File(imagePath);
            String fileName = fileRegister.getName();
            if (fileName != null)
                tvCarRegistration.setText(fileName);
        } else if (picImageFor.equals("License") && imagePath.length() > 0) {
            fileLicense = new File(imagePath);
            String fileName = fileLicense.getName();
            if (fileName != null)
                tvDrivingLicense.setText(fileName);
        } else if (picImageFor.equals("ID") && imagePath.length() > 0) {
            fileID = new File(imagePath);
            String fileName = fileID.getName();
            if (fileName != null)
                tvIDImage.setText(fileName);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!submitFlag) {
            dataBaseHandeller.delete();
            RegistrationDetails registrationDetails = new RegistrationDetails();
            registrationDetails.setName(etName.getText().toString().trim());
            registrationDetails.setYearPosition(String.valueOf(spBirthYear.getSelectedItemPosition()));
            registrationDetails.setCountryPosition(String.valueOf(spNationality.getSelectedItemPosition()));
            registrationDetails.setEmail(etEmail.getText().toString().trim());
            registrationDetails.setGender(gender);
            registrationDetails.setPhone(etMobile.getText().toString().trim());

            int radioButtonID = rgInternet.getCheckedRadioButtonId();
            View radioButton = rgInternet.findViewById(radioButtonID);
            int internet_idx = rgInternet.indexOfChild(radioButton);
            registrationDetails.setInternetAccessPosition(String.valueOf(internet_idx));

            int radioButtonID2 = rgTransport.getCheckedRadioButtonId();
            View radioButton2 = rgTransport.findViewById(radioButtonID2);
            int transport_idx = rgTransport.indexOfChild(radioButton2);
            registrationDetails.setTransportPosition(String.valueOf(transport_idx));

            registrationDetails.setYourOffer(etTellUsAns.getText().toString().trim());
            registrationDetails.setYearOfExperience(String.valueOf(spYearExp.getSelectedItemPosition()));
            registrationDetails.setLiveInQatar(String.valueOf(spLiveInQatar.getSelectedItemPosition()));
            registrationDetails.setWorkPreferencePosition(String.valueOf(spYouPrefer.getSelectedItemPosition()));
            registrationDetails.setLanguagePreferencePosition(language);
            String isShop = getShopflag();
            registrationDetails.setIsShop(isShop);
            if (isShop.equals("yes")) {
                registrationDetails.setShopName(etShopName.getText().toString().trim());
            } else {
                registrationDetails.setShopName("");
            }
            if (fileProfile != null) {
                registrationDetails.setAdminPicPath(fileProfile.getAbsolutePath().toString());
            } else {
                registrationDetails.setAdminPicPath("");
            }
            if (fileTools != null) {
                registrationDetails.setToolsPicPath(fileTools.getAbsolutePath().toString());
            } else {
                registrationDetails.setToolsPicPath("");
            }
            if (fileShop != null) {
                registrationDetails.setShopPicPath(fileShop.getAbsolutePath().toString());
            } else {
                registrationDetails.setShopPicPath("");
            }
            if (fileCV != null) {
                registrationDetails.setCVFilePath(fileCV.getAbsolutePath().toString());
            } else {
                registrationDetails.setCVFilePath("");
            }
            if (fileRegister != null) {
                registrationDetails.setRegisterPicPath(fileRegister.getAbsolutePath().toString());
            } else {
                registrationDetails.setRegisterPicPath("");
            }
            if (fileLicense != null) {
                registrationDetails.setDrivingLicensePath(fileLicense.getAbsolutePath().toString());
            } else {
                registrationDetails.setDrivingLicensePath("");
            }
            if (fileID != null) {
                registrationDetails.setIdentityPicPath(fileID.getAbsolutePath().toString());
            } else {
                registrationDetails.setIdentityPicPath("");
            }

            dataBaseHandeller.saveRegData(registrationDetails);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(RegistrationActivity.this, RegisterActivity.class));
        finish();
    }

    @Override
    public void onItemsSelected(boolean[] selected, String allText) {
        language = allText;
    }
}
