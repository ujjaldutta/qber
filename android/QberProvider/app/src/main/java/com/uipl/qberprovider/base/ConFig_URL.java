package com.uipl.qberprovider.base;

/**
 * Created by abhijit on 19/9/16.
 */
public class ConFig_URL {

    public static final String BASEURL = "http://uiplonline.com/qber-app/master/api/provider/";
    public static final String SERVICE_BASE_URL = "http://uiplonline.com/qber-app/master/api/service/";
    public static final String IMAGE_URL = "http://uiplonline.com/qber-app/master/";
//    public static final String BASEURL = "http://demo.uiplonline.com/qber-app/dev/api/provider/";
//    public static final String SERVICE_BASE_URL = "http://demo.uiplonline.com/qber-app/dev/api/service/";
//    public static final String IMAGE_URL = "http://demo.uiplonline.com/qber-app/dev/public/";

    public static final String URL_LOGIN = BASEURL + "login";
    public static final String URL_LOGOUT = BASEURL + "logout";
    public static final String URL_SIGNUP = BASEURL + "signup";
    public static final String URL_SMS_VERIFY = BASEURL + "verify-sms";
    public static final String URL_RESEND_VERIFY_CODE = BASEURL + "resend-verify-sms";
    public static final String URL_FORGOT_PASSWORD = BASEURL + "forgot-password";
    public static final String URL_GET_PROFILE = BASEURL + "get-provider-profile";
    public static final String URL_PROVIDER_PREFERENCE = BASEURL + "get-provider-preference";
    public static final String URL_UPDATE_PROFILE = BASEURL + "set-provider-profile";
    public static final String URL_GET_PREFERENCE = BASEURL + "get-preference";
    public static final String URL_GET_SETTING = BASEURL + "get-settings";
    public static final String URL_UPDATE_SETTING = BASEURL + "update-settings";
    public static final String URL_EMAIL_VERIFIED = BASEURL + "verify-email";
    public static final String URL_SAVE_PASSWORD = BASEURL + "savepassword";
    public static final String URL_SET_SCHEDULE = BASEURL + "schedule/set-schedule";
    public static final String URL_GET_SCHEDULE = BASEURL + "schedule/get-schedule";
    public static final String URL_JOBS_LISTING = BASEURL + "list-appointment";
    public static final String URL_JOB_DETAILS = BASEURL + "appointment-details";
    public static final String URL_UPDATE_DEVICE = BASEURL + "update-device";
    public static final String URL_RATE_CUSTOMER = BASEURL + "send-rating";
    public static final String URL_ACCEPT_APPOINTMENT = BASEURL + "accept-appointment";
    public static final String URL_WAIT_APPOINTMENT = BASEURL + "wait-appointment";
    public static final String URL_REJECT_APPOINTMENT = BASEURL + "cancel-appointment";
    public static final String URL_UPDATE_CAR_DETAILS = BASEURL + "update-cardetails";
    public static final String URL_REQUEST_BALANCE = BASEURL + "request-balance";
    public static final String URL_GET_BALANCE = BASEURL + "get-balance";
    public static final String URL_CANCEL_BALANCE_SETTING = BASEURL + "cancel-balance-sattle";
    public static final String URL_UPDATE_DURATION = BASEURL + "update-duration";
    public static final String URL_UPDATE_END_TIME = BASEURL + "update-endtime";
    public static final String URL_UPDATE_PRICE = BASEURL + "update-price";
    public static final String URL_UPDATE_LOCATION = BASEURL + "live-track";
    public static final String URL_SCHEDULE = BASEURL + "schedule/get-today-schedule";
    public static final String URL_SCHEDULE_UPDATE = BASEURL + "update-duration";

    public static final String URL_SERVICE_TYPE = SERVICE_BASE_URL + "type";
    public static final String URL_SERVICE_COUNTRY_LIST = SERVICE_BASE_URL + "country-list";
    public static final String URL_REASON_LIST = SERVICE_BASE_URL + "cancel-reason";

}
