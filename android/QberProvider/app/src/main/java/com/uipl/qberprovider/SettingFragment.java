package com.uipl.qberprovider;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.ui.IconGenerator;
import com.uipl.qberprovider.adapter.JobLocationAdapter;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.ServiceSetting;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Permission;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {


    public static String TAG = "SettingFragment";
    private TextView tvJobLocation, tvTransport;
    private GridView gvJobLocation;
    private GoogleMap mMap;
    JobLocationAdapter jobLocationAdapter;
    ArrayList<ServiceSetting> jobLocationArray = new ArrayList<>();
    private LinearLayout llJobLocation, llTransport;
    private DialogView dialogview;
    private RequestQueue mQueue;
    String transport = "";
    private LinearLayout llCar, llWalking;
    private TextView tvCar, tvWalking;
    private ImageView imgSelectCar, imgCarService, imgSelectWalk, imgWalkService;
    private Preferences pref;
    float zoomValue = 15.0f;
    //private Button tvSave;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        initView(view);
        initToolbar(view);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


        FragmentManager fragmentManager;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            fragmentManager = getFragmentManager();
        } else {
            Log.d(TAG, "using getChildFragmentManager");
            fragmentManager = getChildFragmentManager();
        }
        MapFragment mapFragment = (MapFragment) fragmentManager
                .findFragmentById(R.id.map_2);
        mapFragment.getMapAsync(this);
        return view;
    }

    private void initView(View view) {
        pref = new Preferences(getActivity());
        dialogview = new DialogView(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());
        llJobLocation = (LinearLayout) view.findViewById(R.id.llJobLocation);
        llTransport = (LinearLayout) view.findViewById(R.id.llTransport);
        tvJobLocation = (TextView) view.findViewById(R.id.tvJobLocation);
        tvTransport = (TextView) view.findViewById(R.id.tvTransport);
        tvJobLocation.setOnClickListener(this);
        tvTransport.setOnClickListener(this);

        gvJobLocation = (GridView) view.findViewById(R.id.gvJobLocation);
        jobLocationAdapter = new JobLocationAdapter(getActivity(), jobLocationArray);
        gvJobLocation.setAdapter(jobLocationAdapter);

        /*Transport*/
        llCar = (LinearLayout) view.findViewById(R.id.llCar);
        llCar.setOnClickListener(this);
        tvCar = (TextView) view.findViewById(R.id.tvCar);
        imgSelectCar = (ImageView) view.findViewById(R.id.imgSelectCar);
        imgCarService = (ImageView) view.findViewById(R.id.imgCarService);
        llWalking = (LinearLayout) view.findViewById(R.id.llWalking);
        llWalking.setOnClickListener(this);
        tvWalking = (TextView) view.findViewById(R.id.tvWalking);
        imgSelectWalk = (ImageView) view.findViewById(R.id.imgSelectWalk);
        imgWalkService = (ImageView) view.findViewById(R.id.imgWalkService);

        volleyGetSettingTask();


    }

    /**
     * @param view Method to toolbar setup
     */
    void initToolbar(View view) {
        MenuActivity.tvToolbarTitle.setVisibility(View.VISIBLE);
        MenuActivity.ibSave.setImageResource(R.drawable.pop_up_1);
        MenuActivity.ibSave.setVisibility(View.VISIBLE);
        MenuActivity.ibSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selectedJob = getSelectedJob();
                if (!selectedJob.equals("")) {
                    if (!transport.equals("")) {
                        volleyUpDateSettingTask(selectedJob);
                    } else {
                        dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_your_transportation));
                    }
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_atleast_one_service_location));
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        PermissionChecking();
        // Add a marker in Sydney and move the camera
        mMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(Polygon polygon) {

                for (int i = 0; i < jobLocationArray.size(); i++) {

                    if (polygon.equals(jobLocationArray.get(i).getPolygon())) {
                        if (jobLocationArray.get(i).isSelected()) {
                            polygon.setStrokeColor(getResources().getColor(R.color.light_green));
                            polygon.setFillColor(getResources().getColor(R.color.light_green_transparent));
                            jobLocationArray.get(i).setSelected(false);
                        } else {
                            polygon.setStrokeColor(getResources().getColor(R.color.black));
                            polygon.setFillColor(getResources().getColor(R.color.grey_transparent));
                            jobLocationArray.get(i).setSelected(true);
                        }
                        jobLocationAdapter.notifyDataSetChanged();
                        break;

                    }
                }

            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                int pos = Integer.parseInt(marker.getSnippet());
                if (jobLocationArray.get(pos).isSelected()) {
                    jobLocationArray.get(pos).getPolygon().setStrokeColor(getResources().getColor(R.color.light_green));
                    jobLocationArray.get(pos).getPolygon().setFillColor(getResources().getColor(R.color.light_green_transparent));
                    jobLocationArray.get(pos).setSelected(false);
                } else {
                    jobLocationArray.get(pos).getPolygon().setStrokeColor(getResources().getColor(R.color.black));
                    jobLocationArray.get(pos).getPolygon().setFillColor(getResources().getColor(R.color.grey_transparent));
                    jobLocationArray.get(pos).setSelected(true);
                }
                jobLocationAdapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    dialogview.showToast(getActivity(), getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvJobLocation:
                tvJobLocation.setTextColor(getResources().getColor(R.color.white));
                tvJobLocation.setBackgroundResource(R.drawable.rounded_purple_bg);

                tvTransport.setTextColor(getResources().getColor(R.color.black));
                tvTransport.setBackgroundResource(R.color.transparent);

                llJobLocation.setVisibility(View.VISIBLE);
                llTransport.setVisibility(View.GONE);
                break;
            case R.id.tvTransport:
                tvTransport.setTextColor(getResources().getColor(R.color.white));
                tvTransport.setBackgroundResource(R.drawable.rounded_purple_bg);

                tvJobLocation.setTextColor(getResources().getColor(R.color.black));
                tvJobLocation.setBackgroundResource(R.color.transparent);

                llTransport.setVisibility(View.VISIBLE);
                llJobLocation.setVisibility(View.GONE);
                break;
            case R.id.llCar:
                transport = "car";
                tvCar.setTextColor(getResources().getColor(R.color.white));
                tvWalking.setTextColor(getResources().getColor(R.color.black));
                llCar.setBackgroundResource(R.drawable.rounded_purple_bg);
                llWalking.setBackgroundResource(R.drawable.rounded_white_bg_with_border);
                imgCarService.setImageResource(R.drawable.car_hv);
                imgWalkService.setImageResource(R.drawable.walking);
                imgSelectCar.setVisibility(View.VISIBLE);
                imgSelectWalk.setVisibility(View.INVISIBLE);
                break;
            case R.id.llWalking:
                transport = "walk";
                tvWalking.setTextColor(getResources().getColor(R.color.white));
                tvCar.setTextColor(getResources().getColor(R.color.black));
                llWalking.setBackgroundResource(R.drawable.rounded_purple_bg);
                llCar.setBackgroundResource(R.drawable.rounded_white_bg_with_border);
                imgCarService.setImageResource(R.drawable.car);
                imgWalkService.setImageResource(R.drawable.walking_hv);
                imgSelectWalk.setVisibility(View.VISIBLE);
                imgSelectCar.setVisibility(View.INVISIBLE);
                break;
            case R.id.tvSave:
                String selectedJob = getSelectedJob();
                if (!selectedJob.equals("")) {
                    if (!transport.equals("")) {
                        volleyUpDateSettingTask(selectedJob);
                    } else {
                        dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_your_transportation));
                    }
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.please_select_atleast_one_service_location));
                }
                break;

        }
    }


    private void volleyGetSettingTask() {
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_SETTING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject provideObj = object.optJSONObject("provider");
                                    transport = provideObj.optString("transport");
                                    JSONArray dataObj = object.optJSONArray("preference");
                                    if (dataObj != null) {

                                        LatLng startLatLng = null, endLatLng = null;
                                        if (mMap != null) {
                                            mMap.clear();


                                            for (int j = 0; j < dataObj.length(); j++) {
                                                JSONObject preferenceObj = dataObj.optJSONObject(j);
                                                ServiceSetting serviceSetting = new ServiceSetting();
                                                serviceSetting.setPreferenceId(preferenceObj.optString("id"));
                                                serviceSetting.setPreferenceName(preferenceObj.optString("name"));
                                                serviceSetting.setSelected(preferenceObj.optBoolean("selected"));
                                            /*............polygon......................*/
                                                JSONArray polyArray = preferenceObj.optJSONArray("polygon");
                                                if (polyArray != null) {
                                                    PolygonOptions lineOptions = new PolygonOptions();
                                                    ArrayList<LatLng> latLngPoints = new ArrayList<LatLng>();
                                                    for (int i = 0; i < polyArray.length(); i++) {
                                                        JSONObject polyobj = polyArray.optJSONObject(i);
                                                        double lat = Double.parseDouble(polyobj.optString("latitude"));
                                                        double lng = Double.parseDouble(polyobj.optString("longitude"));
                                                        if (i == 0 && startLatLng == null) {
                                                            startLatLng = new LatLng(lat, lng);
                                                        }
                                                        if (i == polyArray.length() - 1 && endLatLng == null) {
                                                            endLatLng = new LatLng(lat, lng);
                                                        }
                                                        LatLng position = new LatLng(lat, lng);
                                                        latLngPoints.add(position);
                                                    }
                                                    LatLng centerLatLng = getPolygonCenterPoint(latLngPoints);
                                                    serviceSetting.setCenterLanLng(centerLatLng);
                                                    setMarker(centerLatLng, j);


                                                    lineOptions.addAll(latLngPoints);
                                                    Polygon polygon;

                                                    if (preferenceObj.optBoolean("selected")) {
                                                        polygon = mMap.addPolygon(lineOptions.strokeWidth(2.f).strokeColor(getResources().getColor(R.color.black)).fillColor(getResources().getColor(R.color.grey_transparent)));
                                                        polygon.setClickable(true);
                                                    } else {
                                                        polygon = mMap.addPolygon(lineOptions.strokeWidth(2.f).strokeColor(getResources().getColor(R.color.light_green)).fillColor(getResources().getColor(R.color.light_green_transparent)));
                                                        polygon.setClickable(true);
                                                    }
                                                    serviceSetting.setPolygon(polygon);
                                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, zoomValue));
                                                }

                                                jobLocationArray.add(serviceSetting);
                                            }
                                            jobLocationAdapter.notifyDataSetChanged();
                                            transportSetup();
                                        }
                                    }
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }

                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyUpDateSettingTask(final String selectedJob) {

        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_SETTING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.success), object.optString("msg"));
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("joblocations", selectedJob);
                params.put("transport", transport);
                params.put("priority", "fixed");
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private String getSelectedJob() {
        String params = "";
        try {
            JSONArray jobArray = new JSONArray();
            for (int i = 0; i < jobLocationArray.size(); i++) {
                JSONObject object = new JSONObject();
                if (jobLocationArray.get(i).isSelected()) {
                    object.put("id", jobLocationArray.get(i).getPreferenceId());
                    jobArray.put(object);
                }
            }
            if (jobArray.length() > 0) {
                params = jobArray.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return params;
    }

    private void setMarker(LatLng centerLatLng, int j) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.inflate_marker_layout, null);
        TextView tvMarkerText = (TextView) v.findViewById(R.id.tvMarkerText);
        tvMarkerText.setText(String.valueOf(j + 1));
        IconGenerator generator = new IconGenerator(getActivity());
        generator.setBackground(null);
        generator.setContentView(v);
        Bitmap icon = generator.makeIcon();
        if (mMap != null && icon != null) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(centerLatLng).snippet(String.valueOf(j)).icon(BitmapDescriptorFactory.fromBitmap(icon)));
            marker.setTag(j);
        }
    }

    private void transportSetup() {
        if (transport.equals("car") || transport.equals("taxi")) {
            transport = "car";
            tvCar.setTextColor(getResources().getColor(R.color.white));
            tvWalking.setTextColor(getResources().getColor(R.color.black));
            llCar.setBackgroundResource(R.drawable.rounded_purple_bg);
            llWalking.setBackgroundResource(R.drawable.rounded_white_bg_with_border);
            imgCarService.setImageResource(R.drawable.car_hv);
            imgWalkService.setImageResource(R.drawable.walking);
            imgSelectCar.setVisibility(View.VISIBLE);
            imgSelectWalk.setVisibility(View.INVISIBLE);
        } else if (transport.equals("walk")) {
            transport = "walk";
            tvWalking.setTextColor(getResources().getColor(R.color.white));
            tvCar.setTextColor(getResources().getColor(R.color.black));
            llWalking.setBackgroundResource(R.drawable.rounded_purple_bg);
            llCar.setBackgroundResource(R.drawable.rounded_white_bg_with_border);
            imgCarService.setImageResource(R.drawable.car);
            imgWalkService.setImageResource(R.drawable.walking_hv);
            imgSelectWalk.setVisibility(View.VISIBLE);
            imgSelectCar.setVisibility(View.INVISIBLE);
        }
    }

    private LatLng getPolygonCenterPoint(ArrayList<LatLng> polygonPointsList) {
        LatLng centerLatLng = null;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < polygonPointsList.size(); i++) {
            builder.include(polygonPointsList.get(i));
        }
        LatLngBounds bounds = builder.build();
        centerLatLng = bounds.getCenter();

        return centerLatLng;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fragmentManager;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            fragmentManager = getFragmentManager();
        } else {
            Log.d(TAG, "using getChildFragmentManager");
            fragmentManager = getChildFragmentManager();
        }

        Fragment fragment = (fragmentManager.findFragmentById(R.id.map_2));
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (fragment != null && ft != null) {
            ft.remove(fragment);
            ft.commit();
        }
    }
}
