package com.uipl.qberprovider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.adapter.JobsListingPagerAdapter;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.Job;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JobsListingFragment extends Fragment {

    static String TAG = "JobsListingFragment";

    public static DialogView dialogview;
    RequestQueue mQueue;
    Preferences pref;
    View v;

    public static boolean currentJobsFragmentLoaded = false, pastJobsFragmentLoaded = false;

    ViewPager viewPager;
    JobsListingPagerAdapter pagerAdapter;
    TabLayout tabLayout;
    static ArrayList<Job> pastJobsList = new ArrayList<>();
    static ArrayList<Job> currentJobsList = new ArrayList<>();

    final int JOB_TAG = 0, DATE_TAG = 1, INTENT_RELOAD = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_jobs_listing, container, false);

        if (GlobalVariable.reloadJobsListingFragment) {
            GlobalVariable.reloadJobsListingFragment = false;
        }

        initViews();

        initToolbar();

        volleyGetCurrentJobs(pref.getStringPreference(getActivity(), Preferences.PHONE));

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case INTENT_RELOAD:
                    volleyGetCurrentJobs(pref.getStringPreference(getActivity(), Preferences.PHONE));
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (GlobalVariable.reloadJobsListingFragment) {
            GlobalVariable.reloadJobsListingFragment = false;
            volleyGetCurrentJobs(pref.getStringPreference(getActivity(), Preferences.PHONE));
        }
    }

    void initToolbar() {
        MenuActivity.tvToolbarTitle.setVisibility(View.VISIBLE);
        MenuActivity.ibSave.setVisibility(View.GONE);
        MenuActivity.tvToolbarTitle.setText(getResources().getString(R.string.jobs));
    }

    void initViews() {
        dialogview = new DialogView(getActivity());
        pref = new Preferences(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());
    }

    void initPager() {
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        pagerAdapter = new JobsListingPagerAdapter(getFragmentManager(), getActivity());
        viewPager.setAdapter(pagerAdapter);

        tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }
    }

    public void volleyGetCurrentJobs(final String mobile) {
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_JOBS_LISTING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                currentJobsList.clear();
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JSONArray dataJsonArr = responseJsonObj.optJSONArray("data");
                                    int size = dataJsonArr.length();
                                    Calendar todayCal = Calendar.getInstance();
                                    Date startDate = null;
                                    for (int i = 0; i < size; i++) {
                                        JSONObject jobJsonObj = dataJsonArr.optJSONObject(i);
                                        JSONArray serviceJsonArr = jobJsonObj.optJSONArray("service");
                                        for (int j = 0; j < serviceJsonArr.length(); j++) {
                                            JSONObject serviceJsonObj = serviceJsonArr.optJSONObject(j);
                                            Job job = new Job();
                                            job.setId(serviceJsonObj.optString("id"));
                                            job.setStatus(serviceJsonObj.optString("job_status"));
                                            job.setLat(serviceJsonObj.optString("latitude"));
                                            job.setLng(serviceJsonObj.optString("longitude"));

                                            JSONObject customerJsonObj = serviceJsonObj.optJSONObject("customer");
                                            job.setCustomerPhoneNumber(customerJsonObj.optString("phone"));
                                            job.setCustomerName(customerJsonObj.optString("name"));

                                            String address = serviceJsonObj.optString("address");
                                            if (address != null && !address.toString().trim().equals("")) {
                                                String addressArr[] = address.split(", ");
                                                if (addressArr != null && addressArr.length > 0) {
                                                    job.setHeaderLocation(addressArr[0]);
                                                }
                                                if (addressArr.length > 1) {
                                                    String temp = "";
                                                    for (int k = 1; k < addressArr.length; k++) {
                                                        if (k == 1) {
                                                            temp = addressArr[k];
                                                        } else {
                                                            temp = temp + ", " + addressArr[k];
                                                        }
                                                    }
                                                    job.setFullAddress(temp);
                                                }
                                            }

                                            JSONObject bookedJsonObj = serviceJsonObj.optJSONObject("booked");
                                            String finishTime = "";
                                            if (bookedJsonObj != null) {
                                                finishTime = bookedJsonObj.optString("to_time");
                                            }

                                            String startDateTime = serviceJsonObj.optString("from_time");
                                            String endDateTime = serviceJsonObj.optString("to_time");
                                            Calendar startCal = Calendar.getInstance();
                                            startDate = GlobalVariable.dateTime24Format.parse(startDateTime);
                                            startCal.setTime(startDate);
                                            job.setDate(startDate);
                                            if (todayCal.get(Calendar.DATE) == startCal.get(Calendar.DATE) && todayCal.get(Calendar.MONTH) == startCal.get(Calendar.MONTH)
                                                    && todayCal.get(Calendar.YEAR) == startCal.get(Calendar.YEAR)) {
                                                job.setDateString(getResources().getString(R.string.today));
                                            } else if (todayCal.get(Calendar.YEAR) == startCal.get(Calendar.YEAR)) {
                                                job.setDateString(GlobalVariable.dateMonthFormat.format(startDate));
                                            } else {
                                                job.setDateString(GlobalVariable.dateMonthYearFormat.format(startDate));
                                            }
                                            job.setArrivalRangeStartTime(GlobalVariable.time12Format.format(GlobalVariable.dateTime24Format.parse(startDateTime)));
                                            job.setArrivalRangeEndTime(GlobalVariable.time12Format.format(GlobalVariable.dateTime24Format.parse(endDateTime)));
                                            if (finishTime != null && !finishTime.toString().trim().equals("")) {
                                                job.setFinishTime(GlobalVariable.time12Format.format(GlobalVariable.dateTime24Format.parse(finishTime)));
                                            }
                                            job.setPrice(serviceJsonObj.optString("expected_cost"));
                                            job.setJobDuration(serviceJsonObj.optString("job_duration_set_by_provider"));
                                            currentJobsList.add(job);
                                        }
                                    }
                                    volleyGetPastJobs(mobile);
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else if (responseJsonObj.optString("success").equals("2")) {
                                    volleyGetPastJobs(mobile);
                                }
                            } else {
                                dialogview.dismissCustomSpinProgress();
                                dialogview.showCustomSingleButtonDialog(getActivity(),
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogview.dismissCustomSpinProgress();
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        } catch (ParseException e) {
                            dialogview.dismissCustomSpinProgress();
                            e.printStackTrace();
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("type", "current");
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyGetPastJobs(final String mobile) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_JOBS_LISTING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                pastJobsList.clear();
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JSONArray dataJsonArr = responseJsonObj.optJSONArray("data");
                                    int size = dataJsonArr.length();
                                    Date startDate = null;
                                    for (int i = 0; i < size; i++) {
                                        JSONObject jobJsonObj = dataJsonArr.optJSONObject(i);
                                        JSONArray serviceJsonArr = jobJsonObj.optJSONArray("service");
                                        Job dateJob = new Job();
                                        dateJob.setNumOfJobs(String.valueOf(serviceJsonArr.length()));
                                        String dateString = jobJsonObj.optString("date");
                                        dateJob.setDate(GlobalVariable.simpleDateFormat.parse(dateString));
                                        dateJob.setType(DATE_TAG);
                                        pastJobsList.add(dateJob);
                                        for (int j = 0; j < serviceJsonArr.length(); j++) {
                                            JSONObject serviceJsonObj = serviceJsonArr.optJSONObject(j);
                                            Job job = new Job();
                                            job.setId(serviceJsonObj.optString("id"));
                                            job.setStatus(serviceJsonObj.optString("job_status"));
                                            job.setLat(serviceJsonObj.optString("latitude"));
                                            job.setLng(serviceJsonObj.optString("longitude"));

                                            JSONObject customerJsonObj = serviceJsonObj.optJSONObject("customer");
                                            job.setCustomerPhoneNumber(customerJsonObj.optString("phone"));
                                            job.setCustomerName(customerJsonObj.optString("name"));

                                            String address = serviceJsonObj.optString("address");
                                            if (address != null && !address.toString().trim().equals("")) {
                                                String addressArr[] = address.split(", ");
                                                if (addressArr != null && addressArr.length > 0) {
                                                    job.setHeaderLocation(addressArr[0]);
                                                }
                                                if (addressArr.length > 1) {
                                                    String temp = "";
                                                    for (int k = 1; k < addressArr.length; k++) {
                                                        if (k == 1) {
                                                            temp = addressArr[k];
                                                        } else {
                                                            temp = temp + ", " + addressArr[k];
                                                        }
                                                    }
                                                    job.setFullAddress(temp);
                                                }
                                            }

                                            JSONObject bookedJsonObj = serviceJsonObj.optJSONObject("booked");
                                            String finishTime = "";
                                            if (bookedJsonObj != null) {
                                                finishTime = bookedJsonObj.optString("to_time");
                                            }

                                            String startDateTime = serviceJsonObj.optString("from_time");
                                            String endDateTime = serviceJsonObj.optString("to_time");
                                            startDate = GlobalVariable.dateTime24Format.parse(startDateTime);
                                            job.setDateString(GlobalVariable.dateMonthFormat.format(startDate));
                                            job.setStartTime(GlobalVariable.time12Format.format(startDate));
                                            if (finishTime != null && !finishTime.toString().trim().equals("")) {
                                                job.setFinishTime(GlobalVariable.time12Format.format(GlobalVariable.dateTime24Format.parse(finishTime)));
                                            }
                                            job.setPrice(serviceJsonObj.optString("expected_cost"));
                                            job.setJobDuration(serviceJsonObj.optString("job_duration_set_by_provider"));
                                            job.setType(JOB_TAG);
                                            pastJobsList.add(job);
                                        }
                                    }
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                }
                            } else {
                                dialogview.dismissCustomSpinProgress();
                                dialogview.showCustomSingleButtonDialog(getActivity(),
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                            initPager();
                        } catch (JSONException e) {
                            dialogview.dismissCustomSpinProgress();
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        } catch (ParseException e) {
                            dialogview.dismissCustomSpinProgress();
                            e.printStackTrace();
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("type", "past");
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    @Override
    public void onPause() {
        super.onPause();
        mQueue.cancelAll(TAG);
    }
}
