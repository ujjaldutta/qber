package com.uipl.qberprovider.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.uipl.qberprovider.R;
import com.uipl.qberprovider.RegistrationActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pallab on 3/11/16.
 *
 */

public class CommonAdapter extends BaseAdapter {
    private final Activity mActivity;
    private final ArrayList<HashMap<String, String>> mArrayList;
    LayoutInflater inflater;

    public CommonAdapter(Activity activity, ArrayList<HashMap<String,String>> arrayList) {
        this.mActivity=activity;
        this.mArrayList=arrayList;
        this.inflater=LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
            view=inflater.inflate(R.layout.spinner_item,viewGroup,false);
        }
        TextView text1=(TextView)view.findViewById(R.id.text1);
        text1.setText(mArrayList.get(i).get("name"));

        return view;
    }
}
