package com.uipl.qberprovider.model;

/**
 * Created by abhijit on 3/10/16.
 */
public class Reviews_Model {

    private String name;
    private String comments;
    private String date;
    private Float rateValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Float getRateValue() {
        return rateValue;
    }

    public void setRateValue(Float rateValue) {
        this.rateValue = rateValue;
    }
}
