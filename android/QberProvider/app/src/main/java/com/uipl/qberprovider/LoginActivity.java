package com.uipl.qberprovider;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.GPSTracker;
import com.uipl.qberprovider.utils.Preferences;
import com.uipl.qberprovider.utils.SnackBarCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private EditText etMobile, etPassword;
    private Button btnLogin;
    private TextView tvForgotPassword;
    private DialogView dialogview;
    String str_message = "";
    private String TAG = "LoginActivity";
    private Preferences pref;
    private RequestQueue mQueue;
    private ImageButton imgBtnBack;
    GPSTracker gpsTracker;

    final int ON_CREATE_PERMISSION = 1, LOGIN_PERMISSION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref = new Preferences(LoginActivity.this);
        initView();
        initData();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, ON_CREATE_PERMISSION);
        } else {
            gpsTracker = new GPSTracker(this);
            if (!gpsTracker.isGPSEnabled) {
                gpsTracker.showSettingsAlert();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case ON_CREATE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsTracker = new GPSTracker(this);
                    if (!gpsTracker.isGPSEnabled) {
                        gpsTracker.showSettingsAlert();
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                }
                return;
            }

            case LOGIN_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsTracker = new GPSTracker(this);
                    if (!gpsTracker.isGPSEnabled) {
                        gpsTracker.showSettingsAlert();
                    } else {
                        if (Validation()) {
                            dialogview.showCustomSpinProgress(LoginActivity.this);
                            new getFirebaseToken().execute();
                        }
                    }
                } else {
                    Toast.makeText(this, getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    void initData() {
        if (!pref.getStringPreference(this, Preferences.PHONE).equals("")) {
            String phone = pref.getStringPreference(this, Preferences.PHONE).toString().trim();
            etMobile.setText(phone.substring((GlobalVariable.ISD_CODE.length())));
        }
    }

    /**
     * Method to initialize variable
     */
    private void initView() {
        gpsTracker = new GPSTracker(this);
        dialogview = new DialogView(this);
        mQueue = Volley.newRequestQueue(this);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        imgBtnBack.setOnClickListener(this);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);

        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, LOGIN_PERMISSION);
                } else {
                    gpsTracker = new GPSTracker(this);
                    double lat = gpsTracker.getLatitude();
                    double lng = gpsTracker.getLongitude();
                    if (!gpsTracker.isGPSEnabled) {
                        gpsTracker.showSettingsAlert();
                    } else if (Double.compare(lat, 0.0) == 0 && Double.compare(lng, 0.0) == 0) {
                        dialogview.showCustomSingleButtonDialog(this, getResources().getString(R.string.sorry), getResources().getString(R.string.location_permission_error));
                    } else {
                        if (Validation()) {
                            dialogview.showCustomSpinProgress(LoginActivity.this);
                            new getFirebaseToken().execute();
                        }
                    }
                }
                break;
            case R.id.tvForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }
    }

    class getFirebaseToken extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            GlobalVariable.FIREBASE_TOKEN = refreshedToken;
            return refreshedToken;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null && !s.toString().equals("")) {
                volleyCustomerLoginTask(etMobile.getText().toString().trim(), etPassword.getText().toString().trim());
            } else {
                dialogview.dismissCustomSpinProgress();
            }
            super.onPostExecute(s);
        }
    }

    /**
     * @param mobile
     * @param password Method to call web service for login
     */
    private void volleyCustomerLoginTask(final String mobile, final String password) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject dataObj = object.optJSONObject("data");
                                    //pref.storeStringPreference(LoginActivity.this, Preferences.USER_ID, dataObj.optString("id"));
                                    pref.storeStringPreference(LoginActivity.this, Preferences.PHONE, dataObj.optString("phone"));
                                    pref.storeStringPreference(LoginActivity.this, Preferences.TOKEN, dataObj.optString("token"));
                                    pref.storeBooleanPreference(LoginActivity.this, Preferences.IS_LOGGED_IN, true);
                                    startActivity(new Intent(LoginActivity.this, MenuActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                } else if (object.optString("success").equals("2")) {
                                    startActivity(new Intent(LoginActivity.this, MobileVerificationActivity.class).putExtra("SMS_CODE", "").putExtra("PHONE", etMobile.getText().toString().trim()));
                                } else {
                                    str_message = object.optString("msg");
                                    dialogview.showCustomSingleButtonDialog(LoginActivity.this, getResources().getString(R.string.sorry), str_message);
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(LoginActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(LoginActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(LoginActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(LoginActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", GlobalVariable.ISD_CODE + mobile);
                params.put("password", password);
                params.put("device_id", GlobalVariable.FIREBASE_TOKEN);
                params.put("latitude", String.valueOf(gpsTracker.getLatitude()));
                params.put("longitude", String.valueOf(gpsTracker.getLongitude()));
                params.put("device_type", "android");
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * @return true if all the data are valid
     * Method to check validation
     */
    public boolean Validation() {
        if (etMobile.getText().toString().trim().isEmpty()) {
            SnackBarCustom.showSnackWithActionButton(etMobile, getResources().getString(R.string.please_enter_mobile_number));
            etMobile.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etPassword.getText().toString().trim().isEmpty()) {
            etPassword.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etPassword, getResources().getString(R.string.please_enter_password));
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);

            return false;
        }

        return true;
    }

}
