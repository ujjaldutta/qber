package com.uipl.qberprovider;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.utils.DateFormatter;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.GPSTracker;
import com.uipl.qberprovider.utils.Permission;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class BalanceSettlingFragment extends Fragment implements View.OnClickListener, GoogleMap.OnInfoWindowClickListener, OnMapReadyCallback {
    private static final int ACTIVITY_REQUEST_CODE_FOR_MAP = 102;


    private GoogleMap mMap;
    GPSTracker gpsTracker;
    double lat = 0.0, lng = 0.0;
    double currentLat = 0.0, currentLng = 0.0;
    private String address = "";
    Marker marker;
    TextView current_bill, limit_bill, tv_info;
    Spinner spinner1, spinner2, spinner3;
    String arrival_time, deprt_time;
    String s1 = "", s2 = "";
    String lat1, lng1;
    public static String TAG = "BalanceSettlingFragment";
    private DialogView dialogview;
    private Preferences pref;
    private RequestQueue mQueue;
    private TextView tvTitle;
    private Button btnRequestAgent;
    private LinearLayout llArrivalTimeView, llShowMsgView;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
    private ImageView imgLimitQuestion, imgRequestSetting;
    private ImageView imgSpDay, imgSpFromTime, imgSpToTime;
    private LinearLayout llMainInfoWindow;
    private CustomInfoWindow customInfoWindow;
    private boolean addressFlag = false;
    float zoomValue = 10.0f;

    public BalanceSettlingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_balance_setting, container, false);
        initView(view);
        FragmentManager fragmentManager;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d(TAG, "using getFragmentManager");
            fragmentManager = getFragmentManager();
        } else {
            Log.d(TAG, "using getChildFragmentManager");
            fragmentManager = getChildFragmentManager();
        }
        MapFragment mapFragment = (MapFragment) fragmentManager
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getBalanceSetting();
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SpinnerDateChecking();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                String fromDate = (String) adapterView.getAdapter().getItem(pos);
                ArrayList tempArray = new ArrayList();
                try {
                    Date date1 = GlobalVariable.time12Format.parse(fromDate);
                    String[] testArray = getResources().getStringArray(R.array.times_array);
                    for (int i = 0; i < testArray.length; i++) {
                        Date date2 = GlobalVariable.time12Format.parse(testArray[i]);
                        if (date1.before(date2)) {
                            tempArray.add(testArray[i]);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item_work_time, tempArray);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                spinner3.setAdapter(arrayAdapter);
                spinner3.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    private void initView(View view) {
        dialogview = new DialogView(getActivity());
        pref = new Preferences(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());
        gpsTracker = new GPSTracker(getActivity());
        btnRequestAgent = (Button) view.findViewById(R.id.btnRequestAgent);
        llArrivalTimeView = (LinearLayout) view.findViewById(R.id.llArrivalTimeView);
        llShowMsgView = (LinearLayout) view.findViewById(R.id.llShowMsgView);
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        imgSpDay = (ImageView) view.findViewById(R.id.imgSpDay);
        imgSpDay.setOnClickListener(this);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
        imgSpFromTime = (ImageView) view.findViewById(R.id.imgSpFromTime);
        imgSpFromTime.setOnClickListener(this);
        spinner3 = (Spinner) view.findViewById(R.id.spinner3);
        imgSpToTime = (ImageView) view.findViewById(R.id.imgSpToTime);
        imgSpToTime.setOnClickListener(this);
        current_bill = (TextView) view.findViewById(R.id.tv_current_bill);
        limit_bill = (TextView) view.findViewById(R.id.tv_limit);
        tv_info = (TextView) view.findViewById(R.id.tv_info);
        imgLimitQuestion = (ImageView) view.findViewById(R.id.imgLimitQuestion);
        imgRequestSetting = (ImageView) view.findViewById(R.id.imgRequestSetting);
        imgRequestSetting.setOnClickListener(this);
        imgLimitQuestion.setOnClickListener(this);

        btnRequestAgent.setOnClickListener(this);
    }

    private void getBalanceSetting() {
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_BALANCE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject dataObj = object.getJSONObject("data");
                                    String current_balnc = dataObj.optString("current_credit_balance").toString().trim();
                                    String max_due = dataObj.optString("max_due_amount").toString().trim();
                                    if (current_balnc != null && !current_balnc.equals("null")) {
                                        current_bill.setText(current_balnc + " " + getResources().getString(R.string.qar));
                                    } else {
                                        current_bill.setText("00 " + getResources().getString(R.string.qar));
                                    }
                                    if (max_due != null && !max_due.equals("null")) {
                                        limit_bill.setText(max_due + " " + getResources().getString(R.string.qar));
                                    } else {
                                        limit_bill.setText("00 " + getResources().getString(R.string.qar));
                                    }
                                    if (dataObj.optString("can_cancel").equals("0")) {
                                        btnRequestAgent.setText(getString(R.string.request_agent));
                                        llArrivalTimeView.setVisibility(View.VISIBLE);
                                        llShowMsgView.setVisibility(View.GONE);
                                    } else if (dataObj.optString("can_cancel").equals("1")) {
                                        btnRequestAgent.setText(getString(R.string.cancel_request));
                                        llArrivalTimeView.setVisibility(View.GONE);
                                        llShowMsgView.setVisibility(View.VISIBLE);
                                        Double mlat = Double.parseDouble(dataObj.optString("agent_arrival_latitude"));
                                        Double mlng = Double.parseDouble(dataObj.optString("agent_arrival_longitude"));
                                        if (mlat != null && mlng != null) {
                                            lat = mlat;
                                            lng = mlng;
                                        }
                                        String address = dataObj.optString("address");
                                        if (address == null || address.equals("null")) {
                                            address = "";
                                        }
                                        mapCameraMove(mlat, mlng, address);
                                        String strAv = dataObj.optString("arrival_time");
                                        String strDp = dataObj.optString("departure_time");
                                        FormattedDateSetup(strAv, strDp);

                                    }
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }

                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }


    private void cancelBalanceSetting() {

        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_CANCEL_BALANCE_SETTING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.success), object.optString("msg"));
                                    btnRequestAgent.setText(getString(R.string.request_agent));
                                    llArrivalTimeView.setVisibility(View.VISIBLE);
                                    llShowMsgView.setVisibility(View.GONE);
                                    SpinnerSetup();
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }

                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    public void FormattedDateSetup(String startDateTime, String endDateTime) {
        String startTime = "";
        String endTime = "";
        String date = "";
        if (startDateTime != null && !startDateTime.equals("") && !startDateTime.equals("null")) {
            date = DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy", startDateTime);
            startTime = DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", startDateTime);
        }
        if (endDateTime != null && !endDateTime.equals("") && !endDateTime.equals("null")) {
            endTime = DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", endDateTime);
        }


        Calendar todayCal = Calendar.getInstance();
        Calendar startCal = Calendar.getInstance();
        try {
            startCal.setTime(GlobalVariable.dateTime24Format.parse(startDateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (todayCal.get(Calendar.DATE) == startCal.get(Calendar.DATE) && todayCal.get(Calendar.MONTH) == startCal.get(Calendar.MONTH)
                && todayCal.get(Calendar.YEAR) == startCal.get(Calendar.YEAR)) {
            tv_info.setText(getString(R.string.agent_will_arrive) + " " + getString(R.string.today) + " " + getString(R.string.between) + " " + startTime + " " + getString(R.string.to) + " " +
                    endTime + " " + getString(R.string.at_selected_location));
        } else {
            tv_info.setText(getString(R.string.agent_will_arrive) + " " + date + " " + getString(R.string.between) + " " + startTime + " " + getString(R.string.to) + " " +
                    endTime + " " + getString(R.string.at_selected_location));
        }

    }

    public void getLocation() {
        if (gpsTracker.canGetLocation()) {
            currentLat = gpsTracker.getLatitude();
            currentLng = gpsTracker.getLongitude();
            mapCameraMove(currentLat, currentLng, "");

        } else {
            gpsTracker.showSettingsAlert();
        }
    }


    private void setUpMap() {
        getLocation();
        if (currentLat == 0.0 && currentLng == 0.0) {
            currentLat = 25.358;
            currentLng = 51.1839;
        }

        LatLng latLng = new LatLng(currentLat, currentLng);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        PermissionChecking();
        marker = mMap.addMarker(new MarkerOptions().position(latLng).snippet(getResources().getString(R.string.tap_to_set_location)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomValue));
        customInfoWindow = new CustomInfoWindow();
        mMap.setInfoWindowAdapter(customInfoWindow);
        marker.showInfoWindow();
        mMap.setOnInfoWindowClickListener(this);
        SpinnerSetup();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if (marker != null) {
                    marker.showInfoWindow();
                }
                if (btnRequestAgent != null && btnRequestAgent.getText().toString().equals(getResources().getString(R.string.request_agent))) {
                    if (lat == 0.0 && lng == 0.0) {
                        lat = currentLat;
                        lng = currentLng;
                    }
                    if (address.equals(getString(R.string.tap_to_set_location))) {
                        address = "";
                    }

                    startActivityForResult(new Intent(getActivity(), LocationSelectMapActivity.class).putExtra("LAT", String.valueOf(lat)).putExtra("LNG", String.valueOf(lng)).putExtra("ADDRESS", address), ACTIVITY_REQUEST_CODE_FOR_MAP);
                }
            }
        });
    }

    public void SpinnerSetup() {
        ArrayList<String> tempArr = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        tempArr.add("Today");
        tempArr.add("Tomorrow");
        for (int i = 0; i < 5; i++) {
            cal.add(Calendar.DATE, 1);
            tempArr.add(dateFormat.format(cal.getTime()));
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item_work_time, tempArr);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinner1.setAdapter(arrayAdapter);
        spinner1.setSelection(0);
        SpinnerDateChecking();

    }

    public void SpinnerDateChecking() {
        if (spinner1.getSelectedItemPosition() == 0) {
            ArrayList tempArray = new ArrayList();
            try {

                Calendar c = Calendar.getInstance();
                String currentDate = GlobalVariable.time12Format.format(c.getTime());
                Date date1 = GlobalVariable.time12Format.parse(currentDate);

                String[] testArray = getResources().getStringArray(R.array.times_array);
                for (int i = 0; i < testArray.length; i++) {
                    Date date2 = GlobalVariable.time12Format.parse(testArray[i]);
                    if (date1.before(date2)) {
                        tempArray.add(testArray[i]);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (tempArray != null) {
                if (tempArray.size() > 1) {
                    tempArray.remove(tempArray.size() - 1);
                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item_work_time, tempArray);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                    spinner2.setAdapter(arrayAdapter);
                    spinner2.setSelection(0);
                } else {
                    spinner1.setSelection(1);
                }

            }
        } else {
            String[] testArray = getResources().getStringArray(R.array.times_array);
            if (testArray.length > 1) {
                List<String> fromArray = new ArrayList<String>(Arrays.asList(testArray));
                fromArray.remove(fromArray.size() - 1);
                ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item_work_time, fromArray);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                spinner2.setAdapter(arrayAdapter);
                spinner2.setSelection(0);
            }

        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (btnRequestAgent != null && btnRequestAgent.getText().toString().equals(getResources().getString(R.string.request_agent))) {
            if (lat == 0.0 && lng == 0.0) {
                lat = currentLat;
                lng = currentLng;
            }
            if (address.equals(getString(R.string.tap_to_set_location))) {
                address = "";
            }

            startActivityForResult(new Intent(getActivity(), LocationSelectMapActivity.class).putExtra("LAT", String.valueOf(lat)).putExtra("LNG", String.valueOf(lng)).putExtra("ADDRESS", address), ACTIVITY_REQUEST_CODE_FOR_MAP);
        }
    }


    public void mapCameraMove(Double Lat, Double Lng, String address) {

        LatLng latLng = null;
        if (Lat == 0.0 && Lng == 0.0) {
            latLng = new LatLng(currentLat, currentLng);
        } else {
            latLng = new LatLng(Lat, Lng);
        }
        if (address.equals("")) {
            address = getResources().getString(R.string.tap_to_set_location);

        }
        if (mMap != null) {
            mMap.clear();
            if (customInfoWindow != null && address.equals(getString(R.string.tap_to_set_location))) {
                addressFlag = false;
            } else if (customInfoWindow != null) {
                addressFlag = true;
            }
            marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)).snippet(address));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomValue));

            marker.showInfoWindow();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_REQUEST_CODE_FOR_MAP && resultCode == Activity.RESULT_OK) {
            if (data != null && data.hasExtra("lat") && data.hasExtra("lng")) {
                lat = data.getDoubleExtra("lat", lat);
                lng = data.getDoubleExtra("lng", lng);
                address = data.getStringExtra("address");
                if (address.equals("")) {
                    address = getResources().getString(R.string.tap_to_set_location);
                }
                mapCameraMove(lat, lng, address);

            }
        }
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    dialogview.showToast(getActivity(), getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }

    private void volleyRequestBalance() {
        dialogview.showCustomSpinProgress(getActivity());
        arrival_time = spinner2.getSelectedItem().toString();
        deprt_time = spinner3.getSelectedItem().toString();
        SimpleDateFormat sdf1 = new SimpleDateFormat("d MMM yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("d MMM yyyy hh:mm a");
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tempDate = spinner1.getSelectedItem().toString();
        Calendar c = Calendar.getInstance();
        if (spinner1.getSelectedItemPosition() == 0) {
            tempDate = sdf1.format(c.getTime());
        } else if (spinner1.getSelectedItemPosition() == 1) {
            c.add(Calendar.DATE, 1);
            tempDate = sdf1.format(c.getTime());
        }
        Date d = null;
        try {
            d = sdf2.parse(tempDate + " " + arrival_time);
            s1 = sdf3.format(d);
            d = sdf2.parse(tempDate + " " + deprt_time);
            s2 = sdf3.format(d);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        lat1 = Double.toString(lat).trim();
        lng1 = Double.toString(lng).trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REQUEST_BALANCE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.success), object.optString("msg"));
                                    btnRequestAgent.setText(getString(R.string.cancel_request));
                                    llArrivalTimeView.setVisibility(View.GONE);
                                    llShowMsgView.setVisibility(View.VISIBLE);
                                    JSONObject dataObj = object.getJSONObject("data");
                                    String strAv = dataObj.optString("arrival_time");
                                    String strDp = dataObj.optString("departure_time");
                                    FormattedDateSetup(strAv, strDp);
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }

                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("arrival_time", s1);
                params.put("departure_time", s2);
                params.put("agent_arrival_latitude", lat1);
                params.put("agent_arrival_longitude", lng1);
                params.put("address", address);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRequestAgent:
                if (btnRequestAgent.getText().toString().equals(getString(R.string.request_agent))) {
                    if (Validation()) {
                        MyDialog(getActivity(), getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_request_agent), "request");

                    }
                } else if (btnRequestAgent.getText().toString().equals(getString(R.string.cancel_request))) {
                    MyDialog(getActivity(), getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_cancel_request), "cancel");

                }
                break;
            case R.id.imgLimitQuestion:
                dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.app_name), "coming soon");
                break;
            case R.id.imgRequestSetting:
                dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.app_name), "coming soon");
                break;
            case R.id.imgSpDay:
                spinner1.performClick();
                break;
            case R.id.imgSpFromTime:
                spinner2.performClick();
                break;
            case R.id.imgSpToTime:
                spinner3.performClick();
                break;


        }
    }

    private boolean Validation() {
        if (address.equals("") || address.equals(getString(R.string.tap_to_set_location))) {
            dialogview.showCustomSingleButtonDialog(getActivity(), getString(R.string.sorry), getResources().getString(R.string.please_select_a_service_location));
            return false;
        } else if (spinner2.getSelectedItemPosition() > spinner3.getSelectedItemPosition()) {
            dialogview.showCustomSingleButtonDialog(getActivity(), getString(R.string.sorry), getResources().getString(R.string.end_time_should_be_after_start_time));
            return false;
        }

        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            setUpMap();
        }

    }


    /**
     * InfoWindowAdapter to set custom infoWindow
     */
    public class CustomInfoWindow implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            View v = getActivity().getLayoutInflater().inflate(R.layout.infowindowlayout, null);
            String title = marker.getSnippet();
            llMainInfoWindow = (LinearLayout) v.findViewById(R.id.llMainInfoWindow);
            if (addressFlag) {
                llMainInfoWindow.setBackgroundResource(R.drawable.pointer320);
            } else {
                llMainInfoWindow.setBackgroundResource(R.drawable.pointer_green);
            }
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
            return v;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }

    public void MyDialog(Context context, String header,
                         String msg, final String request) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (request.equals("request")) {
                    volleyRequestBalance();
                } else if (request.equals("cancel")) {
                    cancelBalanceSetting();
                }

                dialog.dismiss();


            }
        });
        adb.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();


            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fragmentManager;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            fragmentManager = getFragmentManager();
        } else {
            fragmentManager = getChildFragmentManager();
        }

        Fragment fragment = (fragmentManager.findFragmentById(R.id.map));
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (fragment != null && ft != null) {
            ft.remove(fragment);
            ft.commit();
        }
    }

}
