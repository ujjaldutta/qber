package com.uipl.qberprovider.base;

import com.uipl.qberprovider.model.DayState;
import com.uipl.qberprovider.model.Timeline;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by pallab on 28/9/16.
 */

public class GlobalVariable {
    public static int Connection_Time_Out = 25000;

    public static String ISD_CODE = "974";
//    public static String ISD_CODE = "91";

    public static String SMS_CODE = "";

    public static ArrayList<DayState> DAYState = new ArrayList<>();

    public static ArrayList<String> weekDaysArr = new ArrayList<>();
    public static ArrayList<String> containedWeeks = new ArrayList<>();

    public static String[] weeArr = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    public static ArrayList<String> showingWeeArr = new ArrayList<>();

    public static ArrayList<ArrayList<Timeline>> timelinesArrayList = new ArrayList<>();

    public static boolean isAvailableArray[] = new boolean[7];

    public static String FIREBASE_TOKEN = "";

    public static DecimalFormat decimalFormat = new DecimalFormat("#0.00");
    public static SimpleDateFormat time12Format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    public static SimpleDateFormat dateTime24Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat dateMonthFormat = new SimpleDateFormat("dd MMMM");
    public static SimpleDateFormat dateMonthYearFormat = new SimpleDateFormat("d MMM, yyyy");

    public static boolean reloadJobsListingFragment = false;

    public static String pushNotificationServiceId = "";

}
