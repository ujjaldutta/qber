package com.uipl.qberprovider.model;

/**
 * Created by pallab on 21/11/16.
 */

public class RegistrationDetails {
    private String name;
    private String yearPosition;
    private String countryPosition;
    private String email;
    private String gender;
    private String phone;
    private String internetAccessPosition;
    private String transportPosition;
    private String yourOffer;
    private String yearOfExperience;
    private String liveInQatar;
    private String workPreferencePosition;
    private String languagePreferencePosition;
    private String isShop;
    private String shopName;
    private String adminPicPath;
    private String toolsPicPath;
    private String shopPicPath;
    private String CVFilePath;
    private String registerPicPath;
    private String drivingLicense;
    private String identityPicPath;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setYearPosition(String yearPosition) {
        this.yearPosition = yearPosition;
    }

    public String getYearPosition() {
        return yearPosition;
    }

    public void setCountryPosition(String countryPosition) {
        this.countryPosition = countryPosition;
    }

    public String getCountryPosition() {
        return countryPosition;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setInternetAccessPosition(String internetAccessPosition) {
        this.internetAccessPosition = internetAccessPosition;
    }

    public String getInternetAccessPosition() {
        return internetAccessPosition;
    }

    public void setTransportPosition(String transportPosition) {
        this.transportPosition = transportPosition;
    }

    public String getTransportPosition() {
        return transportPosition;
    }

    public void setYourOffer(String yourOffer) {
        this.yourOffer = yourOffer;
    }

    public String getYourOffer() {
        return yourOffer;
    }

    public void setYearOfExperience(String yearOfExperience) {
        this.yearOfExperience = yearOfExperience;
    }

    public String getYearOfExperience() {
        return yearOfExperience;
    }

    public void setLiveInQatar(String liveInQatar) {
        this.liveInQatar = liveInQatar;
    }

    public String getLiveInQatar() {
        return liveInQatar;
    }

    public void setWorkPreferencePosition(String workPreferencePosition) {
        this.workPreferencePosition = workPreferencePosition;
    }

    public String getWorkPreferencePosition() {
        return workPreferencePosition;
    }

    public void setLanguagePreferencePosition(String languagePreferencePosition) {
        this.languagePreferencePosition = languagePreferencePosition;
    }

    public String getLanguagePreferencePosition() {
        return languagePreferencePosition;
    }

    public void setIsShop(String isShop) {
        this.isShop = isShop;
    }

    public String getIsShop() {
        return isShop;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setAdminPicPath(String adminPicPath) {
        this.adminPicPath = adminPicPath;
    }

    public String getAdminPicPath() {
        return adminPicPath;
    }

    public void setToolsPicPath(String toolsPicPath) {
        this.toolsPicPath = toolsPicPath;
    }

    public String getToolsPicPath() {
        return toolsPicPath;
    }

    public void setShopPicPath(String shopPicPath) {
        this.shopPicPath = shopPicPath;
    }

    public String getShopPicPath() {
        return shopPicPath;
    }

    public void setCVFilePath(String CVFilePath) {
        this.CVFilePath = CVFilePath;
    }

    public String getCVFilePath() {
        return CVFilePath;
    }

    public void setRegisterPicPath(String registerPicPath) {
        this.registerPicPath = registerPicPath;
    }

    public String getRegisterPicPath() {
        return registerPicPath;
    }

    public void setDrivingLicensePath(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public String getDrivingLicensePath() {
        return drivingLicense;
    }

    public void setIdentityPicPath(String identityPicPath) {
        this.identityPicPath = identityPicPath;
    }

    public String getIdentityPicPath() {
        return identityPicPath;
    }
}
