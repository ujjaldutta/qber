package com.uipl.qberprovider;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qberprovider.adapter.RegServicesListAdapter;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.ServiceType;
import com.uipl.qberprovider.model.SubServiceType;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {


    private DialogView dialogview;
    private Preferences pref;
    private RequestQueue mQueue;
    private String TAG = "RegisterActivity";
    private ImageButton imgBtnBack;
    private GridView gridServicesList;

    private TextView tvServiceDescription, tvToolbarTitle;
    Button btnJoinNow;
    private ImageView imgServiceImage;
    RegServicesListAdapter adapter;
    private ArrayList<ServiceType> serviceTypesArray = new ArrayList<>();
    private ImageLoader imageLoader;
    DisplayImageOptions options;
    private String typeId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        dialogview = new DialogView(this);
        pref = new Preferences(RegisterActivity.this);
        mQueue = Volley.newRequestQueue(this);
        initView();
        volleyTypeTask();
    }

    private void initView() {

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(RegisterActivity.this));
        options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.service_img).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        tvToolbarTitle = (TextView) findViewById(R.id.tvToolbarTitle);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getString(R.string.register));
        imgBtnBack = (ImageButton) findViewById(R.id.ibLeft);
        imgBtnBack.setVisibility(View.VISIBLE);
        imgBtnBack.setOnClickListener(this);

        gridServicesList = (GridView) findViewById(R.id.gridServicesList);
        btnJoinNow = (Button) findViewById(R.id.btnJoinNow);
        btnJoinNow.setOnClickListener(this);
        tvServiceDescription = (TextView) findViewById(R.id.tvServiceDescription);
        imgServiceImage = (ImageView) findViewById(R.id.imgServiceImage);
        adapter = new RegServicesListAdapter(this, serviceTypesArray);
        gridServicesList.setAdapter(adapter);
        gridServicesList.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ibLeft:
                onBackPressed();
                break;
            case R.id.btnJoinNow:
                if (!typeId.equals("")) {
                    startActivity(new Intent(RegisterActivity.this, RegistrationActivity.class).putExtra("type_id", typeId));
                    finish();
                } else {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.select_a_service));
                }
                break;
        }
    }


    /**
     * Call Web Services for  get all services type.
     */
    private void volleyTypeTask() {
        dialogview.showCustomSpinProgress(RegisterActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_TYPE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            serviceTypesArray.clear();
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray = object.optJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject serviceObj = jsonArray.optJSONObject(i);
                                        ServiceType serviceType = new ServiceType();

                                        serviceType.setServiceId(serviceObj.optString("id"));
                                        serviceType.setImageUrl(serviceObj.optString("image"));
                                        serviceType.setDisplayText(serviceObj.optString("display_text"));
                                        if (serviceObj.optString("is_active").equals("1")) {
                                            serviceType.setActiveStatus(true);
                                        } else {
                                            serviceType.setActiveStatus(false);
                                        }
                                        serviceType.setServiceName(serviceObj.optString("name"));
                                        if (serviceObj.has("servicesetting") && serviceObj.get("servicesetting") instanceof JSONObject) {
                                            JSONObject serviceSettingObj = serviceObj.optJSONObject("servicesetting");
                                            serviceType.setFixedCost(serviceSettingObj.optString("fixed_cost"));
                                            serviceType.setArrivalDuration(serviceSettingObj.optString("arrival_duration"));
                                            serviceType.setAllowedStartTime(serviceSettingObj.optString("allowed_start_time"));
                                            serviceType.setAllowedEndTime(serviceSettingObj.optString("allowed_end_time"));
                                            serviceType.setMinCost(serviceSettingObj.optString("min_cost"));
                                        }

                                        JSONArray optionArray = serviceObj.optJSONArray("options");

                                        ArrayList<SubServiceType> subServiceTypesArray = new ArrayList<>();
                                        if (optionArray != null) {
                                            for (int j = 0; j < optionArray.length(); j++) {
                                                JSONObject subServiceObj = optionArray.optJSONObject(j);
                                                SubServiceType subServiceType = new SubServiceType();
                                                subServiceType.setName(subServiceObj.optString("name"));
                                                JSONArray optDetailsArray = subServiceObj.optJSONArray("option_details");
                                                ArrayList<HashMap<String, String>> optDetails = new ArrayList<>();
                                                for (int k = 0; k < optDetailsArray.length(); k++) {
                                                    JSONObject optDetailsObj = optDetailsArray.optJSONObject(k);
                                                    HashMap<String, String> hashMap = new HashMap<>();
                                                    hashMap.put("name", optDetailsObj.optString("name"));
                                                    hashMap.put("cost", optDetailsObj.optString("cost"));
                                                    optDetails.add(hashMap);
                                                }
                                                subServiceType.setOptionDetailsArray(optDetails);
                                                subServiceTypesArray.add(subServiceType);

                                            }
                                        }

                                        serviceType.setsubServicesArray(subServiceTypesArray);
                                        serviceType.setSelectFlag(false);
                                        serviceTypesArray.add(serviceType);
                                    }
                                    /*initially set 1st services type*/
                                    if (serviceTypesArray.size() > 0) {
                                        serviceTypesArray.get(0).setSelectFlag(true);
                                        typeId = serviceTypesArray.get(0).getServiceId();
                                        tvServiceDescription.setText(serviceTypesArray.get(0).getDisplayText());
                                        imageLoader.displayImage(ConFig_URL.IMAGE_URL + serviceTypesArray.get(0).getImageUrl(), imgServiceImage, options);
                                    } else {
                                        dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                                                getResources().getString(R.string.sorry), getResources().getString(R.string.no_service_available));
                                    }
                                    if (serviceTypesArray.size() > 9) {
                                        int height = convertDpToPixels(150.0f, RegisterActivity.this);
                                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);
                                        gridServicesList.setLayoutParams(lp);
                                    } else {
                                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        gridServicesList.setLayoutParams(lp);
                                    }
                                    adapter.notifyDataSetChanged();

                                } else {
                                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(RegisterActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        refreshServiceList(i);
        if (i == 0) {
            btnJoinNow.setEnabled(true);
            btnJoinNow.setTextColor(getResources().getColor(R.color.white));
        } else {
            btnJoinNow.setEnabled(false);
            btnJoinNow.setTextColor(getResources().getColor(R.color.grey));
        }
        tvServiceDescription.setText(serviceTypesArray.get(i).getDisplayText());
        typeId = serviceTypesArray.get(i).getServiceId();
        imageLoader.displayImage(ConFig_URL.IMAGE_URL + serviceTypesArray.get(i).getImageUrl(), imgServiceImage, options);
    }

    /**
     * @param selectPosition Method to refresh Service List
     */
    private void refreshServiceList(int selectPosition) {
        for (int i = 0; i < serviceTypesArray.size(); i++) {
            if (i == selectPosition) {
                serviceTypesArray.get(i).setSelectFlag(true);
            } else {
                serviceTypesArray.get(i).setSelectFlag(false);
            }
        }
        adapter.notifyDataSetChanged();
    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }
}
