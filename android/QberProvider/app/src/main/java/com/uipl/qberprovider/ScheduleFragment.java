package com.uipl.qberprovider;


import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.Schedule;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment {

    LinearLayout llScheduleContainer;
    DialogView dialogview;
    String str_status = "", str_message = "";
    String str_START = "", str_END = "";
    Preferences pref;
    RequestQueue mQueue;
    public static String TAG = "ScheduleActivity";
    private ArrayList<Schedule> temp_scheduleList = new ArrayList<Schedule>();
    private ArrayList<Schedule> scheduleList = new ArrayList<Schedule>();


    public ScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        init(view);
        getScheduleData();
        return view;
    }

    private void init(View view) {
        dialogview = new DialogView(getActivity());
        pref = new Preferences(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());


        llScheduleContainer = (LinearLayout) view.findViewById(R.id.llScheduleContainer);


        System.out.println("ABHI:" + System.currentTimeMillis());
    }

    private void getScheduleData() {
        Calendar calendar = Calendar.getInstance();
        final String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SCHEDULE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        Log.e("fbLogin", response);
                        try {
                            System.out.println("result = " + response.toString());
                            JSONObject Obj = new JSONObject(response);
                            Log.d("ANKAN", Obj.toString(1));
                            if (Obj.optString("success").equals("0")) {
                                JSONArray jarray = Obj.optJSONArray("schedule");
                                temp_scheduleList.clear();
                                scheduleList.clear();
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jsonObject = jarray.optJSONObject(i);
                                    JSONObject scheduleObject = jsonObject.optJSONObject("schedule");
                                    if (scheduleObject.optString("time_type").equalsIgnoreCase("booked")) {
                                        temp_scheduleList.add(new Schedule(
                                                scheduleObject.optString("from_time"),
                                                scheduleObject.optString("to_time"),
                                                scheduleObject.optString("time_type"),
                                                "null",
                                                "null",
                                                scheduleObject.optString("work_start_time"),
                                                scheduleObject.optString("work_end_time"),
                                                scheduleObject.optString("work_duration"),
                                                scheduleObject.optString("heading_time"),
                                                scheduleObject.optString("service_id"),
                                                scheduleObject.optString("job_id"),
                                                scheduleObject.optString("shorttime"),
                                                scheduleObject.optString("current_work_start_time")));
                                    } else {
                                        if (!scheduleObject.optString("duration").equalsIgnoreCase("null")) {
                                            temp_scheduleList.add(new Schedule(
                                                    scheduleObject.optString("from_time"),
                                                    scheduleObject.optString("to_time"),
                                                    scheduleObject.optString("time_type"),
                                                    scheduleObject.optString("duration"),
                                                    scheduleObject.optString("free_id"),
                                                    "null",
                                                    "null",
                                                    "null",
                                                    "null",
                                                    "null",
                                                    "null",
                                                    "null",
                                                    "null"));
                                        }

                                    }
                                }
                                removeDuplicate();
                                System.out.println("size:" + scheduleList.size());
                                showScheduleList();
                            } else if (Obj.optString("success").equals("1")) {
                                pref.clearAllPref();
                                startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            } else {
                                str_message = Obj.optString("msg");
                                dialogview.showCustomSingleButtonDialog(getActivity(),
                                        getResources().getString(R.string.sorry), str_message);
                            }
                        } catch (JSONException e) {

                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), "JSON error");

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), "Timeout Error");
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), "Network error");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("date", currentDate);
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    private void removeDuplicate() {
        try {
            if (!temp_scheduleList.isEmpty()) {
                for (int i = 0; i < temp_scheduleList.size(); i++) {
                    Schedule currentSchedule = new Schedule();
                    currentSchedule = temp_scheduleList.get(i);
                    if (i != 0) {
                        Schedule lastSchedule = new Schedule();
                        lastSchedule = scheduleList.get(scheduleList.size() - 1);
                        if (lastSchedule.getType().toString().equals("free")) {
                            Date lastDate = null, currentDate = null;
                            lastDate = GlobalVariable.dateTime24Format.parse(lastSchedule.getFrom_time());
                            currentDate = GlobalVariable.dateTime24Format.parse(currentSchedule.getFrom_time());
                            if (lastDate.compareTo(currentDate) >= 0) {
                                scheduleList.remove(scheduleList.size() - 1);
                            }
                        }
                    }
                    scheduleList.add(currentSchedule);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void showScheduleList() {
        llScheduleContainer.removeAllViews();
        int a = 0;
        LayoutInflater layoutInflater = (LayoutInflater) getActivity()
                .getBaseContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < scheduleList.size(); i++) {
            final int pos = i;

            View view = null;
            if (scheduleList.get(i).getType().equalsIgnoreCase("free")) {
                view = layoutInflater.inflate(R.layout.item_available_schedule, null);
                TextView tvTime = (TextView) view.findViewById(R.id.tvTime);
//              tvTime.setText(dateFormate(scheduleList.get(i).getFrom_time(), "yyyy-mm-dd hh:mm:ss"));
                String str = dateFormate(scheduleList.get(i).getFrom_time(), "yyyy-mm-dd hh:mm:ss");
                tvTime.setText(Html.fromHtml(str.substring(0, str.length() - 2) + "<small>" +
                        str.substring(str.length() - 2) + "</small>"));
                if (scheduleList.get(i).getDuration() > 50) {
                    View viewLine = (View) view.findViewById(R.id.viewLine);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewLine.getLayoutParams();
                    params.height = scheduleList.get(i).getDuration() * 1;
                    viewLine.setLayoutParams(params);
                } else {
                    View viewLine = (View) view.findViewById(R.id.viewLine);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewLine.getLayoutParams();
                    params.height = scheduleList.get(i).getDuration() * 2;
                    viewLine.setLayoutParams(params);
                }
            } else if (scheduleList.get(i).getType().equalsIgnoreCase("booked")) {
                view = layoutInflater.inflate(R.layout.item_appointment_schedule, null);
                if (scheduleList.get(i).getShorttime().equalsIgnoreCase("true")) {
                    View viewStaticDot = (View) view.findViewById(R.id.viewStaticDot);
                    viewStaticDot.setVisibility(View.VISIBLE);
                    LinearLayout llHeadingLate = (LinearLayout) view.findViewById(R.id.llHeadingLate);
                    llHeadingLate.setVisibility(View.VISIBLE);

                    if (Integer.parseInt(scheduleList.get(i).getHeading_time()) > 20) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                llHeadingLate.getLayoutParams();
                        params.height = Integer.parseInt(scheduleList.get(i).getHeading_time()) * 4;
                        llHeadingLate.setLayoutParams(params);
                    }
                    for (int j = 0; j < 100; j++) {
                        View viewDot = layoutInflater.inflate(R.layout.item_dot_view, null);
                        llHeadingLate.addView(viewDot);
                    }
                    RelativeLayout rlLate = (RelativeLayout) view.findViewById(R.id.rlLate);
                    rlLate.setVisibility(View.VISIBLE);
                    LinearLayout llWorkingLate = (LinearLayout) view.findViewById(R.id.llWorkingLate);
                    if (timeDifferenceInMinutes(scheduleList.get(i).getWork_start_time(),
                            scheduleList.get(i).getCurrent_work_start_time(), "yyyy-mm-dd hh:mm:ss") > 20) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                llWorkingLate.getLayoutParams();
                        params.height = (int) (timeDifferenceInMinutes(scheduleList.get(i).getWork_start_time(),
                                scheduleList.get(i).getCurrent_work_start_time(), "yyyy-mm-dd hh:mm:ss")) * 4;
                        llWorkingLate.setLayoutParams(params);
                    }
                    for (int j = 0; j < 100; j++) {
                        View viewDot = layoutInflater.inflate(R.layout.item_dot_view, null);
                        llWorkingLate.addView(viewDot);
                    }
                    View viewAppointmentLateLine = (View) view.findViewById(R.id.viewAppointmentLateLine);
                    if (timeDifferenceInMinutes(scheduleList.get(i).getWork_start_time(),
                            scheduleList.get(i).getCurrent_work_start_time(), "yyyy-mm-dd hh:mm:ss") > 20) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                viewAppointmentLateLine.getLayoutParams();
                        params.height = (int) (timeDifferenceInMinutes(scheduleList.get(i).getWork_start_time(),
                                scheduleList.get(i).getCurrent_work_start_time(), "yyyy-mm-dd hh:mm:ss")) * 4;
                        viewAppointmentLateLine.setLayoutParams(params);
                    }
                    View viewAppointmentStartLine = (View) view.findViewById(R.id.viewAppointmentStartLine);
                    if (timeDifferenceInMinutes(scheduleList.get(i).getCurrent_work_start_time(),
                            scheduleList.get(i).getWork_end_time(), "yyyy-mm-dd hh:mm:ss") > 20) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                viewAppointmentStartLine.getLayoutParams();
                        params.height = (int) (timeDifferenceInMinutes(scheduleList.get(i).getCurrent_work_start_time(),
                                scheduleList.get(i).getWork_end_time(), "yyyy-mm-dd hh:mm:ss")) * 4;
                        viewAppointmentStartLine.setLayoutParams(params);
                    }
                    TextView tvAppointmentLate = (TextView) view.findViewById(R.id.tvAppointmentLate);
                    String str = dateFormate(scheduleList.get(i).getCurrent_work_start_time(), "yyyy-mm-dd hh:mm:ss");
                    tvAppointmentLate.setText(Html.fromHtml(str.substring(0, str.length() - 2) + "<small>" +
                            str.substring(str.length() - 2) + "</small>"));
                } else {
                    View viewStaticDot = (View) view.findViewById(R.id.viewStaticDot);
                    viewStaticDot.setVisibility(View.GONE);
                    LinearLayout llHeadingLate = (LinearLayout) view.findViewById(R.id.llHeadingLate);
                    llHeadingLate.setVisibility(View.GONE);
                    RelativeLayout rlLate = (RelativeLayout) view.findViewById(R.id.rlLate);
                    rlLate.setVisibility(View.GONE);

                    View viewAppointmentStartLine = (View) view.findViewById(R.id.viewAppointmentStartLine);

                    if (Integer.parseInt(scheduleList.get(i).getWork_duration()) > 20
                            && Integer.parseInt(scheduleList.get(i).getWork_duration()) <= 150) {
                        System.out.println("RRR" + scheduleList.get(i).getWork_duration());
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                viewAppointmentStartLine.getLayoutParams();
                        params.height = Integer.parseInt(scheduleList.get(i).getWork_duration()) * 4;
                        //   params.height = 200;
                        viewAppointmentStartLine.setLayoutParams(params);
                    } else if (Integer.parseInt(scheduleList.get(i).getWork_duration()) > 150) {
                        System.out.println("SSS" + scheduleList.get(i).getWork_duration());
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                viewAppointmentStartLine.getLayoutParams();
                        params.height = Integer.parseInt(scheduleList.get(i).getWork_duration()) * 2;
                        viewAppointmentStartLine.setLayoutParams(params);
                    }
                }
                TextView tvAppointmentTime = (TextView) view.findViewById(R.id.tvAppointmentTime);
//              tvAppointmentTime.setText(dateFormate(scheduleList.get(i).getFrom_time(), "yyyy-mm-dd hh:mm:ss"));
                String str = dateFormate(scheduleList.get(i).getFrom_time(), "yyyy-mm-dd hh:mm:ss");
                tvAppointmentTime.setText(Html.fromHtml(str.substring(0, str.length() - 2) + "<small>" +
                        str.substring(str.length() - 2) + "</small>"));
                TextView tvAppointmentCount = (TextView) view.findViewById(R.id.tvAppointmentCount);
                a = a + 1;
                tvAppointmentCount.setText(String.valueOf(a));
                TextView tvheadingDuration = (TextView) view.findViewById(R.id.tvheadingDuration);
                if (Integer.parseInt(scheduleList.get(i).getHeading_time()) > 60
                        && Integer.parseInt(scheduleList.get(i).getHeading_time()) % 60 != 0) {
                    int hours = Integer.parseInt(scheduleList.get(i).getHeading_time()) / 60; //since both are ints, you get an int
                    int minutes = Integer.parseInt(scheduleList.get(i).getHeading_time()) % 60;
//                  tvheadingDuration.setText(hours + " hr " + minutes + " min");
                    tvheadingDuration.setText(Html.fromHtml(hours + "<small>" + " hr " + "</small>" + minutes + "<small>" + " min " + "</small>"));
                } else if (Integer.parseInt(scheduleList.get(i).getHeading_time()) % 60 == 0) {
                    int hours = Integer.parseInt(scheduleList.get(i).getHeading_time()) / 60;
//                  tvheadingDuration.setText(hours + " hrs");
                    tvheadingDuration.setText(Html.fromHtml(hours + "<small>" + " hrs" + "</small>"));
                } else {
//                  tvheadingDuration.setText(scheduleList.get(i).getHeading_time() + " min");
                    tvheadingDuration.setText(Html.fromHtml(scheduleList.get(i).getHeading_time() + "<small>" + " min" + "</small>"));
                }
                TextView tvAppointmentDuration = (TextView) view.findViewById(R.id.tvAppointmentDuration);
                if (Integer.parseInt(scheduleList.get(i).getWork_duration()) > 60
                        && Integer.parseInt(scheduleList.get(i).getWork_duration()) % 60 != 0) {
                    int hours = Integer.parseInt(scheduleList.get(i).getWork_duration()) / 60; //since both are ints, you get an int
                    int minutes = Integer.parseInt(scheduleList.get(i).getWork_duration()) % 60;
//                  tvAppointmentDuration.setText(hours + " hr " + minutes + " min");
                    tvAppointmentDuration.setText(Html.fromHtml(hours + "<small>" + " hr " + "</small>" + minutes + "<small>" + " min " + "</small>"));
                } else if (Integer.parseInt(scheduleList.get(i).getWork_duration()) % 60 == 0) {
                    int hours = Integer.parseInt(scheduleList.get(i).getWork_duration()) / 60;
//                  tvAppointmentDuration.setText(hours + " hrs");
                    tvAppointmentDuration.setText(Html.fromHtml(hours + "<small>" + " hrs" + "</small>"));
                } else {
//                  tvAppointmentDuration.setText(scheduleList.get(i).getWork_duration() + " min");
                    tvAppointmentDuration.setText(Html.fromHtml(scheduleList.get(i).getWork_duration() + "<small>" + " min" + "</small>"));
                }
                View viewHeadingLine = (View) view.findViewById(R.id.viewHeadingLine);
                if (Integer.parseInt(scheduleList.get(i).getHeading_time()) > 20) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                            viewHeadingLine.getLayoutParams();
                    params.height = Integer.parseInt(scheduleList.get(i).getHeading_time()) * 4;
                    viewHeadingLine.setLayoutParams(params);
                }
                ImageView imvEditAppointment = (ImageView) view.findViewById(R.id.imvEditAppointment);

                ImageView imvEditHeadingTime = (ImageView) view.findViewById(R.id.imvEditHeadingTime);
                imvEditHeadingTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        System.out.println("NAME C:" + scheduleList.get(pos).getService_id());
                    }
                });
                TextView tvViewDetails = (TextView) view.findViewById(R.id.tvViewDetails);
                tvViewDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // redirect to details page
                        System.out.println("SER ID:" + scheduleList.get(pos).getService_id());
                        Intent intent = new Intent(getActivity(), JobDetailsActivity.class);
                        intent.putExtra("serviceId", scheduleList.get(pos).getService_id());
                        intent.putExtra("isEditable", false);
                        startActivity(intent);


                    }
                });

                RelativeLayout rlDuration = (RelativeLayout) view.findViewById(R.id.rlDuration);
                rlDuration.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showCustomEditDurationDialog(getActivity(),
                                scheduleList.get(pos).getService_id(), scheduleList.get(pos).getWork_duration());
                    }
                });


            } else if (scheduleList.get(i).getType().equalsIgnoreCase("unavailable")) {
                view = layoutInflater.inflate(R.layout.item_unavailable_schedule, null);
                TextView tvUnavailableTime = (TextView) view.findViewById(R.id.tvUnavailableTime);
//                tvUnavailableTime.setText(dateFormate(scheduleList.get(i).getFrom_time(), "yyyy-mm-dd hh:mm:ss"));
                String str = dateFormate(scheduleList.get(i).getFrom_time(), "yyyy-mm-dd hh:mm:ss");
                tvUnavailableTime.setText(Html.fromHtml(str.substring(0, str.length() - 2) + "<small>" +
                        str.substring(str.length() - 2) + "</small>"));
                View viewUnavailableLine = (View) view.findViewById(R.id.viewUnavailableLine);
                if (scheduleList.get(i).getDuration() > 20
                        && scheduleList.get(i).getDuration() <= 150) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                            viewUnavailableLine.getLayoutParams();
                    params.height = scheduleList.get(i).getDuration() * 4;
                    viewUnavailableLine.setLayoutParams(params);
                } else if (scheduleList.get(i).getDuration() > 150) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                            viewUnavailableLine.getLayoutParams();
                    params.height = scheduleList.get(i).getDuration() * 2;
                    viewUnavailableLine.setLayoutParams(params);
                }
                TextView tvUnavailableDuration = (TextView) view.findViewById(R.id.tvUnavailableDuration);
                if (scheduleList.get(i).getDuration() > 60
                        && scheduleList.get(i).getDuration() % 60 != 0) {
                    int hours = scheduleList.get(i).getDuration() / 60; //since both are ints, you get an int
                    int minutes = scheduleList.get(i).getDuration() % 60;
                    //  tvUnavailableDuration.setText(hours + " hr " + minutes + " min");
                    tvUnavailableDuration.setText(Html.fromHtml(hours + "<small>" + " hr " + "</small>" + minutes + "<small>" + " min " + "</small>"));
                } else if (scheduleList.get(i).getDuration() % 60 == 0) {
                    int hours = scheduleList.get(i).getDuration() / 60;
                    //  tvUnavailableDuration.setText(hours + " hrs");
                    tvUnavailableDuration.setText(Html.fromHtml(hours + "<small>" + " hrs" + "</small>"));
                } else {
                    // tvUnavailableDuration.setText(scheduleList.get(i).getDuration() + " min");
                    tvUnavailableDuration.setText(Html.fromHtml(scheduleList.get(i).getDuration() + "<small>" + " min" + "</small>"));
                }
                ImageView imvEditUnavailable = (ImageView) view.findViewById(R.id.imvEditUnavailable);


                RelativeLayout rlBreaktime = (RelativeLayout) view.findViewById(R.id.rlBreaktime);
                rlBreaktime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getActivity(), EditAvailitysetupActivity.class));
                    }
                });
            }


            if (view != null) {
                view.setId(i);
                llScheduleContainer.addView(view);
            }
        }
        View view1 = layoutInflater.inflate(R.layout.end_layout, null);/**/
        ImageView imvEndImageCircle = (ImageView) view1.findViewById(R.id.imvEndImageCircle);

        if (scheduleList.get(scheduleList.size() - 1).getType().equalsIgnoreCase("free")) {
            imvEndImageCircle.setImageResource(R.drawable.darkgreen_circle_bg);
        } else if (scheduleList.get(scheduleList.size() - 1).getType().equalsIgnoreCase("booked")) {
            imvEndImageCircle.setImageResource(R.drawable.darkred_circle_bg);
        } else if (scheduleList.get(scheduleList.size() - 1).getType().equalsIgnoreCase("unavailable")) {
            imvEndImageCircle.setImageResource(R.drawable.gray_circle_bg);
        }
        llScheduleContainer.addView(view1);
    }

    private String dateFormate(String from_time, String s) {
        DateFormat f1 = new SimpleDateFormat(s, Locale.ENGLISH); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(from_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mma", Locale.ENGLISH);
        return f2.format(d).toUpperCase();
    }

    @Override
    public void onPause() {
        super.onPause();
        mQueue.cancelAll(TAG);
    }

    public void showCustomEditDurationDialog(Context context, final String serviceid, final String duration) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog.setContentView(R.layout.edit_duration_dialog);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);
        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);

        final TextView tvHR = (TextView) dialog.findViewById(R.id.tvHR);
        final TextView tvMIN = (TextView) dialog.findViewById(R.id.tvMIN);
        if (Integer.parseInt(duration) > 60
                && Integer.parseInt(duration) % 60 != 0) {
            int hours = Integer.parseInt(duration) / 60; //since both are ints, you get an int
            int minutes = Integer.parseInt(duration) % 60;
            tvHR.setText("" + hours);
            tvMIN.setText("" + minutes);

        } else if (Integer.parseInt(duration) % 60 == 0) {
            int hours = Integer.parseInt(duration) / 60;
            tvHR.setText("" + hours);
            tvMIN.setText("0");
        } else {
            tvHR.setText("0");
            tvMIN.setText(duration);
        }
        tvHR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberPicker(getActivity(), Integer.parseInt(tvHR.getText().toString()), 23, tvHR);
            }
        });
        tvMIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberPicker(getActivity(), Integer.parseInt(tvMIN.getText().toString()), 59, tvMIN);
            }
        });
        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int updated_duration = (Integer.parseInt(tvHR.getText().toString()) * 60)
                        + Integer.parseInt(tvMIN.getText().toString());
                if (updated_duration != Integer.parseInt(duration)) {
                    //call web service
                    updateWorkDuration(serviceid, String.valueOf(updated_duration));
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void updateWorkDuration(final String serviceid, final String updatedDuration) {
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SCHEDULE_UPDATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        Log.e("fbLogin", response);
                        try {
                            System.out.println("result = " + response);
                            JSONObject Obj = new JSONObject(response);
                            if (Obj.optString("success").equals("0")) {
                                getScheduleData();
                            } else if (Obj.optString("success").equals("1")) {
                                pref.clearAllPref();
                                startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                            } else {
                                str_message = Obj.optString("msg");
                                dialogview.showCustomSingleButtonDialog(getActivity(), getResources().getString(R.string.sorry), str_message);
                            }
                        } catch (JSONException e) {

                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), "JSON error");

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), "Timeout Error");
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), "Network error");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("service_id", serviceid);
                params.put("duration", updatedDuration);
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    /*public void showCustomEditBreakDialog(final Context context, String from_time, String to_time) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog.setContentView(R.layout.edit_breaktime_dialog);

//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        final TextView tvStartTime = (TextView) dialog.findViewById(R.id.tvStartTime);
        final TextView tvStartPM = (TextView) dialog.findViewById(R.id.tvStartPM);
        final TextView tvStartAM = (TextView) dialog.findViewById(R.id.tvStartAM);

        final TextView tvEndTime = (TextView) dialog.findViewById(R.id.tvEndTime);
        final TextView tvEndPM = (TextView) dialog.findViewById(R.id.tvEndPM);
        final TextView tvEndAM = (TextView) dialog.findViewById(R.id.tvEndAM);

        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);
        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);

        if (from_time.substring(from_time.length() - 2).equalsIgnoreCase("PM")) {
            tvStartPM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
            tvStartPM.setTextColor(getResources().getColor(R.color.white));
        } else if (from_time.substring(from_time.length() - 2).equalsIgnoreCase("AM")) {
            tvStartAM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
            tvStartAM.setTextColor(getResources().getColor(R.color.white));
        }
        tvStartTime.setText(from_time.substring(0, from_time.length() - 2));
        str_START = from_time;


        if (to_time.substring(from_time.length() - 2).equalsIgnoreCase("PM")) {
            tvEndPM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
            tvEndPM.setTextColor(getResources().getColor(R.color.white));
        } else if (to_time.substring(from_time.length() - 2).equalsIgnoreCase("AM")) {
            tvEndAM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
            tvEndAM.setTextColor(getResources().getColor(R.color.white));
        }
        tvEndTime.setText(to_time.substring(0, to_time.length() - 2));
        str_END = to_time;

        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(getActivity(), "Select Start Time", str_START, tvStartTime, tvStartPM, tvStartAM);
            }
        });
        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker(getActivity(), "Select End Time", str_END, tvEndTime, tvEndPM, tvEndAM);
            }
        });
        tvStartPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvStartPM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                tvStartAM.setBackgroundResource(R.drawable.full_white_rounded_bg);
                tvStartPM.setTextColor(getResources().getColor(R.color.white));
                tvStartAM.setTextColor(getResources().getColor(R.color.black));
            }
        });
        tvStartAM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvStartAM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                tvStartPM.setBackgroundResource(R.drawable.full_white_rounded_bg);
                tvStartPM.setTextColor(getResources().getColor(R.color.black));
                tvStartAM.setTextColor(getResources().getColor(R.color.white));
            }
        });
        tvEndPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvEndPM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                tvEndAM.setBackgroundResource(R.drawable.full_white_rounded_bg);
                tvEndPM.setTextColor(getResources().getColor(R.color.white));
                tvEndAM.setTextColor(getResources().getColor(R.color.black));
            }
        });
        tvEndAM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvEndAM.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                tvEndPM.setBackgroundResource(R.drawable.full_white_rounded_bg);
                tvEndPM.setTextColor(getResources().getColor(R.color.black));
                tvEndAM.setTextColor(getResources().getColor(R.color.white));
            }
        });
        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }*/

    private void showNumberPicker(Context context, int presetValue, int totalValue, final TextView textView) {
        final Dialog npdialog = new Dialog(context);
        // to remove the dialog title...
        npdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        npdialog.setContentView(R.layout.number_picker_dialog);
        npdialog.setCanceledOnTouchOutside(false);
        npdialog.setCancelable(false);
        // dialog.setTitle("Contacts");
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(npdialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        npdialog.show();
        npdialog.getWindow().setAttributes(lp);
        npdialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        final NumberPicker np = (NumberPicker) npdialog
                .findViewById(R.id.numberPicker);
        np.setMinValue(0);
        np.setMaxValue(totalValue);
        np.setValue(presetValue);
        np.setWrapSelectorWheel(false);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        Button btnCancel = (Button) npdialog
                .findViewById(R.id.btnCancel);
        Button btnDone = (Button) npdialog
                .findViewById(R.id.btnDone);

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker,
                                      int oldVal, int newVal) {
                // TODO Auto-generated method stub
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                npdialog.dismiss();
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("V:" + np.getValue());
                textView.setText("" + np.getValue());
                npdialog.dismiss();
            }
        });

    }

    private void showTimePicker(Context context, final String title, String time,
                                final TextView setTime, final TextView pm, final TextView am) {
        SimpleDateFormat sdf = new SimpleDateFormat("h:mma", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String format;
                if (selectedHour == 0) {
                    selectedHour += 12;
                    format = "AM";
                } else if (selectedHour == 12) {
                    format = "PM";
                } else if (selectedHour > 12) {
                    selectedHour -= 12;
                    format = "PM";
                } else {
                    format = "AM";
                }
                String min = "";
                if (selectedMinute < 10)
                    min = "0" + selectedMinute;
                else
                    min = String.valueOf(selectedMinute);
                setTime.setText(selectedHour + ":" + min);
                if (format.equalsIgnoreCase("AM")) {
                    am.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                    am.setTextColor(getResources().getColor(R.color.white));
                    pm.setBackgroundResource(R.drawable.full_white_rounded_bg);
                    pm.setTextColor(getResources().getColor(R.color.black));
                } else if (format.equalsIgnoreCase("PM")) {
                    pm.setBackgroundResource(R.drawable.full_darkred_rounded_bg);
                    pm.setTextColor(getResources().getColor(R.color.white));
                    am.setBackgroundResource(R.drawable.full_white_rounded_bg);
                    am.setTextColor(getResources().getColor(R.color.black));
                }
                if (title.equalsIgnoreCase("Select Start Time"))
                    str_START = selectedHour + ":" + min + format;
                if (title.equalsIgnoreCase("Select End Time"))
                    str_END = selectedHour + ":" + min + format;

            }
        }, hour, minute, false);//Yes 12 hour time
        mTimePicker.setTitle(title);
        mTimePicker.show();
    }

    private long timeDifferenceInMinutes(String from_time, String to_time, String s) {
        // Custom date format
        SimpleDateFormat format = new SimpleDateFormat(s, Locale.ENGLISH);
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(from_time);
            d2 = format.parse(to_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Get msec from each, and subtract.
        long diff = d2.getTime() - d1.getTime();
        return diff / (60 * 1000) % 60;
    }

}
