package com.uipl.qberprovider.model;

/**
 * Created by abhijit on 16/12/16.
 */
public class Schedule {

    private String from_time;
    private String to_time;
    private String type;
    private String duration;
    private String free_id;
    private String work_start_time;
    private String work_end_time;
    private String work_duration;
    private String heading_time;
    private String service_id;
    private String job_id;
    private String shorttime;
    private String current_work_start_time;

    public Schedule() {
            super();
    }

    public Schedule(String from_time, String to_time, String type, String duration, String free_id, String work_start_time, String work_end_time, String work_duration, String heading_time, String service_id, String job_id, String shorttime, String current_work_start_time) {
        this.from_time = from_time;
        this.to_time = to_time;
        this.type = type;
        this.duration = duration;
        this.free_id = free_id;
        this.work_start_time = work_start_time;
        this.work_end_time = work_end_time;
        this.work_duration = work_duration;
        this.heading_time = heading_time;
        this.service_id = service_id;
        this.job_id = job_id;
        this.shorttime = shorttime;
        this.current_work_start_time = current_work_start_time;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDuration() {
        Double du=Double.parseDouble(duration);
        return du.intValue();
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFree_id() {
        return free_id;
    }

    public void setFree_id(String free_id) {
        this.free_id = free_id;
    }

    public String getWork_start_time() {
        return work_start_time;
    }

    public void setWork_start_time(String work_start_time) {
        this.work_start_time = work_start_time;
    }

    public String getWork_end_time() {
        return work_end_time;
    }

    public void setWork_end_time(String work_end_time) {
        this.work_end_time = work_end_time;
    }

    public String getWork_duration() {
        return work_duration;
    }

    public void setWork_duration(String work_duration) {
        this.work_duration = work_duration;
    }

    public String getHeading_time() {
        return heading_time;
    }

    public void setHeading_time(String heading_time) {
        this.heading_time = heading_time;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getShorttime() {
        return shorttime;
    }

    public void setShorttime(String shorttime) {
        this.shorttime = shorttime;
    }

    public String getCurrent_work_start_time() {
        return current_work_start_time;
    }

    public void setCurrent_work_start_time(String current_work_start_time) {
        this.current_work_start_time = current_work_start_time;
    }
}
