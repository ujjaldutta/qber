package com.uipl.qberprovider.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uipl.qberprovider.R;
import com.uipl.qberprovider.model.ServiceSetting;
import com.uipl.qberprovider.utils.DialogView;

import java.util.ArrayList;

/**
 * Created by pallab on 14/11/16.
 */

public class JobLocationAdapter extends BaseAdapter implements View.OnClickListener {
    LayoutInflater inflater;
    DialogView dialogView;
    private ArrayList<ServiceSetting> jobLocationArray;
    private Activity mActivity;

    public JobLocationAdapter(Activity activity, ArrayList<ServiceSetting> jobLocationArray) {
        this.mActivity = activity;
        this.jobLocationArray = jobLocationArray;
        this.inflater = LayoutInflater.from(activity);
        this.dialogView = new DialogView(activity);
    }

    @Override
    public int getCount() {
        return jobLocationArray.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.inflate_job_location, viewGroup, false);
        }
        TextView tvSLNo = (TextView) view.findViewById(R.id.tvSLNo);
        tvSLNo.setText(String.valueOf(i + 1));
        ImageView imgNumber = (ImageView) view.findViewById(R.id.imgNumber);
        ImageView imgDesc = (ImageView) view.findViewById(R.id.imgDesc);
        if (jobLocationArray.get(i).isSelected()) {
            imgNumber.setImageResource(R.drawable.tick_5);
        } else {
            imgNumber.setImageResource(R.drawable.circle_2);
        }
        imgDesc.setTag(i);
        imgNumber.setTag(i);
        imgNumber.setOnClickListener(this);
        imgDesc.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        int pos = (int) view.getTag();
        switch (view.getId()) {
            case R.id.imgNumber:
                if (pos > -1 && pos < jobLocationArray.size()) {
                    if (jobLocationArray.get(pos).isSelected()) {
                        jobLocationArray.get(pos).getPolygon().setStrokeColor(mActivity.getResources().getColor(R.color.light_green));
                        jobLocationArray.get(pos).getPolygon().setFillColor(mActivity.getResources().getColor(R.color.light_green_transparent));
                        jobLocationArray.get(pos).setSelected(false);
                    } else {
                        jobLocationArray.get(pos).getPolygon().setStrokeColor(mActivity.getResources().getColor(R.color.black));
                        jobLocationArray.get(pos).getPolygon().setFillColor(mActivity.getResources().getColor(R.color.grey_transparent));
                        jobLocationArray.get(pos).setSelected(true);

                    }
                    this.notifyDataSetChanged();
                }
                break;
            case R.id.imgDesc:
                dialogView.showCustomSingleButtonDialog(mActivity, mActivity.getResources().getString(R.string.app_name), "coming soon");
                break;
        }
    }
}
