package com.uipl.qberprovider.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.uipl.qberprovider.R;


public class DialogView {


    public Activity _activity;
    Preferences pref;

    public static Dialog customSpinProgress;


    public DialogView(Activity activity){
        this._activity = activity;
        pref = new Preferences(_activity);
    }
    public void showCustomSingleButtonDialog(Context context, String header,
                                       String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(header);
        adb.setMessage(msg);
        adb.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();


            }
        });

        AlertDialog alert = adb.create();
        alert.show();
    }

    /*public void showCustomSingleButtonDialog(Context context, String header, String body) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog.setContentView(R.layout.custom_single_button_dialog);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

//        Typeface light = Typeface.createFromAsset(context.getAssets(),
//                "QUICKSAND_LIGHT.OTF");
//        Typeface normal = Typeface.createFromAsset(context.getAssets(), "QUICKSAND_BOOK.OTF");
//        Typeface bold = Typeface.createFromAsset(context.getAssets(),
//                "lato_regular.ttf");

        RelativeLayout rl_dialogOK = (RelativeLayout) dialog
                .findViewById(R.id.rl_dialogOK);

        TextView tv_header = (TextView) dialog.findViewById(R.id.tv_header);
//        tv_header.setTypeface(bold);
        TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
//        tv_message.setTypeface(normal);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
//        tv_OKtext.setTypeface(normal);



        tv_header.setText(header);
        tv_message.setText(body);

        btn_ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.show();

    }*/

    public void showCustomSpinProgress(Context context) {

        customSpinProgress = new Dialog(context);
        customSpinProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customSpinProgress.setContentView(R.layout.dialog_loading);
        customSpinProgress.setCanceledOnTouchOutside(false);
        customSpinProgress.setCancelable(true);
        customSpinProgress.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        WindowManager.LayoutParams wlmp = customSpinProgress.getWindow()
                .getAttributes();
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.height = WindowManager.LayoutParams.MATCH_PARENT;
        customSpinProgress.show();
    }

    public void dismissCustomSpinProgress() {
        if(_activity!=null && customSpinProgress!=null) {
            if (customSpinProgress.isShowing())
                customSpinProgress.dismiss();
        }
    }


//    public void showCustomSpinProgressWithTitle(Context context) {
//
//        customSpinProgress = new Dialog(context);
//        customSpinProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        customSpinProgress.setContentView(R.layout.dialog_loading_title);
//
//        customSpinProgress.setCanceledOnTouchOutside(false);
//        customSpinProgress.setCancelable(true);
//        customSpinProgress.getWindow().setBackgroundDrawableResource(
//               android.R.color.transparent);
//        WindowManager.LayoutParams wlmp = customSpinProgress.getWindow()
//                .getAttributes();
//        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        wlmp.height = WindowManager.LayoutParams.MATCH_PARENT;
//        customSpinProgress.show();
//    }

//    public void dismissCustomSpinProgressWithTitle() {
//        if (customSpinProgress.isShowing())
//            customSpinProgress.dismiss();
//    }






    public void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
//    public void showCustomToast(Context context, String msg){
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View toastRoot = inflater.inflate(R.layout.toast, null);
//        TextView tv_toast = (TextView) toastRoot.findViewById(R.id.tv_toast);
//        tv_toast.setText(msg);
//        Typeface bold = Typeface.createFromAsset(context.getAssets(),
//                "lato_regular.ttf");
//        tv_toast.setTypeface(bold);
//        Toast toast = new Toast(context);
//        toast.setView(toastRoot);
//        toast.setGravity(Gravity.BOTTOM, 0, 50);
//        toast.setDuration(Toast.LENGTH_SHORT);
//        toast.show();
//    }

}
