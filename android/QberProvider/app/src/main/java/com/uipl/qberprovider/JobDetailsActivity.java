package com.uipl.qberprovider;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.Constants;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.JobDetails;
import com.uipl.qberprovider.model.Reason;
import com.uipl.qberprovider.model.WorkHeadingDetails;
import com.uipl.qberprovider.model.WorkSubHeadingDetails;
import com.uipl.qberprovider.utils.DateFormatter;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class JobDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "JobDetailsActivity";

    DialogView dialogview;
    RequestQueue mQueue;
    Preferences pref;

    ImageButton imgBtnBack;
    TextView tvLocation, tvDate, tvJobId, tvCustomerName, tvRequestTime, tvRequestAmPm, tvConfirmTime, tvConfirmAmPm, tvHeadingTime, tvHeadingAmPm, tvStartTime,
            tvStartAmPm, tvCompleteTime, tvCompleteAmPm, tvSuvCount, tvSedanCount, tvSuv, tvSedan;
    Button btnCancelJob;
    ImageView ivEdit;
    LinearLayout llAddableSuv, llAddableSedan;
    ScrollView scrollView;
    Spinner spinnerSuv, spinnerSedan;

    String serviceId = "";
    boolean isWorkDetailsEditMode = false;
    double suvHeadingPrice = 0, sedanHeadingPrice = 0;
    int totalSuvCount = 0, totalSedanCount = 0;

    ArrayList<WorkHeadingDetails> suvArrList = new ArrayList<>();
    ArrayList<WorkHeadingDetails> sedanArrList = new ArrayList<>();
    ArrayList<Reason> reasonArrList = new ArrayList<>();
    final ArrayList<WorkSubHeadingDetails> suvSubDetailsList = new ArrayList<>();
    final ArrayList<WorkSubHeadingDetails> sedanSubDetailsList = new ArrayList<>();

    boolean isEditable = false, isEdited = false, enableSuvSpinner = false, enableSedanSpinner = false, enableSuvToggleButtons = false, enableSedanToggleButtons = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);

        initViews();

        serviceId = getIntent().getStringExtra("serviceId");
        isEditable = getIntent().getBooleanExtra("isEditable", false);

        if (isEditable == true) {
            btnCancelJob.setVisibility(View.VISIBLE);
            ivEdit.setVisibility(View.VISIBLE);
        } else {
            btnCancelJob.setVisibility(View.GONE);
            ivEdit.setVisibility(View.GONE);
        }
        dialogview.showCustomSpinProgress(this);
        volleyGetJobDetails(pref.getStringPreference(this, Preferences.PHONE), serviceId);
    }

    @Override
    public void onPause() {
        super.onPause();
        mQueue.cancelAll(TAG);
    }

    private void initViews() {
        dialogview = new DialogView(this);
        pref = new Preferences(this);
        mQueue = Volley.newRequestQueue(this);

        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvJobId = (TextView) findViewById(R.id.tvJobId);
        tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
        tvRequestTime = (TextView) findViewById(R.id.tvRequestTime);
        tvRequestAmPm = (TextView) findViewById(R.id.tvRequestAmPm);
        tvConfirmTime = (TextView) findViewById(R.id.tvConfirmTime);
        tvConfirmAmPm = (TextView) findViewById(R.id.tvConfirmAmPm);
        tvHeadingTime = (TextView) findViewById(R.id.tvHeadingTime);
        tvHeadingAmPm = (TextView) findViewById(R.id.tvHeadingAmPm);
        tvStartTime = (TextView) findViewById(R.id.tvStartTime);
        tvStartAmPm = (TextView) findViewById(R.id.tvStartAmPm);
        tvCompleteTime = (TextView) findViewById(R.id.tvCompleteTime);
        tvCompleteAmPm = (TextView) findViewById(R.id.tvCompleteAmPm);
        tvSuvCount = (TextView) findViewById(R.id.tvSuvCount);
        tvSedanCount = (TextView) findViewById(R.id.tvSedanCount);
        tvSuv = (TextView) findViewById(R.id.tvSuv);
        tvSedan = (TextView) findViewById(R.id.tvSedan);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        btnCancelJob = (Button) findViewById(R.id.btnCancelJob);
        ivEdit = (ImageView) findViewById(R.id.ivEdit);
        llAddableSuv = (LinearLayout) findViewById(R.id.llAddableSuv);
        llAddableSedan = (LinearLayout) findViewById(R.id.llAddableSedan);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        spinnerSuv = (Spinner) findViewById(R.id.spinnerSuv);
        spinnerSedan = (Spinner) findViewById(R.id.spinnerSedan);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.spinnerCountArray));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSuv.setAdapter(spinnerArrayAdapter);
        spinnerSedan.setAdapter(spinnerArrayAdapter);

        for (int k = 0; k < 3; k++) {
            WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
            if (k == 0) {
                workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.pressureSopWashId);
                workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.pressure_soap_wash));
                workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.pressureSopWashPrice);
            } else if (k == 1) {
                workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.interiorVaccumCleaningId);
                workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.interior_vaccum_cleaning));
                workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.interiorVaccumCleaningPrice);
            } else {
                workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.tyreShiningId);
                workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.tyre_shining));
                workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.tyreShiningPrice);
            }
            workSubHeadingDetails.setSelected(true);
            workSubHeadingDetails.setServiceOptionId(Constants.typeId.carWashType);
            suvSubDetailsList.add(workSubHeadingDetails);
        }

        for (int k = 0; k < 3; k++) {
            WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
            if (k == 0) {
                workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.pressureSopWashId);
                workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.pressure_soap_wash));
                workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.pressureSopWashPrice);
            } else if (k == 1) {
                workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.interiorVaccumCleaningId);
                workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.interior_vaccum_cleaning));
                workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.interiorVaccumCleaningPrice);
            } else {
                workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.tyreShiningId);
                workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.tyre_shining));
                workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.tyreShiningPrice);
            }
            workSubHeadingDetails.setSelected(true);
            workSubHeadingDetails.setServiceOptionId(Constants.typeId.carWashType);
            sedanSubDetailsList.add(workSubHeadingDetails);
        }

        spinnerSuv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setText("");
                if (enableSuvSpinner) {
                    tvSuvCount.setText(adapterView.getSelectedItem().toString());
                    totalSuvCount = Integer.parseInt(adapterView.getSelectedItem().toString());
                    if (totalSuvCount <= 0) {
                        suvArrList.clear();
                        llAddableSuv.removeAllViews();
                    } else {
                        int size = suvArrList.size();
                        if (totalSuvCount < size) {
                            do {
                                suvArrList.remove(totalSuvCount);
                                llAddableSuv.removeViewAt(totalSuvCount);
                            } while (suvArrList.size() != totalSuvCount);
                        } else if (totalSuvCount > size) {
                            if (isWorkDetailsEditMode) {
                                dialogview.showCustomSpinProgress(JobDetailsActivity.this);
                            }
                            int totalChildCount = llAddableSuv.getChildCount();
                            ArrayList<WorkSubHeadingDetails> tempArr = new ArrayList<WorkSubHeadingDetails>();
                            for (int k = 0; k < 3; k++) {
                                WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                if (k == 0) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.pressureSopWashId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.pressure_soap_wash));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.pressureSopWashPrice);
                                } else if (k == 1) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.interiorVaccumCleaningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.interior_vaccum_cleaning));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.interiorVaccumCleaningPrice);
                                } else {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.tyreShiningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.tyre_shining));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.tyreShiningPrice);
                                }
                                workSubHeadingDetails.setSelected(true);
                                workSubHeadingDetails.setServiceOptionId(Constants.typeId.carWashType);
                                tempArr.add(workSubHeadingDetails);
                            }
                            for (int j = totalChildCount; j < totalSuvCount; j++) {
                                WorkHeadingDetails workHeadingDetails = new WorkHeadingDetails();
                                String name = getResources().getString(R.string.suv) + (j + 1);
                                workHeadingDetails.setWorkHeadingName(name);
                                workHeadingDetails.setSubHeadingDetailsArrayList(tempArr);
                                suvArrList.add(workHeadingDetails);
                            }
                            inflateSuvDetails();
                        } else {
                            if (isWorkDetailsEditMode) {
                                dialogview.showCustomSpinProgress(JobDetailsActivity.this);
                            }
                            inflateSuvDetails();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSedan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setText("");
                if (enableSedanSpinner) {
                    tvSedanCount.setText(adapterView.getSelectedItem().toString());
                    totalSedanCount = Integer.parseInt(adapterView.getSelectedItem().toString());
                    if (totalSedanCount <= 0) {
                        sedanArrList.clear();
                        llAddableSedan.removeAllViews();
                    } else {
                        int size = sedanArrList.size();
                        if (totalSedanCount < size) {
                            do {
                                sedanArrList.remove(totalSedanCount);
                                llAddableSedan.removeViewAt(totalSedanCount);
                            } while (sedanArrList.size() != totalSedanCount);
                        } else if (totalSedanCount > size) {
                            if (isWorkDetailsEditMode) {
                                dialogview.showCustomSpinProgress(JobDetailsActivity.this);
                            }
                            int totalChildCount = llAddableSedan.getChildCount();
                            ArrayList<WorkSubHeadingDetails> tempArr = new ArrayList<WorkSubHeadingDetails>();
                            for (int k = 0; k < 3; k++) {
                                WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                if (k == 0) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.pressureSopWashId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.pressure_soap_wash));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.pressureSopWashPrice);
                                } else if (k == 1) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.interiorVaccumCleaningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.interior_vaccum_cleaning));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.interiorVaccumCleaningPrice);
                                } else {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.tyreShiningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.tyre_shining));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.tyreShiningPrice);
                                }
                                workSubHeadingDetails.setSelected(true);
                                workSubHeadingDetails.setServiceOptionId(Constants.typeId.carWashType);
                                tempArr.add(workSubHeadingDetails);
                            }
                            for (int j = totalChildCount; j < totalSedanCount; j++) {
                                WorkHeadingDetails workHeadingDetails = new WorkHeadingDetails();
                                String name = getResources().getString(R.string.sedan) + (j + 1);
                                workHeadingDetails.setWorkHeadingName(name);
                                workHeadingDetails.setSubHeadingDetailsArrayList(tempArr);
                                sedanArrList.add(workHeadingDetails);
                            }
                            inflateSedanDetails();
                        } else {
                            if (isWorkDetailsEditMode) {
                                dialogview.showCustomSpinProgress(JobDetailsActivity.this);
                            }
                            inflateSedanDetails();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSuv.setEnabled(false);
        spinnerSedan.setEnabled(false);

        btnCancelJob.setOnClickListener(this);
        imgBtnBack.setOnClickListener(this);
        ivEdit.setOnClickListener(this);
    }

    private void volleyGetJobDetails(final String mobile, final String serviceId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_JOB_DETAILS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            suvArrList.clear();
                            sedanArrList.clear();

                            ArrayList<WorkSubHeadingDetails> tempSuvList = new ArrayList<>();
                            for (int k = 0; k < 3; k++) {
                                WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                if (k == 0) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.pressureSopWashId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.pressure_soap_wash));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.pressureSopWashPrice);
                                } else if (k == 1) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.interiorVaccumCleaningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.interior_vaccum_cleaning));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.interiorVaccumCleaningPrice);
                                } else {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.suvDetailId.tyreShiningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.tyre_shining));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.suvDetailPrice.tyreShiningPrice);
                                }
                                workSubHeadingDetails.setSelected(false);
                                workSubHeadingDetails.setServiceOptionId(Constants.typeId.carWashType);
                                tempSuvList.add(workSubHeadingDetails);
                            }

                            ArrayList<WorkSubHeadingDetails> tempSedanList = new ArrayList<>();
                            for (int k = 0; k < 3; k++) {
                                WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                if (k == 0) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.pressureSopWashId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.pressure_soap_wash));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.pressureSopWashPrice);
                                } else if (k == 1) {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.interiorVaccumCleaningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.interior_vaccum_cleaning));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.interiorVaccumCleaningPrice);
                                } else {
                                    workSubHeadingDetails.setWorkSubHeadingId(Constants.sedanDetailId.tyreShiningId);
                                    workSubHeadingDetails.setWorkSubHeadingName(getResources().getString(R.string.tyre_shining));
                                    workSubHeadingDetails.setWorkSubHeadingPrice(Constants.sedanDetailPrice.tyreShiningPrice);
                                }
                                workSubHeadingDetails.setSelected(false);
                                workSubHeadingDetails.setServiceOptionId(Constants.typeId.carWashType);
                                tempSedanList.add(workSubHeadingDetails);
                            }

                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JobDetails jobDetails = new JobDetails();

                                    JSONObject dataJsonObj = responseJsonObj.optJSONObject("data");
                                    jobDetails.setCustomerName(dataJsonObj.optString("customer_name"));
                                    jobDetails.setCustomerPhoneNumber(dataJsonObj.optString("customer_phone"));
                                    jobDetails.setJobId(dataJsonObj.optString("job_id"));
                                    jobDetails.setLocation(dataJsonObj.optString("address"));
                                    String requestTime = dataJsonObj.optString("request_date");
                                    String headingTime = dataJsonObj.optString("heading_at");
                                    String startTime = dataJsonObj.optString("started_at");
                                    String confirmTime = dataJsonObj.optString("confirmed_at");
                                    String completeTime = dataJsonObj.optString("completed_at");
                                    jobDetails.setRequestTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", requestTime));
                                    jobDetails.setHeadingTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", headingTime));
                                    jobDetails.setStartTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", startTime));
                                    jobDetails.setConfirmTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", confirmTime));
                                    jobDetails.setCompleteTime(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "hh:mm a", completeTime));
                                    jobDetails.setDate(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "dd MMMM", requestTime));

                                    JSONObject carDetailsJsonObj = dataJsonObj.optJSONObject("cardetails");

                                    if (carDetailsJsonObj != null && carDetailsJsonObj.length() > 0) {
                                        JSONArray nameArr = carDetailsJsonObj.names();
                                        for (int k = 0; k < nameArr.length(); k++) {
                                            Log.e(TAG, nameArr.opt(k).toString());
                                        }
                                        for (int j = 0; j < carDetailsJsonObj.length(); j++) {
                                            WorkHeadingDetails workHeadingDetails = new WorkHeadingDetails();
                                            String arrName = nameArr.opt(j).toString();
                                            JSONArray carJsonArr = carDetailsJsonObj.optJSONArray(arrName);
                                            ArrayList<WorkSubHeadingDetails> workSubHeadingDetailsArrayList = new ArrayList<>();
                                            int length = arrName.length();
                                            if (length > 0) {
                                                arrName = arrName.substring(0, length - 1) + " " + arrName.charAt(length - 1);
                                                workHeadingDetails.setWorkHeadingName(arrName);
                                                if (length == 4) {
                                                    workSubHeadingDetailsArrayList.addAll(tempSuvList);
                                                } else {
                                                    workSubHeadingDetailsArrayList.addAll(tempSedanList);
                                                }
                                                for (int k = 0; k < carJsonArr.length(); k++) {
                                                    JSONArray jsonArr = carJsonArr.optJSONArray(k);
                                                    JSONObject jsonObj = jsonArr.optJSONObject(0);
                                                    WorkSubHeadingDetails workSubHeadingDetails = new WorkSubHeadingDetails();
                                                    String id = jsonObj.optString("id");
                                                    workSubHeadingDetails.setWorkSubHeadingId(id);
                                                    workSubHeadingDetails.setWorkSubHeadingName(jsonObj.optString("name"));
                                                    workSubHeadingDetails.setServiceOptionId(jsonObj.optString("service_option_id"));
                                                    workSubHeadingDetails.setWorkSubHeadingPrice(Double.parseDouble(jsonObj.optString("cost")));
                                                    workSubHeadingDetails.setSelected(true);
                                                    if (id.equals(Constants.suvDetailId.pressureSopWashId) || id.equals(Constants.sedanDetailId.pressureSopWashId)) {
                                                        workSubHeadingDetailsArrayList.set(0, workSubHeadingDetails);
                                                    } else if (id.equals(Constants.suvDetailId.interiorVaccumCleaningId) || id.equals(Constants.sedanDetailId.interiorVaccumCleaningId)) {
                                                        workSubHeadingDetailsArrayList.set(1, workSubHeadingDetails);
                                                    } else if (id.equals(Constants.suvDetailId.tyreShiningId) || id.equals(Constants.sedanDetailId.tyreShiningId)) {
                                                        workSubHeadingDetailsArrayList.set(2, workSubHeadingDetails);
                                                    }
                                                }
                                                workHeadingDetails.setSubHeadingDetailsArrayList(workSubHeadingDetailsArrayList);
                                                if (length == 4) {
                                                    suvArrList.add(workHeadingDetails);
                                                } else {
                                                    sedanArrList.add(workHeadingDetails);
                                                }
                                            }
                                        }
                                    }
                                    Collections.sort(suvArrList, new sortNames());
                                    Collections.sort(sedanArrList, new sortNames());
                                    setData(jobDetails);
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(JobDetailsActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                }
                            } else {
                                dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(JobDetailsActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    class sortNames implements Comparator<WorkHeadingDetails> {

        @Override
        public int compare(WorkHeadingDetails m1, WorkHeadingDetails m2) {
            String name1 = m1.getWorkHeadingName();
            String name2 = m2.getWorkHeadingName();
            int i1 = Integer.parseInt(name1.substring(name1.length() - 1));
            int i2 = Integer.parseInt(name2.substring(name2.length() - 1));
            if (i1 > i2) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    private void volleyUpdateCarDetailsRequest(final String mobile, final String carTypeJson) {
        dialogview.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_CAR_DETAILS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    isWorkDetailsEditMode = false;
                                    if (!isEdited) {
                                        isEdited = true;
                                    }
                                    ivEdit.setImageResource(R.drawable.appoin_16);
                                    spinnerSuv.setEnabled(false);
                                    spinnerSedan.setEnabled(false);
                                    tvSuv.setTextColor(getResources().getColor(R.color.purple));
                                    tvSuvCount.setTextColor(getResources().getColor(R.color.purple));
                                    tvSedan.setTextColor(getResources().getColor(R.color.purple));
                                    tvSedanCount.setTextColor(getResources().getColor(R.color.purple));
                                    tvSuvCount.setCompoundDrawables(null, null, null, null);
                                    tvSedanCount.setCompoundDrawables(null, null, null, null);
                                    int headingSuvChildCount = llAddableSuv.getChildCount();
                                    if (headingSuvChildCount > 0) {
                                        for (int i = 0; i < headingSuvChildCount; i++) {
                                            View v = llAddableSuv.getChildAt(i);
                                            LinearLayout subHeadingAddable = (LinearLayout) v.findViewById(R.id.llAddableSubHeading);
                                            int subHeadingChildCount = subHeadingAddable.getChildCount();
                                            for (int j = 0; j < subHeadingChildCount; j++) {
                                                View v2 = subHeadingAddable.getChildAt(j);
                                                ToggleButton tb = (ToggleButton) v2.findViewById(R.id.tb);
                                                tb.setEnabled(false);
                                            }
                                        }
                                    }
                                    int headingSedanChildCount = llAddableSedan.getChildCount();
                                    if (headingSedanChildCount > 0) {
                                        for (int i = 0; i < headingSedanChildCount; i++) {
                                            View v = llAddableSedan.getChildAt(i);
                                            LinearLayout subHeadingAddable = (LinearLayout) v.findViewById(R.id.llAddableSubHeading);
                                            int subHeadingChildCount = subHeadingAddable.getChildCount();
                                            for (int j = 0; j < subHeadingChildCount; j++) {
                                                View v2 = subHeadingAddable.getChildAt(j);
                                                ToggleButton tb = (ToggleButton) v2.findViewById(R.id.tb);
                                                tb.setEnabled(false);
                                            }
                                        }
                                    }
//                                    volleyGetJobDetails(mobile, serviceId);
                                    Toast.makeText(JobDetailsActivity.this, responseJsonObj.optString("msg"), Toast.LENGTH_SHORT).show();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(JobDetailsActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(JobDetailsActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("car_type", carTypeJson);
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    void setData(final JobDetails jobDetails) {
        enableSuvSpinner = true;
        enableSedanSpinner = true;
        spinnerSuv.setSelection(suvArrList.size());
        spinnerSedan.setSelection(sedanArrList.size());

        tvCustomerName.setText(jobDetails.getCustomerName());
        tvDate.setText(jobDetails.getDate());
        tvJobId.setText(jobDetails.getJobId());

        String address = jobDetails.getLocation();
        if (address != null && !address.toString().trim().equals("")) {
            String addressArr[] = address.split(", ");
            if (addressArr != null && addressArr.length > 0) {
                tvLocation.setText(addressArr[0]);
            }
        }

        String headingTime = jobDetails.getHeadingTime();
        String requestTime = jobDetails.getRequestTime();
        String startTime = jobDetails.getStartTime();
        String confirmTime = jobDetails.getConfirmTime();
        String completeTime = jobDetails.getCompleteTime();
        if (headingTime != null && !headingTime.toString().trim().equals("")) {
            tvHeadingTime.setText(headingTime.substring(0, 5));
            tvHeadingAmPm.setText(headingTime.substring(6));
        } else {
            tvHeadingTime.setText("-");
        }
        if (requestTime != null && !requestTime.toString().trim().equals("")) {
            tvRequestTime.setText(requestTime.substring(0, 5));
            tvRequestAmPm.setText(requestTime.substring(6));
        } else {
            tvRequestTime.setText("-");
        }
        if (startTime != null && !startTime.toString().trim().equals("")) {
            tvStartTime.setText(startTime.substring(0, 5));
            tvStartAmPm.setText(startTime.substring(6));
        } else {
            tvStartTime.setText("-");
        }
        if (confirmTime != null && !confirmTime.toString().trim().equals("")) {
            tvConfirmTime.setText(confirmTime.substring(0, 5));
            tvConfirmAmPm.setText(confirmTime.substring(6));
        } else {
            tvConfirmTime.setText("-");
        }
        if (completeTime != null && !completeTime.toString().trim().equals("")) {
            tvCompleteTime.setText(completeTime.substring(0, 5));
            tvCompleteAmPm.setText(completeTime.substring(6));
        } else {
            tvCompleteTime.setText("-");
        }

        dialogview.dismissCustomSpinProgress();
        scrollView.setVisibility(View.VISIBLE);

    }

    void inflateSuvDetails() {
        int alreadyAddedSuvCount = llAddableSuv.getChildCount();
        int extraSuvCount = totalSuvCount - alreadyAddedSuvCount;
        enableSuvToggleButtons = false;

        if (extraSuvCount > 0) {
            for (int j = alreadyAddedSuvCount; j < suvArrList.size(); j++) {

                final int currentMainPos = j;

                final WorkHeadingDetails whd = suvArrList.get(j);

                LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View headingView = headingInflater.inflate(R.layout.inflate_work_details_heading, null);
                final TextView tvHeadingName = (TextView) headingView.findViewById(R.id.tvHeading);
                final TextView tvHeadingTotalPrice = (TextView) headingView.findViewById(R.id.tvTotalPrice);
                final LinearLayout llAddableSubHeading = (LinearLayout) headingView.findViewById(R.id.llAddableSubHeading);

                tvHeadingName.setText(whd.getWorkHeadingName());

                final ArrayList<WorkSubHeadingDetails> tempSubArrList = new ArrayList<>();
                tempSubArrList.addAll(whd.getSubHeadingDetailsArrayList());

                int size = whd.getSubHeadingDetailsArrayList().size();
                for (int i = 0; i < size; i++) {
                    final int currentSubPos = i;
                    WorkSubHeadingDetails swhd = new WorkSubHeadingDetails();
                    swhd = tempSubArrList.get(i);
                    LayoutInflater subHeadingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View subHeadingView = subHeadingInflater.inflate(R.layout.inflate_work_details_sub_item, null);
                    final TextView tvSubHeadingName = (TextView) subHeadingView.findViewById(R.id.tvSubHeadingName);
                    final TextView tvSubHeadingPrice = (TextView) subHeadingView.findViewById(R.id.tvSubHeadingPrice);
                    final ToggleButton toggleButton = (ToggleButton) subHeadingView.findViewById(R.id.tb);

                    tvSubHeadingName.setText(swhd.getWorkSubHeadingName());
                    tvSubHeadingPrice.setText("" + GlobalVariable.decimalFormat.format(swhd.getWorkSubHeadingPrice()));

                    if (!isWorkDetailsEditMode) {
                        toggleButton.setEnabled(false);
                    } else {
                        toggleButton.setEnabled(true);
                    }
                    toggleButton.setChecked(swhd.isSelected());

                    toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (enableSuvToggleButtons) {
                                suvArrList.get(currentMainPos).getSubHeadingDetailsArrayList().get(currentSubPos).setSelected(b);

                                final ArrayList<WorkSubHeadingDetails> tempSubArrList = new ArrayList<>();
                                tempSubArrList.addAll(suvArrList.get(currentMainPos).getSubHeadingDetailsArrayList());

                                suvHeadingPrice = 0;

                                for (int j = 0; j < tempSubArrList.size(); j++) {
                                    WorkSubHeadingDetails wshd = tempSubArrList.get(j);
                                    if (wshd.isSelected()) {
                                        suvHeadingPrice = suvHeadingPrice + wshd.getWorkSubHeadingPrice();
                                    }
                                }

                                tvHeadingTotalPrice.setText("" + GlobalVariable.decimalFormat.format(suvHeadingPrice));
                            }
                        }
                    });

                    llAddableSubHeading.addView(subHeadingView);
                }

                llAddableSuv.addView(headingView);
            }
        } else if (extraSuvCount < 0) {
            for (int j = totalSuvCount; j < alreadyAddedSuvCount; j++) {
                llAddableSuv.removeViewAt(j);
            }
        }
        calculateSuvTotalPrice();
    }

    void inflateSedanDetails() {
        int alreadyAddedSedanCount = llAddableSedan.getChildCount();
        int extraSedanCount = totalSedanCount - alreadyAddedSedanCount;
        enableSedanToggleButtons = false;

        if (extraSedanCount > 0) {
            for (int j = alreadyAddedSedanCount; j < sedanArrList.size(); j++) {

                final int currentMainPos = j;

                final WorkHeadingDetails whd = sedanArrList.get(j);

                LayoutInflater headingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View headingView = headingInflater.inflate(R.layout.inflate_work_details_heading, null);
                final TextView tvHeadingName = (TextView) headingView.findViewById(R.id.tvHeading);
                final TextView tvHeadingTotalPrice = (TextView) headingView.findViewById(R.id.tvTotalPrice);
                final LinearLayout llAddableSubHeading = (LinearLayout) headingView.findViewById(R.id.llAddableSubHeading);

                tvHeadingName.setText(whd.getWorkHeadingName());

                final ArrayList<WorkSubHeadingDetails> tempSubArrList = new ArrayList<>();
                tempSubArrList.addAll(whd.getSubHeadingDetailsArrayList());

                final int size = tempSubArrList.size();
                for (int i = 0; i < size; i++) {
                    final int currentSubPos = i;
                    WorkSubHeadingDetails swhd = new WorkSubHeadingDetails();
                    swhd = whd.getSubHeadingDetailsArrayList().get(i);
                    LayoutInflater subHeadingInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View subHeadingView = subHeadingInflater.inflate(R.layout.inflate_work_details_sub_item, null);
                    final TextView tvSubHeadingName = (TextView) subHeadingView.findViewById(R.id.tvSubHeadingName);
                    final TextView tvSubHeadingPrice = (TextView) subHeadingView.findViewById(R.id.tvSubHeadingPrice);
                    final ToggleButton toggleButton = (ToggleButton) subHeadingView.findViewById(R.id.tb);

                    tvSubHeadingName.setText(swhd.getWorkSubHeadingName());
                    tvSubHeadingPrice.setText("" + GlobalVariable.decimalFormat.format(swhd.getWorkSubHeadingPrice()));

                    if (!isWorkDetailsEditMode) {
                        toggleButton.setEnabled(false);
                    } else {
                        toggleButton.setEnabled(true);
                    }
                    toggleButton.setChecked(swhd.isSelected());

                    toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (enableSedanToggleButtons) {
                                sedanArrList.get(currentMainPos).getSubHeadingDetailsArrayList().get(currentSubPos).setSelected(b);

                                final ArrayList<WorkSubHeadingDetails> tempSubArrList = new ArrayList<>();
                                tempSubArrList.addAll(sedanArrList.get(currentMainPos).getSubHeadingDetailsArrayList());

                                sedanHeadingPrice = 0;

                                for (int j = 0; j < tempSubArrList.size(); j++) {
                                    WorkSubHeadingDetails wshd = tempSubArrList.get(j);
                                    if (wshd.isSelected()) {
                                        sedanHeadingPrice = sedanHeadingPrice + wshd.getWorkSubHeadingPrice();
                                    }
                                }

                                tvHeadingTotalPrice.setText("" + GlobalVariable.decimalFormat.format(sedanHeadingPrice));
                            }
                        }
                    });

                    llAddableSubHeading.addView(subHeadingView);
                }
                tvHeadingTotalPrice.setText("" + GlobalVariable.decimalFormat.format(sedanHeadingPrice));
                llAddableSedan.addView(headingView);
            }
        } else if (extraSedanCount < 0) {
            for (int j = totalSedanCount; j < alreadyAddedSedanCount; j++) {
                llAddableSedan.removeViewAt(j);
            }
        }
        calculateSedanTotalPrice();
        dialogview.dismissCustomSpinProgress();
    }

    void calculateSuvTotalPrice() {
        int currentPos = 0;
        for (WorkHeadingDetails whd : suvArrList) {

            View v = llAddableSuv.getChildAt(currentPos);
            final TextView tvHeadingTotalPrice = (TextView) v.findViewById(R.id.tvTotalPrice);
            LinearLayout subHeadingAddable = (LinearLayout) v.findViewById(R.id.llAddableSubHeading);

            final ArrayList<WorkSubHeadingDetails> tempSubArrList = new ArrayList<>();
            tempSubArrList.addAll(whd.getSubHeadingDetailsArrayList());

            suvHeadingPrice = 0;

            for (int j = 0; j < tempSubArrList.size(); j++) {
                WorkSubHeadingDetails wshd = tempSubArrList.get(j);
                if (wshd.isSelected()) {
                    suvHeadingPrice = suvHeadingPrice + wshd.getWorkSubHeadingPrice();
                }
            }

            tvHeadingTotalPrice.setText("" + GlobalVariable.decimalFormat.format(suvHeadingPrice));
            currentPos++;
        }
        enableSuvToggleButtons = true;
        dialogview.dismissCustomSpinProgress();
    }

    void calculateSedanTotalPrice() {
        int currentPos = 0;
        for (WorkHeadingDetails whd : sedanArrList) {

            View v = llAddableSedan.getChildAt(currentPos);
            final TextView tvHeadingTotalPrice = (TextView) v.findViewById(R.id.tvTotalPrice);
            LinearLayout subHeadingAddable = (LinearLayout) v.findViewById(R.id.llAddableSubHeading);

            final ArrayList<WorkSubHeadingDetails> tempSubArrList = new ArrayList<>();
            tempSubArrList.addAll(whd.getSubHeadingDetailsArrayList());

            sedanHeadingPrice = 0;

            for (int j = 0; j < tempSubArrList.size(); j++) {
                WorkSubHeadingDetails wshd = tempSubArrList.get(j);
                if (wshd.isSelected()) {
                    sedanHeadingPrice = sedanHeadingPrice + wshd.getWorkSubHeadingPrice();
                }
            }

            tvHeadingTotalPrice.setText("" + GlobalVariable.decimalFormat.format(sedanHeadingPrice));
            currentPos++;
        }
        enableSedanToggleButtons = true;
        dialogview.dismissCustomSpinProgress();
    }

    private void volleyRejectRequest(final String mobile, final String reasonId, final String comment) {
        dialogview.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REJECT_APPOINTMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    if (!isEdited) {
                                        isEdited = true;
                                    }
                                    Toast.makeText(JobDetailsActivity.this, "Appointment is cancelled successfully!", Toast.LENGTH_SHORT).show();
                                    onBackPressed();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(JobDetailsActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(JobDetailsActivity.this, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("reason_id", reasonId);
                params.put("comment", comment);
                params.put("type", "cancelled");
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyGetReasonsRequest(final String typeId) {
        dialogview.showCustomSpinProgress(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_REASON_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);
                            reasonArrList.clear();
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JSONArray serviceJsonArr = responseJsonObj.optJSONArray("service");
                                    for (int i = 0; i < serviceJsonArr.length(); i++) {
                                        JSONObject jsonObj = serviceJsonArr.optJSONObject(i);
                                        Reason reason = new Reason();
                                        reason.setId(jsonObj.optString("id"));
                                        reason.setName(jsonObj.optString("title"));
                                        String isActive = jsonObj.optString("is_active");
                                        if (isActive != null && !isActive.toString().trim().equals("")) {
                                            if (isActive.toString().trim().equals("1")) {
                                                reason.setActive(true);
                                            } else {
                                                reason.setActive(false);
                                            }
                                        } else {
                                            reason.setActive(false);
                                        }
                                        reasonArrList.add(reason);
                                    }
                                    showReasonDialog();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(JobDetailsActivity.this, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                            getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type_id", typeId);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    void showReasonDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_reason);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        final EditText etComment = (EditText) dialog.findViewById(R.id.etComment);
        final Spinner spinnerReason = (Spinner) dialog.findViewById(R.id.spinnerReason);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);

        ArrayList<String> tempArr = new ArrayList<>();
        for (int i = 0; i < reasonArrList.size(); i++) {
            tempArr.add(reasonArrList.get(i).getName());
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tempArr);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerReason.setAdapter(spinnerArrayAdapter);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etComment.getText() == null || etComment.getText().toString().trim().equals("")) {
                    dialogview.showCustomSingleButtonDialog(JobDetailsActivity.this, getResources().getString(R.string.sorry), getResources().getString(R.string.comment_error));
                } else {
                    dialog.dismiss();
                    volleyRejectRequest(pref.getStringPreference(JobDetailsActivity.this, Preferences.PHONE), reasonArrList.get(spinnerReason.getSelectedItemPosition()).getId(),
                            etComment.getText().toString().trim());
                }
            }
        });

        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void saveWorkDetails() {
        JSONObject carTypeJson = new JSONObject();

        JSONArray suvJsonArr = new JSONArray();
        JSONArray sedanJsonArr = new JSONArray();

        boolean isAtleastOneServiceFound = false;

        try {
            for (int i = 0; i < suvArrList.size(); i++) {
                JSONObject suvJsonObj = new JSONObject();
                suvJsonObj.put("type_id", Constants.typeId.carWashType);
                suvJsonObj.put("quantity", "1");
                JSONArray optionDetailsJsonArr = new JSONArray();
                ArrayList<WorkSubHeadingDetails> tempSub = new ArrayList<>();
                tempSub.addAll(suvArrList.get(i).getSubHeadingDetailsArrayList());
                for (int j = 0; j < tempSub.size(); j++) {
                    if (tempSub.get(j).isSelected()) {
                        if (!isAtleastOneServiceFound) {
                            isAtleastOneServiceFound = true;
                        }
                        optionDetailsJsonArr.put(tempSub.get(j).getWorkSubHeadingId());
                    }
                }
                suvJsonObj.put("option_details", optionDetailsJsonArr);
                suvJsonArr.put(suvJsonObj);
            }

            for (int i = 0; i < sedanArrList.size(); i++) {
                JSONObject sedanJsonObj = new JSONObject();
                sedanJsonObj.put("type_id", Constants.typeId.carWashType);
                sedanJsonObj.put("quantity", "1");
                JSONArray optionDetailsJsonArr = new JSONArray();
                ArrayList<WorkSubHeadingDetails> tempSub = new ArrayList<>();
                tempSub.addAll(sedanArrList.get(i).getSubHeadingDetailsArrayList());
                for (int j = 0; j < tempSub.size(); j++) {
                    if (tempSub.get(j).isSelected()) {
                        if (!isAtleastOneServiceFound) {
                            isAtleastOneServiceFound = true;
                        }
                        optionDetailsJsonArr.put(tempSub.get(j).getWorkSubHeadingId());
                    }
                }
                sedanJsonObj.put("option_details", optionDetailsJsonArr);
                sedanJsonArr.put(sedanJsonObj);
            }

            carTypeJson.put("suv", suvJsonArr);
            carTypeJson.put("sedan", sedanJsonArr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, carTypeJson.toString());
        if (isAtleastOneServiceFound) {
            volleyUpdateCarDetailsRequest(pref.getStringPreference(this, Preferences.PHONE), carTypeJson.toString());
        } else {
            dialogview.showCustomSingleButtonDialog(this, getResources().getString(R.string.sorry), getResources().getString(R.string.atleast_one_service_required));
        }
    }

    @Override
    public void onBackPressed() {
        if (isEdited) {
            setResult(Activity.RESULT_OK);
        }
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnCancelJob:
                volleyGetReasonsRequest(Constants.typeId.carWashType);
                break;

            case R.id.ivEdit:
                if (!suvArrList.isEmpty() || !sedanArrList.isEmpty()) {
                    if (isWorkDetailsEditMode) {
                        saveWorkDetails();
                    } else {
                        isWorkDetailsEditMode = true;
                        ivEdit.setImageResource(R.drawable.tick_3);
                        spinnerSuv.setEnabled(true);
                        spinnerSedan.setEnabled(true);
                        tvSuv.setTextColor(getResources().getColor(R.color.white));
                        tvSedan.setTextColor(getResources().getColor(R.color.white));
                        tvSuvCount.setCompoundDrawables(null, null, getResources().getDrawable(R.drawable.arrow_downward), null);
                        tvSedanCount.setCompoundDrawables(null, null, getResources().getDrawable(R.drawable.arrow_downward), null);
                        tvSuvCount.setTextColor(getResources().getColor(R.color.white));
                        tvSedanCount.setTextColor(getResources().getColor(R.color.white));
                        int headingSuvChildCount = llAddableSuv.getChildCount();
                        if (headingSuvChildCount > 0) {
                            for (int i = 0; i < headingSuvChildCount; i++) {
                                View v = llAddableSuv.getChildAt(i);
                                LinearLayout subHeadingAddable = (LinearLayout) v.findViewById(R.id.llAddableSubHeading);
                                int subHeadingChildCount = subHeadingAddable.getChildCount();
                                for (int j = 0; j < subHeadingChildCount; j++) {
                                    View v2 = subHeadingAddable.getChildAt(j);
                                    ToggleButton tb = (ToggleButton) v2.findViewById(R.id.tb);
                                    tb.setEnabled(true);
                                }
                            }
                        }
                        int headingSedanChildCount = llAddableSedan.getChildCount();
                        if (headingSedanChildCount > 0) {
                            for (int i = 0; i < headingSedanChildCount; i++) {
                                View v = llAddableSedan.getChildAt(i);
                                LinearLayout subHeadingAddable = (LinearLayout) v.findViewById(R.id.llAddableSubHeading);
                                int subHeadingChildCount = subHeadingAddable.getChildCount();
                                for (int j = 0; j < subHeadingChildCount; j++) {
                                    View v2 = subHeadingAddable.getChildAt(j);
                                    ToggleButton tb = (ToggleButton) v2.findViewById(R.id.tb);
                                    tb.setEnabled(true);
                                }
                            }
                        }
                    }
                }
                break;

            case R.id.imgBtnBack:
                onBackPressed();
                break;

        }
    }
}
