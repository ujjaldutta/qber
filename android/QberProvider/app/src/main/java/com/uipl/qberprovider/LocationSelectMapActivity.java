package com.uipl.qberprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.GPSTracker;
import com.uipl.qberprovider.utils.Permission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import customView.TypedfaceTextView;


/**
 * Created by Ankan on 07-Oct-16.
 * Activity to display google map for choose location
 */
public class LocationSelectMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {
    private GoogleMap mMap;
    private String TAG = "LocationSelectMapActivity";
    EditText etSearch;
    ImageView imvSearchShow, imgCross;
    ListView searchList;
    ProgressBar pbProgress;
    private Double lat = 0.0;
    private Double lng = 0.0;
    ArrayList<HashMap<String, String>> placeList = new ArrayList<>();
    AutoCompleteAdapter adapterAutoComplete;
    View footerView;
    PlaceApiServices placeApi;
    boolean shouldCancel = false;
    boolean stopListener = false;
    private RequestQueue mQueue;
    private DialogView dialogView;
    private GPSTracker gpsTracker;
    String address = "";
    private Button btnDone;
    String callingFrom = "";
    String locationAddress = "";
    float zoomValue = 12.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        if (getIntent().hasExtra("CALLING_FROM")) {
            callingFrom = getIntent().getStringExtra("CALLING_FROM");
        }
        if (getIntent().hasExtra("LAT") && !getIntent().getStringExtra("LAT").equals("")) {

            lat = Double.parseDouble(getIntent().getStringExtra("LAT"));
        }
        if (getIntent().hasExtra("LNG") && !getIntent().getStringExtra("LNG").equals("")) {
            lng = Double.parseDouble(getIntent().getStringExtra("LNG"));
        }
        if (getIntent().hasExtra("ADDRESS")) {
            locationAddress = getIntent().getStringExtra("ADDRESS");
        }
        gpsTracker = new GPSTracker(this);
        init();

        setListener();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        //Move the camera to the user's location and zoom in!
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomValue));
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        PermissionChecking();
        mMap.setOnMapClickListener(this);
        getLocation();
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(LocationSelectMapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            ActivityCompat.requestPermissions(LocationSelectMapActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mMap != null) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    dialogView.showToast(LocationSelectMapActivity.this, getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }


    /**
     * Method to initialize variable
     */
    private void init() {
        dialogView = new DialogView(this);
        mQueue = Volley.newRequestQueue(this);
        etSearch = (EditText) findViewById(R.id.etSearch);
        imgCross = (ImageView) findViewById(R.id.imgCross);
        imvSearchShow = (ImageView) findViewById(R.id.imvSearchShow);
        searchList = (ListView) findViewById(R.id.searchList);

        pbProgress = (ProgressBar) findViewById(R.id.pbProgress);

        adapterAutoComplete = new AutoCompleteAdapter(LocationSelectMapActivity.this, placeList);
        searchList.setAdapter(adapterAutoComplete);

        btnDone = (Button) findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);
        if (!locationAddress.equals("")) {
            stopListener = true;
            address = locationAddress;
            etSearch.setText(locationAddress);
            stopListener = false;
        }

    }

    /**
     * Method to get current latitude and longitude
     */
    public void getLocation() {
        if (gpsTracker.canGetLocation()) {
            if (lat == 0.0 && lng == 0.0) {
                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();
                mMap.clear();
                LatLng latLng = new LatLng(latitude, longitude);
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomValue));
                lat = latitude;
                lng = longitude;
                System.out.println("latitude = " + latitude);
                System.out.println("longitude = " + longitude);
            }
            if (locationAddress != null && locationAddress.equals("")) {
                getLocationFromLatLng(lat, lng);
            }
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    /**
     * Method to set edit text Listener
     */
    private void setListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().equals("") && !stopListener) {
                    shouldCancel = false;
                    if (searchList.getFooterViewsCount() > 0) {
                        TypedfaceTextView tvFooter = (TypedfaceTextView) footerView.findViewById(R.id.tvFooter);
                        tvFooter.setText("Search for \"" + charSequence.toString() + "\"");
                    } else {
                        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.location_footer, null, false);
                        TypedfaceTextView tvFooter = (TypedfaceTextView) footerView.findViewById(R.id.tvFooter);
                        tvFooter.setText("Search for \"" + charSequence.toString() + "\"");
                        searchList.addFooterView(footerView);
                    }

                    placeList.clear();
                    adapterAutoComplete.notifyDataSetChanged();
                    placeApi = new PlaceApiServices(charSequence.toString());
                    placeApi.execute();
                } else {
                    shouldCancel = true;

                    imvSearchShow.setVisibility(View.VISIBLE);
                    pbProgress.setVisibility(View.GONE);

                    if (searchList.getFooterViewsCount() > 0)
                        searchList.removeFooterView(footerView);
                    placeList.clear();
                    adapterAutoComplete.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                if (placeApi != null) {
                    if (placeApi.getStatus() == AsyncTask.Status.RUNNING)
                        placeApi.cancel(true);
                    imvSearchShow.setVisibility(View.VISIBLE);
                    pbProgress.setVisibility(View.GONE);
                }
                if (searchList.getFooterViewsCount() > 0)
                    searchList.removeFooterView(footerView);
                placeList.clear();
                adapterAutoComplete.notifyDataSetChanged();
            }
        });

        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == placeList.size()) {
                    placeList.clear();
                    adapterAutoComplete.notifyDataSetChanged();
                    placeApi = new PlaceApiServices(etSearch.getText().toString());
                    placeApi.execute();
                } else if (placeList.size() > position) {
                    String placeID = placeList.get(position).get("place_id");
                    new LatLongApiServices(placeID).execute();
                    stopListener = true;
                    address = placeList.get(position).get("title") + "," + placeList.get(position).get("description");
                    etSearch.setText(placeList.get(position).get("title") + "," + placeList.get(position).get("description"));

                    if (searchList.getFooterViewsCount() > 0)
                        searchList.removeFooterView(footerView);
                    placeList.clear();
                    adapterAutoComplete.notifyDataSetChanged();
                    stopListener = false;
                }
                hideKeyboard(etSearch);
            }
        });
    }

    @Override
    public void onMapClick(LatLng latLng) {
        getLocationFromLatLng(latLng.latitude, latLng.longitude);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDone:
                Intent intent = new Intent();
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("address", address);
                setResult(Activity.RESULT_OK, intent);
                finish();

                break;
        }
    }

    /**
     * AsyncTask class to call Api service for get place id
     */
    public class PlaceApiServices extends AsyncTask<String, Void, String> {

        private String serchString = "";

        public PlaceApiServices(String s) {
            serchString = s;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            imvSearchShow.setVisibility(View.GONE);
            pbProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            return getPlace(serchString);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (!shouldCancel) {
                    JSONObject jsonObj = new JSONObject(result);
                    JSONArray predsJsonArray = jsonObj.optJSONArray("predictions");

                    for (int i = 0; i < predsJsonArray.length(); i++) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        JSONObject obj = predsJsonArray.optJSONObject(i);
                        JSONArray terms = obj.optJSONArray("terms");
                        StringBuilder sb = new StringBuilder();
                        for (int j = 1; j < terms.length(); j++) {
                            JSONObject obj2 = terms.optJSONObject(j);
                            sb.append(", ");
                            sb.append(obj2.optString("value"));
                        }
                        String description = "";
                        if (sb.toString().length() > 1) {
                            description = sb.toString().substring(2);
                        }

                        hashMap.put("title", terms.optJSONObject(0).optString("value"));
                        hashMap.put("description", description);
                        hashMap.put("place_id", predsJsonArray.optJSONObject(i).getString("place_id"));
                        placeList.add(hashMap);
                    }

                    searchList.setVisibility(View.VISIBLE);
                    adapterAutoComplete.notifyDataSetChanged();

                    imvSearchShow.setVisibility(View.VISIBLE);
                    pbProgress.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                Log.e(TAG, "Cannot process JSON results", e);
            }

        }
    }

    /**
     * @param input text to search
     * @return Method to call Api service to get place id list
     */
    public String getPlace(String input) {
        String result = "";

        String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete";
        String OUT_JSON = "/json";

        String API_KEY = getResources().getString(R.string.place_key);

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + OUT_JSON);
            sb.append("?input=" + URLEncoder.encode(input, "utf8"));
            sb.append("&components=country:QAT");
            sb.append("&key=" + API_KEY);

            Log.e(TAG, sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
            result = jsonResults.toString();

        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            //return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            //return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

    /**
     * AsyncTask Class for call api service to get latitude and longitude
     */
    public class LatLongApiServices extends AsyncTask<String, Void, String> {

        private String placeId = "";

        public LatLongApiServices(String place_id) {
            placeId = place_id;
        }

        @Override
        protected String doInBackground(String... strings) {
            return getLatLong(placeId);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result != null) {
                    // Create a JSON object hierarchy from the results
                    JSONObject jsonObj = new JSONObject(result);
                    JSONObject resultObj = jsonObj.optJSONObject("result");
                    JSONObject geometryObj = resultObj.optJSONObject("geometry");
                    JSONObject locationObj = geometryObj.optJSONObject("location");
                    lat = Double.parseDouble(locationObj.optString("lat"));
                    lng = Double.parseDouble(locationObj.optString("lng"));
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
                    //Move the camera to the user's location and zoom in!
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), zoomValue));
                }
            } catch (JSONException e) {
                Log.e(TAG, "Cannot process JSON results", e);
            }

        }
    }


    /**
     * @param placeId place id
     * @return Method for call api services to get latitude & longitude base on place id
     */
    public String getLatLong(String placeId) {
        String result = "";
        String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
        String TYPE_DETAILS = "/details";
        String OUT_JSON = "/json";

        String API_KEY = getResources().getString(R.string.place_key);


        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON);
            sb.append("?placeid=" + placeId);
            sb.append("&key=" + API_KEY);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
            result = jsonResults.toString();

        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            //return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            //return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

    /**
     * @param latitude  Latitude
     * @param longitude Longitude
     *                  Method to get location address base on latitude & longitude
     */
    void getLocationFromLatLng(final double latitude, final double longitude) {
        dialogView.showCustomSpinProgress(this);
        String API_KEY = getResources().getString(R.string.place_key);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                + latitude + "," + longitude + "&key=" + API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray resultsArray = obj.optJSONArray("results");
                            if (resultsArray.length() > 0) {
                                JSONArray jsonArray = resultsArray.optJSONObject(0).optJSONArray("address_components");
                                String countryName = "";
                                String sortName = "";
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.optJSONObject(i);
                                    JSONArray typeArray = object.optJSONArray("types");
                                    if (typeArray.get(0).equals("country")) {
                                        countryName = object.optString("long_name");
                                        sortName = object.optString("short_name");

                                    }
                                }
                                if (countryName.equals("Qatar") || sortName.equals("QA")) {
                                    String formattedAddress = resultsArray.getJSONObject(0).optString("formatted_address");
                                    if (formattedAddress != null && !formattedAddress.toString().trim().equals("")) {

                                        stopListener = true;
                                        etSearch.setText(formattedAddress);
                                        address = formattedAddress;
                                        lat = latitude;
                                        lng = longitude;
                                        mMap.clear();
                                        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map)));
                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoomValue));
                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
                                        stopListener = false;
                                        dialogView.dismissCustomSpinProgress();

                                    } else {
                                        dialogView.dismissCustomSpinProgress();

                                    }
                                } else {
                                    dialogView.dismissCustomSpinProgress();
                                    Toast.makeText(LocationSelectMapActivity.this, getResources().getString(R.string.please_select_location_of_qatar), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                dialogView.dismissCustomSpinProgress();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialogView.dismissCustomSpinProgress();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialogView.dismissCustomSpinProgress();
                error.printStackTrace();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy((int) GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        mQueue.add(stringRequest);
    }


    /**
     * BaseAdapter for location list
     */
    public class AutoCompleteAdapter extends BaseAdapter {

        private final Activity mActivity;
        private final ArrayList<HashMap<String, String>> mPlaceList;
        LayoutInflater inflater;

        public AutoCompleteAdapter(Activity activity, ArrayList<HashMap<String, String>> placeList) {
            this.mActivity = activity;
            this.mPlaceList = placeList;
            this.inflater = LayoutInflater.from(activity);

        }

        @Override
        public int getCount() {
            return mPlaceList.size();
        }

        @Override
        public Object getItem(int i) {
            return mPlaceList.get(i);
        }


        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = inflater.inflate(R.layout.inflate_map_address_list, viewGroup, false);
            }

            TextView tvAddressName = (TextView) view.findViewById(R.id.tvAddressName);
            TextView tvAddressDetails = (TextView) view.findViewById(R.id.tvAddressDetails);

            tvAddressName.setText(mPlaceList.get(i).get("title"));
            tvAddressDetails.setText(mPlaceList.get(i).get("description"));
            return view;
        }
    }

    /**
     * @param view Method to hide soft keyboard
     */
    public void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
