package com.uipl.qberprovider.adapter;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uipl.qberprovider.R;
import com.uipl.qberprovider.model.ServiceType;

import java.util.ArrayList;

/**
 * Created by pallab on 31/10/16.
 */

public class RegServicesListAdapter extends BaseAdapter {
    Activity mActivity;
    ArrayList<ServiceType> mServiceList;
    LayoutInflater inflater;

    public RegServicesListAdapter(Activity activity, ArrayList<ServiceType> serviceList) {
        this.mActivity = activity;
        this.mServiceList = serviceList;
        this.inflater = LayoutInflater.from(activity);

    }

    @Override
    public int getCount() {
        return mServiceList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.inflate_register_service_list, viewGroup, false);
        }
        LinearLayout llMainView = (LinearLayout) view.findViewById(R.id.llMainView);
        ImageView imgServiceSelect = (ImageView) view.findViewById(R.id.imgServiceSelect);
        TextView tvServiceName = (TextView) view.findViewById(R.id.tvServiceName);
        if (mServiceList.get(i).isSelectFlag()) {
            imgServiceSelect.setImageResource(R.drawable.tick);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                tvServiceName.setTextColor(mActivity.getResources().getColor(R.color.purple));
            } else {
                tvServiceName.setTextColor(mActivity.getResources().getColor(R.color.purple));
            }
        } else {
            imgServiceSelect.setImageResource(R.drawable.circle);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                tvServiceName.setTextColor(mActivity.getResources().getColor(R.color.black));
            } else {
                tvServiceName.setTextColor(mActivity.getResources().getColor(R.color.black));
            }
        }
        tvServiceName.setText(mServiceList.get(i).getServiceName());
        return view;
    }
}
