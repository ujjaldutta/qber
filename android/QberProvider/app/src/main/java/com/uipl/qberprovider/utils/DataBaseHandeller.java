package com.uipl.qberprovider.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.uipl.qberprovider.model.RegistrationDetails;


/**
 * Created by Pallab on 21/11/16.
 */
public class DataBaseHandeller extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;


    private static DataBaseHandeller instance;
    // Database Name
    private static final String DATABASE_NAME = "QBER_PROVIDER";
    // Table Name
    private static final String TABLE_REGISTER = "TableRegister";

    private static final String KEY_ID = "ID";
    private static final String NAME = "Name";
    private static final String YEAR_POSITION = "YearPosition";
    private static final String COUNTRY_POSITION = "CountryPosition";
    private static final String EMAIL = "Email";
    private static final String GENDER = "Gender";
    private static final String MOBILE = "Mobile";
    private static final String INTERNET_ACCESS_POSITION = "InternetAccessPosition";
    private static final String TRANSPORT_POSITION = "TransportPosition";
    private static final String YOUR_OFFER = "YourOffer";
    private static final String YEAR_OF_EXPERIENCE = "YearOfExperience";
    private static final String LIVE_IN_QATAR = "LiveInQatar";
    private static final String WORK_PREFERENCE_POSITION = "WorkPreferencePosition";
    private static final String LANGUAGE_PREFERENCE_POSITION = "LanguagePreferencePosition";
    private static final String IS_SHOP = "IsShop";
    private static final String SHOP_NAME = "ShopName";
    private static final String ADMIN_PIC_PATH = "AdminPicPath";
    private static final String TOOLS_PIC_PATH = "ToolsPicPath";
    private static final String SHOP_PIC_PATH = "ShopPicPath";
    private static final String CV_FILE_PATH = "CVFilePath";
    private static final String CAR_REG_PIC_PATH = "CarRegPicPath";
    private static final String DRIVING_LICENSE_PIC_PATH = "DrivingLicensePicPath";
    private static final String IDENTITY_PIC_PATH = "IdentityPicPath";


    SQLiteDatabase mdb;

    public DataBaseHandeller(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mdb = getWritableDatabase();
        onCreate(mdb);
    }

    public static synchronized DataBaseHandeller getInstance(Context context) {

        if (instance == null) {
            instance = new DataBaseHandeller(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_STATE = "CREATE TABLE  IF NOT EXISTS " + TABLE_REGISTER + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NAME + " TEXT,"
                + YEAR_POSITION + " TEXT,"
                + COUNTRY_POSITION + " TEXT,"
                + EMAIL + " TEXT,"
                + GENDER + " TEXT,"
                + MOBILE + " TEXT,"
                + INTERNET_ACCESS_POSITION + " TEXT,"
                + TRANSPORT_POSITION + " TEXT,"
                + YOUR_OFFER + " TEXT,"
                + YEAR_OF_EXPERIENCE + " TEXT,"
                + LIVE_IN_QATAR + " TEXT,"
                + WORK_PREFERENCE_POSITION + " TEXT,"
                + LANGUAGE_PREFERENCE_POSITION + " TEXT,"
                + IS_SHOP + " TEXT,"
                + SHOP_NAME + " TEXT,"
                + ADMIN_PIC_PATH + " TEXT,"
                + TOOLS_PIC_PATH + " TEXT,"
                + SHOP_PIC_PATH + " TEXT,"
                + CV_FILE_PATH + " TEXT,"
                + CAR_REG_PIC_PATH + " TEXT,"
                + DRIVING_LICENSE_PIC_PATH + " TEXT,"
                + IDENTITY_PIC_PATH + " TEXT"
                + ")";


        db.execSQL(CREATE_TABLE_STATE);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed

        // Create tables again
        onCreate(db);
    }

    public void saveRegData(RegistrationDetails registrationDetails) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(NAME, registrationDetails.getName());
        values.put(YEAR_POSITION, registrationDetails.getYearPosition());
        values.put(COUNTRY_POSITION, registrationDetails.getCountryPosition());
        values.put(EMAIL, registrationDetails.getEmail());
        values.put(GENDER, registrationDetails.getGender());
        values.put(MOBILE, registrationDetails.getPhone());
        values.put(INTERNET_ACCESS_POSITION, registrationDetails.getInternetAccessPosition());
        values.put(TRANSPORT_POSITION, registrationDetails.getTransportPosition());
        values.put(YOUR_OFFER, registrationDetails.getYourOffer());
        values.put(YEAR_OF_EXPERIENCE, registrationDetails.getYearOfExperience());
        values.put(LIVE_IN_QATAR, registrationDetails.getLiveInQatar());
        values.put(WORK_PREFERENCE_POSITION, registrationDetails.getWorkPreferencePosition());
        values.put(LANGUAGE_PREFERENCE_POSITION, registrationDetails.getLanguagePreferencePosition());
        values.put(IS_SHOP, registrationDetails.getIsShop());
        values.put(SHOP_NAME, registrationDetails.getShopName());
        values.put(ADMIN_PIC_PATH, registrationDetails.getAdminPicPath());
        values.put(TOOLS_PIC_PATH, registrationDetails.getToolsPicPath());
        values.put(SHOP_PIC_PATH, registrationDetails.getShopPicPath());
        values.put(CV_FILE_PATH, registrationDetails.getCVFilePath());
        values.put(CAR_REG_PIC_PATH, registrationDetails.getRegisterPicPath());
        values.put(DRIVING_LICENSE_PIC_PATH, registrationDetails.getDrivingLicensePath());
        values.put(IDENTITY_PIC_PATH, registrationDetails.getIdentityPicPath());

        long ret = db.insert(TABLE_REGISTER, null, values);


        db.close(); // Closing database connection
    }


    public RegistrationDetails getRegData() {
        RegistrationDetails registrationDetails = new RegistrationDetails();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        cursor = db.rawQuery("SELECT * FROM " + TABLE_REGISTER, null);
        int count = cursor.getCount();
        if (cursor != null && cursor.moveToFirst()) {
            Log.e("name", cursor.getString(cursor.getColumnIndex(NAME)));
            registrationDetails.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            registrationDetails.setYearPosition(cursor.getString(cursor.getColumnIndex(YEAR_POSITION)));
            registrationDetails.setCountryPosition(cursor.getString(cursor.getColumnIndex(COUNTRY_POSITION)));
            registrationDetails.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            registrationDetails.setGender(cursor.getString(cursor.getColumnIndex(GENDER)));
            registrationDetails.setPhone(cursor.getString(cursor.getColumnIndex(MOBILE)));
            registrationDetails.setInternetAccessPosition(cursor.getString(cursor.getColumnIndex(INTERNET_ACCESS_POSITION)));
            registrationDetails.setTransportPosition(cursor.getString(cursor.getColumnIndex(TRANSPORT_POSITION)));
            registrationDetails.setYourOffer(cursor.getString(cursor.getColumnIndex(YOUR_OFFER)));
            registrationDetails.setYearOfExperience(cursor.getString(cursor.getColumnIndex(YEAR_OF_EXPERIENCE)));
            registrationDetails.setLiveInQatar(cursor.getString(cursor.getColumnIndex(LIVE_IN_QATAR)));
            registrationDetails.setWorkPreferencePosition(cursor.getString(cursor.getColumnIndex(WORK_PREFERENCE_POSITION)));
            registrationDetails.setLanguagePreferencePosition(cursor.getString(cursor.getColumnIndex(LANGUAGE_PREFERENCE_POSITION)));
            registrationDetails.setIsShop(cursor.getString(cursor.getColumnIndex(IS_SHOP)));
            registrationDetails.setShopName(cursor.getString(cursor.getColumnIndex(SHOP_NAME)));
            registrationDetails.setAdminPicPath(cursor.getString(cursor.getColumnIndex(ADMIN_PIC_PATH)));
            registrationDetails.setToolsPicPath(cursor.getString(cursor.getColumnIndex(TOOLS_PIC_PATH)));
            registrationDetails.setShopPicPath(cursor.getString(cursor.getColumnIndex(SHOP_PIC_PATH)));
            registrationDetails.setCVFilePath(cursor.getString(cursor.getColumnIndex(CV_FILE_PATH)));
            registrationDetails.setRegisterPicPath(cursor.getString(cursor.getColumnIndex(CAR_REG_PIC_PATH)));
            registrationDetails.setDrivingLicensePath(cursor.getString(cursor.getColumnIndex(DRIVING_LICENSE_PIC_PATH)));
            registrationDetails.setIdentityPicPath(cursor.getString(cursor.getColumnIndex(IDENTITY_PIC_PATH)));


        }

        cursor.close();
        db.close();
        return registrationDetails;
    }


    public void delete() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_REGISTER);


    }

}
