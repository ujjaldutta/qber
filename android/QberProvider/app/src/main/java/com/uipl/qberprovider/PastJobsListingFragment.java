package com.uipl.qberprovider;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.adapter.PastJobsAdapter;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

/**
 * Created by richa on 25/11/16.
 */
public class PastJobsListingFragment extends Fragment {

    String TAG = "CurrentJobsFragment";

    DialogView dialogview;
    RequestQueue mQueue;
    Preferences pref;

    RecyclerView rv;
    LinearLayoutManager llm;
    TextView tvEmpty;

    PastJobsAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_past_jobs_listing, container, false);

        initViews(rootView);

        adapter.notifyDataSetChanged();
        checkIfAdapterEmpty();

        JobsListingFragment.pastJobsFragmentLoaded = true;
        if (JobsListingFragment.dialogview != null && JobsListingFragment.currentJobsFragmentLoaded && JobsListingFragment.pastJobsFragmentLoaded) {
            JobsListingFragment.dialogview.dismissCustomSpinProgress();
        }

        return rootView;
    }

    private void initViews(View v) {
        dialogview = new DialogView(getActivity());
        pref = new Preferences(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());

        tvEmpty = (TextView) v.findViewById(R.id.tvEmpty);
        rv = (RecyclerView) v.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);
        adapter = new PastJobsAdapter(JobsListingFragment.pastJobsList, getActivity());
        rv.setAdapter(adapter);

        llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }

    void checkIfAdapterEmpty() {
        if (JobsListingFragment.pastJobsList.isEmpty()) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.GONE);
        }
    }

}