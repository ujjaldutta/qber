package com.uipl.qberprovider;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.adapter.CurrentJobsAdapter;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

/**
 * Created by richa on 25/11/16.
 */
public class CurrentJobsListingFragment extends Fragment {

    String TAG = "CurrentJobsFragment";

    DialogView dialogview;
    RequestQueue mQueue;
    Preferences pref;

    RecyclerView rv;
    LinearLayoutManager llm;
    TextView tvEmpty;

    CurrentJobsAdapter adapter;

    boolean callPermission = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_current_jobs_listing, container, false);

        initViews(rootView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, 1);
        } else {
            callPermission = true;
            adapter.setCallPermission(callPermission);
        }

        adapter.notifyDataSetChanged();
        checkIfAdapterEmpty();

        JobsListingFragment.currentJobsFragmentLoaded = true;
        if (JobsListingFragment.dialogview != null && JobsListingFragment.currentJobsFragmentLoaded && JobsListingFragment.pastJobsFragmentLoaded) {
            JobsListingFragment.dialogview.dismissCustomSpinProgress();
        }

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPermission = true;
                    Log.d("tag", "granted");
                } else {
                    callPermission = false;
                    Toast.makeText(getActivity(), getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                }
                adapter.setCallPermission(callPermission);
                return;
            }
        }
    }

    private void initViews(View v) {
        dialogview = new DialogView(getActivity());
        pref = new Preferences(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());

        tvEmpty = (TextView) v.findViewById(R.id.tvEmpty);
        rv = (RecyclerView) v.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);
        adapter = new CurrentJobsAdapter(JobsListingFragment.currentJobsList, getActivity(), callPermission);
        rv.setAdapter(adapter);

        llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }

    void checkIfAdapterEmpty() {
        if (JobsListingFragment.currentJobsList.isEmpty()) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.GONE);
        }
    }

}