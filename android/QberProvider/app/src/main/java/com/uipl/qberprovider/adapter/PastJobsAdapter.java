package com.uipl.qberprovider.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.JobDetailsActivity;
import com.uipl.qberprovider.LoginRegisterActivity;
import com.uipl.qberprovider.R;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.Job;
import com.uipl.qberprovider.utils.CommonMethods;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by richa on 25/11/16.
 */
public class PastJobsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Job> jobArrayList = new ArrayList<>();
    Activity activity;
    Preferences pref;
    DialogView dialogView;
    private RequestQueue mQueue;
    public static final String TAG = "PastJobsAdapter";

    final int JOB_TAG = 0, DATE_TAG = 1, INTENT_RELOAD = 2, PAST_JOB = 1;

    public static class PastJobsViewHolder extends RecyclerView.ViewHolder {

        TextView tvHeaderLocation, tvFullAddress, tvStatus, tvSubStatus, tvTimeStarted, tvTimeStartedAmPm, tvPrice, tvTimeSpentHourValue, tvTimeSpentHour,
                tvTimeSpentMinutesValue, tvTimeSpentMin;
        LinearLayout llLocation, llFeedback, llDetails;
        ImageView iv;
        TableRow trTimeStarted, trPrice, trTimeSpent;

        public PastJobsViewHolder(View v) {
            super(v);

            tvHeaderLocation = (TextView) v.findViewById(R.id.tvHeaderLocation);
            tvFullAddress = (TextView) v.findViewById(R.id.tvFullAddress);
            tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            tvSubStatus = (TextView) v.findViewById(R.id.tvSubStatus);
            tvTimeStarted = (TextView) v.findViewById(R.id.tvTimeStarted);
            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            tvTimeSpentHourValue = (TextView) v.findViewById(R.id.tvTimeSpentHourValue);
            tvTimeSpentHour = (TextView) v.findViewById(R.id.tvTimeSpentHour);
            tvTimeSpentMinutesValue = (TextView) v.findViewById(R.id.tvTimeSpentMinutesValue);
            tvTimeSpentMin = (TextView) v.findViewById(R.id.tvTimeSpentMin);
            tvTimeStartedAmPm = (TextView) v.findViewById(R.id.tvTimeStartedAmPm);
            iv = (ImageView) v.findViewById(R.id.iv);
            llLocation = (LinearLayout) v.findViewById(R.id.llLocation);
            llFeedback = (LinearLayout) v.findViewById(R.id.llFeedback);
            llDetails = (LinearLayout) v.findViewById(R.id.llDetails);
            trTimeStarted = (TableRow) v.findViewById(R.id.trTimeStarted);
            trPrice = (TableRow) v.findViewById(R.id.trPrice);
            trTimeSpent = (TableRow) v.findViewById(R.id.trTimeSpent);
        }
    }

    public static class DateViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate, tvNumOfJobs;

        public DateViewHolder(View v) {
            super(v);

            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvNumOfJobs = (TextView) v.findViewById(R.id.tvNumOfJobs);
        }
    }

    public PastJobsAdapter(ArrayList<Job> mJobArrayList, Activity activity1) {
        this.jobArrayList = mJobArrayList;
        this.activity = activity1;
        pref = new Preferences(activity1);
        dialogView = new DialogView(activity1);
        mQueue = Volley.newRequestQueue(activity1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {

            case JOB_TAG:
                View v1 = inflater.inflate(R.layout.inflate_past_job, parent, false);
                viewHolder = new PastJobsViewHolder(v1);
                break;

            case DATE_TAG:
                View v2 = inflater.inflate(R.layout.inflate_date_past_job, parent, false);
                viewHolder = new DateViewHolder(v2);
                break;

            default:
                View v = inflater.inflate(R.layout.inflate_past_job, parent, false);
                viewHolder = new PastJobsViewHolder(v);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Job job = jobArrayList.get(position);

        switch (holder.getItemViewType()) {

            case JOB_TAG:
                PastJobsViewHolder vh1 = (PastJobsViewHolder) holder;
                if (job.getHeaderLocation() != null && !job.getHeaderLocation().toString().trim().equals("")) {
                    vh1.tvHeaderLocation.setText(activity.getResources().getString(R.string.at) + " " + job.getHeaderLocation());
                }
                if (job.getFullAddress() != null && !job.getFullAddress().toString().trim().equals("")) {
                    vh1.tvFullAddress.setVisibility(View.VISIBLE);
                    vh1.tvFullAddress.setText(job.getFullAddress());
                } else {
                    vh1.tvFullAddress.setVisibility(View.GONE);
                }
                vh1.tvPrice.setText(GlobalVariable.decimalFormat.format(Double.parseDouble(job.getPrice())));
                String status = job.getStatus();
                String subStatus = "";
                final String subStatusArr[] = status.split(" ");
                if (subStatusArr[0].toString().trim().equalsIgnoreCase("completed") || subStatusArr[0].toString().trim().equalsIgnoreCase("confirmed")
                        || subStatusArr[0].toString().trim().equalsIgnoreCase("working") || subStatusArr[0].toString().trim().equalsIgnoreCase("heading")) {
                    vh1.iv.setImageResource(R.drawable.ic_right);
                    vh1.trTimeStarted.setVisibility(View.VISIBLE);
                    vh1.trTimeSpent.setVisibility(View.VISIBLE);

                    String startTime = job.getStartTime();
                    vh1.tvTimeStarted.setText(startTime.substring(0, 5));
                    vh1.tvTimeStartedAmPm.setText(startTime.substring(6));

                    String jobDuration = job.getJobDuration();
                    if (jobDuration != null && !jobDuration.toString().trim().equals("")) {
                        int time = Integer.parseInt(jobDuration);
                        int hour = time / 60;
                        int min = time % 60;
                        if (hour > 0) {
                            vh1.tvTimeSpentHour.setVisibility(View.VISIBLE);
                            vh1.tvTimeSpentHourValue.setVisibility(View.VISIBLE);
                            vh1.tvTimeSpentHourValue.setText(String.valueOf(hour));
                        } else {
                            vh1.tvTimeSpentHour.setVisibility(View.GONE);
                            vh1.tvTimeSpentHourValue.setVisibility(View.GONE);
                        }
                        vh1.tvTimeSpentMinutesValue.setText(String.valueOf(min));
                    } else {
                        vh1.tvTimeSpentHour.setVisibility(View.GONE);
                        vh1.tvTimeSpentHourValue.setVisibility(View.GONE);
                        vh1.tvTimeSpentMinutesValue.setText(jobDuration);
                    }
                } else {
                    vh1.iv.setImageResource(R.drawable.ic_close);
                    vh1.trTimeStarted.setVisibility(View.GONE);
                    vh1.trTimeSpent.setVisibility(View.GONE);
                }

                if (subStatusArr != null && subStatusArr.length > 1) {
                    vh1.tvStatus.setText(subStatusArr[0]);
                    vh1.tvSubStatus.setVisibility(View.VISIBLE);
                    for (int i = 1; i < subStatusArr.length; i++) {
                        if (i == 1) {
                            subStatus = subStatusArr[i].substring(0, 1).toUpperCase() + subStatusArr[i].substring(1);
                        } else {
                            subStatus = subStatus + " " + subStatusArr[i].substring(0, 1).toUpperCase() + subStatusArr[i].substring(1);
                        }
                    }
                    vh1.tvSubStatus.setText(subStatus);
                } else {
                    vh1.tvStatus.setText(job.getStatus());
                    vh1.tvSubStatus.setVisibility(View.GONE);
                }

                vh1.llDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.startActivityForResult(new Intent(activity, JobDetailsActivity.class)
                                .putExtra("serviceId", job.getId())
                                .putExtra("isEditable", false), INTENT_RELOAD);
                    }
                });

                vh1.llFeedback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (subStatusArr.length == 1 && subStatusArr[0].toString().trim().equalsIgnoreCase("completed")) {
                            showFeedbackDialog(activity, job.getId(), true);
                        } else {
                            dialogView.showCustomSingleButtonDialog(activity, activity.getResources().getString(R.string.sorry),
                                    activity.getResources().getString(R.string.feedback_is_not_allowed_for_uncompleted_jobs));
                        }
                    }
                });

                vh1.llLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String label = job.getHeaderLocation() + "," + job.getFullAddress();
                        String uriBegin = "geo:" + job.getLat() + "," + job.getLng();
                        String query = job.getLat() + "," + job.getLng() + "(" + label + ")";
                        String encodedQuery = Uri.encode(query);
                        String uriString = uriBegin + "?q=" + encodedQuery;
                        Uri uri = Uri.parse(uriString);
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                        activity.startActivity(intent);
                    }
                });

                break;

            case DATE_TAG:
                DateViewHolder vh2 = (DateViewHolder) holder;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(job.getDate());
                vh2.tvDate.setText(Html.fromHtml(new SimpleDateFormat("dd").format(calendar.getTime()) + "<sup><small>"
                        + getDateSuffix(calendar.get(Calendar.DATE)) + " </small></sup>" + " " + new SimpleDateFormat("MMM").format(calendar.getTime()) + ", "
                        + new SimpleDateFormat("yyyy").format(calendar.getTime())));
                int num = Integer.parseInt(job.getNumOfJobs());
                if (num <= 1) {
                    vh2.tvNumOfJobs.setText(job.getNumOfJobs() + " " + activity.getResources().getString(R.string.job));
                } else {
                    vh2.tvNumOfJobs.setText(job.getNumOfJobs() + " " + activity.getResources().getString(R.string.jobs));
                }
                break;
        }
    }

    public void showFeedbackDialog(final Context context, final String serviceid, final boolean showRating) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_fragment_feedback);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        final ImageView imvCross = (ImageView) dialog.findViewById(R.id.imvCross);
        final Button btnSave = (Button) dialog.findViewById(R.id.btnOk);

        final EditText etComment = (EditText) dialog.findViewById(R.id.etComment);
        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);

        if (showRating) {
            ratingBar.setVisibility(View.VISIBLE);
        } else {
            ratingBar.setVisibility(View.GONE);
        }

        imvCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.hideSoftKeyboard(activity, v);
                dialog.dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.hideSoftKeyboard(activity, v);
                float rating = ratingBar.getRating();
                if (showRating && rating <= 0) {
                    dialogView.showCustomSingleButtonDialog(context, context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.rating_error));
                } else {
                    String comment = "";
                    if (etComment.getText() == null || etComment.getText().toString().trim().equals("")) {
                        comment = etComment.getText().toString();
                    }
                    if (!showRating) {
                        rating = 0;
                    }
                    dialog.dismiss();
                    volleyFeedback(pref.getStringPreference(context, Preferences.PHONE), String.valueOf(rating), comment, serviceid);
                }
            }
        });

        dialog.show();
    }

    private void volleyFeedback(final String mobile, final String rating, final String comment, final String serviceId) {
        dialogView.showCustomSpinProgress(activity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_RATE_CUSTOMER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogView.dismissCustomSpinProgress();
                        try {
                            JSONObject responseJsonObj = new JSONObject(response);

                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    Toast.makeText(activity, activity.getResources().getString(R.string.feedback_success_msg), Toast.LENGTH_SHORT).show();
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    activity.startActivity(new Intent(activity, LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogView.showCustomSingleButtonDialog(activity,
                                            activity.getResources().getString(R.string.sorry), responseJsonObj.optString("msg"));
                                }
                            } else {
                                dialogView.showCustomSingleButtonDialog(activity,
                                        activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogView.showCustomSingleButtonDialog(activity,
                                    activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogView.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogView.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogView.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.timeout_error));
                } else {
                    dialogView.showCustomSingleButtonDialog(activity,
                            activity.getResources().getString(R.string.sorry), activity.getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(activity, Preferences.TOKEN));
                params.put("service_id", serviceId);
                params.put("rating", rating);
                params.put("comment", comment);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return jobArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return jobArrayList.get(position).getType();
    }

    public String getDateSuffix(int day) {
        switch (day) {
            case 1:
            case 21:
            case 31:
                return ("st");

            case 2:
            case 22:
                return ("nd");

            case 3:
            case 23:
                return ("rd");

            default:
                return ("th");
        }
    }
}
