package com.uipl.qberprovider.Receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class StartPendingIntent {

    private static Intent intent;
    private static PendingIntent pendingIntent;

    public static void startPendingIntent(Context context, long invokeTime) {
        intent = new Intent("com.uipl.qberprovider");
        pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, invokeTime, Constant.LOOP_INTERVEL, pendingIntent);
    }
}
