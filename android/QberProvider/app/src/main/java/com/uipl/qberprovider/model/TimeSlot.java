package com.uipl.qberprovider.model;

/**
 * Created by Ankan on 29-Sep-16.
 */
public class TimeSlot {
    String fromTime, toTime;

    public TimeSlot(String fromTime, String toTime) {
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }
}
