package com.uipl.qberprovider;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.uipl.qberprovider.Receiver.StartPendingIntent;
import com.uipl.qberprovider.utils.Preferences;

public class SplashScreenActivity extends AppCompatActivity {

    Handler handler;
    private long TIME = 4000;
    private Preferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        StartPendingIntent.startPendingIntent(SplashScreenActivity.this, System.currentTimeMillis() + 1000);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, 1);
        } else {
            pref = new Preferences(SplashScreenActivity.this);
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!pref.getBooleanPreference(SplashScreenActivity.this, Preferences.IS_LOGGED_IN)) {
                        startActivity(new Intent(SplashScreenActivity.this, LoginRegisterActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(SplashScreenActivity.this, MenuActivity.class));
                        finish();
                    }

                }
            }, TIME);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("tag", "granted");
                } else {
                    Toast.makeText(this, getResources().getString(R.string.app_permission), Toast.LENGTH_SHORT).show();
                }
                pref = new Preferences(SplashScreenActivity.this);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!pref.getBooleanPreference(SplashScreenActivity.this, Preferences.IS_LOGGED_IN)) {
                            startActivity(new Intent(SplashScreenActivity.this, LoginRegisterActivity.class));
                            finish();
                        } else {
                            startActivity(new Intent(SplashScreenActivity.this, MenuActivity.class));
                            finish();
                        }

                    }
                }, TIME);
                return;
            }
        }
    }
}

