package com.uipl.qberprovider.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.utils.ConnectionActivity;
import com.uipl.qberprovider.utils.GPSTracker;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BroadCastReceiverUpdateLocation extends BroadcastReceiver {

    Context context;
    Preferences pref;
    RequestQueue mQueue;
    GPSTracker gpsTracker;
    final String TAG = "UpdateLocation";

    @Override
    public void onReceive(final Context con, Intent arg1) {
        // TODO Auto-generated method stub
        this.context = con;
        pref = new com.uipl.qberprovider.utils.Preferences(con);
        mQueue = Volley.newRequestQueue(con);
        gpsTracker = new GPSTracker(con);
        if (!pref.getStringPreference(con, Preferences.PHONE).equals("") && ConnectionActivity.isNetConnected(context)) {
            double lat = gpsTracker.getLatitude();
            double lng = gpsTracker.getLongitude();
            if (Double.compare(lat, 0.0) != 0 && Double.compare(lng, 0.0) != 0) {
                volleyUpdateLocation(String.valueOf(lat), String.valueOf(lng));
            }
        }
    }

    private void volleyUpdateLocation(final String lat, final String lng) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_UPDATE_LOCATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e(TAG, response);
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    Log.e(TAG, object.optString("msg"));
                                } else {
                                    Log.e(TAG, object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("Error", "Error");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e(TAG, error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(context, Preferences.PHONE));
                params.put("latitude", lat);
                params.put("longitude", lng);
                Log.d(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }
}
