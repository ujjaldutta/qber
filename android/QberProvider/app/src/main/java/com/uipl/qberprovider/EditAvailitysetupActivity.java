package com.uipl.qberprovider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.TextView;

public class EditAvailitysetupActivity extends AppCompatActivity implements View.OnClickListener {

    public static TextView tvToolbarTitle;
    public static ImageButton ibSave, ibMenu;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_availitysetup);
        initToolbar();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new FragmentAvailibilitySetup(), FragmentAvailibilitySetup.TAG).commit();


    }

    void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvTitle);
        ibMenu = (ImageButton) findViewById(R.id.imgBtnBack);
        ibMenu.setVisibility(View.VISIBLE);
        ibMenu.setImageResource(R.drawable.ic_back_new);
        ibMenu.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBtnBack:
                onBackPressed();
                break;
        }

    }
}
