package com.uipl.qberprovider;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.uipl.qberprovider.base.GlobalVariable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

/**
 * Created by richa on 15/12/16.
 */
public class CustomFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCMService";

    private enum PushTypes {
        jobcreated, jobcancelled, dispute, message, customerupdateservice;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        Log.d(TAG, "Data: " + remoteMessage.getData());

        if (remoteMessage.getData() != null) {
            Map<String, String> data = remoteMessage.getData();
            String type = "", customerName = "", message = "", title = "", provider_name = "";
            final String id = data.get("id");
            type = data.get("type");
            provider_name = data.get("provider_name");
            customerName = data.get("customer_name");
            message = data.get("message");

            String msg = "";

            try {
                PushTypes pushTypes = PushTypes.valueOf(type);
                switch (pushTypes) {
                    case jobcreated:
                        Log.e(TAG, "job created");
                        if (id != null && !id.toString().trim().equals("") && type != null && !type.toString().trim().equals("")) {
                            if (type.equals("jobcreated")) {
                                msg = String.format(getResources().getString(R.string.new_job_received_from), customerName);
                            }
                            PowerManager powerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
                            boolean isSceenAwake = false;
                            if (Build.VERSION.SDK_INT < 20) {
                                isSceenAwake = powerManager.isScreenOn();
                            } else {
                                isSceenAwake = powerManager.isInteractive();
                            }
                            sendNotification(msg, id, type);
                            if (isSceenAwake) {
                                Intent cIntent = new Intent(this, PushNotificationActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .putExtra("serviceId", id)
                                        .putExtra("type", type);
                                startActivity(cIntent);
                            }
                            GlobalVariable.pushNotificationServiceId = id;
                            Intent intent = new Intent(CustomFirebaseMessagingService.this, PushNotificationTimerService.class);
                            startService(intent);
                        }
                        break;
                    case jobcancelled:
                        Log.e(TAG, "job cancelled");
                        msg = String.format(getResources().getString(R.string.has_cancelled_the_service), customerName);
                        sendNotification(msg, id, type);
                        break;
                    case dispute:
                        Log.e(TAG, "dispute");
                        break;
                    case message:
                        sendMessageNotification(message, provider_name);
                        break;
                    case customerupdateservice:
                        msg = String.format(getResources().getString(R.string.has_updated_the_service), customerName);
                        sendNotification(msg, id, type);
                        break;
                    default:
                        break;
                }
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException : " + type);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void sendNotification(String messageBody, String id, String type) {
        int icon = R.mipmap.ic_app_logo2;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, getResources().getString(R.string.app_name), when);
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push_notification);
        contentView.setTextViewText(R.id.notification_time, getCurrentTime());
        contentView.setTextViewText(R.id.notification_text, messageBody);
        notification.contentView = contentView;
        if (type.equals("jobcreated")) {
            Intent cIntent = new Intent(this, PushNotificationActivity.class)
                    .putExtra("serviceId", id)
                    .putExtra("type", type);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, cIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            notification.contentIntent = resultPendingIntent;
            notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
            notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
            notification.defaults |= Notification.DEFAULT_SOUND; // Sound
            notification.flags |= Notification.FLAG_ONGOING_EVENT;
        } else {
            Intent cIntent = new Intent(this, JobDetailsActivity.class)
                    .putExtra("serviceId", id)
                    .putExtra("isEditable", true);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, cIntent, PendingIntent.FLAG_ONE_SHOT);
            notification.contentIntent = resultPendingIntent;
            notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
            notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
            notification.defaults |= Notification.DEFAULT_SOUND; // Sound
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
        }
        mNotificationManager.notify(Integer.parseInt(id), notification);
    }

    private void sendMessageNotification(String messageBody, String providerName) {
        int icon = R.mipmap.ic_app_logo2;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, getResources().getString(R.string.app_name), when);
        NotificationManager mNotificationManager;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push_notification2);
        contentView.setTextViewText(R.id.notification_time, getCurrentTime());
        contentView.setTextViewText(R.id.notification_text, messageBody);
        contentView.setTextViewText(R.id.notification_sub_title, "Message from " + providerName + ":");
        notification.contentView = contentView;
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
        notification.contentIntent = resultPendingIntent;
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(0, notification);
    }

    String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        String outputDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
        outputDate = dateFormat.format(c.getTime());
        return outputDate;
    }
}
