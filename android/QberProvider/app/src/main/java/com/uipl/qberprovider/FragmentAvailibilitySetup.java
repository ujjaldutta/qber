package com.uipl.qberprovider;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.Timeline;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import customView.TypedfaceTextView;


/**
 * Created by Ankan on 28-Sep-16.
 */
public class FragmentAvailibilitySetup extends Fragment implements View.OnClickListener {

    public static final String TAG = "FragmentAvailSetup";
    static View v;
    public static DialogView dialogview;
    private RequestQueue mQueue;
    public static ViewPager myviewpager;
    MyPagerAdapter adapter;
    private Preferences pref;

    RelativeLayout rlSun, rlMon, rlTue, rlWed, rlThu, rlFri, rlSat;
    TextView tvSun, tvMon, tvTue, tvWed, tvThu, tvFri, tvSat;
    ImageView ivSun, ivMon, ivTue, ivWed, ivThu, ivFri, ivSat;
    String[] weekDaysArr = {"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm:ss");
    SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    ArrayList<Date> dateArrList = new ArrayList<>();

    int lastPos = -1, currentPos = -1;

    public static Button btnSubmit;
    static FragmentActivity fragmentActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.fragmentActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_availibility_setup, container, false);
        // MenuActivity.tvToolbarTitle.setText(getResources().getString(R.string.availability));

        init();

        initToolbar(v);

        initLists();

        clearLists();

        volleyGetSchedule(pref.getStringPreference(fragmentActivity, Preferences.PHONE));

        setListener();

        initTimelines();

        return v;
    }

    /**
     * @param view Method to toolbar setup
     */
    void initToolbar(View view) {

        if (fragmentActivity instanceof MenuActivity) {
            MenuActivity.tvToolbarTitle.setVisibility(View.VISIBLE);
            MenuActivity.ibSave.setImageResource(R.drawable.pop_up_1);
            MenuActivity.ibSave.setVisibility(View.GONE);

            MenuActivity.tvToolbarTitle.setText(getResources().getString(R.string.availability));
        } else if (fragmentActivity instanceof EditAvailitysetupActivity) {
            EditAvailitysetupActivity.tvToolbarTitle.setVisibility(View.VISIBLE);
            EditAvailitysetupActivity.tvToolbarTitle.setText(getResources().getString(R.string.availability));
        }


    }

    void initTimelines() {
        Calendar c = Calendar.getInstance();
        for (int i = 0; i < 7; i++) {
            dateArrList.add(c.getTime());
            Log.d(TAG, dateFormat.format(c.getTime()));
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            Log.d(TAG, "" + dayOfWeek);
            if (i == 0) {
                myviewpager.setCurrentItem(dayOfWeek - 1);
            }
            c.add(Calendar.DATE, 1);
        }
    }

    private void setListener() {

        btnSubmit.setOnClickListener(this);

        myviewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (lastPos >= 0) {
                    switch (lastPos) {
                        case 0:
                            tvSun.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivSun.setVisibility(View.INVISIBLE);
                            break;
                        case 1:
                            tvMon.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivMon.setVisibility(View.INVISIBLE);
                            break;
                        case 2:
                            tvTue.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivTue.setVisibility(View.INVISIBLE);
                            break;
                        case 3:
                            tvWed.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivWed.setVisibility(View.INVISIBLE);
                            break;
                        case 4:
                            tvThu.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivThu.setVisibility(View.INVISIBLE);
                            break;
                        case 5:
                            tvFri.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivFri.setVisibility(View.INVISIBLE);
                            break;
                        case 6:
                            tvSat.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.text_color_4));
                            ivSat.setVisibility(View.INVISIBLE);
                            break;
                    }
                }

                lastPos = position;
                currentPos = position;

                switch (position) {
                    case 0:
                        tvSun.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivSun.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        tvMon.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivMon.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        tvTue.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivTue.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        tvWed.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivWed.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        tvThu.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivThu.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        tvFri.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivFri.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        tvSat.setTextColor(ContextCompat.getColor(fragmentActivity, R.color.theme_color));
                        ivSat.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void init() {
        dialogview = new DialogView(fragmentActivity);
        pref = new Preferences(fragmentActivity);
        mQueue = Volley.newRequestQueue(fragmentActivity);

        for (int i = 0; i < GlobalVariable.weeArr.length; i++) {
            GlobalVariable.weekDaysArr.add(GlobalVariable.weeArr[i]);
        }

        myviewpager = (ViewPager) v.findViewById(R.id.myviewpager);
        myviewpager.setOffscreenPageLimit(7);
        adapter = new MyPagerAdapter(fragmentActivity.getSupportFragmentManager());
        myviewpager.setAdapter(adapter);

        btnSubmit = (Button) v.findViewById(R.id.btnSubmit);

        rlSun = (RelativeLayout) v.findViewById(R.id.rlSun);
        rlSun.setOnClickListener(this);
        rlMon = (RelativeLayout) v.findViewById(R.id.rlMon);
        rlMon.setOnClickListener(this);
        rlTue = (RelativeLayout) v.findViewById(R.id.rlTue);
        rlTue.setOnClickListener(this);
        rlWed = (RelativeLayout) v.findViewById(R.id.rlWed);
        rlWed.setOnClickListener(this);
        rlThu = (RelativeLayout) v.findViewById(R.id.rlThu);
        rlThu.setOnClickListener(this);
        rlFri = (RelativeLayout) v.findViewById(R.id.rlFri);
        rlFri.setOnClickListener(this);
        rlSat = (RelativeLayout) v.findViewById(R.id.rlSat);
        rlSat.setOnClickListener(this);

        tvSun = (TypedfaceTextView) v.findViewById(R.id.tvSun);
        tvMon = (TypedfaceTextView) v.findViewById(R.id.tvMon);
        tvTue = (TypedfaceTextView) v.findViewById(R.id.tvTue);
        tvWed = (TypedfaceTextView) v.findViewById(R.id.tvWed);
        tvThu = (TypedfaceTextView) v.findViewById(R.id.tvThu);
        tvFri = (TypedfaceTextView) v.findViewById(R.id.tvFri);
        tvSat = (TypedfaceTextView) v.findViewById(R.id.tvSat);

        ivSun = (ImageView) v.findViewById(R.id.ivSun);
        ivMon = (ImageView) v.findViewById(R.id.ivMon);
        ivTue = (ImageView) v.findViewById(R.id.ivTue);
        ivWed = (ImageView) v.findViewById(R.id.ivWed);
        ivThu = (ImageView) v.findViewById(R.id.ivThu);
        ivFri = (ImageView) v.findViewById(R.id.ivFri);
        ivSat = (ImageView) v.findViewById(R.id.ivSat);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.rlSun:
                myviewpager.setCurrentItem(0);
                break;

            case R.id.rlMon:
                myviewpager.setCurrentItem(1);
                break;

            case R.id.rlTue:
                myviewpager.setCurrentItem(2);
                break;

            case R.id.rlWed:
                myviewpager.setCurrentItem(3);
                break;

            case R.id.rlThu:
                myviewpager.setCurrentItem(4);
                break;

            case R.id.rlFri:
                myviewpager.setCurrentItem(5);
                break;

            case R.id.rlSat:
                myviewpager.setCurrentItem(6);
                break;

            case R.id.btnSubmit:
                boolean isFound = false;
                for (int i = 0; i < adapter.getCount(); i++) {
                    boolean status = adapter.getFragment(i).getOverlapstatus();
                    if (status) {
                        myviewpager.setCurrentItem(i);
                        dialogview.showToast(fragmentActivity, "Unavailable time overlaping in " + weekDaysArr[i]);
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj = generateAvailabilityJsonArray();
                    if (jsonObj != null && jsonObj.length() > 0) {
                        Log.e(TAG, jsonObj.toString());
                        volleySetSchedule(pref.getStringPreference(fragmentActivity, Preferences.PHONE), jsonObj.toString());
                    } else {
                        Toast.makeText(fragmentActivity, "No Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private void volleySetSchedule(final String mobile, final String s) {
        dialogview.showCustomSpinProgress(fragmentActivity);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SET_SCHEDULE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        dialogview.dismissCustomSpinProgress();
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    String str_message = object.optString("msg");
                                    Toast.makeText(fragmentActivity, str_message, Toast.LENGTH_SHORT).show();
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    String str_message = object.optString("msg");
                                    dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), str_message);
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("details", s);
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void volleyGetSchedule(final String mobile) {
        dialogview.showCustomSpinProgress(fragmentActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_SCHEDULE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        try {
                            Log.e(TAG, response);
                            JSONObject responseJsonObj = new JSONObject(response);
                            if (responseJsonObj != null) {
                                if (responseJsonObj.optString("success").equals("0")) {
                                    JSONObject scheduleJsonObj = responseJsonObj.optJSONObject("schedule");
                                    if (scheduleJsonObj != null) {
                                        clearLists();
                                        setData(scheduleJsonObj);
                                    } else {
                                        dialogview.dismissCustomSpinProgress();
                                        String str_message = responseJsonObj.optString("msg");
                                        dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), str_message);
                                    }
                                } else if (responseJsonObj.optString("success").equals("1")) {
                                    for (int i = 0; i < 7; i++) {
                                        GlobalVariable.isAvailableArray[i] = true;
                                    }

                                    adapter = new MyPagerAdapter(fragmentActivity.getSupportFragmentManager());
                                    myviewpager.setAdapter(adapter);

                                    setListener();

                                    initTimelines();

                                    FragmentAvailibilitySetup.myviewpager.setVisibility(View.VISIBLE);
                                    FragmentAvailibilitySetup.btnSubmit.setVisibility(View.VISIBLE);
                                    FragmentAvailibilitySetup.dialogview.dismissCustomSpinProgress();
                                } else {
                                    dialogview.dismissCustomSpinProgress();
                                    String str_message = responseJsonObj.optString("msg");
                                    dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), str_message);
                                }
                            } else {
                                dialogview.dismissCustomSpinProgress();
                                dialogview.showCustomSingleButtonDialog(fragmentActivity,
                                        getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(fragmentActivity, getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(fragmentActivity,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(fragmentActivity,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(fragmentActivity,
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", mobile);
                Log.e(TAG, params.toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    void clearLists() {
        for (int i = 0; i < GlobalVariable.timelinesArrayList.size(); i++) {
            GlobalVariable.timelinesArrayList.get(i).clear();
        }
    }

    void initLists() {
        for (int i = 0; i < 7; i++) {
            GlobalVariable.timelinesArrayList.add(new ArrayList<Timeline>());
        }
    }

    public void setData(JSONObject scheduleJsonObj) {

        for (int i = 0; i < 7; i++) {
            GlobalVariable.isAvailableArray[i] = false;
        }

        JSONObject sundayJsonObj = scheduleJsonObj.optJSONObject("sunday");
        JSONObject mondayJsonObj = scheduleJsonObj.optJSONObject("monday");
        JSONObject tuesdayJsonObj = scheduleJsonObj.optJSONObject("tuesday");
        JSONObject wednesdayJsonObj = scheduleJsonObj.optJSONObject("wednesday");
        JSONObject thursdayJsonObj = scheduleJsonObj.optJSONObject("thursday");
        JSONObject fridayJsonObj = scheduleJsonObj.optJSONObject("friday");
        JSONObject saturdayJsonObj = scheduleJsonObj.optJSONObject("saturday");

        if (sundayJsonObj != null && sundayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(0).addAll(saveDataToArraylist(sundayJsonObj, 0));
        }

        if (mondayJsonObj != null && mondayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(1).addAll(saveDataToArraylist(mondayJsonObj, 1));
        }

        if (tuesdayJsonObj != null && tuesdayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(2).addAll(saveDataToArraylist(tuesdayJsonObj, 2));
        }

        if (wednesdayJsonObj != null && wednesdayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(3).addAll(saveDataToArraylist(wednesdayJsonObj, 3));
        }

        if (thursdayJsonObj != null && thursdayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(4).addAll(saveDataToArraylist(thursdayJsonObj, 4));
        }

        if (fridayJsonObj != null && fridayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(5).addAll(saveDataToArraylist(fridayJsonObj, 5));
        }

        if (saturdayJsonObj != null && saturdayJsonObj.length() > 0) {
            GlobalVariable.timelinesArrayList.get(6).addAll(saveDataToArraylist(saturdayJsonObj, 6));
        }

        adapter = new MyPagerAdapter(fragmentActivity.getSupportFragmentManager());
        myviewpager.setAdapter(adapter);

        setListener();

        initTimelines();

        FragmentAvailibilitySetup.myviewpager.setVisibility(View.VISIBLE);
        FragmentAvailibilitySetup.btnSubmit.setVisibility(View.VISIBLE);
        FragmentAvailibilitySetup.dialogview.dismissCustomSpinProgress();
    }

    ArrayList<Timeline> saveDataToArraylist(JSONObject dayJsonObj, int fragmentPos) {
        ArrayList<Timeline> resultArrList = new ArrayList<>();
        try {
            String workingStartTime = date12Format.format(date24Format.parse(dayJsonObj.optString("day_start")));
            String workingEndTime = date12Format.format(date24Format.parse(dayJsonObj.optString("day_end")));
            int breakTimePosition = 0;
            int i = 0;
            JSONObject obj = dayJsonObj.optJSONObject(String.valueOf(i));
            while (obj != null && obj.length() > 0) {
                Timeline t = new Timeline();
                String startTimeString = date12Format.format(date24Format.parse(obj.optString("from_time")));
                String endTimeString = date12Format.format(date24Format.parse(obj.optString("to_time")));
                t.setStartTime(startTimeString);
                t.setEndTime(endTimeString);
                t.setWorkingStartTime(workingStartTime);
                t.setWorkingEndTime(workingEndTime);
                Date sTime = date24Format.parse(date24Format.format(date12Format.parse(startTimeString)));
                Date eTime = date24Format.parse(date24Format.format(date12Format.parse(endTimeString)));
                t.setStartDateTime(sTime);
                t.setEndDateTime(eTime);
                float diff = 0;
                diff = getDiffInHours(sTime, eTime);
                if (diff > 0) {
                    t.setDiffInHours(diff);
                    String type = obj.optString("time_type");
                    t.setActive(true);
                    if (type != null) {
                        type = type.toString().trim();
                        if (type.equalsIgnoreCase("unavailable")) {
                            t.setAvailable(false);
                            t.setBreakTimePosition(breakTimePosition);
                            breakTimePosition++;
                        } else if (type.equalsIgnoreCase("free")) {
                            t.setAvailable(true);
                            t.setBreakTimePosition(-1);
                        }
                    }
                    resultArrList.add(t);
                }
                i++;
                obj = dayJsonObj.optJSONObject(String.valueOf(i));
            }

            if (resultArrList.size() == 1) {
                obj = dayJsonObj.optJSONObject("0");
                String type = obj.optString("time_type");
                if (type.equalsIgnoreCase("unavailable")) {
                    Timeline t = resultArrList.get(0);
                    t.setAvailable(true);
                    resultArrList.set(0, t);
                    GlobalVariable.isAvailableArray[fragmentPos] = false;
                } else {
                    GlobalVariable.isAvailableArray[fragmentPos] = true;
                }
            } else {
                GlobalVariable.isAvailableArray[fragmentPos] = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return checkForDuplicates(resultArrList);
    }

    float getDiffInHours(Date startTime, Date endTime) {
        float result = 0;
        long difference = endTime.getTime() - startTime.getTime();
        float diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(difference);
        float d = diffInMinutes % 60;
        float diffInHours = TimeUnit.MILLISECONDS.toHours(difference);
        if (d > 0) {
            diffInHours = diffInHours + (d / 60);
        }
        BigDecimal bd = new BigDecimal(Float.toString(diffInHours));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        result = bd.floatValue();
        if (result >= 0) {
            return result;
        } else {
            return 0;
        }
    }

    ArrayList<Timeline> checkForDuplicates(ArrayList<Timeline> arrTemp) {
        ArrayList<Timeline> result = new ArrayList<>();
        for (Timeline t : arrTemp) {
            boolean isFound = false;
            for (Timeline t2 : result) {
                if (t.getStartTime().equals(t2.getStartTime()) && t.getEndTime().equals(t2.getEndTime()) && t.isAvailable() == t2.isAvailable()) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                result.add(t);
            }
        }
        return result;
    }

    JSONObject generateAvailabilityJsonArray() {

        JSONArray availabilityJsonArr = new JSONArray();
        JSONObject jsonObject = new JSONObject();

        Calendar c = Calendar.getInstance();
        for (int i = 0; i < 7; i++) {
            String date = dateFormat.format(c.getTime());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            JSONObject jsonObj = new JSONObject();

            jsonObj = convertArrListToJsonObj(GlobalVariable.timelinesArrayList.get(dayOfWeek - 1), date, adapter.getFragment(dayOfWeek - 1).cbAvailable.isChecked());

            if (jsonObj != null && jsonObj.length() > 0) {
                availabilityJsonArr.put(jsonObj);
            }
            c.add(Calendar.DATE, 1);
        }

        try {
            if (availabilityJsonArr != null && availabilityJsonArr.length() > 0) {
                jsonObject.put("availibility", availabilityJsonArr);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    JSONObject convertArrListToJsonObj(ArrayList<Timeline> timelineArrList, String date, boolean isActive) {
        int size = timelineArrList.size();
        JSONObject mainJsonObj = new JSONObject();
        if (size > 0) {
            try {
                mainJsonObj.put("date", date);
                mainJsonObj.put("startTime", date24Format.format(date12Format.parse(timelineArrList.get(0).getWorkingStartTime())));
                mainJsonObj.put("endTime", date24Format.format(date12Format.parse(timelineArrList.get(0).getWorkingEndTime())));
                JSONArray breakTimeJsonArr = new JSONArray();
                if (isActive) {
                    for (int i = 0; i < size; i++) {
                        Timeline t = timelineArrList.get(i);
                        if (t.isActive() && !t.isAvailable()) {
                            JSONObject breakTimeJsonObj = new JSONObject();
                            breakTimeJsonObj.put("start", date24Format.format(date12Format.parse(t.getStartTime())));
                            breakTimeJsonObj.put("end", date24Format.format(date12Format.parse(t.getEndTime())));
                            breakTimeJsonArr.put(breakTimeJsonObj);
                        }
                    }
                }
                if (isActive == false) {
                    mainJsonObj.put("isActive", "false");
                } else {
                    mainJsonObj.put("isActive", "true");
                }
                mainJsonObj.put("unavailableTime", breakTimeJsonArr);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return mainJsonObj;
    }

    public void resetFragments(int currentPos) {

        dialogview.showCustomSpinProgress(fragmentActivity);

        init();

        myviewpager.setVisibility(View.INVISIBLE);
        btnSubmit.setVisibility(View.INVISIBLE);

        myviewpager.setCurrentItem(currentPos);

        setListener();

        FragmentAvailibilitySetup.myviewpager.setVisibility(View.VISIBLE);
        FragmentAvailibilitySetup.btnSubmit.setVisibility(View.VISIBLE);
        FragmentAvailibilitySetup.dialogview.dismissCustomSpinProgress();

    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        SparseArray<FragmentAvailibility> arr = new SparseArray<>();

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public FragmentAvailibility getFragment(int key) {
            return arr.get(key);
        }

        @Override
        public Fragment getItem(int pos) {

            String workingStartTime = "6:00 AM", workingEndTime = "11:59 PM";

            if (!GlobalVariable.timelinesArrayList.get(pos).isEmpty()) {
                workingStartTime = GlobalVariable.timelinesArrayList.get(pos).get(0).getWorkingStartTime();
                workingEndTime = GlobalVariable.timelinesArrayList.get(pos).get(0).getWorkingEndTime();
            }

            FragmentAvailibility myFragment = FragmentAvailibility.newInstance(pos, workingStartTime, workingEndTime);
            arr.put(pos, myFragment);
            return myFragment;
        }

        @Override
        public int getCount() {
            return 7;
        }
    }
}
