package com.uipl.qberprovider.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;

/**
 * Created by pallab on 23/11/16.
 */

public class ServiceSetting {
    private boolean selected;
    private LatLng centerLanLng;
    private Polygon polygon;
    private String preferenceId;
    private String preferenceName;

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setCenterLanLng(LatLng centerLanLng) {
        this.centerLanLng = centerLanLng;
    }

    public LatLng getCenterLanLng() {
        return centerLanLng;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public void setPreferenceId(String preferenceId) {
        this.preferenceId = preferenceId;
    }

    public String getPreferenceId() {
        return preferenceId;
    }

    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public String getPreferenceName() {
        return preferenceName;
    }
}
