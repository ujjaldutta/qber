package com.uipl.qberprovider.model;

/**
 * Created by Ankan on 03-Nov-16.
 */
public class DayState {
    private int startTime, endTime;

    public DayState(int startTime, int endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }
}
