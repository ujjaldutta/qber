package com.uipl.qberprovider.model;

/**
 * Created by pallab on 31/10/16.
 */

public class ServiceDetails {
    private String serviceName="";
    private boolean selectFlag=false;

    public ServiceDetails(String s, boolean b) {
        this.serviceName=s;
        this.selectFlag=b;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public boolean isSelectFlag() {
        return selectFlag;
    }

    public void setSelectFlag(boolean selectFlag) {
        this.selectFlag = selectFlag;
    }
}
