package com.uipl.qberprovider.base;

/**
 * Created by richa on 9/1/17.
 */
public class Constants {

    public static class Timer{
        public static final int wait_timer = 13000; // 13 seconds
        public static final int confirm_timer = 180000; // 3 minutes or 180 seconds
    }

    public static class PushNotificationTimerBroadcast{
        public static final String PUSH_NOTIFICATION_TIMER_BROADCAST_ACTION = "com.uipl.qberprovider.TIMER_ACTION";
    }

    public static class suvDetailPrice {
        public static final double pressureSopWashPrice = 50.0;
        public static final double interiorVaccumCleaningPrice = 23.0;
        public static final double tyreShiningPrice = 7.0;
    }

    public static class suvDetailId {
        public static final String pressureSopWashId = "1";
        public static final String interiorVaccumCleaningId = "4";
        public static final String tyreShiningId = "5";
    }

    public static class sedanDetailPrice {
        public static final double pressureSopWashPrice = 35.0;
        public static final double interiorVaccumCleaningPrice = 18.0;
        public static final double tyreShiningPrice = 17.0;
    }

    public static class sedanDetailId {
        public static final String pressureSopWashId = "2";
        public static final String interiorVaccumCleaningId = "6";
        public static final String tyreShiningId = "7";
    }

    public static class typeId {
        public static final String carWashType = "1";
    }
}
