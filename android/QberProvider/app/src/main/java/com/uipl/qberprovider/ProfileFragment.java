package com.uipl.qberprovider;


import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.uipl.qberprovider.adapter.CommonAdapter;
import com.uipl.qberprovider.base.ConFig_URL;
import com.uipl.qberprovider.base.GlobalVariable;
import com.uipl.qberprovider.model.Reviews_Model;
import com.uipl.qberprovider.utils.CaptureImage;
import com.uipl.qberprovider.utils.CommonMethods;
import com.uipl.qberprovider.utils.Compress;
import com.uipl.qberprovider.utils.CompressionImage;
import com.uipl.qberprovider.utils.DateFormatter;
import com.uipl.qberprovider.utils.DialogView;
import com.uipl.qberprovider.utils.InputVadidation;
import com.uipl.qberprovider.utils.MultiSpinner;
import com.uipl.qberprovider.utils.MultipartRequest;
import com.uipl.qberprovider.utils.Permission;
import com.uipl.qberprovider.utils.Preferences;
import com.uipl.qberprovider.utils.SnackBarCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener, Compress, RadioGroup.OnCheckedChangeListener, MultiSpinner.MultiSpinnerListener {

    TextView tvProfileName, tvToolbarTitle, tvReviews, tvNoReview;
    ImageView ivProfile, ivBanner1, ivBanner2, ivBanner3, imgEditBanner2, imgProfileEdit, imgEditBanner1, imgEditBanner3;
    Toolbar toolbar;
    ImageButton ibBack, ibSave;
    ScrollView svMain;
    LinearLayout llReviews;


    RequestQueue mQueue;

    ImageLoader imageLoader;
    DisplayImageOptions options;

    ArrayList<Reviews_Model> review_list = new ArrayList<Reviews_Model>();


    public static String TAG = "ProfileFragment";

    private EditText etName, etEmail, etMobile, etTellUsAns, etShopName, etPassword, etConfirmPassword;
    private Spinner spBirthYear, spNationality, spYouPrefer, spYearExp, spLiveInQatar;
    MultiSpinner spLanguages;
    private TextView tvMale, tvFemale, tvISDCode, tvProfilePic, tvProfileSelect, tvPhotoTools, tvPhotoToolsSelect, tvPhotoShop,
            tvPhotoShopSelect, tvAttachCV, tvAttachCVSelect, tvCarRegistration, tvCarRegistrationSelect, tvDrivingLicense,
            tvDrivingLicenseSelect, tvIDImage, tvIDImageSelect;

    private LinearLayout llAdditionalView;
    private RadioButton rbMobileInternet, rbWifi, rbMobileWifiInternet, rbCarMotorcycle, rbTaxiFriendCar, rbWalkBicycle, RdBtnYes, RdBtnNo;
    private RadioGroup rgIsCompany, rgInternet, rgTransport;
    ArrayList<String> birthYear = new ArrayList<>();
    ArrayList<String> expYear = new ArrayList<>();
    ArrayList<HashMap<String, String>> nationalityArray = new ArrayList<>();
    ArrayList<HashMap<String, String>> preferLocation = new ArrayList<>();
    ArrayList<String> languageList = new ArrayList<>();
    boolean[] selectLanguageList;
    private String typeId = "", gender = "male";
    int dltUserProfile = 0, dltBanner1 = 0, dltBanner2 = 0, dltBanner3 = 0;

    /*capture image*/
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "QberCustomer";
    private static int LOAD_IMAGE_RESULTS_FROM_CAMERA = 100;
    private static int LOAD_IMAGE_RESULTS_FROM_GALLERY = 200;
    private static int PIC_CROP = 201;
    private int PICKFILE_RESULT_CODE = 300;
    private String ImagePath = "", picImageFor = "", language = "";
    private File fileProfile, fileTools, fileShop, fileCV, fileRegister, fileLicense, fileID,
            fileBanner1, fileBanner2, fileBanner3, fileUserProfile;
    private LinearLayout llPersonalProfile, llPublicProfile, llPerBasicProfile, llPerOptionalProfile;
    private TextView tvPublic, tvPersonal, tvOptional, tvBasic, tvAccountNumber;
    private DialogView dialogview;
    private Preferences pref;
    private TextView tvRate, tvShopName;
    private RatingBar professionalRatingBar;
    private TextView tvVerifiedLink, tvNotVerified, tvChangePassword;
    private RelativeLayout rlEmailVerified;
    String currentPass, newPass, confirmPass;
    boolean isNewPassShowing = false, isConfirmPassShowing = false;
    private boolean removeFlag = false;
    private ImageButton btnImgUserDetails;
    private EditText tvDesc;
    private boolean changeFlag = true;
    private boolean birthYearFlag = true;
    private boolean nationalityFlag = true;
    private boolean yearExpFlag = true;
    private boolean liveInQatarFlag = true;
    private boolean yourPreferFlag = true;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        birthYear = getBirthYear();
        expYear = getExpYear();
        languageArraySetUp();
        initViews(view);
        initToolbar(view);
        volleyNationalityTask();
        return view;
    }

    private void languageArraySetUp() {
        languageList.add("English");
        languageList.add("Qatari");
        languageList.add("Hindi");
        selectLanguageList = new boolean[languageList.size()];
        for (int i = 0; i < selectLanguageList.length; i++) {
            if (i == 0) {
                selectLanguageList[i] = true;
                language = languageList.get(i);
            } else {
                selectLanguageList[i] = false;
            }
        }
    }

    private ArrayList<String> getBirthYear() {
        ArrayList<String> yearList = new ArrayList<>();
        yearList.add(getString(R.string.select_birth_year));
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = year - 18; i > year + 18 - 100; i--) {
            yearList.add(String.valueOf(i));
        }
        return yearList;
    }

    private ArrayList<String> getExpYear() {
        ArrayList<String> yearList = new ArrayList<>();
        yearList.add("Less than 1 year");
        yearList.add("1-2");
        yearList.add("2-4");
        yearList.add("4-8");
        yearList.add("8-15");
        yearList.add("15-25");
        yearList.add(" 25+");
        return yearList;
    }

    /**
     * Method to call web services for country list
     */
    private void volleyNationalityTask() {
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SERVICE_COUNTRY_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    HashMap<String, String> hashMap1 = new HashMap<>();
                                    hashMap1.put("name", getString(R.string.select_nationality));
                                    hashMap1.put("code", "");
                                    nationalityArray.add(hashMap1);
                                    JSONArray jsonArray = object.optJSONArray("provider");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.optJSONObject(i);
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("name", obj.optString("countryname"));
                                        hashMap.put("code", obj.optString("code"));
                                        nationalityArray.add(hashMap);
                                    }
                                    volleyPreferLocationTask();
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.dismissCustomSpinProgress();
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * Method to call web services for Prefer location
     */
    private void volleyPreferLocationTask() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_PREFERENCE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONArray jsonArray = object.optJSONArray("locations");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.optJSONObject(i);
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("name", obj.optString("name"));
                                        hashMap.put("id", obj.optString("id"));
                                        preferLocation.add(hashMap);
                                    }
                                    volleyGetProfile();
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.dismissCustomSpinProgress();
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    /**
     * Method to call web services for get profile details
     */
    private void volleyGetProfile() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_GET_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        svMain.setVisibility(View.VISIBLE);
                        try {
                            JSONObject object = new JSONObject(response);
                            review_list.clear();
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    JSONObject providerObj = object.optJSONObject("provider");
                                    changeFlag = true;

                                    tvProfileName.setText(providerObj.optString("name"));
                                    etName.setText(providerObj.optString("name"));
                                    if (!providerObj.optString("email").trim().equals("null")) {
                                        etEmail.setText(providerObj.optString("email"));
                                    }
                                    if (etEmail.getText().toString().trim().equals("")) {
                                        rlEmailVerified.setVisibility(View.GONE);
                                    } else {
                                        rlEmailVerified.setVisibility(View.VISIBLE);
                                    }
                                    if (providerObj.optString("email_verified").equals("1")) {
                                        tvNotVerified.setVisibility(View.GONE);
                                        tvVerifiedLink.setText(getString(R.string.email_verified));
                                        tvVerifiedLink.setTextColor(getResources().getColor(R.color.light_green));
                                    } else {
                                        tvNotVerified.setVisibility(View.VISIBLE);
                                        tvVerifiedLink.setText(getString(R.string.send_verification_link));
                                        tvVerifiedLink.setTextColor(getResources().getColor(R.color.text_color_light_blue));
                                    }
                                    tvAccountNumber.setText(getString(R.string.account_number) + " " + providerObj.optString("account_no"));
                                    String phone = pref.getStringPreference(getActivity(), Preferences.PHONE).substring(GlobalVariable.ISD_CODE.length());
                                    etMobile.setText(phone);

                                    JSONObject profileObj = providerObj.optJSONObject("profile");
                                    DataSetup(profileObj.optString("service_details"), profileObj.optString("image"), profileObj.optString("nationality"), profileObj.optString("language_preference"), profileObj.optString("banner_img1"), profileObj.optString("banner_img2"),
                                            profileObj.optString("banner_img3"), profileObj.optString("date_of_birth"), profileObj.optString("gender"),
                                            profileObj.optString("internet_access"), profileObj.optString("transport"), profileObj.optString("your_offer"),
                                            profileObj.optString("year_of_experience"), profileObj.optString("live_in_qatar"), profileObj.optString("work_preference"),
                                            profileObj.optString("work_in_company"), profileObj.optString("shop_name"), profileObj.optString("admin_profile_image"), profileObj.optString("tools_use_image"), profileObj.optString("shop_image"),
                                            profileObj.optString("cv_file"), profileObj.optString("car_registration_image"), profileObj.optString("driving_license_image"),
                                            profileObj.optString("identity_image"));
                                    JSONArray jsonRatingArray = object.optJSONArray("profilerating");
                                    if (jsonRatingArray != null) {
                                        if (jsonRatingArray.length() > 0) {
                                            String rating = jsonRatingArray.optJSONObject(0).getString("rating");
                                            int ratecount = jsonRatingArray.optJSONObject(0).getInt("ratecount");
                                            if (rating != null && ratecount > 0) {
                                                professionalRatingBar.setRating(Float.parseFloat(rating));
                                                tvRate.setText(String.format("%.2f", Float.parseFloat(rating)) + "(" + String.valueOf(ratecount) + ")");
                                            }
                                        }
                                    }
                                    JSONArray reviewArray = providerObj.optJSONArray("ratings");
                                    for (int i = 0; i < reviewArray.length(); i++) {
                                        JSONObject reviewObj = reviewArray.getJSONObject(i);
                                        Reviews_Model review = new Reviews_Model();
                                        review.setName(reviewObj.optString("name"));
                                        review.setComments(reviewObj.optString("comment"));
                                        review.setDate(DateFormatter.getFormattedTime("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", reviewObj.optString("date")));
                                        String r = reviewObj.optString("rate_value");
                                        if (r != null && !r.toString().trim().equals("") && !r.toString().trim().equals("null")) {
                                            review.setRateValue(Float.parseFloat(r));
                                        } else {
                                            review.setRateValue(0.0f);
                                        }
                                        review_list.add(review);
                                    }
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(object.optString("msg"))
                                            .setTitle(getResources().getString(R.string.sorry))
                                            .setCancelable(false)
                                            .setPositiveButton(getResources().getString(R.string.ok_txt), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //onBackPressed();
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                                if (review_list.isEmpty()) {
                                    //tvReviews.setVisibility(View.GONE);
                                    llReviews.setVisibility(View.GONE);
                                    tvNoReview.setVisibility(View.VISIBLE);
                                } else {
                                    tvNoReview.setVisibility(View.GONE);
                                    setReviews();
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    private void DataSetup(String service_details, String image, String nationality, String language_preference, String banner_img1, String
            banner_img2, String banner_img3, String date_of_birth, String mGender, String internet_access,
                           String transport, String your_offer, String year_of_experience, String live_in_qatar,
                           String work_preference, String work_in_company, String shop_name, String admin_profile_image, String tools_use_image,
                           String shop_image, String cv_file, String car_registration_image,
                           String driving_license_image, String identity_image) {


        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item, birthYear);
        birthYearFlag = true;
        spBirthYear.setAdapter(arrayAdapter);

        CommonAdapter arrayAdapter2 = new CommonAdapter(getActivity(), nationalityArray);
        nationalityFlag = true;
        spNationality.setAdapter(arrayAdapter2);


        CommonAdapter arrayAdapter3 = new CommonAdapter(getActivity(), preferLocation);
        yourPreferFlag = true;
        spYouPrefer.setAdapter(arrayAdapter3);
        changeFlag = true;
        spLanguages.setItems(languageList, selectLanguageList, this);

        ArrayAdapter arrayAdapter4 = new ArrayAdapter(getActivity(), R.layout.spinner_item, expYear);
        arrayAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearExpFlag = true;
        spYearExp.setAdapter(arrayAdapter4);

        ArrayAdapter arrayAdapter5 = new ArrayAdapter(getActivity(), R.layout.spinner_item, expYear);
        arrayAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        liveInQatarFlag = true;
        spLiveInQatar.setAdapter(arrayAdapter4);
        if (service_details != null && !service_details.equals("") && !service_details.equals("null")) {
            tvDesc.setText(service_details);
        }

        if (image != null && !image.toString().trim().equals("") && !image.toString().trim().equals("null")) {
            imageLoader.displayImage(ConFig_URL.IMAGE_URL + image, ivProfile, options);
            imgProfileEdit.setVisibility(View.VISIBLE);
        }
        if (banner_img1 != null && !banner_img1.toString().trim().equals("") && !banner_img1.toString().trim().equals("null")) {
            imageLoader.displayImage(ConFig_URL.IMAGE_URL + banner_img1, ivBanner1, options);
            imgEditBanner1.setVisibility(View.VISIBLE);
        }
        if (banner_img2 != null && !banner_img2.toString().trim().equals("") && !banner_img2.toString().trim().equals("null")) {
            imageLoader.displayImage(ConFig_URL.IMAGE_URL + banner_img2, ivBanner2, options);
            imgEditBanner2.setVisibility(View.VISIBLE);
        }
        if (banner_img3 != null && !banner_img3.toString().trim().equals("") && !banner_img3.toString().trim().equals("null")) {
            imageLoader.displayImage(ConFig_URL.IMAGE_URL + banner_img3, ivBanner3, options);
            imgEditBanner3.setVisibility(View.VISIBLE);
        }
        if (mGender.equals("male")) {
            tvMale.setBackgroundResource(R.drawable.rounded_purple_bg);
            tvMale.setTextColor(getResources().getColor(R.color.white));
            tvFemale.setBackgroundResource(R.color.transparent);
            tvFemale.setTextColor(getResources().getColor(R.color.black));
            gender = "male";
        } else if (mGender.equals("female")) {
            tvFemale.setBackgroundResource(R.drawable.rounded_purple_bg);
            tvFemale.setTextColor(getResources().getColor(R.color.white));
            tvMale.setBackgroundResource(R.color.transparent);
            tvMale.setTextColor(getResources().getColor(R.color.black));
            gender = "female";
        }
        etTellUsAns.setText(your_offer);

        for (int i = 0; i < expYear.size(); i++) {
            if (expYear.get(i).toString().trim().equals(live_in_qatar.trim())) {
                liveInQatarFlag = true;
                spLiveInQatar.setSelection(i);
                break;
            }
        }
        for (int i = 0; i < expYear.size(); i++) {
            if (expYear.get(i).toString().trim().equals(year_of_experience.trim())) {
                yearExpFlag = true;
                spYearExp.setSelection(i);
                break;
            }
        }

        if (shop_name != null && !shop_name.toString().trim().equals("") && !shop_name.toString().trim().equals("null")) {
            RdBtnYes.setChecked(true);
            etShopName.setVisibility(View.VISIBLE);
            etShopName.setText(shop_name);
        } else {
            RdBtnNo.setChecked(true);
            etShopName.setVisibility(View.GONE);
        }

        for (int i = 0; i < nationalityArray.size(); i++) {
            if (nationalityArray.get(i).get("code").equals(nationality)) {
                nationalityFlag = true;
                spNationality.setSelection(i);
                break;
            }
        }
        for (int j = 0; j < birthYear.size(); j++) {
            if (birthYear.get(j).toString().equals(date_of_birth)) {
                birthYearFlag = true;
                spBirthYear.setSelection(j);
                break;
            }
        }
        if (internet_access.equals("mobile")) {
            rbMobileInternet.setChecked(true);
        } else if (internet_access.equals("wifi")) {
            rbWifi.setChecked(true);
        } else if (internet_access.equals("mobile+wifi")) {
            rbMobileWifiInternet.setChecked(true);
        }

        if (transport.equals("car")) {
            rbCarMotorcycle.setChecked(true);
        } else if (transport.equals("taxi")) {
            rbTaxiFriendCar.setChecked(true);
        } else if (transport.equals("walk")) {
            rbWalkBicycle.setChecked(true);
        }

        for (int k = 0; k < preferLocation.size(); k++) {
            if (preferLocation.get(k).get("id").equals(work_preference)) {
                yourPreferFlag = true;
                spYouPrefer.setSelection(k);
                break;
            }
        }

        if (language_preference != null && !language_preference.equals("") && !language_preference.equals("null")) {
            for (int i = 0; i < selectLanguageList.length; i++) {
                selectLanguageList[i] = false;
            }
            String[] str = language_preference.split(",");
            for (int i = 0; i < str.length; i++) {
                language = language_preference;
                for (int j = 0; j < languageList.size(); j++) {
                    if (str[i].toString().trim().equals(languageList.get(j).toString().trim())) {
                        selectLanguageList[j] = true;
                        break;
                    }
                }
            }
            spLanguages.setItems(languageList, selectLanguageList, this);
        }

        if (admin_profile_image != null && !admin_profile_image.toString().trim().equals("") && !admin_profile_image.toString().trim().equals("null")) {
            String imgName = admin_profile_image.substring(admin_profile_image.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvProfilePic.setText(imgName);
            }
        }
        if (tools_use_image != null && !tools_use_image.toString().trim().equals("") && !tools_use_image.toString().trim().equals("null")) {
            String imgName = tools_use_image.substring(tools_use_image.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvPhotoTools.setText(imgName);
            }
        }
        if (shop_image != null && !shop_image.toString().trim().equals("") && !shop_image.toString().trim().equals("null")) {
            String imgName = shop_image.substring(shop_image.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvPhotoShop.setText(imgName);
            }
        }
        if (cv_file != null && !cv_file.toString().trim().equals("") && !cv_file.toString().trim().equals("null")) {
            String imgName = cv_file.substring(cv_file.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvAttachCV.setText(imgName);
            }
        }
        if (car_registration_image != null && !car_registration_image.toString().trim().equals("") && !car_registration_image.toString().trim().equals("null")) {
            String imgName = car_registration_image.substring(car_registration_image.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvCarRegistration.setText(imgName);
            }
        }
        if (driving_license_image != null && !driving_license_image.toString().trim().equals("") && !driving_license_image.toString().trim().equals("null")) {
            String imgName = driving_license_image.substring(driving_license_image.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvDrivingLicense.setText(imgName);
            }
        }
        if (identity_image != null && !identity_image.toString().trim().equals("") && !identity_image.toString().trim().equals("null")) {
            String imgName = identity_image.substring(identity_image.lastIndexOf("/") + 1);
            if (imgName != null) {
                tvIDImage.setText(imgName);
            }
        }

        changeFlag = false;

    }


    /**
     * Method to show Reviews Details
     */
    void setReviews() {
        llReviews.removeAllViewsInLayout();
        for (int i = 0; i < review_list.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.customer_reviews_item, llReviews, false);
            ImageView iv_first_alphabet = (ImageView) view.findViewById(R.id.iv_first_alphabet);
            TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
            RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            TextView tv_comments = (TextView) view.findViewById(R.id.tv_comments);
            TextView tv_date = (TextView) view.findViewById(R.id.tv_date);

            //get first letter of each String item
            if (!review_list.get(i).getName().equals("")) {
                String firstLetter = String.valueOf(review_list.get(i).getName().charAt(0));
                ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
                int color = generator.getRandomColor();
                TextDrawable drawable = TextDrawable.builder().buildRound(firstLetter.toUpperCase(), color); // radius in px
                iv_first_alphabet.setImageDrawable(drawable);
            }
            tv_name.setText(review_list.get(i).getName());
            ratingBar.setRating(review_list.get(i).getRateValue());
            tv_comments.setText(review_list.get(i).getComments());
            tv_date.setText(review_list.get(i).getDate());
            llReviews.addView(view);
        }
    }

    /**
     * @param view Method to initialize Variable
     */
    private void initViews(View view) {


        dialogview = new DialogView(getActivity());
        pref = new Preferences(getActivity());
        mQueue = Volley.newRequestQueue(getActivity());
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_placeholder)
                .showImageForEmptyUri(R.drawable.profile_placeholder)
                .showImageOnFail(R.drawable.profile_placeholder).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        llPersonalProfile = (LinearLayout) view.findViewById(R.id.llPersonalProfile);
        llPublicProfile = (LinearLayout) view.findViewById(R.id.llPublicProfile);
        tvPublic = (TextView) view.findViewById(R.id.tvPublic);
        tvPersonal = (TextView) view.findViewById(R.id.tvPersonal);

        llPerBasicProfile = (LinearLayout) view.findViewById(R.id.llPerBasicProfile);
        llPerOptionalProfile = (LinearLayout) view.findViewById(R.id.llPerOptionalProfile);
        tvBasic = (TextView) view.findViewById(R.id.tvBasic);
        tvOptional = (TextView) view.findViewById(R.id.tvOptional);

        tvProfileName = (TextView) view.findViewById(R.id.tv_profile_name);
        tvDesc = (EditText) view.findViewById(R.id.tv_profile_details);
        editTextChangeListener(tvDesc);
        tvDesc.setEnabled(false);
        btnImgUserDetails = (ImageButton) view.findViewById(R.id.btnImgUserDetails);
        btnImgUserDetails.setOnClickListener(this);
        tvReviews = (TextView) view.findViewById(R.id.tv_Reviews);
        tvNoReview = (TextView) view.findViewById(R.id.tvNoReview);
        professionalRatingBar = (RatingBar) view.findViewById(R.id.professionalRatingBar);
        tvRate = (TextView) view.findViewById(R.id.tvRate);
        ivProfile = (ImageView) view.findViewById(R.id.iv_profileimage);
        imgProfileEdit = (ImageView) view.findViewById(R.id.imgProfileEdit);
        imgEditBanner1 = (ImageView) view.findViewById(R.id.imgEditBanner1);
        imgEditBanner2 = (ImageView) view.findViewById(R.id.imgEditBanner2);
        imgEditBanner3 = (ImageView) view.findViewById(R.id.imgEditBanner3);
        ivProfile.setOnClickListener(this);
        ivBanner1 = (ImageView) view.findViewById(R.id.ivBanner1);
        ivBanner2 = (ImageView) view.findViewById(R.id.ivBanner2);
        ivBanner3 = (ImageView) view.findViewById(R.id.ivBanner3);
        svMain = (ScrollView) view.findViewById(R.id.svPublicProfile);
        llReviews = (LinearLayout) view.findViewById(R.id.llReviews);

        tvPublic.setOnClickListener(this);
        tvPersonal.setOnClickListener(this);
        tvBasic.setOnClickListener(this);
        tvOptional.setOnClickListener(this);
        ivBanner1.setOnClickListener(this);
        ivBanner2.setOnClickListener(this);
        ivBanner3.setOnClickListener(this);


        etName = (EditText) view.findViewById(R.id.etName);
        editTextChangeListener(etName);
        spBirthYear = (Spinner) view.findViewById(R.id.spBirthYear);
        spBirthYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!birthYearFlag) {
                    dataChange();
                } else {
                    birthYearFlag = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spBirthYear.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);
                return false;
            }
        });
        spNationality = (Spinner) view.findViewById(R.id.spNationality);
        spNationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!nationalityFlag) {
                    dataChange();
                } else {
                    nationalityFlag = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spNationality.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);
                return false;
            }
        });
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        editTextChangeListener(etEmail);
        rlEmailVerified = (RelativeLayout) view.findViewById(R.id.rlEmailVerified);
        tvVerifiedLink = (TextView) view.findViewById(R.id.tvVerifiedLink);
        tvNotVerified = (TextView) view.findViewById(R.id.tvNotVerified);
        tvVerifiedLink.setOnClickListener(this);
        tvChangePassword = (TextView) view.findViewById(R.id.tvChangePassword);
        tvChangePassword.setOnClickListener(this);
        tvMale = (TextView) view.findViewById(R.id.tvMale);
        tvMale.setOnClickListener(this);
        tvFemale = (TextView) view.findViewById(R.id.tvFemale);
        tvFemale.setOnClickListener(this);
        tvISDCode = (TextView) view.findViewById(R.id.tvISDCode);
        tvISDCode.setText("+" + GlobalVariable.ISD_CODE);
        etMobile = (EditText) view.findViewById(R.id.etMobile);
        etPassword = (EditText) view.findViewById(R.id.etPassword);

        etConfirmPassword = (EditText) view.findViewById(R.id.etConfirmPassword);


        rgInternet = (RadioGroup) view.findViewById(R.id.rgInternet);
        rgInternet.setOnCheckedChangeListener(this);
        rbMobileInternet = (RadioButton) view.findViewById(R.id.rbMobileInternet);
        rbWifi = (RadioButton) view.findViewById(R.id.rbWifi);
        rbMobileWifiInternet = (RadioButton) view.findViewById(R.id.rbMobileWifiInternet);
        rbMobileInternet.setChecked(true);

        rgTransport = (RadioGroup) view.findViewById(R.id.rgTransport);
        rgTransport.setOnCheckedChangeListener(this);
        rbCarMotorcycle = (RadioButton) view.findViewById(R.id.rbCarMotorcycle);
        rbTaxiFriendCar = (RadioButton) view.findViewById(R.id.rbTaxiFriendCar);
        rbWalkBicycle = (RadioButton) view.findViewById(R.id.rbWalkBicycle);
        rbCarMotorcycle.setChecked(true);

        tvAccountNumber = (TextView) view.findViewById(R.id.tvAccountNumber);

        // Optional Information

        etTellUsAns = (EditText) view.findViewById(R.id.etTellUsAns);
        editTextChangeListener(etTellUsAns);
        spYearExp = (Spinner) view.findViewById(R.id.spYearExp);
        spYearExp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!yearExpFlag) {
                    dataChange();
                } else {
                    yearExpFlag = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spYearExp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);
                return false;
            }
        });
        spLiveInQatar = (Spinner) view.findViewById(R.id.spLiveInQatar);
        spLiveInQatar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!liveInQatarFlag) {
                    dataChange();
                } else {
                    liveInQatarFlag = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spLiveInQatar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);

                return false;
            }
        });
        spYouPrefer = (Spinner) view.findViewById(R.id.spYouPrefer);
        spYouPrefer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!yourPreferFlag) {
                    dataChange();
                } else {
                    yourPreferFlag = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spYouPrefer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);
                return false;
            }
        });
        spLanguages = (MultiSpinner) view.findViewById(R.id.spLanguages);
        spLanguages.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);
                return false;
            }
        });
        rgIsCompany = (RadioGroup) view.findViewById(R.id.rgIsCompany);
        rgIsCompany.setOnCheckedChangeListener(this);
        RdBtnYes = (RadioButton) view.findViewById(R.id.RdBtnYes);
        RdBtnNo = (RadioButton) view.findViewById(R.id.RdBtnNo);
        etShopName = (EditText) view.findViewById(R.id.etShopName);
        editTextChangeListener(etShopName);
        tvShopName = (TextView) view.findViewById(R.id.tvShopName);
        RdBtnNo.setChecked(true);

        tvProfilePic = (TextView) view.findViewById(R.id.tvProfilePic);
        tvProfileSelect = (TextView) view.findViewById(R.id.tvProfileSelect);
        tvProfileSelect.setOnClickListener(this);
        tvPhotoTools = (TextView) view.findViewById(R.id.tvPhotoTools);
        tvPhotoToolsSelect = (TextView) view.findViewById(R.id.tvPhotoToolsSelect);
        tvPhotoToolsSelect.setOnClickListener(this);
        tvPhotoShop = (TextView) view.findViewById(R.id.tvPhotoShop);
        tvPhotoShopSelect = (TextView) view.findViewById(R.id.tvPhotoShopSelect);
        tvPhotoShopSelect.setOnClickListener(this);
        tvAttachCV = (TextView) view.findViewById(R.id.tvAttachCV);
        tvAttachCVSelect = (TextView) view.findViewById(R.id.tvAttachCVSelect);
        tvAttachCVSelect.setOnClickListener(this);
        tvCarRegistration = (TextView) view.findViewById(R.id.tvCarRegistration);
        tvCarRegistrationSelect = (TextView) view.findViewById(R.id.tvCarRegistrationSelect);
        tvCarRegistrationSelect.setOnClickListener(this);
        tvDrivingLicense = (TextView) view.findViewById(R.id.tvDrivingLicense);
        tvDrivingLicenseSelect = (TextView) view.findViewById(R.id.tvDrivingLicenseSelect);
        tvDrivingLicenseSelect.setOnClickListener(this);
        tvIDImage = (TextView) view.findViewById(R.id.tvIDImage);
        tvIDImageSelect = (TextView) view.findViewById(R.id.tvIDImageSelect);
        tvIDImageSelect.setOnClickListener(this);


    }


    /**
     * @param view Method to toolbar setup
     */
    void initToolbar(View view) {


        MenuActivity.tvToolbarTitle.setVisibility(View.VISIBLE);
        MenuActivity.ibSave.setImageResource(R.drawable.grey_tick);
        MenuActivity.ibSave.setVisibility(View.VISIBLE);
        MenuActivity.ibSave.setEnabled(false);

        MenuActivity.tvToolbarTitle.setText(getResources().getString(R.string.profile));

        MenuActivity.ibSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {
                    volleyUpdateProfileTask();
                }

            }
        });
    }

    public void setUpUpdateFlag() {
        if (!changeFlag) {
            dataChange();

        }
    }

    public void dataChange() {
        MenuActivity.ibSave.setEnabled(true);
        MenuActivity.ibSave.setImageResource(R.drawable.pop_up_1);
    }


    @Override
    public void onClick(View v) {
        CommonMethods.hideSoftKeyboard(getActivity(), v);
        switch (v.getId()) {


            case R.id.tvPublic:
                llPublicProfile.setVisibility(View.VISIBLE);
                llPersonalProfile.setVisibility(View.GONE);
                tvPublic.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvPublic.setTextColor(getResources().getColor(R.color.white));
                tvPersonal.setBackgroundResource(R.color.transparent);
                tvPersonal.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.tvPersonal:
                llPersonalProfile.setVisibility(View.VISIBLE);
                llPublicProfile.setVisibility(View.GONE);
                tvPersonal.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvPersonal.setTextColor(getResources().getColor(R.color.white));
                tvPublic.setBackgroundResource(R.color.transparent);
                tvPublic.setTextColor(getResources().getColor(R.color.black));
                break;

            case R.id.tvBasic:
                llPerBasicProfile.setVisibility(View.VISIBLE);
                llPerOptionalProfile.setVisibility(View.GONE);
                tvBasic.setBackgroundResource(R.drawable.rounded_top_left_corner_blue);
                tvBasic.setTextColor(getResources().getColor(R.color.white));
                tvOptional.setBackgroundResource(R.color.transparent);
                tvOptional.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.tvOptional:
                llPerBasicProfile.setVisibility(View.GONE);
                llPerOptionalProfile.setVisibility(View.VISIBLE);
                tvOptional.setBackgroundResource(R.drawable.rounded_top_right_corner_blue);
                tvOptional.setTextColor(getResources().getColor(R.color.white));
                tvBasic.setBackgroundResource(R.color.transparent);
                tvBasic.setTextColor(getResources().getColor(R.color.black));

                break;

            case R.id.tvMale:
                tvMale.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvMale.setTextColor(getResources().getColor(R.color.white));
                tvFemale.setBackgroundResource(R.color.transparent);
                tvFemale.setTextColor(getResources().getColor(R.color.black));
                gender = "male";
                setUpUpdateFlag();
                break;
            case R.id.tvFemale:
                tvFemale.setBackgroundResource(R.drawable.rounded_purple_bg);
                tvFemale.setTextColor(getResources().getColor(R.color.white));
                tvMale.setBackgroundResource(R.color.transparent);
                tvMale.setTextColor(getResources().getColor(R.color.black));
                gender = "female";
                setUpUpdateFlag();
                break;

            case R.id.iv_profileimage:
                picImageFor = "UserProfile";
                if (imgProfileEdit.getVisibility() == View.VISIBLE) {
                    removeFlag = true;
                } else {
                    removeFlag = false;
                }
                PermissionChecking();
                break;
            case R.id.ivBanner1:
                picImageFor = "Banner1";
                if (imgEditBanner1.getVisibility() == View.VISIBLE) {
                    removeFlag = true;
                } else {
                    removeFlag = false;
                }
                PermissionChecking();
                break;

            case R.id.ivBanner2:
                picImageFor = "Banner2";
                if (imgEditBanner2.getVisibility() == View.VISIBLE) {
                    removeFlag = true;
                } else {
                    removeFlag = false;
                }
                PermissionChecking();
                break;
            case R.id.ivBanner3:
                picImageFor = "Banner3";
                if (imgEditBanner3.getVisibility() == View.VISIBLE) {
                    removeFlag = true;
                } else {
                    removeFlag = false;
                }
                PermissionChecking();
                break;
            case R.id.tvProfileSelect:
                picImageFor = "Profile";
                removeFlag = false;
                PermissionChecking();
                break;
            case R.id.tvPhotoToolsSelect:
                picImageFor = "Tools";
                removeFlag = false;
                PermissionChecking();
                break;
            case R.id.tvPhotoShopSelect:
                picImageFor = "Shop";
                removeFlag = false;
                PermissionChecking();
                break;
            case R.id.tvAttachCVSelect:
                picImageFor = "CV";
                //captureImage();
                OpenFile();


                break;
            case R.id.tvCarRegistrationSelect:
                picImageFor = "Register";
                removeFlag = false;
                PermissionChecking();
                break;
            case R.id.tvDrivingLicenseSelect:
                picImageFor = "License";
                OpenFile();
                break;
            case R.id.tvIDImageSelect:
                picImageFor = "ID";
                OpenFile();
                break;
            case R.id.tvVerifiedLink:
                if (tvVerifiedLink.getText().toString().trim().equals(getString(R.string.send_verification_link))) {
                    volleyEmailVerified();
                }

                break;
            case R.id.tvChangePassword:
                showChangePassDialog();
                break;
            case R.id.btnImgUserDetails:

                if (tvDesc.isEnabled()) {
                    tvDesc.setEnabled(false);
                    btnImgUserDetails.setImageResource(R.drawable.ic_edit);
                } else {
                    tvDesc.setEnabled(true);
                    btnImgUserDetails.setImageResource(R.drawable.ic_edit_purple);
                    tvDesc.requestFocus();
                    CommonMethods.showSoftKeyboard(getActivity(), tvDesc);
                }
                break;


        }
    }

    public void OpenFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*|application/pdf/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Choose a Files"),
                    PICKFILE_RESULT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            dialogview.showToast(getActivity(), "Please install a Files Manager.");
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        setUpUpdateFlag();
        if (i == R.id.RdBtnYes) {
            tvShopName.setVisibility(View.VISIBLE);
            etShopName.setVisibility(View.VISIBLE);
        } else if (i == R.id.RdBtnNo) {
            tvShopName.setVisibility(View.GONE);
            etShopName.setVisibility(View.GONE);

        }
    }

    private void volleyEmailVerified() {
        dialogview.showCustomSpinProgress(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_EMAIL_VERIFIED,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        svMain.setVisibility(View.VISIBLE);
                        try {
                            JSONObject object = new JSONObject(response);
                            review_list.clear();
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.success), object.optString("msg"));
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }

                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

    /**
     * Method to call web services for Update Profile
     */
    private void volleyUpdateProfileTask() {
        dialogview.showCustomSpinProgress(getActivity());
        String internetAccess = getInternetAccess();
        String transport = getTransport();
        String isShop = getShopflag();
        HashMap<String, File> fileParams = new HashMap<>();
        if (fileProfile != null) {
            fileParams.put("admin_profile_image", fileProfile);
        }
        if (fileTools != null) {
            fileParams.put("tools_use_image", fileTools);
        }
        if (fileShop != null) {
            fileParams.put("shop_image", fileShop);
        }
        if (fileCV != null) {
            fileParams.put("cv_file", fileCV);
        }
        if (fileRegister != null) {
            fileParams.put("car_registration_image", fileRegister);
        }
        if (fileLicense != null) {
            fileParams.put("driving_license_image", fileLicense);
        }
        if (fileID != null) {
            fileParams.put("identity_image", fileID);
        }
        if (fileBanner1 != null) {
            fileParams.put("banner_img1", fileBanner1);
        }
        if (fileBanner2 != null) {
            fileParams.put("banner_img2", fileBanner2);
        }
        if (fileBanner3 != null) {
            fileParams.put("banner_img3", fileBanner3);
        }
        if (fileUserProfile != null) {
            fileParams.put("profile_image", fileUserProfile);
        }


        HashMap<String, String> params = new HashMap<>();
        params.put("phone", GlobalVariable.ISD_CODE + etMobile.getText().toString());
        params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
        params.put("name", etName.getText().toString());
        params.put("email", etEmail.getText().toString());
        params.put("service_details", tvDesc.getText().toString().trim());
        params.put("type_id", typeId);
        int pos = spNationality.getSelectedItemPosition();
        if (pos > -1 && pos < nationalityArray.size()) {
            params.put("nationality", nationalityArray.get(pos).get("code"));
        }
        params.put("date_of_birth", spBirthYear.getSelectedItem().toString());
        params.put("gender", gender);
        params.put("internet_access", internetAccess);
        params.put("transport", transport);
        params.put("your_offer", etTellUsAns.getText().toString());
        params.put("year_of_experience", spYearExp.getSelectedItem().toString());
        params.put("live_in_qatar", spLiveInQatar.getSelectedItem().toString());
        int pos2 = spYouPrefer.getSelectedItemPosition();
        if (pos2 > -1 && pos2 < preferLocation.size()) {
            params.put("work_preference", preferLocation.get(pos2).get("id"));
        }
        params.put("language_preference", language);
        params.put("work_in_company", isShop);
        if (isShop.equals("yes")) {
            params.put("shop_name", etShopName.getText().toString());
        }
        params.put("profile_image_delete", String.valueOf(dltUserProfile));
        params.put("banner_img1_delete", String.valueOf(dltBanner1));
        params.put("banner_img2_delete", String.valueOf(dltBanner2));
        params.put("banner_img3_delete", String.valueOf(dltBanner3));

        MultipartRequest request = new MultipartRequest(ConFig_URL.URL_UPDATE_PROFILE, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                /**/
                dialogview.dismissCustomSpinProgress();
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("success").equals("0")) {
                        tvDesc.setEnabled(false);
                        dialogview.showCustomSingleButtonDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), object.optString("msg"));
                    } else if (object.optString("success").equals("1")) {
                        pref.clearAllPref();
                        startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                }
            }
        }, fileParams, params);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        Volley.newRequestQueue(getActivity()).add(request);
    }

    private String getShopflag() {
        String str = "";
        int radioButtonID = rgIsCompany.getCheckedRadioButtonId();
        View radioButton = rgIsCompany.findViewById(radioButtonID);
        int idx = rgIsCompany.indexOfChild(radioButton);
        if (idx == 0) {
            str = "yes";
        } else if (idx == 1) {
            str = "no";
        }

        return str;
    }

    /**
     * @return String
     * Method to get selected transport option
     */
    private String getTransport() {
        String str = "";
        int radioButtonID = rgTransport.getCheckedRadioButtonId();
        View radioButton = rgTransport.findViewById(radioButtonID);
        int idx = rgTransport.indexOfChild(radioButton);
        if (idx == 0) {
            str = "car";
        } else if (idx == 1) {
            str = "taxi";
        } else if (idx == 2) {
            str = "walk";

        }

        return str;
    }

    /**
     * @return String
     * Method to get selected Internet access option
     */
    private String getInternetAccess() {
        String str = "";
        int radioButtonID = rgInternet.getCheckedRadioButtonId();
        View radioButton = rgInternet.findViewById(radioButtonID);
        int idx = rgInternet.indexOfChild(radioButton);
        if (idx == 0) {
            str = "mobile";
        } else if (idx == 1) {
            str = "wifi";
        } else if (idx == 2) {
            str = "mobile+wifi";

        }

        return str;
    }

    /**
     * @return true if all the data are valid
     * Method to check validation
     */
    public boolean Validation() {
        if (etName.getText().toString().trim().isEmpty()) {
            SnackBarCustom.showSnackWithActionButton(etName, getResources().getString(R.string.please_enter_name));
            etName.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etName, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (spBirthYear.getSelectedItem().toString().equals(getString(R.string.select_birth_year))) {
            SnackBarCustom.showSnackWithActionButton(spBirthYear, getResources().getString(R.string.select_birth_year));
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(spBirthYear, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (nationalityArray.get(spNationality.getSelectedItemPosition()).get("name").equals(getString(R.string.select_nationality))) {
            SnackBarCustom.showSnackWithActionButton(spNationality, getResources().getString(R.string.select_nationality));
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(spNationality, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (!etEmail.getText().toString().trim().isEmpty() && !InputVadidation.isEditTextContainEmail(etEmail)) {
            etEmail.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etEmail, getResources().getString(R.string.please_enter_valid_email));
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
            return false;
        } else if (etMobile.getText().toString().trim().isEmpty()) {
            etMobile.requestFocus();
            SnackBarCustom.showSnackWithActionButton(etMobile, getResources().getString(R.string.please_enter_mobile_number));
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etMobile, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }

        return true;
    }

    public void PermissionChecking() {
        if (Permission.selfPermissionGranted(getActivity(), android.Manifest.permission.CAMERA)) {
            if (CaptureImage.isDeviceSupportCamera(getActivity())) {
                if (removeFlag) {
                    captureImage2();
                } else {
                    captureImage();
                }

            } else {
                dialogview.showToast(getActivity(), "Camera not supported");
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (CaptureImage.isDeviceSupportCamera(getActivity())) {
                        if (removeFlag) {
                            captureImage2();
                        } else {
                            captureImage();
                        }
                    } else {
                        dialogview.showToast(getActivity(), "Camera not supported");
                    }
                } else {
                    dialogview.showToast(getActivity(), getResources().getString(R.string.app_permission));
                }
                return;
            }
        }
    }

    private void captureImage() {


        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    fileUri = CaptureImage.getOutputMediaFileUri(getActivity(),
                            MEDIA_TYPE_IMAGE, IMAGE_DIRECTORY_NAME, MEDIA_TYPE_IMAGE);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, LOAD_IMAGE_RESULTS_FROM_CAMERA);


                } else if (items[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, getResources().getString(R.string.select_file)),
                            LOAD_IMAGE_RESULTS_FROM_GALLERY);

                }
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    private void captureImage2() {


        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_from_gallery), getResources().getString(R.string.remove_photo)};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    fileUri = CaptureImage.getOutputMediaFileUri(getActivity(),
                            MEDIA_TYPE_IMAGE, IMAGE_DIRECTORY_NAME, MEDIA_TYPE_IMAGE);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, LOAD_IMAGE_RESULTS_FROM_CAMERA);


                } else if (items[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, getResources().getString(R.string.select_file)),
                            LOAD_IMAGE_RESULTS_FROM_GALLERY);

                } else if (items[item].equals(getResources().getString(R.string.remove_photo))) {
                    removePhoto(picImageFor);
                }
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == LOAD_IMAGE_RESULTS_FROM_CAMERA
                && resultCode == Activity.RESULT_OK) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (picImageFor.equals("UserProfile") || picImageFor.equals("Banner1") || picImageFor.equals("Banner2") || picImageFor.equals("Banner3")) {
                    performCrop(fileUri);
                } else {
                    ImagePath = CaptureImage.getPath(getActivity(),
                            fileUri);
                    CompressionImage imageCompression = new CompressionImage(getActivity(), ImagePath, picImageFor);
                    imageCompression.setMethod(this);
                    imageCompression.CompressSize();
                }

            } else {
                if (picImageFor.equals("UserProfile") || picImageFor.equals("Banner1") || picImageFor.equals("Banner2") || picImageFor.equals("Banner3")) {
                    performCrop(fileUri);
                } else {
                    ImagePath = CaptureImage.getPath(getActivity(),
                            fileUri);
                    CompressionImage imageCompression = new CompressionImage(getActivity(), ImagePath, picImageFor);
                    imageCompression.setMethod(this);
                    imageCompression.CompressSize();
                }


            }


        } else if (requestCode == LOAD_IMAGE_RESULTS_FROM_GALLERY
                && resultCode == Activity.RESULT_OK && data != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Uri pickedImage = data.getData();
            if (picImageFor.equals("UserProfile") || picImageFor.equals("Banner1") || picImageFor.equals("Banner2") || picImageFor.equals("Banner3")) {
                performCrop(pickedImage);
            } else {
                ImagePath = CaptureImage.getPath(getActivity(),
                        pickedImage);

                CompressionImage imageCompression = new CompressionImage(getActivity(), ImagePath, picImageFor);
                imageCompression.setMethod(this);
                imageCompression.CompressSize();
            }
        } else if (requestCode == PICKFILE_RESULT_CODE
                && resultCode == Activity.RESULT_OK && data != null) {
            Uri pickedImage = data.getData();
            ImagePath = CaptureImage.getPath(getActivity(),
                    pickedImage);
            getCompressData(ImagePath, picImageFor);
        } else if (requestCode == PIC_CROP
                && resultCode == Activity.RESULT_OK && data != null) {
            try {
                //get the returned data
                Bundle extras = data.getExtras();
//get the cropped bitmap
                Bitmap thePic = null;
                if (extras != null) {
                    thePic = extras.getParcelable("data");
                } else {
                    Uri uri = data.getData();
                    thePic = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                }
                if (thePic != null) {
                    Uri pickedImage = getImageUri(getActivity(), thePic);
                    ImagePath = CaptureImage.getPath(getActivity(),
                            pickedImage);

                    CompressionImage imageCompression = new CompressionImage(getActivity(), ImagePath, picImageFor);
                    imageCompression.setMethod(this);
                    imageCompression.CompressSize();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void removePhoto(String picImageFor) {
        setUpUpdateFlag();
        if (picImageFor.equals("Banner1")) {
            fileBanner1 = null;
            ivBanner1.setImageResource(R.drawable.download_2);
            dltBanner1 = 1;
            imgEditBanner1.setVisibility(View.GONE);

        } else if (picImageFor.equals("Banner2")) {
            fileBanner2 = null;
            ivBanner2.setImageResource(R.drawable.download_2);
            dltBanner2 = 1;
            imgEditBanner2.setVisibility(View.GONE);
        } else if (picImageFor.equals("Banner3")) {
            fileBanner3 = null;
            ivBanner3.setImageResource(R.drawable.download_2);
            dltBanner3 = 1;
            imgEditBanner3.setVisibility(View.GONE);
        } else if (picImageFor.equals("UserProfile")) {
            fileUserProfile = null;
            ivProfile.setImageResource(R.drawable.profile_placeholder);
            dltUserProfile = 1;
            imgProfileEdit.setVisibility(View.GONE);
        }

    }

    @Override
    public void getCompressData(String imagePath, String picImageFor) {
        setUpUpdateFlag();
        if (picImageFor.equals("Profile") && imagePath.length() > 0) {
            fileProfile = new File(imagePath);
            String fileName = fileProfile.getName();
            if (fileName != null)
                tvProfilePic.setText(fileName);
        } else if (picImageFor.equals("Tools") && imagePath.length() > 0) {
            fileTools = new File(imagePath);
            String fileName = fileTools.getName();
            if (fileName != null)
                tvPhotoTools.setText(fileName);
        } else if (picImageFor.equals("Shop") && imagePath.length() > 0) {
            fileShop = new File(imagePath);
            String fileName = fileShop.getName();
            if (fileName != null)
                tvPhotoShop.setText(fileName);
        } else if (picImageFor.equals("CV") && imagePath.length() > 0) {
            fileCV = new File(imagePath);
            String fileName = fileCV.getName();
            if (fileName != null)
                tvAttachCV.setText(fileName);
        } else if (picImageFor.equals("Register") && imagePath.length() > 0) {
            fileRegister = new File(imagePath);
            String fileName = fileRegister.getName();
            if (fileName != null)
                tvCarRegistration.setText(fileName);
        } else if (picImageFor.equals("License") && imagePath.length() > 0) {
            fileLicense = new File(imagePath);
            String fileName = fileLicense.getName();
            if (fileName != null)
                tvDrivingLicense.setText(fileName);
        } else if (picImageFor.equals("ID") && imagePath.length() > 0) {
            fileID = new File(imagePath);
            String fileName = fileID.getName();
            if (fileName != null)
                tvIDImage.setText(fileName);
        } else if (picImageFor.equals("Banner1") && imagePath.length() > 0) {
            fileBanner1 = new File(imagePath);
            imageLoader.displayImage("file://" + imagePath, ivBanner1, options);
            dltBanner1 = 0;
            imgEditBanner1.setVisibility(View.VISIBLE);
        } else if (picImageFor.equals("Banner2") && imagePath.length() > 0) {
            fileBanner2 = new File(imagePath);
            imageLoader.displayImage("file://" + imagePath, ivBanner2, options);
            dltBanner2 = 0;
            imgEditBanner2.setVisibility(View.VISIBLE);
        } else if (picImageFor.equals("Banner3") && imagePath.length() > 0) {
            fileBanner3 = new File(imagePath);
            imageLoader.displayImage("file://" + imagePath, ivBanner3, options);
            dltBanner3 = 0;
            imgEditBanner3.setVisibility(View.VISIBLE);
        } else if (picImageFor.equals("UserProfile") && imagePath.length() > 0) {
            fileUserProfile = new File(imagePath);
            imageLoader.displayImage("file://" + imagePath, ivProfile, options);
            dltUserProfile = 0;
            imgProfileEdit.setVisibility(View.VISIBLE);
        }
    }

    public void editTextChangeListener(EditText et) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!changeFlag) {
                    dataChange();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    @Override
    public void onItemsSelected(boolean[] selected, String allText) {
        language = allText;
        setUpUpdateFlag();
    }


    private void showChangePassDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_change_password);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText etCurrentPass, etNewPass, etConfirmPass;
        final ImageView ivNewPass, ivConfirmPass, ivClose;
        Button btnSave;

        etCurrentPass = (EditText) dialog.findViewById(R.id.etCurrentPass);
        etNewPass = (EditText) dialog.findViewById(R.id.etNewPass);
        etConfirmPass = (EditText) dialog.findViewById(R.id.etConfirmPass);
        ivNewPass = (ImageView) dialog.findViewById(R.id.ivNewPass);
        ivConfirmPass = (ImageView) dialog.findViewById(R.id.ivConfirmPass);
        ivClose = (ImageView) dialog.findViewById(R.id.ivClose);
        btnSave = (Button) dialog.findViewById(R.id.btnSave);

        isNewPassShowing = false;
        isConfirmPassShowing = false;

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.hideSoftKeyboard(getActivity(), view);
                dialog.dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.hideSoftKeyboard(getActivity(), v);
                currentPass = "";
                newPass = "";
                confirmPass = "";
                if (etCurrentPass.getText() != null) {
                    currentPass = etCurrentPass.getText().toString().trim();
                }
                if (etNewPass.getText() != null) {
                    newPass = etNewPass.getText().toString().trim();
                }
                if (etConfirmPass.getText() != null) {
                    confirmPass = etConfirmPass.getText().toString().trim();
                }
                if (isPasswordValid(currentPass, newPass, confirmPass)) {
                    volleyChangePassword();
                    dialog.dismiss();
                }
            }
        });

        ivNewPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNewPassShowing) {
                    isNewPassShowing = false;
                    etNewPass.setTransformationMethod(new PasswordTransformationMethod());
                    ivNewPass.setImageResource(R.drawable.eye_closed);
                } else {
                    isNewPassShowing = true;
                    etNewPass.setTransformationMethod(null);
                    ivNewPass.setImageResource(R.drawable.eye_open);
                }
            }
        });

        ivConfirmPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConfirmPassShowing) {
                    isConfirmPassShowing = false;
                    etConfirmPass.setTransformationMethod(new PasswordTransformationMethod());
                    ivConfirmPass.setImageResource(R.drawable.eye_closed);
                } else {
                    isConfirmPassShowing = true;
                    etConfirmPass.setTransformationMethod(null);
                    ivConfirmPass.setImageResource(R.drawable.eye_open);
                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                CommonMethods.hideSoftKeyboard(getActivity(), etNewPass);
            }
        });

        dialog.show();
    }

    boolean isPasswordValid(String currentPass, String newPass, String confirmPass) {
        if (currentPass == null || currentPass.toString().trim().equals("")) {
            dialogview.showCustomSingleButtonDialog(getActivity(), "Error", "Current Password is empty");
            return false;
        } else if (newPass == null || newPass.toString().trim().equals("")) {
            dialogview.showCustomSingleButtonDialog(getActivity(), "Error", "New Password is empty");
            return false;
        } else if (confirmPass == null || confirmPass.toString().trim().equals("")) {
            dialogview.showCustomSingleButtonDialog(getActivity(), "Error", "Confirm Password is empty");
            return false;
        } else if (newPass.toString().trim().length() < 8 || confirmPass.toString().trim().length() < 8 || currentPass.toString().trim().length() < 8) {
            dialogview.showCustomSingleButtonDialog(getActivity(), "Error", "Minimum 8 characters required");
            return false;
        } else if (!newPass.equals(confirmPass)) {
            dialogview.showCustomSingleButtonDialog(getActivity(), "Error", "Password Mismatch");
            return false;
        }
        return true;
    }

    private void volleyChangePassword() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConFig_URL.URL_SAVE_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialogview.dismissCustomSpinProgress();
                        svMain.setVisibility(View.VISIBLE);
                        try {
                            JSONObject object = new JSONObject(response);
                            review_list.clear();
                            if (object != null) {
                                if (object.optString("success").equals("0")) {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.success), object.optString("msg"));
                                } else if (object.optString("success").equals("1")) {
                                    pref.clearAllPref();
                                    startActivity(new Intent(getActivity(), LoginRegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    dialogview.showCustomSingleButtonDialog(getActivity(),
                                            getResources().getString(R.string.sorry), object.optString("msg"));
                                }
                            }
                        } catch (JSONException e) {
                            dialogview.showCustomSingleButtonDialog(getActivity(),
                                    getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                dialogview.dismissCustomSpinProgress();
                if (error instanceof NoConnectionError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.no_net_connection));
                } else if (error instanceof com.android.volley.TimeoutError) {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.timeout_error));
                } else {
                    dialogview.showCustomSingleButtonDialog(getActivity(),
                            getResources().getString(R.string.sorry), getResources().getString(R.string.internal_error));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", pref.getStringPreference(getActivity(), Preferences.PHONE));
                params.put("token", pref.getStringPreference(getActivity(), Preferences.TOKEN));
                params.put("current_password", currentPass);
                params.put("password", newPass);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(GlobalVariable.Connection_Time_Out, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);

    }

}
