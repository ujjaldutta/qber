package customView;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by richa on 25/11/16.
 */
public class HelveticaneueBoldTextView extends TextView{
    private Context mContext;
    public HelveticaneueBoldTextView(Context context) {
        super(context);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }

    public HelveticaneueBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }

    public HelveticaneueBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HelveticaneueBoldTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }
    private void setUp() {
        setTypeface(Typeface.createFromAsset(mContext.getAssets(), "HelveticaNeue-Bold.otf"));
    }
}
