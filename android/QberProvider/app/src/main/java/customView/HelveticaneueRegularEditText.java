package customView;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Pallab on 9/27/2016.
 */
public class HelveticaneueRegularEditText extends EditText {
    private  Context mContext;

    public HelveticaneueRegularEditText(Context context) {
        super(context);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }
    public HelveticaneueRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }

    public HelveticaneueRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HelveticaneueRegularEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            this.mContext = context;
            setUp();
        }
    }

    private void setUp() {
        setTypeface(Typeface.createFromAsset(mContext.getAssets(), "HELVETICANEUE_1.TTF"));
    }
}
