<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{url('company/dashboard')}}">Qber App {{ $title }}</a>
    </div>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
               
                <li>
                    <a href="{{url('company/dashboard')}}">
                        <i class="fa fa-dashboard fa-fw"></i> Dashboard
                    </a>
                </li>


                <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Services
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

                        <li>
                             <a href="{{url('company/service')}}">
                                <i class="glyphicon glyphicon-list"></i> Recent Service
                            </a>
                        </li>
                       
                        
                    </ul>
                </li>

               

                <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Providers
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

                        <li>
                             <a href="{{url('company/providers')}}">
                                <i class="glyphicon glyphicon-list"></i> List Providers
                            </a>
                        </li>
                        <li>
                             <a href="{{url('company/providers/invite')}}">
                                <i class="glyphicon glyphicon-list"></i> Invlite Provider
                            </a>
                        </li>
                        <li>
                             <a href="{{url('company/providers/live-track')}}">
                                <i class="glyphicon glyphicon-list"></i> Live Track
                            </a>
                        </li>
                        
                    </ul>
                </li>
                


                <li>
					
                    <a href="#">
                        <i class="glyphicon glyphicon-bullhorn"></i> Settings
                        <span class="fa arrow"></span>
                    </a>
                   
                    <ul class="nav collapse">

                        <li>
                             <a href="{{url('company/profile')}}">
                                <i class="glyphicon glyphicon-list"></i> Company Profile
                            </a>
                        </li>
                        
                    </ul>
                </li>
               
            
                <li>
                    <a href="{{ url('company/auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
