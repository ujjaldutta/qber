@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-8">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->

{!! Form::open(array('url' => url('company/providers/updatebalance'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
{!! Form::hidden('id', $provider->id) !!}
   
        
				<div class="row">   
					<div class="col-lg-6  col-md-6">   
						
						<div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
							{!! Form::label('name', 'Name', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('name', $provider->name, array('class' => 'form-control','readonly' => 'readonly')) !!}
								<span class="help-block">{{ $errors->first('name', ':message') }}</span>
							</div>
						</div>
					</div>


					<div class="col-lg-6  col-md-6">   
						
						<div class="form-group  {{ $errors->has('phone') ? 'has-error' : '' }}">
							{!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('phone', $provider->phone, array('class' => 'form-control','readonly' => 'readonly')) !!}
								<span class="help-block">{{ $errors->first('phone', ':message') }}</span>
							</div>
						</div>
					</div>
			</div>
			<div class="row">   		
					<div class="col-lg-6  col-md-6">
						<div class="form-group  {{ $errors->has('current_credit_balance') ? 'has-error' : '' }}">
							{!! Form::label('current_credit_balance', 'Current Balance', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('current_credit_balance', $provider->current_credit_balance, array('class' => 'form-control')) !!}
								<span class="help-block">{{ $errors->first('current_credit_balance', ':message') }}</span>
							</div>
						</div>
					</div>
				</div>



       <div class="form-group">
						<div class="col-md-12">
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('company/providers')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span> Back
							</button>
						<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Update
							</button>
						</div>
					</div>
		
					
    

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
 @endsection       
