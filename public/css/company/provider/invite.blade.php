@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Invite Providers
            
        </h3>
        @if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Provider</th>
			<th>Phone</th>
			<th>Details</th>
			<th>Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($providers as $provider)
        <tr>
			<td>{{ $provider->name }}</td>
			<td>{{ $provider->phone }}</td>
			<td>
				@if($provider->profile)
				Location:{{ $provider->profile->nationality }} About: {{ $provider->profile->service_details }}</td>
				@endif
			<td>

				
				
		{!! Form::open(array('url' => url('company/providers/sendinvite'), 'method' => 'post', 'class' => 'bf','id'=>'form'.$provider->id)) !!}
		{!! Form::hidden('provider_id', $provider->id) !!}
		<span data-placement="top" data-toggle="tooltip" title="Invite"><button class="btn btn-link btn-xs" data-title="Invite" type="submit" >
			<span class="fa fa-users"></span></button></span>
		
		{{Form::close()}}
		

				<script>
		$(document).ready(function(){
			$('#form{{$provider->id}}').submit(function(e){
			  e.preventDefault();
				url = $(this).attr('action');
				
				BootstrapDialog.confirm('Are you sure you want to send Invitation?', function(result){
					if(result) {
						
						$.ajax({
							  type: "POST",
							  url: url,
							  data:$('#form{{$provider->id}}').serialize(),
							  success: function(result) {
								   if (result.success) window.location = 'invite';
								
							  }
							});
					}
				});
			});

		});
		</script>	
				
			</td>
        </tr>

        
    @endforeach


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script>
	

</script>
@endsection
