@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Provider List
            
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
			<tr><th >Filter By : Service Type
			<select name="servicetype" onchange="window.location.href='{{url('company/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'nationality'=>Request::input('nationality'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date')])}}'+'&servicetype='+this.value"><option value="">All Service Type</option>
			@foreach($servicetype as $type)
				<option value="{{$type->id}}" {{ Request::input('servicetype')==$type->id ? 'selected' : '' }} >{{$type->name}}</option>
			 @endforeach
			</select></th>

			<th>
				Nationality
			<select name="nationality" onchange="window.location.href='{{url('company/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date')])}}'+'&nationality='+this.value"><option value="">All Country</option>
			@foreach($nationality as $country)
				<option value="{{$country->code}}" {{ Request::input('nationality')==$country->code ? 'selected' : '' }} >{{$country->name}}</option>
			 @endforeach
			</select>
			<th>
			From Date 


											
									<div class='input-group date' id='datetimepicker1'>
									
									{!! Form::text('from_date', Request::input('from_date'), array('class'=>'form-control','id'=>'from_date'))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
			</th>





			<th>
			To Date

											
									<div class='input-group date' id='datetimepicker2'>
									
									{!! Form::text('to_date', Request::input('to_date'), array('class'=>'form-control','id'=>'to_date','onChange'=>"alert('test')"))!!}
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>

			
								
			</th>
			<th><input type="button" onclick="filterbydate()" value="Filter Date"/></th>

			</tr>
			

			
        <tr>
           
			<th>Phone
				<a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'phone', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-down"></i></a>

			</th>
            <th>Name
				<a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'name', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-down"></i>
    </a>
            </th>
            <th>Jobs
			<a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'servicecount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'servicecount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-down"></i>
</a>
            </th>

            <th>Duration
			
            </th>

            
            <th>Income Amount

				<a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'asc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-up"></i>
    </a>
    <a href="{{url('company/providers/') . '?' . http_build_query(['sort' => 'amount', 'order' => 'desc','page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'from_date'=>Request::input('from_date'),'to_date'=>Request::input('to_date'),'nationality'=>Request::input('nationality')])}}">
        <i class="fa fa-chevron-down"></i></a>
            </th>

            <th>Credit Amount </th>
			<th>Avg. Rating</th>
            
            <th>Account No
            </th>
            <th width="20%">Action</th>
            
        </tr>
        </thead>
        <tbody>

	@foreach ($providers as $user)
        <tr>
			
			<td>{{ $user->phone }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->servicecount}}</td>
			<td>{{ $user->spenttime}} Minutes</td>
			<td>${{ $user->amount }}</td>
			<td>${{ $user->commission }}</td>
			<td>{{ number_format($user->rating,2,'.','') }}</td>
			
			<td>{{ $user->account_no }}</td>
			<td >
		
	

			<span data-placement="top" data-toggle="tooltip" title="Profile">
				<button class="btn btn-link btn-xs" data-title="Profile" type="button" onclick='window.location.href="{{url('company/providers/profile')}}/{{ $user->id }}"'>
				
					<span class="fa fa-user" aria-hidden="true"></span>
				
					</button></span>

				<span data-placement="top" data-toggle="tooltip" title="Job History">
					<button class="btn btn-link btn-xs" data-title="History" type="button" onclick='window.location.href="{{url('company/providers/jobhistory')}}/{{ $user->id }}"'>
				
					<span class="fa fa-history" aria-hidden="true"></span>
				
					</button></span>


					<span data-placement="top" data-toggle="tooltip" title="Sattle Balance">
					<button class="btn btn-link btn-xs" data-title="Sattle Balance" type="button" onclick='window.location.href="{{url('company/providers/sattlebalance')}}/{{ $user->id }}"'>
				
					<span class="fa fa-usd" aria-hidden="true"></span>
				
					</button></span>	

			



    
			</td>

        </tr>

        
    @endforeach
<tr>
<td colspan="10">{{ $providers->appends(Request::except('page'))->links() }}</td>
</tr>


        </tbody>
    </table>





    
@endsection

{{-- Scripts --}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script>
 $(function () {
                $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('#datetimepicker2').datetimepicker({
					format: 'YYYY-MM-DD HH:mm'
					
						});

              
                
            });
function filterbydate(){
var url='{{url('company/providers/') . '?' . http_build_query(['sort' => Request::input('sort'), 'order' => Request::input('order'),'page'=>Request::input('page'),'servicetype'=>Request::input('servicetype'),'nationality'=>Request::input('nationality')])}}'+'&from_date='+encodeURIComponent($("#from_date").val())+'&to_date='+encodeURIComponent($("#to_date").val());

window.location.href=url;


}
</script>
@endsection
