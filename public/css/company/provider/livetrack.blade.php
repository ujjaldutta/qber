@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-6">

<div class="row">
			<div class="col-lg-4 col-md-4">

				 <div class="form-group ">
           
            <div class="controls">
              
               
            </div>
        </div>
   </div>
</div>


	<div class="row">
					<div class="col-lg-12">
					<div id="map_canvas"></div>
					</div>
					</div>


      </div>
</div>

@endsection
<style>
 #map_canvas {
        height: 100%;
      }

</style>
{{-- Scripts --}}
@section('scripts')
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsFiXBtATwbqSaMB16DIjxOukFvndgv9k&callback=initialize">
    </script>
 <script>

	 var markers=[];
    

	 $(document).ready(function(){
		
				   $.ajax({
					  url: '{!! url("company/providers/get-providers") !!}',
					   type: 'POST',
					  data:{_token:'{{ csrf_token() }}' },
					 
					  success: function(data) {
						  
						addmarker(data.providers);
					  },
					 
				   });

				 
				

		 })



function addmarker(data){
var bounds = new google.maps.LatLngBounds();

var infoWindowContent=[];
								
    
   
    
    for( i = 0; i < data.length; i++ ) {
		if(!data[i]['current_latitude'] || !data[i]['current_longitude'])
			continue
		
		var content=[data[i]['name'],data[i]['current_latitude'],data[i]['current_longitude']]
		markers.push(content);
		var details
		if(data[i]['profile'])
		details=data[i]['profile']['service_details'];	

		content= ['<div class="info_content">' +
        '<h3>'+data[i]['name']+'</h3>' +
        '<p>'+details+'</p>' +
      
        '</div>'];
        
        infoWindowContent.push(content);
	}
                    

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        markers[i] = marker
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });

}
 var map;

function initialize() {
   
    var myLatlng = new google.maps.LatLng(25.29161,51.53044);
    var mapOptions = {
        
        center: myLatlng,
        zoom: 10,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    
    
}

     
    </script>
  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>

@endsection
