@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-12 col-md-8">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->
@if (isset($provider))
{!! Form::model($provider, array('url' => url('company/providers/saveprofile') . '/' . $provider->id, 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@else
{!! Form::open(array('url' => url('company/providers/saveprofile'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
@endif
   
        
				<div class="row">   
					<div class="col-lg-6  col-md-6">   
						
						<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
							{!! Form::label('name', 'Name', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('name', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								<span class="help-block">{{ $errors->first('name', ':message') }}</span>
							</div>
						</div>
					</div>
					
					<div class="col-lg-6  col-md-6">
						<div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
							{!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('phone', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
								<span class="help-block">{{ $errors->first('phone', ':message') }}</span>
							</div>
						</div>
					</div>
				</div>




				<div class="row">   
					<div class="col-lg-6  col-md-6">   
						
						<div class="form-group  ">
							{!! Form::label('service_details', 'Service Details', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::textarea('service_details', $provider->profile->service_details, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>
					
					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($provider->profile->image)
								<img src="{{url($provider->profile->image)}}" width="300"/>
								@endif								
							</div>
						</div>
					</div>

				</div>



				<div class="row">   
					

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('team_size', 'Team Size', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('team_size', $provider->profile->team_size, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('nationality', 'Nationality', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('nationality', $provider->profile->nationality, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>


					
				</div>




				<div class="row">   
					

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('arabic_speaking', 'Arabic Speaking', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('arabic_speaking', $provider->profile->arabic_speaking, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('own_car', 'Own Car', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('own_car', $provider->profile->own_car, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>


					
				</div>



				<div class="row">   
					

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('max_purchase_limit', 'Max Purchase Limit', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('max_purchase_limit', $provider->profile->max_purchase_limit, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>

					<div class="col-lg-6  col-md-6">
						<div class="form-group  ">
							{!! Form::label('current_credit_balance', 'Credit Balance', array('class' => 'control-label')) !!}
							<div class="controls">
								{!! Form::text('current_credit_balance', $provider->companyaccount->current_credit_balance, array('class' => 'form-control','readonly' => 'readonly')) !!}
								
							</div>
						</div>
					</div>


					
				</div>




				<div class="row">   
					
					<div class="col-lg-3  col-md-3">
						<div class="form-group  ">
							{!! Form::label('image', 'Banner Image1', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($provider->profile->banner_img1)
								<img src="{{url($provider->profile->banner_img1)}}" width="200"/>
								@endif								
							</div>
						</div>
					</div>

					<div class="col-lg-3  col-md-3">
						<div class="form-group  ">
							{!! Form::label('banner_img2', 'Banner Image2', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($provider->profile->banner_img2)
								<img src="{{url($provider->profile->banner_img2)}}" width="200"/>
								@endif								
							</div>
						</div>
					</div>

					<div class="col-lg-3  col-md-3">
						<div class="form-group  ">
							{!! Form::label('banner_img3', 'Banner Image3', array('class' => 'control-label')) !!}
							<div class="controls">
								@if($provider->profile->banner_img3)
								<img src="{{url($provider->profile->banner_img3)}}" width="200"/>
								@endif								
							</div>
						</div>
					</div>


					
				</div>




       <div class="form-group">
						<div class="col-md-12">
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('company/providers')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span> Back
							</button>
						
							<!--<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>Save
							</button>-->
						</div>
					</div>
		
					
    

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
 @endsection       
