@extends('company.layouts.default')

{{-- Web site Title --}}
@section('title') {!! $title !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <h3>
        {{$title}}
    </h3>
    <div class="row">
			<div class="col-lg-4 col-md-4">
						@if(Session::has('message'))
						<div class="alert {{ Session::get('alert-class', 'alert-info') }}">
							{{ Session::get('message') }}
						  </div>
						  
						@endif

<!-- ./ tabs -->

{!! Form::model($company, array('url' => url('company/profile/update') , 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}

   
        
       
        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::email('email', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
                <span class="help-block">{{ $errors->first('email', ':message') }}</span>
            </div>
        </div>

        <div class="form-group  {{ $errors->has('phone') ? 'has-error' : '' }}">
            {!! Form::label('phone', 'Phone', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('phone', null, array('class' => 'form-control','readonly' => 'readonly')) !!}
                <span class="help-block">{{ $errors->first('phone', ':message') }}</span>
            </div>
        </div>


        <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('name', 'Company Name', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::text('name', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
            </div>
        </div>



        <div class="form-group  {{ $errors->has('address') ? 'has-error' : '' }}">
            {!! Form::label('adddress', 'Company Address', array('class' => 'control-label')) !!}
            <div class="controls">
                {!! Form::textarea('address', null, array('class' => 'form-control','required' => 'required')) !!}
                <span class="help-block">{{ $errors->first('address', ':message') }}</span>
            </div>
        </div>

        
        <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
            {!! Form::label('password', 'Password', array('class' => 'control-label')) !!}
            <div class="controls">
				@if (isset($company))
				
                {!! Form::password('password', array('class' => 'form-control')) !!}
                @else
				{!! Form::password('password', array('class' => 'form-control','required' => 'required')) !!}
                @endif
                <span class="help-block">{{ $errors->first('password', ':message') }}</span>
            </div>
        </div>
        <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            {!! Form::label('password_confirmation', 'Confirm Password', array('class' => 'control-label')) !!}
            <div class="controls">
				@if (isset($company))
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
				 @else
				{!! Form::password('password_confirmation', array('class' => 'form-control','required' => 'required')) !!}
				  @endif
                
                <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
            </div>
        </div>

      


        <div class="form-group">
						<div>
							<button type="button" class="btn btn-sm btn-warning close_popup" onclick="location.href='{{url('admin/company')}}'">
								<span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel
							</button>
						
							<button type="submit" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;Save
							</button>
						</div>
					</div>

					
    

     {!! Form::close() !!}

   
   </div>
</div>
@endsection

@section('scripts')
        <script type="text/javascript">
            $(function () {
                $("#roles").select2()
            });
        </script>
 @endsection       
